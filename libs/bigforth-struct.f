;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; support for bigForth structures
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  this code is modelled after Bernd Paysan's bigForth struct implementation.
  it doesn't pollute global namespace with field names (contrary to F2012 structs).

  structure fields are aligned by min(natural_size, dword).

  you can control field align with "@pack" and "@align" prefixes:
    @pack int fldname

  you can switch global align state with "@packed:" and "@aligned:"

  special struct fields:
    structname @sizeof -- structure size
    structure @offset fieldname -- field offset

  you can create unions too:
    struct{
      int a
      {
          int b
        | int c
        | int d
      }
      int e
      {
          int f
        | int g
      }
      int h
    } struct0
  this will create a union for fields "b", "c", and "d", and another union for "f" and "g".
  note "|" usage for unions. also, you *must* use nested "{ ... }" for unions.

  to access struct fields, use "structptr structname fieldname". this will leave field
  address on the stack:
    struct0 @sizeof n-allot value mystruct
    mystruct struct0 h @ . cr

  you can alloc struct fields with:
    struct XAnyEvent xany
  note that this simply reserves some room, no special access syntax for "xany" inner
  members are created. i.e. to access "xany" members, use:
    mystruct struct0 xany XAnyEvent type @ . cr
*)

vocabulary (bigforth-struct) (hidden) also (bigforth-struct) definitions

: (align-by-noclamp)  ( w l -- w' )  dup +if dup >r + 1- r@ u/ r> u* else drop endif ; (hidden)
: (align-by)  ( w l -- w' )  0 cell clamp (align-by-noclamp) ;

: (create-field-offset)  ( w+ nameaddr namecount -- )  create-named , does> @ + ; (hidden)

;; destroys PAD
: (create-field-accessor-header)  ( nameaddr namecount achar -- )  nrot pad c4s:copy-counted pad c4s:cat-char pad count create-named ;

: (create-field-get-byte)  ( w+ nameaddr namecount -- )  [char] @ (create-field-accessor-header) , does> @ + c@ ; (hidden)
: (create-field-set-byte)  ( w+ nameaddr namecount -- )  [char] ! (create-field-accessor-header) , does> @ + c! ; (hidden)

: (create-field-get-word)  ( w+ nameaddr namecount -- )  [char] @ (create-field-accessor-header) , does> @ + w@ ; (hidden)
: (create-field-set-word)  ( w+ nameaddr namecount -- )  [char] ! (create-field-accessor-header) , does> @ + w! ; (hidden)

: (create-field-get-dword)  ( w+ nameaddr namecount -- )  [char] @ (create-field-accessor-header) , does> @ + @ ; (hidden)
: (create-field-set-dword)  ( w+ nameaddr namecount -- )  [char] ! (create-field-accessor-header) , does> @ + ! ; (hidden)

: (create-field-accessors)  ( w+ length nameaddr namecount -- )
  2>r case
    1 of dup 2r@ (create-field-get-byte) dup 2r@ (create-field-set-byte) endof
    2 of dup 2r@ (create-field-get-word) dup 2r@ (create-field-set-word) endof
    4 of dup 2r@ (create-field-get-dword) dup 2r@ (create-field-set-dword) endof
  endcase 2rdrop drop
;

true value (struct-aligned)  ;; is this struct aligned?
true value (field-do-align)  ;; align current field?

;; this also creates "@" and "!" accessors for 1, 2 and 4 byte fields
: (field:)  ( w+ length -- w+ )
  parse-name dup not-?abort" cannot create empty field" 2>r
  (field-do-align) if dup nrot (align-by) swap endif
  (struct-aligned) to (field-do-align)  ;; restore default aligning
  over 2r@ (create-field-offset)  ( w+ length | addr count )
  2dup 2r> (create-field-accessors)  ( w+ length )
  +  ;; advance w+
; (hidden)

: ('struct-sizeof)  ( -- length )  ' cfa->pfa @ ; (hidden)


0 value (#struct) (hidden)
0 value (old-current) (hidden)

: (find-field)  ( addr count stdefptr -- fieldcfa )  \ name
  cell+ @ voc-search ifnot
    ?endcr if space endif ." unknown field \`" type ." \`! "
    drop err-word-expected error
  endif
;  (hidden)


;; special field names, always present
: (@offset)  ( stdefptr -- fieldofs )
  parse-name rot (find-field) cfa->pfa @ [compile] literal
; (hidden)

: (@sizeof)  ( stdefptr -- size )
  @ [compile] literal
; (hidden)


: (special-name)  ( cfa -- )  \ name
  reladdr, parse-name dup 1 64 within not-?abort" invalid special name"
  dup 1+ n-allot c1s:copy-counted
; (hidden)

create (special-list)
  ' (@sizeof) (special-name) @sizeof
  ' (@offset) (special-name) @offset
  0 ,
create; (hidden)

: (do-special)  ( stdefptr addr count -- ... true // stdefptr addr count false )
  (special-list) begin dup @ while cell+
    >r 2dup r@ bcount s=ci if 2drop r> cell- @execute true exit endif
    r> bcount +
  repeat drop false
; (hidden)


: {  ( len -- len len len ) dup dup 1 +to (#struct) ;
: |  ( len +len actlen -- len maxlen len ) max over ;
: }  (#struct) if 1 -to (#struct) max nip exit endif
  get-current previous previous (old-current) set-current
  create immediate swap , ( size) , ( wordlist)
 does>
  parse-name (do-special) ifnot rot (find-field) state @ if compile, else execute-tail endif endif
;

: byte    ( w0 -- w1 )  1 (field:) ;
: short   ( w0 -- w1 )  2 (field:) ;
: word    ( w0 -- w1 )  2 (field:) ;
: cell    ( w0 -- w1 )  1 cells (field:) ;
: dword   ( w0 -- w1 )  4 (field:) ;
: ptr     ( w0 -- w1 )  cell ;
: int     ( w0 -- w1 )  cell ;
: double  ( w0 -- w1 )  2 cells (field:) ;
: qword   ( w0 -- w1 )  2 cells (field:) ;
: tbyte   ( w0 -- w1 )  10 (field:) ;
: string  ( w0 -- w1 )  chars (field:) ;
: struct  ( w0 -- w1 )  ('struct-sizeof) (field:) ;

;; disable aligning for the next field
: @pack   ( -- )  false to (field-do-align) ;
;; align next field
: @align  ( -- )  true to (field-do-align) ;

;; disable struct aligning
: @packed:   ( -- ) false dup to (struct-aligned) to (field-do-align) ;
;; enable struct aligning
: @aligned:  ( -- ) true dup to (struct-aligned) to (field-do-align) ;

;; usage: 16 @align-by
;; also, disables aligning of the next field
: @align-by  ( w0 align -- w1 ) 0 max (align-by-noclamp) @pack ;

previous definitions

: struct{  ( -- )
  get-current to (bigforth-struct):(old-current)  ;; save old CURRENT
  ;; create new anon wordlist, set it as CURRENT, and push it into CONTEXT stack
  forth:wordlist also dup set-current forth:(set-context)
  also (bigforth-struct)  ;; push bigstruct dict to CONTEXT stack too
  0 to (bigforth-struct):(#struct)  ;; to support "{ ... }" inside structs
  true to (bigforth-struct):(field-do-align)  ;; align fields
  true to (bigforth-struct):(struct-aligned)  ;; align fields
  0  ;; current struct size
;
