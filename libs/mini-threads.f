;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: asm os-errno

(*
  note that new thread has pad, but not tib. it means that you cannot use tib
  for input.
  also, don't try to compile code, or TLOAD files: those things won't work.
*)

vocabulary thread
also thread definitions


: (trd-die)  ( code -- )
  \ endcr ." exiting from the thread " os:get-tid . cr
  >r ['] forth:(thread-cleanup) catch
  pad-deallocate
  \ pad-allocate endcr ." !!!: " (pad-tres) @ . cr
  ;; free pad
  ;;pad-area @ ?dup if pad-area #pad-area-resv - #pad-area-resv #pad-area + os:munmap . cr endif
  r> os:(trd-exit)
;
(hidden)


: trd-abort-cleanup  ( -- )
  ERR-USER-ERROR (trd-die)
;

: trd-error-cleanup  ( code -- )
  (trd-die)
;


: (thread-starter)  ( cfa -- )
  rp0! >r sp0!
  fpu-reset
  ['] trd-abort-cleanup (abort-cleanup-ptr) !
  ['] trd-abort-cleanup (abort-ptr) !
  ['] trd-error-cleanup (error-ptr) !
  ['] trd-error-cleanup (fatal-error-ptr) !
  pad-allocate
  forth:(thread-init)
  \ ." starting thread...\n"
  r> execute
  0 (trd-die)
;

: (tls-sp0!)  ( value baseaddr -- sp0 )  ['] sp0 uservar-@ofs + ! ;
: (tls-rp0!)  ( value baseaddr -- rp0 )  ['] rp0 uservar-@ofs + ! ;
: (tls-#sp!)  ( value baseaddr -- sp0 )  ['] #sp uservar-@ofs + ! ;
: (tls-#rp!)  ( value baseaddr -- rp0 )  ['] #rp uservar-@ofs + ! ;
: (tls-size!)  ( value baseaddr -- size )  ['] forth:(user-full-size) uservar-@ofs + ! ;
: (tls-addr!)  ( value baseaddr -- size )  ['] forth:(user-base-addr) uservar-@ofs + ! ;

: (tls-sp0@)  ( baseaddr -- sp0 )  ['] sp0 uservar-@ofs + @ ;
: (tls-rp0@)  ( baseaddr -- rp0 )  ['] rp0 uservar-@ofs + @ ;
: (tls-size@)  ( baseaddr -- size )  ['] forth:(user-full-size) uservar-@ofs + @ ;


;; this will allocate userarea and stacks for a new thread
;; it will also copy userarea defaults, set addresses, stack sizes, and stack pointers
: (alloc-tls)  ( dsize rsize -- baseaddr )
  ;; for simplicity, we will copy the whole user area, even if it isn't wholly used
  2dup cells swap cells + 8 +cells forth:(user-area-max-size) + dup >r  ;; ( dsize rsize bytes | bytes )
  os:prot-r/w os:mmap not-?abort" cannot allocate thread memory"
  r> swap dup >r
  ;; init with default user area
  forth:(user-area-default) r@ forth:(user-area-max-size) 0 max cmove
  ;; setup basic tls params
  (tls-size!)
  r@ r@ (tls-addr!)
  ;; setup stack sizes
  over r@ (tls-#sp!)
  r@ (tls-#rp!)
  ;; setup stack pointers
  r@ (tls-size@) 4 -cells r@ + dup r@ (tls-sp0!)
  swap cells - r@ (tls-rp0!)
  r>
;

;; this will be factored to "spawn-ex" later
: spawn  ( cfa -- tid )
  ;; allocate memory for userarea and stacks
  forth:(user-area-default) ['] #sp uservar-@ofs + @
  forth:(user-area-default) ['] #rp uservar-@ofs + @
  (alloc-tls) >r
  ;; thread stack contains cfa and user-defined data (cfa is first to pop)
  r@ (tls-sp0@) cell- !
  0x29a r@ (tls-sp0@) !  ;; udata
  ['] (thread-starter)
  ;; prepare other clone args
  r@ (tls-sp0@) r@ (tls-rp0@)
  [ 0
    \ os:CLONE-CHILD-CLEARTID or
    \ os:CLONE-CHILD-SETTID or
    os:CLONE-DETACHED or ;; pure and useless compatibility flag that does nothing
    os:CLONE-FILES or
    os:CLONE-FS or
    os:CLONE-IO or
    os:CLONE-PARENT-SETTID or
    os:CLONE-SETTLS or
    os:CLONE-SIGHAND or
    os:CLONE-SYSVSEM or
    os:CLONE-THREAD or
    os:CLONE-VM or
  ] literal
  ;; create TLS descriptor
  r> 16 ralloca swap >r dup
  forth:(user-tls-entry-index) over ! cell+  ;; entry index
  r@ over ! cell+           ;; base address
  r@ (tls-size@) 4095 + 12 rshift over ! cell+  ;; limit in pages
  0b1_0_1_0_00_1 swap !     ;; flags
  0 0 ;; no tid ptrs
  os:clone
  r> 16 rdealloca >r
  dup 0xffff_f000 u>= if
    ;; error
    os:errno-name type cr bye
    r> dup (tls-size@) os:munmap drop
    abort" cannot create thread!"
  endif
  rdrop
;


previous definitions
