;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple locals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(*
69 value val

: loctest {{ a b c || d  -- }}
  ." hello!\n"
  ." a=" a . cr
  ." b=" b . cr
  ." c=" c . cr
  ." c=" d . cr

  b to d
  ." new d=" d . cr
  10 +to d
  ." new d+10=" d . cr

  ." d by addr=" to^ d @ . cr

  ." val=" val . cr
  666 to val
  ." new val=" val . cr
;
*)

vocabulary (locvoc) also (locvoc) definitions

0 value locptr-buf
0 value with-locals?
compiler:allocate-ctlid constant (CTLID-CBLOCK-LOCS)


: reset-locals  ( doalloc -- )
  locptr-buf ifnot
    ifnot exit endif
    4096 os:prot-r/w os:mmap not-?abort" cannot allocate memory for locals"
    to locptr-buf
  else drop endif
  locptr-buf 8 erase
;

: clear-locals  ( -- ) false reset-locals  false to with-locals? ;

..: forth:(abort-cleanup)  ( -- )  clear-locals ;..
..: forth:(startup-init)  ( -- )  0 to locptr-buf false to with-locals? ;..

..: forth:(exc0!)  ( -- )  0 (locptr!) clear-locals ;..
..: forth:(catch-saver)  ( -- )
  r> (locptr@) >r [: ( restoreflag ) ( locptr )  r> swap if r> (locptr!) else rdrop endif >r ;] >r >r
;.. (hidden)

(*
  locals page:
    dd locals-size  -- in bytes (but locals allocation is in cells)

  local record:
   db namelen
     name(namelen)
   dd offset
   dd size

  should end with empty name
*)

: locals-size  ( -- cells )  locptr-buf dup if @ endif ;

: dump-locals  ( -- )
  locptr-buf dup if
    begin cell+ dup c@ while
      dup bcount + @ 4 .r ."  name:<" dup bcount type ." > "
      dup bcount + cell+ @ . cr
      bcount + cell+
    repeat
  endif drop
;

: renum-args  ( -- )
  locals-size >r
  locptr-buf dup if
    begin cell+ dup c@ while
        \ dup bcount + @ 4 .r ."  name:<" dup bcount type ." >: " dup bcount + cell+ @ . cr
      bcount +
      r> over cell+ @ - dup >r over !
    cell+ repeat
  endif drop rdrop
    \ ." ====\n" dump-locals
;

: find-local-free  ( -- offset )
  locptr-buf ?dup if begin cell+ dup c@ while bcount + cell+ repeat
  else true reset-locals locptr-buf cell+
  endif
;

: find-local  ( addr count -- offset true // false )
  locptr-buf dup if
    begin cell+ dup c@ while
      >r 2dup r@ bcount s=ci if 2drop r> bcount + @ true exit endif
      r> bcount + cell+
    repeat
  endif drop 2drop false
;

: (is-alpha)  ( ch -- flag )
  case
    [char] A [char] Z bounds-of true endof
    [char] a [char] z bounds-of true endof
    [char] 0 [char] 9 bounds-of true endof
    127 >of true endof
    false swap
  endcase
;

: (is-digit)  ( ch -- flag)  10 digit? ;

: check-name  ( addr count predicatecfa -- flag )
  nrot bounds do i c@ over execute ifnot drop false unloop exit endif loop drop true
;

: is-alpha-name?  ( addr count -- flag )  ['] (is-alpha) check-name ;
: is-digit-name?  ( addr count -- flag )  ['] (is-digit) check-name ;

create bad-names
  " to" compiler:c1str,
  " +to" compiler:c1str,
  " -to" compiler:c1str,
  " ^to" compiler:c1str,
  " exit" compiler:c1str,
0 ,

: is-bad-name?  ( addr count -- flag )
  bad-names begin dup @ while
    >r 2dup r@ bcount s=ci if rdrop 2drop false exit endif
  r> c1s:skip-aligned repeat
  drop 2drop true
;

: is-good-local-name?  ( addr count -- flag )
  dup 1 64 within ifnot 2drop false exit endif
  2dup is-alpha-name? ifnot 2drop false exit endif
  2dup is-digit-name? if 2drop false exit endif
  [execute-tail] is-bad-name?
;

: new-local  ( addr count size -- offset )
  nrot 2dup is-good-local-name? not-?abort" invalid local name"
  2dup find-local ?abort" duplicate local name"
  find-local-free
  dup locptr-buf - 4000 > ?abort" too many locals"
  dup >r c1s:copy-counted
  locptr-buf @ r> bcount + dup >r ! r> cell+  ;; ( size ldptr )
  2dup ! cell+ 0c!
  locptr-buf +!
;


: end-args?  ( addr count -- addr count flag )  2dup 2dup " |" s= nrot " ||" s= or ;
: end-locals?  ( addr count -- addr count flag )  2dup " }}" s= ;
: end-locpart?  ( addr count -- addr count flag )  2dup " --" s= ;

: parse-locdef  ( -- )
  ;; parse arguments
  begin parse-name dup not-?abort" \`}}\` expected" end-args? not-while end-locals? not-while end-locpart? not-while cell new-local repeat
  renum-args
  ;; parse locals
  locals-size nrot
  end-args? if 2drop
    begin parse-name dup not-?abort" \`}}\` expected" end-locals? not-while end-locpart? not-while cell new-local repeat
  endif
  end-locpart? if
    begin 2drop parse-name dup not-?abort" \`}}\` expected" end-locals? until
  endif 2drop
  locals-size swap [compile] literal [compile] literal
  compile alloc-local-frame
;


;; {{ arg0 arg1 | loc0 loc1 -- }}
: {{
  compiler:?comp
  compiler:?non-macro
  with-locals? ?abort" nested locals are not supported"
  ;; create cblock
  compile forth:LITCBLOCK
  compiler:(mark-j>)
  (CTLID-CBLOCK-LOCS)
  forth:(URFORTH-DOFORTH-ADDR) compiler:(cfa-call,)
  true reset-locals
  ['] parse-locdef catch dup if clear-locals endif throw
  true to with-locals?
; immediate


: (;)
  compiler:?comp
  with-locals? not-?abort" wtf?!"
  false to with-locals?
  (CTLID-CBLOCK-LOCS) compiler:?pairs
  compile free-local-frame
  compile exit
  compiler:(resolve-j>)
  compile execute-tail
; (hidden)

: compile-locaddr  ( locoffs -- )  compile (locptr@) 2 +cells [compile] literal compile + ;

: active?  ( -- flag )  state @ with-locals? logand ;

nested-vocabulary (locvoc-specials) also (locvoc-specials) definitions
;; all normal methods: ( -- addr count false // true )

\ : locaddr:  parse-name find-local not-?abort" local name expected" compile-locaddr true ;

: (other)  ( addr count -- addr count false // true )
  2dup find-local dup if drop nrot 2drop compile-locaddr compile @ true endif
; (hidden)

: exit  compile free-local-frame " EXIT" false ;  ;; continue with normal "EXIT", because it may be not from FORTH vocabulary
;; this must be defined last!
: ;  (;) " ;" false ;  ;; continue with normal ";", because it may be not from FORTH vocabulary
previous definitions

..: value-xto-schook  ( value type addr count -- true // unchanged )
  active? if
    2dup find-local if
      compile-locaddr 2drop forth:(value-xto-type->cfa) ?compile, true exit
    endif
  endif
<;..

;; returns `1` if cfa is immediate, or `-1` if it is a normal word
;; scattered extensions should simply pass "addr count" unchanged (or changed ;-)
..: interpret-wfind  ( addr count -- cfa 1 // cfa -1 // addr count false )
  active? if
    vocid: (locvoc-specials) ['] (locvoc-specials):(other) (intepret-voc-call-helper) if exit endif
  endif
<;..


previous definitions
alias (locvoc):{{ {{
