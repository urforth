;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] forth:asmx86

.LIB-START" x86 assembler"
" asmx86.f" tload
.LIB-END

.LIB-START" x86 assembler plugins"
" user-meta.f" tload
" user-labelman.f" tload
.LIB-END

[ENDIF]
