;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiling support
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; defered words for code generator

;; set to `asm-PC` by Reset-Instruction
;; used to resolve `$`
0 value asm-$

;; current "virtual program counter"
;; used by asm, and can be used by label manager
defer-not-yet asm-PC

;; allocate some tc memory
;; assembler will never call this with negative or zero size
;; should return address of the first byte of alloted memory
;; ( size -- addr )
defer-not-yet asm-n-allot

;; read byte from tc memory
;; assembler will never try to read unallocated memory
;; (i.e. any read will be in `asm-n-allot`-ed area)
;; ( addr -- byte )
defer-not-yet asm-c@

;; write byte to tc memory
;; assembler will never try to write to unallocated memory
;; (i.e. any write will be to `asm-n-allot`-ed area)
;; ( byte addr -- )
defer-not-yet asm-c!


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; the following are constructed with the above primitives
: asm-w!  ( word addr -- )
  >r dup 0xff and r@ asm-c!
  8 rshift 0xff and r> 1+ asm-c!
;

: asm-d!  ( dword addr -- )
  >r dup 0xffff and r@ asm-w!
  16 rshift 0xffff and r> 2+ asm-w!
;


: asm-w@  ( addr -- word )
  dup >r asm-c@  0xff and  \ mask it just in case
  r> 1+ asm-c@  0xff and  \ mask it just in case
  8 lshift or
;

: asm-d@  ( addr -- dword )
  dup >r asm-w@
  r> 2+ asm-w@
  16 lshift or
;


: asm-c,  ( byte -- )  0xff and 1 asm-n-allot asm-c! ;
: asm-w,  ( word -- )  0xffff and 2 asm-n-allot asm-w! ;
: asm-,   ( dword -- )  4 asm-n-allot asm-d! ;

;; this is used in instruction builder
: asm-#,  ( value bytecount -- )
  begin
    dup 0 >
  while
    swap dup asm-c, 8 rshift swap
    1-
  repeat
  2drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find assembler instruction word
: IFind  ( c4str --> xt true // false )
  count vocid: instructions voc-search-noimm ifnot 2drop false else true endif
;
