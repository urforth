;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main assembler file
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
only forth definitions
vocabulary asmx86
also asmx86 definitions
<hidden-words>

;; create vocabulary for instructions
nested-vocabulary instructions

;; this should be 0 if we have no debugger
forth:(jmp-next-addr) value asm-instrumented-next-addr

;; ignore ts: segment overrides? (actually, convert them to ds:)
false value asm-ignore-ts


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
" asm-compat.f" tload
" asm-labelman.f" tload
" asm-errors.f" tload
" asm-tc.f" tload
" asm-gen-instr.f" tload
" parser/" tload
" definers/" tload
" instrs/" tload

<public-words>


;; utility word to get numeric argument; useful for macros and assembler commands
;; rejects undefined labels
: Get-Imm-Defined-Do-Delims  ( allow-delims-flag -- value )
  Reset-Instruction
  4 to *OpSize
  to *Imm-Allow-Other-Delims?
  Imm
  *OpReloc if
    *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
    \ *ImmName bcount asm-Label-Defined? ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
    *ImmLabelDefined ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
  endif
  *Imm
  0 to *Imm-Allow-Other-Delims?
;

: Get-Imm-Defined  ( -- value )  0 Get-Imm-Defined-Do-Delims ;
: Get-Imm-Defined-Allow-Delims  ( -- value )  1 Get-Imm-Defined-Do-Delims ;


;; create aliases for all public words from vocid
: Register-Macros-From-VocId  ( vocid -- )
  current @ >r ;; save "CURRENT"
  also [compile] instructions definitions
  [:  ( nfa -- exitflag )
    dup nfa->ffa ffa@ [ (WFLAG-SMUDGE) (WFLAG-HIDDEN) or ] literal and
    ifnot
      dup nfa->cfa swap id-count
      forth:(alias-str)
    else
      drop
    endif
    false
  ;] foreach-word drop
  previous
  r> current !  ;; restore "CURRENT"
;


: (asm-tib)  ( -- )
  ;; parse labels
  begin
    ;; allow empty lines
    tk-eol? if exit endif

    tk-id? ERRID_ASM_SYNTAX_ERROR not-?asm-error

    ;; label?
    lexer:SkipBlanks tib-peekch [char] : =
  while
    lexer:tkvalue count asm-PC asm-Make-Label
    lexer:NextToken tk-delim? ERRID_ASM_INTERNAL_ERROR not-?asm-error
    lexer:tkvalue [char] : = ERRID_ASM_INTERNAL_ERROR not-?asm-error
    lexer:NextToken
  repeat
  \ asm-Dump-Labels

  lexer:tkvalue IFind ifnot
    ;; check for equ
    lexer:PeekToken lexer:tktype-id = if
      count " EQU" s= if
        Reset-Instruction
        4 to *OpSize
        ;; copy name to `*OffName`, it is not used by `Imm`
        lexer:tkvalue count *OffName c1s:copy-counted
        lexer:NextToken  ;; to equ
        lexer:NextToken  ;; skip equ
        Imm
        ;; check if it is using only defined labels
        *OpReloc if
          *ImmForthType ERRID_ASM_INVALID_FORWARD_REF ?asm-error
          *ImmName c@ ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
          \ *ImmName bcount asm-Label-Defined? ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
          *ImmLabelDefined ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
        endif
        \ ." |" *OffName bcount type ." | " *Imm . cr
        *OffName bcount *Imm asm-Make-Constant
        \ asm-Dump-Labels
        exit
      endif
    endif
    lexer:tkvalue count type [char] ? emit space ERRID_ASM_SYNTAX_ERROR asm-error
  endif

  ;; get flag
  lexer:tkvalue count CondAllowed?
  ;; skip command name
  lexer:NextToken

  ;; augment command with condition
  (x-resetjump)
  if
    ?JCond if
      ExpectCommaOrEOL
      ;; compile jump over the command if we need to
      (x-mkjump)
    endif
  endif

  ;; call instruction parser
  execute

  (x-fixjump)
; (hidden)


: asm-str  ( addr count -- )
  tibstate>r
  #tib ! tib ! >in 0!
  ;; save lexer state
  sp@ >r
  lexer:sbuf.size 3 + 2 rshift 0 do false loop
  sp@ dup >r lexer:(Save-Parser-State-To)
  ;; reinit lexer
  lexer:PrepareLineParser
  lexer:NextToken
  begin
    (asm-tib)
    lexer:tktype
  not-until
  ;; restore lexer state
  r> lexer:(Restore-Parser-State-From)
  r> sp!
  \ lexer:sbuf.size 3 + 2 rshift 0 do drop loop
  ;; restore TIB
  r>tibstate
; (public)


: (code-word)  ( -- )
  create-header
  ;; skip everything to eol
  [compile] \
  lexer:PrepareLineParser
  lexer:NextToken
  begin
    tk-id? if
        \ lexer:tkvalue count endcr type cr
      lexer:tkvalue count " ENDCODE" s= if break endif
    endif
    (asm-tib)
    lexer:tktype
    ifnot
      tib-peekch ?dup if
        nl = if tib-skipch endif
      else
        \ tib-default? ERRID_ASM_ENDCODE_EXPECTED not-?asm-error
        \ tib-reset
        forth:(dbginfo-reset)
        refill ERRID_ASM_ENDCODE_EXPECTED not-?asm-error
      endif
      lexer:PrepareLineParser
      lexer:NextToken
    endif
  repeat
  forth:create;
  ;; latest disasm-word
  asm-Check-Undef-Labels
  smudge
; (hidden)

;; here, so macros can invoke "asm-str"
" macros/" tload
vocid-of macro-instrs Register-Macros-From-VocId


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
only forth definitions

: code:  asmx86:(code-word) ;
