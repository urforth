;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; jumps/calls/loops
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ opc are words, others are bytes
\ first three are for indirect variant:
\   opcode_size opcode register
\ second two are for near variant:
\   opcode_size opcode
\ thirdt two are for short variant:
\   opcode_size opcode

\ indirect  nearver  shortver
   1 $FF 4    1 $E9     1 $EB  Jxx JMP
   1 $FF 2    1 $E8     0   0  Jxx CALL
alias JMP JP

\ indirect  nearver  shortver
0 0 0       0 0       1   $E3  Jxx JECXZ
0 0 0       0 0       2 $E367  Jxx JCXZ
0 0 0       0 0       1   $E2  Jxx LOOP
0 0 0       0 0       1   $E1  Jxx LOOPZ
0 0 0       0 0       1   $E0  Jxx LOOPNZ

;; to remember "above" and "below", use vertical coordinate axis from your math classes
;; i.e. one number is above another if it is greater (unsigned)
;; jump conditions describing the first operand
;; so, to check eax>ebx:
;;   cp  eax,ebx
;;   jr  a,.greater

\ indirect  nearver  shortver
0 0 0       2 $800F     1 $70  Jxx JO     ;; O=1  (overflow)
0 0 0       2 $810F     1 $71  Jxx JNO    ;; O=0  (overflow)
0 0 0       2 $820F     1 $72  Jxx JC     ;; C=1
0 0 0       2 $830F     1 $73  Jxx JNC    ;; C=0
0 0 0       2 $840F     1 $74  Jxx JZ     ;; Z=1
0 0 0       2 $850F     1 $75  Jxx JNZ    ;; Z=0
0 0 0       2 $860F     1 $76  Jxx JNA    ;; C=1 || Z=1
0 0 0       2 $870F     1 $77  Jxx JA     ;; C=0 && Z=0
0 0 0       2 $880F     1 $78  Jxx JS     ;; S=1
0 0 0       2 $890F     1 $79  Jxx JNS    ;; S=0
0 0 0       2 $8A0F     1 $7A  Jxx JPE    ;; P=1  (parity)
0 0 0       2 $8B0F     1 $7B  Jxx JPO    ;; P=0  (parity)
0 0 0       2 $8C0F     1 $7C  Jxx JL     ;; S<>O (sign <> overflow)
0 0 0       2 $8D0F     1 $7D  Jxx JGE    ;; S=O  (sign = overflow)
0 0 0       2 $8E0F     1 $7E  Jxx JLE    ;; S<>O || Z=1 (sign <> overflow, or zero)
0 0 0       2 $8F0F     1 $7F  Jxx JG     ;; S=O && Z=0  (sign = overflow and non-zero)

alias JZ  JE    ;; JE: Z=1
alias JC  JB    ;; JB: C=1
alias JC  JNAE  ;; JNAE: C=1
alias JNC JAE   ;; JAE: C=0
alias JNC JNB   ;; JNB: C=0
alias JNZ JNE   ;; JNE: Z=0
alias JNA JBE   ;; JBE: C=1 || Z=1
alias JA  JNBE  ;; JNBE: C=0 && Z=0
;; k8: sorry, but i am using "jp" as "jump near"
;;     nobody needs parity jumps anyway ;-)
;; alias JPE JP
alias JPO JNP   ;; JNP: P=0  (parity)
alias JL  JNGE  ;; JNGE: S<>O (sign <> overflow)
alias JGE JNL   ;; JNL: S=O  (sign = overflow)
alias JLE JNG   ;; JNG: S<>O || Z=1 (sign <> overflow, or zero)
alias JG  JNLE  ;; JNLE: S=O && Z=0  (sign = overflow and non-zero)

alias LOOPZ  LOOPE
alias LOOPNZ LOOPNE

(* this has no sense, because currently parser cannot parse that
alias JNE  J0<>
alias JE   J0=
alias JNS  J0>=
alias JS   J0<
alias JNE  J<>
alias JE   J=
alias JNLE J>
alias JNGE J<
alias JNL  J>=
alias JNG  J<=
alias JNBE JU>
alias JNAE JU<
alias JNB  JU>=
alias JNA  JU<=
*)

\ this is always short
JRx JR
