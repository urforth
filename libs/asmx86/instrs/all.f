;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create actual machine instruction compiling words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also instructions definitions
<public-words>

" simple.f" tload
" shift.f" tload
" fpu.f" tload
" misc.f" tload
" jmp.f" tload
" set-cmov.f" tload
" p5.f" tload
" specials.f" tload
;; alu should be loaded last, because it overrides bitwise words
" alu.f" tload

previous definitions
<hidden-words>
