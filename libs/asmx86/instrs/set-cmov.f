;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; conditional loads and sets
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$970F SETxx SETA
$930F SETxx SETAE
$920F SETxx SETC
$960F SETxx SETNA
$940F SETxx SETZ
$9F0F SETxx SETG
$9D0F SETxx SETGE
$9C0F SETxx SETL
$9E0F SETxx SETLE
$950F SETxx SETNZ
$900F SETxx SETO
$910F SETxx SETNO
$980F SETxx SETS
$990F SETxx SETNS
$9A0F SETxx SETP
$9B0F SETxx SETNP

alias SETA SETNBE
alias SETAE SETNB
alias SETAE SETNC
alias SETC SETB
alias SETC SETNAE
alias SETNA SETBE
alias SETZ SETE
alias SETG SETNLE
alias SETGE SETNL
alias SETL SETNGE
alias SETLE SETNG
alias SETNZ SETNE
alias SETP SETPE
alias SETNP SETPO

$0F02 LxxCMOV LAR
$0F03 LxxCMOV LSL

$0F40 LxxCMOV CMOVO
$0F41 LxxCMOV CMOVNO
$0F42 LxxCMOV CMOVB
$0F43 LxxCMOV CMOVNB
$0F44 LxxCMOV CMOVE
$0F45 LxxCMOV CMOVNE
$0F46 LxxCMOV CMOVBE
$0F47 LxxCMOV CMOVNBE
$0F48 LxxCMOV CMOVS
$0F49 LxxCMOV CMOVNS
$0F4A LxxCMOV CMOVP
$0F4B LxxCMOV CMOVNP
$0F4C LxxCMOV CMOVL
$0F4D LxxCMOV CMOVNL
$0F4E LxxCMOV CMOVLE
$0F4F LxxCMOV CMOVNLE

Alias CMOVB   CMOVNAE
Alias CMOVNB  CMOVAE
Alias CMOVE   CMOVZ
Alias CMOVNE  CMOVNZ
Alias CMOVBE  CMOVNA
Alias CMOVNBE CMOVA
Alias CMOVP   CMOVPE
Alias CMOVNP  CMOVPO
Alias CMOVL   CMOVNGE
Alias CMOVNL  CMOVGE
Alias CMOVLE  CMOVNG
Alias CMOVNLE CMOVG

Alias CMOVB  CMOVC
Alias CMOVNB CMOVNC
