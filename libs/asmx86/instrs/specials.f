;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; special commands (they need custom parsers)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SEG  ( -- )
  ?SegReg ERRID_ASM_EXPECT_SEGREG not-?asm-error
  to *SegReg
  (Build-Seg-Prefix)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (PUSH-MemReg)  ( -- )
  1 to *OpcSize
  *OpSize 2 < if ERRID_ASM_TYPE_MISMATCH asm-error endif
  *Mod 3 = if
    -1 to *Mod
    *R/M $50 + to *OpCode
  else
    $FF to *OpCode
    6 to *Reg
  endif
; (hidden)

: (PUSH-SegReg)  ( segreg -- )
  case
    0 of 1 $06 endof
    1 of 1 $0E endof
    2 of 1 $16 endof
    3 of 1 $1E endof
    4 of 2 $A00F endof
    5 of 2 $A80F endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  to *OpCode
  to *OpcSize
; (hidden)

: (PUSH-Imm)  ( -- )
  4 to *OpSize
  Imm
  1 to *OpcSize
  \ *ImSize 4 = *Imm -32768 and dup 0= swap -32768 = or and *OpReloc 0= and if 2 to *ImSize endif
  ;; do not allow word-sized immediate, it will generate 16-bit instruction
  *ImSize 4 = *ImSize 1 = or ifnot 4 to *ImSize endif
  *ImSize to *OpSize
  *ImSize 1 = if $6A else $68 endif to *OpCode
; (hidden)

: PUSH  ( -- )
  Reset-Instruction
  ?MemReg if
    (PUSH-MemReg)
  else
    ?SegReg if (PUSH-SegReg) else (PUSH-Imm) endif
  endif
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (POP-MemReg)  ( -- )
  1 to *OpcSize
  *OpSize 2 < if ERRID_ASM_TYPE_MISMATCH asm-error endif
  *Mod 3 = if
    -1 to *Mod
    *R/M $58 + To *OpCode
  else
    $8F to *OpCode
    0 to *Reg
  endif
; (hidden)

: (POP-SegReg)  ( segreg -- )
  case
    0 of 1 $07 endof
    1 of ERRID_ASM_INVALID_OPERAND asm-error endof
    2 of 1 $17 endof
    3 of 1 $1F endof
    4 of 2 $A10F endof
    5 of 2 $A90F endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  to *OpCode
  to *OpcSize
; (hidden)

: POP  ( -- )
  Reset-Instruction
  ?MemReg if
    (POP-MemReg)
  else
    ?SegReg ERRID_ASM_INVALID_OPERAND not-?asm-error
    (POP-SegReg)
  endif
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (MOV-CheckJCond?)  ( -- exitflag )
  ?JCond ifnot false exit endif
  ExpectComma
  lexer:SaveCurrentPos
  dup >r ;; save jump condition, we'll need it in `(x-mkjump)`
  Reset-Instruction
  $0F40 or bswap-word to *OpCode
  2 to *OpcSize
  ?Reg dup if drop *OpSize 1 > endif
  if
    ExpectComma
    ?MemReg if
      rdrop  ;; drop jump condition, we don't need it anymore
      Build-Instruction
      true
      exit
    endif
  endif
  ;; compile jump over the command
  r> (x-mkjump)
  ;; restart it; sorry for this hack
  lexer:RestoreSavedPos
  false
; (hidden)


: (MOV-R32,CR/DR?)  ( reg32 -- build-and-exit-flag )
  to *R/M
  ExpectComma
       ?CRReg if $200F to *OpCode
  else ?DRReg if $210F to *OpCode
  else false exit
  endif endif
  to *Reg
  3 to *Mod
  true
; (hidden)

: (MOV-CR/DR,R32?)  ( -- build-and-exit-flag )
       ?CRReg if $220F to *OpCode
  else ?DRReg if $230F to *OpCode
  else false exit
  endif endif
  ExpectComma ?Reg32 ERRID_ASM_INVALID_OPERAND not-?asm-error
  to *R/M
  to *Reg
  3 to *Mod
  true
; (hidden)

: (MOV-CD/DR?)  ( -- build-and-exit-flag )
  ?Reg32 if
    (MOV-R32,CR/DR?)
  else
    (MOV-CR/DR,R32?)
  endif
; (hidden)


: (MOV-MemReg,Reg)  ( -- opcode-byte )
  *Reg 0= *Mod 0= *R/M 5 = and and if
    -1 to *Mod *OpSize 1 = if $A2 else $A3 endif
  else
    *OpSize 1 = if $88 else $89 endif
  endif
; (hidden)

: (MOV-MemReg,Imm)  ( -- opcode-byte )
  Imm
  *OpSize to *ImSize
  0 to *Reg
  *OpSize 1 = if $C6 else $C7 endif
; (hidden)

: (MOV-MemReg,Reg/Imm)  ( -- opcode-byte )
  ?Reg if
    (MOV-MemReg,Reg)
  else
    (MOV-MemReg,Imm)
  endif
; (hidden)


: (MOV-Reg,MemReg)  ( -- opcode-byte )
  *Reg 0= *Mod 0= *R/M 5 = and and if
     -1 to *Mod *OpSize 1 = if $A0 else $A1 endif
  else
    *OpSize 1 = if $8A else $8B endif
  endif
; (hidden)

: (MOV-Reg,SegReg)  ( segreg -- opcode-byte )
  *OpSize 2 < ERRID_ASM_TYPE_MISMATCH ?asm-error
  *Reg to *R/M
  3 to *Mod
  to *Reg
  $8C
; (hidden)

: (MOV-Reg,Imm)  ( -- opcode-byte )
  Imm
  *OpSize to *ImSize
  *Reg *OpSize 1 = if $B0 else $B8 endif +
; (hidden)


: (MOV-Reg,Reg/Imm/SegReg)  ( -- opcode-byte )
  *R/M to *Reg
  -1 to *Mod
  ?MemReg if (MOV-Reg,MemReg)
  else ?SegReg if (MOV-Reg,SegReg)
  else (MOV-Reg,Imm)
  endif endif
; (hidden)


: (MOV-MemReg,Something)  ( -- )
  ExpectComma
  *Mod 3 = if
    (MOV-Reg,Reg/Imm/SegReg)
  else
    (MOV-MemReg,Reg/Imm)
  endif
  to *OpCode
; (hidden)


: (MOV-SegReg,MemReg)  ( -- )
  ?SegReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  to *Reg
  ExpectComma
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  4 to *OpSize
  $8E to *OpCode
; (hidden)


: (MOV-Other)  ( -- )
  Reset-Instruction
  1 to *OpcSize
  ?MemReg if (MOV-MemReg,Something) else (MOV-SegReg,MemReg) endif
; (hidden)


: MOV  ( -- )
  ;; things like "ld nz,reg,reg" will be converted to "cmovnz reg,reg"
  ;; it will fallback to general "jump over" code for invalid operands
  (MOV-CheckJCond?) if exit endif

  Reset-Instruction
  2 to *OpcSize
  0 to *OpCode

  ;; sorry for this hack
  lexer:SaveCurrentPos

  (MOV-CD/DR?) ifnot
    ;; restart it; sorry for this hack
    lexer:RestoreSavedPos
    (MOV-Other)
  endif

  Build-Instruction
  (x-fixjump)
;
alias mov ld (public)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ TODO: add some way to generate two-byte "int 3" command
: INT  ( -- )
  Reset-Instruction
  1 to *OpSize
  Imm
  *Imm 3 = if
    $CC asm-c,
  else
    $CD asm-c,
    *Imm asm-c,
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (OUT-Reg,Reg)  ( reg -- )
  *Reg 2 = *OpSize 2 = and ERRID_ASM_INVALID_OPERAND not-?asm-error
  ExpectComma
  Reset-Instruction
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ;; should be eax/ax/al
  *Reg ERRID_ASM_INVALID_OPERAND ?asm-error
  *OpSize
  case
    1 of $EE asm-c, endof
    2 of $EF66 asm-w, endof
    4 of $EF asm-c, endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
; (hidden)

: (OUT-Imm,Reg)  ( -- )
  1 to *OpSize
  Imm
  ExpectComma
  *Imm >r
  Reset-Instruction
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ;; should be eax/ax/al
  *Reg ERRID_ASM_INVALID_OPERAND ?asm-error
  *OpSize
  case
    1 of $E6 asm-c, endof
    2 of $E766 asm-w, endof
    4 of $E7 asm-c, endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  ;; *Imm
  r> asm-c,
; (hidden)

: OUT  ( -- )
  Reset-Instruction
  ?Reg if (OUT-Reg,Reg) else (OUT-Imm,Reg) endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (IN-EAX/AX/AL,Reg)  ( a-size -- )
  *Reg 2 = *OpSize 2 = and ERRID_ASM_INVALID_OPERAND not-?asm-error
  case
    1 of $EC asm-c, endof
    2 of $ED66 asm-w, endof
    4 of $ED asm-c, endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
; (hidden)

: (IN-EAX/AX/AL,Imm)  ( a-size -- )
  1 to *OpSize
  Imm
  case
    1 of $E4 asm-c, endof
    2 of $E566 asm-w, endof
    4 of $E5 asm-c, endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  *Imm asm-c,
; (hidden)

: IN  ( -- )
  Reset-Instruction
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ;; should be eax/ax/al
  *Reg ERRID_ASM_INVALID_OPERAND ?asm-error
  ExpectComma
  *OpSize
  Reset-Instruction
  ?Reg if (IN-EAX/AX/AL,Reg) else (IN-EAX/AX/AL,Imm) endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: XCHG  ( -- )
  Reset-Instruction
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ExpectComma
  *Mod 3 = if
    -1 to *Mod
    *R/M to *Reg
    ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
    *Mod 3 = *R/M 0= *Reg 0= or and *OpSize 1 > and if
      *OpSize 2 = if $66 asm-c, endif
      $90 *Reg + *R/M + asm-c, exit
    endif
  else
    ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  endif
  *OpSize 1 = if $86 else $87 endif to *OpCode
  1 to *OpcSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (IMUL-MemReg,MemReg,Imm)  ( -- )
  Imm
  1 to *OpcSize
  *ImSize 1 = if $6B else $69 endif to *OpCode
; (hidden)

: (IMUL-MemReg,MemReg)  ( -- )
  $AF0F to *OpCode
  2 to *OpcSize
; (hidden)

: (IMUL-MemReg,Imm)  ( -- )
  ?SegReg ERRID_ASM_INVALID_OPERAND ?asm-error
  Imm
  3 to *Mod
  *Reg to *R/M
  1 to *OpcSize
  *ImSize 1 = if $6B else $69 endif to *OpCode
; (hidden)

: (IMUL-MemReg,Operand)  ( -- )
  *Mod 3 = ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 1 > ERRID_ASM_INVALID_OPERAND not-?asm-error
  *R/M to *Reg
  -1 to *Mod
  ?MemReg if
    ?Comma if (IMUL-MemReg,MemReg,Imm) else (IMUL-MemReg,MemReg) endif
  else
    (IMUL-MemReg,Imm)
  endif
; (hidden)

: (IMUL-MemReg)  ( -- )
  *OpSize 1 = if $F6 else $F7 endif to *OpCode
  1 to *OpcSize
  5 to *Reg
; (hidden)

: IMUL  ( -- )
  Reset-Instruction
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ?Comma if (IMUL-MemReg,Operand) else (IMUL-MemReg) endif
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ENTER  ( -- )
  Reset-Instruction
  ;; first operand
  2 to *OpSize
  Imm
  *ImSize 4 = ERRID_ASM_NUMBER_TOO_BIG ?asm-error
  *Imm >r
  ;; comma
  ExpectComma
  ;; second operand
  1 to *OpSize
  Imm
  *ImSize 1 > *Imm 31 u> or ERRID_ASM_NUMBER_TOO_BIG ?asm-error
  $C8 asm-c,
  r> asm-w,
  *Imm asm-c,
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: BOUND  ( -- )
  Reset-Instruction
  1 to *OpcSize
  ;; first operand (register)
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ;; it should not be a 8-bit register
  *OpSize dup 1 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  >r  ;; save opsize for later use
  0 to *OpSize
  ;; comma
  ExpectComma
  ;; second operand
  ?XMemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ;; it should not be a register
  *Mod 3 = ERRID_ASM_INVALID_OPERAND ?asm-error
  *OpSize ?dup if r@ ( 1 lshift ) <> ERRID_ASM_INVALID_OPERAND ?asm-error endif
  r> to *OpSize
  $62 to *OpCode
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ARPL  ( -- )
  Reset-Instruction
  1 to *OpcSize
  $63 to *OpCode
  ;; first operand (should be a memreg)
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize dup 2 =  swap 0=  or  ERRID_ASM_TYPE_MISMATCH not-?asm-error
  ;; comma
  ExpectComma
  ;; second operand should be reg16
  ?Reg16 ERRID_ASM_INVALID_OPERAND not-?asm-error
  to *Reg
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: CMPXCHG8B  ( -- )
  Reset-Instruction
  2 to *OpcSize
  $C70F to *OpCode
  (FPU-XMemReg)
  *OpSize 0=  *OpSize 8 =  or  ERRID_ASM_TYPE_MISMATCH not-?asm-error
  1 to *Reg
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: BSWAP  ( -- )
  Reset-Instruction
  2 to *OpcSize
  ?Reg ERRID_ASM_REGISTER_EXPECTED not-?asm-error
  *OpSize 1 > ERRID_ASM_TYPE_MISMATCH not-?asm-error
  *Reg 8 lshift $C80F + to *OpCode
  Build-Instruction
;
