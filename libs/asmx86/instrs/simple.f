;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple one and two-byte instructions without operands
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$C9 simple-byte LEAVE
$CC simple-byte INT3
$CE simple-byte INTO
$37 simple-byte AAA
$3F simple-byte AAS
$99 simple-byte CDQ
$98 simple-byte CWDE
$F8 simple-byte CLC
$FC simple-byte CLD
$FA simple-byte CLI
$F5 simple-byte CMC
$A6 simple-byte CMPSB
$A7 simple-byte CMPSD
$27 simple-byte DAA
$2F simple-byte DAS
$F4 simple-byte HLT
$6C simple-byte INSB
$6D simple-byte INSD
$CF simple-byte IRETD
$9F simple-byte LAHF
$AC simple-byte LODSB
$AD simple-byte LODSD
$A4 simple-byte MOVSB
$A5 simple-byte MOVSD
$90 simple-byte NOP
$6E simple-byte OUTSB
$6F simple-byte OUTSD
$61 simple-byte POPAD
$60 simple-byte PUSHAD
$9D simple-byte POPFD
$9C simple-byte PUSHFD
\ $C3 simple-byte RET  \ moved to RETN
\ $CB simple-byte RETF  \ defined below
$9E simple-byte SAHF
$AE simple-byte SCASB
$AF simple-byte SCASD
$F9 simple-byte STC
$FD simple-byte STD
$FB simple-byte STI
$AA simple-byte STOSB
$AB simple-byte STOSD
$9B simple-byte WAIT
$D7 simple-byte XLAT
$D7 simple-byte XLATB
$F0 simple-byte LOCK
$F3 simple-byte REP
$F3 simple-byte REPE
$F3 simple-byte REPZ
$F2 simple-byte REPNE
$F2 simple-byte REPNZ

alias CMC CCF  ;; complement carry
alias STC SCF  ;; set carry

$66 simple-byte D32
$67 simple-byte A32
\ Performs an operation equivalent to SBB AL, AL without modifying any flags.
\ In other words, AL will be set to FFh or 0 depending on whether CF is set or clear.
$D6 simple-byte SETALC
alias SETALC SALC
$F1 simple-byte ICEBP

\ AAM and AAD has hidden operand, let's support it
\ $D50A simple-word AAD
\ $D40A simple-word AAM

$6698 simple-word CBW
$6699 simple-word CWD
$66A7 simple-word CMPSW
$666D simple-word INSW
$66CF simple-word IRET
$66AD simple-word LODSW
$66A5 simple-word MOVSW
$666F simple-word OUTSW
$6661 simple-word POPAW
$6660 simple-word PUSHAW
$669D simple-word POPFW
$669C simple-word PUSHFW
$66AF simple-word SCASW
$66AB simple-word STOSW

$0F06 simple-word CLTS
$0F08 simple-word INVD
$0F09 simple-word WBINVD
$0F0B simple-word UD2
$0F30 simple-word WRMSR
$0F31 simple-word RDTSC
$0F32 simple-word RDMSR
$0F33 simple-word RDPMC
$0F34 simple-word SYSENTER
$0F35 simple-word SYSEXIT
$0FA2 simple-word CPUID
$0FAA simple-word RSM

$0F77 simple-word EMMS
