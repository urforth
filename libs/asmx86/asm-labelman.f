;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; label manager interface
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; defers, so you can plug in your own label manager

;; create unresolved labels as forward ones?
0 value asm-Labman-Unresolved-As-Forwards?


;; called by `asm-error`
;; this should reset all currently pending locals and such
;; but don't call `Reinit` here, because you don't need to
;; forget everything -- this is just an error in some word,
;; not Code Red Condition
;; ( -- )
defer asm-Labman-Error-Reset

;; can be called by the user to wipe all labels
;; ( -- )
defer asm-Labman-Reinit

;; this is called by main driver
;; you can throw error here
;; ( addr count value -- )
defer-not-yet asm-Make-Constant

;; this is called by main driver
;; you can throw error here
;; ( addr count value -- )
defer-not-yet asm-Make-Label

;; this can be called when "cfa 'name'" or alike encountered
;; the main driver never calls this by itself, though
;; you can (and should) throw error here
;; ( addr count value forward -- )
defer-not-yet asm-Make-Forth-Label

;; this is used in `(?Label)`
;; it should create a new label, if we want to allow forward definitions
;; we may throw errors with `asm-error` here, or return `false`
;; instruction builder will call various fixups for this label
;; "-1" means that the label is not defined yet, and the value is a random prediction
;; ( addr count -- value 1 // value -1 // false )
defer-not-yet asm-Get-Label

0 constant LABEL-TYPE-NORMAL  \ will be used in label fixups later
\ for Forth words
1 constant LABEL-TYPE-CFA
2 constant LABEL-TYPE-PFA
\ 3 constant LABEL-TYPE-NFA
\ 4 constant LABEL-TYPE-LFA
\ 5 constant LABEL-TYPE-BFA
\ 6 constant LABEL-TYPE-SFA
\ 7 constant LABEL-TYPE-DFA
\ 8 constant LABEL-TYPE-HFA
\ TODO: add more (lol)

;; this is used in `(?Label)`
;; it is called for things like [pfa "wordname"]
;; we may throw errors with `asm-error` here, or return `false`
;; instruction builder will call various fixups for this label
;; "-1" means that the label is not defined yet
;; WARNING! for undefined labels, returned value should be offset from CFA to the requred field!
;; ( addr count type -- value 1 // value -1 // false )
defer-not-yet asm-Get-Forth-Word

;; this is used in `?Number`
;; returned value will be inlined as numeric constant, and
;; no fixup call will be made
;; ( addr count -- value true // false )
defer-not-yet asm-Get-Constant

;; you should return "label is defined" flag, so instruction builders
;; can decide if they can use shorter instruction forms
;; by returning `true` you guarantees that label value will not change
;; not called for anything except "normal" labels
;; ( addr count -- flag )
defer-not-yet asm-Label-Defined?

;; this is used in `jmp near` codegen
;; it should create a new label, if we want to allow forward definitions
;; we may throw errors with `asm-error` here
;; it is called with `asm-PC` pointing to the address the label will be put at
;; you don't need to return label value here, because `asm-Get-Label` always called first
;; ( addr count type -- )
defer-not-yet asm-Jmp-Label-Fixup

;; this is used in `jmp short` codegen
;; it should create a new label, if we want to allow forward definitions
;; we may throw errors with `asm-error` here
;; it is called with `asm-PC` pointing to the address the label will be put at
;; you don't need to return label value here, because `asm-Get-Label` always called first
;; ( addr count type -- )
defer-not-yet asm-JR-Label-Fixup

;; this is called from instruction builder
;; it is called with `asm-PC` pointing to the address the label will be put at
;; also, `count` can be zero for anonymous fixups
;; `size` is data size: 1, 2 or 4 bytes
;; WARNING! for now, `size` is always 4, but this may change in the future
;; you don't need to return label value here, because `asm-Get-Label` always called first
;; ( addr count size type -- )
defer-not-yet asm-Label-Fixup

;; called by "CODE:" compiler after "ENDCODE"
;; ( -- )
defer-not-yet asm-Check-Undef-Labels

;; called before metacompiler is going to generate the binary
;; ( -- )
defer-not-yet asm-Check-Undef-Labels-Final


;; debug routine; may be left empty
;; ( -- )

defer asm-Dump-Labels
