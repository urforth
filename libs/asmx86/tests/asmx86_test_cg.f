;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tester
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ " asmx86.f" tload

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
decimal

: (a")  ( addr count -- )
  here >r
\ .stack
  asmx86:asm-str
\ .stack
  \ ." ********: " here r@ - . ." bytes at 0x" r@ .hex8 cr
  r>
  begin
    disasm-one cr
    dup here >=
  until
  drop
;

: a"  ( -- )
  state @ if
    [compile] " compile (a")
  else
    34 word count str-unescape (a")
  endif
; immediate


a" mov   eax,gs:[ebp+0x20]"

a" add  ecx,eax"
a" ret  nc"
a" pushad"

a" add  ecx,4"
a" add  ecx,[esi]"
a" add  ecx,[esi+4]"
a" add  ecx,[esi*8+6]"
a" add  ecx,0x12345678"

a" ld eax,#40_01"

a" mov  eax,42"
a" ld   TOS,freereg"
a" lea  ebx,[edx]"
a" pop  TOS"
a" push TOS"
a" lea  ebx,[edx*4+6]"
a" lea  ecx,[esi*4+6]"
a" lea  ecx,[esi*2-9]"
a" ld   TOS,[EIP*2-9]"
a" ld   TOS,[ERP+8]"

a" nop"
a" lodsd"
a" jmp eax"
a" nop"
a" next"


a" aad"
a" aam"
a" cwd"
a" cbw"
a" CMPSW"
a" INSW"
a" IRET"
a" LODSW"
a" MOVSW"
a" OUTSW"
a" POPAW"
a" PUSHAW"
a" POPFW"
a" PUSHFW"
a" SCASW"
a" STOSW"

a" enter 3,5"
a" bound ecx,[esi]"
a" arpl word [edi],ax"

a" retn 4"

a" das"
a" bswap eax"
a" CMPXCHG8B [edi]"
a" aam"
a" movzx ecx,byte [edi]"
a" movzx ecx,word [edi]"
a" cmp  TOS,esi"
a" cp   TOS,esi"

a" seta cl"
a" cmovz eax,edx"
a" ld nz,eax,edx"
a" ld c,eax,[edx]"
a" ld z,[esi*8+666],eax"

a" fadd st0,st(1)"

a" ld eax,cr0"
a" ld ebx,dr0"
a" ld cr2,edx"
a" ld dr7,ecx"
a" ld nz,dr3,esi"
a" ld nz,ebx,cr2"
a" ld nz,ebx,edx"

a" ld eax,test"
a" jmp label0"
a" jmp $"
a" jmp #12345678"
a" jp  nc,labelx"
a" jp  nc,short labely"

a" jr label7"
a" jr nz,@label8"
a" jr $+4"
a" jr nz,$+4"

a" cp TOS,42"
a" cp TOS,666"
a" jr z,$"

a" nop nop nop nop"
a" ld [ERP],eax"
a" ld [esp],eax"
a" ld [esp+4],eax"
a" ld eax,[esp]"
a" ld eax,[esp+4]"

a" nop"
a" nop"

a" bts   eax,0x0A"
a" bts   ebx,0x0A"
a" bts   ebx,ecx"
a" bts   word [esp-4],0x0A"
a" bts   dword [esp-4],0x0A"

a" bt    eax,0x0A"
a" bt    ebx,0x0A"
a" bt    ebx,ecx"
a" bt    word [esp-4],0x0A"
a" bt    dword [esp-4],0x0A"

a" btc   eax,0x0A"
a" btc   ebx,0x0A"
a" btc   ebx,ecx"
a" btc   word [esp-4],0x0A"
a" btc   dword [esp-4],0x0A"

a" btr   eax,0x0A"
a" btr   ebx,0x0A"
a" btr   ebx,ecx"
a" btr   word [esp-4],0x0A"
a" btr   dword [esp-4],0x0A"

a" nop"
a" nop"

a" fcom  st1"
a" fstsw ax"
a" sahf"
a" fxch  st1"
a" ffree st"
a" fincstp"

a" nop"
a" nop"

;; invalid opcodes
;; a" mov   eax,[esp*2]"
;; a" nop"
;; a" mov   eax,[esp+esp]"
;; a" nop"

a" mov   eax,[ebp*2]"
a" nop"
a" mov   eax,[ebp+ebp]"
a" nop"
a" mov   eax,[ebp+ebp+0x200000]"
a" nop"

a" mov   eax,[ebp+0x20]"
a" mov   eax,[ebp+0x200000]"
a" nop"

a" mov   eax,[ebx*2]"
a" nop"
a" mov   eax,[ebx+ebx]"

a" nop"
a" add   eax,0x12345678"
a" nop"
a" add   ebx,0x12345678"
a" nop"

a" mov   eax,gs:[ebp+0x20]"

a" cmp   esi,4*4*10"
a" cmp   esi,16000"
a" add   eax,18"
a" add   esi,18"
a" test  eax,18"
a" test  esi,18"
a" or    eax,0"
a" or    ecx,0"
a" add   eax,128"
a" add   esi,128"
a" test  eax,128"
a" test  esi,128"
a" or    eax,128"
a" or    ecx,128"
a" and   eax,7"
a" and   ecx,7"

a" push  0x037f"

a" emms"
a" ffreep st(0)"
a" ffreep st(1)"
a" ffreep st(7)"

.stack

asmx86:asm-Dump-Labels

bye
