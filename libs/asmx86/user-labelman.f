;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; label manager
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary asm-labman
also asm-labman definitions

true constant lman-use-hash-table immediate
;; hide labels, and clear buckets after word compilation is done?
false constant lman-hash-delay-remove immediate

false value lman-debug immediate
false value lman-fixup-debug immediate
false value lman-debug-forth immediate

true value lman-allow-forth-implicit

(*
  we'll BRK all the needed memory, and will never free it
  this is totally unacceptable in long-running system, but
  ok for metacompiler and for systems that will be SAVEd.

  sadly, we can't store address label links in the code itself
  (due to short jumps), so don't bother at all.

  so, we have two linked lists: label names and flags, and fixups.

  label name and info:
    cell next-in-bucket  ( or 0 )
    cell flags ( see below )
    cell value
    cell fixuphead ( or 0 )
    cell introline# ( tib-line# at the time this label was first introduced)
    c4str name ( uppercased )

  flags:
    bit 0: undefined yet
    bit 1: forward reference
    bit 2: set for forth word reference (i.e. not a code label)
    bit 3: constant
    bit 31: hidden (so it can be checked with 0<) -- NOT YET

  fixup list:
    cell next ( or 0 )
    cell addr
    cell type
    cell introline# ( generally, should be the same data structure as in nfo)

  types:
    0: direct code address (4 bytes)
    1: long displacement (4 bytes)
    2: short displacement (1 byte)

  @@ labels are done this way:
    when we see @@, we're going to resolve all @f, and remove it
    when we access @f, we're always remember it as a fixup
    when we access @b, we're adding it as @@ (so it will be automatically resolved)
*)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
5 constant nfo-size-cells  ;; w/o name
0 var (mem-allocated)

0x01 constant flag-undefined
0x02 constant flag-forward
0x04 constant flag-forth
0x08 constant flag-constant

0 constant fxtype-direct
1 constant fxtype-disp32
2 constant fxtype-disp8

;; usually, "@" is removed from label name (due to "@name" labels being "global-non-reseting")
;; this is not necessary (and basically destructive) for Forth labels
;; hence this flag
true value do-fix-label-name?


;; use 256 buckets, because it is the easiest way
lman-use-hash-table [IF]
  0 value info-hash-buckets
  lman-hash-delay-remove [IF]
    0 value info-hash-buckets-dirty
  [ENDIF]
  \ 256 cells brk-buffer: info-hash-buckets
  \ info-hash-buckets 256 cells erase
[ELSE]
  0 var info-list-head
[ENDIF]


: Reinit  ( -- )
  lman-use-hash-table [IF]
    info-hash-buckets ifnot 256 cells brk-alloc to info-hash-buckets endif
    info-hash-buckets 256 cells erase
    lman-hash-delay-remove [IF]
      info-hash-buckets-dirty ifnot 256 cells brk-alloc to info-hash-buckets-dirty endif
      info-hash-buckets-dirty 256 cells erase
    [ENDIF]
  [ELSE]
    info-list-head 0!
  [ENDIF]
  \ endcr ." LABELMAN: reinited!\n"
;

[IFDEF] forth:startup-chain-add
  ' Reinit startup-chain-add
[ENDIF]

[IFDEF] forth:(startup-init)
..: forth:(startup-init)  Reinit  ;..
[ENDIF]

: xalloc  ( size -- addr )
  dup (mem-allocated) +!
  brk-alloc
;


\ error if flag is non-zero
: ?lbl-error  ( addr count somevalue flag code -- addr count somevalue )
  swap if
    >r drop
    endcr ." <" type ." >"
    r> asmx86:asm-error
  else
    drop
  endif
;

\ error if flag is zero
: not-?lbl-error  ( addr count somevalue flag code -- addr count somevalue )
  swap not swap ?lbl-error
;

: ?lbl-error-nx  ( addr count flag code -- addr count )
  0 nrot ?lbl-error drop
;

: not-?lbl-error-nx  ( addr count flag code -- addr count )
  swap not swap ?lbl-error-nx
;


lman-debug lman-fixup-debug or lman-debug-forth or [IF]
: lbl-debug-msg  ( lbladdr lblcount msgaddr msgcount -- lbladdr lblcount )
  endcr ." LMAN: " type ."  <" 2dup type ." >" cr
;
[ENDIF]


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: nfo->nfa  ( addr -- addr )  5 +cells ;  \ to name area
: nfo->ffa  ( addr -- addr )  1 +cells ;  \ to flags area
: nfo->pfa  ( addr -- addr )  2 +cells ;  \ to value area
: nfo->xfa  ( addr -- addr )  3 +cells ;  \ to fixup ptr area
: nfo->dfa  ( addr -- addr )  4 +cells ;  \ to "first defined" area

4 constant fix-item-cells
: fix->addr  ( addr -- addr ) 1 +cells ;
: fix-addr@  ( addr -- fxaddr ) fix->addr @ ;
: fix->type  ( addr -- addr ) 2 +cells ;
: fix-type@  ( addr -- type ) fix->type @ ;
: fix->dfa   ( addr -- addr ) 3 +cells ;
: fix-dfa@   ( addr -- addr ) fix->dfa @ ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; DO NOT REORDER! some code checks for `@fb-@ >`, for example
1 constant @fb-@
2 constant @fb-b
3 constant @fb-f

;; remove "@" from global label
: is-@fb?  ( addr count -- flag )
  dup 2 = ifnot 2drop false exit endif
  drop dup c@ [char] @ = ifnot drop false exit endif
  1+ c@
  case
    [char] @ of @fb-@ endof
    [char] B of @fb-b endof
    [char] F of @fb-f endof
    otherwise drop 0
  endcase
;

: fix-@b  ( addr count -- addr count )
  ;; "@b" should resolve to the last "@@", so simply replace the name
  2dup is-@fb? @fb-b = if 2drop " @@" endif
;

: is-local?  ( addr count -- flag )
  drop c@ [char] . =
;

: nfo-is-local?  ( nfo -- flag )
  ?dup ifnot false exit endif
  dup nfo->ffa @ flag-forth and if drop false exit endif
  nfo->nfa count is-local?
;

: fix-label-name  ( addr count -- addr count )
  do-fix-label-name? ifnot exit endif
  over c@ [char] @ = if
    2dup is-@fb? ifnot
      ;; remove '@'
      1- dup asmx86:ERRID_ASM_INVALID_LABEL_NAME not-?lbl-error-nx
      swap 1+ swap
      over c@ [char] @ = asmx86:ERRID_ASM_INVALID_LABEL_NAME ?lbl-error-nx
    endif
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create DFA info and write it into the label
;; for now, we simply store the line number, but we should store
;; file name too
: create-dfa  ( -- addr )
  asmx86:lexer:first-line# ?dup ifnot tib-line# @ endif
  ?dup if
    2 cells xalloc  ;; for line # and file name
    dup >r
    swap over ! cell+
    0 over ! cell+
    drop
    r>
  else
    0
  endif
;

: dfa-@line  ( dfa -- linenum )
  ?dup if @ else 0 endif
;

: dfa-@fname  ( dfa -- addr count )
  ?dup if cell+ @ ?dup if count exit endif endif
  NullString
;

: type-dfa  ( dfa -- )
  ?dup if
    dup dfa-@line ?dup if
      ."  around line #" base @ >r decimal 0 .r r> base !
    endif
    dfa-@fname dup if
      ."  of file \`" type ." \`"
    else
      2drop
    endif
  endif
;

: type-dfa-msg-cr  ( dfa addr count -- )
  rot ?dup if
    dup @ if
      nrot endcr type type-dfa cr
    else
      drop
      2drop
    endif
  else
    2drop
  endif
;

: create-label-dfa  ( nfo -- )
  nfo->dfa create-dfa swap !
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: lman-name-hash-folded  ( addr count -- u8 )
  str-name-hash uhash32->8
;


: find-label  ( addr count -- nfo-block-addr 1 // 0 )
  fix-label-name
  lman-use-hash-table [IF]
    2dup lman-name-hash-folded info-hash-buckets cells^
  [ELSE]
    info-list-head
  [ENDIF]
  begin @ ?dup while
    >r
      \ 2dup type space r@ .hex8 space r@ nfo->nfa count type cr
    2dup r@ nfo->nfa count s=ci if 2drop r> true exit endif
  r> repeat 2drop false
;


: create-label  ( addr count -- nfo-block-addr )
  fix-label-name
  dup 1 < asmx86:ERRID_ASM_INTERNAL_ERROR asmx86:?asm-error
  dup nfo-size-cells 1+ +cells xalloc  ;; name bytes, header, count
  >r
  ;; link it
  lman-use-hash-table [IF]
    2dup lman-name-hash-folded info-hash-buckets cells^
    dup @ r@ rot !
    r@ !
  [ELSE]
    info-list-head @ r@ !
    r@ info-list-head !
  [ENDIF]
  ;; clear other fields
  r@ cell+ nfo-size-cells 1- cells erase
  ;; copy name
  r@ nfo->nfa c4s:copy-counted
  ;; upcase label name here, because why not?
  \ r@ nfo->nfa count upcase-str
  ;; create "definition info"
  r@ create-label-dfa
  ;; done, return new info address
  r>
;


: hide-label  ( nfo -- )
  ;; simply set name length to zero
  lman-use-hash-table [IF]
    ;; mark bucket dirty
    lman-hash-delay-remove [IF]
      nfo->nfa dup @ if dup count lman-name-hash-folded info-hash-buckets-dirty cells^ 1! endif 0!
    [ELSE]
      ;; remove the label from the bucket
      ;; the label is usually (almost) at the top, so this is quite fast
      dup >r nfo->nfa count lman-name-hash-folded info-hash-buckets cells^ r> over >r >r 0 swap  ( prev curr | bktaddr nfo )
      ( hide it -- just in case) r@ nfo->nfa 0!
      begin @ dup while
        dup r@ = if
          over if @ swap ! 2rdrop else rdrop @ r> ! drop endif exit
        endif
      nip dup repeat 2drop rdrop abort" label not found!"
    [ENDIF]
  [ELSE]
    nfo->nfa 0!
  [ENDIF]
;


: nfo-is-hidden?  ( nfo -- flag )
  nfo->nfa @ 0=
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (nfo-xdump-name)  ( nfo -- )
  ?dup if
    nfo->nfa cell+  ;; assume that the counter is dead
    begin dup c@ dup 32 127 within while emit 1+ repeat 2drop
  endif
;

;; remove all hidden labels from the list
: compress-labels  ( -- )
  lman-use-hash-table [IFNOT]
    info-list-head
    begin dup @ ?dup while  ( ptr-to-prev-link curr-nfo )
      dup nfo-is-hidden? if
          \ dup endcr ." removing: " (nfo-xdump-name) cr
        ;; put out next link to prev link
        2dup @ swap !
        ;; the code will move to the next one by itself
        drop
      else
        ;; drop prev, and let the code continue from the current one
        nip
      endif
    repeat drop
  [ELSE]
    lman-hash-delay-remove [IF]
      256 for  ;; for each bucket
        info-hash-buckets-dirty i +cells @ if  ;; dirty bucket
            \ endcr ." *** bucket " i . ." is dirty!\n"
          0 info-hash-buckets-dirty i +cells !
          0 info-hash-buckets i +cells
          begin @ dup while  ( prev curr )
            dup nfo-is-hidden? if
                \ endcr ." dropped label in bucket " i . cr
              over if 2dup @ swap !
              else dup @ i info-hash-buckets cells^ ! endif
            else nip dup endif
          repeat 2drop
        endif
      endfor
    [ENDIF]
  [ENDIF]
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; calls cfa: ( nfo -- ... stopflag )
;; returns when the list is done, or when cfa returned non-zero
;; the result is cfa result or zero if the list is done
;; cfa can push any number of data before stopflag if stopflag is non-zero
;; it is safe to call "hide-label" from cfa
: foreach-label  ( cfa -- ... flag )
  lman-use-hash-table [IF]
    256 for  ;; for each bucket
      info-hash-buckets i +cells
      swap >r  ;; move cfa to rstack
      begin @ dup while
        ;; skip hidden labels
        dup nfo-is-hidden? ifnot
          ;; ( nfo | cfa )
          r@ over >r execute ?dup if 2rdrop unloop exit endif
          r>
        endif
      repeat drop r>
    endfor drop
  [ELSE]
    >r info-list-head begin @ dup while
      ;; skip hidden labels
      dup nfo-is-hidden? ifnot
        ;; ( nfo | cfa )
        r@ over >r execute ?dup if 2rdrop exit endif
        r>
      endif
    repeat drop rdrop
  [ENDIF]
  0
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: dump-label  ( nfo -- )
  ;; name
  dup nfo->nfa count type ." : 0x"
  ;; value
  dup nfo->pfa @ dup .hex8 ."  (" 0 .r ." ) ["
  ;; flags
  dup nfo->ffa @
    dup flag-undefined and if ." U" endif
    dup flag-forward and if ." F" endif
    dup flag-forth and ifnot ." A" endif
    dup flag-constant and if ." C" endif
  drop ." ]" dup nfo->dfa @ type-dfa cr
  ;; dump fixups
  nfo->xfa
  begin
    @ ?dup
  while
    endcr ."   0x" dup cell+ @ .hex8
    ."  (" dup 2 +cells @ 0 .r ." )"
    dup fix-dfa@ type-dfa
    cr
  repeat
;

: dump-labels  ( -- )
  endcr ." === LABELS ===\n"
  [: ( nfo -- 0 )
    endcr dump-label
    false
  ;] foreach-label drop
  ." ---------\n"
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; note that the parser always uppercases the labels, so
;; you don't need to worry about that
;;
;; also note that '@' is a valid identifier character
;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Do-Fixups  ( addr count nfo value -- )
  over >r >r  ;; ( addr count nfo | nfo value )
  nfo->xfa
  begin
    @ ?dup
  while
    ;; ( addr count fixupitem | nfo value )
    dup fix-type@
    ;; fixup address contains an offset, so we need to add it to the value
    case
      fxtype-direct of
        ;; ( addr count fixupitem | nfo value )
        dup fix-addr@
          lman-fixup-debug [IF]
            ." FXOFS-DIRECT: " 1 rpick nfo->nfa count type
            ."  fixaddr: 0x" dup .hex8
            ."  [fixaddr]: 0x" dup asmx86:asm-d@ .hex8
            ."  value: 0x" r@ .hex8
            cr
          [ENDIF]
        ;; ( addr count fixupitem fixaddr | nfo value )
        dup asmx86:asm-d@ r@ +
        ;; ( addr count fixupitem fixaddr [fixaddr]+value | nfo value )
        swap asmx86:asm-d!
      endof
      fxtype-disp32 of
        ;; ( addr count fixupitem | nfo value )
        dup fix-addr@
          lman-fixup-debug [IF]
            ." FXOFS-DISP32: " 1 rpick nfo->nfa count type
            ."  fixaddr: 0x" dup .hex8
            ."  [fixaddr]: " dup asmx86:asm-d@ 0 .r
            ."  value: 0x" r@ .hex8
            cr
          [ENDIF]
        ;; ( addr count fixupitem fixaddr | nfo value )
        dup asmx86:asm-d@ r@ +
        ;; ( addr count fixupitem fixaddr [fixaddr]+value | nfo value )
        over 4+ -  ;; calc displacement
        swap
        ;; warn if could be short
        over -128 128 within if
          dup 1- asmx86:asm-c@ 0xe8 <> if
            ;; not call
            2>r dup fix-dfa@ " WARNING: jump range could be short" type-dfa-msg-cr
            2r>
          endif
        endif
        asmx86:asm-d!
      endof
      fxtype-disp8 of
        ;; ( addr count fixupitem | nfo value )
        dup fix-addr@
        ;; ( addr count fixupitem fxaddr | nfo value )
          lman-fixup-debug [IF]
            ." FXOFS-DISP8: " 1 rpick nfo->nfa count type
            ."  fixaddr: 0x" dup .hex8
            ."  [fixaddr]: " dup asmx86:asm-c@ c>s 0 .r
            ."  value: 0x" r@ .hex8
            cr
          [ENDIF]
        ;; ( addr count fixupitem fixaddr | nfo value )
        dup asmx86:asm-c@ c>s r@ +
        ;; ( addr count fixupitem fixaddr [fixaddr]+value | nfo value )
        ;; calc disp
        over 1+ -  ;; calc displacement
        ;; ( addr count fixupitem fixaddr disp | nfo value )
        dup -128 128 within ifnot
          2drop dup fix-dfa@ " ERROR: invalid jump range" type-dfa-msg-cr
          true asmx86:ERRID_ASM_JUMP_OUT_OF_RANGE ?lbl-error
        endif
        swap asmx86:asm-c!
      endof
      otherwise
        true asmx86:ERRID_ASM_INTERNAL_ERROR ?lbl-error
    endcase
  repeat
  ;; ( addr count | nfo value )
  2drop rdrop
  ;; clear fixup list
  r@ nfo->xfa 0!
  ;; reset undefined and forward flags
  flag-undefined flag-forward or r> nfo->ffa ~and!
;


: Add-Fixup  ( nfo addr type -- )
  rot >r  ;; ( addr type | nfo )
  ;; allocate new fixup record
  fix-item-cells cells xalloc
  ;; ( addr type fxit | nfo )
  ;; setup link
  r@ nfo->xfa @ over !
  dup r> nfo->xfa !
  ;; setup type
  dup fix->type rot swap !
  ;; create definition info
  create-dfa over fix->dfa !
  ;; setup address
  fix->addr !
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is called by main driver
;; you can throw error here
: MakeConstant  ( addr count value -- )
  >r
  lman-debug [IF]
    " MakeConstant" lbl-debug-msg
  [ENDIF]

  ;; no "@@", "@f", "@b"
  2dup is-@fb? asmx86:ERRID_ASM_INVALID_LABEL_NAME ?lbl-error-nx

  2dup find-label asmx86:ERRID_ASM_DUPLICATE_CONSTANT ?lbl-error
  create-label
  ;; set value
  dup nfo->pfa r> swap !
  ;; set flags
  dup nfo->ffa flag-constant swap !
  ;; done
  drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Check-Locals-Resolved  ( -- )
  false
  [: ( errflag nfo -- errflag 0 )
    dup nfo-is-local? if
      dup nfo->xfa @ if
        \ nfo->nfa count true asmx86:ERRID_ASM_UNRESOLVED_LABEL ?lbl-error-nx
        over ifnot endcr ." unresolved local label(s):\n" endif
        dup nfo->nfa count 2 spaces type
        dup nfo->dfa @ type-dfa cr
        nip true swap
      endif
    endif
    drop
    false
  ;] foreach-label drop
  asmx86:ERRID_ASM_UNRESOLVED_LABEL asmx86:?asm-error
;

: Clear-Locals  ( -- )
  [: ( nfo -- 0 )
    dup nfo-is-local? if
      dup hide-label
    endif
    drop
    false
  ;] foreach-label drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Check-@@-Resolved  ( -- )
  [: ( nfo -- 0 )
    dup nfo->nfa count is-@fb? if
      dup nfo->xfa @ if
        nfo->nfa count true asmx86:ERRID_ASM_UNRESOLVED_LABEL ?lbl-error-nx
      endif
    endif
    drop
    false
  ;] foreach-label drop
;

: Clear-@@  ( -- )
  [: ( nfo -- 0 )
    dup nfo->nfa count is-@fb? if
      dup hide-label
    endif
    drop
    false
  ;] foreach-label drop
;


;; resolve "@f" to asm-PC, remove any existing "@@" label
: Resolve-@f-And-Remove-@@  ( -- )
  [: ( nfo -- 0 )
    dup nfo->nfa count is-@fb?
    case
      @fb-f of
        ;; resolve and remove
        dup " @f" rot asmx86:asm-PC Do-Fixups
        dup hide-label
      endof
      @fb-@ of
        ;; simply remove (code cannot access "@@" directly, so no need to resolve it)
        dup hide-label
      endof
    endcase
    drop
    false
  ;] foreach-label drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Clear-Unresolved  ( -- )
  [: ( nfo -- 0 )
    ;; do not clear forwards
    dup nfo->ffa @  flag-undefined flag-forward or and  flag-undefined =
    if
      dup hide-label
    endif
    drop
    false
  ;] foreach-label drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Check-Unresolved  ( -- )
  false
  [: ( errflag nfo -- errflag 0 )
    ;; do not report forwards
    dup nfo->ffa @  flag-undefined flag-forward or and  flag-undefined =
    if
      \ nfo->nfa count true asmx86:ERRID_ASM_UNRESOLVED_LABEL ?lbl-error-nx
      over ifnot endcr ." unresolved label(s):\n" endif
      dup nfo->nfa count 2 spaces type
      dup nfo->dfa @ type-dfa cr
      nip true swap
    endif
    drop
    false
  ;] foreach-label drop
  asmx86:ERRID_ASM_UNRESOLVED_LABEL asmx86:?asm-error
;


: Check-Unresolved-Forwards  ( -- )
  false
  [: ( errflag nfo -- errflag 0 )
    ;; do not report forwards
    dup nfo->ffa @  flag-undefined flag-forward or and  flag-undefined flag-forward or =
    if
      \ nfo->nfa count true asmx86:ERRID_ASM_UNRESOLVED_LABEL ?lbl-error-nx
      over ifnot endcr ." unresolved forward(s):\n" endif
      dup nfo->nfa count 2 spaces type
      dup nfo->dfa @ type-dfa cr
      nip true swap
    endif
    drop
    false
  ;] foreach-label drop
  asmx86:ERRID_ASM_UNRESOLVED_LABEL asmx86:?asm-error
;

: Count-Live-Labels  ( -- n )
  0 [: ( count nfo -- count 0 )
    drop 1+
    false
  ;] foreach-label drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is called by main driver
;; you can throw error here
: MakeLabel  ( addr count value -- )
  >r
  lman-debug [IF]
    " MakeLabel" lbl-debug-msg
  [ENDIF]

  ;; check for special labels
  over c@ [char] @ = if
    ;; no "@f", "@b"
    2dup is-@fb? @fb-@ > asmx86:ERRID_ASM_INVALID_LABEL_NAME ?lbl-error-nx
    ;; resolve "@@"
    2dup is-@fb? @fb-@ = if Resolve-@f-And-Remove-@@ endif
  else
    ;; non-local label should clear all locals
    2dup is-local? ifnot
      Check-Locals-Resolved
      Clear-Locals
    endif
  endif

  2dup find-label if
    ;; existing label, check type
    dup nfo->ffa @ flag-undefined flag-forward or bitnot and asmx86:ERRID_ASM_DUPLICATE_LABEL ?lbl-error
    dup nfo->ffa @ flag-undefined and asmx86:ERRID_ASM_DUPLICATE_LABEL not-?lbl-error
    ;; fix addresses
    ;; ( addr count nfo | value )
    ;; set new value
    r@ over nfo->pfa !
      ;; endcr dup dump-label
    r> Do-Fixups
  else
    ;; new label
    create-label
    ;; set value
    dup nfo->pfa r> swap !
    ;; done
    drop
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EXTERNAL API
: MakeForthLabel  ( addr count value forward -- )
  do-fix-label-name? >r 0 to do-fix-label-name?
  >r >r  ;; ( addr count | forward value )
  dup asmx86:ERRID_ASM_INVALID_LABEL_NAME not-?lbl-error-nx
  2dup find-label if
    ;; existing label, check type
    dup nfo->ffa @ flag-undefined and asmx86:ERRID_ASM_DUPLICATE_LABEL not-?lbl-error
    dup nfo->ffa @ flag-forth and asmx86:ERRID_ASM_DUPLICATE_LABEL not-?lbl-error
    nrot 2drop  ;; drop label name, we don't need it anymore
  else
    ;; new label
    create-label
    ;; mark it as Forth label
    dup nfo->ffa flag-forth swap or!
  endif
  ;; ( nfo | forward value )
  ;; set value
  dup nfo->pfa r> swap !
  ;; ( nfo | forward )
  ;; set type
  r> ifnot
    ;; not forward
    dup nfo->ffa flag-undefined flag-forward or swap ~and!
    dup nfo->nfa count rot
    ;; ( addr count nfo )
    dup nfo->pfa @ Do-Fixups
  else
    dup nfo->ffa flag-undefined flag-forward or swap or!
    drop
  endif
  r> to do-fix-label-name?
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EXTERNAL API
;; "-1" means that the label is not defined yet, and the value is a random prediction
: GetForthLabel  ( addr count type -- value 1 // value -1 // false )
  >r wfind if
    r> case
      asmx86:LABEL-TYPE-CFA of forth:noop endof
      asmx86:LABEL-TYPE-PFA of forth:cfa->pfa endof
      \ asmx86:LABEL-TYPE-NFA of forth:cfa->nfa endof
      \ asmx86:LABEL-TYPE-LFA of forth:cfa->nfa forth:nfa->lfa endof
      \ asmx86:LABEL-TYPE-BFA of forth:cfa->nfa forth:nfa->bfa endof
      \ asmx86:LABEL-TYPE-SFA of forth:cfa->nfa forth:nfa->sfa endof
      \ asmx86:LABEL-TYPE-DFA of forth:cfa->nfa forth:nfa->dfa endof
      \ asmx86:LABEL-TYPE-HFA of forth:cfa->nfa forth:nfa->hfa endof
      asmx86:ERRID_ASM_INTERNAL_ERROR asmx86:asm-error
    endcase
    1
  else
    rdrop ;; 2drop
    false
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EXTERNAL API
: GetConstant  ( addr count -- value true // false )
  lman-debug [IF]
    " GetConstant(00)" lbl-debug-msg
  [ENDIF]
  ;; no "@@", "@f", "@b"
  2dup is-@fb? if 2drop false exit endif
  lman-debug [IF]
    " GetConstant(01)" lbl-debug-msg
  [ENDIF]
  ;; check label type
  2dup find-label ifnot 2drop false exit endif
  dup nfo->ffa @ flag-constant and if
    lman-debug [IF]
      >r
      endcr ." GetConstant: <" 2dup type ." :" r@ nfo->pfa @ 0 .r ." >" cr
      r>
    [ENDIF]
    nfo->pfa @
    nrot 2drop
    true
  else
    drop 2drop false
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EXTERNAL API
;; "-1" means that the label is not defined yet, and the value is a random prediction
: GetLabel  ( addr count -- value 1 // value -1 // false )
  lman-debug [IF]
    " GetLabel(00)" lbl-debug-msg
  [ENDIF]
  ;; no "@@"
  2dup is-@fb? @fb-@ = asmx86:ERRID_ASM_INVALID_LABEL_NAME ?lbl-error-nx
  ;; "@b" should resolve to the last "@@", so simply replace the name
  fix-@b
  lman-debug [IF]
    " GetLabel(01)" lbl-debug-msg
  [ENDIF]
  ;; check if such label exists
  2dup find-label ifnot
    ;; "@b" without "@@" should not be allowed
    2dup is-@fb? @fb-b = asmx86:ERRID_ASM_INVALID_BACKWARD_REF ?lbl-error-nx
    ;; create new label
    create-label
    ;; set flags
    dup nfo->ffa flag-undefined
      asmx86:asm-Labman-Unresolved-As-Forwards? if flag-forward or endif
    swap !
    ;; predict value (our predictor is highly sophisticated!)
    asmx86:asm-$ dup rot nfo->pfa !
    -1  ;; random predition
  else
    ;; check label type
    dup nfo->ffa @ flag-constant and asmx86:ERRID_ASM_TYPE_MISMATCH ?lbl-error
    ;; drop label name
    >r 2drop r>
    ;; return value
    dup nfo->pfa @
    ;; is it undefined?
    swap nfo->ffa @ flag-undefined and if -1 else 1 endif
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EXTERNAL API
: LabelDefined?  ( addr count -- flag )
  lman-debug [IF]
    " LabelDefined?(00)" lbl-debug-msg
  [ENDIF]
  ;; "@f" is never defined
  2dup is-@fb? @fb-f = if 2drop false exit endif
  ;; "@b" is defined if "@@" is defined, so simply replace the name
  fix-@b
  lman-debug [IF]
    " LabelDefined?(01)" lbl-debug-msg
  [ENDIF]
  ;; check if such label exists
  find-label ifnot false exit endif
  ;; check flags
  nfo->ffa @ flag-undefined and not
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: JxxFixupLabel  ( addr count disp32flag -- )
  >r  ;; move that flag away, we'll use it later
  ;; "@b" should resolve to the last "@@", so simply replace the name
  fix-@b
  lman-fixup-debug [IF]
    " JxxFixupLabel" lbl-debug-msg
  [ENDIF]
  ;; the label must be already created here
  \ 2dup find-label asmx86:ERRID_ASM_UNRESOLVED_LABEL not-?lbl-error-nx
  2dup find-label ifnot
    lman-allow-forth-implicit if
      2dup wfind asmx86:ERRID_ASM_UNRESOLVED_LABEL not-?lbl-error-nx
      ;; drop all crap
      rdrop drop 2drop
    else false asmx86:ERRID_ASM_UNRESOLVED_LABEL not-?lbl-error-nx
    endif
  else
    ;; and it must be a code label
    dup nfo->ffa @ flag-constant and asmx86:ERRID_ASM_TYPE_MISMATCH ?lbl-error-nx
    ;; check if we have to add the asm-PC to the fixup list
    dup nfo->ffa @ flag-undefined and if
      ;; yeah, append it
      dup asmx86:asm-PC r@ if fxtype-disp32 else fxtype-disp8 endif Add-Fixup
    endif
    ;; drop all crap
    rdrop drop 2drop
  endif
;


;; EXTERNAL API
: FixupJmpLabel  ( addr count type -- )
  ;; ignore label type (for now)
  ;; label creation code should make sure that the Forth fixup add (in memory) is right for CFA/PFA/ETC
  drop
  lman-fixup-debug [IF]
    " FixupJmpLabel" lbl-debug-msg
  [ENDIF]
  true JxxFixupLabel
;


;; EXTERNAL API
: FixupJRLabel  ( addr count type -- )
  ;; ignore label type (for now)
  ;; label creation code should make sure that the Forth fixup add (in memory) is right for CFA/PFA/ETC
  drop
  lman-fixup-debug [IF]
    " FixupJRLabel" lbl-debug-msg
  [ENDIF]
  false JxxFixupLabel
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EXTERNAL API
: LabelFixup  ( addr count size type -- )
  ;; ignore label type (for now)
  ;; label creation code should make sure that the Forth fixup add (in memory) is right for CFA/PFA/ETC
  drop
  4 = asmx86:ERRID_ASM_INVALID_FORWARD_REF asmx86:not-?asm-error
  lman-fixup-debug [IF]
    " LabelFixup" lbl-debug-msg
  [ENDIF]
  ;; the label must be already created here
  2dup find-label
  lman-allow-forth-implicit if
    ifnot
      2dup wfind if drop 2drop exit endif
      false asmx86:ERRID_ASM_UNRESOLVED_LABEL not-?lbl-error-nx
    endif
  else
    asmx86:ERRID_ASM_UNRESOLVED_LABEL not-?lbl-error-nx
  endif
  ;; and it must not be a constant
  dup nfo->ffa @ flag-constant and asmx86:ERRID_ASM_TYPE_MISMATCH ?lbl-error-nx
  ;; check if we have to add the asm-PC to the fixup list
  dup nfo->ffa @ flag-undefined and if
    ;; yeah, append it
    lman-fixup-debug [IF]
      ." LabelFixup NEW: " dup nfo->nfa count type
      ."  fixaddr: 0x" asmx86:asm-PC .hex8
      ."  [fixaddr]: 0x" asmx86:asm-PC asmx86:asm-d@ .hex8
      cr
    [ENDIF]
    dup asmx86:asm-PC fxtype-direct Add-Fixup
  endif
  ;; and drop all crap
  drop 2drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called by "CODE:" compiler
: CheckUndefLabels  ( -- )
  lman-debug [IF]
    dump-labels
  [ENDIF]
  Check-Locals-Resolved
  Clear-Locals
  Check-@@-Resolved
  Clear-@@
  Check-Unresolved
  ;; cheat for speed
  compress-labels
  \ Count-Live-Labels ifnot Reinit endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; used by metacompiler
;; called before metacompiler is going to generate the binary
: FinalCheckUndefLabels  ( -- )
  CheckUndefLabels
  Check-Unresolved-Forwards
  ;; cheat for speed
  compress-labels
  \ Count-Live-Labels ifnot Reinit endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called by `asm-error`
: ErrorReset  ( -- )
  Clear-Locals
  Clear-@@
  Clear-Unresolved
  ;; cheat for speed
  compress-labels
  \ Count-Live-Labels ifnot Reinit endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Reinit


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous definitions

;; plug in our label manager

' asm-labman:ErrorReset to asmx86:asm-Labman-Error-Reset
' asm-labman:Reinit to asmx86:asm-Labman-Reinit
' asm-labman:MakeConstant to asmx86:asm-Make-Constant
' asm-labman:MakeLabel to asmx86:asm-Make-Label
' asm-labman:MakeForthLabel to asmx86:asm-Make-Forth-Label
' asm-labman:GetForthLabel to asmx86:asm-Get-Forth-Word
' asm-labman:GetLabel to asmx86:asm-Get-Label
' asm-labman:GetConstant to asmx86:asm-Get-Constant
' asm-labman:LabelDefined? to asmx86:asm-Label-Defined?
' asm-labman:FixupJmpLabel to asmx86:asm-Jmp-Label-Fixup
' asm-labman:FixupJRLabel to asmx86:asm-JR-Label-Fixup
' asm-labman:LabelFixup to asmx86:asm-Label-Fixup
' asm-labman:CheckUndefLabels to asmx86:asm-Check-Undef-Labels
' asm-labman:FinalCheckUndefLabels to asmx86:asm-Check-Undef-Labels-Final
' asm-labman:dump-labels to asmx86:asm-Dump-Labels
