;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; assembler pseudoinstructions and macrocommands
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this strange way to avoid defining in asmx86:instructions, because
;; that vocabulary redefines things like AND, OR, etc.
;; main code will use "Register-Macros-From-VocId" to register macro commands

nested-vocabulary macro-instrs
also macro-instrs definitions
<public-words>

" xcommands.f" tload
" condcomp.f" tload
" defx.f" tload

previous definitions
<hidden-words>
