;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; assembler pseudoinstructions and macrocommands
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; insert bytes to make asm-PC aligned
;; ALIGN size[,fillbyte]
;; default fill byte is zero
: ALIGN  ( -- )
  Reset-Instruction
  4 to *OpSize
  Imm
  *Imm 1 257 within ERRID_ASM_INVALID_ALIGN not-?asm-error
  \ 4 to *ImSize
  *OpReloc ERRID_ASM_INVALID_ALIGN ?asm-error
  ;; align
  *Imm
  ;; filler (optional second arg)
  ?Comma if
    Reset-Instruction
    4 to *OpSize
    Imm
    *Imm 0 255 within ERRID_ASM_INVALID_ALIGN not-?asm-error
    *OpReloc ERRID_ASM_INVALID_ALIGN ?asm-error
    *Imm
  else
    0
  endif
  swap
  ;; ( filler align )
  begin
    asm-PC over mod
  while
    over asm-c,
  repeat
  2drop
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SYSCALL  ( -- )
  \ int 0x80
  $80CD asm-w,
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: NEXT  ( -- )
  \ " lodsd" asm-str
  $AD asm-c,
  \ either "jmp eax" of "jmp dword [nextref]"
  \ old: either "jmp eax" of "jmp dword [nextref]"
  \ new: either "jmp eax" of "jmp tc-urforth-next-ptr"
  asm-instrumented-next-addr if
    \ $FF asm-c,
    \ $25 asm-c, asm-instrumented-next-addr asm-,
    $E9 asm-c, asm-instrumented-next-addr asm-pc 4+ - asm-,
  else
    $FF asm-c,
    $E0 asm-c,
  endif
;

alias NEXT URNEXT


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; c4str
1028 brk-buffer: (macro-buf)  (hidden)

: (init-macro-buf)  ( -- )
  (macro-buf) 0!
; (hidden)

: (c>macro-buf)  ( ch -- )
  (macro-buf) count dup 1020 < ERRID_MACRO_BUFFER_OVERFLOW not-?asm-error
  + c!
  (macro-buf) 1+!
; (hidden)

: (s>macro-buf)  ( addr count -- )
  dup 0> if
    over + swap do i c@ (c>macro-buf) loop
  else
    2drop
  endif
; (hidden)

: (u>macro-buf)  ( u32 -- )
  base @ >r hex 0 <# 8 0 do # loop [char] $ hold #> r> base ! (s>macro-buf)
; (hidden)

: (load-tib-line-to-macro-buf)  ( -- )
  \ lexer:Token->Temp-Str (s>macro-buf)
  lexer:tk-start->in >in !
  bl (c>macro-buf) \ just in case
  begin
    tib-peekch
  while
    tib-getch
    dup 10 = over 13 = or if
      drop break
    endif
    (c>macro-buf)
  repeat
  \ current token should be EOL
  lexer:PrepareLineParser
  lexer:NextToken
; (hidden)

: (asm-macro-buf)  ( -- )
    \ endcr ." <<" (macro-buf) count type ." |\n"
  (macro-buf) count asm-str
; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: PUSHR  ( -- )
  ;; generate code
  " sub ERP,4" asm-str
  ;; prepare macro load
  (init-macro-buf)
  " ld [ERP]," (s>macro-buf)
  (load-tib-line-to-macro-buf)
  ;; generate code
  (asm-macro-buf)
;

: POPR  ( -- )
  ;; prepare macro load
  (init-macro-buf)
  " ld " (s>macro-buf)
  (load-tib-line-to-macro-buf)
  " ,[ERP]" (s>macro-buf)
  ;; generate code
  (asm-macro-buf)
  " add ERP,4" asm-str
;

: PEEKR  ( -- )
  ;; prepare macro load
  (init-macro-buf)
  " ld " (s>macro-buf)
  (load-tib-line-to-macro-buf)
  " ,[ERP]" (s>macro-buf)
  ;; generate code
  (asm-macro-buf)
;

: DROPR  ( -- )
  ;; generate code
  " add ERP,4" asm-str
;
