;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiling support
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary-nohash asm-meta
also asm-meta definitions

;; used to get current assembling address
;; should return virtual address
\ : PC  ( -- addr )  forth:here ;

;; size is never zero or negative
;; returns alloted addres (it will be used with `(asm-c!)`
;; should return virtual address
\ : n-allot  ( size -- addr )  forth:n-allot ;

;; the only writing primitive
;; always writing to alloted address
;; accepts virtual address
: c!  ( byte addr -- )
  dup forth:here u< if
    forth:c!
  else
    asmx86:ERRID_ASM_INTERNAL_META_ERROR asmx86:asm-error
  endif
;

;; the only reading primitive
;; always reading from alloted address
;; accepts virtual address
: c@  ( addr -- byte )
  dup forth:here u< if
    forth:c@
  else
    asmx86:ERRID_ASM_INTERNAL_META_ERROR asmx86:asm-error
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous definitions

;; plug in our memory r/w module

\ ' asm-meta:PC to asmx86:asm-PC
\ ' asm-meta:n-allot to asmx86:asm-n-allot

' forth:here to asmx86:asm-PC
' forth:n-allot to asmx86:asm-n-allot
' asm-meta:c@ to asmx86:asm-c@
' asm-meta:c! to asmx86:asm-c!
