;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define FPU instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (FPU-MemReg-Fin)  ( flag -- )
  dup if drop *Mod 3 <> endif ERRID_ASM_INVALID_OPERAND not-?asm-error
;

: (FPU-MemReg)  ( -- )
  ?MemReg (FPU-MemReg-Fin)
;

: (FPU-XMemReg)  ( -- )
  ?XMemReg (FPU-MemReg-Fin)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: F2Op  ( name ( c1esc c1reg ... c4esc c4reg -- )
               ( -- )
  create 4 for swap $D8 + c, c, endfor
 does>
  to *OpArray
  Reset-Instruction
  1 to *OpcSize
  ?STReg if
    3 to *Mod
    ExpectComma
    ?dup if
      to *R/M
      ?STReg dup if drop 0= endif ERRID_ASM_INVALID_OPERAND not-?asm-error
      *OpArray
    else
      ?STReg ERRID_ASM_INVALID_OPERAND not-?asm-error
      to *R/M
      *OpArray 2+
    endif
  else
    tk-eol? if
      ;; FADD (and such) w/o operands; convert to (FOP)P ST1
      1 to *R/M
      3 to *Mod
      1 to *OpcSize
      $DE to *OpCode
      *OpArray 1+ c@ to *Reg
      0 to *OpSize
      Build-Instruction
      exit
    else
      (FPU-XMemReg)
      *OpSize
      case
        0 of ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error endof
        4 of *OpArray 6 + endof
        8 of *OpArray 4+ endof
        ERRID_ASM_TYPE_MISMATCH asm-error
      endcase
    endif
  endif
  dup c@ to *OpCode
  1+ c@ to *Reg
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: F1Op  ( name ( c1esc c1reg ... c4esc c4reg -- )
               ( -- )
  create 4 0 do swap $D8 + c, c, loop
 does>
   to *OpArray
   Reset-Instruction
   1 to *OpcSize
   ?STReg if
     to *R/M
     3 to *Mod
     *OpArray
   else
    tk-eol? if
      ;; FCOM (and such) w/o operands; convert to FOP ST1
      1 to *R/M
      3 to *Mod
      *OpArray
    else
     (FPU-XMemReg)
     *OpSize
     case
       0 of ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error endof
       4 of *OpArray 6 + endof
       8 of *OpArray 4+ endof
       10 of *OpArray 2+ endof
       ERRID_ASM_TYPE_MISMATCH asm-error
     endcase
   endif
  endif
  dup c@ dup $D7 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  to *OpCode
  1+ c@ to *Reg
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FIxx  ( name ( c1esc c1reg ... c3esc c3reg -- )
               ( -- )
  create 3 0 do swap $D8 + c, c, loop
 does>
  to *OpArray
  Reset-Instruction
  1 to *OpcSize
  (FPU-XMemReg)
  *OpSize
  case
    0 of ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error endof
    2 of *OpArray 4+ endof
    4 of *OpArray 2+ endof
    8 of *OpArray endof
    ERRID_ASM_TYPE_MISMATCH asm-error
  endcase
  dup c@ dup $D7 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  to *OpCode
  1+ c@ to *Reg
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: F0Op  ( name ( esc reg r/m -- )
               ( -- )
  create rot $D8 + c, swap c, c,
 does>
  Reset-Instruction
  1 to *OpcSize
  dup c@ to *OpCode
  1+ dup c@ to *Reg
  1+ c@ to *R/M
  3 to *Mod
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FW0Op  ( name ( esc reg r/m -- )
                ( -- )
  create rot $D8 + c, swap c, c,
 does>
  Reset-Instruction
  1 to *OpcSize
  dup c@ to *OpCode
  1+ dup c@ to *Reg
  1+ c@ to *R/M
  3 to *Mod
  $9B asm-c,
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FCtl  ( name ( wait size esc reg -- )
               ( -- )
  create swap $D8 + c, c, c, c,
 does>
   to *OpArray
   Reset-Instruction
   1 to *OpcSize
   (FPU-MemReg)
   *OpSize dup 0= swap 2 = or ERRID_ASM_TYPE_MISMATCH not-?asm-error
   *OpArray c@ to *OpCode
   *OpArray 1+ c@ to *Reg
   *OpArray 3 + c@ ?dup if asm-c, endif
   *OpArray 2+ c@ to *OpSize
   Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FBxx  ( name ( reg -- )
               ( -- )
  create c,
 does>
  Reset-Instruction
  1 to *OpcSize
  (FPU-XMemReg)
  *OpSize 10 = ERRID_ASM_TYPE_MISMATCH not-?asm-error
  c@ to *Reg
  $DF to *OpCode
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FRegOp  ( name ( esc reg -- )
                 ( -- )
  create swap $D8 + c, c,
 does>
  Reset-Instruction
  1 to *OpcSize
  ?STReg ifnot
    ;; default is st(1)
    tk-eol? ERRID_ASM_INVALID_OPERAND not-?asm-error
    1
  endif
  to *R/M
  3 to *Mod
  dup c@ to *OpCode
  1+ c@ to *Reg
  ;; optional st(0) operand
  [char] , ?CheckDelim if
    ?STReg ERRID_ASM_INVALID_OPERAND not-?asm-error
    ERRID_ASM_INVALID_OPERAND ?asm-error
  endif
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FP6Op  ( name ( esc reg -- )
                ( -- )
  create swap $D8 + c, c,
 does>
  Reset-Instruction
  1 to *OpcSize
  ?STReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ERRID_ASM_INVALID_OPERAND ?asm-error
  ExpectComma
  ?STReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  to *R/M
  3 to *Mod
  dup c@ to *OpCode
  1+ c@ to *Reg
  Build-Instruction
;
