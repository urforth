\ ANEW --Sha-1--                                \  Wil Baden  1999-08-19

\  *********************************************************************
\  *                                                                   *
\  *      SHA-1 Secure Hash Algorithm                                  *
\  *                                                                   *
\  *  SHA-INIT            ( -- )                                       *
\  *     Initialize the secure hash algorithm.                         *
\  *                                                                   *
\  *  SHA-UPDATE          ( str len -- )                               *
\  *     Update the algorithm for each text string.                    *
\  *                                                                   *
\  *  SHA-FINAL           ( -- )                                       *
\  *     Complete the algorithm.                                       *
\  *                                                                   *
\  *  .SHA                ( -- )                                       *
\  *     Display the message digest.                                   *
\  *                                                                   *
\  *  WARNING: Bit streams are not implemented.                        *
\  *                                                                   *
\  *********************************************************************

\  Although the complete message may be up to 2^64 bits long,
\  the length argument must be less than 2^32 bytes at a time.
\  This is not a problem yet (1999).

\  *********************************************************************
\  *            Secure Hash Algorithm                                  *
\  *********************************************************************

VOCABULARY SHA1
ALSO SHA1 DEFINITIONS  \  Optional 3 of 5.

<hidden-words>

: THIRD  ( x y z -- x y z x )  2 PICK ;
: 3dup   ( x y z -- x y z x y z )  THIRD THIRD THIRD ;

: Flip-Endian       ( 01020304 -- 04030201 )
    dup 24 LROTATE 0xFF00FF00 AND
    SWAP 8 LROTATE 0x00FF00FF AND OR ;

: HTYPE             ( addr len -- )
    BASE @ >R HEX  0 ?DO                           ( addr)
        dup I [ BASE C@ ] [IF]  3 XOR  [THEN] + C@
        0 <# # # #> TYPE
        I 1+ 4 MOD 0= IF SPACE THEN
    LOOP DROP  R> BASE ! ;

\  Message-Digest      ( -- addr )
\     5 cell contents is computed as the secure hash.

\  Message-Block       ( -- addr )
\     16 cell buffer for intermediate calculation.

\  SIZE                ( -- addr )
\     Double-number variable for the intermediate size in bits of the
\     message.

\  Final-Count         ( -- addr )
\     Double-number variable for the final size in bits of the message.

\  Single-Byte         ( -- addr )
\     Used when padding the message to a multiple of 512 bits.

CREATE Message-Digest   5 CELLS ALLOT  create;

CREATE Message-Block   16 CELLS ALLOT  create;

\ 2VARIABLE SIZE
CREATE SIZE 2 CELLS ALLOT  create;

\ 2VARIABLE Final-Count
CREATE Final-Count 2 CELLS ALLOT  create;

CREATE Single-Byte      0 C,  create;

\  `BLK0` and `BLK` treat the 16 cells of `Message-Block` as though
\  they were the 80 cell expansion.  Used in `TRANSFORM`.

\  BLK0                ( i -- x )
\     Convert the first 16 cells of `Message-Block` to `Work-Block`.

\  BLK                 ( i -- x )
\     Convert the remaining cells of `Message-Block` to `Work-Block`.

BASE C@ [IF]              \  Little Endian
: BLK0              ( i -- x )
  CELLS Message-Block + dup >R @ Flip-Endian dup R> ! ;
[ELSE]                    \  Big Endian
: BLK0              ( i -- x )
  CELLS Message-Block + @ ;
[THEN]

: BLK               ( i -- x )
    dup  13 + 15 AND CELLS Message-Block + @
    over  8 + 15 AND CELLS Message-Block + @  XOR
    over  2 + 15 AND CELLS Message-Block + @  XOR
    over      15 AND CELLS Message-Block + @  XOR
    1 LROTATE  \  This operation was added for SHA-1.
    dup ROT  15 AND CELLS Message-Block + ! ;

\  `F  G  H`
\     The nonlinear functions for scrambling the data.  The names are
\     taken from A. J. Menezes, _Handbook of Applied Cryptography_,
\     ISBN 0-8493-8523-7.  Used in `TRANSFORM`.

\ MIX
\     The unchanging part of the scrambling.  Used in `TRANSFORM`.

: F                 ( d c b -- bc or b'd )
    dup >R AND SWAP R> INVERT AND OR ;

: G                 ( d c b -- bc or bd or cd )
    2dup AND >R  OR AND R>  OR ;

: H                 ( d c b -- d xor c xor b )
    XOR XOR ;

: MIX               ( e d c b temp a m -- e d c b a )
    \  temp = temp + (m + (a <<< 5)) + e
    SWAP dup >R                 ( e d c b temp m a)( R: a)
    5 LROTATE + +               ( e d c b temp)    ( R: a)
    SWAP >R  SWAP >R  SWAP >R   ( e temp)    ( R: a b c d)
    +                           ( temp)      ( R: a b c d)
    \  e = d
       R> SWAP                  ( e temp)      ( R: a b c)
    \  d = c
       R> SWAP                  ( e d temp)      ( R: a b)
    \  c = (b <<< 30)
       R> 30 LROTATE            ( e d temp c)      ( R: a)
       SWAP                     ( e d c temp)      ( R: a)
    \  b = a
       R>                       ( e d c temp b)     ( R: )
    \  a = temp
       SWAP                     ( e d c b a)
    ;

\  Fetch-Message-Digest   ( -- e d c b a )
\     Fetch the values from Message-Digest. Used in `TRANSFORM`.

\  Add-to-Message-Digest  ( e d c b a -- )
\     Accumulate into Message-Digest.  Used in `TRANSFORM`.

\  TRANSFORM              ( -- )
\     Hash the 512 bits of `Message-Block` into the cells of
\     `Message-Digest`.  Does 80 rounds of complicated processing for
\     each 512 bits.  Used in `SHA-UPDATE`.

    : Fetch-Message-Digest   ( -- e d c b a )
        4 CELLS Message-Digest +       ( addr)
            dup @ SWAP CELL-       ( e addr)
            dup @ SWAP CELL-       ( e d addr)
            dup @ SWAP CELL-       ( e d c addr)
            dup @ SWAP CELL-       ( e d c b addr)
                @ ;                    ( e d c b a)

    : Add-to-Message-Digest  ( e d c b a -- )
        Message-Digest                 ( e d c b a addr)
            TUCK +! CELL+              ( e d c b addr)
            TUCK +! CELL+              ( e d c addr)
            TUCK +! CELL+              ( e d addr)
            TUCK +! CELL+              ( e addr)
                 +! ;                  ( )

: TRANSFORM         ( -- )
    Fetch-Message-Digest    ( e d c b a)

    \  Do 80 Rounds of Complicated Processing.
    16  0 DO  >R  3dup F  0x5A827999 +  R>  I BLK0  MIX  LOOP
    20 16 DO  >R  3dup F  0x5A827999 +  R>  I BLK   MIX  LOOP
    40 20 DO  >R  3dup H  0x6ED9EBA1 +  R>  I BLK   MIX  LOOP
    60 40 DO  >R  3dup G  0x8F1BBCDC +  R>  I BLK   MIX  LOOP
    80 60 DO  >R  3dup H  0xCA62C1D6 +  R>  I BLK   MIX  LOOP

    Add-to-Message-Digest ;

\  **********  SHA-INIT   SHA-UPDATE   SHA-FINAL   .SHA  **********
<public-words>

: Sha-Init          ( -- )
    \  Initialize Message-Digest with starting constants.
    Message-Digest
        0x67452301 over !  CELL+
        0xEFCDAB89 over !  CELL+
        0x98BADCFE over !  CELL+
        0x10325476 over !  CELL+
        0xC3D2E1F0 over !  DROP
    \  Zero bit count.
    0 0 SIZE 2! ;

: Sha-Update        ( str len -- )
    \  Transform 512-bit blocks of message.
    BEGIN        \  Transform Message-Block?
        SIZE CELL+ @  511 AND  3 RSHIFT  >R  64 R@ -  over U> NOT
    WHILE        \  Store some of str&len, and transform.
        2dup  64 R@ -  /STRING  dup >R  2SWAP  R> -
            Message-Block R@ + SWAP MOVE
        TRANSFORM
        SIZE 2@  64 R> -  3 LSHIFT  M+  SIZE 2!
    REPEAT
    \  Save final fraction of input.
    Message-Block   R> +  SWAP  dup >R  MOVE  ( )
    SIZE 2@  R> 0  D2* D2* D2* D+  SIZE 2! ;

: Sha-Final         ( -- )
    \  Save SIZE for final padding.
    SIZE 2@
    [ BASE C@ ] [IF]       \  Little-endian to big-endian.
        Flip-Endian  SWAP  Flip-Endian  SWAP
    [THEN]
    Final-Count 2!

    \  Pad so SIZE is 64 bits less than a multiple of 512.
    Single-Byte  0x80  over C!  1 Sha-Update
    BEGIN  SIZE CELL+ @  511 AND  448 = NOT WHILE
        Single-Byte  0 over C!  1 Sha-Update
    REPEAT

    Final-Count 8  Sha-Update ;

: .SHA
    Message-Digest 20 HTYPE   \  Display Message-Digest.
    ;

PREVIOUS DEFINITIONS
