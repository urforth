\ Mini-OOF 12apr98py (Bernd Paysan)
\ https://bernd-paysan.de/mini-oof.html
\ this is small, but fully usable OOP system, with early and late binding
\ see "samples/minioof-demo.f"
\ Ketmar Dark: added parent class pointer and some helper words
\ class layout:
\   cell instvars_size  (in bytes)
\   cell methods_size   (in bytes)
\   cell parent_class   (pointer to the parent class, added by k8)
\   method pointers follows (vmt)
\ first object instance var is always a pointer to the class
: method  ( m v -- m' v )  create over , swap cell+ swap
  does>  ( ... o -- ... )  @ over @ + @execute ;
: var  ( m v size -- m v' )  create over , +
  does>  ( o -- addr )  @ + ;
: class  ( class -- class methods vars )  dup 2@ ;
: (end-class)  ( class methods vars -- newclass )
  here >r ,  ;; put instance size
  dup ,      ;; put class size
  over ,     ;; put pointer to the parent class
  ;; fill vmt with "not implemented"
  3 cells ?DO ['] forth:(NOTIMPL) , 1 cells +LOOP
  ;; copy original class vmt  ( class | newclass )
  cell+ dup 2 +cells r@ rot @ 3 cells /string move r>
; (hidden)
: end-class  ( class methods vars -- )  create (end-class) create; drop ;
: defines  ( xt class -- )  ' cfa->pfa @ + ! ;
: new  ( class -- o )  dup @ n-allot 2dup swap @ erase dup nrot ! ;
: ::  ( class "name" -- ... )  ' cfa->pfa @ + @ compile, ;  \ early binding
\ some extended methods
alias @ (@class)  ( instance -- classptr )  (hidden)  \ get object instance class ptr
: (@class-parent)  ( classptr -- classptr )  [ 2 cells ] literal + @ ; (hidden)  \ get class parent ptr (result can be 0)
: @class  ( instance -- classptr )  dup if (@class) then ;  \ get object instance class ptr
: @class-parent  ( classptr -- classptr )  dup if (@class-parent) endif ;  \ get class parent ptr (result can be 0)
\ dynamic binding (k8)
: (m-exec)  ( ... classptr mtofs -- ... )  + @execute-tail ; (hidden)
: m-invoke  ( instance class "name" -- ... )  compiler:?comp
  ' cfa->pfa @ [compile] literal compile (m-exec) ; immediate
\ dynamic binding (k8)
: m-inherited  ( instance "name" -- ... )  compiler:?comp
  compile dup compile (@class) compile (@class-parent)
  ' cfa->pfa @ [compile] literal compile (m-exec) ; immediate
\ create root object
create Object
  1 cells ,  ;; instvars size (class pointer is always there)
  3 cells ,  ;; methods offset
  0 ,        ;; pointer to the parent class (object has none)
create;
