;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; lazy importing from dynamic libraries, with SAVEing support
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asmx86

(*
example:

  dynlib: libc libc.so.6

  dynlib-import: libc
  <libc-write> ssize_t write( int ( fd) , const void * ( buf) , size_t ( count) );
  <libc-printf> void printf( const char * ( fmt) , ... );  // this does remove all known args (i.e. "..." is left on the stack)
  end;

  " HI!\n" swap 1 libc-write . cr

  : testit  ( -- )  " HI!\n" swap 1 libc-write . cr ;
  : testpf  ( -- )  42 666 " 666=%d; 42=%d\n" drop libc-printf 2drop ;

  testit
  testpf
  .stack bye

please, note that argument order is reverse for cdecl!

all floating point arguments are passed on the data stack as doubles.
you can use "F>FPARG" to move fp argument from FP stack to the data stack.

to move function result back to FP stack, use "FPARG>F".

note that FP stack must be empty on function call. if you are not sure, you
can use FP-WIPE to clear FP stack before invoking a function.

  dynlib: libm libm.so.6

  dynlib-import: libm
  <libm-pow> double pow( double , double );
  end;

  : test-powf  ( -- )
    f# 2 f>fparg f# 3 f>fparg fp-wipe libm-powf
    " res=%g\n" drop libc-printf fparg-drop
  ;

this will print "9".
*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; clear FPU stack
code: FP-WIPE  ( -- )
  emms
  urnext
endcode

;; move FP argument from FP stack to the data stack
code: F>FPARG  ( -- n0 n1 )
  push  TOS
  push  TOS
  push  TOS
  fstp  qword [esp]
  pop   TOS
  urnext
endcode

;; move FP argument from the data stack to FP stack
code: FPARG>F  ( n0 n1 -- )
  push  TOS
  fld   qword [esp]
  add   esp,8
  pop   TOS
  urnext
endcode

;; drop FP argument from the data stack (can be used for ... functions)
alias 2DROP FPARG-DROP  (  n0 n1 -- )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary (dynlib-simple) (hidden) also (dynlib-simple) definitions

;; `true` means "do not import anything, do it on the first call
true value lazy-creation


;; for SAVEd images
0 var (last-dynlib)

(* dynlib list item:
    dd libhandle
    dd previtem-or-0
    dd importlist
    c1sz libname
*)

(* import list item:
    dd procaddr
    dd previtem-or-0
    dd libitemcfa
    db argcount  ;; in cells
    db restype   ;; 0: nope; 1: normal; 2: fp (double)
    c1sz funcname
*)

0 constant (res-none)
1 constant (res-cell)
2 constant (res-double)

: (dynlib-handle@)  ( dlpfa -- libhandle )  @ ;
: (dynlib-handle!)  ( value dlpfa -- )  ! ;
: (dynlib-prev^)  ( dlpfa -- dlpfa+prevaddrofs )  cell+ ;
: (dynlib-prev@)  ( dlpfa -- prevaddr )  (dynlib-prev^) @ ;
: (dynlib-ilist^)  ( dlpfa -- ilistaddr )  2 +cells ;
: (dynlib-ilist!)  ( value dlpfa -- )  (dynlib-ilist^) ! ;
: (dynlib-ilist@)  ( dlpfa -- ilistaddr )  (dynlib-ilist^) @ ;
: (dynlib-libname@)  ( dlpfa -- addr count )  3 +cells bcount ;

alias cfa->pfa (dynlib-cfa->data)  ( dlcfa -- dlpfa )

: (dynimp-addr@)  ( ilistaddr -- procaddr ) @ ;
: (dynimp-addr!)  ( value ilistaddr -- ) ! ;
: (dynimp-prev^)  ( ilistaddr -- ilistaddr+prevaddrofs ) cell+ ;
: (dynimp-prev@)  ( ilistaddr -- prevaddr ) (dynimp-prev^) @ ;
: (dynimp-libcfa@)  ( ilistaddr -- prevaddr ) 2 +cells @ ;
: (dynimp-argc@)  ( ilistaddr -- argc ) 3 +cells c@ ;
: (dynimp-restype@)  ( ilistaddr -- restype ) 3 +cells 1+ c@ ;
: (dynimp-name@)  ( ilistaddr -- addr count )  3 +cells 2+ bcount ;


: (dynlib-clear-imports)  ( dlpfa -- )
  (dynlib-ilist^) begin @ dup while 0 over (dynimp-addr!) (dynimp-prev^) repeat drop
;

;; clear all dynlib handles and imports
..: forth:(startup-init)  ( -- )
  (last-dynlib) begin @ ?dup while 0 over (dynlib-handle!) dup (dynlib-clear-imports) (dynlib-prev^) repeat
;..


: (dynlib-find-by-cfa)  ( cfa -- dlpfa true // false )
  (dynlib-cfa->data) (last-dynlib) begin @ ?dup while 2dup = if nip true exit endif (dynlib-prev^) repeat
  drop false
;

: (dynlib-dump-all)  ( -- )
  (last-dynlib) begin @ ?dup while
    dup (dynlib-libname@) type cr  dup (dynlib-ilist^)
    begin @ ?dup while
      2 spaces dup (dynimp-addr@) .hex8 space
      dup (dynimp-name@) type space
      dup (dynimp-argc@) . ." -> "
      dup (dynimp-restype@) case
        (res-none) of ." void" endof
        (res-cell) of ." cell" endof
        (res-double) of ." double" endof
        otherwise .
      endcase
      cr
    (dynimp-prev^) repeat
  (dynlib-prev^) repeat
; (hidden)


: dynlib:  ( -- )  \ libword oslibpath
  create here 0 , ( handle) (last-dynlib) @ , ( link) 0 , ( imports)
  parse-name dup 1 250 within not-?abort" invalid lib name"
  dup 2+ n-allot dup >r c1s:copy-counted r> c1s:zterm
  (last-dynlib) !
 does>
  dup @ dup ifnot drop  ;; load library
      \ ." DLOADING: \'" dup (dynlib-libname@) type ." \'\n"
    dup (dynlib-libname@) os:dlopen ?dup ifnot endcr ." FATAL: cannot import dynlib \`" (dynimp-name@) type ." \`\n" 1 n-bye endif
    over ! dup (dynlib-clear-imports)
    dup @
  endif nip
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; import syntax:

;; this creates "XGetModMap" word, imports `XGetModifierMapping`
;;   [XGetModMap] int XGetModifierMapping( int ); ( dpy -- modmap )
;;
;; this creates "XGetModifierMapping" word, imports `XGetModifierMapping`
;; int XGetModifierMapping( int ); ( dpy -- modmap )

: (dli-custom-name?)  ( addr count -- flag )
  dup 2 > if
    over c@ [char] < = if
      2dup + 1- c@ [char] > = if 2drop true exit endif
    endif
  endif
  2drop false
; (hidden)

: (dli-import-name?)  ( addr count -- flag )
  dup 2 250 within if
    dup 1 > if + 1- c@ [char] ( =  ;; )
    else 2drop false endif
  else 2drop false
  endif
; (hidden)


;; first two comment chars are not eated
: (dli-interpret-skip-c-multiline)  ( -- )
  tib-skipch tib-skipch
  begin tib-getch ?dup while [char] * = tib-peekch [char] / = and until tib-skipch
;

: id-char?  ( ch -- flag )
  case
    [char] A [char] Z bounds-of true endof
    [char] a [char] z bounds-of true endof
    [char] 0 [char] 9 bounds-of true endof
    [char] _ of true endof
    otherwise drop false
  endcase
;

: (dli-interpret-skip-comments)  ( -- )
  begin
    true parse-skip-comments parse-skip-blanks
    tib-peekch case
      [char] / of
        1 tib-peekch-n [char] * = if (dli-interpret-skip-c-multiline) drop false endif
      endof
      [char] ( of  ;; )
        1 tib-peekch-n bl <= if tib-skipch [char] ) parse 2drop drop false endif
      endof
      [char] \ of
        1 tib-peekch-n bl <= if tib-skipch parse-skip-to-eol drop false endif
      endof
    endcase
  until
;

: (dli-interpret-next-word)  ( -- addr count )
  (dli-interpret-skip-comments)
  tib-peekch dup [char] , = over [char] * = or if
    drop tib-in^ 1 >in 1+!
  else id-char? if
    \ parse-name
    tib-in^ 0 begin tib-peekch id-char? while 1+ tib-skipch repeat
    tib-peekch dup bl > if
      dup [char] , = swap [char] * = or ifnot
        begin tib-peekch bl > while 1+ tib-skipch repeat
      endif
    else drop
    endif
  else parse-name
  endif endif
    \ endcr ." <" 2dup type ." >\n"
;

enum{
  value (last-type-normal)
  value (last-type-fp)
  value (last-type-long)
  value (last-type-pointer)
  value (last-type-signed)
  value (last-type-unsigned)
}

-2 constant (no-type)
-1 constant (...-type)

(last-type-normal) value (last-type?)
(no-type) value (type-size)  ;; <0 for special, or cells

: (last-type-fp?)  ( -- flag )  (last-type?) (last-type-fp) = ;
: (last-type-long?)  ( -- flag )  (last-type?) (last-type-long) = ;
: (last-type-sign?)  ( -- flag )  (last-type?) (last-type-signed) (last-type-unsigned) bounds? ;
: (was-type?)  ( -- flag )  (type-size) (no-type) > ;

: (reset-type)  ( -- ) (no-type) to (type-size) ;

: (type-found)  ( size -- )
  (was-type?) ?abort" invalid type definition"
  to (type-size) (last-type-normal) to (last-type?)
;

: (simple-type)  ( size -- )  \ name
  create c, does> c@ (type-found)
;

: (float-type)  ( size -- )  \ name
  create c, does> c@ (type-found) (last-type-fp) to (last-type?)
;

vocabulary (dli-types) also (dli-types) definitions
: const  ( -- )  ;

: *  ( -- )
  (type-size) 0< ?abort" invalid type definition"
  1 to (type-size) (last-type-pointer) to (last-type?)
;

: int  ( -- )  ;; allow "long int", or a sign
  (last-type-sign?) (last-type-long?) or if (reset-type) endif
  1 (type-found)
;

: long  ( -- )  ;; allow "long long", or a sign
  (last-type-sign?) if false 1 (reset-type)
  else (last-type-long?) if false 2 (reset-type)
  else true 1 endif endif
  (type-found) if (last-type-long) to (last-type?) endif
;

: char  ( -- )  ;; allow sign
  (last-type-sign?) if (reset-type) endif
  1 (type-found)
;

: signed  ( -- )  1 (type-found) (last-type-signed) to (last-type?) ;
: unsigned  ( -- )  1 (type-found) (last-type-unsigned) to (last-type?) ;

1 (simple-type) size_t
1 (simple-type) ssize_t
0 (simple-type) void

1 (simple-type) int8_t
1 (simple-type) uint8_t
1 (simple-type) int16_t
1 (simple-type) uint16_t
1 (simple-type) int32_t
1 (simple-type) uint32_t
2 (simple-type) int64_t
2 (simple-type) uint64_t

2 (float-type) float  ;; floats are passed as doubles
2 (float-type) double

0 (simple-type) GLvoid
1 (simple-type) GLintptr
1 (simple-type) GLsizei
1 (simple-type) GLchar
1 (simple-type) GLcharARB
1 (simple-type) GLushort
2 (simple-type) GLint64EXT
1 (simple-type) GLshort
2 (simple-type) GLuint64
1 (simple-type) GLhalfARB
1 (simple-type) GLubyte
alias double GLdouble
1 (simple-type) GLhandleARB
2 (simple-type) GLint64
1 (simple-type) GLenum
1 (simple-type) GLeglImageOES  ;; pointer
1 (simple-type) GLintptrARB
1 (simple-type) GLsizeiptr
1 (simple-type) GLint
1 (simple-type) GLboolean
1 (simple-type) GLbitfield
1 (simple-type) GLsizeiptrARB
alias float GLfloat
2 (simple-type) GLuint64EXT
alias float GLclampf
1 (simple-type) GLbyte
alias double GLclampd
1 (simple-type) GLuint
1 (simple-type) GLvdpauSurfaceNV
1 (simple-type) GLfixed
1 (simple-type) GLhalf
1 (simple-type) GLclampx
1 (simple-type) GLhalfNV

: ... ( -- -1 )  (...-type) (type-found) ;
previous definitions

: (dli-parse-type)  ( addr count -- argsize true // false )
  (reset-type) (last-type-normal) to (last-type?)
  >in @ >r  ;; save "previn"
  ;; ( addr count | previn )
  begin vocid: (dli-types) voc-search while
    execute rdrop >in @ >r (dli-interpret-next-word)
  repeat 2drop r> >in !
  (was-type?) dup if (type-size) swap endif
    \ >in @ (dli-interpret-next-word) endcr ." **<" type >in ! ." >**\n"
; (hidden)


: (dli-expect-endargs)  ( -- )
  (dli-interpret-next-word) 2dup " );" s= not-?abort" `);` expected"
;

: (dli-parse-args)  ( -- argsize )
  0 (dli-interpret-next-word) dup not-?abort" unfinished import declaration"
  2dup " );" s= if 2drop
  else
    begin
      (dli-parse-type) not-?abort" unfinished import declaration"
      dup ifnot  ;; void -- it should be the only arg here
        drop dup ?abort" void argument? wtf?!"
        (dli-expect-endargs) break
      endif
      dup (...-type) = if ( ... ) drop (dli-expect-endargs) break endif
      +  (dli-interpret-next-word)
      2dup " );" s= ifnot
        " ," s= not-?abort" comma expected"
        (dli-interpret-next-word) dup not-?abort" unfinished import declaration"
        false
      else true endif
    until 2drop
  endif
;


0 value (dli-cfa)  (hidden)
0 value (dli-pfa)  (hidden)
0 value (dli-handle)  (hidden)

;; "does>" part of the function, calls the function
;; imports dynlib and resolves it first if necessary
: (dli-imp-exec)  ( impdata -- )
  dup (dynimp-addr@) dup ifnot drop
      \ ." IMPORTING: \'" dup (dynimp-name@) type ." \'\n"
    dup (dynimp-libcfa@) execute
    over (dynimp-name@) rot os:dlsym ?dup ifnot
      endcr ." FATAL: symbol \'" dup (dynimp-name@) type ." \' not found in library \'"
      (dynimp-libcfa@) (dynlib-cfa->data) (dynlib-libname@) type ." \'\n" 1 n-bye
    endif
    over (dynimp-addr!) dup (dynimp-addr@)
  endif
  over (dynimp-restype@) >r swap (dynimp-argc@) os:cinvoke
  ;; result
  r> case
    (res-none) of drop endof
    (res-double) of drop F>FPARG endof  ;; fp result
  endcase
; (hidden)


: (dynlib-parse-one-import)  ( addr count -- )
  2dup (dli-custom-name?) if  ;; custom-named
    /char 1- create-named
    (dli-interpret-next-word) true
  else false endif
  ;; ( taddr tcount createdflag )
  >r (dli-parse-type) not-?abort" rettype?"
  (last-type-fp?) if 2 = not-?abort" invalid float rettype" (res-double)
  else dup 0 1 bounds? not-?abort" invalid rettype" if (res-cell) else (res-none) endif
  endif
  (dli-interpret-next-word) 2dup (dli-import-name?) not-?abort" import what?" 1-
  ;; ( restype naddr ncount | createdflag )
  r> ifnot 2dup create-named endif  ;; create a word
  (dli-handle) if  ;; immediate import
    2dup (dli-handle) os:dlsym ?dup ifnot endcr ." symbol not found: " type abort" import symbol failure" endif
  else 0
  endif
  here    ;; ( restype naddr ncount procaddr here )
  swap ,  ;; procaddr
  (dli-pfa) (dynlib-ilist@) ,  ;; previtem
  (dli-pfa) (dynlib-ilist!)    ;; link
  (dli-cfa) ,  ;; libitemcfa
  (dli-parse-args) c,  ;; argcount
  rot c,  ;; restype
  ;; put name
  dup 1+ n-allot dup >r c1s:copy-counted r> c1s:zterm
  ['] (dli-imp-exec) latest-cfa forth:(does>!) create;
;

: (dynlib-import-interpret)  ( -- )
  begin
      \ tib-curr-line . depth . cr
    (dli-interpret-next-word) dup while
    2dup " end;" s=ci if break endif
    ;; must be an import
    (dynlib-parse-one-import)
  repeat
  2drop
; (hidden)


: dynlib-import:  ( -- dlpfa dlhandle )  \ libname
  ' dup (dynlib-find-by-cfa) not-?abort" try to import from a real dynlib next time"
  to (dli-pfa) to (dli-cfa)
  lazy-creation if 0 else (dli-cfa) execute endif to (dli-handle)
  (dynlib-import-interpret)
;

previous definitions

alias (dynlib-simple):dynlib: dynlib:
alias (dynlib-simple):dynlib-import: dynlib-import:

