;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; supports base prefixes, but not DPL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; convert a character string left at addr to a signed number, using the current numeric base
: NUMBER-DBL  ( addr count -- ud 1 // d -1 // addr count false )
  ;; check length
  dup 0<= if false exit endif
  2dup 2>r  ;; for failure exit
  ;; ok, we have at least one valid char
  number-parse-pfx-sfx-sign
  base @ >r ?dup if base ! endif
  nrot ;; ( negflag addr count )
  ;; zero count means "nan"
  dup 0> ifnot  ;; no number chars besides a prefix
    r> base !   ;; restore base
    2drop drop 2r> false exit  ;; exit with failure
  endif
  ;; ( negflag addr count | oldbase )
  0 u>d 2swap >number  ;; ( negflag ud addr count | oldbase )
  ?dup ifnot  ;; no more chars
    r> base !  2drop
    swap if negate endif
    2rdrop true
  else  ;; have some more chars
    over c@ [char] . = ifnot r> base ! 2drop 2drop 2r> false exit endif
    /char >number r> base !  ;; ( negflag ud addr count )
    if 2drop 2drop 2r> false exit endif  ;; too many chars
    drop rot if dnegate endif
    2rdrop -1
  endif
;

..: interpret-not-found  ( addr count -- true // addr count false )
  number-dbl dup if
    state @ if
      -if swap [compile] literal endif [compile] literal
    else drop endif
    true exit
  endif
;..
