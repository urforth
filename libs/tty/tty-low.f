;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm
vocabulary tty
also tty definitions


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (setup-winch-handler)  ( -- errcode )
  push  TOS
  sub   esp,4*4
  ld    dword [esp],.sighandler
  ld    dword [esp+4],0
  ;; SA_RESTART+SA_SIGINFO
  ld    dword [esp+8],0x10000000
  ld    dword [esp+12],0
  ld    eax,67
  ld    ebx,28  ;; SIGWINCH
  ld    ecx,esp
  xor   edx,edx
  syscall
  add   esp,4*4
  ld    TOS,eax
  urnext

.sighandler:
  ld    eax,1
  lock  xchg dword [winch_flag],eax
  ret

  align 16,0
winch_flag: dd 0
endcode

;; this doesn't reset the flag
code: was-winch?  ( -- flag )
  push  TOS
  ld    eax,1
  lock  cmpxchg [winch_flag],eax
  ld    TOS,eax
  urnext
endcode

;; this also resets the flag
code: get-winch  ( -- flag )
  push  TOS
  xor   TOS,TOS
  lock  xchg TOS,[winch_flag]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: FD-TTY?  ( fd -- flag )
  ;; winsize is 4 2-byte values: row,col,xpixel,ypixel
  ;; use stack for that
  ;; buffer
  push  eax
  push  eax
  ld    eax,54      ;; ioctl
  ld    ebx,TOS     ;; fd
  ld    ecx,0x5413  ;; TIOCGWINSZ
  ld    edx,esp     ;; buffer
  syscall
  ;; 0 means "success"
  cp    eax,1
  ld    TOS,0
  adc   TOS,0
  add   esp,4+4
  urnext
endcode

code: GET-DIMS  ( -- width height )
  ;; winsize is 4 2-byte values: row,col,xpixel,ypixel
  ;; use stack for that
  push  TOS
  ;; buffer
  push  eax
  push  eax
  ld    eax,54      ;; ioctl
  ld    ebx,[pfa "stdout-fd"]
  ld    ecx,0x5413  ;; TIOCGWINSZ
  ld    edx,esp     ;; buffer
  syscall
  ;; 0 means "success"
  movzx ecx,word [esp]
  movzx edx,word [esp+2]
  or    eax,eax
  jr    z,@f
  ld    cx,24       ;; default
  ld    dx,80       ;; default
@@:
  add   esp,4*2
  push  edx
  urnext
endcode

;; this is using stdout fd
: WIDTH?  ( -- width )
  get-dims drop
;

;; this is using stdout fd
: HEIGHT?  ( -- height )
  get-dims nip
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
60 constant #termios
: (termios-fld-at)  ( n -- )  create c, does> c@ + ;

 0 (termios-fld-at) termios.c_iflag
 4 (termios-fld-at) termios.c_oflag
 8 (termios-fld-at) termios.c_cflag
12 (termios-fld-at) termios.c_lflag
16 (termios-fld-at) termios.c_line
17 (termios-fld-at) termios.c_cc
52 (termios-fld-at) termios.c_ispeed
56 (termios-fld-at) termios.c_ospeed
(*
struct termios {
  uint32_t c_iflag;
  uint32_t c_oflag;
  uint32_t c_cflag;
  uint32_t c_lflag;
  uint8_t c_line;
  uint8_t c_cc[32];
  uint32_t c_ispeed;
  uint32_t c_ospeed;
};
*)

: dump-tio  ( addr -- )
  endcr ." === TIO ===\n"
  ." c_iflag : 0x" dup tty:termios.c_iflag @ .hex8 cr
  ." c_oflag : 0x" dup tty:termios.c_oflag @ .hex8 cr
  ." c_cflag : 0x" dup tty:termios.c_cflag @ .hex8 cr
  ." c_lflag : 0x" dup tty:termios.c_lflag @ .hex8 cr
  ." c_ispeed: " dup tty:termios.c_ispeed @ . cr
  ." c_ospeed: " dup tty:termios.c_ospeed @ . cr
  ." c_line  : 0x" dup tty:termios.c_lflag c@ .hex2 cr
  space 16 for dup tty:termios.c_cc i + c@ space .hex2 endfor cr
  space 16 for dup tty:termios.c_cc 16 + i + c@ space .hex2 endfor cr
  space 3 for dup tty:termios.c_cc 32 + i + c@ space .hex2 endfor cr
  drop
; (hidden)

#termios buffer: (orig-tio)
#termios buffer: (temp-tio)

code: GET-TIO-MODE  ( addr -- errcode )
  ld    edx,TOS     ;; buffer
  ld    eax,54      ;; ioctl
  ld    ebx,1       ;; stdout
  ld    ecx,0x5401  ;; TCGETS
  syscall
  ld    TOS,eax
  urnext
endcode

code: SET-TIO-MODE  ( addr -- errcode )
  ld    edx,TOS     ;; buffer
  ld    eax,54      ;; ioctl
  ld    ebx,1       ;; stdout
  ld    ecx,0x5404  ;; TCSETSF (TCSAFLUSH)
  syscall
  ld    TOS,eax
  urnext
endcode

(orig-tio) get-tio-mode drop

..: forth:(on-bye) (orig-tio) set-tio-mode drop ;..

..: forth:(abort-cleanup) (orig-tio) set-tio-mode drop ;..


true constant raw
false constant cooked

: SET-MODE  ( rawflag -- success )
  stdout-fd fd-tty? ifnot false exit endif
  if
    ;; raw
    \ (temp-tio) get-tio-mode if false exit endif
    (temp-tio) #termios erase
    0o0001 ( IGNBRK) (temp-tio) termios.c_iflag !
    \ output modes: disable post processing
    [ 0o0001 ( OPOST) 0o0004 ( ONLCR) or ] literal (temp-tio) termios.c_oflag !
    \ control modes: set 8 bit chars
    [ 0o0060 ( CS8) 0o4000 ( CLOCAL) or ] literal (temp-tio) termios.c_cflag !
    \ control chars: set return condition: min number of bytes and timer; we want read to return every single byte, without timeout
    \ termios.c_cc[VMIN] = (isWaitKey ? 1 : 0); // wait/poll mode
    \ 0 (temp-tio) termios.c_cc 6 ( VMIN) + c!
    \ termios.c_cc[VTIME] = 0; // no timer
    \ put terminal in raw mode after flushing
    (temp-tio) set-tio-mode if false exit endif
    \ G0 is ASCII, G1 is graphics
    " \x1b(B\x1b)0\x0f" forth:(stdtty-xtype)
    true
  else
    (orig-tio) set-tio-mode 0=
  endif
;


;; to: -1 is infinite; 0 is no wait
: wait-key?  ( mstimeout -- eventflag )
  os:#pollfd ralloca >r 0
  begin
    drop stdin-fd r@ os:pollfd.fd !
    os:poll-in r@ os:pollfd.events !  ;; this also clears revents
    dup r@ 1 rot os:poll dup -4 <>
  until
  rdrop nip os:#pollfd rdealloca 0>
;

;; to: -1 is infinite; 0 is no wait
: wait-key  ( mstimeout -- key // -1 )
  wait-key? if forth:(stdtty-getch) else -1 endif
;


: enable-paste-events   ( -- ) " \x1b[?2004h" forth:(stdtty-xtype) ;
: disable-paste-events  ( -- ) " \x1b[?2004l" forth:(stdtty-xtype) ;

: enable-focus-events   ( -- ) " \x1b[?1004h" forth:(stdtty-xtype) ;
: disable-focus-events  ( -- ) " \x1b[?1004l" forth:(stdtty-xtype) ;

: enable-mouse-events   ( -- ) " \x1b[?1000h\x1b[?1006h\x1b[?1002h" forth:(stdtty-xtype) ;
: disable-mouse-events  ( -- ) " \x1b[?1002l\x1b[?1006l\x1b[?1000l" forth:(stdtty-xtype) ;

: enable-autowrap   ( -- ) " \x1b[?7h" forth:(stdtty-xtype) ;
: disable-autowrap  ( -- ) " \x1b[?7l" forth:(stdtty-xtype) ;


0 constant term-other
1 constant term-rxvt
2 constant term-xterm
3 constant term-linux  ;; linux console

: detect-term  ( -- type )
  term-other
  " TERM" forth:get-env if
    2dup 4 min " rxvt" s=ci if 2drop drop term-rxvt
    else 5 min
      2dup " xterm" s=ci if 2drop drop term-xterm
      else " linux" s=ci if drop term-linux
      endif endif
    endif
  endif
;

detect-term value term-type

: .ansi-escape  ( -- )  " \x1b[" forth:(stdtty-xtype) ;
: (xemit)  ( ch -- )  forth:(stdtty-xemit) ;

: .ansi-delim  ( -- )  [char] ; (xemit) ;
: ansi-endch.  ( ch -- )  (xemit) ;

;; print decimal number, max 4 digits
: (ansi-digit)  ( n wasnum divisor -- n%divisor wasnum )
  swap >r
  2dup u/ dup r@ or if 48 + (xemit) rdrop true >r else drop endif
  umod r>
;

: ansi-num.  ( n -- )
  0 9999 clamp
  false  ;; wasnum
  1000 (ansi-digit) 100 (ansi-digit) 10 (ansi-digit) drop 48 + (xemit)
;


;; we're using "(defer)" to make it work regardless of ans-compatibility

: at-xy  ( x y -- )
  .ansi-escape
  1+ 1 max ansi-num. [char] ; (xemit)
  1+ 1 max ansi-num. [char] H (xemit)
;

: page " \x1b[2J\x1b[H" forth:(stdtty-xtype) ;


(setup-winch-handler) drop

previous definitions
