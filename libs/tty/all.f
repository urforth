;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] forth:tty
\ only forth definitions
.LIB-START" tty interface"

" tty-low.f" tload
" tty-ekey-consts.f" tload
" tty-ekey-names.f" tload
" tty-ekey.f" tload
" tty-editor.f" tload

.LIB-END
[ENDIF]
