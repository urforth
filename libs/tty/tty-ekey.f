;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; THIS IS NOT MULTITHREAD-SAFE!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] tty " tty-low.f" tload [ENDIF]

also tty definitions

false constant (ekey-debug?)


(ekey-debug?) [IF]
: (.keycode)  ( keycode -- )
  dup 33 127 within if emit
  else ." 0x" dup 0xffff u> if .hex8 else .hex2 endif
  endif
;

: wait-key-dbg  ( tout -- keycode )
  wait-key
  endcr ."  key: " dup (.keycode) cr
;
[ELSE]
alias wait-key wait-key-dbg
[ENDIF]

100 value esc-timeout


: (ekey-esc-read?)  ( key -- key false // 27 true )
  dup 0< over 27 = or dup if 2drop 27 true endif
;

: (ekey-fix-ctrl-letter)  ( chcode -- keycode )
  dup 1 27 within if
    dup 9 = over 13 = or ifnot
      [ [CHAR] A 1- K-CTRL-MASK or ] literal +
      ;; xterm has it reversed
    endif
  endif
  term-type term-xterm = if
    ;; xterm has it reversed
    dup K-BACKSPACE = if drop K-^H exit endif
    dup K-^H = if drop K-BACKSPACE exit endif
  endif
;


0 var (ekey-csi-count)
0 var (ekey-csi-array) 2 cells allot

0x1000 constant (ekey-csi-flag-mouse)
0x2000 constant (ekey-csi-flag-linux)
0 var (ekey-csi-endchar)  ;; with flags

: (ekey-parse-csi-init)  ( -- )
  (ekey-csi-count) 0! (ekey-csi-endchar) 0! (ekey-csi-array) 3 cells erase
;

;; "false" means "stop parsing"
: (ekey-parse-csi-firstchar)  ( -- ch true // ch false )
  ;; get first char
  esc-timeout wait-key-dbg (ekey-esc-read?) if false exit endif
  case
    [char] I of K-FOCUS-IN false exit endof
    [char] O of K-FOCUS-OUT false exit endof
    [char] < of (ekey-csi-flag-mouse) (ekey-csi-endchar) ! -1 endof
    [char] [ of (ekey-csi-flag-linux) (ekey-csi-endchar) ! -1 endof
    otherwise  ;; keep the code
  endcase
  dup -if drop esc-timeout wait-key-dbg (ekey-esc-read?) if false exit endif endif
  true
;

;; "false" means "we're done"
: (ekey-parse-csi-check-firstchar)  ( char -- char true // false )
  ;; if it is not a digit and not a semi, we have no args
  dup 10 digit ifnot
    dup [char] ; = ifnot (ekey-csi-endchar) or! false exit endif
  endif
  drop true
;

: (ekey-parse-csi-with-args)  ( char -- true // keycode false )
  begin
    ;; digit?
    dup 10 digit if
      nip  ;; drop keycode
      (ekey-csi-count) @ dup 3 < if
        (ekey-csi-array) cells^
        dup @ 10 * rot + 32767 min swap !
      else
        2drop
      endif
    else
      (ekey-csi-count) 1+!
      dup [char] ; = if drop
        (ekey-csi-count) @ 16 > if K-UNKNOWN false exit endif
      else
        ;; end of csi
        (ekey-csi-endchar) or!
        true exit
      endif
    endif
    ;; get next char
    esc-timeout wait-key-dbg (ekey-esc-read?) if false exit endif
  again
;

: (ekey-parse-csi)  ( -- true // keycode false )
  (ekey-parse-csi-init)
  (ekey-parse-csi-firstchar) ifnot false exit endif
  (ekey-parse-csi-check-firstchar) ifnot true exit endif
  (ekey-parse-csi-with-args)
  (*
    endcr ." CSI: n=" (ekey-csi-count) @ .
    ." n0=" (ekey-csi-array) @ .
    ." n1=" (ekey-csi-array) cell+ @ .
    ." n2=" (ekey-csi-array) 2 +cells @ .
    ." endchar=" (ekey-csi-endchar) @ (.keycode)
    ."  xterm-mods=" (ekey-csi-array) cell+ @ (ekey-xterm-mods) .hex8
    cr
  *)
;


: (ekey-xterm-mods)  ( csicode -- modflags )
  case
    2 of K-SHIFT-MASK endof
    3 of K-ALT-MASK endof
    4 of [ K-ALT-MASK K-SHIFT-MASK or ] literal endof
    5 of K-CTRL-MASK endof
    6 of [ K-CTRL-MASK K-SHIFT-MASK or ] literal endof
    7 of [ K-CTRL-MASK K-ALT-MASK or ] literal endof
    8 of [ K-CTRL-MASK K-ALT-MASK or K-SHIFT-MASK or ] literal endof
    otherwise drop 0
  endcase
;

: (ekey-xterm-special)  ( upcase-letter -- keycode )
  case
    [char] A of K-UP endof
    [char] B of K-DOWN endof
    [char] C of K-RIGHT endof
    [char] D of K-LEFT endof
    [char] E of K-PAD5 endof
    [char] H of K-HOME endof
    [char] F of K-END endof
    [char] P of K-F1 endof
    [char] Q of K-F2 endof
    [char] R of K-F3 endof
    [char] S of K-F4 endof
    [char] Z of K-F1 K-SHIFT-MASK or endof
    otherwise drop K-UNKNOWN
  endcase
;


: (ekey-csi-linux-special)  ( chcode -- keycode )
  case
    [char] A of K-F1 endof
    [char] B of K-F1 endof
    [char] C of K-F2 endof
    [char] D of K-F3 endof
    otherwise drop K-UNKNOWN
  endcase
;

: (ekey-csi-xterm-special)  ( chcode -- keycode )
  (ekey-xterm-special)
;

: (ekey-csi-special)  ( csiarg0 -- keycode )
  case
    1 of K-HOME endof  ;; xterm
    2 of K-INSERT endof
    3 of K-DELETE endof
    4 of K-END endof
    5 of K-PRIOR endof ;; pageup
    6 of K-NEXT endof  ;; pagedown
    7 of K-HOME endof  ;; rxvt
    8 of K-END endof
    [ 1 10 + ] literal of K-F1 endof
    [ 2 10 + ] literal of K-F2 endof
    [ 3 10 + ] literal of K-F3 endof
    [ 4 10 + ] literal of K-F4 endof
    [ 5 10 + ] literal of K-F5 endof
    [ 6 11 + ] literal of K-F6 endof
    [ 7 11 + ] literal of K-F7 endof
    [ 8 11 + ] literal of K-F8 endof
    [ 9 11 + ] literal of K-F9 endof
    [ 10 11 + ] literal of K-F10 endof
    [ 11 12 + ] literal of K-F11 endof
    [ 12 12 + ] literal of K-F12 endof
    otherwise drop K-UNKNOWN
  endcase
;

: (ekey-csi-args-0)  ( -- chcode )
  (ekey-csi-endchar) @ dup 0xff and swap (ekey-csi-flag-linux) and
  if (ekey-csi-linux-special) else (ekey-csi-xterm-special) endif
;

: (ekey-csi-args-1-tilda)  ( -- chcode true // false )
  (ekey-csi-array) @ case
    23 of [ K-SHIFT-MASK K-F1 or ] literal true endof
    24 of [ K-SHIFT-MASK K-F2 or ] literal true endof
    25 of [ K-SHIFT-MASK K-F3 or ] literal true endof
    26 of [ K-SHIFT-MASK K-F4 or ] literal true endof
    28 of [ K-SHIFT-MASK K-F5 or ] literal true endof
    29 of [ K-SHIFT-MASK K-F6 or ] literal true endof
    31 of [ K-SHIFT-MASK K-F7 or ] literal true endof
    32 of [ K-SHIFT-MASK K-F8 or ] literal true endof
    33 of [ K-SHIFT-MASK K-F9 or ] literal true endof
    34 of [ K-SHIFT-MASK K-F10 or ] literal true endof
    200 of K-PASTE-START true endof
    201 of K-PASTE-END true endof
    otherwise drop false
  endcase
;

: (ekey-csi-args-1-xterm)  ( chcode -- chcode )
  (ekey-csi-array) @ (ekey-xterm-mods) swap (ekey-xterm-special)
  dup K-UNKNOWN <> if or else drop endif  ;; apply mask
;

: (ekey-csi-args-1-mask)  ( chcode -- mask true // false )
  case
    [char] ^ of K-CTRL-MASK true endof
    [char] $ of K-SHIFT-MASK true endof
    [char] @ of [ K-CTRL-MASK K-SHIFT-MASK or ] literal true endof
    otherwise drop false
  endcase
;

: (ekey-csi-args-1)  ( -- chcode )
  (ekey-csi-endchar) @ dup 0xff and [char] ~ = if
    drop (ekey-csi-args-1-tilda) if exit endif
    0  ;; mask
  else
    dup [char] A [char] Z bounds? if (ekey-csi-args-1-xterm) exit endif
    (ekey-csi-args-1-mask) ifnot K-UNKNOWN exit endif
  endif
  ;; ( mask )
  (ekey-csi-array) @ (ekey-csi-special)
  dup K-UNKNOWN <> if or else drop endif  ;; apply mask
;

: (ekey-csi-args-2)  ( -- chcode )
  (ekey-csi-array) cell+ @ (ekey-xterm-mods) ?dup ifnot K-UNKNOWN exit endif
  (ekey-csi-endchar) @ 0xff and
  (ekey-csi-array) @ 1 = if (ekey-xterm-special)
  else [char] ~ = if (ekey-csi-array) @ (ekey-csi-special)
  else K-UNKNOWN
  endif endif
  dup K-UNKNOWN <> if or else drop endif  ;; apply mask
;

: (ekey-csi)  ( -- keycode )
  (ekey-parse-csi) ifnot exit endif
  ;; this seems to be a csi; check for valid argcount
  (ekey-csi-count) @ 2 > if K-UNKNOWN exit endif
  ;; mouse events are not here yet -- {\e}[<0;58;32M (button;x;y;[Mm])
  (ekey-csi-endchar) @ (ekey-csi-flag-mouse) and if K-UNKNOWN exit endif
  ;; specials
  (ekey-csi-count) @ case
    0 of (ekey-csi-args-0) endof
    1 of (ekey-csi-args-1) endof
    2 of (ekey-csi-args-2) endof
    otherwise drop K-UNKNOWN
  endcase
;

: (ekey-rxvt-keypad)  ( char -- newchar true // oldchar false )
  dup case
    [char] j of drop [char] * true endof
    [char] k of drop [char] + true endof
    [char] m of drop [char] - true endof
    [char] n of drop K-DELETE true endof
    [char] o of drop [char] / true endof
    [char] p of drop K-INSERT true endof
    [char] q of drop K-END true endof
    [char] r of drop K-DOWN true endof
    [char] s of drop K-PRIOR true endof
    [char] t of drop K-LEFT true endof
    [char] u of drop K-PAD5 true endof
    [char] v of drop K-RIGHT true endof
    [char] w of drop K-HOME true endof
    [char] x of drop K-UP true endof
    [char] y of drop K-NEXT true endof
    [char] M of drop K-ENTER true endof
    otherwise drop false
  endcase
;


;; to: -1 is infinite; 0 is no wait
: wait-ekey  ( mstimeout -- key // -1 )
  get-winch if drop K-WINCH exit endif
  wait-key-dbg dup 27 = ifnot (ekey-fix-ctrl-letter) exit endif
  ;; esc sequence
  drop esc-timeout wait-key-dbg (ekey-esc-read?) if exit endif
  ;; xterm stupidity
  dup [char] O = if
    drop esc-timeout wait-key-dbg dup 0> if
      (ekey-esc-read?) if exit endif
      (ekey-rxvt-keypad) ifnot
        (* dup upcase-char (ekey-xterm-special)
        swap dup upcase-char <> if K-SHIFT-MASK or endif *)
        (ekey-xterm-special)
      endif
      exit
    else drop [char] O endif
  endif
  ;; csi
  dup [char] [ = if drop (ekey-csi)
  else
    ;; ctrl mask for non-control keys
    dup [char] A >= over [char] Z <= and if K-SHIFT-MASK or else upcase-char (ekey-fix-ctrl-letter) endif
    K-ALT-MASK or
  endif
;

: ekey?  ( -- flag )  0 wait-key? ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ekey>fkey  ( keycode -- keycode ekeyflag )
  dup bl < over 127 = or over 255 > or
;

: ekey>char  ( keycode -- keycode false // char true )
  ekey>fkey not
;


previous definitions
