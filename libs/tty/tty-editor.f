;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; line editor
;; THIS IS NOT MULTITHREAD-SAFE!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also tty definitions

nested-vocabulary editor
also editor definitions

0 value addr
0 value maxlen
0 value len
0 value xy
80 value scrlen
0 value last-tty-width  ;; set in editor loop, used to resize the editor
0 value leftpos
0 value curpos
false value need-redraw?

;; highlighted string part
0 value histart
0 value hilen

0 value default-cmd-table
0 value curr-cmd-table

true value abort-allowed?
true value escape-allowed?

-2 constant exit-abort
-1 constant exit-escape
 1 constant exit-accept
0 value exit-code  ;; -2:^C; -1:escaped; 1: entered
0 value ctrlc-counter

#termios buffer: saved-tio

80 constant #spaces
#spaces buffer: spaces-str
spaces-str #spaces blank

;; used to adjust leftpos by 1 after inserting (to avoid ugly jumps)
false value last-was-insert?


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: printable?  ( ch -- flag )  dup bl < over 127 = or swap 255 > or not ;

: (yx@)  ( -- y x )  xy dup 16 rshift 0xffff and swap 0xffff and ;
: (x@)   ( -- x )  (yx@) nip ;
: (y@)   ( -- x )  (yx@) drop ;

: (x!)  ( value -- )  0 9999 clamp xy 0xffff ~and or to xy ;
: (y!)  ( value -- )  0 9999 clamp 16 lshift xy 0xffff and or to xy ;

: atxy-ofs  ( xofs -- )  0 scrlen 1- clamp (yx@) rot + swap at-xy ;
: atxy      ( -- )  0 atxy-ofs ;
: atxy-cur  ( -- )  curpos leftpos - atxy-ofs ;
: atxy-end  ( -- )  scrlen 1- atxy-ofs ;

: dirty  ( -- ) true to need-redraw? ;

: sanitise-leftpos  ( -- )
  leftpos dup
  0  len scrlen 4- 0 max - 0 max  clamp  dup to leftpos
  - if dirty endif
;

: ensure-cursor-visible  ( -- )
  curpos 0 len clamp dup to curpos
  leftpos dup scrlen + within ifnot ;; cursor is out of screen
    curpos leftpos < if curpos 4-
    else curpos scrlen last-was-insert? if 1- else 4- endif -
    endif
    to leftpos dirty
  endif
  sanitise-leftpos
  false to last-was-insert?
;

;; draw helpres assume that leftpos is sanitised
: draw-calc-visible-part  ( -- addr count )
  addr leftpos +  len leftpos - 0 scrlen clamp
;

: draw-calc-clear-part  ( -- count )
  scrlen draw-calc-visible-part nip -
;

: draw-reset-attrs  ( -- ) " \x1b[0m" forth:(stdtty-xtype) ;
: draw-attr-search-hi  ( onflag -- ) if " \x1b[1m" else " \x1b[22m" endif forth:(stdtty-xtype) ;
: draw-attr-scroll-hi  ( onflag -- ) if " \x1b[4m" else " \x1b[24m" endif forth:(stdtty-xtype) ;

: draw-normal  ( -- )
  draw-calc-visible-part
  forth:(stdtty-xtype)
;

: draw-highlighted  ( -- )
  draw-calc-visible-part dup +if
    over addr - histart u> if true draw-attr-search-hi endif
    over + swap do
      i addr - histart = if true draw-attr-search-hi endif
      i addr - histart hilen + = if false draw-attr-search-hi endif
      i c@ forth:(stdtty-xemit)
    loop
    false draw-attr-search-hi
  else 2drop endif
;

: draw-spaces  ( count -- )
  begin dup +while dup #spaces min spaces-str over forth:(stdtty-xtype) - repeat drop
;

: draw-scroll-mark-char  ( ch -- )
  true draw-attr-scroll-hi forth:(stdtty-xemit) false draw-attr-scroll-hi
;

: draw-left-scroll-mark  ( -- )
  leftpos +if atxy [char] < draw-scroll-mark-char endif
;

: draw-right-scroll-mark  ( -- )
  leftpos scrlen + len <= if atxy-end [char] > draw-scroll-mark-char endif
;

: draw-scroll-marks  ( -- ) draw-left-scroll-mark draw-right-scroll-mark ;

: draw  ( -- )
  ensure-cursor-visible
  need-redraw? if
    false to need-redraw?
    atxy draw-reset-attrs
    leftpos len < if
      hilen +if draw-highlighted else draw-normal endif
      draw-reset-attrs  ;; just in case
    endif
    draw-calc-clear-part draw-spaces
    draw-scroll-marks
  endif
  atxy-cur
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: inschar  ( ch -- )
  dup printable? ifnot drop
  else len maxlen < ifnot drop forth:(stdtty-bell)
  else 1 +to len
       addr curpos + dup 1+ len curpos - move
       addr curpos + c! 1 +to curpos dirty
       0 to hilen  true to last-was-insert?
  endif endif
;

: delchar  ( -- )
  curpos len < if
    1 -to len
    addr curpos + dup 1+ swap len curpos - move dirty
    0 to hilen  true to last-was-insert?
  endif
;

;; oobchar on out-of-bounds
: peekchar-ex  ( ofs oobchar -- ch )
  swap curpos + dup 0 len within ifnot drop else nip addr + c@ endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(*
  history list is double-linked list of all entered strings
  dd next
  dd prev
  dd strlen
    string

  strings are ordered from the newest to the oldest
*)
;; list head (newest string)
0 value history-list
;; current history item address
0 value history-curr

0 value history-saved
0 value history-#saved

3 cells constant hitem-#header

;; incremental history search buffer, 4KB (one page), c4s
0 value history-search-buf


: (history-init)  ( -- )
  0 to history-list
  0 to history-curr
  0 to history-saved
  0 to history-#saved
  0 to history-search-buf
; (hidden)


: hitem-next@  ( itemaddr -- iaddr )  dup if @ endif ;
: hitem-prev@  ( itemaddr -- iaddr )  dup if cell+ @ endif ;

: hitem-next!  ( addr itemaddr -- )  dup if ! else 2drop endif ;
: hitem-prev!  ( addr itemaddr -- )  dup if cell+ ! else 2drop endif ;

: hitem->text  ( itemaddr -- textaddr )  dup if 2 +cells endif ;


: history-find-whole  ( addr count -- itemaddr true // false )
  dup +if
    history-list begin dup while
      >r 2dup r@ hitem->text count s= if 2drop r> true exit endif
    r> hitem-next@ repeat
    drop
  endif
  2drop false
;

: history-find-part-from  ( addr count itemaddr -- itemaddr true // false )
  over +if
    begin dup while
      >r 2dup r@ hitem->text count 2swap search nrot 2drop if 2drop r> true exit endif
    r> hitem-next@ repeat
  endif
  drop 2drop false
;

: history-find-part  ( addr count -- itemaddr true // false )
  history-curr if
    2dup history-curr history-find-part-from if nrot 2drop true exit endif
  endif
  history-list history-find-part-from
;


: history-search-reset  ( -- )
  history-search-buf ?dup if 0! endif
;

: history-search-add-char  ( ch -- okflag )
  dup printable? ifnot drop false exit endif
  history-search-buf dup ifnot  ;; allocate buffer
    drop 4096 os:prot-r/w os:mmap ifnot 2drop false exit endif
    dup dup to history-search-buf 0!
  endif
  dup count-only 4090 > if 2drop false exit endif
  c4s:cat-char true
;

: history-search-del-char  ( -- successflag )
  history-search-buf dup ifnot exit endif
  dup @ 1-  dup -if 2drop false exit endif
  swap ! true
;


;; extract (remove) item from the list
: history-extract  ( itemaddr -- itemaddr )  >r
  ;; h->prev->next = next
  ;; h->next->prev = prev
  r@ hitem-next@ r@ hitem-prev@ ?dup ifnot to history-list else hitem-next! endif
  r@ hitem-prev@ r@ hitem-next@ hitem-prev!
  r@ 2 cells erase r>
;

: history-append  ( itemaddr -- )
  >r history-list r@ hitem-next!
  0 r@ hitem-prev!
  history-list ?dup if r@ swap hitem-prev! endif
  r> to history-list
;


: history-add  ( addr count -- )
  dup +if
    2dup history-find-whole if
      ;; move old item to the top
      nrot 2drop history-extract
    else
      ;; append a new item
      dup hitem-#header + brk-alloc dup >r hitem->text c4s:copy-counted r>
    endif
    history-append
  else 2drop endif
;


: (copy-to-editline)  ( addr count -- )
  dirty maxlen min 0 max 0 to leftpos
  dup to len dup to curpos addr swap move
;

: history-copy-curr  ( -- )
  dirty history-curr ?dup if hitem->text count (copy-to-editline)
  else history-saved ?dup if count (copy-to-editline)
  else 0 to curpos 0 to leftpos 0 to len
  endif endif
;

: history-need-save-curr?  ( -- flag )
  history-saved 0= history-curr 0= or if true exit endif
  history-curr 2 +cells count addr len s= not
;

;; if current line is not equal to history-curr, save it
: history-save-curr  ( -- )
  history-need-save-curr? ifnot exit endif
  history-saved if
    len history-#saved > if  ;; realloc temp buffer
      history-saved history-#saved cell+ len cell+ os:mremap ifnot drop exit endif
      to history-saved len to history-#saved
    endif
  else  ;; allocate temp buffer
    len cell+ os:prot-r/w os:mmap ifnot drop exit endif
    to history-saved len to history-#saved
  endif
  addr len history-saved c4s:copy-counted
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: edit-cmd  ( keycode -- )  \ cmdword
  , -find-required ,
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: cmd-invalid  ( keycode -- )  drop forth:(stdtty-bell) ;

: cmd-enter  ( -- ) exit-accept to exit-code ;
: cmd-escape  ( -- ) 0 to ctrlc-counter escape-allowed? if exit-escape to exit-code endif ;

: cmd-left  ( -- )  curpos 1- 0 max to curpos ;
: cmd-right  ( -- )  curpos 1+ len min to curpos ;
: cmd-home  ( -- )  0 to curpos ;
: cmd-end  ( -- )  len to curpos ;

: cmd-delete  ( -- ) 0 to ctrlc-counter delchar ;
: cmd-backspace  ( -- )  0 to ctrlc-counter curpos if 1 -to curpos delchar endif ;

: cmd-clear-line  ( -- )  0 to ctrlc-counter 0 to curpos 0 to leftpos 0 to len dirty ;

: cmd-ctrl-c  ( -- )
  len if cmd-clear-line  0 to ctrlc-counter
  else abort-allowed? if 1 +to ctrlc-counter ctrlc-counter 1 > if exit-abort to exit-code endif
  endif endif
;


: cmd-word-left  ( -- )
  -1 0 peekchar-ex bl <= if  ;; left is space, go to the end of the previous word
    begin -1 127 peekchar-ex bl <= while 1 -to curpos repeat
  else  ;; left is non-space, go to the word start
    begin -1 0 peekchar-ex bl > while 1 -to curpos repeat
  endif
;

: cmd-word-right  ( -- )
  0 0 peekchar-ex bl <= if  ;; at space, go to the start of the previous word
    begin 0 127 peekchar-ex bl <= while 1 +to curpos repeat
  else  ;; at non-space, go to the word end
    begin 0 0 peekchar-ex bl > while 1 +to curpos repeat
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: cmd-del-prev-word  ( -- )
  0 to ctrlc-counter
  curpos ifnot exit endif
  -1 0 peekchar-ex bl <= if  ;; left is space, go to the end of the previous word
    begin -1 127 peekchar-ex bl <= while cmd-backspace repeat
  else  ;; left is non-space, go to the word start
    begin -1 0 peekchar-ex bl > while cmd-backspace repeat
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: cmd-history-next  ( -- )
  history-list ifnot forth:(stdtty-bell) exit endif
  history-save-curr
  history-curr ?dup if hitem-next@ ?dup ifnot forth:(stdtty-bell) exit endif
  else history-list endif
  to history-curr history-copy-curr
;

: cmd-history-prev  ( -- )
  history-list 0= history-curr 0= or if forth:(stdtty-bell) exit endif
  history-save-curr
  history-curr hitem-prev@
  to history-curr history-copy-curr
;

: cmd-history-start  ( -- )
  history-list ?dup ifnot forth:(stdtty-bell) exit endif
;

: cmd-history-end  ( -- )
  history-list ifnot forth:(stdtty-bell) exit endif
  begin dup hitem-next@ while hitem-next@ repeat
  history-save-curr
  to history-curr history-copy-curr
;

: cmd-history-dump  ( -- )
  history-list ifnot forth:(stdtty-bell) exit endif
  endcr history-list begin ?dup while dup hitem->text count type cr hitem-next@ repeat
  dirty
;

: cmd-history-clear  ( -- )
  0 to history-curr 0 to history-list
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: cmd-other  ( keycode -- )
  dup printable? if inschar else cmd-invalid endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 value edit-commands-incr-search

: (incr-again)  ( -- ) edit-commands-incr-search to curr-cmd-table ;

: cmd-incr-stop  ( -- ) 0 to hilen dirty ;

: cmd-incr-search  ( -- )  0 to ctrlc-counter 0 to hilen history-search-reset dirty (incr-again) ;

: cmd-incr-clear  ( -- )  0 to hilen history-search-reset dirty (incr-again) ;

: cmd-incr-delchar  ( -- )
  history-search-del-char if
    hilen if
      history-search-buf dup if count nip endif to hilen
    endif
    dirty (incr-again)
  endif
;


;; the caller should perform "history-save-curr"!
: (cmd-incr-do-search)  ( -- successflag )
  history-search-buf count history-find-part if
    dup to history-curr
    hitem->text count history-search-buf count search if
      history-search-buf count-only to hilen
      drop history-curr hitem->text count drop - to histart
    else 0 to hilen endif
      \ endcr ." histart=" histart . ." hilen=" hilen . cr
    history-copy-curr true
  else forth:(stdtty-bell) false
  endif
;

: cmd-incr-next  ( -- )
  history-search-buf ifnot (incr-again) exit endif
  history-list ifnot forth:(stdtty-bell) (incr-again) exit endif
  history-curr >r  ;; we have to restore it if search failed
  ;; skip current history item (with wrapping)
  history-curr ?dup ifnot history-list
  else hitem-next@ ?dup ifnot history-list
  endif endif to history-curr
  (cmd-incr-do-search) if rdrop else r> to history-curr endif
  dirty (incr-again)
;

: cmd-other-incr  ( keycode -- )
  history-search-add-char if
    history-save-curr (cmd-incr-do-search) ifnot history-search-del-char endif
  endif
  (incr-again)
;

create (edit-commands-incr-search) here to edit-commands-incr-search
  K-^Y edit-cmd cmd-incr-clear
  K-^H edit-cmd cmd-incr-delchar
  K-BACKSPACE edit-cmd cmd-incr-delchar
  K-ESCAPE edit-cmd cmd-incr-stop
  K-ENTER edit-cmd cmd-incr-stop
  K-^C edit-cmd cmd-incr-stop
  K-^R edit-cmd cmd-incr-next

  0 edit-cmd cmd-other-incr  ( keycode -- )
create;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: cmd-clear-to-eol  ( -- )  curpos len < if curpos to len dirty endif ;


create edit-commands-ws
  K-^E edit-cmd cmd-clear-to-eol

  0 edit-cmd cmd-invalid  ( keycode -- )
create;

: cmd-ws-table  ( -- )  0 to ctrlc-counter edit-commands-ws to curr-cmd-table ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
create edit-commands-x
  K-^Y edit-cmd cmd-history-clear
  K-^H edit-cmd cmd-history-dump  \ debug
  K-^A edit-cmd cmd-history-start
  K-^E edit-cmd cmd-history-end

  0 edit-cmd cmd-invalid  ( keycode -- )
create;

: cmd-ext-table  ( -- )  0 to ctrlc-counter edit-commands-x to curr-cmd-table ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ TODO: make this extendable
create edit-commands
  K-LEFT edit-cmd cmd-left
  K-RIGHT edit-cmd cmd-right

  K-HOME edit-cmd cmd-home
  K-END edit-cmd cmd-end
  K-^A edit-cmd cmd-home
  K-^E edit-cmd cmd-end

  K-UP edit-cmd cmd-history-next
  K-DOWN edit-cmd cmd-history-prev

  K-CTRL-MASK K-LEFT or edit-cmd cmd-word-left
  K-CTRL-MASK K-RIGHT or edit-cmd cmd-word-right

  K-DELETE edit-cmd cmd-delete
  K-BACKSPACE edit-cmd cmd-backspace

  K-RETURN edit-cmd cmd-enter
  K-ESCAPE edit-cmd cmd-escape
  K-^C edit-cmd cmd-ctrl-c

  \ K-TAB edit-cmd cmd-autocomplete

  K-^W edit-cmd cmd-del-prev-word
  \ K-^Y edit-cmd cmd-clear-line
  K-^R edit-cmd cmd-incr-search
  K-^X edit-cmd cmd-ext-table
  K-^K edit-cmd cmd-ws-table

  0 edit-cmd cmd-other  ( keycode -- )
create;

edit-commands to default-cmd-table


: edit-winch-smallscr-fix  ( doneflag -- doneflag )
  ?dup ifnot
    (x@) width? 4- > if false
    else 0 (x!)  width? to scrlen  true
    endif
  endif
;

: edit-winch-eos-fix  ( doneflag -- doneflag )
  ?dup ifnot
    (x@) scrlen + last-tty-width >= ifnot false
    else width? (x@) - to scrlen  true
    endif
  endif
;

: edit-winch-width-fix  ( doneflag -- doneflag )
  ?dup ifnot
    (x@) scrlen + width? <= if false
    else width? (x@) - to scrlen  true
    endif
  endif
;

: edit-winch  ( -- )
  false edit-winch-smallscr-fix edit-winch-eos-fix edit-winch-width-fix drop
  width? to last-tty-width
  (y@) 0 height? 1- 0 max clamp (y!)
  dirty
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: run-command  ( keycode -- )
  dup K-WINCH = if drop edit-winch exit endif  ;; sorry for this hack
  curr-cmd-table  edit-commands to curr-cmd-table  ;; restore command table
  begin dup @ while
    2dup @ = if nip cell+ @execute-tail endif
    2 +cells
  repeat
  0 to ctrlc-counter
  cell+ @execute-tail
;

: edit-loop  ( -- )
  width? to last-tty-width
  0 to ctrlc-counter  0 to exit-code
  saved-tio get-tio-mode raw set-mode 2drop
  disable-autowrap
  dirty begin draw  -1 wait-ekey run-command  exit-code until
  enable-autowrap
  saved-tio set-tio-mode drop
  exit-code +if addr len history-add endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; call this *after* scr-init
: init  ( addr maxlen len -- )
  to len to maxlen to addr
  0 to len 0 to leftpos 0 to curpos
  cmd-end
  0 to history-curr 0 to hilen
  history-saved ?dup if 0! endif
  default-cmd-table to curr-cmd-table
  history-search-reset
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -1 means "buffer overflow"
0 value (prev-accept)
true value (accept-disabled)

: (edit-accept-usable?)  ( -- flag )
  false (accept-disabled) ifnot
    stdin-fd fd-tty? stdout-fd fd-tty? and if
      drop get-dims 4 >= swap 16 >= and
    endif
  endif
;

: (edit-accept)  ( addr maxlen fromrefill -- readlen // -1 )
  (edit-accept-usable?) ifnot (prev-accept) execute-tail endif
  over 0<= if drop 2drop -1 exit endif
  0 (x!) get-dims 1- 0 max (y!) to scrlen
  if  ;; from refill
    atxy draw-reset-attrs " \x1b[G\x1b[1;4m>" forth:(stdtty-xtype) draw-reset-attrs
    1 (x!) 1 -to scrlen
  endif
  0 init
  abort-allowed? escape-allowed? 2>r
  false to escape-allowed? true to abort-allowed?
  ['] tty:editor:edit-loop catch if 1 n-bye endif
  2r> to escape-allowed? to abort-allowed?
  " \r\x1b[0m\x1b[K\x1b[33;1m>\x1b[0m" forth:(stdtty-xtype) addr len type cr
  exit-code exit-abort = if ." USER ABORT!\n" 1 n-bye endif
  len
;


: install-editor  ( -- )
  (prev-accept) ifnot
    defer-@ (accept) to (prev-accept)
    ['] (edit-accept) to (accept)
  endif
  false to (accept-disabled)
;

: uninstall-editor  ( -- )
  (prev-accept) if
    defer-@ (accept) ['] (edit-accept) = if
      (prev-accept) to (accept)
      0 to (prev-accept)
    endif
  endif
  true to (accept-disabled)
;

install-editor


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
..: forth:(startup-init)  ( -- )  (history-init) ;..


previous previous definitions
