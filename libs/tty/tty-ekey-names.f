;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; THIS IS NOT MULTITHREAD-SAFE!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also tty definitions


: (k-find-name)  ( code -- addr count true // false )
  0xffff and vocid: tty [:  ( nfa -- stopflag )
    dup nfa->cfa word-type? word-type-const = if
      2dup nfa->cfa cfa->pfa @ = if
        id-count over w@ 0x2d4b = over 2 > and  ;; is this a "K-*" const?
        dup ifnot nrot 2drop else >r rot drop 2 /string r> endif
        exit
      endif
    endif
    drop false
  ;] foreach-word
  dup ifnot nip endif
;

;; uses PAD
: ekey>name  ( code -- addr count )
  pad 0!
  dup K-CTRL-MASK and if " C-" pad c4s:cat-counted endif
  dup K-ALT-MASK and if " M-" pad c4s:cat-counted endif
  dup K-SHIFT-MASK and if " S-" pad c4s:cat-counted endif
  dup K-HYPER-MASK and if " H-" pad c4s:cat-counted endif
  dup (k-find-name) ifnot
    dup 0xffff and bl 128 within over 0xffff and 129 256 within or if
      dup [ K-ALT-MASK K-CTRL-MASK or ] literal and ifnot
        dup locase-char over 0xffff and <> if dup K-SHIFT-MASK and ifnot " S-" pad c4s:cat-counted endif endif
      endif
      upcase-char pad c4s:cat-char pad count exit
    else " 0x" pad c4s:cat-counted base @ >r hex <#u # # # # #> r> base !
    endif
  else rot drop
  endif
  pad c4s:cat-counted pad count
;


previous definitions
