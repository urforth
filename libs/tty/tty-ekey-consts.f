;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; THIS IS NOT MULTITHREAD-SAFE!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] tty " tty-low.f" tload [ENDIF]

also tty definitions

-1 constant K-NONE

0xffff constant K-UNKNOWN

0x1001 constant K-F1
0x1002 constant K-F2
0x1003 constant K-F3
0x1004 constant K-F4
0x1005 constant K-F5
0x1006 constant K-F6
0x1007 constant K-F7
0x1008 constant K-F8
0x1009 constant K-F9
0x100a constant K-F10
0x100b constant K-F11
0x100c constant K-F12

0x1010 constant K-INSERT
0x1011 constant K-DELETE

0x1020 constant K-HOME
0x1021 constant K-END

0x1030 constant K-UP
0x1031 constant K-DOWN
0x1032 constant K-RIGHT
0x1033 constant K-LEFT

0x1040 constant K-PRIOR
0x1041 constant K-NEXT

27 constant K-ESCAPE
127 constant K-BACKSPACE  \ also 8, ^H
9 constant K-TAB
13 constant K-ENTER
bl constant K-SPACE
alias K-ENTER K-RETURN

0x1080 constant K-PAD5  ;; xterm can return this

;; various events
0x2000 constant K-FOCUS-IN
0x2001 constant K-FOCUS-OUT
0x2002 constant K-WINCH  ;; tty size changed (you have to install signal handler)
0x2003 constant K-PASTE-START
0x2004 constant K-PASTE-END

0x3000 constant K-M-LEFT-DOWN
0x3001 constant K-M-LEFT-UP
0x3002 constant K-M-LEFT-MOTION
0x3010 constant K-M-MIDDLE-DOWN
0x3011 constant K-M-MIDDLE-UP
0x3012 constant K-M-MIDDLE-MOTION
0x3020 constant K-M-RIGHT-DOWN
0x3021 constant K-M-RIGHT-UP
0x3022 constant K-M-RIGHT-MOTION

0x3030 constant K-M-WHEEL-UP
0x3031 constant K-M-WHEEL-DOWN

0x3040 constant K-M-MOTION  ;; mouse motion without buttons, not read from tty by now, but can be useful for other backends

;; synthesized events, used in tui
0x3050 constant K-M-LEFT-CLICK
0x3051 constant K-M-MIDDLE-CLICK
0x3052 constant K-M-RIGHT-CLICK

0x3060 constant K-M-LEFT-DOUBLE
0x3061 constant K-M-MIDDLE-DOUBLE
0x3062 constant K-M-RIGHT-DOUBLE

;; modifier masks
0x0100_0000 constant K-CTRL-MASK
0x0200_0000 constant K-ALT-MASK
0x0400_0000 constant K-SHIFT-MASK
0x0800_0000 constant K-HYPER-MASK  ;; not implemented yet

K-CTRL-MASK [char] A or constant K-^A
K-CTRL-MASK [char] B or constant K-^B
K-CTRL-MASK [char] C or constant K-^C
K-CTRL-MASK [char] D or constant K-^D
K-CTRL-MASK [char] E or constant K-^E
K-CTRL-MASK [char] F or constant K-^F
K-CTRL-MASK [char] G or constant K-^G
K-CTRL-MASK [char] H or constant K-^H  ;; this is also CTRL+BACKSPACE
K-CTRL-MASK [char] I or constant K-^I  ;; this it K-TAB, never returned
K-CTRL-MASK [char] J or constant K-^J  ;; this is K-ENTER, never returned
K-CTRL-MASK [char] K or constant K-^K
K-CTRL-MASK [char] L or constant K-^L
K-CTRL-MASK [char] M or constant K-^M
K-CTRL-MASK [char] N or constant K-^N
K-CTRL-MASK [char] O or constant K-^O
K-CTRL-MASK [char] P or constant K-^P
K-CTRL-MASK [char] Q or constant K-^Q
K-CTRL-MASK [char] R or constant K-^R
K-CTRL-MASK [char] S or constant K-^S
K-CTRL-MASK [char] T or constant K-^T
K-CTRL-MASK [char] U or constant K-^U
K-CTRL-MASK [char] V or constant K-^V
K-CTRL-MASK [char] W or constant K-^W
K-CTRL-MASK [char] X or constant K-^X
K-CTRL-MASK [char] Y or constant K-^Y
K-CTRL-MASK [char] Z or constant K-^Z

previous definitions
