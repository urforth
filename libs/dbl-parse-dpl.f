;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; augment compiler with double numbers
;; doesn't support base prefixes, because it conflicts with DPL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

-1 uservar DPL

;; convert a character string left at addr to a signed number, using the current numeric base
: NUMBER-DBL-DPL  ( addr count -- ud 1 // d -1 // addr count false )
  ;; check length
  dup 0> ifnot false exit endif
  2dup 2>r  ;; for failure exit
  ;; ok, we have at least one char; check for a sign
  over c@ case
    [char] - of /char true endof
    [char] + of /char false endof
    otherwise drop false
  endcase nrot  ;; ( negflag addr count )
  ;; should have at least one char to work with
  dup 0> ifnot 2drop drop 2r> false exit endif
  0 u>d 2swap >number  ;; ( negflag ud addr count )
  dup ifnot  ;; no more chars
    2drop drop  ;; drop str, and convert double to single
    swap if negate endif
    -1 dpl !  2rdrop true
  else  ;; have some more chars
    over c@ [char] . = ifnot 2drop 2drop 2r> false exit endif
    /char dup >r >number  ;; ( negflag ud addr count | oldcount )
    if rdrop 2drop 2drop 2r> false exit endif  ;; too many chars
    r> dpl ! 2rdrop drop rot if dnegate endif
    -1
  endif
;

..: interpret-not-found  ( addr count -- true // addr count false )
  number-dbl-dpl dup if
    state @ if
      -if swap [compile] literal endif [compile] literal
    else drop endif
    true exit
  endif
;..
