;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
true constant ANS-CRAP

(*
;; ANS morons
alias 1+ CHAR+  ( count -- count+1 )
alias 1- CHAR-  ( count -- count-1 )
;; they are immediate because they're doing nothing at all
: CHARS  ( count -- count )  ; immediate
: ALIGN  ( -- )  ; immediate
: ALIGNED  ( addr -- addr )  ; immediate

alias bitnot invert
alias CFA->PFA >BODY
*)

;; as counted string
: LITCSTR-C  ( -- addr )
  r> dup bcount + 3 or 1+ >r  ;; skip string (with trailing zero)
; (arg-c1strz) (hidden)


: CSLITERAL-C  ( addr count -- )
  state @ if
    ['] litcstr-c compiler:custom-c1sliteral,
  else
    dup 0 255 bounds? not ERR-STRING-TOO-LONG ?error
    over pad u< if
      pad
    else
      over pad = if
        pad 256 +
      else
        over 1- c! 1-
        exit
      endif
    endif
    dup >r c1s:copy-counted r>
  endif
; immediate (hidden)

: C"  ( -- addr count )  \ word  ;; "
  34 (parse-and-unescape) [compile] csliteral-c
; immediate
