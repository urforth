;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
true constant ANS-FILE

OS:O-RDONLY constant R/O
OS:O-WRONLY constant W/O
OS:O-RDWR constant R/W


: open-file  ( addr count mode -- fid errcode )
  dup 0 2 bounds? ifnot 2drop drop -1 ERR-FILE-NOT-FOUND exit endif
  os:O-LARGEFILE or 0 os:open dup 0< if ERR-FILE-NOT-FOUND else 0 endif
;

: create-file  ( addr count mode -- fid errcode )
  dup 1 2 bounds? ifnot 2drop drop -1 ERR-FILE-NOT-FOUND exit endif
  [ os:o-create-flags-nomode os:O-LARGEFILE or ] literal or os:o-create-mode-normal os:open
  dup 0< if ERR-FILE-NOT-FOUND else 0 endif
;

: close-file  ( fid -- errcode )
  os:close 0< if ERR-FILE-NOT-FOUND else 0 endif
;

: file-position  ( fid -- ud errcode )
  >r 0 0 os:seek-cur r> os:llseek 0< if ERR-FILE-NOT-FOUND else 0 endif
;

: file-size  ( fid -- ud errcode )
  >r
  0 0 os:seek-cur r@ os:llseek 0< if rdrop ERR-FILE-NOT-FOUND exit endif
  ;; ( currposlo currposhi | fid )
  0 0 os:seek-end r@ os:llseek 0< if rdrop 2drop ERR-FILE-NOT-FOUND exit endif
  ;; ( currposlo currposhi endlo endhi | fid )
  2swap os:seek-set r> os:llseek nrot 2drop 0< if ERR-FILE-NOT-FOUND else 0 endif
;

: reposition-file  ( ud fid -- errcode )
  os:seek-set swap os:llseek nrot 2drop 0< if ERR-FILE-NOT-FOUND else 0 endif
;

: read-file  ( addr count fid -- rdcount errcode )
  os:read dup 0< if ERR-FILE-READ-ERROR else 0 endif
;

: (read-file-char)  ( fid -- ch 1/0 errcode )
  >r 0 sp@ 1 r> read-file
;

;; this is very slow, but i don't care for now
: read-line  ( addr count fid -- rdcount not-eofflag errcode )
  over 0<= if drop 2drop 0 0 ERR-FILE-READ-ERROR exit endif
  >r 0
  begin
    ;; ( addr count rdcount | fid )
    2dup >
  while
    r@ (read-file-char) ?dup if rdrop >r 2drop nrot 2drop r> exit endif
    ;; ( addr count rdcount ch crdcount | fid )
    ifnot rdrop drop nrot 2drop dup 0<> 0 exit endif ;; eof
    dup nl = if rdrop drop nrot 2drop true 0 exit endif ;; eol
    swap >r >r over r> swap c! /char r> 1+
  repeat
  nrot 2drop rdrop true 0
;

: write-file  ( addr count fid -- errcode )
  over 0< if 2drop drop ERR-FILE-WRITE-ERROR exit endif
  >r
  begin
    dup
  while
    2dup r@ os:write dup 0< if rdrop drop 2drop ERR-FILE-WRITE-ERROR exit endif
    dup >r - swap r> + swap
  repeat
  rdrop 2drop 0
;

: write-line  ( addr count fid -- errcode )
  dup >r write-file
  ?dup if rdrop exit endif
  " \n" r> write-file
;

: flush-file  ( fid -- errcode )
  os:fsync-data 0< if ERR-FILE-WRITE-ERROR else 0 endif
;

: resize-file  ( ud fid -- errcode )
  os:ftrunc64 0< if ERR-FILE-WRITE-ERROR else 0 endif
;

: delete-file  ( addr count -- errcode )
  os:unlink 0< if ERR-FILE-WRITE-ERROR else 0 endif
;

: rename-file  ( addrold countold addrnew countnew -- errcode )
  os:rename 0< if ERR-FILE-WRITE-ERROR else 0 endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mmap extension

: open-file-mmap  ( addr count mode -- addr size fd 0 // errcode )
  dup >r open-file ?dup if rdrop nip exit endif
  r> case  ;; it is guaranteed to be valid
    r/o of os:prot-read endof
    w/o of os:prot-write endof
    r/w of os:prot-r/w endof
  endcase swap >r  ;; ( prot | fd )
  ;; check size
  r@ file-size ?dup if r> close-file drop nrot 2drop nip exit endif
  over 0x3fff_ffff u> or if 2drop r> close-file drop ERR-FILE-TOO-BIG exit endif  ;; ( prot size | fd )
  dup rot 0 r@ os:mmap-fd ifnot r> close-file drop nip exit endif
  swap r> 0
;

: close-file-mmap  ( addr size fd -- errcode )
  >r os:munmap  r> close-file drop
;
