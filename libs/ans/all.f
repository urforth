;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] forth:ANS-COMPAT
only forth definitions

true constant ANS-COMPAT

warning-redefine @
warning-redefine 0!
require ANS-ABORT    abort.f
require ANS-CRAP     crap.f
require ANS-WORDLIST wordlist.f
require ANS-VARIABLE vardefer.f
require ANS-NUMBER   number.f
require ANS-FILE     file.f
require ANS-TTY      tty.f
warning-redefine !

[ENDIF]