;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
true constant ANS-TTY

: .ansi-escape  ( -- )  " \x1b[" xtype ; (hidden)

;; print decimal number, max 4 digits
: (ansi-digit)  ( n wasnum divisor -- n%divisor wasnum )
  swap >r
  2dup / dup r@ or if 48 + xemit rdrop true >r else drop endif
  mod r>
; (hidden)

: ansi-num.  ( n -- )
  0 9999 clamp
  false  ;; wasnum
  1000 (ansi-digit) 100 (ansi-digit) 10 (ansi-digit) drop 48 + xemit
; (hidden)


: at-xy  ( x y -- )
  .ansi-escape
  1+ 1 max ansi-num. [char] ; xemit
  1+ 1 max ansi-num. [char] H xemit
;

: page " \x1b[2J\x1b[H" xtype ;

: ms ( msecs -- ) os:mssleep ;
