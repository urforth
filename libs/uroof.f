;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; slightly more complicated OO system than mini-oof
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(*
  internal class layout (somewhat confusingly called VMT):
    typeid-class   (just a typeid signature)
    parent-class   (ptr, can be 0)
    vocid          (vocabulary for this class)
    instance-size  (bytes)
    vmt-size       (bytes)
  then vmt data follows (so default vmt-size is 16)
  first field of each instance is classptr (so default instance-size is 4)
  VMT holds both method implementation pointers, and class vars.
  class VOCID may contain point to class word header -- it can be used to get class name.

  class vocabulary contains all class variables and methods. pfa contains:
    offset
    typeid

  interpreter is augmented via "interpret-wfind"
*)

vocabulary oop
also oop definitions

vocabulary oop-internal

;; various typeids
enum{
  666_666 set

  value typeid-class

  value typeid-method
  value typeid-variable
  value typeid-field

  value typeid-class-method
  value typeid-class-variable
  value typeid-class-field

  value typeid-value
}

5 cells constant (class-header-size)

;; used when defining a new class
0 value (current-parent)  ;; saved class parent (we need it in "end-class:", but it is specified in "oop:class")
0 var (current-vmtsize)   ;; VMT size -- VMT is an internal class representation (see above)
0 var (current-instsize)  ;; instance (fields) size
0 value (current-vocid)   ;; class vocabulary

;; used when compiling a class method
0 value (current-self)         ;; classid (VMT ptr); 0 means "not compiling a method"
0 value (current-mtofs)        ;; offset of the current method
false value (current-classmt)  ;; is current method a class method?


;; cleanup after class/method definition
: (cleanup-def)  ( -- )
  0 to (current-parent)
  (current-vmtsize) 0!
  (current-instsize) 0!
  0 to (current-vocid)
  0 to (current-self)
  0 to (current-mtofs)
  false to (current-classmt)
;

: defining-class?   ( -- flag ) (current-vocid) notnot ;
: defining-method?  ( -- flag ) (current-self) notnot ;

..: forth:(abort-cleanup)  ( -- )  0 (self!) (cleanup-def) ;..
..: forth:(exc0!)  ( -- )  0 (self!) (cleanup-def) ;..
..: forth:(catch-saver)  ( -- )
  r> (self@) >r [: ( restoreflag ) ( self )  r> swap if r> (self!) else rdrop endif >r ;] >r >r
;.. (hidden)


;; accessors
: (class->typeid)    ( class -- addr ) ; immediate
: (class->parent)    ( class -- addr ) cell+ ;
: (class->vocid)     ( class -- addr ) 2 +cells ;
: (class->instsize)  ( class -- addr ) 3 +cells ;
: (class->vmtsize)   ( class -- addr ) 4 +cells ;

: (class-typeid@)    ( class -- addr ) (class->typeid) @ ;
: (class-parent@)    ( class -- addr ) (class->parent) @ ;
: (class-vocid@)     ( class -- addr ) (class->vocid) @ ;
: (class-instsize@)  ( class -- addr ) (class->instsize) @ ;
: (class-vmtsize@)   ( class -- addr ) (class->vmtsize) @ ;
: (class-name@)      ( class -- addr count ) (class->vocid) @ vocid-headnfa@ dup if id-count else drop NullString endif ;


;; check if the given vocid looks like a valid class vocabulary
: class?  ( classid -- flag )  dup if (class-typeid@) typeid-class = endif ;

;; convert instance to a classid; can be called with instance, classid or 0
: inst->class  ( inst -- clsid//0 )  dup if dup class? ifnot @ endif endif ;

;; is subclassid a child of superclassid? accepts classid, instance or 0
: child-of?  ( subclassid superclassid -- flag )
  inst->class swap inst->class
  begin dup while 2dup = if 2drop true exit endif (class-parent@) repeat
  2drop false
;

;; get class name; accepts classid, instance or 0
: class-name  ( classid -- addr count )  inst->class dup class? if (class-name@) else drop NullString endif ;


;; find name in vocabulary
: (xfind-vocid)  ( addr count voicid -- pfaaddr immflag // addr count false )
  [ (wflag-smudge) (wflag-vocab) or (wflag-codeblock) or (wflag-immediate) or ] literal
  forth:(voc-search-with-mask) dup if swap cfa->pfa swap endif
;

;; get typeid from pfa address
: (xfindres-typeid@)  ( pfaaddr -- typeid )  dup if cell+ @ endif ; (hidden)

;; find name in the given class or in parents
: (xfind-in-class)  ( addr count classid -- pfaaddr typeid true // false )
  inst->class begin ?dup while
    dup >r (class-vocid@) (xfind-vocid) if rdrop dup (xfindres-typeid@) true exit endif
    r> (class-parent@)
  repeat
  2drop false
;

;; find name in the given class or in parents, return vmtofs and typeid
: (xfind-in-class-vofs)  ( addr count classid -- vofs typeid true // false )
  (xfind-in-class) if swap @ swap true else false endif
;

;; find name with the given typeid in the given class or in parents
: (find-typed)  ( addr count classid type -- vmtofs/instofs true // false )
  >r (xfind-in-class-vofs) if r> = ifnot drop false else true endif
  else rdrop false endif
;

: find-method  ( addr count classid -- vmtofs true // false )  typeid-method (find-typed) ;
: find-class-method  ( addr count classid -- vmtofs true // false )  typeid-class-method (find-typed) ;


: has-super-method-by-vmt?  ( vmtofs classid -- typeid true // false )
  inst->class begin dup while
    dup (class-vocid@) [:  ( vmtofs classid nfa -- vmtofs classid false // vmtofs classid typeid true )
      dup nfa->ffa ffa@ [ (wflag-smudge) (wflag-hidden) or ] literal and if
        drop false
      else
        nfa->cfa cfa->pfa dup cell+ @ dup typeid-class-method = swap typeid-method = or if
          ( vmtofs classid pfa )
          dup @ 2over drop = if cell+ @ true else drop false endif
        else drop false endif
      endif
    ;] foreach-word if nrot 2drop true exit endif
  (class-parent@) repeat
  2drop false
;

;; find name in current class or in parents
: (xfind)  ( addr count -- addr true // false )
  (current-vocid) if (current-vocid) (xfind-vocid) if true exit endif endif
  (current-parent) (xfind-in-class) dup if nip endif
;

: (create-typed-str)  ( addr count ofsptr type -- )
  2over (xfind) if drop 2drop <abort " duplicate class field \`" abort-type abort-type 34 abort-emit abort> endif
  2swap (current-vocid) create-named-in over @ , , cell swap +!
;


: (var-str)  ( addr count -- )  (current-instsize) typeid-variable (create-typed-str) ;
: (field-str)  ( addr count -- )  (current-instsize) typeid-field (create-typed-str) ;
: (method-str)  ( addr count -- )  (current-vmtsize) typeid-method (create-typed-str) ;
: (class-var-str)  ( addr count -- )  (current-vmtsize) typeid-class-variable (create-typed-str) ;
: (class-field-str)  ( addr count -- )  (current-vmtsize) typeid-class-field (create-typed-str) ;
: (class-method-str)  ( addr count -- )  (current-vmtsize) typeid-class-method (create-typed-str) ;


;; start defining a new class
: class  ( parentclassid -- )  \ name
  defining-class? ?abort" cannot create nested class"
  defining-method? ?abort" cannot create class in method definition"
  dup if
    dup class? not-?abort" parent is not a class"
    dup (class-vmtsize@) over (class-instsize@)
  else
    (class-header-size) 0
  endif
  (current-instsize) ! (current-vmtsize) !
  forth:wordlist to (current-vocid)  ;; create anonymous wordlist
  dup to (current-parent)
  ;; create default field for empty class
  ifnot " my-class" (var-str) endif
  also oop-internal
;


: (compile-var-addr) ( instofs -- )
  compiler:?comp
  (current-classmt) ?abort" cannot use instance vars in class method"
  compile forth:(self@) [compile] literal compile +
; (hidden)

: (compile-var-xto) ( instofs cfa -- )  swap (compile-var-addr) ?compile, ; (hidden)

: (compile-method)  ( vmtofs -- )
  compiler:?comp
  (current-classmt) ?abort" cannot use instance methods in class method"
  compile forth:(self@) compile @ [compile] literal compile + compile @execute
; (hidden)


;; in class method, `self` is classid
: (compile-class-var-addr) ( instofs -- )
  compiler:?comp
  compile forth:(self@)
  (current-classmt) ifnot compile @ endif
  [compile] literal compile +
; (hidden)

: (compile-class-var-xto) ( instofs cfa -- )  swap (compile-class-var-addr) ?compile, ; (hidden)

;; in class method, `self` is classid
: (compile-class-method)  ( vmtofs -- )
  compiler:?comp
  compile forth:(self@)
  (current-classmt) ifnot compile @ compile (self@) compile >r compile dup compile (self!) endif
  [compile] literal compile + compile @execute
  (current-classmt) ifnot compile r> compile (self!) endif
; (hidden)


: compile-something  ( vofs typeid -- )
  compiler:?comp case
    typeid-method of (compile-method) endof
    typeid-field of ['] @ (compile-var-xto) endof
    typeid-variable of (compile-var-addr) endof
    typeid-class-method of (compile-class-method) endof
    typeid-class-field of ['] @ (compile-class-var-xto) endof
    typeid-class-variable of (compile-class-var-addr) endof
    abort" wut?!"
  endcase
;

: compile-xto  ( vofs typeid cfa -- )
  swap compiler:?comp case
    typeid-field of (compile-var-xto) endof
    typeid-class-field of (compile-class-var-xto) endof
    typeid-variable of abort" cannot assign value to var" endof
    typeid-method of abort" cannot assign value to method" endof
    typeid-class-variable of abort" cannot assign value to class var" endof
    typeid-class-method of abort" cannot assign value to class method" endof
    abort" wut?!"
  endcase
;

;; zero instance data, set classid
: init-instance  ( classid instaddr -- instaddr )
  2dup swap (class-instsize@) erase
  dup nrot !
;

;; allot new instance of the given class
: new-allot  ( classid -- addr )
  dup class? not-?abort" not a class"
  dup (class-instsize@) n-allot init-instance
;


;; compiled into method code
: (child-of-check) ( subclassid superclassid -- subclassid )
  over swap child-of? not-?abort" class/instance access violation"
;


: (invoke-inst-check)  ( instance classid vmtofs -- vmtofs instance )
  nrot dup class? not-?abort" invalid invoke (bad class)"
  2dup child-of? not-?abort" invalid invoke (bad class)"
  drop dup class? ?abort" cannot invoke instance in class method"
;

: (invoke-class-check)  ( inst/clsid classid vmtofs -- vmtofs clsid )
  nrot swap inst->class dup class? not-?abort" invalid class invoke (bad class)"
  over child-of? not-?abort" invalid class invoke (bad class)"
;

;; invoke instance method, common code
: (invoke-common)  ( vmtofs inst/clsid instance -- )  (self@) >r over (self!) if @ endif + @execute r> [execute-tail] (self!) ; (hidden)

;; invoke instance something
: (invoke-method)  ( instance classid vmtofs -- )  (invoke-inst-check) true [execute-tail] (invoke-common) ; (hidden)
: (invoke-var)  ( instance classid vmtofs -- )  (invoke-inst-check) [execute-tail] + ; (hidden)
: (invoke-field)  ( instance classid vmtofs -- )  (invoke-inst-check) + [execute-tail] @ ; (hidden)

;; invoke class something
: (invoke-cmethod)  ( inst/clsid classid vmtofs -- )  (invoke-class-check) false [execute-tail] (invoke-common) ; (hidden)
: (invoke-cvar)  ( inst/clsid classid vmtofs -- )  (invoke-class-check) [execute-tail] + ; (hidden)
: (invoke-cfield)  ( inst/clsid classid vmtofs -- )  (invoke-class-check) + [execute-tail] @ ; (hidden)

: (invoke-get-parentclass)  ( instance/class -- )  inst->class dup class? if (class-parent@) else drop 0 endif ; (hidden)


: (invoke-compiler-route)  ( -- )  \ wordname  word-to-route
  create smudge -find-required reladdr, smudge (hidden)
 does>  ( pfa )
  nip state @ if @ compile, else @execute-tail endif
;

(invoke-compiler-route) (invoke-compiler-class) inst->class
(invoke-compiler-route) (invoke-compiler-parent-class) (invoke-get-parentclass)
(invoke-compiler-route) (invoke-compiler-class-name) class-name

: (invoke-compiler-child-of?)  ( cid -- )  ( instance )  drop state @ if compile swap compile child-of? else swap [execute-tail] child-of? endif ; (hidden)
: (invoke-compiler-child-of:)  ( cid -- )  ( instance )  \ classname
  drop -find-required cfa->pfa dup class? not-?abort" not a class!"
  [compile] addrliteral ['] child-of? state @ if compile, else execute-tail endif
; (hidden)


;; special in-method words
nested-vocabulary (invoke-specials) also (invoke-specials) definitions
: self  ( inst/clsid cid -- )  drop ;
: class  ( inst/clsid cid -- )  (invoke-compiler-class) ;
: parent-class  ( inst/clsid cid -- )  (invoke-compiler-parent-class) ;
: child-of:  ( inst/clsid cid -- )  (invoke-compiler-child-of:) ;
: child-of?  ( inst/clsid cid -- )  (invoke-compiler-child-of?) ;
: class-name  ( inst/clsid cid -- )  (invoke-compiler-class-name) ;

: (other) ( inst/clsid cid addr count -- )  rot >r  ;; save cid
  r@ (xfind-in-class-vofs) not-?abort" methor/field name expected"
  r> nrot case  ;; ( cid vmtofs )
    typeid-method of ['] (invoke-method) endof
    typeid-field of ['] (invoke-field) endof
    typeid-variable of ['] (invoke-var) endof
    typeid-class-method of ['] (invoke-cmethod) endof
    typeid-class-field of ['] (invoke-cfield) endof
    typeid-class-variable of ['] (invoke-cvar) endof
    abort" wut?!"
  endcase
  state @ if >r swap [compile] addrliteral [compile] literal r> compile, else execute-tail endif
; (hidden)
previous definitions

: (invoke-compiler)  ( cid -- )  ( instance -- )  \ name
  inst->class dup class? not-?abort" not a class!"
  parse-name vocid: (invoke-specials) voc-search-noimm ifnot ['] (invoke-specials):(other) endif
  execute-tail
; (hidden)

;; obj ::invoke classname methodname -- dynamic binding
: ::invoke  ( instance -- )  \ classname methodname
  ' cfa->pfa [execute-tail] (invoke-compiler)
; immediate


;; worker word for string dispatch in the current instance
: (dispatch-str)  ( addr count -- ... )
  2dup (self@) inst->class (xfind-in-class-vofs) ifnot
    " (unknown-dispatch)" (self@) @ (xfind-in-class-vofs) ifnot
      <abort " cannot dispatch method \`" abort-type abort-type 34 abort-emit dispatch-abort>
    endif
  else 2swap 2drop endif
  case
    typeid-method of (self@) dup class? ?abort" cannot dispatch instance method in class" inst->class + @execute-tail endof
    typeid-field of (self@) dup class? ?abort" cannot dispatch instance field in class" + @ endof
    typeid-variable of (self@) dup class? ?abort" cannot dispatch instance variable in class" + endof
    typeid-class-method of (self@) inst->class + @execute-tail endof
    typeid-class-field of (self@) inst->class + @ endof
    typeid-class-variable of (self@) inst->class + endof
    abort" wut?!"
  endcase
; (hidden)

;; obj invoke name -- dynamic binding by name
: dispatch-str  ( instance addr count -- ... )
  (self@) >r rot (self!) (dispatch-str) r> [execute-tail] (self!)
;


: method:  ( classid -- )  \ name
  defining-class? ?abort" cannot define method while defining a class"
  defining-method? ?abort" cannot define method while defining a method"
  dup class? not-?abort" cannot define method for something that is not a class"
  dup parse-name 2dup 2>r rot oop:find-method ifnot
    ;; class method?
    dup 2r@ rot oop:find-class-method not-?abort" unknown method"
    true to (current-classmt)
  else false to (current-classmt) endif
  to oop:(current-mtofs) to oop:(current-self)
  \ :noname
  ;; make it named
  " (" pad c1s:copy-counted
  oop:(current-self) (class-name@) pad c1s:cat-counted
  [char] : pad c1s:cat-char
  2r> pad c1s:cat-counted [char] ) pad c1s:cat-char pad bcount (:noname-named)
  also oop:oop-internal
;

;; value that holds object instance
;; data: typeid inst clsid
: value  ( inst -- )  \ name classname
  create typeid-value , , smudge
  -find-required cfa->pfa dup class? not-?abort" not a class!" ,
  smudge immediate
 does>  ( pfa )
  dup @ typeid-value = not-?abort" instance value is corrupted"
  dup cell+ state @ if [compile] addrliteral compile @ 2 +cells @
  else @ dup not-?abort" cannot call method of null object" swap 2 +cells @ endif
  [execute-tail] (invoke-compiler)
;

: to  ( inst -- )  \ name
  -find-required cfa->pfa
  dup @ typeid-value = not-?abort" instance value expected"
  cell+ [compile] addrliteral ['] ! state @ if compile, else execute-tail endif
; immediate

: value@  ( -- inst )  \ name
  -find-required cfa->pfa
  dup @ typeid-value = not-?abort" instance value expected"
  cell+ @ [compile] addrliteral
; immediate


: (expect-defining-class)  ( -- )  defining-class? not-?abort" outside of class definition" ; (hidden)
: (expect-defining-method)  ( -- )  defining-method? not-?abort" outside of method definition" ; (hidden)


;; this vocabulary is used inside class/method definition
also oop-internal definitions

: end-class:  ( -- )  \ name
  oop:(expect-defining-class)
  previous
  parse-name 2dup 2>r create-named smudge
  here >r ;; will be used later
  typeid-class ,
  (current-parent) ,
  (current-vocid) ,
  (current-instsize) @ ,
  (current-vmtsize) @ ,
  (current-vmtsize) @ (class-header-size) - dup 0< ?abort" invalid class data size" n-allot
  ;; put class name (nope, we will use vocid field for that)
  ;; r> 2r@ rot >r dup 1+ n-allot c1s:copy-counted
  ;; copy vmt from the parent, leave new-vmt-addr on the stack
  (current-parent) if  >r
    (current-parent) (class-vmtsize@) (class-header-size) -
    (current-parent) (class-header-size) +
    over r@ swap 0 max cmove r> swap
  else (class-header-size) endif
  over (current-vmtsize) @ + nrot + ?do ['] forth:(notimpl) , cell +loop
  smudge create;
  latest-nfa (current-vocid) vocid-headnfa!
  (cleanup-def)
  ;; create "classname::" word too
  r> 2r> pad c4s:copy-counted " ::" pad c4s:cat-counted pad count create-named , immediate
 does> ( pfa )  ;; does invoke
  @ [execute-tail] oop:(invoke-compiler)
;

: var  ( -- )  (( name ))  oop:(expect-defining-class) parse-name (var-str) ;
: field  ( -- )  (( name ))  oop:(expect-defining-class) parse-name (field-str) ;
: method  ( -- )  (( name ))  oop:(expect-defining-class) parse-name (method-str) ;
: buffer:  ( size -- )  (( name ))  dup 1 < ?abort" invalid buffer size" oop:(expect-defining-class) parse-name (var-str) cell- (current-instsize) +! ;

: class-var  ( -- )  (( name ))  oop:(expect-defining-class) parse-name (class-var-str) ;
: class-field  ( -- )  (( name ))  oop:(expect-defining-class) parse-name (class-field-str) ;
: class-method  ( -- )  (( name ))  oop:(expect-defining-class) parse-name (class-method-str) ;


: self  ( -- )
  oop:(expect-defining-method) compiler:?comp
  compile (self@)
; immediate

: class-name  ( -- )
  oop:(expect-defining-method) compiler:?comp
  compile (self@) compile oop:class-name
; immediate

;; runtime vmt call by method name
: dispatch-str  ( addr count -- ... )
  oop:(expect-defining-method) compiler:?comp
  compile oop:(dispatch-str)
; immediate

: ::invoke  ( instance -- )  \ classname methodname
  oop:(expect-defining-method) compiler:?comp
  [compile] oop:::invoke
; immediate

: invoke  ( instance -- )  \ methodname
  oop:(expect-defining-method) compiler:?comp
  (current-self) oop:(invoke-compiler)
; immediate

;; does no sanity checks!
: var^  ( instance -- )  \ varname
  oop:(expect-defining-method) compiler:?comp
  parse-name (current-self) (xfind-in-class-vofs) not-?abort" var/field name expected"
  dup typeid-class-field = if drop typeid-class-variable endif
  dup typeid-class-variable = if compile inst->class drop typeid-variable endif
  dup typeid-field = if drop typeid-variable endif
  typeid-variable = not-?abort" cannot take address of something that is not a var/field"
  (current-self) [compile] addrliteral compile oop:(child-of-check)
  [compile] literal compile +
; immediate

;; call current inherited method
: call-super  ( -- )
  oop:(expect-defining-method) compiler:?comp
  (current-self) (class-parent@) dup not-?abort" no parent class"
  (current-mtofs) (current-self) (class-parent@) has-super-method-by-vmt? not-?abort" no supermethod found"
  (current-classmt) if typeid-class-method else typeid-method endif = not-?abort" invalid supermethod type"
  (current-mtofs) + [compile] addrliteral compile @execute
; immediate

;; call inherited method
: inherited  ( -- )  \ name
  oop:(expect-defining-method) compiler:?comp
  (current-self) (class-parent@) dup not-?abort" no parent class"
  (current-classmt) if
    dup parse-name rot find-class-method not-?abort" unknown method"
    + [compile] addrliteral compile @execute
  else
    dup parse-name 2dup 2>r rot find-method ifnot
      ;; class method?
      dup 2r> rot find-class-method not-?abort" unknown method"
      compile (self@) compile >r over [compile] addrliteral compile (self!)
      + [compile] addrliteral compile @execute
      compile r> compile (self!)
    else
      ;; instance method
      2rdrop + [compile] addrliteral compile @execute
    endif
  endif
; immediate

;; compiles class name of the defining method
: static-class-name  ( -- )
  oop:(expect-defining-method) compiler:?comp
  (current-self) (class-name@) [compile] sliteral
; immediate

;; compiles class ptr of the defining method
: static-class  ( -- )
  oop:(expect-defining-method) compiler:?comp
  (current-self) [compile] addrliteral
; immediate


vocabulary (interpret-specials) also (interpret-specials) definitions
;; all normal methods: ( -- addr count false // true )

\ : addr:  parse-name (current-self) (xfind-in-class-vofs) not-?abort" field/var name expected" 0 compile-xto true ;

: (other)  ( addr count -- addr count false // true )
  2dup (current-self) (xfind-in-class-vofs) dup if drop 2swap 2drop compile-something true endif
; (hidden)
previous definitions

..: value-xto-schook  ( value type addr count -- true // unchanged )
  state @ defining-method? logand if
    2dup (current-self) (xfind-in-class-vofs) if
      ( type addr count vofs typeid )
      2swap 2drop rot forth:(value-xto-type->cfa) compile-xto true exit
    endif
  endif
<;..

;; returns `1` if cfa is immediate, or `-1` if it is a normal word
;; scattered extensions should simply pass "addr count" unchanged (or changed ;-)
..: interpret-wfind  ( addr count -- cfa 1 // cfa -1 // addr count false )
  state @ defining-method? logand if
    vocid: (interpret-specials) ['] (interpret-specials):(other) (intepret-voc-call-helper) if exit endif
  endif
;..


;; redefine ";"
: ;
    \ endcr ." UROOF: semi: " latest-nfa id. cr
  oop:(expect-defining-method) compiler:?comp
  previous
  " ;" wfind dup not-?abort" semicolon not found"
  +if execute else compile, endif
  \ [compile] forth:;
  (current-self) (current-mtofs) + !  ;; put noname cfa to vmt
  (cleanup-def)
; immediate


previous previous definitions


alias oop:method: method:
alias oop:::invoke ::invoke
alias oop:dispatch-str ::dispatch-str
