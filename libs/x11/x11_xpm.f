;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adapted from bigForth by Bernd Paysan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ Xrender extension
use-libs: dynlib big-struct

[IFNDEF] libxpm
dynlib: libxpm libXpm.so.4
[ENDIF]


dynlib-import: libxpm

int XpmReadFileToPixmap( int , int , int , int , int , int ); ( dpy d filename pixmap_r shapemask_r attribs -- n )

end;


struct{
  cell valuemask             (* Specifies which attributes are defined *)
  ptr visual                 (* Specifies the visual to use *)
  ptr colormap               (* Specifies the colormap to use *)
  cell depth                 (* Specifies the depth *)
  cell width                 (* Returns the width of the created pixmap *)
  cell height                (* Returns the height of the created pixmap *)
  cell x_hotspot             (* Returns the x hotspot's coordinate *)
  cell y_hotspot             (* Returns the y hotspot's coordinate *)
  cell cpp                   (* Specifies the number of char per pixel *)
  ptr pixels                 (* List of used color pixels *)
  cell npixels               (* Number of pixels *)
  ptr colorsymbols           (* Array of color symbols to override *)
  cell numsymbols            (* Number of symbols *)
  ptr rgb_fname              (* RGB text file name *)
  cell nextensions           (* number of extensions *)
  ptr extensions             (* pointer to array of extensions *)
  cell ncolors               (* Number of colors *)
  ptr colorTable             (* Color table pointer *)
(* 3.2 backward compatibility code *)
  ptr hints_cmt              (* Comment of the hints section *)
  ptr colors_cmt             (* Comment of the colors section *)
  ptr pixels_cmt             (* Comment of the pixels section *)
(* end 3.2 bc *)
  cell mask_pixel            (* Transparent pixel's color table index *)
  (* Color Allocation Directives *)
  cell exactColors           (* Only use exact colors for visual *)
  cell closeness             (* Allowable RGB deviation *)
  cell red_closeness         (* Allowable red deviation *)
  cell green_closeness       (* Allowable green deviation *)
  cell blue_closeness        (* Allowable blue deviation *)
  cell color_key             (* Use colors from this color set *)
  ptr alloc_pixels           (* Returns the list of alloc'ed color pixels *)
  cell nalloc_pixels         (* Returns the number of alloc'ed color pixels *)
  cell alloc_close_colors    (* Specify whether close colors should be allocated using XAllocColor or not *)
  cell bitmap_format         (* Specify the format of 1bit depth images: ZPixmap or XYBitmap *)
  (* Color functions *)
  ptr alloc_color            (* Application color allocator *)
  ptr free_colors            (* Application color de-allocator *)
  ptr color_closure          (* Application private data to pass to alloc_color and free_colors *)
} XpmAttributes

(* XpmAttributes value masks bits *)
bitenum{
  value XpmVisual
  value XpmColormap
  value XpmDepth
  value XpmSize         (* width & height *)
  value XpmHotspot      (* x_hotspot & y_hotspot *)
  value XpmCharsPerPixel
  value XpmColorSymbols
  value XpmRgbFilename
(* 3.2 backward compatibility code *)
  value XpmInfos
  XpmInfos constant XpmReturnInfos
(* end 3.2 bc *)
  value XpmReturnPixels
  value XpmExtensions
  XpmExtensions constant XpmReturnExtensions

  value XpmExactColors
  value XpmCloseness
  value XpmRGBCloseness
  value XpmColorKey

  value XpmColorTable
  XpmColorTable constant XpmReturnColorTable

  value XpmReturnAllocPixels
  value XpmAllocCloseColors
  value XpmBitmapFormat

  value XpmAllocColor
  value XpmFreeColors
  value XpmColorClosure
}
