;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[IFNDEF] x11
vocabulary x11 also x11 definitions
" x11_const.f" tload
" x11_xlib.f" tload
" x11_xext.f" tload
" x11_xft.f" tload
" x11_xrender.f" tload
" x11_xpm.f" tload
\ " x11_opengl.f" tload
previous definitions
[ENDIF]
