;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adapted from bigForth by Bernd Paysan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: dynlib

[IFNDEF] libxext
dynlib: libxext libXext.so.6
[ENDIF]


dynlib-import: libxext

int XMITMiscGetBugMode( int );  ( dpy -- flag )
int XMITMiscQueryExtension( int , int , int );  ( event error dpy -- flag )
int XMITMiscSetBugMode( int , int );  ( onOff dpy -- Status )

int XMissingExtension( const char * , int );  ( ext_name dpy -- n )
int XSetExtensionErrorHandler( void * );  ( handler -- n )

int XShapeCombineMask( int , int , int , int , int , int , int );  ( op src y x dest_kind dest dpy -- r )
int XShapeCombineRectangles( int , int , int , int , int , int , int , int , int );  ( ordering op n rects y x dest_kind dest dpy -- r )
int XShapeCombineRegion( int , int , int , int , int , int , int );  ( op region y x dest_kind dest dpy -- r )
int XShapeCombineShape( int , int , int , int , int , int , int , int );  ( op src_kind src y x dest_kind dest dpy -- r )
int XShapeGetRectangles( void * , void * , int , int , int );  ( &ordering &count kind win dpy -- rects )
int XShapeInputSelected( int , int );  ( win dpy -- n )
int XShapeOffsetShape( int , int , int , int , int );  ( y x dest_kind dest dpy -- r )
int XShapeQueryExtension( int , int , int );  ( error event dpy -- flag )
int XShapeQueryExtents( void * , void * , void * , void * , int , void * , void * , void * , void * , int , int , int );  ( &hc &wc &yc &xc clip_shaped &h &w &y &x bounding-shaped win dpy -- Status )
int XShapeQueryVersion( void * , void * , int );  ( &minor &mayor dpy -- Status )
int XShapeSelectInput( int , int , int );  ( mask win dpy -- r )

int XShmAttach( int , int );  ( shminfo dpy -- status )
int XShmCreateImage( int , int , int , int , int , int , int , int );  ( h w shminfo data format depth vidual dpy -- ximage )
int XShmCreatePixmap( int , int , int , int , int , int , int );  ( depth h w shminfo data d dpy -- pixmap )
int XShmDetach( int , int );  ( shminfo dpy -- status )
int XShmGetEventBase( int );  ( dpy -- status )
int XShmGetImage( int , int , int , int , int , int );  ( plane_mask y x image d dpy -- status )
int XShmPixmapFormat( int );  ( dpy -- n )
int XShmPutImage( int , int , int , int , int , int , int , int , int , int , int );  ( send_e sh sw dy dx sy sx image gc d dpy -- status )
int XShmQueryExtension( int , int , int );  ( error event dpy -- flag )
int XShmQueryVersion( void * , void * , void * , int );  ( &sharedpm &minor &mayor dpy -- flag )

int XSyncAwait( int , int , int );  ( n_conds wait_list dpy -- status )
int XSyncChangeAlarm( void * , int , int , int );  ( &values mask alarm dpy -- status )
int XSyncChangeCounter( int , int , int );  ( value counter dpy -- status )
int XSyncCreateAlarm( int , int , int );  ( values mask dpy -- alarm )
int XSyncCreateCounter( int , int );  ( init dpy -- counter )
int XSyncDestroyAlarm( int , int );  ( alarm dpy -- status )
int XSyncDestroyCounter( int , int );  ( counter dpy -- status )
int XSyncFreeSystemCounterList( int );  ( list -- r )
int XSyncGetPriority( int , int , int );  ( priority client_rc_id dpy -- status )
int XSyncInitialize( void * , void * , int );  ( &minor &mayor dpy -- statis )
int XSyncIntToValue( int , int );  ( i pv -- r )
int XSyncIntsToValue( int , int , int );  ( h l pv -- r )
int XSyncListSystemCounters( void * , int );  ( &n dpy -- list )
int XSyncMaxValue( void * );  ( &v -- r )
int XSyncMinValue( void * );  ( &v -- r )
int XSyncQueryAlarm( void * , int , int );  ( &values alarm dpy -- status )
int XSyncQueryCounter( void * , int , int );  ( &value counter dpy -- status )
int XSyncQueryExtension( int , int , int );  ( eror event dpy -- status )
int XSyncSetCounter( int , int , int );  ( value counter dpy -- status )
int XSyncSetPriority( int , int , int );  ( prior client_rid dpy -- status )
int XSyncValueAdd( void * , int , int , void * );  ( &ov b a &r -- r )
int XSyncValueEqual( int , int );  ( b a -- flag )
int XSyncValueGreaterOrEqual( int , int );  ( b a -- flag )
int XSyncValueGreaterThan( int , int );  ( b a -- flag )
int XSyncValueHigh32( int );  ( v -- n )
int XSyncValueIsNegative( int );  ( v -- flag )
int XSyncValueIsPositive( int );  ( v -- flag )
int XSyncValueIsZero( int );  ( v -- flag )
int XSyncValueLessOrEqual( int , int );  ( b a -- flag )
int XSyncValueLessThan( int , int );  ( b a -- flag )
int XSyncValueLow32( int );  ( v -- u )
int XSyncValueSubtract( void * , int , int , void * );  ( &ov b a &r -- r )

end;
