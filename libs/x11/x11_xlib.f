;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adapted from bigForth by Bernd Paysan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: dynlib big-struct

struct{
    cell size
    cell addr
} XrmValue

[IFNDEF] libx11
dynlib: libx11 libX11.so.6
[ENDIF]


dynlib-import: libx11

int XGetModifierMapping( int ); ( dpy -- modmap )
int XNewModifiermap( int ); ( maxkeys -- modmap )
int XInitImage( int ); ( image -- status )
int XOpenDisplay( const char * ); ( name -- dpy )
int XDisplayName( int ); ( string -- string )
int XKeysymToString( int ); ( keysym -- string )
int XGContextFromGC( int ); ( gc -- gcontext )
int XStringToKeysym( int ); ( string -- keysym )
int XMaxRequestSize( int ); ( dpy -- n )
int XExtendedMaxRequestSize( int ); ( dpy -- n )
int XResourceManagerString( int ); ( dpy -- string )
int XScreenResourceString( int ); ( screen -- string )
int XDisplayMotionBufferSize( int ); ( dpy -- u )
int XVisualIDFromVisual( int ); ( visual -- visualID )
int XLockDisplay( int ); ( dpy -- r )
int XUnlockDisplay( int ); ( dpy -- r )
int XAddExtension( int ); ( dpy -- extcodes )
int XEHeadOfExtensionList( int ); ( object -- extdata** )
int XDefaultRootWindow( int ); ( dpy -- window )
int XRootWindowOfScreen( int ); ( screen -- window )
int XDefaultVisualOfScreen( int ); ( screen -- visual )
int XDefaultGCOfScreen( int ); ( screen -- gc )
int XBlackPixelOfScreen( int ); ( screen -- u )
int XWhitePixelOfScreen( int ); ( screen -- u )
int XNextRequest( int ); ( dpy -- u )
int XLastKnownRequestProcessed( int ); ( dpy -- u )
int XServerVendor( int ); ( dpy -- string )
int XDisplayString( int ); ( dpy -- string )
int XDefaultColormapOfScreen( int ); ( screen -- colormap )
int XDisplayOfScreen( int ); ( screen -- dpy )
int XDefaultScreenOfDisplay( int ); ( dpy -- screen )
int XEventMaskOfScreen( int ); ( screen -- mask )
int XScreenNumberOfScreen( int ); ( screen -- n )
int XSetErrorHandler( int ); ( handler -- errorhandler )
int XSetIOErrorHandler( int ); ( handler -- ioerrorhandler )
int XFreeStringList( int ); ( list -- r )
int XActivateScreenSaver( int ); ( dpy -- r )
int XAutoRepeatOff( int ); ( dpy -- r )
int XAutoRepeatOn( int ); ( dpy -- r )
int XBitmapBitOrder( int ); ( dpy -- n )
int XBitmapPad( int ); ( dpy -- n )
int XBitmapUnit( int ); ( dpy -- n )
int XCellsOfScreen( int ); ( screen -- n )
int XCloseDisplay( int ); ( dpy -- r )
int XConnectionNumber( int ); ( dpy -- r )
int XDefaultDepthOfScreen( int ); ( screen -- i )
int XDefaultScreen( int ); ( dpy -- i )
int XDoesBackingStore( int ); ( screen -- i )
int XDoesSaveUnders( int ); ( screen -- flag )
int XDisableAccessControl( int ); ( dpy -- r )
int XEnableAccessControl( int ); ( dpy -- r )
void XFlush( int ); ( dpy -- )
void XFree( int ); ( data -- )
int XFreeExtensionList( int ); ( list -- r )
int XFreeFontNames( int ); ( list -- r )
int XFreeFontPath( int ); ( list -- r )
int XFreeModifiermap( int ); ( modmap -- r )
int XGrabServer( int ); ( dpy -- r )
int XHeightMMOfScreen( int ); ( screen -- n )
int XHeightOfScreen( int ); ( screen -- n )
int XImageByteOrder( int ); ( dpy -- n )
int XMaxCmapsOfScreen( int ); ( screen -- n )
int XMinCmapsOfScreen( int ); ( screen -- n )
int XNoOp( int ); ( dpy -- r )
int XPending( int ); ( dpy -- n )
int XPlanesOfScreen( int ); ( screen -- n )
int XProtocolRevision( int ); ( dpy -- n )
int XProtocolVersion( int ); ( dpy -- n )
int XQLength( int ); ( dpy -- n )
int XRefreshKeyboardMapping( int ); ( event_map -- r )
int XResetScreenSaver( int ); ( dpy -- r )
int XScreenCount( int ); ( dpy -- n )
int XUngrabServer( int ); ( dpy -- r )
int XVendorRelease( int ); ( dpy -- n )
int XWidthMMOfScreen( int ); ( screen -- n )
int XWidthOfScreen( int ); ( screen -- n )
int XSetLocaleModifiers( int ); ( modifier_list -- string )
int XCloseOM( int ); ( om -- status )
int XSetOMValues( int ); ( ...om -- string )
int XGetOMValues( int ); ( ...om -- string )
int XDisplayOfOM( int ); ( om -- dpy )
int XLocaleOfOM( int ); ( om -- string )
int XCreateOC( int ); ( ...om -- XOC )
int XDestroyOC( int ); ( oc -- r )
int XOMOfOC( int ); ( oc -- XOM )
int XSetOCValues( int ); ( ...oc -- string )
int XGetOCValues( int ); ( ...oc -- string )
int XBaseFontNameListOfFontSet( int ); ( fontset -- string )
int XLocaleOfFontSet( int ); ( fontset -- string )
int XContextDependentDrawing( int ); ( fontset -- flag )
int XDirectionalDependentDrawing( int ); ( fontset -- flag )
int XContextualDrawing( int ); ( fontset -- flag )
int XExtentsOfFontSet( int ); ( fontset -- fontsetextents )
int XCloseIM( int ); ( im -- status )
int XDisplayOfIM( int ); ( im -- dpy )
int XLocaleOfIM( int ); ( im -- string )
int XCreateIC( int , ... ); ( ... im -- XIC )
int XDestroyIC( int ); ( ic -- r )
int XSetICFocus( int ); ( ic -- r )
int XUnsetICFocus( int ); ( ic -- r )
int XwcResetIC( int ); ( ic -- wstring )
int XmbResetIC( int ); ( ic -- string )
int XIMOfIC( int ); ( ic -- XIM )
int XDestroyRegion( int ); ( reg -- r )
int XEmptyRegion( int ); ( reg -- r )
int XwcFreeStringList( int ); ( list -- r )
int Xpermalloc( int ); ( size -- addr )
int XrmStringToQuark( int ); ( string -- quark )
int XrmPermStringToQuark( int ); ( string -- quark )
int XrmQuarkToString( int ); ( quark -- string )
int XrmDestroyDatabase( int ); ( dbase -- r )
int XrmGetDatabase( int ); ( dpy -- dbase )
int XrmGetFileDatabase( int ); ( filename -- dbase )
int XrmGetStringDatabase( int ); ( string -- dbase )
int XrmLocaleOfDatabase( int ); ( dbase -- string )

int XrmInitialize( );   ( -- r )
int XInitThreads( );    ( -- status )
int XAllPlanes( );      ( -- u )
void * XAllocClassHint( ); ( -- classhint* )
void * XAllocIconSize( );  ( -- iconsize* )
void * XAllocSizeHints( ); ( -- sizehints* )
void * XAllocStandardColormap( );  ( -- stdcmap* )
void * XAllocWMHints( );   ( -- wmhints* )
int XCreateRegion( );   ( -- reg )
void * XDefaultString( );  ( -- char* )
int XrmUniqueQuark( );  ( -- quark )

int XLoadQueryFont( int , int ); ( dpy name -- fontstruct )
int XAllocColor( int , int , int ); ( color colormap dpy -- status )
int XGetWindowAttributes( int , int , int ); ( dpy win windows_attributes_r -- status )
int XChangeWindowAttributes( int , int , int , int ); ( dpy win valuemask attributes -- r )
int XGetErrorText( int , int , int , int ); ( dpy code buffer_r length -- r )
int XSupportsLocale( ); ( -- flag )
int XGetVisualInfo( int , int , int , int ); ( dpy vinfo_mask vinfo_templ nitems_r -- vinfo* )
int XCreateColormap( int , int , int , int ); ( dpy w visual alloc -- colormap )
int XOpenIM( int , int , int , int ); ( dpy rdb res_name res_class -- XIM )
char * XGetIMValues( int , ... ); ( ... im -- string )
char * XSetIMValues( int , ... ); ( ... im  -- string )
void * XVaCreateNestedList( int , ... ); ( ...unused -- vanestedlist )
int XDestroyWindow( int , int ); ( dpy win -- r )
int XKeycodeToKeysym( int , int , int ); ( dpy keycode index -- keysym )
int XCreateFontSet( int , int , int , int , int ); ( dpy base_font_name_list missing_charset_list missing-charset_count def_string -- fontset )
int XFreeColors( int , int , int , int , int ); ( dpy colormap pixels npixels planes -- r )
int XCreateFontCursor( int , int ); ( dpy shape -- cursor )
int XChangeGC( int , int , int , int ); ( dpy gc valuemask values -- r )
int XSetFunction( int , int , int ); ( dpy gc function -- r )
int XStoreBytes( int , int , int ); ( dpy bytes nbytes -- r )
int XSetSelectionOwner( int , int , int , int ); ( dpy selection owner time -- r )
int XChangeProperty( int , int , int , int , int , int , int , int ); ( dpy w property type format mode data elements -- r )
int XConvertSelection( int , int , int , int , int , int ); ( dpy selection target property requestor time -- r )
int XGetSelectionOwner( int , int ); ( dpy atom -- window )
int XFetchBytes( int , int ); ( dpy nbytesr -- buffer )
int XCreateWindow( int , int , int , int , int , int , int , int , int , int , int , int ); ( dpy parent x y w h borderwidth depth class visual valuemask attribs -- window )
int XGetWindowProperty( int , int , int , int , int , int , int , int , int , int , int , int ); ( dpy w property offset length delete req_type actual_typer actual_format_r nitems_r bytes_after_r prop_r -- n )
int XDefineCursor( int , int , int ); ( dpy win cursor -- r )
int XSetLineAttributes( int , int , int , int , int , int ); ( dpy gc line_width line_style cap_style join_style -- r )
void XDrawLine( int , int , int , int , int , int , int ); ( dpy d gc x1 x2 y1 y2 -- )
void XFillRectangle( int , int , int , int , int , int , int ); ( dpy d gc x y w h -- )
void XFillPolygon( int , int , int , int , int , int , int ); ( dpy d gc points npoints shape mode -- )
void XDrawLines( int , int , int , int , int , int ); ( dpy d gc points npoints mode -- )
void XSetClipMask( int , int , int ); ( dpy gc pixmap -- )
void XSetClipOrigin( int , int , int , int ); ( dpy gc clip_x_orig clip_y_orig -- )
void XCopyArea( int , int , int , int , int , int , int , int , int , int ); ( dpy src dest gc x y w h dx dy -- )
void XUnionRectWithRegion( int , int , int ); ( rect src_reg dest_reg_r -- )
void XSetRegion( int , int , int ); ( dpy gc reg -- )
int XCheckMaskEvent( int , int , int ); ( dpy event_mask event_r -- flag )
void XNextEvent( int , int ); ( dpy event_r -- )
void XSync( int , int ); ( dpy discard -- )
int XQueryPointer( int , int , int , int , int , int , int , int , int ); ( dpy w root_r child_r root_x_r root_y_r win_x_r win_y_r mask_r -- flag )
int Xutf8LookupString( int , int , int , int , int , int ); ( ic event buffer_r wchars_buffer keysym_r status_r -- n )
int XLookupString( int , int , int , int , int ); ( event buffer bufsize keysym_r composestatus* -- n )
char *XGetAtomName( int , int ); ( dpy atom -- name )
int XSendEvent( int , int , int , int , int ); ( dpy w propagate event_mask event_send -- status )
void XTextExtents( int , int , int , int , int , int , int ); ( font_struct string nchars direction_r font_ascent_r font_descent_r overall_r -- )
void XTextExtents16( int , int , int , int , int , int , int ); ( font_struct string nchars direction_r font_ascent_r font_descent_r overall_r -- )
void XDrawText( int , int , int , int , int , int , int ); ( dpy d gc x y items nitems -- )
void XDrawText16( int , int , int , int , int , int , int ); ( dpy d gc x y items nitems -- )
void XFreePixmap( int , int ); ( dpy pixmap -- )
int XCreatePixmap( int , int , int , int , int ); ( dpy d w h depth -- pixmap )
void * XCreateImage( int , int , int , int , int , int , int , int , int , int ); ( dpy visual depth format offset data w h bmpad b/line -- image )
void * XGetImage( int , int , int , int , int , int , int , int ); ( dpy d x y w h planemask format -- image )
int XInternAtom( int , int , int ); ( dpy atomname flag -- atom )
int XListPixmapFormats( int , int ); ( dpy count_r -- pixmapformatvalues )
void XSetTransientForHint( int , int , int ); ( dpy w propw -- )
void XCopyPlane( int , int , int , int , int , int , int , int , int , int , int ); ( dpy src dest gc x y w h dx dy plane -- )
void XFreeColormap( int , int ); ( dpy colormap -- )
int XCreateGC( int , int , int , ... );  ( display Drawable valuemask XGCValues * -- GC )
void XFreeGC( int , int ); ( dpy gc -- )
int XGetGeometry( int , int , int , int , int , int , int , int , int ); ( dpy d root_r x_r y_r w_r h_r borderw_r depth_r -- status )
int XGrabPointer( int , int , int , int , int , int , int , int , int ); ( time cursor confine_to keyboard_mode pointer_mode owner_events grab_window dpy -- n )
void XMapRaised( int , int ); ( dpy w -- )
void XMapWindow( int , int ); ( dpy w -- )
void XMoveResizeWindow( int , int , int , int , int , int ); ( dpy win x y w h -- )
void XMoveWindow( int , int , int , int ); ( dpy win x y -- )
int XParseGeometry( int , int , int , int , int ); ( parsestring x_r y_r w_r h_r -- n )
void XPutImage( int , int , int , int , int , int , int , int , int , int ); ( dpy d gc image s_x s_y d_x d_y w h -- )
void XResizeWindow( int , int , int , int ); ( dpy win w h -- )
void XSetBackground( int , int , int ); ( dpy gc background -- )
void XSetForeground( int , int , int ); ( dpy gc fg -- )
void XSetIconName( int , int , int ); ( dpy win icon_name -- )
void XSetInputFocus( int , int , int , int ); ( dpy focus revert_to time -- )
void XStoreName( int , int , int ); ( dpy win window_name -- )
void XUngrabPointer( int , int ); ( dpy time -- )
void XUnmapWindow( int , int ); ( dpy w -- )
void XWarpPointer( int , int , int , int , int , int , int , int , int ); ( dpy src_w dest_w src_x src_y src_w src_h dest_x dest_y -- )
void XSelectInput( int , int , int ); ( dpy w event_mask -- )
void XWindowEvent( int , int , int , int ); ( dpy w event_mask event_r -- )
int XSetICValues( int , ... ); ( ... ic -- string )
int XFilterEvent( int , int ); ( event w -- flag )
void XIntersectRegion( int , int , int ); ( sra srb dr_r -- )
void XSetClassHint( int , int , int ); ( dpy w class_hints -- )
void XSetTextProperty( int , int , int , int ); ( dpy w text_prop prop -- )
void XSetWMHints( int , int , int ); ( dpy w wm_hints -- )
void XSetWMNormalHints( int , int , int ); ( dpy w hints -- )
int XCreateSimpleWindow( int , int , int , int , int , int , int , int , int ); ( dpy parent x y w h bwidth border bg -- window )

int XQueryFont( int , int ); ( XID dpy -- fontstruct )
int XGetMotionEvents( int , int , int , int , int ); ( neventsr stop start w dpy -- timecoord )
int XDeleteModifiermapEntry( int , int , int ); ( mod keycode modmap -- modmap )
int XInsertModifiermapEntry( int , int , int ); ( mod keycode modmap -- modmap )
int XGetSubImage( int , int , int , int , int , int , int , int , int , int , int ); ( dy dx dimage format planemask h w y x d dpy -- image )
int XFetchBuffer( int , int , int ); ( buffer nbytesr dpy -- buffer )
int XGetAtomNames( int , int , int , int ); ( namesr count atoms dpy -- status )
int XGetDefault( int , int , int ); ( option program dpy --  string )
int XInternAtoms( int , int , int , int , int ); ( atoms flag count names dpy -- status )
int XCopyColormapAndFree( int , int ); ( colormap dpy -- colormap )
int XCreatePixmapCursor( int , int , int , int , int , int , int ); ( y x bgcol fgcol mask source dpy -- cursor )
int XCreateGlyphCursor( int , int , int , int , int , int , int ); ( bgcol fgcol maskc sourcec maskfont sourcefont dpy -- cursor )
int XLoadFont( int , int ); ( name dpy -- font )
int XFlushGC( int , int ); ( gc dpy -- r )
int XCreateBitmapFromData( int , int , int , int , int ); ( h w data d dpy -- pixmap )
int XCreatePixmapFromBitmapData( int , int , int , int , int , int , int , int ); ( depth bg fg h w data d dpy -- pixmap )
int XListInstalledColormaps( int , int , int ); ( num_r w dpy -- colormap* )
int XListFonts( int , int , int , int ); ( actual_count_r maxnames pattern dpy -- char** )
int XListFontsWithInfo( int , int , int , int , int ); ( info_r count_r maxnames pattern dpy -- char** )
int XGetFontPath( int , int ); ( npaths_r dpy -- char** )
int XListExtensions( int , int ); ( nextensions_r dpy -- char** )
int XListProperties( int , int , int ); ( num_prop_r w dpy -- atom* )
int XListHosts( int , int , int ); ( status_r nhosts_r dpy -- hostaddr* )
int XLookupKeysym( int , int ); ( index key_event -- keysym )
int XGetKeyboardMapping( int , int , int , int ); ( keysym/keycode_r keycode_c first_kc dpy -- keysym* )
int XInitExtension( int , int ); ( name dpy -- extcodes )
int XFindOnExtensionList( int , int ); ( number structure -- extdata )
int XRootWindow( int , int ); ( screen_nr dpy -- window )
int XDefaultVisual( int , int ); ( screen_nr dpy -- visual )
int XDefaultGC( int , int ); ( screen_nr dpy -- gc )
int XBlackPixel( int , int ); ( screen_nr dpy -- u )
int XWhitePixel( int , int ); ( screen_nr dpy -- u )
int XDefaultColormap( int , int ); ( screen_nr dpy -- colormap )
int XScreenOfDisplay( int , int ); ( screen_nr dpy -- screen )
int XListDepths( int , int , int ); ( count_r screen_nr dpy -- int* )
int XReconfigureWMWindow( int , int , int , int , int ); ( changes mask screen_nr w dpy -- status )
int XGetWMProtocols( int , int , int , int ); ( count_r protocols_r w dpy -- status )
int XSetWMProtocols( int , int , int , int ); ( count protocols w dpy -- status )
int XIconifyWindow( int , int , int ); ( screen_nr w dpy -- status )
int XWithdrawWindow( int , int , int ); ( screen_nr w dpy -- status )
int XGetCommand( int , int , int , int ); ( argc_r argv_r w dpy -- status )
int XGetWMColormapWindows( int , int , int , int ); ( count_r windows_r w dpy -- status )
int XSetWMColormapWindows( int , int , int , int ); ( count colormap_windows w dpy -- status )
int XAddHost( int , int ); ( host dpy -- r )
int XAddHosts( int , int , int ); ( num hosts dpy -- r )
int XAddToExtensionList( int , int ); ( extdata structure -- r )
int XAddToSaveSet( int , int ); ( w dpy -- r )
int XAllocColorCells( int , int , int , int , int , int , int ); ( pixels pixels_r planes planemask_r contig colormap dpy -- status )
int XAllocColorPlanes( int , int , int , int , int , int , int , int , int , int , int ); ( bmask_r gmask_r rmask_r blues greens reds colors pixels_r contig colormap dpy -- status )
int XAllocNamedColor( int , int , int , int , int ); ( exact_def_r screen_def_r cname colormap dpy -- status )
int XAllowEvents( int , int , int ); ( time event_mode dpy -- r )
int XBell( int , int ); ( percent dpy -- r )
int XChangeActivePointerGrab( int , int , int , int ); ( time cursor event_mask dpy -- r )
int XChangeKeyboardControl( int , int , int ); ( values valuemask dpy -- r )
int XChangeKeyboardMapping( int , int , int , int , int ); ( num_codes keysyms keysyms/keycode first_kc dpy -- r )
int XChangePointerControl( int , int , int , int , int , int ); ( threshold accel_denom accel_num do_thres do_accel dpy -- r )
int XChangeSaveSet( int , int , int ); ( change_mode w dpy -- r )
int XCheckIfEvent( int , int , int , int ); ( arg predicate event_return dpy -- flag )
int XCheckTypedEvent( int , int , int ); ( event_r event_type dpy -- flag )
int XCheckTypedWindowEvent( int , int , int , int ); ( event_r event_type w dpy -- flag )
int XCheckWindowEvent( int , int , int , int ); ( event_r event_mask w dpy -- flag )
int XCirculateSubwindows( int , int , int ); ( direction w dpy -- r )
int XCirculateSubwindowsDown( int , int ); ( w dpy -- r )
int XCirculateSubwindowsUp( int , int ); ( w dpy -- r )
int XClearArea( int , int , int , int , int , int , int ); ( exposures h w y x w dpy -- r )
int XClearWindow( int , int ); ( w dpy -- r )
int XConfigureWindow( int , int , int , int ); ( values value_mask w dpy -- r )
int XCopyGC( int , int , int , int ); ( dest valuemask src dpy -- r )
int XDefaultDepth( int , int ); ( screen_nr dpy -- i )
int XDeleteProperty( int , int , int ); ( atom w dpy -- r )
int XDestroySubwindows( int , int ); ( w dpy -- r )
int XDisplayCells( int , int ); ( screen_nr dpy -- i )
int XDisplayHeight( int , int ); ( screen_nr dpy -- i )
int XDisplayHeightMM( int , int ); ( screen_nr dpy -- i )
int XDisplayKeycodes( int , int , int ); ( min_keycodes_r max_keycodes_r dpy -- r )
int XDisplayPlanes( int , int ); ( screen_nr dpy -- i )
int XDisplayWidth( int , int ); ( screen_nr dpy -- i )
int XDisplayWidthMM( int , int ); ( screen_nr dpy -- i )
void XDrawArc( int , int , int , int , int , int , int , int , int ); ( angle2 angle1 h w y x gc d dpy -- )
void XDrawArcs( int , int , int , int , int ); ( narcs arcs gc d dpy -- )
void XDrawImageString( int , int , int , int , int , int , int ); ( length string y x gc d dpy -- )
void XDrawImageString16( int , int , int , int , int , int , int ); ( length string y x gc d dpy -- )
void Xutf8DrawText( int , int , int , int , int , int , int ); ( dpy d gc x y items nitems -- )
int XDrawPoint( int , int , int , int , int ); ( y x gc d dpy -- r )
int XDrawPoints( int , int , int , int , int , int ); ( mode npoints point gc d dpy -- r )
int XDrawRectangle( int , int , int , int , int , int , int ); ( h w y x gc d dpy -- r )
int XDrawRectangles( int , int , int , int , int ); ( nrects rects gc d dpy -- r )
int XDrawSegments( int , int , int , int , int ); ( nsegments segments gc d dpy -- r )
void XDrawString( int , int , int , int , int , int , int ); ( length string y x gc d dpy -- )
void XDrawString16( int , int , int , int , int , int , int ); ( length string y x gc d dpy -- )
int XEventsQueued( int , int ); ( mode dpy -- i )
int XFetchName( int , int , int ); ( name_r w dpy -- status )
void XFillArc( int , int , int , int , int , int , int , int , int ); ( angle2 angle1 h w y x gc d dpy -- )
void XFillArcs( int , int , int , int , int ); ( narcs arcs gc d dpy -- )
void XFillRectangles( int , int , int , int , int ); ( nrects rects gc d dpy -- )
int XForceScreenSaver( int , int ); ( mode dpy -- r )
int XFreeCursor( int , int ); ( cursor dpy -- r )
int XFreeFont( int , int ); ( fontstruct dpy -- r )
int XFreeFontInfo( int , int , int ); ( actual_count free_info names -- r )
int XGeometry( int , int , int , int , int , int , int , int , int , int , int , int , int ); ( hr wr yr xr yadder xadder fh fw bw default_pos pos screen dpy -- n )
int XGetErrorDatabaseText( int , int , int , int , int , int ); ( display name message default_string buffer_return length -- flag )
int XGetGCValues( int , int , int , int ); ( values_r valuemask gc dpy -- status )
int XGetIconName( int , int , int ); ( icon_name-r w dpy -- status )
int XGetInputFocus( int , int , int ); ( revert_to_r focus_r dpy -- r )
int XGetKeyboardControl( int , int ); ( values_r dpy -- r )
int XGetPointerControl( int , int , int , int ); ( threshold_r accel_denom_r accel_num_r dpy -- r )
int XGetPointerMapping( int , int , int ); ( nmap map_r dpy -- n )
int XGetScreenSaver( int , int , int , int , int ); ( allow_exposures_r prefer_blanking_r interval_r timeout_r dpy -- r )
int XGetTransientForHint( int , int , int ); ( prop_w_r w dpy -- status )
int XGrabButton( int , int , int , int , int , int , int , int , int ); ( cursor confine_to keyboard_mode pointer_mode owner_events grab_window modifiers button dpy -- r )
int XGrabKey( int , int , int , int , int , int , int , int ); ( keyboard_mode pointer_mode owner_events grab_events grab_window modifiers keycode dpy -- r )
int XGrabKeyboard( int , int , int , int , int , int ); ( time keyboard_mode pointer_mode owner_events grab_window dpy -- n )
int XIfEvent( int , int , int , int ); ( arg predicate event_r dpy -- r )
int XInstallColormap( int , int ); ( colormap dpy -- r )
int XKeysymToKeycode( int , int ); ( keysym dpy -- keycode )
int XKillClient( int , int ); ( resource dpy -- r )
\ 1 Extern: XLastKnownRequestProcessed   XLastKnownRequestProcessed      ( dpy -- u )
int XLookupColor( int , int , int , int , int ); ( screen_def_r exact_def_r color_name colormap dpy -- status )
int XLowerWindow( int , int ); ( w dpy -- r )
int XMapSubwindows( int , int ); ( w dpy -- r )
int XMaskEvent( int , int , int ); ( event_r event_mask dpy -- r )
int XParseColor( int , int , int , int ); ( exact_def_r spec colormap dpy -- status )
int XPeekEvent( int , int ); ( event_r dpy -- r )
int XPeekIfEvent( int , int , int , int ); ( arg predicate event_r dpy -- r )
int XPutBackEvent( int , int ); ( event dpy -- r )
int XQueryBestCursor( int , int , int , int , int , int ); ( h_r w_r h w d dpy -- status )
int XQueryBestSize( int , int , int , int , int , int , int ); ( h_r w_r h w which_screen class dpy -- status )
int XQueryBestStipple( int , int , int , int , int , int ); ( h_r w_r h w which_screen dpy -- status )
int XQueryBestTile( int , int , int , int , int , int ); ( h_r w_r h w which_screen dpy -- status )
int XQueryColor( int , int , int ); ( def_in_out colormap dpy -- r )
int XQueryColors( int , int , int , int ); ( ncolors defs_in_out colormap dpy -- r )
int XQueryExtension( int , int , int , int , int ); ( first_error_r first_event_r major_opcode_r name dpy -- flag )
int XQueryKeymap( int , int ); ( keys_r dpy -- r )
int XQueryTextExtents( int , int , int , int , int , int , int , int ); ( overall_r font_descent_r font_ascent_r direction_r nchars string font_id dpy -- r )
int XQueryTextExtents16( int , int , int , int , int , int , int , int ); ( overall_r font_descent_r font_ascent_r direction_r nchars string font_id dpy -- r )
int XQueryTree( int , int , int , int , int , int ); ( nchildren_r children_r parent_r root_r w dpy -- status )
int XRaiseWindow( int , int ); ( w dpy -- r )
int XReadBitmapFile( int , int , int , int , int , int , int , int ); ( y_hot_r x_hot_r bitmap_r height_r width_r filename d dpy -- n )
int XReadBitmapFileData( int , int , int , int , int , int ); ( y_hot_r x_hot_r bitmap_r height_r width_r filename -- n )
int XRebindKeysym( int , int , int , int , int , int ); ( bytes_string string mod_count list keysym dpy -- r )
int XRecolorCursor( int , int , int , int ); ( bg fg cursor dpy -- r )
int XRemoveFromSaveSet( int , int ); ( w dpy -- r )
int XRemoveHost( int , int ); ( host dpy -- r )
int XRemoveHosts( int , int , int ); ( num_hosts hosts dpy -- r )
int XReparentWindow( int , int , int , int , int ); ( y x parent w dpy -- r )
int XRestackWindows( int , int , int ); ( nwindows windows dpy -- r )
int XRotateBuffers( int , int ); ( rotate dpy -- r )
int XRotateWindowProperties( int , int , int , int , int ); ( npositions num_prop properties w dpy -- r )
int XSetAccessControl( int , int ); ( mode dpy -- r )
int XSetArcMode( int , int , int ); ( arc_mode gc dpy -- r )
int XSetClipRectangles( int , int , int , int , int , int , int ); ( ordering n rectangles clip_y_orig clip_x_orig gc dpy -- r )
int XSetCloseDownMode( int , int ); ( close_mode dpy -- r )
int XSetCommand( int , int , int , int ); ( argc argv w dpy -- r )
int XSetDashes( int , int , int , int , int ); ( n dash_list dash_ofset gc dpy -- r )
int XSetFillRule( int , int , int ); ( fill_rule gc dpy -- r )
int XSetFillStyle( int , int , int ); ( fill_style gc dpy -- r )
int XSetFont( int , int , int ); ( font gc dpy -- r )
int XSetFontPath( int , int , int ); ( ndirs directories dpy -- r )
int XSetGraphicsExposures( int , int , int ); ( graphics_exposures gc dpy -- r )
int XSetModifierMapping( int , int ); ( modmap dpy -- n )
int XSetPlaneMask( int , int , int ); ( plane_mask gc dpy -- r )
int XSetPointerMapping( int , int , int ); ( nmap map dpy -- n )
int XSetScreenSaver( int , int , int , int , int ); ( allow_exposures prefer_blanking interval timeout dpy -- r )
int XSetState( int , int , int , int , int , int ); ( plane_mask function bg fg gc dpy -- r )
int XSetStipple( int , int , int ); ( stripple gc dpy -- r )
int XSetSubwindowMode( int , int , int ); ( subwindow_mode gc dpy -- r )
int XSetTSOrigin( int , int , int , int ); ( ts_y_orig ts_x_orig gc dpy -- r )
int XSetTile( int , int , int ); ( tile gc dpy -- )
int XSetWindowBackground( int , int , int ); ( bg_pixel w dpy -- )
int XSetWindowBackgroundPixmap( int , int , int ); ( bg_pixmap w dpy -- r )
int XSetWindowBorder( int , int , int ); ( border_pixel w dpy -- r )
int XSetWindowBorderPixmap( int , int , int ); ( border_pixmap w dpy -- r )
int XSetWindowBorderWidth( int , int , int ); ( width w dpy -- r )
int XSetWindowColormap( int , int , int ); ( colormap w dpy -- r )
int XStoreBuffer( int , int , int , int ); ( buffer nbytes bytes dpy -- r )
int XStoreColor( int , int , int ); ( color colormap dpy -- r )
int XStoreColors( int , int , int , int ); ( ncolors color colormap dpy -- r )
int XStoreNamedColor( int , int , int , int , int ); ( flags pixel color colormap dpy -- r )
int XTextWidth( int , int , int ); ( count string font_struct -- n )
int XTextWidth16( int , int , int ); ( count string font_struct -- n )
int XTranslateCoordinates( int , int , int , int , int , int , int , int ); ( child_r dest_y_r dest_x_r src_y src_x dest_w src_w dpy -- flag )
int XUndefineCursor( int , int ); ( w dpy -- r )
int XUngrabButton( int , int , int , int ); ( grab_window modifiers button dpy -- r )
int XUngrabKey( int , int , int , int ); ( grab_window modifiers keycode dpy -- r )
int XUngrabKeyboard( int , int ); ( time dpy -- r )
int XUninstallColormap( int , int ); ( colormap dpy -- r )
int XUnloadFont( int , int ); ( font dpy -- r )
int XUnmapSubwindows( int , int ); ( w dpy -- r )
int XWriteBitmapFile( int , int , int , int , int , int , int ); ( y_hot x_hot h w bitmap filename dpy -- r )
int XOpenOM( int , int , int , int ); ( res_class res_name rdb dpy -- XOM )
int XFreeFontSet( int , int ); ( fontset dpy -- r )
int XFontsOfFontSet( int , int , int ); ( font_name_list font_struct_list font_set -- n )
int XmbTextEscapement( int , int , int ); ( bytes_text text fontset -- n )
int XwcTextEscapement( int , int , int ); ( num_wchars text fontset -- n )
int XmbTextExtents( int , int , int , int , int ); ( overall_logical_r overall_ink_r bytes_text text fontset -- n )
int XwcTextExtents( int , int , int , int , int ); ( overall_logical_r overall_ink_r num_wchars text fontset -- n )
int XmbTextPerCharExtents( int , int , int , int , int , int , int , int , int ); ( overal_logical_r overall_ink_r num_chars buffer_size logical_extents_buffer ink_extents_buffer bytes_text text fontset -- status )
int XwcTextPerCharExtents( int , int , int , int , int , int , int , int , int ); ( overal_logical_r overall_ink_r num_chars buffer_size logical_extents_buffer ink_extents_buffer num_wchars text fontset -- status )
void XmbDrawText( int , int , int , int , int , int , int ); ( nitems text_items y x gc d dpy -- )
void XwcDrawText( int , int , int , int , int , int , int ); ( nitems text_items y x gc d dpy -- )
void XmbDrawString( int , int , int , int , int , int , int , int ); ( bytes_text text y x gc fontset d dpy -- )
void XwcDrawString( int , int , int , int , int , int , int , int ); ( num_wchars text y x gc fontset d dpy -- )
void XmbDrawImageString( int , int , int , int , int , int , int , int ); ( bytes_text text y x gc fontset d dpy -- )
void XwcDrawImageString( int , int , int , int , int , int , int , int ); ( num_wchars text y x gc fontset d dpy -- )
char * XGetICValues( int , ... ); ( ... ic -- string )
int XmbLookupString( int , int , int , int , int , int ); ( status_r keysym_r bytes_buffer buffer_r event ic -- n )
int XwcLookupString( int , int , int , int , int , int ); ( status_r keysym_r wchars_buffer buffer_r event ic -- n )
int XRegisterIMInstantiateCallback( int , int , int , int , int , int ); ( client_data callback res_class res_name rdb dpy -- flag )
int XUnregisterIMInstantiateCallback( int , int , int , int , int , int ); ( client_data callback res_class res_name rdb dpy -- flag )
int XInternalConnectionNumbers( int , int , int ); ( count_r fd_r dpy -- status )
int XProcessInternalConnection( int , int ); ( fd dpy -- r )
int XAddConnectionWatch( int , int , int ); ( client_data callback dpy -- status )
int XRemoveConnectionWatch( int , int , int ); ( client_data callback dpy -- status )

\ Xutil.h
\ The following functions are not performed by the XServer, but in Xlib.

int XClipBox( int , int ); ( rect reg -- r )
int XDeleteContext( int , int , int ); ( context rid dpy -- n )
int XEqualRegion( int , int ); ( r1 r2 -- r )
int XFindContext( int , int , int , int ); ( data_r context rid dpy -- n )
int XGetClassHint( int , int , int ); ( class_hints_r w dpy -- status )
int XGetIconSizes( int , int , int , int ); ( count_r size_list_r w dpy -- status )
int XGetNormalHints( int , int , int ); ( hints_r w dpy -- status )
int XGetRGBColormaps( int , int , int , int , int ); ( prop count_r stdcmap_r w dpy -- status )
int XGetSizeHints( int , int , int , int ); ( prop hints_r w dpy -- status )
int XGetStandardColormap( int , int , int , int ); ( prop colormap_r w dpy -- status )
int XGetTextProperty( int , int , int , int ); ( prop text_prop_r w dpy -- status )
int XGetWMClientMachine( int , int , int ); ( text_prop_r w dpy -- status )
int XGetWMHints( int , int ); ( w dpy -- WMHints* )
int XGetWMIconName( int , int , int ); ( text_prop_r w dpy -- status )
int XGetWMName( int , int , int ); ( text_prop_r w dpy -- status )
int XGetWMNormalHints( int , int , int , int ); ( supplied_r hints_r w dpy -- status )
int XGetWMSizeHints( int , int , int , int , int ); ( prop supplied_r hints_r w dpy -- status )
int XGetZoomHints( int , int , int ); ( zhints_r w dpy -- status )
int XConvertCase( int , int , int ); ( upper* lower* sym -- r )
int XMatchVisualInfo( int , int , int , int , int ); ( vinfo_r class depth screen dpy -- status )
int XOffsetRegion( int , int , int ); ( dy dx reg -- r )
int XPointInRegion( int , int , int ); ( y x reg -- flag )
int XPolygonRegion( int , int , int ); ( fill_rile n points -- reg )
int XRectInRegion( int , int , int , int , int ); ( h w y x reg -- n )
int XSaveContext( int , int , int , int ); ( data context rid dpy -- n )
int XSetIconSizes( int , int , int , int ); ( count size_list w dpy -- r )
int XSetNormalHints( int , int , int ); ( hints w dpy -- r )
int XSetRGBColormaps( int , int , int , int , int ); ( prop count stdcmaps w dpy -- r )
int XSetSizeHints( int , int , int , int ); ( prop hints w dpy -- r )
int XSetStandardProperties( int , int , int , int , int , int , int , int ); ( sizehints argc argv icon_pixmap icon_name window_name w dpy -- r )
int XSetWMClientMachine( int , int , int ); ( text_prop w dpy -- r )
int XSetWMIconName( int , int , int ); ( text_prop w dpy -- r )
int XSetWMName( int , int , int ); ( text_prop w dpy -- r )
int XSetWMProperties( int , int , int , int , int , int , int , int , int ); ( class_hints wm_hints normal_hints argc argv icon_name window_name w dpy -- r )
int XmbSetWMProperties( int , int , int , int , int , int , int , int , int ); ( class_hints wm_hints normal_hints argc argv icon_name window_name w dpy -- r )
int XSetWMSizeHints( int , int , int , int ); ( prop hints w dpy -- r )
int XSetStandardColormap( int , int , int , int ); ( prop colormap w dpy -- r )
int XSetZoomHints( int , int , int ); ( zhints w dpy -- r )
int XShrinkRegion( int , int , int ); ( dy dx reg -- r )
int XStringListToTextProperty( int , int , int ); ( text_prop_r count list -- status )
int XSubtractRegion( int , int , int ); ( dr_r srb sra -- r )
int XmbTextListToTextProperty( int , int , int , int , int ); ( text_prop_r style count list dpy -- n )
int XwcTextListToTextProperty( int , int , int , int , int ); ( text_prop_r style count list dpy -- n )
int XTextPropertyToStringList( int , int , int ); ( count_r list_r text_prop -- status )
int XmbTextPropertyToTextList( int , int , int , int ); ( count_r list_r text_prop dpy -- n )
int XwcTextPropertyToTextList( int , int , int , int ); ( count_r list_r text_prop dpy -- n )
int XUnionRegion( int , int , int ); ( dr_r srb sra -- r )
int XWMGeometry( int , int , int , int , int , int , int , int , int , int ); ( gravity_r h_r w_r y_r x_r hints border_w default_geometry user_geometry screen_nr dpy -- n )
int XXorRegion( int , int , int ); ( dr_r srb sra -- r )

\ Memory Management
int XrmStringToQuarkList( int , int ); ( quarks string -- r )
int XrmStringToBindingQuarkList( int , int , int ); ( quarks bindings string -- r )

int XrmQPutResource( int , int , int , int , int ); ( value type quarks bindings dbase -- r )
int XrmPutResource( int , int , int , int ); ( value type spec dbase -- r )
int XrmQPutStringResource( int , int , int , int ); ( value quarks bindings dbase -- r )
int XrmPutStringResource( int , int , int ); ( value spec dbase -- r )
int XrmPutLineResource( int , int ); ( line dbase -- r )
int XrmQGetResource( int , int , int , int , int ); ( value_r quark_t_r quark_c quark_n dbase -- r )
int XrmGetResource( int , int , int , int , int ); ( value_r str_t_r str_c str_n dbase -- flag )
int XrmQGetSearchList( int , int , int , int , int ); ( list_len list_r classes names dbase -- flag )
int XrmQGetSearchResource( int , int , int , int , int ); ( value_r type_r class name list -- flag )
int XrmSetDatabase( int , int ); ( dbase dpy -- r )
int XrmCombineFileDatabase( int , int , int ); ( flag dbase* filename -- status )
int XrmPutFileDatabase( int , int ); ( string dbase -- r )
int XrmMergeDatabases( int , int ); ( dbase* dbase -- r )
int XrmCombineDatabase( int , int , int ); ( flag dbase* dbase )
int XrmEnumerateDatabase( int , int , int , int , int , int ); ( closure callback mode class_prefix name_prefix db -- flag )
int XrmParseCommand( int , int , int , int , int , int ); ( argv_io argc_io name table-count table dbase -- r )

end;
