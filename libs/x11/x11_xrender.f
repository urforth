;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adapted from bigForth by Bernd Paysan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ Xrender extension
use-libs: dynlib big-struct

[IFNDEF] libxrender
dynlib: libxrender libXrender.so.1
[ENDIF]


dynlib-import: libxrender

char * XRenderFindFormat( char * , int , char * , int );  ( dpy mask templ count -- pict )
char * XRenderFindVisualFormat( char * , char * );  ( dpy visual -- pict )
char * XRenderFindStandardFormat( char * , int );  ( dpy format -- pict )
int XRenderCreatePicture( char * , int , char * , int , char * );  ( dpy drawable format valuemask attributes -- picture )
void XRenderComposite( char * , int , char * , char * , char * , int , int , int , int , int , int , int , int ); ( dpy op src mask dst srcx srcy maskx masky dstx dsty w h -- )
void XRenderSetPictureClipRegion( char * , int , int );  ( dpy pict region -- )
void XRenderSetPictureClipRectangles( char * , char * , int , int , char * , int );
void XRenderFreePicture( char * , char * );

end;

struct{
  cell repeat
  cell alpha_map
  cell alpha_x_origin
  cell alpha_y_origin
  cell clip_x_origin
  cell clip_y_origin
  cell clip_mask
  cell graphics_exposures
  cell subwindow_mode
  cell poly_edge
  cell poly_mode
  cell dither
  cell component_alpha
} XRenderPictureAttributes

enum{
  value PictStandardARGB32
  value PictStandardRGB24
  value PictStandardA8
  value PictStandardA4
  value PictStandardA1
\  value PictStandardNUM
}
