;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adapted from bigForth by Bernd Paysan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ Xft fonts
use-libs: dynlib big-struct

[IFNDEF] libxft
dynlib: libxft libXft.so.2
[ENDIF]


dynlib-import: libxft

int XftFontMatch( int , int , int , int ); ( dpy screen pattern result -- pattern )
int XftFontOpenPattern( int , int ); ( dpy pattern -- font )
int XftFontOpenXlfd( int , int , int ); ( dpy screen xlfd -- font )
int XftXlfdParse( int , int , int ); ( xlfd igf compf -- pattern )
void XftTextExtents8( int , int , int , int , int ); ( dpy font string len extents -- )
void XftTextExtentsUtf8( int , int , int , int , int ); ( dpy font string len extents -- )
int XftDrawCreate( int , int , int , int ); ( dpy drawable visual colormap -- draw )
int XftDrawCreateAlpha( int , int , int ); ( dpy drawable depth -- draw )
void XftDrawChange( int , int ); ( draw drawable -- )
void XftDrawDestroy( int ); ( draw -- )
void XftDrawString8( int , int , int , int , int , int , int ); ( d color font x y addr u -- )
void XftDrawString16( int , int , int , int , int , int , int ); ( d color font x y addr u -- )
void XftDrawString32( int , int , int , int , int , int , int ); ( d color font x y addr u -- )
void XftDrawStringUtf8( int , int , int , int , int , int , int ); ( d color font x y addr u -- )
void XftDrawRect( int , int , int , int , int , int , int ); ( d color x y w h -- )
int XftColorAllocValue( int , int , int , int , int ); ( d v cmap color result -- )
void XftColorFree( int , int , int , int ); ( d v cmap color -- )
int XftDrawSetClip( int , int ); ( d r -- bool )

end;


enum{
  value XftTypeVoid
  value XftTypeInteger
  value XftTypeDouble
  value XftTypeString
  value XftTypeBool
  value XftTypeMatrix
}

struct{
  short red
  short green
  short blue
  short alpha
} XRenderColor

struct{
  cell pixel
  struct XRenderColor color
} XftColor

struct{
  double xx
  double xy
  double yx
  double yy
} XftMatrix

\ create Xft0Matrix  $3FF00000. 2, 0. 2, 0. 2, $3FF00000. 2,
\ create Xft90Matrix  0. 2, $3FF00000. 2, $3FF00000. 2, 0. 2,

struct{
  cell type {
    ptr s
|   cell i
|   cell b
|   double d
|   ptr m
  }
} XftValue

struct{
  ptr next
  struct XftValue value
} XftValueList

struct{
  ptr object
  ptr values
} XftPatternElt

struct{
  cell num
  cell size
  ptr elts
} XftPattern

struct{
  cell ascent
  cell descent
  cell height
  cell max_advance_width
  ptr charset
  ptr pattern
} XftFont

struct{
  short width
  short height
  short x
  short y
  short xOff
  short yOff
} XGlyphInfo
