\ adapted from bigForth by Bernd Paysan
(*
 * $XConsortium: X.h,v 1.69 94/04/17 20:10:48 dpw Exp $
 *)

(* Definitions for the X window system likely to be used by applications *)

(*

Copyright (c) 1987  X Consortium

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the X Consortium shall not be
used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from the X Consortium.


Copyright 1987 by Digital Equipment Corporation, Maynard, Massachusetts.

                        All Rights Reserved

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation, and that the name of Digital not be
used in advertising or publicity pertaining to distribution of the
software without specific, written prior permission.

DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
SOFTWARE.

***************************************************************** *)
use-lib: big-struct

\ converted to bigFORTH by Bernd Paysan
\ adapted for UrForth by Ketmar Dark

vocabulary Xconst also Xconst definitions

base @ decimal

11 constant X_PROTOCOL (* current protocol version *)
0 constant X_PROTOCOL_REVISION (* current minor version *)

(* Resources *)

(*
 * _XSERVER64 must ONLY be defined when compiling X server sources on
 * systems where unsigned long is not 32 bits, must NOT be used in
 * client or library code.
 *)

(* ***************************************************************
 * RESERVED RESOURCE AND constant DEFINITIONS
 *************************************************************** *)

0 constant None (* universal null resource or null atom *)

1 constant ParentRelative (* background pixmap in CreateWindow
        and ChangeWindowAttributes *)

0 constant CopyFromParent (* border pixmap in CreateWindow
           and ChangeWindowAttributes
       special VisualID and special window
           class passed to CreateWindow *)

0 constant PointerWindow (* destination window in SendEvent *)
1 constant InputFocus (* destination window in SendEvent *)

1 constant PointerRoot (* focus window in SetInputFocus *)

0 constant AnyPropertyType (* special Atom, passed to GetProperty *)

0 constant AnyKey (* special Key Code, passed to GrabKey *)

0 constant AnyButton (* special Button Code, passed to GrabButton *)

0 constant AllTemporary (* special Resource ID passed to KillClient *)

0 constant CurrentTime (* special Time *)

0 constant NoSymbol (* special KeySym *)

(* ***************************************************************
 * EVENT DEFINITIONS
 **************************************************************** *)

(* Input Event Masks. Used as event-mask window attribute and as arguments
   to Grab requests.  Not to be confused with event names.  *)

bitenum{
  0 set
  value NoEventMask
  value KeyPressMask
  value KeyReleaseMask
  value ButtonPressMask
  value ButtonReleaseMask
  value EnterWindowMask
  value LeaveWindowMask
  value PointerMotionMask
  value PointerMotionHintMask
  value Button1MotionMask
  value Button2MotionMask
  value Button3MotionMask
  value Button4MotionMask
  value Button5MotionMask
  value ButtonMotionMask
  value KeymapStateMask
  value ExposureMask
  value VisibilityChangeMask
  value StructureNotifyMask
  value ResizeRedirectMask
  value SubstructureNotifyMask
  value SubstructureRedirectMask
  value FocusChangeMask
  value PropertyChangeMask
  value ColormapChangeMask
  value OwnerGrabButtonMask
}

(* Event names.  Used in "type" field in XEvent structures.  Not to be
confused with event masks above.  They start from 2 because 0 and 1
are reserved in the protocol for errors and replies. *)

enum{
  2 set
  value KeyPress
  value KeyRelease
  value ButtonPress
  value ButtonRelease
  value MotionNotify
  value EnterNotify
  value LeaveNotify
  value FocusIn
  value FocusOut
  value KeymapNotify
  value Expose
  value GraphicsExpose
  value NoExpose
  value VisibilityNotify
  value CreateNotify
  value DestroyNotify
  value UnmapNotify
  value MapNotify
  value MapRequest
  value ReparentNotify
  value ConfigureNotify
  value ConfigureRequest
  value GravityNotify
  value ResizeRequest
  value CirculateNotify
  value CirculateRequest
  value PropertyNotify
  value SelectionClear
  value SelectionRequest
  value SelectionNotify
  value ColormapNotify
  value ClientMessage
  value MappingNotify
  value GenericEvent
  value LASTEvent (* must be bigger than any event # *)
}

: get-x11-event-type-name  ( type -- addr count )
  case
    KeyPress of " KeyPress" endof
    KeyRelease of " KeyRelease" endof
    ButtonPress of " ButtonPress" endof
    ButtonRelease of " ButtonRelease" endof
    MotionNotify of " MotionNotify" endof
    EnterNotify of " EnterNotify" endof
    LeaveNotify of " LeaveNotify" endof
    FocusIn of " FocusIn" endof
    FocusOut of " FocusOut" endof
    KeymapNotify of " KeymapNotify" endof
    Expose of " Expose" endof
    GraphicsExpose of " GraphicsExpose" endof
    NoExpose of " NoExpose" endof
    VisibilityNotify of " VisibilityNotify" endof
    CreateNotify of " CreateNotify" endof
    DestroyNotify of " DestroyNotify" endof
    UnmapNotify of " UnmapNotify" endof
    MapNotify of " MapNotify" endof
    MapRequest of " MapRequest" endof
    ReparentNotify of " ReparentNotify" endof
    ConfigureNotify of " ConfigureNotify" endof
    ConfigureRequest of " ConfigureRequest" endof
    GravityNotify of " GravityNotify" endof
    ResizeRequest of " ResizeRequest" endof
    CirculateNotify of " CirculateNotify" endof
    CirculateRequest of " CirculateRequest" endof
    PropertyNotify of " PropertyNotify" endof
    SelectionClear of " SelectionClear" endof
    SelectionRequest of " SelectionRequest" endof
    SelectionNotify of " SelectionNotify" endof
    ColormapNotify of " ColormapNotify" endof
    ClientMessage of " ClientMessage" endof
    MappingNotify of " MappingNotify" endof
    GenericEvent of " GenericEvent" endof
    otherwise drop " UnknownEvent"
  endcase
;

: get-x11-event-handler-name  ( type -- addr count )
  case
    KeyPress of " KeyPress-Handler" endof
    KeyRelease of " KeyRelease-Handler" endof
    ButtonPress of " ButtonPress-Handler" endof
    ButtonRelease of " ButtonRelease-Handler" endof
    MotionNotify of " MotionNotify-Handler" endof
    EnterNotify of " EnterNotify-Handler" endof
    LeaveNotify of " LeaveNotify-Handler" endof
    FocusIn of " FocusIn-Handler" endof
    FocusOut of " FocusOut-Handler" endof
    KeymapNotify of " KeymapNotify-Handler" endof
    Expose of " Expose-Handler" endof
    GraphicsExpose of " GraphicsExpose-Handler" endof
    NoExpose of " NoExpose-Handler" endof
    VisibilityNotify of " VisibilityNotify-Handler" endof
    CreateNotify of " CreateNotify-Handler" endof
    DestroyNotify of " DestroyNotify-Handler" endof
    UnmapNotify of " UnmapNotify-Handler" endof
    MapNotify of " MapNotify-Handler" endof
    MapRequest of " MapRequest-Handler" endof
    ReparentNotify of " ReparentNotify-Handler" endof
    ConfigureNotify of " ConfigureNotify-Handler" endof
    ConfigureRequest of " ConfigureRequest-Handler" endof
    GravityNotify of " GravityNotify-Handler" endof
    ResizeRequest of " ResizeRequest-Handler" endof
    CirculateNotify of " CirculateNotify-Handler" endof
    CirculateRequest of " CirculateRequest-Handler" endof
    PropertyNotify of " PropertyNotify-Handler" endof
    SelectionClear of " SelectionClear-Handler" endof
    SelectionRequest of " SelectionRequest-Handler" endof
    SelectionNotify of " SelectionNotify-Handler" endof
    ColormapNotify of " ColormapNotify-Handler" endof
    ClientMessage of " ClientMessage-Handler" endof
    MappingNotify of " MappingNotify-Handler" endof
    GenericEvent of " GenericEvent-Handler" endof
    otherwise drop " UnknownEvent-Handler"
  endcase
;

(* Key masks. Used as modifiers to GrabButton and GrabKey, results of QueryPointer,
   state in various key-, mouse-, and button-related events. *)

bitenum{
  value ShiftMask
  value LockMask
  value ControlMask
  value Mod1Mask
  value Mod2Mask
  value Mod3Mask
  value Mod4Mask
  value Mod5Mask
  (* button masks.  Used in same manner as Key masks above. Not to be confused with button names below. *)
  value Button1Mask
  value Button2Mask
  value Button3Mask
  value Button4Mask
  value Button5Mask
  15 set-bit value AnyModifier (* used in GrabButton, GrabKey *)
}

(* modifier names.  Used to build a SetModifierMapping request or
   to read a GetModifierMapping request.  These correspond to the
   masks defined above. *)
enum{
  value ShiftMapIndex
  value LockMapIndex
  value ControlMapIndex
  value Mod1MapIndex
  value Mod2MapIndex
  value Mod3MapIndex
  value Mod4MapIndex
  value Mod5MapIndex
}


(* button names. Used as arguments to GrabButton and as detail in ButtonPress
   and ButtonRelease events.  Not to be confused with button masks above.
   Note that 0 is already defined above as "AnyButton".  *)
enum{
  1 set
  value Button1
  value Button2
  value Button3
  value Button4
  value Button5
}

(* Notify modes *)
enum{
  value NotifyNormal
  value NotifyGrab
  value NotifyUngrab
  value NotifyWhileGrabbed
}

1 constant NotifyHint (* for MotionNotify events *)

(* Notify detail *)
enum{
  value NotifyAncestor
  value NotifyVirtual
  value NotifyInferior
  value NotifyNonlinear
  value NotifyNonlinearVirtual
  value NotifyPointer
  value NotifyPointerRoot
  value NotifyDetailNone
}

(* Visibility notify *)
enum{
  value VisibilityUnobscured
  value VisibilityPartiallyObscured
  value VisibilityFullyObscured
}

(* Circulation request *)
enum{
  value PlaceOnTop
  value PlaceOnBottom
}

(* protocol families *)
enum{
  value FamilyInternet
  value FamilyDECnet
  value FamilyChaos
}

(* Property notification *)
enum{
  value PropertyNewValue
  value PropertyDelete
}

(* Color Map notification *)
enum{
  value ColormapUninstalled
  value ColormapInstalled
}

(* GrabPointer, GrabButton, GrabKeyboard, GrabKey Modes *)
enum{
  value GrabModeSync
  value GrabModeAsync
}

(* GrabPointer, GrabKeyboard reply status *)
enum{
  value GrabSuccess
  value AlreadyGrabbed
  value GrabInvalidTime
  value GrabNotViewable
  value GrabFrozen
}

(* AllowEvents modes *)
enum{
  value AsyncPointer
  value SyncPointer
  value ReplayPointer
  value AsyncKeyboard
  value SyncKeyboard
  value ReplayKeyboard
  value AsyncBoth
  value SyncBoth
}

(* Used in SetInputFocus, GetInputFocus *)

None constant RevertToNone
PointerRoot constant RevertToPointerRoot
2 constant RevertToParent

(* ***************************************************************
 * ERROR CODES
 **************************************************************** *)
enum{
  value Success (* everything's okay *)
  value BadRequest (* bad request code *)
  value BadValue (* int parameter out of range *)
  value BadWindow (* parameter not a Window *)
  value BadPixmap (* parameter not a Pixmap *)
  value BadAtom (* parameter not an Atom *)
  value BadCursor (* parameter not a Cursor *)
  value BadFont (* parameter not a Font *)
  value BadMatch (* parameter mismatch *)
  value BadDrawable (* parameter not a Pixmap or Window *)
  value BadAccess (* depending on context:
     - key/button already grabbed
     - attempt to free an illegal
       cmap entry
    - attempt to store into a read-only
       color map entry.
     - attempt to modify the access control
       list from other than the local host.
    *)
  value BadAlloc (* insufficient resources *)
  value BadColor (* no such colormap *)
  value BadGC (* parameter not a GC *)
  value BadIDChoice (* choice not in range or already used *)
  value BadName (* font or color name doesn't exist *)
  value BadLength (* Request length incorrect *)
  value BadImplementation (* server is defective *)
}

128 constant FirstExtensionError
255 constant LastExtensionError

(* ***************************************************************
 * WINDOW DEFINITIONS
 **************************************************************** *)

(* Window classes used by CreateWindow *)
(* Note that CopyFromParent is already defined as 0 above *)

enum{
  1 set
  value InputOutput
  value InputOnly
}

(* Window attributes for CreateWindow and ChangeWindowAttributes *)
bitenum{
  value CWBackPixmap
  value CWBackPixel
  value CWBorderPixmap
  value CWBorderPixel
  value CWBitGravity
  value CWWinGravity
  value CWBackingStore
  value CWBackingPlanes
  value CWBackingPixel
  value CWOverrideRedirect
  value CWSaveUnder
  value CWEventMask
  value CWDontPropagate
  value CWColormap
  value CWCursor
}

(* ConfigureWindow structure *)
bitenum{
  value CWX
  value CWY
  value CWWidth
  value CWHeight
  value CWBorderWidth
  value CWSibling
  value CWStackMode
}

(* Bit Gravity *)
enum{
  value ForgetGravity
  value NorthWestGravity
  value NorthGravity
  value NorthEastGravity
  value WestGravity
  value CenterGravity
  value EastGravity
  value SouthWestGravity
  value SouthGravity
  value SouthEastGravity
  value StaticGravity
}

(* Window gravity + bit gravity above *)
0 constant UnmapGravity

(* Used in CreateWindow for backing-store hint *)
enum{
  value NotUseful
  value WhenMapped
  value Always
}

(* Used in GetWindowAttributes reply *)
enum{
  value IsUnmapped
  value IsUnviewable
  value IsViewable
}

(* Used in ChangeSaveSet *)
enum{
  value SetModeInsert
  value SetModeDelete
}

(* Used in ChangeCloseDownMode *)
enum{
  value DestroyAll
  value RetainPermanent
  value RetainTemporary
}

(* Window stacking method (in configureWindow) *)
enum{
  value Above
  value Below
  value TopIf
  value BottomIf
  value Opposite
}

(* Circulation direction *)
enum{
  value RaiseLowest
  value LowerHighest
}

(* Property modes *)
enum{
  value PropModeReplace
  value PropModePrepend
  value PropModeAppend
}

(* ***************************************************************
 * GRAPHICS DEFINITIONS
 **************************************************************** *)

(* graphics functions, as in GC.alu *)

enum{
  value GXclear (* 0 *)
  value GXand (* src AND dst *)
  value GXandReverse (* src AND NOT dst *)
  value GXcopy (* src *)
  value GXandInverted (* NOT src AND dst *)
  value GXnoop (* dst *)
  value GXxor (* src XOR dst *)
  value GXor (* src OR dst *)
  value GXnor (* NOT src AND NOT dst *)
  value GXequiv (* NOT src XOR dst *)
  value GXinvert (* NOT dst *)
  value GXorReverse (* src OR NOT dst *)
  value GXcopyInverted (* NOT src *)
  value GXorInverted (* NOT src OR dst *)
  value GXnand (* NOT src OR NOT dst *)
  value GXset (* 1 *)
}

(* LineStyle *)
enum{
  value LineSolid
  value LineOnOffDash
  value LineDoubleDash
}

(* capStyle *)
enum{
  value CapNotLast
  value CapButt
  value CapRound
  value CapProjecting
}

(* joinStyle *)
enum{
  value JoinMiter
  value JoinRound
  value JoinBevel
}

(* fillStyle *)
enum{
  value FillSolid
  value FillTiled
  value FillStippled
  value FillOpaqueStippled
}

(* fillRule *)
enum{
  value EvenOddRule
  value WindingRule
}

(* subwindow mode *)
enum{
  value ClipByChildren
  value IncludeInferiors
}

(* SetClipRectangles ordering *)
enum{
  value Unsorted
  value YSorted
  value YXSorted
  value YXBanded
}

(* CoordinateMode for drawing routines *)
enum{
  value CoordModeOrigin (* relative to the origin *)
  value CoordModePrevious (* relative to previous point *)
}

(* Polygon shapes *)
enum{
  value Complex (* paths may intersect *)
  value Nonconvex (* no paths intersect, but not convex *)
  value Convex (* wholly convex *)
}

(* Arc modes for PolyFillArc *)
enum{
  value ArcChord (* join endpoints of arc *)
  value ArcPieSlice (* join endpoints to center of arc *)
}

(* GC components: masks used in CreateGC, CopyGC, ChangeGC, OR'ed into
   GC.stateChanges *)
bitenum{
  value GCFunction
  value GCPlaneMask
  value GCForeground
  value GCBackground
  value GCLineWidth
  value GCLineStyle
  value GCCapStyle
  value GCJoinStyle
  value GCFillStyle
  value GCFillRule
  value GCTile
  value GCStipple
  value GCTileStipXOrigin
  value GCTileStipYOrigin
  value GCFont
  value GCSubwindowMode
  value GCGraphicsExposures
  value GCClipXOrigin
  value GCClipYOrigin
  value GCClipMask
  value GCDashOffset
  value GCDashList
  value GCArcMode
}

22 constant GCLastBit


(* ***************************************************************
 * FONTS
 **************************************************************** *)

(* used in QueryFont -- draw direction *)
enum{
  value FontLeftToRight
  value FontRightToLeft
}

255 constant FontChange

(* ***************************************************************
 *  IMAGING
 **************************************************************** *)

(* ImageFormat -- PutImage, GetImage *)
enum{
  value XYBitmap (* depth 1, XYFormat *)
  value XYPixmap (* depth == drawable depth *)
  value ZPixmap (* depth == drawable depth *)
}

(* ***************************************************************
 *  COLOR MAP STUFF
 **************************************************************** *)

(* For CreateColormap *)
enum{
  value AllocNone (* create map with no entries *)
  value AllocAll (* allocate entire map writeable *)
}

(* Flags used in StoreNamedColor, StoreColors *)
bitenum{
  value DoRed
  value DoGreen
  value DoBlue
}

(* ***************************************************************
 * CURSOR STUFF
 **************************************************************** *)

(* QueryBestSize Class *)
enum{
  value CursorShape (* largest size that can be displayed *)
  value TileShape (* size tiled fastest *)
  value StippleShape (* size stippled fastest *)
}


(* ***************************************************************
 * KEYBOARD/POINTER STUFF
 **************************************************************** *)

enum{
  value AutoRepeatModeOff
  value AutoRepeatModeOn
  value AutoRepeatModeDefault
}

enum{
  value LedModeOff
  value LedModeOn
}

(* masks for ChangeKeyboardControl *)
bitenum{
  value KBKeyClickPercent
  value KBBellPercent
  value KBBellPitch
  value KBBellDuration
  value KBLed
  value KBLedMode
  value KBKey
  value KBAutoRepeatMode
}

enum{
  value MappingSuccess
  value MappingBusy
  value MappingFailed
}

enum{
  value MappingModifier
  value MappingKeyboard
  value MappingPointer
}

(* ***************************************************************
 * SCREEN SAVER STUFF
 **************************************************************** *)

enum{
  value DontPreferBlanking
  value PreferBlanking
  value DefaultBlanking
}

0 constant DisableScreenSaver
0 constant DisableScreenInterval

enum{
  value DontAllowExposures
  value AllowExposures
  value DefaultExposures
}

(* for ForceScreenSaver *)
enum{
  value ScreenSaverReset
  value ScreenSaverActive
}

(* ***************************************************************
 * HOSTS AND CONNECTIONS
 **************************************************************** *)

(* for ChangeHosts *)
enum{
  value HostInsert
  value HostDelete
}

(* for ChangeAccessControl *)
enum{
  value EnableAccess
  value DisableAccess
}

(* Display classes  used in opening the connection
 * Note that the statically allocated ones are even numbered and the
 * dynamically changeable ones are odd numbered *)
enum{
  value StaticGray
  value GrayScale
  value StaticColor
  value PseudoColor
  value TrueColor
  value DirectColor
}

(* Byte order  used in imageByteOrder and bitmapBitOrder *)
enum{
  value LSBFirst
  value MSBFirst
}

(* $XConsortium: Xlib.h,v 11.237 94/09/01 18:44:49 kaleb Exp $ *)
(* $XFree86: xc/lib/X11/Xlib.h,v 3.2 1994/09/17 13:44:15 dawes Exp $ *)
(*

Copyright (c) 1985, 1986, 1987, 1991  X Consortium

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the X Consortium shall not be
used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from the X Consortium.

 *)


(*
 * Xlib.h - Header definition and support file for the C subroutine
 * interface library (Xlib) to the X Window System Protocol (V11).
 * Structures and symbols starting with "_" are private to the library.
 *)

6 constant XlibSpecificationRelease

enum{
  value QueuedAlready
  value QueuedAfterReading
  value QueuedAfterFlush
}

(*
 * Extensions need a way to hang private data on some structures.
 *)
struct{
 cell number       (* number returned by XRegisterExtension *)
 ptr  next         (* next item on list of data for structure *)
 ptr  free_private (* called to free private storage *)
 ptr  private_data (* data private to this extension. *)
} XExtData

(*
 * This file contains structures used by the extension mechanism.
 *)
struct{  (* public to extension, cannot be changed *)
 cell extension  (* extension number *)
 cell major_opcode (* major op-code assigned by server *)
 cell first_event (* first event number for the extension *)
 cell first_error (* first error number for the extension *)
} XExtCodes

(*
 * Data structure for retrieving info about pixmap formats.
 *)

struct{
    cell depth
    cell bits_per_pixel
    cell scanline_pad
} XPixmapFormatValues


(*
 * Data structure for setting graphics context.
 *)
struct{
 cell function  (* logical operation *)
 cell plane_mask (* plane mask *)
 cell foreground (* foreground pixel *)
 cell background (* background pixel *)
 cell line_width  (* line width *)
 cell line_style   (* LineSolid, LineOnOffDash, LineDoubleDash *)
 cell cap_style    (* CapNotLast, CapButt,  CapRound, CapProjecting *)
 cell join_style   (* JoinMiter, JoinRound, JoinBevel *)
 cell fill_style   (* FillSolid, FillTiled,  FillStippled, FillOpaeueStippled *)
 cell fill_rule    (* EvenOddRule, WindingRule *)
 cell arc_mode  (* ArcChord, ArcPieSlice *)
 cell tile  (* tile pixmap for tiling operations *)
 cell stipple  (* stipple 1 plane pixmap for stipping *)
 cell ts_x_origin (* offset for tile or stipple operations *)
 cell ts_y_origin
 ptr font         (* default text font for text operations *)
 cell subwindow_mode     (* ClipByChildren, IncludeInferiors *)
 cell graphics_exposures (* boolean, should exposures be generated *)
 cell clip_x_origin (* origin for clipping *)
 cell clip_y_origin
 cell clip_mask (* bitmap clipping; other calls for rects *)
 cell dash_offset (* patterned/dashed line information *)
 byte dashes
} XGCValues

(*
 * Graphics context.  The contents of this structure are implementation
 * dependent.  A GC should be treated as opaque by application code.
 *)

struct{
    ptr ext_data (* hook for extension to hang data *)
    ptr gid (* protocol ID for graphics context *)
    (* there is more to this structure, but it is private to Xlib *)
} GC

(*
 * Visual structure contains information about colormapping possible.
 *)
struct{
 ptr ext_data (* hook for extension to hang data *)
 cell visualid (* visual id of this visual *)
 cell class  (* class of screen (monochrome, etc.) *)
 cell red_mask cell green_mask cell blue_mask (* mask values *)
 cell bits_per_rgb (* log base 2 of distinct color values *)
 cell map_entries (* color map entries *)
} Visual

(*
 * Depth structure contains information for each possible depth.
 *)
struct{
 cell depth  (* this depth (Z) of the depth *)
 cell nvisuals  (* number of Visual types at this depth *)
 ptr visuals (* list of visuals possible at this depth *)
} Depth

(*
 * Information about the screen.  The contents of this structure are
 * implementation dependent.  A Screen should be treated as opaque
 * by application code.
 *)

struct{
 ptr ext_data (* hook for extension to hang data *)
 ptr display (* back pointer to display structure *)
 cell root  (* Root window id. *)
 cell width   cell height (* width and height of screen *)
 cell mwidth   cell mheight (* width and height of  in millimeters *)
 cell ndepths  (* number of depths possible *)
 ptr depths  (* list of allowable depths on the screen *)
 cell root_depth  (* bits per pixel *)
 ptr root_visual (* root visual *)
 cell default_gc  (* GC for the root root visual *)
 cell cmap  (* default color map *)
 cell white_pixel
 cell black_pixel (* White and Black pixel values *)
 cell max_maps   cell min_maps (* max and min color maps *)
 cell backing_store (* Never, WhenMapped, Always *)
 cell save_unders
 cell root_input_mask (* initial root input mask *)
} Screen

(*
 * Format structure; describes ZFormat data the screen will understand.
 *)
struct{
 ptr ext_data (* hook for extension to hang data *)
 cell depth  (* depth of this image format *)
 cell bits_per_pixel (* bits/pixel at this depth *)
 cell scanline_pad (* scanline must padded to this multiple *)
} ScreenFormat

(*
 * Data structure for setting window attributes.
 *)
struct{
    cell background_pixmap (* background or None or ParentRelative *)
    cell background_pixel (* background pixel *)
    cell border_pixmap (* border of the window *)
    cell border_pixel (* border pixel value *)
    cell bit_gravity  (* one of bit gravity values *)
    cell win_gravity  (* one of the window gravity values *)
    cell backing_store  (* NotUseful, WhenMapped, Always *)
    cell backing_planes (* planes to be preseved if possible *)
    cell backing_pixel (* value to use in restoring planes *)
    cell save_under  (* should bits under be saved? (popups) *)
    cell event_mask  (* set of events that should be saved *)
    cell do_not_propagate_mask (* set of events that should not propagate *)
    cell override_redirect (* boolean value for override-redirect *)
    cell colormap  (* color map to be associated with window *)
    cell cursor  (* cursor to be displayed (or None) *)
} XSetWindowAttributes

struct{
    cell x  cell y   (* location of window *)
    cell width   cell height  (* width and height of window *)
    cell border_width  (* border width of window *)
    cell depth           (* depth of window *)
    ptr visual  (* the associated visual structure *)
    cell root         (* root of screen containing window *)
    cell class   (* InputOutput, InputOnly *)
    cell bit_gravity  (* one of bit gravity values *)
    cell win_gravity  (* one of the window gravity values *)
    cell backing_store  (* NotUseful, WhenMapped, Always *)
    cell backing_planes (* planes to be preserved if possible *)
    cell backing_pixel (* value to be used when restoring planes *)
    cell save_under  (* boolean, should bits under be saved? *)
    ptr colormap  (* color map to be associated with window *)
    cell map_installed  (* boolean, is color map currently installed *)
    cell map_state  (* IsUnmapped, IsUnviewable, IsViewable *)
    cell all_event_masks (* set of events all people have interest in *)
    cell your_event_mask (* my event mask *)
    cell do_not_propagate_mask (* set of events that should not propagate *)
    cell override_redirect (* boolean value for override-redirect *)
    ptr screen  (* back pointer to correct screen *)
} XWindowAttributes

(*
 * Data structure for host setting; getting routines.
 *
 *)

struct{
 cell family  (* for example Familyinternet *)
 cell length  (* length of address, in bytes *)
 ptr address  (* pointer to where to find the bytes *)
} XHostAddress

(*
 * Data structure for "image" data, used by image manipulation routines.
 *)
struct{
    cell width   cell height  (* size of image *)
    cell xoffset  (* number of pixels offset in X direction *)
    cell format   (* XYBitmap, XYPixmap, ZPixmap *)
    ptr data   (* pointer to image data *)
    cell byte_order  (* data byte order, LSBFirst, MSBFirst *)
    cell bitmap_unit  (* quant. of scanline 8, 16, 32 *)
    cell bitmap_bit_order (* LSBFirst, MSBFirst *)
    cell bitmap_pad  (* 8, 16, 32 either XY or ZPixmap *)
    cell depth   (* depth of image *)
    cell bytes_per_line  (* accelarator to next line *)
    cell bits_per_pixel  (* bits per pixel (ZPixmap) *)
    cell red_mask (* bits in z arrangment *)
    cell green_mask
    cell blue_mask
    ptr obdata  (* hook for the object routines to hang on *)
    {  (* image manipulation routines *)
       ptr create_image
       ptr destroy_image
       ptr get_pixel
       ptr put_pixel
       ptr sub_image
       ptr add_pixel
    }
} XImage

(*
 * Data structure for XReconfigureWindow
 *)
struct{
    cell x  cell y
    cell width   cell height
    cell border_width
    cell sibling
    cell stack_mode
} XWindowChanges

(*
 * Data structure used by color operations
 *)
struct{
 cell pixel
 short red  short green  short blue
 byte flags  (* do_red, do_green, do_blue *)
 byte pad
} XColor

(*
 * Data structures for graphics operations.  On most machines, these are
 * congruent with the wire protocol structures, so reformatting the data
 * can be avoided on these architectures.
 *)
struct{
    short x1  short  y1  short x2  short y2
} XSegment

struct{
    short x   short y
} XPoint

struct{
    short x   short y
    short width   short height
} XRectangle

struct{
    short x   short y
    short width   short height
    short angle1  short angle2
} XArc


(* Data structure for XChangeKeyboardControl *)

struct{
        cell key_click_percent
        cell bell_percent
        cell bell_pitch
        cell bell_duration
        cell led
        cell led_mode
        cell key
        cell auto_repeat_mode   (* On, Off, Default *)
} XKeyboardControl

(* Data structure for XGetKeyboardControl *)

struct{
 cell key_click_percent
 cell bell_percent
 cell bell_pitch   cell bell_duration
 cell led_mask
 cell global_auto_repeat
 32 string auto_repeats
} XKeyboardState

(* Data structure for XGetMotionEvents.  *)

struct{
        cell time
 short x   short y
} XTimeCoord

(* Data structure for X{Set,Get}ModifierMapping *)

struct{
  cell max_keypermod (* The server's max # of keys per modifier *)
  ptr modifiermap (* An 8 by max_keypermod array of modifiers *)
} XModifierKeymap


(*
 * Display datatype maintaining display specific data.
 * The contents of this structure are implementation dependent.
 * A Display should be treated as opaque by application code.
 *)

struct{
 ptr ext_data (* hook for extension to hang data *)
 ptr  private1
 cell fd   (* Network socket. *)
 cell private2
 cell proto_major_version (* major version of server's X protocol *)
 cell proto_minor_version (* minor version of servers X protocol *)
 ptr vendor  (* vendor of the server hardware *)
 ptr private3
 ptr private4
 ptr private5
 cell private6
 ptr resource_alloc (* allocator function *)
 cell byte_order  (* screen byte order, LSBFirst, MSBFirst *)
 cell bitmap_unit (* padding and data requirements *)
 cell bitmap_pad  (* padding requirements on bitmaps *)
 cell bitmap_bit_order (* LeastSignificant or MostSignificant *)
 cell nformats  (* number of pixmap formats in list *)
 ptr pixmap_format (* pixmap format list *)
 cell private8
 cell release  (* release of the server *)
 ptr private9  ptr private10
 cell qlen  (* Length of input event queue *)
 cell last_request_read (* seq number of last event read *)
 cell request (* sequence number of last request. *)
 ptr private11
 ptr private12
 ptr private13
 ptr private14
 cell max_request_size (* maximum number 32 bit words in request *)
 ptr db
 ptr private15
 ptr display_name (* "host:display" string used on this connect *)
 cell default_screen (* default screen for operations *)
 cell nscreens  (* number of screens on this server *)
 ptr screens (* pointer to list of screens *)
 cell motion_buffer (* size of motion buffer *)
 cell private16
 cell min_keycode (* minimum defined keycode *)
 cell max_keycode (* maximum defined keycode *)
 ptr private17
 ptr private18
 cell private19
 ptr xdefaults (* contents of defaults from server *)
 (* there is more to this structure, but it is private to Xlib *)
} Display

(*
 * Definitions of specific events.
 *)
struct{
 cell type  (* of event *)
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window         (* "event" window it is reported relative to *)
 cell root         (* root window that the event occured on *)
 cell subwindow (* child window *)
 cell time  (* milliseconds *)
 cell x  cell y  (* pointer x, y coordinates in event window *)
 cell x_root  cell y_root (* coordinates relative to root *)
 cell state (* key or button mask *)
 cell keycode (* detail *)
 cell same_screen (* same screen flag *)
} XKeyEvent
alias XKeyEvent XKeyPressedEvent
alias XKeyEvent XKeyReleasedEvent
struct{
 cell type  (* of event *)
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window         (* "event" window it is reported relative to *)
 cell root         (* root window that the event occured on *)
 cell subwindow (* child window *)
 cell time  (* milliseconds *)
 cell x  cell y  (* pointer x, y coordinates in event window *)
 cell x_root  cell y_root (* coordinates relative to root *)
 cell state (* key or button mask *)
 cell button (* detail *)
 cell same_screen (* same screen flag *)
} XButtonEvent
alias XButtonEvent XButtonPressedEvent
alias XButtonEvent XButtonReleasedEvent
struct{
 cell type  (* of event *)
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window         (* "event" window reported relative to *)
 cell root         (* root window that the event occured on *)
 cell subwindow (* child window *)
 cell time  (* milliseconds *)
 cell x  cell y  (* pointer x, y coordinates in event window *)
 cell x_root  cell y_root (* coordinates relative to root *)
 cell state (* key or button mask *)
 byte is_hint  (* detail *)
 cell same_screen (* same screen flag *)
} XMotionEvent
alias XMotionEvent XPointerMovedEvent

struct{
 cell type  (* of event *)
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window         (* "event" window reported relative to *)
 cell root         (* root window that the event occured on *)
 cell subwindow (* child window *)
 cell time  (* milliseconds *)
 cell x  cell y  (* pointer x, y coordinates in event window *)
 cell x_root  cell y_root (* coordinates relative to root *)
 cell mode  (* NotifyNormal, NotifyGrab, NotifyUngrab *)
 cell detail
 (*
  * NotifyAncestor, NotifyVirtual, NotifyInferior,
  * NotifyNonlinear,NotifyNonlinearVirtual
  *)
 cell same_screen (* same screen flag *)
 cell focus  (* boolean focus *)
 cell state (* key or button mask *)
} XCrossingEvent
alias XCrossingEvent XEnterWindowEvent
alias XCrossingEvent XLeaveWindowEvent

struct{
 cell type  (* FocusIn or FocusOut *)
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window  (* window of event *)
 cell mode  (* NotifyNormal, NotifyGrab, NotifyUngrab *)
 cell detail
 (*
  * NotifyAncestor, NotifyVirtual, NotifyInferior,
  * NotifyNonlinear,NotifyNonlinearVirtual, NotifyPointer,
  * NotifyPointerRoot, NotifyDetailNone
  *)
} XFocusChangeEvent
alias XFocusChangeEvent XFocusInEvent
alias XFocusChangeEvent XFocusOutEvent

(* generated on EnterWindow and FocusIn  when KeyMapState selected *)
struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 &d32 string key_vector
} XKeymapEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 cell x  cell y
 cell width   cell height
 cell count  (* if non-zero, at least this many more *)
} XExposeEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell drawable
 cell x  cell y
 cell width   cell height
 cell count  (* if non-zero, at least this many more *)
 cell major_code  (* core is CopyArea or CopyPlane *)
 cell minor_code  (* not defined in the core *)
} XGraphicsExposeEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell drawable
 cell major_code  (* core is CopyArea or CopyPlane *)
 cell minor_code  (* not defined in the core *)
} XNoExposeEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 cell state  (* Visibility state *)
} XVisibilityEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell parent  (* parent of the window *)
 cell window  (* window id of window created *)
 cell x  cell y  (* window location *)
 cell width   cell height (* size of window *)
 cell border_width (* border width *)
 cell override_redirect (* creation should be overridden *)
} XCreateWindowEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
} XDestroyWindowEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
 cell from_configure
} XUnmapEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
 cell override_redirect (* boolean, is override set... *)
} XMapEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell parent
 cell window
} XMapRequestEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
 cell parent
 cell x  cell y
 cell override_redirect
} XReparentEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
 cell x  cell y
 cell width   cell height
 cell border_width
 cell above
 cell override_redirect
} XConfigureEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
 cell x  cell y
} XGravityEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 cell width   cell height
} XResizeRequestEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell parent
 cell window
 cell x  cell y
 cell width   cell height
 cell border_width
 cell above
 cell detail  (* Above, Below, TopIf, BottomIf, Opposite *)
 cell value_mask
} XConfigureRequestEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell event
 cell window
 cell place  (* PlaceOnTop, PlaceOnBottom *)
} XCirculateEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell parent
 cell window
 cell place  (* PlaceOnTop, PlaceOnBottom *)
} XCirculateRequestEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 cell atom
 cell time
 cell state  (* NewValue, Deleted *)
} XPropertyEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 cell selection
 cell time
} XSelectionClearEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell owner
 cell requestor
 cell selection
 cell target
 cell property
 cell time
} XSelectionRequestEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell requestor
 cell selection
 cell target
 cell property  (* ATOM or None *)
 cell time
} XSelectionEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 ptr colormap (* COLORMAP or None *)
 cell new
 cell state  (* ColormapInstalled, ColormapUninstalled *)
} XColormapEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window
 cell message_type
 cell format
 &d20 string data
} XClientMessageEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window  (* unused *)
 cell request  (* one of MappingModifier, MappingKeyboard, MappingPointer *)
 cell first_keycode (* first keycode *)
 cell count  (* defines range of change w. first_keycode *)
} XMappingEvent

struct{
 cell type
 ptr display (* Display the event was read from *)
 ptr resourceid  (* resource id *)
 cell serial (* serial number of failed request *)
 byte error_code (* error code of failed request *)
 byte request_code (* Major op-code of failed request *)
 byte minor_code (* Minor op-code of failed request *)
} XErrorEvent

struct{
 cell type
 cell serial (* # of last request processed by server *)
 cell send_event (* true if this came from a SendEvent request *)
 ptr display (* Display the event was read from *)
 cell window (* window on which event was requested in event mask *)
} XAnyEvent

(*
 * this union is defined so Xlib can always use the same sized
 * event structure internally, to avoid memory fragmentation.
 *)
struct{ {
  cell type  (* must not be changed first element *)
| struct XAnyEvent xany
| struct XKeyEvent xkey
| struct XButtonEvent xbutton
| struct XMotionEvent xmotion
| struct XCrossingEvent xcrossing
| struct XFocusChangeEvent xfocus
| struct XExposeEvent xexpose
| struct XGraphicsExposeEvent xgraphicsexpose
| struct XNoExposeEvent xnoexpose
| struct XVisibilityEvent xvisibility
| struct XCreateWindowEvent xcreatewindow
| struct XDestroyWindowEvent xdestroywindow
| struct XUnmapEvent xunmap
| struct XMapEvent xmap
| struct XMapRequestEvent xmaprequest
| struct XReparentEvent xreparent
| struct XConfigureEvent xconfigure
| struct XGravityEvent xgravity
| struct XResizeRequestEvent xresizerequest
| struct XConfigureRequestEvent xconfigurerequest
| struct XCirculateEvent xcirculate
| struct XCirculateRequestEvent xcirculaterequest
| struct XPropertyEvent xproperty
| struct XSelectionClearEvent xselectionclear
| struct XSelectionRequestEvent xselectionrequest
| struct XSelectionEvent xselection
| struct XColormapEvent xcolormap
| struct XClientMessageEvent xclient
| struct XMappingEvent xmapping
| struct XErrorEvent xerror
| struct XKeymapEvent xkeymap
| &d96 string pad
} } XEvent

\ TODO!
\ : XAllocID ( dpy -- id )  dup >r Display resource_alloc perform rdrop ; macro

(*
 * per character font metric information.
 *)
struct{
    short lbearing (* origin to left edge of raster *)
    short rbearing (* origin to right edge of raster *)
    short width  (* advance to next char's origin *)
    short ascent  (* baseline to top edge of raster *)
    short descent (* baseline to bottom edge of raster *)
    short attributes (* per char flags (not predefined) *)
} XCharStruct

(*
 * To allow arbitrary information with fonts, there are additional properties
 * returned.
 *)
struct{
    cell name
    cell card32
} XFontProp

struct{
    ptr ext_data (* hook for extension to hang data *)
    ptr        fid            (* Font id for this font *)
    cell direction (* hint about direction the font is painted *)
    cell min_char_or_byte2 (* first character *)
    cell max_char_or_byte2 (* last character *)
    cell min_byte1 (* first row that exists *)
    cell max_byte1 (* last row that exists *)
    cell all_chars_exist (* flag if all characters have non-zero size *)
    cell default_char (* char to print for undefined character *)
    cell         n_properties   (* how many properties there are *)
    ptr properties (* pointer to array of additional properties *)
    struct XCharStruct min_bounds (* minimum bounds over all existing char *)
    struct XCharStruct max_bounds (* maximum bounds over all existing char *)
    ptr per_char (* first_char to last_char information *)
    cell  ascent  (* log. extent above baseline for spacing *)
    cell  descent (* log. descent below baseline for spacing *)
} XFontStruct

(*
 * PolyText routines take these as arguments.
 *)
struct{
    ptr chars  (* pointer to string *)
    cell nchars   (* number of characters *)
    cell delta   (* delta between strings *)
    ptr font   (* font to print it in, None don't change *)
} XTextItem

struct{  (* normal 16 bit characters are two bytes *)
    byte byte1
    byte byte2
} XChar2b

struct{
    ptr chars  (* two byte characters *)
    cell nchars   (* number of characters *)
    cell delta   (* delta between strings *)
    ptr font   (* font to print it in, None don't change *)
} XTextItem16


struct{ {
  ptr display
| cell gc
| ptr visual
| ptr screen
| ptr pixmap_format
| ptr font } } XEDataObject

struct{
    struct XRectangle      max_ink_extent
    struct XRectangle      max_logical_extent
} XFontSetExtents

struct{
    ptr chars
    cell nchars
    cell delta
    ptr font_set
} XmbTextItem

struct{
    ptr chars
    cell nchars
    cell delta
    ptr font_set
} XwcTextItem


struct{
    cell charset_count
    ptr charset_list
} XOMCharSetList


enum{
  value XOMOrientation_LTR_TTB
  value XOMOrientation_RTL_TTB
  value XOMOrientation_TTB_LTR
  value XOMOrientation_TTB_RTL
  value XOMOrientation_Context
}

struct{
    cell num_orient
    ptr orient (* Input Text description *)
} XOMOrientation

struct{
    cell num_font
    ptr font_struct_list
    ptr font_name_list
} XOMFontInfo

struct{
    short count_styles
    ptr supported_styles
} XIMStyles

bitenum{
  value XIMPreeditArea
  value XIMPreeditCallbacks
  value XIMPreeditPosition
  value XIMPreeditNothing
  value XIMPreeditNone
  0x100 set
  value XIMStatusArea
  value XIMStatusCallbacks
  value XIMStatusNothing
  value XIMStatusNone
}

XIMPreeditPosition XIMPreeditArea or
XIMPreeditNothing or XIMPreeditNone or constant XIMPreedit
XIMStatusArea XIMStatusNothing or XIMStatusNone or constant XIMStatus
XIMStatus XIMPreedit or constant XIMSupported

-1 constant XBufferOverflow
enum{
  1 set
  value XLookupNone
  value XLookupChars
  value XLookupKeySym
  value XLookupBoth
}

struct{
    ptr client_data
    ptr callback
} XIMCallback

bitenum{
  value XIMReverse
  value XIMUnderline
  value XIMHighlight
  5 set-bit
  value XIMPrimary
  value XIMSecondary
  value XIMTertiary
  value XIMVisibleToForward
  value XIMVisibleToBackword
  value XIMVisibleToCenter
}

struct{
    short length
    ptr feedback
    cell encoding_is_wchar
    ptr string
} XIMText

bitenum{
  0 set
  value XIMPreeditUnKnown
  value XIMPreeditEnable
  value XIMPreeditDisable
}

struct{
   cell state
} XIMPreeditStateNotifyCallback

bitenum{
  value XIMInitialState
  value XIMPreserveState
}

bitenum{
  value XIMStringConversionLeftEdge
  value XIMStringConversionRightEdge
  value XIMStringConversionTopEdge
  value XIMStringConversionBottomEdge
  value XIMStringConversionConcealed
  value XIMStringConversionWrapped
}

struct{
    short length
    ptr feedback
    cell encoding_is_wchar
    ptr string
} XIMStringConversionText

enum{
  1 set
  value XIMStringConversionBuffer
  value XIMStringConversionLine
  value XIMStringConversionWord
  value XIMStringConversionChar
}

enum{
  1 set
  value XIMStringConversionSubstitution
  value XIMStringConversionRetrival
}

struct{
    short position
    short type
    short operation
    short factor
    ptr text
} XIMStringConversionCallback

struct{
    cell caret  (* Cursor offset within pre-edit string *)
    cell chg_first (* Starting change position *)
    cell chg_length (* Length of the change in character count *)
    ptr text
} XIMPreeditDrawCallbackStruct

enum{
  value XIMForwardChar
  value XIMBackwardChar
  value XIMForwardWord
  value XIMBackwardWord
  value XIMCaretUp
  value XIMCaretDown
  value XIMNextLine
  value XIMPreviousLine
  value XIMLineStart
  value XIMLineEnd
  value XIMAbsolutePosition
  value XIMDontChange
}

enum{
  value XIMIsInvisible (* Disable caret feedback *)
  value XIMIsPrimary (* UI defined caret feedback *)
  value XIMIsSecondary (* UI defined caret feedback *)
}

struct{
    cell position   (* Caret offset within pre-edit string *)
    cell direction (* Caret moves direction *)
    cell style  (* Feedback of the caret *)
} XIMPreeditCaretCallbackStruct

enum{
  value XIMTextType
  value XIMBitmapType
}

struct{
    cell type
    {
      ptr text
    | cell bitmap
    }
} XIMStatusDrawCallbackStruct

struct{
    ptr  keysym
    cell   modifier
    cell   modifier_mask
} XIMHotKeyTrigger

struct{
    cell    num_hot_key
    ptr key
} XIMHotKeyTriggers

enum{
  1 set
  value XIMHotKeyStateON
  value XIMHotKeyStateOFF
}

struct{
    short count_values
    ptr supported_values
} XIMValuesList

: ScreenOfDisplay ( dpy scr -- ) Screen @sizeof u*
  swap Display screens @ + ; ( macro)
: ConnectionNumber ( dpy -- fd )  Display fd @ ; ( macro)
: RootWindow ( dpy scr -- w ) ScreenOfDisplay Screen root @ ; ( macro)
: DefaultScreen ( dpy -- scr )  Display default_screen @ ; ( macro)
: DefaultRootWindow ( dpy -- ) dup DefaultScreen RootWindow ; ( macro)
: DefaultVisual ( dpy scr -- visual ) ScreenOfDisplay Screen root_visual @ ; ( macro)
: DefaultGC ( dpy scr -- GC ) ScreenOfDisplay Screen default_gc @ ; ( macro)
: BlackPixel ( dpy scr -- ) ScreenOfDisplay Screen black_pixel @ ; ( macro)
: WhitePixel ( dpy scr -- ) ScreenOfDisplay Screen white_pixel @ ; ( macro)
0 constant AllPlanes
: QLength ( dpy -- n )  Display qlen @ ; ( macro)
: DisplayWidth ( dpy scr -- w ) ScreenOfDisplay Screen width @ ; ( macro)
: DisplayHeight ( dpy scr -- h ) ScreenOfDisplay Screen height @ ; ( macro)
: DisplayWidthMM ( dpy scr -- mw ) ScreenOfDisplay Screen mwidth @ ; ( macro)
: DisplayHeightMM ( dpy scr -- mh ) ScreenOfDisplay Screen mheight @ ; ( macro)
: DisplayPlanes ( dpy scr -- n ) ScreenOfDisplay Screen root_depth @ ; ( macro)
: DisplayCells ( dpy scr -- n ) DefaultVisual Visual map_entries @ ; ( macro)
: ScreenCount ( dpy -- n )  Display nscreens @ ; ( macro)
: ServerVendor ( dpy -- addr )  Display vendor @ ; ( macro)
: ProtocolVersion ( dpy -- n )  Display proto_major_version @ ; ( macro)
: ProtocolRevision ( dpy -- n )  Display proto_minor_version @ ; ( macro)
: VendorRelease ( dpy -- n )  Display release @ ; ( macro)
: DisplayString ( dpy -- addr )  Display display_name @ ; ( macro)
: DefaultDepth ( dpy scr -- n ) ScreenOfDisplay Screen root_depth @ ; ( macro)
: DefaultColormap ( dpy scr -- addr ) ScreenOfDisplay Screen cmap @ ; ( macro)
: BitmapUnit ( dpy -- n )  Display bitmap_unit @ ; ( macro)
: BitmapBitOrder ( dpy -- n )  Display bitmap_bit_order @ ; ( macro)
: BitmapPad ( dpy -- addr )  Display bitmap_pad @ ; ( macro)
: ImageByteOrder ( dpy -- n )  Display byte_order @ ; ( macro)
: NextRequest ( dpy -- n ) Display request @ 1+ ; ( macro)
: LastKnownRequestProcessed ( dpy -- n )  Display last_request_read @ ; ( macro)

(* macros for screen oriented applications (toolkit) *)

: DefaultScreenOfDisplay ( dpy -- scr )  dup DefaultScreen ScreenOfDisplay ; ( macro)
: DefaultVisualOfScreen ( s -- visual ) Screen root_visual @ ; ( macro)
: DisplayOfScreen ( s -- n )  Screen display @ ; ( macro)
: RootWindowOfScreen ( s -- n )  Screen root @ ; ( macro)
: BlackPixelOfScreen ( s -- n )  Screen black_pixel @ ; ( macro)
: WhitePixelOfScreen ( s -- n )  Screen white_pixel @ ; ( macro)
: DefaultColormapOfScreen ( s -- n ) Screen cmap @ ; ( macro)
: DefaultDepthOfScreen ( s -- n )  Screen root_depth @ ; ( macro)
: DefaultGCOfScreen ( s -- n )  Screen default_gc @ ; ( macro)
: WidthOfScreen ( s -- n )  Screen width @ ; ( macro)
: HeightOfScreen ( s -- n )  Screen height @ ; ( macro)
: WidthMMOfScreen ( s -- n )  Screen mwidth @ ; ( macro)
: HeightMMOfScreen ( s -- n )  Screen mheight @ ; ( macro)
: PlanesOfScreen ( s -- n )  Screen root_depth @ ; ( macro)
: CellsOfScreen ( s -- visual )  DefaultVisualOfScreen Visual map_entries @ ; ( macro)
: MinCmapsOfScreen ( s -- n )  Screen min_maps @ ; ( macro)
: MaxCmapsOfScreen ( s -- n )  Screen max_maps @ ; ( macro)
: DoesSaveUnders ( s -- n )  Screen save_unders @ ; ( macro)
: DoesBackingStore ( s -- n )  Screen backing_store @ ; ( macro)
: EventMaskOfScreen ( s -- n )  Screen root_input_mask @ ; ( macro)

(* You must include <X11/Xlib.h> before including this file *)

(*
 * Bitmask returned by XParseGeometry().  Each bit tells if the corresponding
 * value (x, y, width, height) was found in the parsed string.
 *)
bitenum{
  0 set
  value NoValue
  value XValue
  value YValue
  value WidthValue
  value HeightValue
  0x0F constant AllValues
  0x10 set
  value XNegative
  value YNegative
}

(*
 * new version containing base_width, base_height, and win_gravity fields;
 * used with WM_NORMAL_HINTS.
 *)
struct{
        cell flags      (* marks which fields in this structure are defined *)
        cell x  cell y          (* obsolete for new window mgrs, but clients *)
        cell width  cell height (* should set so old wm's don't mess up *)
        cell min_width  cell min_height
        cell max_width  cell max_height
        cell width_inc  cell height_inc
        cell min_aspect.x  cell min_aspect.y
        cell max_aspect.x  cell max_aspect.y
        cell base_width  cell base_height       (* added by ICCCM version 1 *)
        cell win_gravity                        (* added by ICCCM version 1 *)
} XSizeHints

(*
 * The next block of definitions are for window manager properties that
 * clients and applications use for communication.
 *)

(* flags argument in size hints *)
bitenum{
  value USPosition (* user specified x, y *)
  value USSize (* user specified width, height *)

  value PPosition (* program specified position *)
  value PSize (* program specified size *)
  value PMinSize (* program specified minimum size *)
  value PMaxSize (* program specified maximum size *)
  value PResizeInc (* program specified resize increments *)
  value PAspect (* program specified min and max aspect ratios *)
  value PBaseSize (* program specified base for incrementing *)
  value PWinGravity (* program specified window gravity *)
}

(* obsolete *)
PPosition PSize or PMinSize or PMaxSize or PResizeInc or PAspect or  constant PAllHints



struct{
        cell flags      (* marks which fields in this structure are defined *)
        cell input      (* does this application rely on the window manager to
                        get keyboard input? *)
        cell initial_state      (* see below *)
        cell icon_pixmap        (* pixmap to be used as icon *)
        cell icon_window        (* window to be used as icon *)
        cell icon_x  cell icon_y        (* initial position of icon *)
        cell icon_mask  (* icon mask bitmap *)
        cell window_group       (* id of related window group *)
        (* this structure may be extended in the future *)
} XWMHints

(* definition for flags of XWMHints *)
bitenum{
  value InputHint
  value StateHint
  value IconPixmapHint
  value IconWindowHint
  value IconPositionHint
  value IconMaskHint
  value WindowGroupHint
    InputHint StateHint or IconPixmapHint or IconWindowHint or
    IconPositionHint or IconMaskHint or WindowGroupHint or constant AllHints
  8 set-bit
  value XUrgencyHint
}

(* definitions for initial window state *)
0 constant WithdrawnState       (* for windows that are not mapped *)
1 constant NormalState  (* most applications want to start this way *)
3 constant IconicState  (* application wants to start as an icon *)

(*
 * Obsolete states no longer defined by ICCCM
 *)
0 constant DontCareState        (* don't know or care *)
2 constant ZoomState    (* application wants to start zoomed *)
4 constant InactiveState        (* application believes it is seldom used; *)
                        (* some wm's may put it on inactive menu *)


(*
 * new structure for manipulating TEXT properties; used with WM_NAME,
 * WM_ICON_NAME, WM_CLIENT_MACHINE, and WM_COMMAND.
 *)
struct{
    cell value          (* same as Property routines *)
    cell encoding                       (* prop type *)
    cell format                         (* prop data format: 8, 16, or 32 *)
    cell nitems         (* number of data items in value *)
} XTextProperty

-1 constant XNoMemory
-2 constant XLocaleNotSupported
-3 constant XConverterNotFound

enum{
  value XStringStyle         (* STRING *)
  value XCompoundTextStyle   (* COMPOUND_TEXT *)
  value XTextStyle           (* text in owner's encoding (current locale) *)
  value XStdICCTextStyle     (* STRING, else COMPOUND_TEXT *)
}

struct{
        cell min_width cell min_height
        cell max_width cell max_height
        cell width_inc cell height_inc
} XIconSize

struct{
        cell res_name
        cell res_class
} XClassHint

(*
 * These macros are used to give some sugar to the image routines so that
 * naive people are more comfortable with them.
 *)

: XDestroyImage ( image -- )  dup XImage destroy_image @ 1 os:cinvoke drop ;
: XGetPixel ( y x image -- pixel )  dup XImage get_pixel @ 2 os:cinvoke ;
: XPutPixel ( pixel y x image -- pixel )  dup XImage put_pixel @ 4 os:cinvoke  ;
: XSubImage ( h w y x image -- )  dup XImage sub_image @ 5 os:cinvoke drop ;
: XAddPixel ( value image -- )  dup XImage get_pixel @ 2 os:cinvoke drop ;

(*
 * Compose sequence status structure, used in calling XLookupString.
 *)
struct{
    cell compose_ptr    (* state table pointer *)
    cell chars_matched          (* match state *)
} XComposeStatus

0xffffff constant XK_VoidSymbol

$FF08 constant XK_BackSpace             (* back space, back char *)
$FF09 constant XK_Tab
$FF0A constant XK_Linefeed              (* Linefeed, LF *)
$FF0B constant XK_Clear
$FF0D constant XK_Return                (* Return, enter *)
$FF13 constant XK_Pause                 (* Pause, hold *)
$FF14 constant XK_Scroll_Lock
$FF15 constant XK_Sys_Req
$FF1B constant XK_Escape
$FFFF constant XK_Delete                (* Delete, rubout *)

$FF50 constant XK_Home
$FF51 constant XK_Left                          (* Move left, left arrow *)
$FF52 constant XK_Up                            (* Move up, up arrow *)
$FF53 constant XK_Right                 (* Move right, right arrow *)
$FF54 constant XK_Down                          (* Move down, down arrow *)
$FF55 constant XK_Prior                 (* Prior, previous *)
$FF55 constant XK_Page_Up
$FF56 constant XK_Next                          (* Next *)
$FF56 constant XK_Page_Down
$FF57 constant XK_End                           (* EOL *)
$FF58 constant XK_Begin                 (* BOL *)

(* Misc Functions *)

$FF60 constant XK_Select                        (* Select, mark *)
$FF61 constant XK_Print
$FF62 constant XK_Execute                       (* Execute, run, do *)
$FF63 constant XK_Insert                        (* Insert, insert here *)
$FF65 constant XK_Undo                          (* Undo, oops *)
$FF66 constant XK_Redo                          (* redo, again *)
$FF67 constant XK_Menu
$FF68 constant XK_Find                          (* Find, search *)
$FF69 constant XK_Cancel                        (* Cancel, stop, abort, exit *)
$FF6A constant XK_Help                          (* Help *)
$FF6B constant XK_Break
$FF7E constant XK_Mode_switch                   (* Character set switch *)
$FF7E constant XK_script_switch          (* Alias for mode_switch *)
$FF7F constant XK_Num_Lock

(* Keypad Functions, keypad numbers cleverly chosen to map to ascii *)

$FF80 constant XK_KP_Space                      (* space *)
$FF89 constant XK_KP_Tab
$FF8D constant XK_KP_Enter                      (* enter *)
$FF91 constant XK_KP_F1                 (* PF1, KP_A, ... *)
$FF92 constant XK_KP_F2
$FF93 constant XK_KP_F3
$FF94 constant XK_KP_F4
$FF95 constant XK_KP_Home
$FF96 constant XK_KP_Left
$FF97 constant XK_KP_Up
$FF98 constant XK_KP_Right
$FF99 constant XK_KP_Down
$FF9A constant XK_KP_Prior
$FF9A constant XK_KP_Page_Up
$FF9B constant XK_KP_Next
$FF9B constant XK_KP_Page_Down
$FF9C constant XK_KP_End
$FF9D constant XK_KP_Begin
$FF9E constant XK_KP_Insert
$FF9F constant XK_KP_Delete
$FFBD constant XK_KP_Equal                      (* equals *)
$FFAA constant XK_KP_Multiply
$FFAB constant XK_KP_Add
$FFAC constant XK_KP_Separator                  (* separator, often comma *)
$FFAD constant XK_KP_Subtract
$FFAE constant XK_KP_Decimal
$FFAF constant XK_KP_Divide

$FFB0 constant XK_KP_0
$FFB1 constant XK_KP_1
$FFB2 constant XK_KP_2
$FFB3 constant XK_KP_3
$FFB4 constant XK_KP_4
$FFB5 constant XK_KP_5
$FFB6 constant XK_KP_6
$FFB7 constant XK_KP_7
$FFB8 constant XK_KP_8
$FFB9 constant XK_KP_9


(*
 * Auxilliary Functions; note the duplicate definitions for left and right
 * function keys;  Sun keyboards and a few other manufactures have such
 * function key groups on the left and/or right sides of the keyboard.
 * We've not found a keyboard with more than 35 function keys total.
 *)

$FFBE constant XK_F1
$FFBF constant XK_F2
$FFC0 constant XK_F3
$FFC1 constant XK_F4
$FFC2 constant XK_F5
$FFC3 constant XK_F6
$FFC4 constant XK_F7
$FFC5 constant XK_F8
$FFC6 constant XK_F9
$FFC7 constant XK_F10
$FFC8 constant XK_F11
$FFC8 constant XK_L1
$FFC9 constant XK_F12
$FFC9 constant XK_L2
$FFCA constant XK_F13
$FFCA constant XK_L3
$FFCB constant XK_F14
$FFCB constant XK_L4
$FFCC constant XK_F15
$FFCC constant XK_L5
$FFCD constant XK_F16
$FFCD constant XK_L6
$FFCE constant XK_F17
$FFCE constant XK_L7
$FFCF constant XK_F18
$FFCF constant XK_L8
$FFD0 constant XK_F19
$FFD0 constant XK_L9
$FFD1 constant XK_F20
$FFD1 constant XK_L10
$FFD2 constant XK_F21
$FFD2 constant XK_R1
$FFD3 constant XK_F22
$FFD3 constant XK_R2
$FFD4 constant XK_F23
$FFD4 constant XK_R3
$FFD5 constant XK_F24
$FFD5 constant XK_R4
$FFD6 constant XK_F25
$FFD6 constant XK_R5
$FFD7 constant XK_F26
$FFD7 constant XK_R6
$FFD8 constant XK_F27
$FFD8 constant XK_R7
$FFD9 constant XK_F28
$FFD9 constant XK_R8
$FFDA constant XK_F29
$FFDA constant XK_R9
$FFDB constant XK_F30
$FFDB constant XK_R10
$FFDC constant XK_F31
$FFDC constant XK_R11
$FFDD constant XK_F32
$FFDD constant XK_R12
$FFDE constant XK_F33
$FFDE constant XK_R13
$FFDF constant XK_F34
$FFDF constant XK_R14
$FFE0 constant XK_F35
$FFE0 constant XK_R15

(* Modifiers *)

$FFE1 constant XK_Shift_L                       (* Left shift *)
$FFE2 constant XK_Shift_R                       (* Right shift *)
$FFE3 constant XK_Control_L                     (* Left control *)
$FFE4 constant XK_Control_R                     (* Right control *)
$FFE5 constant XK_Caps_Lock                     (* Caps lock *)
$FFE6 constant XK_Shift_Lock                    (* Shift lock *)

$FFE7 constant XK_Meta_L                        (* Left meta *)
$FFE8 constant XK_Meta_R                        (* Right meta *)
$FFE9 constant XK_Alt_L                 (* Left alt *)
$FFEA constant XK_Alt_R                 (* Right alt *)
$FFEB constant XK_Super_L                       (* Left super *)
$FFEC constant XK_Super_R                       (* Right super *)
$FFED constant XK_Hyper_L                       (* Left hyper *)
$FFEE constant XK_Hyper_R                       (* Right hyper *)

(*
 *  Latin 1
 *  Byte 3 = 0, Byte 4 = ISO-latin1 character number
 *)

(*
 * Keysym macros, used on Keysyms to test for classes of symbols
 *)
: IsKeypadKey  ( keysym -- flag )  XK_KP_Space XK_KP_Equal 1+ within ;
: IsPrivateKeypadKey  ( keysym -- flag )  $11000000 $1100FFFF 1+ within ;
: IsCursorKey  ( keysym -- flag )  XK_Home XK_Select within ;
: IsPFKey  ( keysym -- flag )  XK_KP_F1 XK_KP_F4 1+ within ;
: IsFunctionKey  ( keysym -- flag )  XK_F1 XK_F35 1+ within ;
: IsMiscFunctionKey  ( keysym -- flag ) XK_Select XK_Break 1+ within ;

: IsModifierKey  ( keysym -- flag )
  dup XK_Shift_L XK_Hyper_R 1+ within swap
  dup XK_Mode_switch = swap
  XK_Num_Lock = or or ;

(* Return values from XRectInRegion() *)
enum{
  value RectangleOut
  value RectangleIn
  value RectanglePart
}

(*
 * Information used by the visual utility routines to find desired visual
 * type from the many visuals a display may support.
 *)

struct{
  cell visual
  cell visualid
  cell screen
  cell depth
  cell class
  cell red_mask
  cell green_mask
  cell blue_mask
  cell colormap_size
  cell bits_per_rgb
} XVisualInfo

bitenum{
  0 set
  value VisualNoMask
  value VisualIDMask
  value VisualScreenMask
  value VisualDepthMask
  value VisualClassMask
  value VisualRedMaskMask
  value VisualGreenMaskMask
  value VisualBlueMaskMask
  value VisualColormapSizeMask
  value VisualBitsPerRGBMask
  0x1FF constant VisualAllMask
}

(*
 * This defines a window manager property that clients may use to
 * share standard color maps of type RGB_COLOR_MAP:
 *)
struct{
        cell colormap
        cell red_max
        cell red_mult
        cell green_max
        cell green_mult
        cell blue_max
        cell blue_mult
        cell base_pixel
        cell visualid           (* added by ICCCM version 1 *)
        cell killid             (* added by ICCCM version 1 *)
} XStandardColormap

1 constant ReleaseByFreeingColormap  (* for killid field above *)


(*
 * return codes for XReadBitmapFile and XWriteBitmapFile
 *)
enum{
  value BitmapSuccess
  value BitmapOpenFailed
  value BitmapFileInvalid
  value BitmapNoMemory
}

(* **************************************************************
 *
 * Context Management
 *
 *************************************************************** *)


(* Associative lookup table return codes *)
enum{
  value XCSUCCESS    (* No error. *)
  value XCNOMEM      (* Out of memory *)
  value XCNOENT      (* No entry in table *)
}

\ ' XrmUniqueQuark Alias XUniqueContext
\ ' XrmStringToQuark Alias XStringToContext

enum{
  value XrmBindTightly
  value XrmBindLoosely
}

enum{
  value XrmEnumAllLevels
  value XrmEnumOneLevel
}

enum{
  value XrmoptionNoArg     (* Value is specified in OptionDescRec.value          *)
  value XrmoptionIsArg     (* Value is the option string itself                  *)
  value XrmoptionStickyArg (* Value is characters immediately following option *)
  value XrmoptionSepArg    (* Value is next argument in argv             *)
  value XrmoptionResArg    (* Resource and value in next argument in argv      *)
  value XrmoptionSkipArg   (* Ignore this option and the next argument in argv *)
  value XrmoptionSkipLine  (* Ignore this option and the rest of argv            *)
  value XrmoptionSkipNArgs (* Ignore this option and the next OptionDescRes.value arguments in argv *)
}

struct{
    cell   option           (* Option abbreviation in argv          *)
    cell   specifier        (* Resource specifier                   *)
    cell   argKind          (* Which style of option it is          *)
    cell   value            (* Value to provide if XrmoptionNoArg   *)
} XrmOptionDescRec

(* $XConsortium: cursorfont.h,v 1.4 94/04/17 20:22:00 rws Exp $ *)
(*

Copyright (c) 1987  X Consortium

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the X Consortium shall
not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization
from the X Consortium.

 *)

154 constant XC_num_glyphs
0 constant XC_X_cursor
2 constant XC_arrow
4 constant XC_based_arrow_down
6 constant XC_based_arrow_up
8 constant XC_boat
10 constant XC_bogosity
12 constant XC_bottom_left_corner
14 constant XC_bottom_right_corner
16 constant XC_bottom_side
18 constant XC_bottom_tee
20 constant XC_box_spiral
22 constant XC_center_ptr
24 constant XC_circle
26 constant XC_clock
28 constant XC_coffee_mug
30 constant XC_cross
32 constant XC_cross_reverse
34 constant XC_crosshair
36 constant XC_diamond_cross
38 constant XC_dot
40 constant XC_dotbox
42 constant XC_double_arrow
44 constant XC_draft_large
46 constant XC_draft_small
48 constant XC_draped_box
50 constant XC_exchange
52 constant XC_fleur
54 constant XC_gobbler
56 constant XC_gumby
58 constant XC_hand1
60 constant XC_hand2
62 constant XC_heart
64 constant XC_icon
66 constant XC_iron_cross
68 constant XC_left_ptr
70 constant XC_left_side
72 constant XC_left_tee
74 constant XC_leftbutton
76 constant XC_ll_angle
78 constant XC_lr_angle
80 constant XC_man
82 constant XC_middlebutton
84 constant XC_mouse
86 constant XC_pencil
88 constant XC_pirate
90 constant XC_plus
92 constant XC_question_arrow
94 constant XC_right_ptr
96 constant XC_right_side
98 constant XC_right_tee
100 constant XC_rightbutton
102 constant XC_rtl_logo
104 constant XC_sailboat
106 constant XC_sb_down_arrow
108 constant XC_sb_h_double_arrow
110 constant XC_sb_left_arrow
112 constant XC_sb_right_arrow
114 constant XC_sb_up_arrow
116 constant XC_sb_v_double_arrow
118 constant XC_shuttle
120 constant XC_sizing
122 constant XC_spider
124 constant XC_spraycan
126 constant XC_star
128 constant XC_target
130 constant XC_tcross
132 constant XC_top_left_arrow
134 constant XC_top_left_corner
136 constant XC_top_right_corner
138 constant XC_top_side
140 constant XC_top_tee
142 constant XC_trek
144 constant XC_ul_angle
146 constant XC_umbrella
148 constant XC_ur_angle
150 constant XC_watch
152 constant XC_xterm

1 constant XA_PRIMARY
2 constant XA_SECONDARY
31 constant XA_STRING
6 constant XA_CARDINAL
39 constant XA_WM_NAME
4 constant XA_ATOM
33 constant XA_WINDOW
35 constant XA_WM_HINTS


base !

previous definitions
