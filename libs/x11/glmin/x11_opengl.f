;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adapted from bigForth by Bernd Paysan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: dynlib big-struct

[IFNDEF] libgl
dynlib: libgl libGL.so.1
[ENDIF]

[IFNDEF] libglu
dynlib: libglu libGLU.so.1
[ENDIF]

dynlib-import: libgl

\ : ?texture  true ; immediate
\ : !gl-lib ( "name" -- ) bl word drop ;

\ functions                                            04oct97py

void glClearIndex( float ); ( f -- )
void glClearColor( float , float , float , float ); ( r g b a -- )
void glClear( int ); ( mask -- )
void glIndexMask( int ); ( mask -- )
void glColorMask( int , int , int , int ); ( r g b a -- )
void glAlphaFunc( float , int ); ( f r -- )
void glBlendFunc( int , int ); ( d s -- )
void glLogicOp( int ); ( opcode -- )
void glCullFace( int ); ( mode -- )
void glFrontFace( int ); ( mode -- )
void glPointSize( float ); ( sf -- )
void glLineWidth( float ); ( wf -- )
void glLineStipple( int , int ); ( factor pattern -- )
void glPolygonMode( int , int ); ( face mode -- )
void glPolygonOffset( float , float ); ( funits ffactor -- )
void glPolygonStipple( int ); ( *mask -- )
void glGetPolygonStipple( int ); ( *mask -- )
void glEdgeFlag( int ); ( flag -- )
void glEdgeFlagv( int ); ( *flag -- )
void glScissor( int , int , int , int ); ( x y w h -- )
void glClipPlane( int , int ); ( plane *equation -- )
void glGetClipPlane( int , int ); ( plane *eqation -- )
void glDrawBuffer( int ); ( mode -- )
void glReadBuffer( int ); ( mode -- )
void glEnable( int ); ( cap -- )
void glDisable( int ); ( cap -- )
int glIsEnabled( int ); ( cap -- flag )
void glEnableClientState( int ); ( cap -- )
void glDisableClientState( int ); ( cap -- )
void glGetBooleanv( int , int ); ( pname params -- )
void glGetDoublev( int , int ); ( pname params -- )
void glGetFloatv( int , int ); ( pname params -- )
void glGetIntegerv( int , int ); ( pname params -- )
void glPushAttrib( int ); ( mask -- )
void glPopAttrib( void ); ( -- )
void glPushClientAttrib( int ); ( mask -- )
void glPopClientAttrib( void ); ( -- )
void glRenderMode( int ); ( mode -- n )
void glGetError( void ); ( -- n )
void glGetString( int ); ( name -- string )
void glFinish( void ); ( -- )
void glFlush( void ); ( -- )
void glHint( int , int ); ( target mode -- )

\ Depth Buffer                                         04oct97py

void glClearDepth( double ); ( ddepth -- )
void glDepthFunc( int ); ( func -- )
void glDepthMask( int ); ( flag -- )
void glDepthRange( double , double ); ( dnear dfar -- )

\ Accumulation Buffer                                  04oct97py

void glClearAccum( float , float , float , float ); ( fr fg fb fa -- )
void glAccum( int , float ); ( op fvalue -- )

\ Transformation                                       04oct97py

void glMatrixMode( int ); ( mode -- )
void glOrtho( double , double , double , double , double , double );                   ( dleft dright dbottom dtop dnear doublear -- )
void glFrustum( double , double , double , double , double , double );                   ( dleft dright dbottom dtop dnear dfar -- )
void glViewport( int , int , int , int ); ( x y w h -- )
void glPushMatrix( void ); ( -- )
void glPopMatrix( void ); ( -- )
void glLoadIdentity( void ); ( -- )
void glLoadMatrixd( int ); ( *m -- )
void glLoadMatrixf( int ); ( *m -- )
void glMultMatrixd( int ); ( *m -- )
void glMultMatrixf( int ); ( *m -- )
void glRotated( double , double , double , double ); ( da dx dy dz -- )
void glRotatef( float , float , float , float ); ( fa fx fy fz -- )
void glScaled( double , double , double ); ( dx dy dz -- )
void glScalef( float , float , float ); ( fx fy fz -- )
void glTranslated( double , double , double ); ( dx dy dz -- )
void glTranslatef( float , float , float ); ( fx fy fz -- )

\ Display Lists                                        04oct97py

int glIsList( int ); ( list -- flag )
void glDeleteLists( int , int ); ( range list -- )
int glGenLists( int ); ( range -- u )
void glNewList( int , int ); ( list mode -- )
void glEndList( void ); ( -- )
void glCallList( int ); ( list -- )
void glCallLists( int , int , int ); ( n type *lists -- )
void glListBase( int ); ( base -- )

\ Drawing Functions                                    04oct97py

void glBegin( int ); ( mode -- )
void glEnd( void ); ( -- )

void glVertex2d( double , double ); ( dx dy -- )
void glVertex2f( float , float ); ( fx fy -- )
void glVertex2i( int , int ); ( x y -- )
void glVertex2s( int , int ); ( x y -- )

void glVertex3d( double , double , double ); ( dx dy dz -- )
void glVertex3f( float , float , float ); ( fx fy fz -- )
void glVertex3i( int , int , int ); ( x y z -- )
void glVertex3s( int , int , int ); ( x y z -- )

void glVertex4d( double , double , double , double ); ( fx fy fz fw -- )
void glVertex4f( float , float , float , float ); ( fx fy fz fw -- )
void glVertex4i( int , int , int , int ); ( x y z w -- )
void glVertex4s( int , int , int , int ); ( x y z w -- )

void glVertex2dv( int ); ( *v -- )
void glVertex2fv( int ); ( *v -- )
void glVertex2iv( int ); ( *v -- )
void glVertex2sv( int ); ( *v -- )

void glVertex3dv( int ); ( *v -- )
void glVertex3fv( int ); ( *v -- )
void glVertex3iv( int ); ( *v -- )
void glVertex3sv( int ); ( *v -- )

void glVertex4dv( int ); ( *v -- )
void glVertex4fv( int ); ( *v -- )
void glVertex4iv( int ); ( *v -- )
void glVertex4sv( int ); ( *v -- )

void glNormal3b( int , int , int ); ( nx ny nz -- )
void glNormal3d( double , double , double ); ( dnx dny dnz -- )
void glNormal3f( float , float , float ); ( nx ny nz -- )
void glNormal3i( int , int , int ); ( nx ny nz -- )
void glNormal3s( int , int , int ); ( nx ny nz -- )

void glNormal3bv( int ); ( *v -- )
void glNormal3dv( int ); ( *v -- )
void glNormal3fv( int ); ( *v -- )
void glNormal3iv( int ); ( *v -- )
void glNormal3sv( int ); ( *v -- )

void glIndexd( double ); ( dc -- )
void glIndexf( float ); ( fc -- )
void glIndexi( int ); ( c -- )
void glIndexs( int ); ( c -- )
void glIndexub( int ); ( c -- )

void glIndexdv( int ); ( *c -- )
void glIndexfv( int ); ( *c -- )
void glIndexiv( int ); ( *c -- )
void glIndexsv( int ); ( *c -- )
void glIndexubv( int ); ( *c -- )

void glColor3b( int , int , int ); ( r g b -- )
void glColor3d( double , double , double ); ( r g b -- )
void glColor3f( float , float , float ); ( r g b -- )
void glColor3i( int , int , int ); ( r g b -- )
void glColor3s( int , int , int ); ( r g b -- )
void glColor3ub( int , int , int ); ( r g b -- )
void glColor3ui( int , int , int ); ( r g b -- )
void glColor3us( int , int , int ); ( r g b -- )

void glColor4b( int , int , int , int ); ( r g b a -- )
void glColor4d( double , double , double , double ); ( r g b a -- )
void glColor4f( float , float , float , float ); ( r g b a -- )
void glColor4i( int , int , int , int ); ( r g b a -- )
void glColor4s( int , int , int , int ); ( r g b a -- )
void glColor4ub( int , int , int , int ); ( r g b a -- )
void glColor4ui( int , int , int , int ); ( r g b a -- )
void glColor4us( int , int , int , int ); ( r g b a -- )

void glColor3bv( int ); ( *v -- )
void glColor3dv( int ); ( *v -- )
void glColor3fv( int ); ( *v -- )
void glColor3iv( int ); ( *v -- )
void glColor3sv( int ); ( *v -- )
void glColor3ubv( int ); ( *v -- )
void glColor3uiv( int ); ( *v -- )
void glColor3usv( int ); ( *v -- )

void glColor4bv( int ); ( *v -- )
void glColor4dv( int ); ( *v -- )
void glColor4fv( int ); ( *v -- )
void glColor4iv( int ); ( *v -- )
void glColor4sv( int ); ( *v -- )
void glColor4ubv( int ); ( *v -- )
void glColor4uiv( int ); ( *v -- )
void glColor4usv( int ); ( *v -- )

void glTexCoord1d( double ); ( ds -- )
void glTexCoord1f( float ); ( ds -- )
void glTexCoord1i( int ); ( ds -- )
void glTexCoord1s( int ); ( ds -- )

void glTexCoord2d( double , double ); ( ds dt -- )
void glTexCoord2f( float , float ); ( ds dt -- )
void glTexCoord2i( int , int ); ( ds dt -- )
void glTexCoord2s( int , int ); ( ds dt -- )

void glTexCoord3d( double , double , double ); ( ds dt dr -- )
void glTexCoord3f( float , float , float ); ( ds dt dr -- )
void glTexCoord3i( int , int , int ); ( ds dt dr -- )
void glTexCoord3s( int , int , int ); ( ds dt dr -- )

void glTexCoord4d( double , double , double , double ); ( ds dt dr dq -- )
void glTexCoord4f( float , float , float , float ); ( ds dt dr dq -- )
void glTexCoord4i( int , int , int , int ); ( ds dt dr dq -- )
void glTexCoord4s( int , int , int , int ); ( ds dt dr dq -- )

void glTexCoord1dv( int ); ( *s -- )
void glTexCoord1fv( int ); ( *s -- )
void glTexCoord1iv( int ); ( *s -- )
void glTexCoord1sv( int ); ( *s -- )

void glTexCoord2dv( int ); ( *s -- )
void glTexCoord2fv( int ); ( *s -- )
void glTexCoord2iv( int ); ( *s -- )
void glTexCoord2sv( int ); ( *s -- )

void glTexCoord3dv( int ); ( *s -- )
void glTexCoord3fv( int ); ( *s -- )
void glTexCoord3iv( int ); ( *s -- )
void glTexCoord3sv( int ); ( *s -- )

void glTexCoord4dv( int ); ( *s -- )
void glTexCoord4fv( int ); ( *s -- )
void glTexCoord4iv( int ); ( *s -- )
void glTexCoord4sv( int ); ( *s -- )

void glRasterPos2d( double , double ); ( dx dy -- )
void glRasterPos2f( float , float ); ( fx fy -- )
void glRasterPos2i( int , int ); ( x y -- )
void glRasterPos2s( int , int ); ( x y -- )

void glRasterPos3d( double , double , double ); ( dx dy dz -- )
void glRasterPos3f( float , float , float ); ( fx fy fz -- )
void glRasterPos3i( int , int , int ); ( x y z -- )
void glRasterPos3s( int , int , int ); ( x y z -- )

void glRasterPos4d( double , double , double , double ); ( dx dy dz dw -- )
void glRasterPos4f( float , float , float , float ); ( fx fy fz fw -- )
void glRasterPos4i( int , int , int , int ); ( x y z w -- )
void glRasterPos4s( int , int , int , int ); ( x y z w -- )

void glRasterPos2dv( int ); ( *v -- )
void glRasterPos2fv( int ); ( *v -- )
void glRasterPos2iv( int ); ( *v -- )
void glRasterPos2sv( int ); ( *v -- )

void glRasterPos3dv( int ); ( *v -- )
void glRasterPos3fv( int ); ( *v -- )
void glRasterPos3iv( int ); ( *v -- )
void glRasterPos3sv( int ); ( *v -- )

void glRasterPos4dv( int ); ( *v -- )
void glRasterPos4fv( int ); ( *v -- )
void glRasterPos4iv( int ); ( *v -- )
void glRasterPos4sv( int ); ( *v -- )

void glRectd( double , double , double , double ); ( dx1 dy2 dx2 dy2 -- )
void glRectf( float , float , float , float ); ( fx1 fy1 fx2 fy2 -- )
void glRecti( int , int , int , int ); ( x1 y1 x2 y2 -- )
void glRects( int , int , int , int ); ( x1 y1 x2 y2 -- )

void glRectdv( int ); ( *v -- )
void glRectfv( int ); ( *v -- )
void glRectiv( int ); ( *v -- )
void glRectsv( int ); ( *v -- )

\ Vertex Arrays                                        04oct97py

void glVertexPointer( int , int , int , int ); ( s t stride ptr -- )
void glNormalPointer( int , int , int ); ( type stride ptr -- )
void glColorPointer( int , int , int , int ); ( s t stride ptr -- )
void glIndexPointer( int , int , int ); ( type stride ptr -- )
void glTexCoordPointer( int , int , int , int ); ( s t str ptr -- )
void glEdgeFlagPointer( int , int ); ( stride ptr -- )
void glGetPointerv( int , int ); ( pname params -- )
void glArrayElement( int ); ( i -- )
void glDrawArrays( int , int , int ); ( mode first count -- )
void glDrawElements( int , int , int , int ); ( m c type indices -- )
void glInterleavedArrays( int , int , int );                                      ( format stride ptr -- )

\ Lighting                                             04oct97py

void glShadeModel( int ); ( mode -- )
void glLightf( int , int , float ); ( light pname fparam -- )
void glLighti( int , int , int ); ( light pname param -- )
void glLightfv( int , int , int ); ( light pname *fparam -- )
void glLightiv( int , int , int ); ( light pname *param -- )
void glGetLightfv( int , int , int ); ( light pname *fparam -- )
void glGetLightiv( int , int , int ); ( light pname *param -- )
void glLightModelf( int , float ); ( pname fparam -- )
void glLightModeli( int , int ); ( pname param -- )
void glLightModelfv( int , int ); ( pname *param -- )
void glLightModeliv( int , int ); ( pname *param -- )
void glMaterialf( int , int , float ); ( face pname fparam -- )
void glMateriali( int , int , int ); ( face pname param -- )
void glMaterialfv( int , int , int ); ( face pname *fparam -- )
void glMaterialiv( int , int , int ); ( face pname *param -- )
void glGetMaterialfv( int , int , int ); ( face pname *fp -- )
void glGetMaterialiv( int , int , int ); ( face pname *p -- )
void glColorMaterial( int , int ); ( face mode -- )

\ Raster functions                                     04oct97py

void glPixelZoom( float , float ); ( xfactor yfactor -- )
void glPixelStoref( int , float ); ( pname param -- )
void glPixelStorei( int , int ); ( pname param -- )
void glPixelTransferf( int , float ); ( pname param -- )
void glPixelTransferi( int , int ); ( pname param -- )
void glPixelMapfv( int , int , int ); ( map mapsize *values -- )
void glPixelMapuiv( int , int , int ); ( map mapsize *values -- )
void glPixelMapusv( int , int , int ); ( map mapsize *values -- )
void glGetPixelMapfv( int , int , int ); ( m ms *values -- )
void glGetPixelMapuiv( int , int , int ); ( m ms *values -- )
void glGetPixelMapusv( int , int , int ); ( m ms *values -- )
void glBitmap( int , int , int , int , int , int , int );          ( w h xorig yorig xmove ymove bitmap -- )
void glReadPixels( int , int , int , int , int , int , int );          ( x y w h format type pixels -- )
void glDrawPixels( int , int , int , int , int ); ( w h format type *pixs -- )
void glCopyPixels( int , int , int , int , int ); ( x y w h type -- )

\ Stenciling                                           04oct97py

void glStencilFunc( int , int , int ); ( mask ref func -- )
void glStencilMask( int ); ( mask -- )
void glStencilOp( int , int , int ); ( zpass zfail fail -- )
void glClearStencil( int ); ( s -- )

\ Texture Mapping                                      04oct97py

void glTexGend( int , int , double ); ( coord pname dparam -- )
void glTexGenf( int , int , float ); ( coord pname fparam -- )
void glTexGeni( int , int , int ); ( coord pname param -- )
void glTexGendv( int , int , int ); ( coord pname *param -- )
void glTexGenfv( int , int , int ); ( coord pname *param -- )
void glTexGeniv( int , int , int ); ( coord pname *param -- )
void glGetTexGendv( int , int , int ); ( coord pname *param -- )
void glGetTexGenfv( int , int , int ); ( coord pname *param -- )
void glGetTexGeniv( int , int , int ); ( coord pname *param -- )
void glTexEnvf( int , int , float ); ( target pname fparam -- )
void glTexEnvi( int , int , int ); ( target pname param -- )
void glTexEnvfv( int , int , int ); ( target pname *fparam -- )
void glTexEnviv( int , int , int ); ( target pname *param -- )
void glGetTexEnvfv( int , int , int ); ( target pname *param -- )
void glGetTexEnviv( int , int , int ); ( target pname *param -- )
void glTexParameterf( int , int , float ); ( target pname fparam -- )
void glTexParameteri( int , int , int ); ( target pname param -- )
void glTexParameterfv( int , int , int ); ( target pname *fparam -- )
void glTexParameteriv( int , int , int ); ( target pname *param -- )
void glGetTexParameterfv( int , int , int ); ( target pname *fparam -- )
void glGetTexParameteriv( int , int , int ); ( target pname *param -- )

void glGetTexLevelParameterfv( int , int , int , int );           ( target level pname *params -- )
void glGetTexLevelParameteriv( int , int , int , int );           ( target level pname *params -- )
void glTexImage1D( int , int , int , int , int , int , int , int );           ( target level component width border format type *pixels -- )
void glTexImage2D( int , int , int , int , int , int , int , int , int );           ( target level component width height border format type *pixels -- )
void glGetTexImage( int , int , int , int , int );           ( target level format type *pixels -- )
void glGenTextures( int , int ); ( n *textures -- )
void glDeleteTextures( int , int ); ( n *textures -- )
void glBindTexture( int , int ); ( target textures -- )
void glPrioritizeTextures( int , int , int );           ( n *textures *priorities -- )
int glAreTexturesResident( int , int , int );           ( n *textures *residences -- flag )
int glIsTexture( int ); ( texture -- flag )
void glTexSubImage1D( int , int , int , int , int , int , int );           ( target level xoffset width format type *pixels -- )
void glTexSubImage2D( int , int , int , int , int , int , int , int , int );           ( target level xoffset yoffset widht height format type *pixels -- )
void glCopyTexImage1D( int , int , int , int , int , int , int );           ( target level intformat x y w border -- )
void glCopyTexImage2D( int , int , int , int , int , int , int , int );           ( target level intformat x y w h border -- )
void glCopyTexSubImage1D( int , int , int , int , int , int );           ( target level xoffset x y w -- )
void glCopyTexSubImage2D( int , int , int , int , int , int , int , int );           ( target level xoffset yoffset x y w h -- )

\ Evaluators                                           04oct97py

void glMap1d( int , double , double , int , int , int ); ( t du1 du2 stride order *points -- )
void glMap1f( int , float , float , int , int , int ); ( t fu1 fu2 stride order *points -- )
void glMap2d( int , double , double , int , int , double , double , int , int , int );
           ( *points vorder vstride dv2 dv1 uorder ustride du2 du1 target -- )
void glMap2f( int , float , float , int , int , float , float , int , int , int );
           ( *points vorder vstride fv2 fv1 uorder ustride fu2 fu1 target -- )
void glGetMapdv( int , int , int ); ( target query *v -- )
void glGetMapfv( int , int , int ); ( target query *v -- )
void glGetMapiv( int , int , int ); ( target query *v -- )
void glEvalCoord1d( double ); ( du -- )
void glEvalCoord1f( float ); ( fu -- )
void glEvalCoord1dv( int ); ( *du -- )
void glEvalCoord1fv( int ); ( *fu -- )
void glEvalCoord2d( double , double ); ( du dv -- )
void glEvalCoord2f( float , float ); ( fu fv -- )
void glEvalCoord2dv( int ); ( *du -- )
void glEvalCoord2fv( int ); ( *fu -- )
void glMapGrid1d( int , double , double ); ( un du1 du2 -- )
void glMapGrid1f( int , float , float ); ( un fu1 fu2 -- )
void glMapGrid2d( int , double , double , int , double , double ); ( un du1 du2 vn dv1 dv2 -- )
void glMapGrid2f( int , float , float , int , float , float ); ( fv2 fv1 vn fu2 fu1 un -- )
void glEvalPoint1( int ); ( i -- )
void glEvalPoint2( int , int ); ( i j -- )
void glEvalMesh1( int , int , int ); ( mode i1 i2 -- )
void glEvalMesh2( int , int , int , int , int ); ( mode i1 i2 j1 j2 -- )

\ Fog                                                  04oct97py

void glFogf( int , float ); ( pname param -- )
void glFogi( int , int ); ( pname param -- )
void glFogfv( int , int ); ( pname *param -- )
void glFogiv( int , int ); ( pname *param -- )

\ Selection and Feedback                               04oct97py

void glFeedbackBuffer( int , int , int );                      ( size type *buffer -- )
void glPassThrough( int ); ( token -- )
void glSelectBuffer( int , int ); ( size *buffer -- )
void glInitNames( void ); ( -- )
void glLoadName( int ); ( name -- )
void glPushName( int ); ( name -- )
void glPopName( void ); ( -- )

\ glX calls                                            04oct97py

\ [defined] x11 [IF]
int glXChooseVisual( int , int , int );                        ( dpy screen attriblist -- XVisualInfo )
int glXCreateContext( int , int , int , int );                        ( dpy vis shareList direct -- GLXContext )
void glXDestroyContext( int , int ); ( dpy ctx -- )
int glXMakeCurrent( int , int , int );                        ( dpy drawable ctx -- flag )
void glXCopyContext( int , int , int , int ); ( dpy src dst mask -- )
void glXSwapBuffers( int , int ); ( dpy drawable -- )
int glXCreateGLXPixmap( int , int , int );                              ( dpy visual pixmap -- GLXPixmap )
void glXDestroyGLXPixmap( int , int );                                             ( dpy pixmap -- )
int glXQueryExtension( int , int , int );                                    ( dpy errorb event -- flag )
int glXQueryVersion( int , int , int ); ( dpy maj min -- flag )
int glXIsDirect( int , int ); ( dpy ctx -- flag )
void glXGetConfig( int , int , int , int ); ( dpy visual attrib value -- )
int glXGetCurrentContext( void );                                               ( -- GLXContext )
int glXGetCurrentDrawable( void );                                              ( -- GLXDrawable )
void glXWaitGL( void ); ( -- )
void glXWaitX( void ); ( -- )
void glXUseXFont( int , int , int , int ); ( font first count list -- )
int glXQueryExtensionsString( int , int );                                        ( dpy screen -- string )
int glXQueryServerString( int , int , int );                                   ( dpy screen name -- string )
int glXGetClientString( int , int );                                          ( name dpy -- string )
\ [THEN]
end;

dynlib-import: libglu

int gluNewTess( void ); ( -- tess )
void gluDeleteTess( int ); ( tess -- )
void gluTessBeginContour( int ); ( tess -- )
void gluTessBeginPolygon( int , int ); ( tess data -- )
void gluTessCallback( int , int , int ); ( tess which cfun -- )
void gluTessEndContour( int ); ( tess -- )
void gluTessEndPolygon( int ); ( tess -- )
void gluTessNormal( int , double , double , double ); ( tess rx ry< rz -- )
void gluTessProperty( int , int , double ); ( tess wich rdata -- )
void gluTessVertex( int , int , int ); ( tess loc data -- )

end;
