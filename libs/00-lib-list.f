;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ forth:warning-redefine @

;; this loads floating point kernel if it is not loaded yet
[IFNDEF] (known-libs):FP
also (known-libs) definitions
: FP  ( -- )
  " forth:FPU-GETSW" has-word? if
    " forth:F#" has-word? if
      " forth:F." has-word? if
        exit
      endif
    endif
  endif
  " !libs/ext/float.f" tload>forth
;
previous definitions
[ENDIF]

lib-type-require: LOCALS  !libs/locals.f  forth:(locvoc)

lib-type-require: ASM     !libs/asmx86 forth:asmx86
lib-type-require: DISASM  !libs/disx86 forth:disx86

lib-type-require: ASMX86  !libs/asmx86 forth:asmx86
lib-type-require: DISX86  !libs/disx86 forth:disx86

lib-type-require: ANS           !libs/ans             forth:ANS-COMPAT
lib-type-require: ANS-FILE      !libs/ans/file.f      forth:ANS-FILE
lib-type-require: ANS-ABORT     !libs/ans/abort.f     forth:ANS-ABORT
lib-type-require: ANS-CRAP      !libs/ans/crap.f      forth:ANS-CRAP
lib-type-require: ANS-NUMBER    !libs/ans/number.f    forth:ANS-NUMBER
lib-type-require: ANS-VAR       !libs/ans/vardefer.f  forth:ANS-VARIABLE
lib-type-require: ANS-WORDLIST  !libs/ans/wordlist.f  forth:ANS-WORDLIST
lib-type-require: ANS-TTY       !libs/ans/tty.f       forth:ANS-TTY

lib-type-require: EXT     !libs/ext                   forth:URFORTH-EXT
lib-type-require: TMATH   !libs/ext/math_extprec.f    forth:TNEGATE
lib-type-require: STRUCT  !libs/ext/struct.f          forth:BEGIN-STRUCTURE
lib-type-require: TTY     !libs/tty/                  forth:TTY

lib-type-require: UNIX-TIME  !libs/ext/unixtime.f     forth:unix-time
lib-type-require: GLOB-MATCH  !libs/ext/globmatch.f   forth:glob:match-ex

lib-type-require: EXEC    !libs/ext/exec.f            forth:os:exec

lib-type-require: STRUCT-ACCESSORS  !libs/ext/simple-accessors.f  forth:struct-accessors

lib-type-require: PRNG-PCG32EX   !libs/ext/prngs.f  forth:prng:PCG32EX-NEXT
lib-type-require: PRNG-XORSHIFT  !libs/ext/prngs.f  forth:prng:XORSHIFT*-64/32
lib-type-require: PRNG-BJEX      !libs/ext/prngs.f  forth:prng:BJEX-NEXT
lib-type-require: PRNG-ISAAC     !libs/ext/isaac.f  forth:prng:ISAAC

lib-type-require: OS-SELECT  !libs/ext/os_select.f  os:select
lib-type-require: OS-ERRNO   !libs/ext/os_errno.f   os:errno-name
lib-type-require: OS-DIR     !libs/ext/os_dir.f     os:getdents

lib-type-require: OOF      !libs/uroof.f    forth:oop:::invoke
lib-type-require: MINIOOF  !libs/minioof.f  forth:(end-class)

lib-type-require: MD5SUM   !libs/md5sum.f  forth:md5sum
lib-type-require: SHA1     !libs/sha1.f    forth:sha1

lib-type-require: UTF    !libs/utf/  forth:utf

lib-type-require: MINI-THREADS !libs/mini-threads.f  forth:tread
lib-type-require: MINITHREADS  !libs/mini-threads.f  forth:tread

lib-type-require: DBL-PARSE-DPL  !libs/dbl-parse-dpl.f  forth:number-dbl-dpl
lib-type-require: DBL-PARSE      !libs/dbl-parse.f      forth:number-dbl

;; bigForth structures
lib-type-require: BIG-STRUCT  !libs/bigforth-struct.f  forth:(bigforth-struct)

;; simple lazy dynlib imports, with support for SAVEing
lib-type-require: DYNLIB  !libs/dynimport-simple.f  forth:(dynlib-simple)

lib-type-require: X11  !libs/x11/  forth:x11

lib-type-require: XOG  !libs/xog/  forth:xog-open-dpy


lib-type-require: DISZX  !libs/zxdisasm.f  forth:zxdis


\ forth:warning-redefine !
