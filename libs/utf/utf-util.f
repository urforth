;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] forth:utf
vocabulary utf
[ENDIF]

also utf definitions


: valid-unich?  ( unich -- bool-flag )  dup 0xD800 u< swap 0xE000 0x10FFFF bounds? or ;

;; rough check for valid utf8 start byte
: valid-start?  ( byte -- bool-flag )  0xff and dup 128 < swap 0xc0 and 0xc0 = or ;

;; does this char start UTF-8 sequence?
: utf8-start?  ( byte -- bool-flag )  0xc0 and 0xc0 = ;

;; does this char continue UTF-8 sequence?
: utf8-cont?  ( byte -- bool-flag )  0xc0 and 0x80 = ;


;; -1: invalid utf8
: detect-len  ( first-byte -- len )
  0xff and dup 0x80 u< if drop 1 exit endif
  dup &b1111_1110 and &b1111_1100 = if drop 6 exit endif
  dup &b1111_1100 and &b1111_1000 = if drop 5 exit endif
  dup &b1111_1000 and &b1111_0000 = if drop 4 exit endif
  dup &b1111_0000 and &b1110_0000 = if drop 3 exit endif
  dup &b1110_0000 and &b1100_0000 = if drop 2 exit endif
  drop -1  ;; invalid
;

: valid-buf?  ( addr count -- bool-flag )
  0x7fff_fffe min begin dup +while
    over c@ detect-len 0x7fff_ffff and 2dup < if drop 2drop false exit endif  ( addr count len )
    >r /char r> 1- for
      over c@ &b1100_0000 and &b1000_0000 <> if unloop 2drop false exit endif
    /char endfor
  repeat 2drop true
;


;; doesn't do much checks, so don't pass invalid utf8
: strlen  ( addr count -- length )  0 >r ( length)
  begin dup +while
    over c@ detect-len 1 max 2dup < if drop 2drop r> exit endif  ( addr count len | length)
    dup r> + >r /string
  repeat 2drop r>
;


;; returned dword is little-endian, ready for "!"
;; max length is 4 bytes
: encode-char  ( unich -- dword byte-count true // 63 1 false )
  dup valid-unich? ifnot drop 63 1 false exit endif
  dup case
    0x80 u<of 1 endof
    0x800 u<of
      dup 6 rshift 0xc0 or
      swap 0x3f and 0x80 or 8 lshift or
      2
    endof
    0x10000 u<of
      dup 12 rshift 0xe0 or
      over 6 rshift 0x3f and 0x80 or 8 lshift or
      swap 0x3f and 0x80 or 16 lshift or
      3
    endof
    otherwise drop
      dup 18 rshift 0xf0 or
      dup 12 rshift 0x3f and 0x80 8 lshift or
      over 6 rshift 0x3f and 0x80 16 lshift or or
      swap 0x3f and 0x80 or 24 lshift or
      4
  endcase true
;


previous definitions
