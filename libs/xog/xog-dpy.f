;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- open/close X11 Display
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: x11


also x11 also xconst

: xog-open-dpy-ex ( -- successflag )
  xog-dpy if true exit endif
  (xog-dpy-before-open)
  0 XOpenDisplay dup to xog-dpy ifnot false exit endif
  (xog-dpy-after-open)
  true
;


: xog-open-dpy ( -- )
  xog-open-dpy-ex not-?abort" cannot open X11 display"
;


: xog-close-dpy  ( -- )
  xog-dpy ifnot exit endif
  (xog-dpy-before-close)
  xog-dpy XCloseDisplay drop
  0 to xog-dpy
  (xog-dpy-after-close)
;

previous previous
