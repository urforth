;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- X11 Base Widget Class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: oof x11

also x11 also xconst


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also xog-style definitions
CoreFont oop:new-allot oop:value font BaseFont

\ FIXME: make this adjustable
..: (xog-dpy-after-open)  ( -- )
  14 FontType-Normal font build-font-name
  font create not-?abort" cannot create X11 font"
;..

previous definitions


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow oop:class
  field hotkey
  field caption-width
  field caption-height
  field caption-hotofs  ;; offset of caption underscore
  field caption-hotlen  ;; pixel length of caption underscore
  field caption-hotpos  ;; string position of hotkey underscore
  field cb-action  ( self -- )
  field down?

  ;; this WILL MODIFY THE STRING!
  ;; it will also update the corresponding fields
  method (caption-extract-hotkey)  ( addr count -- newcount )

  method (calc-caption-props)  ( -- )

  method set-action  ( actionptr -- )

  ;; calls cb-action by default
  method click  ( -- )

  method draw-caption  ( -- )
  method draw-bevel  ( -- )
  method draw-focus-rect  ( -- )
end-class: BaseWidget


BaseWidget method: (caption-extract-hotkey)  ( addr count -- newcount )
  0 to hotkey -1 to caption-hotpos 0 to caption-hotlen
  ;; reset caption size, so everything will be recalced
  0 to caption-width 0 to caption-height
  dup 2 < if nip 0 max exit endif
  2dup [char] & str-char-index ifnot nip exit endif
  dup to caption-hotpos >r  ( addr count | pos )
  over r@ + 1+ c@ locase-char
  dup case
    [char] 0 [char] 9 bounds-of to hotkey endof
    [char] a [char] z bounds-of to hotkey endof
    otherwise 2drop nip rdrop exit
  endcase
  r> over 1- >r /string >r dup 1+ swap r> 1- move r>
;

BaseWidget method: get-caption  ( -- addr count )  NullString ;

BaseWidget method: bg-color  ( -- color )  xog-style:background-color ;

BaseWidget method: init  ( -- )
  call-super
  92 to width
  42 to height
  true to visible?
  false to down?
  0 to cb-action
  0 to hotkey
  0 to caption-width
  0 to caption-height
  0 to caption-hotofs
  0 to caption-hotlen
  -1 to caption-hotpos
;

BaseWidget method: set-action  ( actionptr -- )
  to cb-action
;

BaseWidget method: click  ( -- )
  cb-action ?dup if self swap execute-tail endif
;

BaseWidget method: (calc-caption-props)  ( -- )
  caption-width caption-height logand ifnot
    get-caption ?dup if
      2dup xog-style:font log-extents
      to caption-height to caption-width 2drop
      caption-hotpos over 0 swap within if
        ;; calculate hotkey underscore
        drop dup caption-hotpos +
        swap caption-hotpos xog-style:font log-width to caption-hotofs
        1 xog-style:font log-width to caption-hotlen
          \ endcr get-caption type ." : hotpos=" caption-hotpos . ." hotofs=" caption-hotofs . ." hotlen=" caption-hotlen . cr
      else 2drop 0 to caption-hotlen endif
    else drop 0 to caption-hotlen ( just in case) endif
  endif
;


BaseWidget method: on-draw-part  ( x y width height count -- )
  dirty? double-buffered? logand if drop 2drop 2drop on-draw else call-super endif
;


BaseWidget method: draw-caption  ( -- )
  get-caption ?dup if
    xog-style:text-color set-color
    (calc-caption-props)
    width caption-width - 2/ down? if 1+ endif
    height caption-height - 2/ down? if 1+ endif
    self xog-style:font draw
    caption-hotlen if
      set-line-solid
      width caption-width - 2/ down? if 1+ endif caption-hotpos +
      height caption-height - 2/ down? if 1+ endif caption-height + 1-
      over caption-hotlen + over draw-line
    endif
  else drop endif
;

BaseWidget method: draw-bevel  ( -- )
  ;; top and left
  down? if xog-style:dark-color else xog-style:light-color endif set-color
  0 0 width 0 draw-line
  0 0 0 height draw-line
  ;; right and bottom
  down? if xog-style:light-color else xog-style:dark-color endif set-color
  width 1- 0 width 1- height draw-line
  0 height 1- width height 1- draw-line
  ;; shadow
  down? if
    xog-style:shadow-color set-color
    1 1 width 2- 1 draw-line
    1 2 1 height 2- draw-line
  else
    xog-style:shadow-color set-color
    width 2- 1 width 2- height 2- draw-line
    1 height 2- width 2- height 2- draw-line
  endif
;

BaseWidget method: draw-focus-rect  ( -- )
  focused? if
    set-line-dashed
    xog-style:dark-color set-color
    2 down? if 1+ endif
    2 down? if 1+ endif
    width 6 - height 6 - draw-rect
    set-line-solid
  endif
;

BaseWidget method: on-draw  ( -- )
  dirty? if non-dirty!
      \ endcr ." on-draw for " (debug-id.) cr
    xog-style:background-color set-color
    0 0 width height fill-rect
    draw-caption
    draw-bevel
    draw-focus-rect
  endif
;

BaseWidget method: on-button-down  ( bnum -- )
  can-focus? if
    1 = if focus true to down? invalidate event-eat endif
  else drop endif
;

BaseWidget method: on-button-up  ( bnum -- )
  can-focus? if
    1 = if
      down? false to down? if
        invalidate
        (event) XButtonEvent x @ 0 width within
        (event) XButtonEvent y @ 0 height within and if click endif
      endif
      event-eat
    endif
  else drop down? if false to down? invalidate
  endif endif
;

BaseWidget method: on-focus  ( -- )
  focused? ifnot invalidate endif
;

BaseWidget method: on-blur  ( -- )
  focused? if invalidate endif
;

BaseWidget method: on-hide  ( -- )
  false to down?
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous previous
