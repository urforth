;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- X11 Basic Button Widget
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: oof x11

also x11 also xconst


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWidget oop:class
  64 buffer: caption

  method get-hotkey  ( -- hotkey )
  method set-hotkey  ( hotkey -- )
end-class: Button


Button method: init  ( -- )
  call-super
  92 to width
  42 to height
  caption 0c!
;

Button method: get-caption  ( -- addr count )
  caption bcount
;

Button method: set-caption  ( addr count -- )
  0 63 clamp caption c1s:copy-counted
  0 to caption-width
  0 to caption-height
  caption bcount (caption-extract-hotkey) caption c!
  invalidate
;

Button method: get-hotkey  ( -- hotkey )  hotkey ;
Button method: set-hotkey  ( hotkey -- )  locase-char to hotkey ;

Button method: check-hotkey  ( keyevent -- boolflag )
  dup XKeyEvent state @ Mod1Mask and if  ;; alt pressed
    get-hotkey ?dup if swap (get-keysym) = dup if click endif exit endif
  endif drop false
;


Button method: on-keydown  ( keysym -- )
    \ ." ***KEYDOWN: " dup . (event) XKeyEvent state @ .hex8 cr
  case
    XK_Return of down? false to down? if invalidate endif click event-eat endof
    bl of down? ifnot focus true to down? invalidate endif event-eat endof
    otherwise call-super
  endcase
;

Button method: on-keyup  ( keysym -- )
    \ ." ***KEYUP: " dup . (event) XKeyEvent state @ .hex8 cr
  case
    XK_Return of event-eat endof
    bl of down? if false to down? invalidate click endif event-eat endof
    otherwise call-super
  endcase
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous previous
