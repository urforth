;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- common variables and utility words
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: x11


0 value xog-dpy  ;; X11 display

0 value WM_PROTOCOLS
0 value XOF_INTERNAL_CLOSE
0 value WM_DELETE_WINDOW
0 value WM_TAKE_FOCUS
0 value WM_CHANGE_STATE
0 value WM_STATE
0 value NET_WM_WINDOW_TYPE
0 value NET_WM_WINDOW_TYPE_NORMAL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (xog-dpy-before-open)  ( -- )  ... ;
: (xog-dpy-after-open)  ( -- )  ... ;
: (xog-dpy-before-close)  ( -- )  ... ;
: (xog-dpy-after-close)  ( -- )  ... ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also x11 also xconst

\ in the real app you should cache this
: xog-get-color  ( addr count -- color )
  xog-dpy if
    XColor @sizeof ralloca >r
    r@ dup  ;; xcolor
    2swap ensure-asciiz >r  ;; colorname
    xog-dpy XDefaultScreen xog-dpy XDefaultColormap  ;; cmap
    xog-dpy XAllocNamedColor not-?abort" cannot allocate color"
    r> free-asciiz
    r> XColor pixel @
    XColor @sizeof rdealloca
  else 2drop 0 endif
;

: xog-black-color  ( -- )  xog-dpy dup DefaultScreen BlackPixel ;
: xog-white-color  ( -- )  xog-dpy dup DefaultScreen WhitePixel ;

: (xrect>stack)  ( xrect-addr -- x y width height )  >r
  r@ XRectangle x w@ w>s r@ XRectangle y w@ w>s
  r@ XRectangle width w@ w>s r> XRectangle height w@ w>s
;

: (xog-fix-stack)  ( origsp -- )
  (sp-check) err-stack-underflow not-?error
  >r sp@ r@  \ 2dup ucmp ?dup if . endif
  u> err-stack-underflow ?error
  r> sp!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
..: (xog-dpy-after-open)  ( -- )
  false " WM_PROTOCOLS" drop xog-dpy XInternAtom to WM_PROTOCOLS
  false " WM_DELETE_WINDOW" drop xog-dpy XInternAtom to WM_DELETE_WINDOW
  false " WM_TAKE_FOCUS" drop xog-dpy XInternAtom to WM_TAKE_FOCUS
  false " WM_CHANGE_STATE" drop xog-dpy XInternAtom to WM_CHANGE_STATE
  false " WM_STATE" drop xog-dpy XInternAtom to WM_STATE
  false " URFORTH_XOF_INTERNAL_CLOSE" drop xog-dpy XInternAtom to XOF_INTERNAL_CLOSE
  false " _NET_WM_WINDOW_TYPE" drop xog-dpy XInternAtom to NET_WM_WINDOW_TYPE
  false " _NET_WM_WINDOW_TYPE_NORMAL" drop xog-dpy XInternAtom to NET_WM_WINDOW_TYPE_NORMAL
;..

..: (xog-dpy-after-close)  ( -- )
  0 to WM_PROTOCOLS
  0 to WM_DELETE_WINDOW
  0 to WM_TAKE_FOCUS
  0 to WM_CHANGE_STATE
  0 to WM_STATE
  0 to XOF_INTERNAL_CLOSE
  0 to NET_WM_WINDOW_TYPE
  0 to NET_WM_WINDOW_TYPE_NORMAL
;..


previous previous
