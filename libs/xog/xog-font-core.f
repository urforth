;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- X11 Core Font implementation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: oof x11


BaseFont oop:class
  field fsetid  ;; fontset

  method (setup-font-sizes)  ( -- )

  method fsetid@  ( -- fsetid )
end-class: CoreFont


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also x11 also xconst


CoreFont method: build-font-name  ( size fonttype -- addr count )
  NullString pad c4s:copy-counted
  " -*-" pad c4s:cat-counted
  " helvetica" pad c4s:cat-counted
  dup FontType-Bold and if " -bold-" else " -medium-" endif pad c4s:cat-counted
  dup FontType-Italic and if " i" else " r" endif pad c4s:cat-counted
  drop  ;; fonttype
  " -*-*-" pad c4s:cat-counted
  dup +if <#u #s #> else " *" endif pad c4s:cat-counted
  " -*-*-*-*-*-*-*" pad c4s:cat-counted
  pad c4s:zterm
  pad count
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CoreFont method: init  ( -- )  0 to fsetid call-super ;
CoreFont method: fsetid@  ( -- ascent )  fsetid ;
CoreFont method: is-valid?  ( -- flag )  fsetid notnot ;

CoreFont method: destroy  ( -- )
  fsetid xog-dpy logand if fsetid xog-dpy XFreeFontSet drop endif
  call-super
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CoreFont method: (setup-font-sizes)  ( -- )
  0 to ascent 0 to descent 0 to max-width  ;; just in case
  2 cells ralloca >r  ;; XFontStruct **, char **font_names
  r@ 2 cells erase
  r@ cell+ ( names) r@ ( fstructs) fsetid XFontsOfFontSet  ;; returns count
  begin dup +while
    (*
      ." font: " r@ cell+ @ @ zcount type space
      ." ascent: " r@ @ @ XFontStruct ascent c@ .
      ." descent: " r@ @ @ XFontStruct descent c@ . cr
    *)
    r@ @ @
    dup XFontStruct ascent c@ ascent max to ascent
    XFontStruct descent c@ descent max to descent
    cell r@ +! cell r@ cell+ +! 1-  ;; is this right?
  repeat drop rdrop 2 cells rdealloca
  (*
    ." final ascent: " ascent . cr
    ." final descent: " descent . cr
  *)
;


CoreFont method: create  ( addr count -- successflag )
  is-valid? if 2drop false exit endif
  xog-dpy ifnot 2drop false exit endif
  ensure-asciiz >r
  2 cells ralloca >r
  0 r@ rot >r dup cell+ r> xog-dpy XCreateFontSet
  rdrop 2 cells rdealloca r> free-asciiz
  dup to fsetid notnot
  dup if (setup-font-sizes) endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; `XmbTextExtents` returns log width

;; y is "up from the base line"
CoreFont method: ink-extents  ( addr count -- x y width height )  0 max
  0 XRectangle @sizeof ralloca dup >r
  2swap swap fsetid XmbTextExtents drop \ ." !!!" . cr
  r> (xrect>stack) XRectangle @sizeof rdealloca
;

;; y is "up from the base line"
CoreFont method: log-extents  ( addr count -- x y width height )  0 max
  XRectangle @sizeof ralloca dup >r
  0 2swap swap fsetid XmbTextExtents drop \ ." !!!" . cr
  r> (xrect>stack) XRectangle @sizeof rdealloca
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CoreFont method: fill  ( addr count x y winobj -- )
  >r ( save winobj) ascent +  ;; to draw from (x, y)
  swap 2swap swap 2swap r@ ::invoke BaseWindow wingc fsetid r> ::invoke BaseWindow my-drawable xog-dpy XmbDrawImageString
;

CoreFont method: draw  ( addr count x y winobj -- )
  >r ( save winobj) ascent +  ;; to draw from (x, y)
  swap 2swap swap 2swap r@ ::invoke BaseWindow wingc fsetid r> ::invoke BaseWindow my-drawable xog-dpy XmbDrawString
;


previous previous
