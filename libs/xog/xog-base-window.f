;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- BaseWindow class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: oof

;; single-linked list of all *created* windows
;; used to dispatch events
0 value (winlist-tail)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 oop:class
  field (prev-win)  ;; for winlist
  field (event)     ;; current event is stored here; using this out of event handlers is UB

  ;; winlist management
  ;; no sanity checks
  method (register)  ( -- )
  method (unregister)  ( -- )

  class-method find-by-wid  ( wid -- ptr//0 )

  ;;;; WARNING! do not call internal dispatches manually
  ;;;; WARNING! you cannot call "dispatch-event" from event handlers -- it is UB, and may crash
  ;;;;          i.e. no recursive event dispatching is allowed

  ;; this calls parent sink, then does self
  method (dispatch-event-sink)  ( event -- )
  method (dispatch-event-internal)  ( event -- )

  ;; dispatch X11 event -- find the window and perform sink/bubble
  class-method dispatch-event  ( event -- dispatch-success-flag )

  ;; destroy all windows when X11 display is closed
  class-method (display-closing)  ( -- )

  ;; send special "close" event (does no variable checks)
  method (send-close-event)  ( -- )
  ;; send "repaint window" event (does no variable checks)
  method (send-expose-event)  ( -- )

  ;; BaseWindow
  field parent
  field next-sibling
  field first-child
  field focused-child

  method prev-sibling  ( -- childobj // 0 )
  method top-parent  ( -- obj )

  ;; this does depth-first traversal
  ;; cfa: ( -- stopflag )
  ;; self is set to the corresponding window
  method foreach-child  ( cfa -- exitcode )

  ;; simple assign, no checks, no nothing
  method (focused-child!)  ( childobj -- )

  ;; doesn't do anything special (but may change "focused-child", yet without calling "focus" or something)
  method (remove-child)  ( childobj -- )
  method (append-child)  ( childobj -- )

  ;; various window creation parameters
  ;; should be set after "init", but before "create"
  field min-width
  field min-height
  field max-width   ;; <=0: unlimited
  field max-height  ;; <=0: unlimited
  field motion-events?   ;; default is "false"
  field (can-focus?)     ;; default is "true"
  field double-buffered? ;; create pixmap in "create-ex"? don't change after the window was created! default is "true"
  field (first-paint?)   ;; call "on-draw" on the first expose

  ;; called by "create-ex"
  method bg-color  ( -- color )

  ;; window position will be used in "create"; it *may* be updated for child windows
  ;; for top-level windows it is usually only a hint, and not updated after window creation
  field posx
  field posy

  ;; window size: will be used in "create", and updated aftewards
  field width
  field height

  ;; will be set after successfull "create"
  field winid     ;; X11 window handle
  field wingc     ;; default GC for this window
  field winpixmap ;; pixmap, for double-buffered windows
  field visible?  ;; initial and current; initial is hidden
  field invalidate-sent?  ;; internal flag, to avoid sending a lot of expose events
  field close-sent?  ;; internal flag, to avoid sending a lot of close events
  field mapped?   ;; internal flag, set/reset in "show" and "hide"
  field focused?  ;; set in focus receive/lost events; note that child parent is not focused
  field dirty?    ;; should we perform a full redraw?

  method dirty!  ( -- )
  method non-dirty!  ( -- )

  ;; may used by font renderer
  field current-fg-color  ;; default is white
  field current-bg-color  ;; default is black

  ;; this should be called after object creation
  ;; also called by "(destroy-cleanup)" to clear the object
  method init

  ;; destroy child windows, and this window
  method (destroy)  ( -- )

  ;; called *after* window is destroyed (i.e. "win" is invalid here)
  ;; the window is already removed from global window list
  method (destroy-cleanup)  ( -- )

  method (init-gc)  ( -- )
  method (deinit-gc)  ( -- )

  method (init-pixmap)  ( -- )
  method (deinit-pixmap)  ( -- )

  method (set-xhints)  ( -- )

  ;; "event" must be keyboard event; returns keysym for the pressed/released key
  method (get-keysym)  ( event -- keysym )

  ;; override this to change default window class
  method (get-class)  ( -- addr count )

  ;; override this to change default window/widget title
  method get-caption  ( -- addr count )
  ;; this does nothing by default
  method set-caption  ( addr count -- )

  ;; create window
  ;; after successfull creation calls "(set-xhints)" and "(init-gc)"
  ;; creates all appended children
  method create-ex  ( parentobj -- successflag )

  ;; creates top-level window
  ;; creates all appended children
  method create  ( -- )

  ;; this can be called before "create" or "create-ex"
  method append-child ( wininst -- )

  ;; returs "true" if the window is created and can be used
  ;; you can (should) call this after "create" to check if everything is ok
  method is-valid?  ( -- flag )

  ;; returns "(can-focus?)" by default
  method can-focus?  ( -- flag )

  ;; called from MapNotify
  method (map-children)  ( -- )

  ;; called from "create-ex"
  method (create-children)  ( -- )

  ;; used to check if this control must be focused
  ;; checks all "focused-child"s
  method (need-focus?)  ( -- flag )

  ;; focus the child that should have a focus ;-)
  method (focus-child)  ( -- )

  ;;;; handlers for all X11 events ;;;;
  ;; "(process-event)" will call them by name
  ;; "(event)" must point at valid X11 event structure
  method KeyPress-Handler  ( -- )
  method KeyRelease-Handler  ( -- )
  method ButtonPress-Handler  ( -- )
  method ButtonRelease-Handler  ( -- )
  method MotionNotify-Handler  ( -- )
  method EnterNotify-Handler  ( -- )
  method LeaveNotify-Handler  ( -- )
  method FocusIn-Handler  ( -- )
  method FocusOut-Handler  ( -- )
  method KeymapNotify-Handler  ( -- )
  method Expose-Handler  ( -- )
  method GraphicsExpose-Handler  ( -- )
  method NoExpose-Handler  ( -- )
  method VisibilityNotify-Handler  ( -- )
  method CreateNotify-Handler  ( -- )
  method DestroyNotify-Handler  ( -- )
  method UnmapNotify-Handler  ( -- )
  method MapNotify-Handler  ( -- )
  method MapRequest-Handler  ( -- )
  method ReparentNotify-Handler  ( -- )
  method ConfigureNotify-Handler  ( -- )
  method ConfigureRequest-Handler  ( -- )
  method GravityNotify-Handler  ( -- )
  method ResizeRequest-Handler  ( -- )
  method CirculateNotify-Handler  ( -- )
  method CirculateRequest-Handler  ( -- )
  method PropertyNotify-Handler  ( -- )
  method SelectionClear-Handler  ( -- )
  method SelectionRequest-Handler  ( -- )
  method SelectionNotify-Handler  ( -- )
  method ColormapNotify-Handler  ( -- )
  method ClientMessage-Handler  ( -- )
  method MappingNotify-Handler  ( -- )
  method GenericEvent-Handler  ( -- )
  method UnknownEvent-Handler  ( -- )

  ;; called when WM_STATE property was changed
  method PropertyNotify-Handler-WM_STATE  ( -- )

  ;; this will call the corresponding event handler
  ;; it works exactly as written:
  ;;   first the event sinks from the top window to the destination, calling "(sink-event)"
  ;;   then, destination got "(process-event)"
  ;;   then, the event bubbles up to the top, calling "(bubble-event)"
  ;; note that the destination reveives only "(process-event)"
  ;; at any step you can set event type to 0 to "eat" it
  ;; things you should not do:
  ;;   don't remove windows (calling "close" is ok, creating new windows is ok)
  ;;   don't change event destination (this is UB)
  ;;   but you can convert event to something completely different, if you want to; just be careful
  method (sink-event)  ( -- )
  method (process-event)  ( -- )
  method (bubble-event)  ( -- )

  method (is-kb-focus-forward-event?)  ( event -- flag )
  method (check-do-kb-focus)  ( -- )

  method (gain-focus)  ( -- successflag )

  method (kb-focus-first)  ( -- successflag )
  method (kb-focus-forward)  ( -- successflag )
  method (kb-focus-backward)  ( -- successflag )  \ not implemeted yet

  ;; can be called in any event handler, does all the checks it needs
  method (kb-broadcast-hotkey)  ( -- )

  ;; this is called with KeyPress event
  method check-hotkey  ( keyevent -- boolflag )

  ;; call this to stop event propagation
  method event-eat  ( -- )

  ;;;; various high-level control methods ;;;;

  ;; sends "close event", sets "close-sent?"
  ;; this will not immediately destroy a window, so it can be used in various handlers
  ;; the window will be destroyed later, in event loop
  ;; can be called for non-valid window
  method close  ( -- )

  ;; send "repaint event", sets "invalidate-sent?"
  ;; it doesn't change "dirty?" flag
  ;; can be called for non-valid window
  method weak-invalidate  ( -- )

  ;; send "repaint event", sets "invalidate-sent?"
  ;; you can use this to mark window "dirty", so event loop will call repaint event later
  ;; it sets "dirty?" flag
  ;; can be called for non-valid window
  method invalidate  ( -- )

  ;; flush X11 events; can be called to force-send repaint commands
  ;; most of the time you don't need to call this (it will slow things down)
  ;; can be called for non-valid window
  method flush  ( -- )

  ;; sync X11 events; can be called to force-send repaint commands
  ;; most of the time you don't need to call this (it will REALLY slow things down)
  ;; can be called for non-valid window
  method sync  ( -- )

  ;; show (map) window
  ;; can be called for non-valid window
  method show  ( -- )

  ;; hide (unmap) window
  ;; can be called for non-valid window
  method hide  ( -- )

  method focus  ( -- )

  ;; this can be called before and after "create"
  ;; calling after "create" should work, but it's not guaranteed
  method set-pos  ( x y -- )
  method set-size  ( width height -- )
  method set-min-size  ( minwidth minheight -- )
  method set-max-size  ( maxwidth maxheight -- )

  ;;;; the following methods won't check if the window is valid! ;;;;

  ;; event handlers, called by the corresponding "*-Handler" methods
  ;; override this handlers instead of hooking the above

  ;; called after the window was succesfully created
  ;; WARNING! *NOT* from CreateNotify!
  method on-created  ( -- )

  ;; called *after* window is destroyed (i.e. "winid" is invalid here)
  ;; the window is already removed from global window list
  method on-destroyed  ( -- )

  ;; window visibility changed (*NOT* mapping)
  method on-visibility ( visible-flag -- )

  ;; window mapping changed
  method on-show   ( -- )
  method on-hide   ( -- )

  ;; "focused?" will be changed after calling the corresponding handler
  method on-focus  ( -- )
  method on-blur  ( -- )

  ;; returns either window, or a pixmap
  ;; checks both "double-buffered?" and "winpixmap"
  method my-drawable  ( -- drw )

  ;; call this when you finish painting, to copy window pixmap to the screen
  ;; will be automatically called by expose handler
  method realize  ( -- )
  method realize-part  ( x y w h -- )

  ;; this is called for expose events
  ;; "count" tells if there are more expose events in the queue
  ;; coords and size is update region
  ;; (i.e. if "count" is non-zero, this is partial update)
  ;; if you don't want to perform partial updates, you can ignore non-zero count repaint requests
  ;; default handler calls "on-draw" when the count is zero
  method on-draw-part   ( x y width height count -- )

  ;; called by the default "on-draw-part" handler for the last expose event
  method on-draw   ( -- )

  ;; window geometry (configuration) changed
  method on-resize ( oldwidth oldheight -- )

  ;; keyboard key pressed
  method on-keydown  ( keysym -- )

  ;; keyboard key released
  method on-keyup  ( keysym -- )

  ;; mouse button pressed
  method on-button-down  ( bnum -- )

  ;; mouse button released
  method on-button-up  ( bnum -- )

  ;; WM sent close requiest
  ;; return "false" to prevent closing
  method on-close-query  ( -- allow-close-flag )

  ;; WithdrawnState / NormalState / IconicState
  method on-state-change  ( newstate -- )

  ;;;; simple drawing ;;;;
  method set-color  ( color -- )
  method set-named-color  ( addr count -- )

  method set-bg-color  ( color -- )
  method set-named-bg-color  ( addr count -- )

  ;; thin lines
  method set-line-style  ( style -- )
  method set-line-solid  ( -- )
  method set-line-dashed  ( -- )

  method draw-point  ( x y -- )
  method draw-line  ( x0 y0 x1 y1 -- )
  method fill-rect  ( x y w h -- )
  method draw-rect  ( x y w h -- )
  method draw-ellipse  ( x0 y0 w h -- )
  method fill-ellipse  ( x0 y0 w h -- )
  method draw-rounded-rect  ( x y w h ew eh -- )
  method fill-rounded-rect  ( x y w h ew eh -- )

  ;;;;;;
  method (debug-id.)  ( -- )
  method (debug-dump-children)  ( indent -- )
end-class: BaseWindow

" xog-base-window-impl.f" tload
