;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- event loop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: x11

also x11 also xconst

create xog-event XEvent @sizeof allot

false value xog-event-loop-exit?

;; <0: wait forever
;;  0: no wait
;; >0: wait until this os:GetTickCount
;;     if os:GetTickCount is lower or equal, do not wait
;; this autoresets to -1 when the time is reached
-1 value xog-event-loop-next-tick


;; this will override any existing timeout
;; negative means "forever"
: xog-set-event-timeout  ( toutmsecs -- )
  dup +if os:GetTickCount + else -1 max endif
  to xog-event-loop-next-tick
;

: xog-has-event-timeout?  ( -- flag )  xog-event-loop-next-tick 0>= ;


;; you can check/change "xog-event" here
;; "xog-event-loop-next-tick" is autoreset, and you may set it again
: (xog-before-event-handle)  ( -- )  ... ;  ;; called even if there is no pending event
: (xog-after-event-read)  ( -- )  ... ;     ;; called after event was read, but before dispatching; you can set type to 0 to ignore this event
: (xog-after-event-handle)  ( -- )  ... ;   ;; called even if there was no pending event


: xog-has-pending-event?  ( -- flag ) xog-dpy dup if XPending endif ;

: xog-handle-event  ( -- )
    \ forth:depth .
  sp@ >r
  (xog-before-event-handle) r@ (xog-fix-stack)
  xog-event xog-dpy XNextEvent
  (xog-after-event-read) r@ (xog-fix-stack)
  xog-event @ if
    xog-event BaseWindow ::invoke BaseWindow dispatch-event ifnot
      ." cannot dispatch event " xog-event @ get-x11-event-type-name type ."  wid=" xog-event XAnyEvent window @ .hex8 cr
    endif
     r@ (xog-fix-stack)
  endif
  (xog-after-event-handle) r> (xog-fix-stack)
    \ forth:depth .
;


: xog-event-loop  ( -- )
  false to xog-event-loop-exit?
  begin xog-event-loop-exit? not-while
    begin xog-has-pending-event? while xog-handle-event repeat
    -1  ;; calculate mswait; -1 means "forever"
    xog-event-loop-next-tick 0>= if
      xog-event-loop-next-tick os:GetTickCount 2dup u<= if 2drop drop 0  ;; no wait
      else - nip endif  ;; has wait time
    endif
    ;; reset wait time if necessary
    dup 0<= if -1 to xog-event-loop-next-tick endif
    ;; need wait?
    dup +if  ;; timeout
      ;; pending events already sent by the above handle loop
      os:#pollfd ralloca >r
      xog-dpy XConnectionNumber r@ os:pollfd.fd !
      [ os:poll-in ( os:poll-out or) ] literal r@ os:pollfd.events !  ;; this also clears revents
      r> 1 rot os:poll  drop  ;; we don't need any result
      os:#pollfd rdealloca
    else if xog-handle-event  ;; negative
    else xog-has-pending-event? ifnot (xog-before-event-handle) (xog-after-event-handle) endif  ;; ensure timer ticker
    endif endif
  (winlist-tail) not-until
;



previous previous
