;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- style (colors)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary xog-style also xog-style definitions

(* color struct:
     dd typeid
     dd xcolor
     dd prev
     db 32  ;; color name
*)

333_633 constant typeid-color

0 var (latest-clr)

: (typeid^)  ( addr -- addr ) ; immediate
: (xcolor^)  ( addr -- addr ) cell+ ;
: (prev^)  ( addr -- addr ) 2 +cells ;
: (name^)  ( addr -- addr ) 3 +cells ;
: (name@)  ( addr -- addr count ) dup if (name^) bcount else drop " #000000" endif ;

;; xog-dpy must be valid
: update-style  ( -- )  (latest-clr) begin @ dup while dup (name@) xog-get-color over (xcolor^) ! (prev^) repeat drop ;

: (create-color)  ( addr count -- )  \ cname
  dup 1 32 within not-?abort" invalid color name"
  create here typeid-color , 0 ,  ;; typeid, xcolor
  (latest-clr) @ , (latest-clr) ! ;; update link
  32 n-allot dup 32 erase c1s:copy-counted
 does> ( pfa )
  (xcolor^) @
; (hidden)

: to  ( addr count -- )  \ cname
  dup 1 32 within not-?abort" invalid color name"
  parse-name vocid: xog-style voc-search-noimm ifnot
    (abort-msg-reset) " xog color \`" (abort-msg-type) (abort-msg-type)
    " \` not found" (abort-msg-type) (abort-with-built-msg)
  else cfa->pfa dup @ typeid-color = not-?abort" not a xog color" cell+ c1s:copy-counted update-style
  endif
; immediate

..: (xog-dpy-after-open)  ( -- )  update-style ;..


" #aaa" (create-color) background-color
" #000" (create-color) text-color

" #ccc" (create-color) background-hover-color
" #000" (create-color) text-hover-color

" #fff" (create-color) background-active-color
" #000" (create-color) text-active-color

" #00e" (create-color) background-selection-color
" #ff0" (create-color) text-selection-color

" #aaa" (create-color) face-color
" #555" (create-color) shadow-color
" #000" (create-color) dark-color
" #fff" (create-color) light-color

" #fff" (create-color) editor-background-color
" #aaa" (create-color) editor-background-ro-color

" #0e0" (create-color) editor-background-match-color
" #000" (create-color) editor-text-match-color

" #777" (create-color) slider-background-color
" #555" (create-color) slider-background-active-color
" #555" (create-color) slider-track-color

" #777" (create-color) scrollbar-background-color


previous definitions
