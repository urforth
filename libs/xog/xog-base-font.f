;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- BaseFont class
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-libs: oof

bitenum{
  0 set
  value FontType-Normal
  value FontType-Bold
  value FontType-Italic
}


0 oop:class
  field ascent
  field descent
  field max-width

  ;; builds font name at PAD
  method build-font-name  ( size fonttype -- addr count )

  ;; this should be called after object creation
  ;; also called by "destroy" to clear the object
  method init  ( -- )

  method create  ( addr count -- successflag )
  method destroy  ( -- )

  method is-valid?  ( -- flag )
  method ascent@  ( -- ascent )
  method descent@  ( -- descent )
  method height@  ( -- height )
  method max-width@  ( -- max-width )

  ;;;; the following methods do not check for font validity ;;;

  ;; y is "up from the base line"
  method ink-extents  ( addr count -- x y width height )
  method log-extents  ( addr count -- x y width height )

  method ink-width  ( addr count -- width )
  method ink-height  ( addr count -- height )
  method log-width  ( addr count -- width )
  method log-height  ( addr count -- height )

  method fill  ( addr count x y winobj -- )  ;; with background
  method draw  ( addr count x y winobj -- )
end-class: BaseFont


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseFont method: init  ( -- )
  0 to ascent 0 to descent 0 to max-width
;

BaseFont method: destroy  ( -- )  init ;

BaseFont method: is-valid?  ( -- flag )  false ;
BaseFont method: ascent@  ( -- ascent )  ascent ;
BaseFont method: descent@  ( -- descent )  descent ;
BaseFont method: height@  ( -- height )  ascent descent + ;
BaseFont method: max-width@  ( -- max-width )  max-width ;

BaseFont method: ink-width  ( addr count -- width )  ink-extents 2swap 2drop drop ;
BaseFont method: ink-height  ( addr count -- height )  ink-extents 2swap 2drop nip ;
BaseFont method: log-width  ( addr count -- width )  log-extents 2swap 2drop drop ;
BaseFont method: log-height  ( addr count -- height )  log-extents 2swap 2drop nip ;
