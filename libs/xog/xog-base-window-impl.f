;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 OOF GUI -- BaseWindow class implementation
;; (included from "xog-base-window.f")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; we need locals for rounded rect drawing methods
use-libs: locals x11

also x11 also xconst

false value (basewin-debug-show-events?)

false constant xog-window-debug-child-delete?
false constant xog-window-debug-child-create?
false constant xog-window-debug-dispatch?
false constant xog-window-debug-kbfocus?


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; destroy all windows when X11 display is closed
;;
..: (xog-dpy-before-close)  ( -- )
  BaseWindow ::invoke BaseWindow (display-closing)
;..


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: (debug-id.)  ( -- )
  ." <" class-name type ." :" self .hex8 ." :" winid .hex8 ." :" get-caption safe-type ." >"
;

BaseWindow method: (debug-dump-children)  ( indent -- )
  dup spaces (debug-id.) cr
  first-child begin ?dup while
  over 2+ over invoke (debug-dump-children) invoke next-sibling repeat
  drop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; global window list management
;;
BaseWindow method: (register)  ( -- )
  (winlist-tail) to (prev-win) self to (winlist-tail)
    \ ." registered window " win . ." self=0x" self .hex8 ."  winlist-tail=0x" (winlist-tail) .hex8 cr
;

BaseWindow method: (unregister)  ( -- )
  (winlist-tail) self = if
    (prev-win) to (winlist-tail)
  else
    ;; find next window
    (winlist-tail) begin dup invoke (prev-win) dup while dup self = not-while nip repeat
    not-?abort" cannot find previous window"
    (prev-win) swap var^ (prev-win) !
  endif
;

BaseWindow method: find-by-wid  ( wid -- ptr//0 )
  dup ifnot exit endif  ;; just in case
  (winlist-tail) begin dup while 2dup invoke winid = not-while invoke (prev-win) repeat nip
;


;; destroy all windows when X11 display is closed
BaseWindow method: (display-closing)  ( -- )
    \ ." killing all windows...\n"
  begin (winlist-tail) ?dup while (winlist-tail) invoke (destroy) repeat
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find window and dispatch the event
;;
;; this calls parent sink, then does self
BaseWindow method: (dispatch-event-sink)  ( event -- )
  to (event)
  [ xog-window-debug-dispatch? ] [IF]
    ."   going up; curr is " (debug-id.) cr
  [ENDIF]
  parent ?dup if (event) swap invoke (dispatch-event-sink) endif
  [ xog-window-debug-dispatch? ] [IF]
    ."   calling \`(sink-event)\`; curr is " (debug-id.) cr
  [ENDIF]
  (event) @ if (sink-event) endif
  0 to (event)
;

BaseWindow method: (dispatch-event-internal)  ( event -- )
  to (event)
  ;; sink
  parent ?dup if (event) swap invoke (dispatch-event-sink) endif
  ;; destination
  [ xog-window-debug-dispatch? ] [IF]
    ."   calling \`(process-event)\`; curr is " (debug-id.) cr
  [ENDIF]
  (event) @ ifnot exit endif (process-event)
  ;; bubble
  parent begin dup while (event) @ while
    [ xog-window-debug-dispatch? ] [IF]
      ."   calling \`(bubble-event)\`; curr is " dup invoke (debug-id.) cr
    [ENDIF]
    (event) over var^ (event) !  ;; hack: set (event) in current instptr
    dup invoke (bubble-event)
    dup var^ (event) 0!  ;; and clear it, why not
  invoke parent repeat drop
;

BaseWindow method: dispatch-event  ( event -- dispatch-success-flag )
  dup XAnyEvent display @ xog-dpy = ifnot drop false exit endif
  dup XAnyEvent window @ find-by-wid dup if
    [ xog-window-debug-dispatch? ] [IF]
      endcr ." *********** dispatching winid " over XAnyEvent window @ .hex8 ."  to " dup invoke (debug-id.) cr
      ."   EVENT: type=" over @ . over @ get-x11-event-type-name type cr
    [ENDIF]
    invoke (dispatch-event-internal) true
  else
    [ xog-window-debug-dispatch? ] [IF]
      endcr ." cannot dispatch winid " over XAnyEvent window @ .hex8 cr
      ."   EVENT: type=" over @ . over @ get-x11-event-type-name type cr
    [ENDIF]
    nip
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various helper methods
;;

BaseWindow method: prev-sibling ( -- childobj // 0 )
  parent dup if
    invoke first-child 0 swap begin ( prev curr ) dup while
      dup self = if drop exit endif
    nip dup invoke next-sibling repeat nip
  endif
;

BaseWindow method: top-parent  ( -- obj )
  parent ?dup if invoke top-parent else self endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; children list management
;;

;; simple assign, no checks, no nothing
BaseWindow method: (focused-child!)  ( childobj -- )  to focused-child ;


BaseWindow method: (remove-child)  ( childobj -- )
  \ ?dup ifnot exit endif
  dup not-?abort" cannot orphan already orphan window"
  dup invoke parent self <> ?abort" cannot orphan alien window"
  dup focused-child = if 0 to focused-child endif  ;; just in case
  dup invoke prev-sibling  ( childobj prevchildobj/0 )
  over var^ parent 0!  ;; orphan it
  ?dup if over invoke next-sibling swap var^ next-sibling !  ;; remove from the list
  else dup invoke next-sibling to first-child endif  ;; first one
  var^ next-sibling 0!
;


BaseWindow method: (append-child)  ( childobj -- )
  ?dup ifnot exit endif
  dup invoke parent ?abort" cannot parent already parented window"
  dup invoke next-sibling ?abort" cannot parent already parented window"
  self over var^ parent !  ;; adopt it
  first-child ?dup if
    begin dup invoke next-sibling ?dup while nip repeat
    var^ next-sibling !
  else to first-child endif  ;; first child
;

BaseWindow method: append-child  ( childobj -- )
  dup (append-child)
  focused-child ifnot dup invoke can-focus? if dup to focused-child endif endif drop
;


;; called from MapNotify
BaseWindow method: (map-children)  ( -- )
  is-valid? ifnot exit endif
  first-child begin ?dup while
    dup invoke visible? over invoke mapped? or if
      [ xog-window-debug-child-create? ] [IF]
        endcr ." mapchild: " dup invoke (debug-id.) cr
      [ENDIF]
      dup var^ mapped? 0!
      dup var^ visible? 1!
      dup invoke (map-children)
    endif
  invoke next-sibling repeat
;


BaseWindow method: (create-children)  ( -- )
  is-valid? ifnot exit endif
  first-child begin ?dup while
    dup invoke visible? over invoke mapped? or if
      [ xog-window-debug-child-create? ] [IF]
        endcr ." createchild: " dup invoke (debug-id.) cr
      [ENDIF]
      self over invoke create-ex not-?abort" cannot create child window"
    endif
  invoke next-sibling repeat
;


BaseWindow method: (focus-child)  ( -- )
  focused-child ?dup if
      \ endcr ." ---self " (debug-id.) ."  is going to focus child " dup invoke (debug-id.) cr
    invoke (focus-child)
  else
      \ endcr ." ***FOCUSING: " (debug-id.) cr
    focus
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; children traversal
;;

;; this does depth-first traversal
;; cfa: ( -- stopflag )
;; self is set to the corresponding window
BaseWindow method: foreach-child  ( cfa -- exitcode )
  first-child begin ?dup while  ( cfa childobj )
    2dup 2>r invoke foreach-child ?dup if 2rdrop exit endif
  2r> invoke next-sibling repeat
  execute-tail  ;; self is last
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; default window class and title
;;
BaseWindow method: (get-class)  ( -- addr count )  " UrForth BaseWindow" ;
BaseWindow method: get-caption  ( -- addr count )  " UrForth Window" ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; send various internal events
;;
BaseWindow method: (send-close-event)  ( -- )
  XEvent @sizeof ralloca >r
  r@ XEvent @sizeof erase
  ClientMessage r@ XClientMessageEvent type !
  r@ XClientMessageEvent send_event 1!
  winid r@ XClientMessageEvent window !
  XOF_INTERNAL_CLOSE r@ XClientMessageEvent message_type !
  32 r@ XClientMessageEvent format !
  r@ 0 true winid xog-dpy XSendEvent drop
  rdrop XEvent @sizeof rdealloca
;


BaseWindow method: (send-expose-event)  ( -- )
  XEvent @sizeof ralloca >r
  r@ XEvent @sizeof erase
  Expose r@ XExposeEvent type !
  r@ XExposeEvent send_event 1!
  winid r@ XExposeEvent window !
  r@ XExposeEvent x 0!
  r@ XExposeEvent y 0!
  width r@ XExposeEvent width !
  height r@ XExposeEvent height !
  r@ 0 true winid xog-dpy XSendEvent drop
  rdrop XEvent @sizeof rdealloca
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; reset all internal fields
;;
BaseWindow method: init  ( -- )
  0 to (prev-win)

  0 to parent
  0 to next-sibling
  0 to first-child
  0 to focused-child

  0 to min-width 0 to min-height
  0 to max-width 0 to max-height
  false to motion-events?
  true to (can-focus?)

  0 to posx 0 to posy
  512 to width 256 to height

  0 to winid 0 to wingc 0 to winpixmap
  false to visible?
  false to invalidate-sent?
  false to close-sent?
  false to mapped?
  false to focused?
  true to dirty?
  true to double-buffered?
  true to (first-paint?)

  0 to current-fg-color 0 to current-bg-color
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; low-level window methods
;;
BaseWindow method: (get-keysym)  ( event -- keysym )
  0 swap XKeyEvent keycode @ xog-dpy XKeycodeToKeysym
;

BaseWindow method: (destroy-cleanup)  ( -- )
  (deinit-gc) (deinit-pixmap) (unregister) on-destroyed init
;

BaseWindow method: (init-gc)  ( -- )
  0 0 my-drawable xog-dpy XCreateGC to wingc drop  ;; "drop" due to "..."
  set-line-solid
  ;; invalidate colors
  xog-black-color 1+ to current-bg-color
  xog-white-color 1+ to current-fg-color
  ;; background is black, foreground is white
  xog-black-color set-bg-color
  xog-white-color set-color
;

BaseWindow method: (deinit-gc)  ( -- )
  wingc ?dup if xog-dpy XFreeGC 0 to wingc endif
;

BaseWindow method: (init-pixmap)  ( -- )
  (deinit-pixmap) xog-dpy if
    xog-dpy DefaultScreen xog-dpy XDefaultDepth height 1 max width 1 max winid xog-dpy XCreatePixmap
    to winpixmap dirty!
  endif
;

BaseWindow method: (deinit-pixmap)  ( -- )
  winpixmap if xog-dpy if winpixmap xog-dpy XFreePixmap endif 0 to winpixmap endif
;

BaseWindow method: (set-xhints)  ( -- )
  (get-class) ensure-asciiz >r
  ;; class hints
  XClassHint @sizeof ralloca >r
  r@ XClassHint @sizeof erase
  dup r@ XClassHint res_name ! r@ XClassHint res_class !
  r@  ;; class hints address
  ;; WM hints
  XWMHints @sizeof ralloca >r
  r@ XWMHints @sizeof erase
  InputHint r@ XWMHints flags !
  true r@ XWMHints input !
  r@  ;; WM hints address
  ;; size hints
  XSizeHints @sizeof ralloca >r
  r@ XSizeHints @sizeof erase
  min-width 0 max min-height 0 max or if
    PMinSize r@ XSizeHints flags or!
    min-width r@ XSizeHints min_width !
    min-height r@ XSizeHints min_height !
  endif
  max-width 0 max max-height 0 max or if
    PMaxSize r@ XSizeHints flags or!
    max-width r@ XSizeHints max_width !
    max-height r@ XSizeHints max_height !
  endif
  r@  ;; size hints address
  0 0 0 0  ;; argc, argv, iconname windowname
  winid xog-dpy XSetWMProperties drop
  ;; free structures
  rdrop XSizeHints @sizeof rdealloca
  rdrop XWMHints @sizeof rdealloca
  rdrop XClassHint @sizeof rdealloca
  r> free-asciiz
;


;; destroy child windows, and this window
BaseWindow method: (destroy)  ( -- )
  [ xog-window-debug-child-delete? ] [IF]
    endcr ." removing chldren of " (debug-id.) cr
  [ENDIF]
  ;; destroy children
  begin first-child ?dup while invoke (destroy) repeat
  ;; remove ourself from the parent list
  parent ?dup if
    [ xog-window-debug-child-delete? ] [IF]
      endcr ." removing " (debug-id.) ."  from the parent\n"
      ."  parent is " dup invoke (debug-id.) cr
    [ENDIF]
    self swap invoke (remove-child)
  endif
  [ xog-window-debug-child-delete? ] [IF]
    endcr ." removing " (debug-id.) ."  from the parent\n"
  [ENDIF]
  xog-dpy if
    winid ?dup if
      [ xog-window-debug-child-delete? ] [IF]
        endcr ." destroying xwin of " (debug-id.) cr
      [ENDIF]
      xog-dpy XDestroyWindow drop
    endif
  endif
  winid if
    [ xog-window-debug-child-delete? ] [IF]
      endcr ." calling \`(destroy-cleanup)\` of " (debug-id.) cr
    [ENDIF]
    0 to winid (destroy-cleanup)
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level window creation
;;
BaseWindow method: create-ex  ( parentobj -- successflag )
  xog-dpy 0= winid 0<> or if drop false exit endif
  ;; check for valid parent
  dup if
    dup invoke winid ifnot drop false exit endif
    parent ?dup if over <> if drop false exit endif endif
  endif
  >r  ;; save parent object
  ;; XCreateSimpleWindow arguments
  bg-color         ;; background color
  xog-black-color  ;; border color
  0                ;; border width
  ;; position (we cannot create windows with zero dimensions)
  height 1 max dup to height
  width 1 max dup to width
  posy posx  ;; h w y x
    [ xog-window-debug-child-create? ] [IF]
      endcr ." creating " (debug-id.) 2dup ."  x=" . ." y=" . 2over ." w=" . ." h=" . cr
    [ENDIF]
  r@ if r@ invoke winid else xog-dpy XDefaultRootWindow endif  ;; parent
  xog-dpy XCreateSimpleWindow dup to winid ifnot rdrop false exit endif  ;; oops, cannot create a window
  ;; register in global window list
  (register)
  r> ?dup ifnot
    ;; set title
    get-caption 0 255 clamp
    ensure-asciiz >r winid xog-dpy XStoreName r> free-asciiz
    ;; set WM protocols (only for top-level windows)
    WM_TAKE_FOCUS >r WM_DELETE_WINDOW >r
    2 rp@ winid xog-dpy XSetWMProtocols drop 2rdrop
    ;; set NETWM type
    NET_WM_WINDOW_TYPE_NORMAL >r
    1 rp@ 0 ( PropModeReplace) 32 ( bits) XA_ATOM NET_WM_WINDOW_TYPE winid xog-dpy XChangeProperty drop rdrop
  else
    ;; append to parent (this also sets "parent" field)
    parent if drop else self swap invoke (append-child) endif
  endif
  ;; set input mask
  [ NoEventMask
    KeyPressMask or
    KeyReleaseMask or
    ButtonPressMask or
    ButtonReleaseMask or
    \ EnterWindowMask or
    \ LeaveWindowMask or
    \ PointerMotionMask or
    \ PointerMotionHintMask or
    \ Button1MotionMask or
    \ Button2MotionMask or
    \ Button3MotionMask or
    \ Button4MotionMask or
    \ Button5MotionMask or
    \ ButtonMotionMask or
    \ KeymapStateMask or
    ExposureMask or
    VisibilityChangeMask or
    StructureNotifyMask or
    \ ResizeRedirectMask or
    \ SubstructureNotifyMask or
    \ SubstructureRedirectMask or
    FocusChangeMask or
    PropertyChangeMask or
    \ ColormapChangeMask or
    \ OwnerGrabButtonMask or
  ] literal motion-events? if PointerMotionMask or endif
  winid xog-dpy XSelectInput
  parent ifnot (set-xhints) endif
  ;; create pixmap
  double-buffered? if
    (init-pixmap)
    ;; replace window background with empty one-pixel pixmap
    winpixmap if
      (*
      xog-dpy DefaultScreen xog-dpy XDefaultDepth 1 1 winid xog-dpy XCreatePixmap
      dup winid xog-dpy XSetWindowBackgroundPixmap drop
      xog-dpy XFreePixmap
      *)
      None winid xog-dpy XSetWindowBackgroundPixmap drop
    endif
  endif
  (init-gc)
  (create-children)
  on-created
  parent visible? logand if false to visible? false to mapped? show true to visible? endif  ;; always map children
  dirty! true  ;; done
;

BaseWindow method: create  ( -- )  0 create-ex not-?abort" cannot create X11 window" ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; getters
;;
BaseWindow method: is-valid?  ( -- flag )  winid notnot ;
BaseWindow method: can-focus?  ( -- flag )  (can-focus?) ;
BaseWindow method: bg-color  ( -- color )  xog-black-color ;

BaseWindow method: dirty!  ( -- )  true to dirty? ;
BaseWindow method: non-dirty!  ( -- )  false to dirty? ;

;; returns either window, or a pixmap
;; checks both "double-buffered?" and "winpixmap"
BaseWindow method: my-drawable  ( -- drw )
  double-buffered? winpixmap logand if winpixmap else winid endif
;

BaseWindow method: realize-part  ( x y w h -- )
  winid double-buffered? logand winpixmap logand if
    over 0> over 0> and if
      swap 2swap swap 2swap 2over  ( y x h w y x )
      wingc winid winpixmap xog-dpy XCopyArea
      exit
    endif
  endif 2drop 2drop
;

;; call this when you finish painting, to copy window pixmap to the screen
BaseWindow method: realize  ( -- )
  winid double-buffered? logand winpixmap logand if
    0 0  ;; desty, destx
    height width
    0 0  ;; srcy, srcx
    wingc winid winpixmap xog-dpy XCopyArea
  endif
;


BaseWindow method: (need-focus?)  ( -- flag )
  parent if  ;; has parent
    parent invoke focused-child self = if  ;; parent focused child is this one
      parent invoke (need-focus?) exit
    endif
  endif false
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 event handlers
;;
BaseWindow method: KeyPress-Handler  ( -- )  (event) (get-keysym) on-keydown ;
BaseWindow method: KeyRelease-Handler  ( -- )  (event) (get-keysym) on-keyup ;
BaseWindow method: ButtonPress-Handler  ( -- )  (event) XButtonEvent button @ on-button-down ;
BaseWindow method: ButtonRelease-Handler  ( -- )  (event) XButtonEvent button @ on-button-up ;
BaseWindow method: MotionNotify-Handler  ( -- )  ;
BaseWindow method: EnterNotify-Handler  ( -- )  ;
BaseWindow method: LeaveNotify-Handler  ( -- )  ;
BaseWindow method: KeymapNotify-Handler  ( -- )  ;
BaseWindow method: GraphicsExpose-Handler  ( -- )  ;
BaseWindow method: NoExpose-Handler  ( -- )  ;
BaseWindow method: VisibilityNotify-Handler  ( -- )  (event) XVisibilityEvent state @ VisibilityFullyObscured <> on-visibility ;
BaseWindow method: CreateNotify-Handler  ( -- )  ;
BaseWindow method: DestroyNotify-Handler  ( -- )  0 to winid (destroy-cleanup) ;
BaseWindow method: MapRequest-Handler  ( -- )  ;
BaseWindow method: ReparentNotify-Handler  ( -- )  ;
BaseWindow method: ConfigureRequest-Handler  ( -- )  ;
BaseWindow method: GravityNotify-Handler  ( -- )  ;
BaseWindow method: ResizeRequest-Handler  ( -- )  ;
BaseWindow method: CirculateNotify-Handler  ( -- )  ;
BaseWindow method: CirculateRequest-Handler  ( -- )  ;
\ BaseWindow method: PropertyNotify-Handler  ( -- )  ;
BaseWindow method: SelectionClear-Handler  ( -- )  ;
BaseWindow method: SelectionRequest-Handler  ( -- )  ;
BaseWindow method: SelectionNotify-Handler  ( -- )  ;
BaseWindow method: ColormapNotify-Handler  ( -- )  ;
BaseWindow method: MappingNotify-Handler  ( -- )  ;
BaseWindow method: GenericEvent-Handler  ( -- )  ;
BaseWindow method: UnknownEvent-Handler  ( -- )  ;

BaseWindow method: FocusIn-Handler  ( -- )
  (event) XFocusChangeEvent detail @  \ dup . cr
  dup NotifyNonlinear = if drop NotifyAncestor endif
  case
    NotifyAncestor of on-focus true to focused? endof
    NotifyPointer of focus endof
  endcase
  parent ifnot
      \ endcr ." going to focus a child...\n"
    (focus-child)
  else
    self parent begin dup while  ( child parent )
        \ endcr ." +++child " over invoke (debug-id.) ."  is focused child for " dup invoke (debug-id.) cr
      2dup invoke (focused-child!)
    nip dup invoke parent repeat 2drop
  endif
;

BaseWindow method: FocusOut-Handler  ( -- )
  (event) XFocusChangeEvent detail @  \ dup . cr
  dup NotifyNonlinear = if drop NotifyAncestor endif
  NotifyAncestor = if on-blur false to focused? endif
;

BaseWindow method: MapNotify-Handler  ( -- )  true to (first-paint?) true to visible? (map-children) on-show ;
BaseWindow method: UnmapNotify-Handler  ( -- )  true to (first-paint?) false to visible? on-hide ;

BaseWindow method: Expose-Handler  ( -- )
  false to invalidate-sent?
  (event) dup >r XExposeEvent x @ r@ XExposeEvent y @
  r@ XExposeEvent width @ dup -0if drop width endif
  r@ XExposeEvent height @ dup -0if drop height endif
  (first-paint?) if dirty!
    false to (first-paint?)
    2drop 2drop 0 0 width height 0  ;; paint the whole thing
  else
    r@ XExposeEvent count @
  endif
  winid double-buffered? logand winpixmap logand ifnot dirty! endif
  on-draw-part
  r@ XExposeEvent x @ r@ XExposeEvent y @
  r@ XExposeEvent width @ dup -0if drop width endif
  r@ XExposeEvent height @ dup -0if drop height endif
    \ endcr ." Expose: x=" 2over swap . ." y=" . ." w=" 2dup swap . ." h=" . ." count=" r@ XExposeEvent count @ . cr
  realize-part
  rdrop
;

BaseWindow method: ConfigureNotify-Handler  ( -- )
  (event) dup XConfigureEvent width @
  swap XConfigureEvent height @
  2dup height = swap width = and ifnot
    width height  ;; old size
    2swap to height to width  ;; set new size
    winpixmap if (deinit-pixmap) (init-pixmap) else dirty! endif
    on-resize
    (send-expose-event)
  else 2drop endif
;

BaseWindow method: ClientMessage-Handler  ( -- )
  (event) XClientMessageEvent message_type @ case
    WM_PROTOCOLS of
      (event) XClientMessageEvent data @ case
        WM_DELETE_WINDOW of on-close-query if close endif endof
        ;; this is rarely used (at least FluxBox doesn't send it)
        WM_TAKE_FOCUS of (event) XClientMessageEvent data cell+ @ (debug-id.) ."  WM_TAKE_FOCUS: " . cr endof
      endcase
    endof
    XOF_INTERNAL_CLOSE of (destroy) endof
    \ WM_CHANGE_STATE of (event) XClientMessageEvent data @ on-state-change endof
  endcase
;

BaseWindow method: PropertyNotify-Handler-WM_STATE  ( -- )
  5 cells ralloca >r
  r@ 5 cells erase
  r@ ( prop_return) r@ cell+ ( bytes_after_return)
  r@ 2 +cells ( nitems_return)
  r@ 3 +cells ( actual_format_return)
  r@ 4 +cells ( actual_type_return)
  AnyPropertyType false 1 0 WM_STATE winid xog-dpy XGetWindowProperty ifnot
    r@ @ @ on-state-change
    r@ @ XFree
  endif
  rdrop 5 cells rdealloca
;

BaseWindow method: PropertyNotify-Handler  ( -- )
  (event) XPropertyEvent atom @ xog-dpy XGetAtomName >r
  endcr (debug-id.) ."  PROPERTY \`" r@ zcount type ." \` is "
  (event) XPropertyEvent state @ PropertyDelete = if ." deleted" else ." set" endif
  cr r> XFree

  (event) XPropertyEvent state @ PropertyNewValue = if
    (event) XPropertyEvent atom @ case
      WM_STATE of PropertyNotify-Handler-WM_STATE endof
    endcase
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; X11 event processor
;; calls "*-Handler" by name
;;

;; call this to stop event propagation
BaseWindow method: event-eat  ( -- )  (event) ?dup if 0! endif ;

;; this is called with KeyPress event
BaseWindow method: check-hotkey  ( keyevent -- boolflag )  drop false ;


;; can be called in any event handler, does all the checks it needs
BaseWindow method: (kb-broadcast-hotkey)  ( -- )
  parent if exit endif  ;; only top-level window does this
  (event) ?dup ifnot exit endif
  dup @ KeyPress = ifnot drop exit endif
  [:  ( keyevent -- stopflag )
    dup (self@) BaseWindow:: check-hotkey
  ;] foreach-child 2drop
;


BaseWindow method: (is-kb-focus-forward-event?)  ( event -- flag )
  dup if dup @ KeyPress = if (get-keysym) XK_Tab = else drop false endif endif
;

BaseWindow method: (gain-focus)  ( -- successflag )
  can-focus? if
    first-child begin dup while
      dup invoke (gain-focus) if drop true break endif
    invoke next-sibling repeat
    ?dup ifnot focus true endif
  else false endif
;


BaseWindow method: (kb-focus-first)  ( -- successflag )
  [ xog-window-debug-kbfocus? ] [IF]
    ." FFIRST " (debug-id.) cr
  [ENDIF]
  (gain-focus)
;

BaseWindow method: (kb-focus-forward)  ( -- successflag )
  [ xog-window-debug-kbfocus? ] [IF]
    endcr ." FFORWARD at " (debug-id.)
    next-sibling if ."  with next sibling as " next-sibling invoke (debug-id.) endif
    cr
  [ENDIF]
  next-sibling begin dup while
    dup invoke (gain-focus) if drop true break endif
  invoke next-sibling repeat
;

BaseWindow method: (check-do-kb-focus)  ( -- )
  (event) (is-kb-focus-forward-event?) if
    (kb-focus-forward) if event-eat
    else parent ifnot (kb-focus-first) if event-eat endif endif
    endif
  endif
;

BaseWindow method: (sink-event)  ( -- )
  (basewin-debug-show-events?) if
    ." SINK EVENT: destwinid=" (event) XAnyEvent window @ .hex8 ."  type=" (event) @ . (event) @ get-x11-event-type-name type cr
  endif
;

BaseWindow method: (bubble-event)  ( -- )
  (basewin-debug-show-events?) if
    ." BUBBLE EVENT: destwinid=" (event) XAnyEvent window @ .hex8 ."  type=" (event) @ . (event) @ get-x11-event-type-name type cr
  endif
  (check-do-kb-focus)
  (kb-broadcast-hotkey)
;

BaseWindow method: (process-event)  ( -- )
  (basewin-debug-show-events?) if
    ." HANDLE EVENT: winid=" (event) XAnyEvent window @ .hex8 ."  type=" (event) @ . (event) @ get-x11-event-type-name type cr
  endif
  sp@ >r
  (event) @ get-x11-event-handler-name dispatch-str
  r> (xog-fix-stack)
  (check-do-kb-focus)
  (kb-broadcast-hotkey)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level window control methods
;;
BaseWindow method: close  ( -- )
  is-valid? if close-sent? ifnot true to close-sent? (send-close-event) endif endif
;

BaseWindow method: weak-invalidate  ( -- )
  is-valid? if invalidate-sent? ifnot true to invalidate-sent? (send-expose-event) endif endif
;

BaseWindow method: invalidate  ( -- )
  is-valid? if dirty! weak-invalidate endif
;

BaseWindow method: sync  ( -- )
  xog-dpy if false xog-dpy XSync endif
;

BaseWindow method: flush  ( -- )
  xog-dpy if xog-dpy XFlush endif
;

BaseWindow method: show  ( -- )
  is-valid? if mapped? ifnot true to mapped? winid xog-dpy XMapWindow endif endif
;

BaseWindow method: hide  ( -- )
  is-valid? if mapped? if false to mapped? false to visible? winid xog-dpy XUnmapWindow endif endif
;

BaseWindow method: focus  ( -- )
  visible? if is-valid? if CurrentTime RevertToParent winid xog-dpy XSetInputFocus endif endif
;

BaseWindow method: set-min-size  ( minwidth minheight -- )
  0 max to min-height  0 max to min-width  is-valid? if (set-xhints) endif
;

BaseWindow method: set-max-size  ( maxwidth maxheight -- )
  0 max to max-height  0 max to max-width  is-valid? if (set-xhints) endif
;

BaseWindow method: set-pos  ( x y -- )
  over posx - over posy - or if
    to posy to posx is-valid? if posy posx winid xog-dpy XMoveWindow endif
  else 2drop endif
;

BaseWindow method: set-size  ( width height -- )
  1 max swap 1 max
  over height - over width - or if
    to width to height is-valid? if height width winid xog-dpy XResizeWindow endif
  else 2drop endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level event handlers
;;
BaseWindow method: on-created  ( -- )  ;
BaseWindow method: on-destroyed  ( -- )  ;
BaseWindow method: on-visibility  ( visible-flag -- )  drop ;
BaseWindow method: on-show  ( -- )  ;
BaseWindow method: on-hide  ( -- )  ;
BaseWindow method: on-focus  ( -- )  ;
BaseWindow method: on-blur  ( -- )  ;
BaseWindow method: on-draw-part  ( x y width height count -- )  >r 2drop 2drop r> ifnot on-draw endif ;
BaseWindow method: on-draw  ( -- )  ;
BaseWindow method: on-resize ( oldwidth oldheight -- )  2drop ;
BaseWindow method: on-keydown  ( keysym -- )  drop ;
BaseWindow method: on-keyup  ( keysym -- )  drop ;
BaseWindow method: on-button-down  ( bnum -- )  drop ;
BaseWindow method: on-button-up  ( bnum -- )  drop ;
BaseWindow method: on-close-query  ( -- allow-close-flag )  true ;
BaseWindow method: on-state-change  ( newstate -- )  drop ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple drawing
;;
BaseWindow method: set-color  ( color -- )  dup to current-fg-color wingc xog-dpy XSetForeground ;
BaseWindow method: set-named-color  ( addr count -- )  xog-get-color set-color ;

BaseWindow method: set-bg-color  ( color -- ) dup to current-bg-color wingc xog-dpy XSetBackground ;
BaseWindow method: set-named-bg-color  ( addr count -- ) xog-get-color set-bg-color ;

BaseWindow method: set-line-style  ( style -- )  JoinMiter CapButt rot 0 wingc xog-dpy XSetLineAttributes drop ;
BaseWindow method: set-line-solid  ( -- )  LineSolid set-line-style ;
BaseWindow method: set-line-dashed  ( -- )  LineOnOffDash set-line-style ;

BaseWindow method: draw-point  ( x y -- )  swap wingc my-drawable xog-dpy XDrawPoint ;
BaseWindow method: draw-line  ( x0 y0 x1 y1 -- )  swap 2swap swap wingc my-drawable xog-dpy XDrawLine ;
BaseWindow method: fill-rect  ( x y w h -- )  swap 2swap swap wingc my-drawable xog-dpy XFillRectangle ;
BaseWindow method: draw-rect  ( x y w h -- )  swap 2swap swap wingc my-drawable xog-dpy XDrawRectangle ;
BaseWindow method: draw-ellipse  ( x0 y0 w h -- )  swap 2swap swap 23040 0 2nrot wingc my-drawable xog-dpy XDrawArc ;
BaseWindow method: fill-ellipse  ( x0 y0 w h -- )  swap 2swap swap 23040 0 2nrot wingc my-drawable xog-dpy XFillArc ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; draw rounded rectangles
;;
BaseWindow method: draw-rounded-rect  {{ x y w h ew eh | arcs curarc ew2 eh2 -- }}
  XArc @sizeof 8 * ralloca dup to arcs to curarc

  ew 0 max to ew
  eh 0 max to eh

  ew 1 lshift dup to ew2 w > if 0 dup to ew2 to ew endif
  eh 1 lshift dup to eh2 h > if 0 dup to eh2 to eh endif

  ;; [0]
  x curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 180 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [1]
  x ew + curarc XArc x w!
  y curarc XArc y w!
  w ew2 - curarc XArc width w!
  0 curarc XArc height w!
  [ 180 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [2]
  x w + ew2 - curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 90 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [3]
  x w + curarc XArc x w!
  y eh + curarc XArc y w!
  0 curarc XArc width w!
  h eh2 - curarc XArc height w!
  [ 90 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [4]
  x w + ew2 - curarc XArc x w!
  y h + eh2 - curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 0 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [5]
  x ew + curarc XArc x w!
  y h + curarc XArc y w!
  w ew2 - curarc XArc width w!
  0 curarc XArc height w!
  [ 0 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [6]
  x curarc XArc x w!
  y h + eh2 - curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 270 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [7]
  x curarc XArc x w!
  y eh + curarc XArc y w!
  0 curarc XArc width w!
  h eh2 - curarc XArc height w!
  [ 270 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!

  8 arcs wingc my-drawable xog-dpy XDrawArcs

  XArc @sizeof 8 * rdealloca
;


BaseWindow method: fill-rounded-rect  {{ x y w h ew eh | arcs curarc rects currect gcvals ew2 eh2 -- }}
  XArc @sizeof 4 * ralloca dup to arcs to curarc
  XRectangle @sizeof 3 * ralloca dup to rects to currect
  XGCValues @sizeof ralloca to gcvals

  gcvals GCArcMode wingc xog-dpy XGetGCValues drop
  gcvals XGCValues arc_mode @ ArcPieSlice <> if
    ArcPieSlice wingc xog-dpy XSetArcMode drop
  endif

  ew 0 max to ew
  eh 0 max to eh

  ew 1 lshift dup to ew2 w > if 0 dup to ew2 to ew endif
  eh 1 lshift dup to eh2 h > if 0 dup to eh2 to eh endif

  ;; [0]
  x curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 180 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [1]
  x w + ew2 - 1- curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 90 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [2]
  x w + ew2 - 1- curarc XArc x w!
  y h + eh2 - 1- curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 0 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [3]
  x curarc XArc x w!
  y h + eh2 - 1- curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 270 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!

  4 arcs wingc my-drawable xog-dpy XFillArcs

  ;; [0]
  x ew + currect XRectangle x w!
  y currect XRectangle y w!
  w ew2 - currect XRectangle width w!
  h currect XRectangle height w!
  currect XRectangle @sizeof + to currect

  ;; [1]
  x currect XRectangle x w!
  y eh + currect XRectangle y w!
  ew currect XRectangle width w!
  h eh2 - currect XRectangle height w!
  currect XRectangle @sizeof + to currect

  ;; [2]
  x w + ew - currect XRectangle x w!
  y eh + currect XRectangle y w!
  ew currect XRectangle width w!
  h eh2 - currect XRectangle height w!

  3 rects wingc my-drawable xog-dpy XFillRectangles

  gcvals XGCValues arc_mode @ ArcPieSlice <> if
    gcvals XGCValues arc_mode @ wingc xog-dpy XSetArcMode drop
  endif

  XGCValues @sizeof rdealloca
  XRectangle @sizeof 3 * rdealloca
  XArc @sizeof 4 * rdealloca
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous previous
