;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple locals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(*
69 value val

: loctest {{ a b c || d  -- }}
  ." hello!\n"
  ." a=" a . cr
  ." b=" b . cr
  ." c=" c . cr
  ." c=" d . cr

  b to d
  ." new d=" d . cr
  10 +to d
  ." new d+10=" d . cr

  ." d by addr=" to^ d @ . cr

  ." val=" val . cr
  666 to val
  ." new val=" val . cr
;
*)

vocabulary (locvoc) also (locvoc) definitions

;; warning! compile-time switch!
false constant use-interpreter-hook (hidden)

0 value locs-buf
0 value locs-vocid
0 value locs-size  ;; in cells
compiler:allocate-ctlid constant (CTLID-CBLOCK-LOCS)

#dp-temp-sbuf buffer: locs-dp-temp


: clear-locals  ( -- )  0 to locs-vocid ;

: compile-locaddr  ( locoffs -- )  [compile] literal compile (local-addr) ;
: compile-locload  ( locoffs -- )  [compile] literal compile (local-load) ;


: (;)
  locs-vocid not-?abort" wtf?!"
  locs-size 1- >r
  clear-locals
  r@ if previous endif
  (CTLID-CBLOCK-LOCS) compiler:?pairs
  r> if
    compiler:cblock-end
    ;; call cblock
    compile execute
    compile free-local-frame
  endif
; (hidden)


use-interpreter-hook [IF]

alias compiler:?comp (?comp)

[ELSE]
: (create-special-word)  ( addr count doescfa -- )
  nrot locs-vocid create-named-in locs-vocid vocid-immediate
  locs-vocid vocid-latest-cfa forth:(does>!)
;

: (?comp)  ( -- )  state @ ifnot previous compiler:?comp endif ;

: (create-override-words)  ( -- )
(*
  " exit" [: drop ( drop pfa)
    (?comp)
    compile free-local-frame
    " EXIT" compiler:late-chain-prevvoc
  ;] (create-special-word)
*)
  " ;" [: drop ( drop pfa)
      \ endcr ." LOCALS: semi: " latest-nfa id. cr
    (?comp) (;) " ;" compiler:late-chain
  ;] (create-special-word)
;

[ENDIF]

: init-locals  ( -- )
  0 to locs-vocid 0 to locs-size
  locs-buf ?dup ifnot 0 alloc-dp-temp dup to locs-buf endif
  locs-dp-temp save-dp-temp setup-dp-temp
  [:
    new-temp-voc to locs-vocid
    [ use-interpreter-hook not ] [IF]
      (create-override-words)
    [ENDIF]
    1 to locs-size
  ;] catch locs-dp-temp swap-dp-temp throw
;

..: forth:(abort-cleanup)  ( -- )  clear-locals ;..
..: forth:(startup-init)  ( -- )  0 to locs-buf 0 to locs-vocid 0 to locs-size ;..

..: forth:(exc0!)  ( -- )  0 (locptr!) clear-locals ;..
..: forth:(catch-saver)  ( -- | locptr restcfa )
  r> (locptr@) >r [: ( restoreflag ) ( locptr )
    r> swap if r> (locptr!) else rdrop endif >r
  ;] >r >r
;.. (hidden)

(*
  local word:
   dd cell-offset
   dd cell-size
*)

: (local-does)  ( pfa -- )  (?comp) @ compile-locload ; (hidden)

: (create-local-word)  ( addr count cell-size -- )
  locs-dp-temp swap-dp-temp
  get-current >r locs-vocid set-current
  [:
    nrot create-named smudge
    locs-size , ( offset) , ( size) immediate create; smudge
    [ use-interpreter-hook not ] [IF]
      ['] (local-does) locs-vocid vocid-latest-cfa forth:(does>!)
    [ENDIF]
  ;] catch r> set-current locs-dp-temp swap-dp-temp
  throw
; (hidden)

: create-named-local  ( addr count cell-size -- )
  locs-vocid not-?abort" not in local definition mode"
  dup >r (create-local-word) r> +to locs-size
;


: find-local  ( addr count -- offset true // false )
  locs-vocid dup if voc-search endif
  if cfa->pfa @ true else 2drop false endif
;


: (is-alpha)  ( ch -- flag )
  case
    [char] A [char] Z bounds-of true endof
    [char] a [char] z bounds-of true endof
    [char] 0 [char] 9 bounds-of true endof
    127 >of true endof
    false swap
  endcase
;

: (is-digit)  ( ch -- flag)  10 digit? ;

: check-name  ( addr count predicatecfa -- flag )
  nrot bounds do i c@ over execute ifnot drop false unloop exit endif loop drop true
;

: is-alpha-name?  ( addr count -- flag )  ['] (is-alpha) check-name ;
: is-digit-name?  ( addr count -- flag )  ['] (is-digit) check-name ;

: is-good-local-name?  ( addr count -- flag )
  dup 1 64 within ifnot 2drop false exit endif
  2dup is-alpha-name? ifnot 2drop false exit endif
  2dup is-digit-name? if 2drop false exit endif
  2dup " to" s=ci if 2drop false exit endif
  2dup " +to" s=ci if 2drop false exit endif
  2dup " -to" s=ci if 2drop false exit endif
  2dup " ^to" s=ci if 2drop false exit endif
  " exit" s=ci not
;

: new-local  ( addr count size -- )
  nrot 2dup find-local ?abort" duplicate local name"
  2dup is-good-local-name? not-?abort" invalid local name"
  rot create-named-local
;


: end-args?  ( addr count -- addr count flag )  2dup 2dup " |" s= nrot " ||" s= or ;
: end-locals?  ( addr count -- addr count flag )  2dup " }}" s= ;
: end-locpart?  ( addr count -- addr count flag )  2dup " --" s= ;

: parse-locdef  ( -- argcells totalcells )
  ;; parse arguments
  begin parse-name dup not-?abort" \`}}\` expected" end-args? not-while end-locals? not-while end-locpart? not-while 1 new-local repeat
  ;; parse locals
  locs-size 1- nrot
  end-args? if 2drop
    begin parse-name dup not-?abort" \`}}\` expected" end-locals? not-while end-locpart? not-while 1 new-local repeat
  endif
  end-locpart? if
    begin 2drop parse-name dup not-?abort" \`}}\` expected" end-locals? until
  endif 2drop
  \ locs-size dup 1- if [compile] 2literal compile alloc-local-frame else 2drop endif
  locs-size
;


;; {{ arg0 arg1 || loc0 loc1 -- anything }}
: {{
  compiler:?comp
  compiler:?non-macro
  locs-vocid if clear-locals abort" nested locals are not supported" endif
  init-locals
  ['] parse-locdef catch dup if clear-locals endif throw
  dup 1- if
    [compile] 2literal compile alloc-local-frame
    compiler:cblock-start
    (CTLID-CBLOCK-LOCS)
    optimiser:jstack-subinit
    [ use-interpreter-hook not ] [IF]
      also locs-vocid forth:(set-context)
    [ENDIF]
  else 2drop endif
; immediate


: active?  ( -- flag )  state @ locs-vocid logand ;


..: value-xto-schook  ( value type addr count -- true // unchanged )
  active? if
    2dup find-local if
      dup ifnot clear-locals abort" invalid local name" endif
      compile-locaddr 2drop forth:(value-xto-type->cfa) ?compile, true exit
    endif
  endif
<;..


use-interpreter-hook [IF]
nested-vocabulary (locvoc-specials) also (locvoc-specials) definitions
;; all normal methods: ( -- addr count false // true )

: (other)  ( addr count -- addr count false // true )
  2dup find-local dup if
    dup ifnot clear-locals abort" invalid local name" endif
    drop nrot 2drop compile-locload true
  endif
; (hidden)

: exit  compile free-local-frame " EXIT" false ;  ;; continue with normal "EXIT", because it may be not from FORTH vocabulary
;; this must be defined last!
: ;  (;) " ;" false ;  ;; continue with normal ";", because it may be not from FORTH vocabulary
previous definitions

;; returns `1` if cfa is immediate, or `-1` if it is a normal word
;; scattered extensions should simply pass "addr count" unchanged (or changed ;-)
..: interpret-wfind  ( addr count -- cfa 1 // cfa -1 // addr count false )
  active? if
    vocid: (locvoc-specials) ['] (locvoc-specials):(other) (intepret-voc-call-helper) if exit endif
  endif
<;..
[ENDIF]


previous definitions
alias (locvoc):{{ {{
