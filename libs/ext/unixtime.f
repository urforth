;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary unix-time also unix-time definitions

create long-dows
" Monday   Tuesday  WednesdayThursday Friday   Saturday Sunday   "
dup n-allot swap move align-here create;


create long-monthes
" January  February March    April    May      June     July     August   SeptemberOctober  November December "
dup n-allot swap move align-here create;


: dow->long-name  ( dow -- addr count )  0 7 clamp 9 u* long-dows + 9 -trailing ;
: dow->short-name  ( dow -- addr count )  dow->long-name 3 min ;

: month->long-name  ( month -- addr count )  1 12 clamp 1- 9 u* long-monthes + 9 -trailing ;
: month->short-name  ( month -- addr count )  month->long-name 3 min ;

: ->time  ( utime -- hour minute second )
  60 u/mod 60 u/mod 24 umod nrot swap
;

;; month is [1..12], day is [1..n]
: ->date  ( utime -- year month day )
  [ 60 60 * 24 * ] literal u/                  ;; drop h:m:s                    ( t )
  dup 2 lshift 102032 + 146097 u/ 15 +         ;; a = (4*t+102032)/146097+15    ( t a )
  dup 2 rshift - 2442113 + +                   ;; b = t+2442113+a-(a/4)         ( b )
  dup 20 u* 2442 - 7305 u/ tuck                ;; c = (20*b-2442)/7305 -- year  ( c b c )
  dup 2 rshift swap 365 u* rot swap - swap -   ;; d = b-365*c-(c/4)             ( c d )
  dup 1000 30601 u*/ swap                      ;; e = d*1000/30601 -- month     ( c e d )
  over dup 30 u* swap 601 1000 u*/ rot swap - swap -  ;; f = d-e*30-e*601/1000  ( c e f ) -- day
  >r dup 13 <= if  ;; jan and feb are counted as months 13 and 14 of the previous year
    1- swap 4716 - swap
  else
    13 - swap 4715 - swap
  endif r>
;

: ->date/time  ( utime -- year month day hour minute second )
  dup >r ->date r> ->time
;


: time->  ( hour minute second -- utime )
  swap 60 u* + swap [ 60 60 * ] literal u* +
;


: date->  ( year month day  -- utime )
  over 2 <= if  ;; jan and feb are counted as months 13 and 14 of the previous year
    rot 1- rot 12 + rot
  endif
  noop swap dup 30 u* swap 1+ 3 swap 5 u*/ + + swap            ;; (30*m)+(3*(m+1)/5)+d
  noop dup 365 u* over 2 rshift + over 100 u/ - swap 400 u/ +  ;; (365*y)+(y/4)-(y/100)+(y/400)
  noop + 719561 -  ;; to Epoch
  [ 60 60 * 24 * ] literal u*
;

: date/time->  ( year month day hour minute second -- utime )
  time-> >r date-> r> +
;


;; compute day of week -- [0..6], 0 is monday
: date->dow  ( year month day -- dow )
  over 2 <= if  ;; jan and feb are counted as months 13 and 14 of the previous year
    rot 1- rot 12 + rot
  endif
  ;; Zeller's congruence
  swap 1+ 26 swap 10 u*/ +    ;; d+(26*(m+1)/10)...  ( y x )
  swap 100 u/mod              ;; ( x cy c )
  dup 5 u* swap 2 rshift +    ;; ( x cy c*5+c/4 )
  swap dup 2 rshift +         ;; ( x c*5+c/4 cy+cy/4 )
  + + 5 + 7 umod              ;; ( dow )
;


;; to numeric conversion buffer
: (.time)  ( hour minute second -- addr count )
  base @ >r decimal
  <#u # # [char] : hold
  2drop u>d # # [char] : hold
  2drop u>d # # #>
  r> base !
;

;; to numeric conversion buffer
: (.date)  ( year month day -- addr count )
  base @ >r decimal
  <#u # # [char] / hold
  2drop u>d # # [char] / hold
  2drop u>d # # # # #>
  r> base !
;


previous definitions
