;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm

also os definitions

code: getdents  ( dentaddr dentsize fd -- errcode // bytesread // 0 )
  ld    ebx,TOS
  pop   edx
  pop   ecx
  ld    eax,141
  syscall
  ld    TOS,eax
  urnext
endcode

2 cells 2+ 256 + constant #dent

 0 constant dent-ino
 4 constant dent-off
 8 constant dent-reclen  ;; 2 bytes
10 constant dent-namez


previous definitions


: open-dir  ( c-addr u -- dirfd wior )
  [ os:O-RDONLY os:O-DIRECTORY or ] literal 0 os:open dup -if err-file-not-found else 0 endif
;

: close-dir  ( dirfd -- wior )
  os:close if err-file-read-error else 0 endif
;

\ calls xt for each directory entry except "." and ".."
\ xt: ( nameaddr namecount inode -- stopflag )
\ name buffer is temporary, you should not store its address anywhere
: foreach-dir  ( dirfd xt -- eflag wior )
  over -if 2drop 0 err-file-read-error exit endif
  os:#dent ralloca nrot >r 2>r  ( xt buf-dirfd )
  begin
    2r@ os:#dent swap os:getdents
    dup -if drop 2rdrop rdrop 0 err-file-read-error os:#dent rdealloca exit endif
    dup ifnot 2rdrop rdrop 0 os:#dent rdealloca exit endif
    2r@ drop swap ( buf bleft | xt buf-dirfd )
    2r> r@ nrot 2>r >r  ( buf bleft | xt buf-dirfd xt )
    begin
      over dup os:dent-ino + @ swap os:dent-namez + zcount  ( buf bleft uno addr count | xt buf-dirfd xt )
      2dup 2dup " ." s= nrot " .." s= or if 2drop drop
      else  ( buf bleft uno addr count | xt buf-dirfd xt )
        \ rot r@ execute  ( buf bleft stopflag | xt buf-dirfd xt )
        rot r@ 2rot 2>r execute 2r> rot
        ?dup if nrot 2drop 2rdrop 2rdrop 0 os:#dent rdealloca exit endif
      endif
      over os:dent-reclen + w@ /string
    dup 1- -until 2drop rdrop
  again
;

\ inode is zero when we hit end-of-dir (and the name is empty, i.e. count is 0)
: (read-dir)  ( addr maxcount dirfd -- addr count inode wior )
  dup -if 2drop 0 0 err-file-read-error exit endif
  os:#dent ralloca >r
  os:dent-namez 1+ 0 begin  ( addr maxcount dirfd curlen dummy | buf )
    drop 2dup r@ swap rot os:getdents dup -22 ( EINVAL) =
  while
    swap 1+ swap over os:#dent >
  until nrot 2drop
  dup -if nip 0 swap err-file-read-error exit endif
  if 0 max over swap r@ os:dent-namez + zcount rot min dup >r rot swap move r>
     r@ os:dent-ino + @ 1 umax 0
  else drop 0 0 0 endif  ;; no more
  rdrop os:#dent rdealloca
; (hidden)

;; this ignores "." and ".."
\ inode is zero when we hit end-of-dir (and the name is empty, i.e. count is 0)
: read-dir  ( addr maxcount dirfd -- addr count inode wior )
  >r begin 2dup r@ (read-dir) dup not-while
  2over 2dup " ." s= nrot " .." s= or while
  2drop 2drop repeat
  rdrop 2rot 2drop
;
