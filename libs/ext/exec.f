;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm

get-current also os definitions

code: xexec  ( envza argsza exez -- pid true // err false )
  push  TOS
  ld    eax,11
  pop   ebx  ;; exe
  pop   ecx  ;; args
  pop   edx  ;; env
  syscall
  push  eax
  ld    TOS,0
  urnext
endcode

code: exec  ( envza argsza exez -- pid true // err false )
  push  TOS
  ;; first, vfork
  ld    eax,190
  syscall
  cp    eax,-4096
  jr    nc,.forkerror
  ;; 0: child
  ;; !0: parent
  or    eax,eax
  jr    z,.child
.parent:
  add   esp,4*2
  ld    [esp],eax
  ld    TOS,1
  urnext

.child:
  ;; close handles (except the first three)
  ld    ebx,3
.close_loop:
  push  ebx
  ld    eax,6
  syscall
  pop   ebx
  inc   ebx
  cp    ebp,0x10000
  jr    c,.close_loop

  ld    eax,11
  pop   ebx  ;; exe
  pop   ecx  ;; args
  pop   edx  ;; env
  syscall
  ;; returns on some error
  mov   eax,1
  mov   ebx,123
  syscall

.forkerror:
  add   esp,4*2
  ld    [esp],eax
  xor   TOS,TOS
  urnext
endcode


1 constant WNOHANG
2 constant WUNTRACED
4 constant WEXITED
8 constant WCONTINUED

2 constant WSTOPPED

0x1000000 constant WNOWAIT

: WEXITSTATUS  ( s -- status )  0xff00 and 8 rshift ;

: WTERMSIG  ( s -- sig )  0x7f and ;
alias WEXITSTATUS WSTOPSIG

: WCOREDUMP  ( s -- flag )  0x80 and notnot ;

: WIFEXITED  ( s -- flag )  WTERMSIG not ;
: WIFSTOPPED  ( s -- flag )  0xffff and 0x10001 u* 8 rshift 0x7f00 > ;
: WIFSIGNALED  ( s -- flag )  0xffff and 1- 0xff u< ;
: WIFCONTINUED  ( s -- flag )  0xffff = ;

code: wait  ( pid flags -- waitcode pid true // err false )
  ld    edx,TOS    ;; flags
  ld    eax,7
  ld    ebx,[esp]  ;; pid
  ld    ecx,esp    ;; buffer (will replace pid)
  syscall
  cp    eax,-4096
  jr    nc,.error
  ;; eax is PID, [esp] is waitcode
  push  eax
  ld    TOS,1
  urnext
.error:
  ld    [esp],eax
  xor   TOS,TOS
  urnext
endcode


: strarray-free  ( memaddr -- )
  ?dup if count os:munmap drop endif
;

;; first cell: memory size in bytes
;; second cell: string count
: strarray-init  ( -- memaddr true // error false )
  4096 os:prot-r/w os:mmap ifnot false exit endif
  4096 over !
  2 over cell+ !  true
;

: strarray-push  ( memaddr addr count -- memaddr true // memaddr error false )
  dup 0 32767 bounds? ifnot 2drop -22 false exit endif  ;; -EINVAL
  rot dup cell+ @ 1021 >= if nrot 2drop -12 false exit endif  ;; -ENOMEM
  ;; realloc
  >r dup 1+ r@ @ + r@ swap r@ @ swap os:mremap ifnot nrot 2drop r> r> swap false exit endif rdrop >r
  ;; copy string
  2dup r@ dup @ + swap move r@ @ over + r@ + 0c!
  ;; save pointer and update counter
  r@ @  r@ dup cell+ @ +cells !  r@ cell+ 1+!
  ;; update size
  1+ r@ +!
  drop r> true
;

: strarray-finalized?  ( memaddr -- flag ) cell+ @ 0xffff > ;

: strarray-first  ( memaddr -- addrz )
  dup strarray-finalized? if 2 +cells @
  else dup 2 +cells @ + endif
;

: strarray-envp  ( -- memaddr true // error false )
  os:strarray-init ifnot false exit endif
  forth:envp @ swap begin over c@ while
    over zcount
    2dup [char] = str-char-index if drop
      strarray-push ifnot swap strarray-free nip false exit endif
    else 2drop endif
    swap zcount + 1+ swap
  repeat nip true
;

: strarray-finalize  ( memaddr -- memaddr )
  dup strarray-finalized? ifnot
    dup cell+ @ cells over 2 +cells swap bounds ?do dup i +! cell +loop
    dup cell+ @ cells over + 0!
    0x4000_0000 over cell+ or!
  endif
;

: strarray->array  ( memaddr -- arraddr )  2 +cells ;

: strarray-dump  ( memaddr -- )
  endcr ." ============\n"
  ." SIZE: " dup @ . ." bytes at 0x" dup .hex8
  dup strarray-finalized? if ."  (finalized)" endif cr
  ." COUNTER: " dup cell+ @ 0xffff and 2- . cr
  dup strarray-finalized? if
    dup cell+ @ 0xffff and 2- cells swap 2 +cells swap bounds ?do 2 spaces i @ .hex8 ."  <" i @ zcount type ." >\n" cell +loop
  else
    dup cell+ @ 2- for dup i 2+ +cells @ over + 2 spaces i . ." <" zcount dup nrot type ." > : " . cr endfor
    drop
  endif
;


previous set-current
