;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; created word returns structure size
: BEGIN-STRUCTURE  ( "<spaces>name" -- struct-sys 0 )
  create
  here 0 0 ,  ;;  mark stack, lay dummy
 does>
  @    ;; -- rec-len
;
alias BEGIN-STRUCTURE BEGIN-STRUCT

: END-STRUCTURE  ( struct-sys +n -- )
  swap !  ;; set len
;
alias END-STRUCTURE END-STRUCT

;; field usage: addr fldname --> addr+fldoffset
: +FIELD  ( n1 n2 "<spaces>name" -- n3 )
  create
  over , +
 does>
  @ +
;

;; non-standard: aligns current offset to dword
: ALIGN-FIELD  ( -- ) 0x03 + 0x03 ~and ;

;; field usage: addr fldname --> addr+fldoffset
;; creates field of one-cell size (and aligns it at a cell boundary)
: FIELD:  ( n1 "<spaces>name" -- n2 )
  create
  align-field
  dup , cell+
 does>
  @ +
;

: CFIELD:  1 +field ;
: BFIELD:  1 +field ;
: WFIELD:  2 +field ;
: LFIELD:  4 +field ;
: XFIELD:  8 +field ;
: SFFIELD:  4 +field ;
: DFFIELD:  8 +field ;
