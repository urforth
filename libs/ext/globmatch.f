;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU basic words; based on SP-Forth code by Dmitry Yakimov <ftech@tula.net>
;; slightly rewritten, adapted for UrForth, bugfixed by Ketmar Dark
;; WARNING! this is using FPU stack, which is circular, and contains 8 items
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; it can be made much faster by scanning a source string with low-level "str-char-index" and such, but meh...
vocabulary glob also glob definitions

: (set-skip-char)  ( addr count -- naddr ncount char // addr 0 0 )
  dup if over c@ nrot /char rot dup [char] \ = if >r /char r> endif
  else dup endif
; (hidden)

;; skips set (and the closing bracket)
;; opening bracket is NOT eaten
: (skip-set)  ( addr count -- addr count )
  /char dup ifnot exit endif
  over c@ [char] ^ = if /char endif
  (set-skip-char) drop  ;; first char is always a pattern
  begin dup while
    over c@ [char] - = if
      /char (set-skip-char) drop 0
    else (set-skip-char) endif
  [char] ] = until
; (hidden)


: (set-get-char-simple)  ( addr count -- naddr ncount char )  dup if over c@ nrot /char rot else -1 endif ;

: (set-get-char)  ( addr count -- naddr ncount char )
  (set-get-char-simple) dup [char] \ = if drop (set-get-char-simple) endif
;

;; opening bracket is NOT eaten
: (set=)  ( char-cased pataddr patcount casecfa -- equflag )  >r /char
  true nrot dup if over c@ [char] ^ = if /char rot drop false nrot endif endif
  begin dup while  ( char-cased positive-set-flag pataddr patcount | casecfa )
    (set-get-char) dup -if drop break endif
    >r dup if over c@ [char] - = else false endif
    ( char-cased positive-set-flag pataddr patcount range-flag | casecfa setchar )
    if  ;; range (sorry for this fuckin' mess)
      /char dup ifnot rdrop break endif
      (set-get-char) dup -if drop rdrop break endif 1+ r> over min
      ;; this stack acrobatics is shitty, but it works
      ;; inside the loop, we have this: ( char-cased positive-set-flag pataddr patcount casecfa )
      ;; of course, we may rearrange the stack a little, but "2over" is as fast as "2dup" anyway
      r> nrot ?do i swap >r >r 2over swap r> r@ execute = =
        if rdrop 2drop nip unloop exit endif
      r> loop >r false
    else 2over swap r> r@ execute = =
    endif
    if rdrop 2drop nip exit endif
  dup while over c@ [char] ] = until
  rdrop 2drop 2drop false
;

: (char=)  ( straddr strcount pataddr patcount casecfa -- straddr strcount pataddr patcount equflag )
  >r 2over 2over drop c@ r@ execute nrot drop c@ r> execute =
; (hidden)

;; stops at string difference, or when one of the strings ends
;; advances both a string and a pattern, skips the stars
: (substr)  ( straddr strcount pataddr patcount casecfa -- straddr strcount pataddr patcount atstar )
  >r begin 2over nip over logand while
    over c@ case
      [char] \ of
        dup 1- if /char r@ (char=) ifnot -1 /string break endif true
        else r@ (char=) endif
      endof
      [char] ? of true endof
      [char] * of
        begin /char dup while over c@ [char] * <> until
        true rdrop exit
      endof
      [char] [ of
        2over drop c@ r@ execute >r 2dup r> nrot r@ (set=) ifnot break endif
        (skip-set) -1 /string true
      endof
      otherwise drop r@ (char=)
    endcase
  while /char 2swap /char 2swap repeat
  rdrop false
; (hidden)


: match-ex  ( addr count pataddr patcount casecfa -- equflag )
  >r 0 max 2swap 0 max 2swap
    \ ." start: str=<" 2over type ." > pat=<" 2dup type ." >\n"
  ;; first check, until a star
  r@ (substr) ifnot rdrop rot or 0= nrot 2drop exit endif  ;; not at a star, should be the full match
  ;; ok, matched up to a star
  ?dup ifnot rdrop drop 2drop true exit endif  ;; pattern ends with a star, it matches rest of the string
  begin 2over nip while  ;; while there is some chars in the input string
      \ ." 000: str=<" 2over type ." > pat=<" 2dup type ." >\n"
    2over 2over r@ (substr)  ( oldstraddr oldstrcount  oldpataddr oldpatcount  straddr strcount  pataddr patcount  atstar )
      \ >r ." 001-res: str=<" 2over type ." > pat=<" 2dup type ." >; atstar=" r@ . cr r>
    if  ;; at a star
      ?dup ifnot rdrop drop 2drop 2drop 2drop true exit endif  ;; pattern ends with a star, it matches the rest of the string
      2>r 2>r 2drop 2drop 2r> 2r>
    else  ;; not at a star
      rot or 0= if rdrop 2drop 2drop 2drop true exit endif  ;; no more string, no more pattern
      2drop 2swap /char 2swap
    endif
  repeat rdrop
  >r drop 2drop r> 0=
;

: match  ( addr count pataddr patcount -- equflag )  ['] noop match-ex ;
: match-ci  ( addr count pataddr patcount -- equflag )  ['] locase-char match-ex ;

previous definitions
