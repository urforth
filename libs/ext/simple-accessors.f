;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(*
" jsv" struct-accessors:begin
  field: type
  field: value
  field: sibling
  field: addr
  field: length
  field: flags
end-@sizeof
*)

vocabulary-nohash struct-accessors
also struct-accessors definitions

vocabulary-nohash (field-accessors) (hidden)
also (field-accessors) definitions

128 cell+ buffer: pfx-c4s

: address-code,  ( accesscfa byteofs -- )
  ?dup if compiler:literal, compile + endif
  ?compile,
;

: accessor,  ( accesscfa byteofs nchar -- )
  pfx-c4s c4s:cat-char pfx-c4s count
  create-header-named compiler:forth-word-prologue
  2dup or ifnot immediate endif  ;; zero offset and zero accessor is noop
  address-code, compile exit
  compiler:forth-word-epilogue smudge
  -1 pfx-c4s +!  ;; restore name length
;

: all-accessors  ( byteofs size -- byteofs size )  \ basename
  pfx-c4s count-only >r swap >r >r parse-name pfx-c4s c4s:cat-counted
  r> 0 r@ [char] ^ accessor,  ;; create "name^"
  dup case
    1 of
      ['] c@ r@ [char] @ accessor,  ;; create "name@"
      ['] c! r@ [char] ! accessor,  ;; create "name!"
    endof
    2 of
      ['] w@ r@ [char] @ accessor,  ;; create "name@"
      ['] w! r@ [char] ! accessor,  ;; create "name!"
    endof
    4 of
      ['] @ r@ [char] @ accessor,  ;; create "name@"
      ['] ! r@ [char] ! accessor,  ;; create "name!"
    endof
  endcase
  r> swap  r> pfx-c4s !  ;; restore length
;

: begin  ( pfxaddr pfxcount -- 0 )
  dup 0 128 within not-?abort" invalid accessor base name"
  pfx-c4s c4s:copy-counted [char] - pfx-c4s c4s:cat-char
  0  ;; current bytesize
;

: create-@sizeof  ( bytesize -- bytesize )
  pfx-c4s count-only >r " @sizeof" pfx-c4s c4s:cat-counted
  dup pfx-c4s count forth:(constant-named)
  r> pfx-c4s !  ;; restore length
;

: n-field:  ( byteofs size -- byteofs+size )  \ basename
  all-accessors +
;

previous definitions

: begin  ( pfxaddr pfxcount -- 0 ) (field-accessors):begin also struct-accessors ;
: end  ( byteofs -- )  drop previous ;
: end-@sizeof  ( byteofs -- )  (field-accessors):create-@sizeof end ;

: n-field:  ( byteofs -- nextbyteofs )  \ basename
  (field-accessors):n-field:
;

: field:  ( byteofs -- nextbyteofs )  \ basename
  cell (field-accessors):n-field:
;

previous definitions
