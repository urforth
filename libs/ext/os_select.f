;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm
get-current also os definitions


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FSSET is 32 dwords; bits are counted from the lowest to the highest

;; in bytes
128 constant #FDSET

code: FDSET-CLEAR  ( fdsaddr -- )
  ld    edi,TOS
  ld    ecx,32
  xor   eax,eax
  rep stosd
  pop   TOS
  urnext
endcode

code: FDSET-1!  ( idx fdsaddr -- )
  pop   eax
  cp    eax,1024
  jr    nc,.done
  ld    edi,eax
  shr   edi,5
  and   eax,31
  bts   dword [TOS+edi*4],eax
.done:
  pop   TOS
  urnext
endcode

code: FDSET-0!  ( idx fdsaddr -- )
  pop   eax
  cp    eax,1024
  jr    nc,.done
  ld    edi,eax
  shr   edi,5
  and   eax,31
  btr   dword [TOS+edi*4],eax
.done:
  pop   TOS
  urnext
endcode

code: FDSET-@  ( idx fdsaddr -- flag )
  xor   edx,edx
  pop   eax
  cp    eax,1024
  jr    nc,.done
  ld    edi,eax
  shr   edi,5
  and   eax,31
  bt    dword [TOS+edi*4],eax
  setc  dl
.done:
  movzx TOS,dl
  urnext
endcode


;; in bytes
8 constant #TIMEVAL

code: TIMEVAL-MS!  ( msecs timevalptr -- )
  ;; multiply msecs by 1000 to get nsecs, and then div by 1000000
  pop   eax
  cp    eax,0
  jr    ge,@f
  xor   eax,eax
@@:
  ld    ebx,1000
  mul   ebx
  ld    ebx,1000000
  div   ebx
  ld    [TOS],eax    ;; seconds
  ld    [TOS+4],edx  ;; microseconds
  pop   TOS
  urnext
endcode

code: TIMEVAL-MS@  ( timevalptr -- msecs )
  ;; multiply secs by 1000000, add nsecs, and then div by 1000
  ld    eax,[TOS]    ;; seconds
  ld    ebx,1000000
  mul   ebx
  add   eax,[TOS+4]  ;; microseconds
  adc   edx,0
  ld    ebx,1000
  div   ebx
  ld    TOS,eax
  urnext
endcode


code: SELECT  ( maxfd+1 rdset wrset excset timevalptr -- res )
  ;; EAX: scidx
  ;; EBX: maxfd
  ;; ECX: rdset
  ;; EDX: wrset
  ;; ESI: extset
  ;; EDI: tvptr
  ld    edi,TOS
  pop   eax  ;; will be swapped later
  pop   edx
  pop   ecx
  pop   ebx
  push  EIP
  push  ERP
  ld    esi,eax
  ld    eax,82
  syscall
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
endcode


previous set-current
