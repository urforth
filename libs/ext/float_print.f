;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print floating number
;; WARNING! cannot be used for reliable round-trip!
;; this is not the best way of doing it
;; i have to port Ruy instead, but for now let's have at least something
;;

17 uservar (PRECISION)

: PRECISION  ( -- u )  (precision) @ ;
: SET-PRECISION  ( u -- )  1 max 17 min (precision) ! ;

;; digits in the integral parts
: (FPU-#EXP)  ( -- n ) ( r -- r )
   fdup f0= if precision
   else fdup fabs flog floor f>d drop
   endif
; (hidden)

: (0.1E)
  1.e 10.e f/
; (hidden)


16 constant (FPU-MPREC) (hidden)  ;; maximum precision
0 uservar (FPU-EXP) cell userallot (hidden)  ;; exponent and sign
\ 2 cells buffer: (FPU-EXP) (hidden)  ;; exponent and sign

;; from http://www.alphalink.com.au/~edsa/represent.html (at least this is what SP-Forth says ;-)
;; if exponent is negative or zero, it is number zeroes right after the decimal point (i.e. for "0.001" it will be -2)
;; if exponent is positive, it is number of digits (i.e. for "1000" it will be 4)
;; for zero, exponent is 1 (becase it indicates number of digits at the left side, and zero is ".0" here)
: REPRESENT  ( c-addr u -- exponent negative-flag nonzero-flag ) ( F: r -- )
  2dup [char] 0 fill
  (fpu-mprec) min 2>r
  fdup f0< 0 (fpu-exp) 2!
  fabs fdup f0= 0=
  begin
  while
   fdup 1.e f< 0= if
     10.e f/
     1
   else
     fdup (0.1e) f< if
       10.e f*
       -1
     else
       0
     then
   then
   dup (fpu-exp) +!
  repeat
  1.e r@ 0 ?do 10.e f* loop f*
  fround f>d
  2dup <# #s #> dup r@ - (fpu-exp) +!
  2r> rot min 1 max cmove
  d0= (fpu-exp) 2@ swap rot if 2drop 1 false endif  ;; 0.0e fix-up
  true
; (hidden)


;; trim trailing zeroes
: (FPU-TRIM-0)  ( c-addr u1 -- c-addr u2 )
  ;;fstrict if exit endif
  begin
    dup
  while
    1- 2dup ( chars ) +
    c@ [char] 0 -
  until
  1+
;

;; this is to avoid ans/non-ans checks
\ cell buffer: (FHLD) (hidden)
0 uservar (FHLD) (hidden)

: (<f#)  ( -- )
  pad (fhld) !
; (hidden)

: (f#>)  ( addr count -- )
  pad (fhld) @ over -
; (hidden)

;; this is not backwards
: (f#put)  ( ch -- )
  (fhld) @ c!
  (fhld) 1+!
; (hidden)

: (f#put-str)  ( addr count -- )
  dup 0> if
    over + swap do i c@ (f#put) loop
  else
    2drop
  endif
; (hidden)


;; "e-nontation"
;; use "scientific", with one dot
;; the original number should be non-zero
;; pad+128 should contain the result of "represent"
: (f.e)  ( exp -- addr count )
  pad 128 + c@ (f#put)
  [char] . (f#put)
  pad 129 + precision 1- 0 max (fpu-trim-0)
  (f#put-str)
  [char] E (f#put)
  1-
  dup 0>= if [char] + else negate [char] - endif
  (f#put)
  0 <# #s #> (f#put-str)
;

;; decimal, with possible dot
;; the original number should be non-zero
;; pad+128 should contain the result of "represent"
: (f.f)  ( exp -- addr count )
  dup 0> if
    ;; integral part
    dup >r
    precision min pad 128 + swap (f#put-str)
    ;; floating part
    r@ precision < if
      pad 128 + r@ + precision r> - (fpu-trim-0)
      dup 1 = if over c@ [char] 0 = if 1- endif endif
      ?dup if [char] . (f#put) (f#put-str) else drop endif
    else
      rdrop
    endif
  else
    negate  ;; now it is number of zeroes after the decimal point
    [char] 0 (f#put)
    [char] . (f#put)
    0 ?do [char] 0 (f#put) loop
    pad 128 + precision (fpu-trim-0) (f#put-str)
  endif
;

: (f.)  ( f:n -- addr count )
  base @ >r decimal
  (<f#)
  f-finite? if
    pad 128 + precision represent
    ifnot
      ;; zero
      [char] 0 (f#put)
      2drop
    else
      ;; non-zero
      if [char] - (f#put) endif
      ;; if exponent is lower or greater than precision, use "e-notation"
      ;; FIXME: this is invalid for negative exponents (see above)
      ;;        for negative exponents, we should count real number of digits
      ;; calc lower exponent bound
      dup 0<= if
        precision
        pad 128 + over (fpu-trim-0) nip 2+ - 0 max
        negate
      else
        0
      endif
      over swap precision within if
        (f.f)
      else
        (f.e)
      endif
    endif
  else
    f-nan? if " nan"
    else f-inf? ?dup if 0< if [char] - else [char] + endif (f#put) " inf"
    else " wtf"
    endif endif
    fdrop
    (f#put-str)
  endif
  (f#>)
  r> base !
;

: F.  ( f:n -- )
  (f.) type space
;
