;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ratios for "*/"
;; PI: 235619449 75000000 -- 3.14159265
;;  E: 11892483 4375000 -- 2.71828182

(*
: D* ( alo ahi blo bhi -- aXblo aXbhi )
  >r swap >r 2dup um* 2swap  r> * swap r> * + +
;
*)

;; tri negate
: TNEGATE  ( t1lo t1mid t1hi -- t2lo t2mid t2hi )
  bitnot >r
  bitnot >r
  bitnot 0 0xffffffff 0xffffffff d+ s>d r> 0 d+
  r> +
;

: T+  ( t1lo t1mid t1hi t2lo t2mid t2hi -- t3lo t3mid t3hi )
  >r rot >r >r swap >r  0 tuck d+  0 r> r>  0 tuck d+  d+ r> r> + +
;

: T- ( t1lo t1mid t1hi t2lo t2mid t2hi -- t3lo t3mid t3hi )
  >r rot >r >r swap >r  0 tuck d-  s>d r> r> 0 tuck d-  d+ r> r> - +
;

;; unsigned double by an unsigned integer to give a tri result
;; some forthes calls this TUM*
: UT*  ( ulo uhi u -- utlo utmid uthi )
  swap >r dup >r
  um* 0 r> r> um* d+
  ;; alternative version
  ;; tuck um* 2swap um* swap >r 0 d+ r> nrot
;
alias UT* TUM*

;; divide a tri number by an integer
;; some forthes calls this TUM/
: UT/  ( utlo utmid uthi n -- d1 )
  dup >r um/mod nrot r> um/mod nip swap
;
alias UT/ TUM/

;; double by a integer to give a tri result
: MT*  ( lo hi n -- tlo tmid thi )
  dup 0< if
    abs over 0< if
      >r dabs r> ut*
    else
      ut* tnegate
    endif
  else
    over 0< if
      >r dabs r> ut* tnegate
    else
      ut*
    endif
  endif
;

;; multiply d1 by n1 producing the triple-cell intermediate result t
;; divide t by +n2 giving the double-cell quotient d2
;; an ambiguous condition exists if +n2 is zero or negative,
;; or the quotient lies outside of the range of a double-precision
;; signed integer
: M*/  ( d1 n1 +n2 -- d2 )
  >r mt* dup 0< if
    tnegate r> ut/ dnegate
  else
    r> ut/
  endif
;


;; DU/MOD -- Double Unsigned Division with Remainder
;; given an unsigned 2-cell dividend and an unsigned 2-cell divisor,
;; return a 2-cell remainder and a 2-cell quotient.
;; the algorithm is based on Knuth's algorithm in volume 2 of his "Art of
;; Computer Programming", simplified for two-cell dividend and two-cell divisor.
: DU/MOD  ( Ddivd Ddivr -- Drem Dquot )
  ?DUP ifnot
    ;; there is a leading zero "digit" in divisor
    >r  0 r@ um/mod  r> swap >r  um/mod  0 swap r> exit
  endif
  0 >r begin dup 0< not-while D2* r> 1+ >r repeat r>  ;; normalize divisor
  dup >r rot rot 2>r
  1 swap lshift ut*
  ;; guess leading "digit" of quotient
  dup  r@ = if -1 else 2dup r@ um/mod nip endif
  ;; multiply divisor by trial quot and subtract from divd
  2r@ rot dup >r ut* t- dup 0< if
    ;; if negative, decrement quot and add to dividend
    r> 1- 2r@ rot >r 0 t+ dup 0< if
      ;; if still negative, do it one more time
      r> 1- 2r@ rot >r 0 t+
    endif
  endif
  ;; undo normalization of dividend to get remainder
  r> 2r> 2drop 1 r> rot >r lshift ut/ r> 0
;
