;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; parse floating number
;; WARNING! cannot be used for reliable round-trip!
;; this is far from the best way of doing it
;; (actually, this is one of the worst ways)
;; but until i port a better parser, this one can be used for rough fp input
;;

;; parse integral part of the floating number
;; uses 2 FP registers
: (parse-float-int)  ( addr u -- addr1 u1 )  ( -- f:value )
  dup 0<= if 0.e exit endif
  FPU-GETCW >r FPU-UP-MODE
  0.e
  begin
    dup
  while
    over c@ 10 digit
  while
    f10* s>f f+
    /char
  repeat
  r> FPU-SETCW
;

;; parse integral part of the floating number
;; uses 2 FP registers
;; dot should be already eaten
: (parse-float-frac)  ( addr u -- addr1 u1 )  ( -- f:value )
  dup 0<= if 0.e exit endif
  dup >r (parse-float-int) r>
  ;; ( addr1 u1 u )
  FPU-GETCW >r FPU-UP-MODE
  over - 0 ?do f10/ loop
  r> FPU-SETCW
;

;; 'e' should be already eaten
;; uses 2 FP registers
: (parse-float-exp)  ( addr u -- addr1 u1 )  ( f:value -- f:exped-value )
  dup 0<= if exit endif
  true >r
  ;; parse exponent sign
  over c@ case
    [char] + of /char endof
    [char] - of rdrop false >r /char endof
  endcase
  ;; parse exponent value
  ;; ( addr u | positive-flag )
  base @ >r decimal
  ;; ( addr u | positive-flag oldbase )
  0 number-parse-simple
  r> base !
  r>
  ;; ( addr u expval positive-flag )
  over 0x800 u>= if
    ;; infinity
    fdrop
    if f+inf else 0.e endif
  else
    ;; ( addr u expval positive-flag )
    if
      ;; positive exponent
      0 ?do f10* loop
    else
      ;; negative exponent
      0 ?do f10/ loop
    endif
  endif
;


;; uses 4 FP registers
: (parse-float)  ( addr u -- addr1 u1 )  ( -- f:value )
  dup 0<= if 0.e exit endif
  dup 3 = if
    2dup " nan" s=ci if 3 /string fnan exit endif
    2dup " inf" s=ci if 3 /string f+inf exit endif
  endif
  ;; negate flag
  false >r
  over c@ case
    [char] + of /char endof
    [char] - of rdrop true >r /char endof
  endcase
  2dup " inf" s=ci if 3 /string r> if f-inf else f+inf endif exit endif
  ;; parse integer part
  (parse-float-int)
  ;; has dot?
  dup if
    over c@ [char] . = if
      /char
      (parse-float-frac) f+
    endif
  endif
  ;; has exponent?
  dup if
    over c@ upcase-char [char] E = if
      /char
      (parse-float-exp)
    endif
  endif
  ;; negate if necessary
  r> if fnegate endif
;


;; this stores float as string, hence the double colon
;; the string will be parsed at runtime
;; WARNING! no error checking!
: F"  ( -- )  ( -- f:value )  ;; "
  state @ if
    compile s"  ;; "
    compile (parse-float)
    compile 2drop
  else
    34 parse (parse-float) 2drop
  endif
; immediate


;; this stores float as 8 bytes
;; remember that float parser sux!
: F#  ( -- )  ( -- f:value )  ;; "
  parse-name 2dup (parse-float) nip if
    ;; error parsing
    type [char] ? emit ERR-NUMBER-EXPECTED ?error
  else
    2drop
  endif
  state @ if
    f>f64 swap [compile] literal [compile] literal
    compile f64>f
  endif
; immediate
