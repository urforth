;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[IFNDEF] forth:URFORTH-EXT
only forth definitions

require TNEGATE         math_extprec.f
require BEGIN-STRUCTURE struct.f
require unix-time       unixtime.f
require glob            globmatch.f

" float.f" tload

true constant URFORTH-EXT

[ENDIF]
