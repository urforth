;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU basic words; based on SP-Forth code by Dmitry Yakimov <ftech@tula.net>
;; slightly rewritten, adapted for UrForth, bugfixed by Ketmar Dark
;; WARNING! this is using FPU stack, which is circular, and contains 8 items
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU control
;;

;; reinit FPU
;; control word set to 0x37f (round to nearest, all exceptions masked, 64-bit precision)
;; all registers marked as empty
code: FINIT
  fninit
  urnext
endcode

' FINIT to fpu-reset

code: FNOP
  fnop
  urnext
endcode

code: FPU-GETSW  ( -- sw )
  push  TOS
  fstsw ax
  movzx TOS,ax
  urnext
endcode

code: FPU-SETCW  ( u -- )
  push  TOS
  fldcw word [esp]
  pop   TOS
  pop   TOS
  urnext
endcode

code: FPU-GETCW  ( -- u )
  push  TOS
  push  TOS
  fstcw word [esp]
  pop   TOS
  movzx TOS,cx
  urnext
endcode

code: FPU-TRUNC-MODE  ( -- )
  fstcw word [esp-4]
  bts   word [esp-4],10
  bts   word [esp-4],11
  fldcw word [esp-4]
  urnext
endcode

code: FPU-ROUND-MODE  ( -- )
  fstcw word [esp-4]
  btr   word [esp-4],10
  btr   word [esp-4],11
  fldcw word [esp-4]
  urnext
endcode

code: FPU-UP-MODE  ( -- )
  fstcw word [esp-4]
  btr   word [esp-4],10
  bts   word [esp-4],11
  fldcw word [esp-4]
  urnext
endcode

code: FPU-LOW-MODE  ( -- )
  fstcw word [esp-4]
  bts   word [esp-4],10
  btr   word [esp-4],11
  fldcw word [esp-4]
  urnext
endcode

;; low 6 bits masks #I #D #Z #O #U #P
;; unmask everything except #P (due to zero cond)
: FPU-ERROR-MODE  ( -- )
   FPU-GETCW
   127 ~and
   32 or
   FPU-SETCW
;

;; only #I and stack
: FPU-NORMAL-MODE  ( -- )
  FPU-GETCW
  127 or
  1 ~and
  FPU-SETCW
;

: FPU-SILENT-MODE  ( -- )
  FPU-GETCW
  127 or
  FPU-SETCW
;

(* my taget compiler cannot do that yet
: FPU-mask CREATE , DOES> @ FPU-GETCW AND 0<> ;
: FPU-state CREATE , DOES> @ FPU-GETSW AND 0<> ;

 1 FPU-state ?FPU-IE   1 FPU-mask ?FPU-IM
 2 FPU-state ?FPU-DE   2 FPU-mask ?FPU-DM
 4 FPU-state ?FPU-ZE   4 FPU-mask ?FPU-ZM
 8 FPU-state ?FPU-OE   8 FPU-mask ?FPU-OM
16 FPU-state ?FPU-UE  16 FPU-mask ?FPU-UM
32 FPU-state ?FPU-PE  32 FPU-mask ?FPU-PM
*)
: (?FPU-STATE)  ( mask -- flag ) FPU-GETSW and 0<> ;
: (?FPU-MASK)   ( mask -- flag ) FPU-GETCW and 0<> ;

: ?FPU-IE  ( -- flag )  0x01 (?fpu-state) ;
: ?FPU-DE  ( -- flag )  0x02 (?fpu-state) ;
: ?FPU-ZE  ( -- flag )  0x04 (?fpu-state) ;
: ?FPU-OE  ( -- flag )  0x08 (?fpu-state) ;
: ?FPU-UE  ( -- flag )  0x10 (?fpu-state) ;
: ?FPU-PE  ( -- flag )  0x20 (?fpu-state) ;

;; exception masks (set means "disabled")
: ?FPU-IM  ( -- flag )  0x01 (?fpu-mask) ;  \ invalid opearion
: ?FPU-DM  ( -- flag )  0x02 (?fpu-mask) ;  \ denormalized operand
: ?FPU-ZM  ( -- flag )  0x04 (?fpu-mask) ;  \ zero divide
: ?FPU-OM  ( -- flag )  0x08 (?fpu-mask) ;  \ overflow
: ?FPU-UM  ( -- flag )  0x10 (?fpu-mask) ;  \ underflow
: ?FPU-PM  ( -- flag )  0x20 (?fpu-mask) ;  \ precision

: ?FPU-PREC  ( -- n ) FPU-GETCW 8 rshift 3 and ;
: ?FPU-ROUND  ( -- n ) FPU-GETCW 10 rshift 3 and ;

: FPU-PREC!  ( n -- )  3 and 8 lshift FPU-GETCW 0xfcff and or FPU-SETCW ;
: FPU-ROUND!  ( n -- )  3 and 10 lshift FPU-GETCW 0xf3ff and or FPU-SETCW ;

\ bits 8-9: precision control:
\   00: 24 bits (single precision, float)
\   01: who knows (reserved)
\   10: 53 bits (double precision)
\   11: 64 bits (extended precision)

\ bits 10-11: rounding control:
\   00: nearest or even
\   01: down (toward -inf)
\   10: up (toward +inf)
\   11: trunc towards zero

\ bit 12: infinity control (does nothing on 80387 and above)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU stack operations
;;
code: FDROP  ( -- )
  ;;ffree st
  ;;fincstp
  fstp st0
  urnext
endcode

code: FDUP  ( -- )
  fld st0
  urnext
endcode

code: FOVER  ( -- )
  fld st1
  urnext
endcode

code: FSWAP  ( -- )
  fxch  st1
  urnext
endcode

code: FROT  ( -- )
  fxch  st2
  fxch  st1
  fxch  st2
  fxch  st1
  urnext
endcode

;; you are not supposed to understand this ;-)
code: FDEPTH  ( -- n )
  push  TOS
  fstsw ax
  shr   eax,11
  and   eax,7
  jr    z,@f
  neg   eax
  lea   eax,[eax+8]
@@:
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU <-> data stack
;;
code: (F>D)  ( -- dl dh )
  push  TOS
  push  TOS
  push  TOS
  fistp qword [esp]
  pop   TOS
  xchg  TOS,[esp]
  urnext
endcode

: F>D  ( -- dl dh )
  FPU-GETCW >R
  FPU-TRUNC-MODE
  (F>D)
  R> FPU-SETCW
;

code: D>F  ( dl dh -- )
  xchg  TOS,[esp]
  push  TOS
  fild  qword [esp]
  pop   TOS
  pop   TOS
  pop   TOS
  urnext
endcode


\ push TOS to the float stack
code: S>F  ( n -- )
  push  TOS
  fild  dword [esp]
  pop   TOS
  pop   TOS
  urnext
endcode

\ pop the top float stack number to the data stack
\ round or truncate according to the current mode
code: F>S  ( -- n )
  push  TOS
  push  TOS
  fistp dword [esp]
  pop   TOS
  urnext
endcode

code: F@S  ( -- n )
  push  TOS
  push  TOS
  fist  dword [esp]
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; binary float representation <-> data stack
;;
code: F32>F  ( n -- )
  push  TOS
  fld   dword [esp]
  add   esp,4
  pop   TOS
  urnext
endcode

code: F>F32  ( -- n )
  push  TOS
  push  TOS
  fstp  dword [esp]
  pop   TOS
  urnext
endcode

code: F@F32  ( -- n )
  push  TOS
  push  TOS
  fst   dword [esp]
  pop   TOS
  urnext
endcode


code: F64>F  ( n0 n1 -- )
  push  TOS
  fld   qword [esp]
  add   esp,8
  pop   TOS
  urnext
endcode

code: F>F64  ( -- n0 n1 )
  push  TOS
  push  TOS
  push  TOS
  fstp  qword [esp]
  pop   TOS
  urnext
endcode

code: F@F64  ( -- n0 n1 )
  push  TOS
  push  TOS
  push  TOS
  fst   qword [esp]
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; binary float representation <-> return stack
;;
code: F32R>F  ( | n -- )
  fld   dword [ERP]
  add   ERP,4
  urnext
endcode

code: F>RF32  ( -- | n )
  add   ERP,4
  fstp  dword [ERP]
  urnext
endcode

code: F@RF32  ( -- | n )
  add   ERP,4
  fst   dword [ERP]
  urnext
endcode

code: F64R>F  ( | n0 n1 -- )
  fld   qword [ERP]
  add   ERP,8
  urnext
endcode

code: F>RF64  ( -- | n0 n1 )
  add   ERP,8
  fstp  qword [ERP]
  urnext
endcode

code: F@RF64  ( -- n0 n1 )
  add   ERP,8
  fst   qword [ERP]
  urnext
endcode

alias F>RF64 F>R
alias F64R>F R>F
alias F@RF64 F@R
alias 2rdrop FRDROP


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple mul/div with a constant
;;
code: F10*  ( -- )
  ld    dword [esp-4],10
  fimul dword [esp-4]
  urnext
endcode

code: F10/  ( -- )
  ld    dword [esp-4],10
  fidiv dword [esp-4]
  urnext
endcode

code: F2*  ( -- )
  ld    dword [esp-4],2
  fimul dword [esp-4]
  urnext
endcode

code: F2/  ( -- )
  ld    dword [esp-4],2
  fidiv dword [esp-4]
  urnext
endcode

code: F1+
  ld    dword [esp-4],1
  fiadd dword [esp-4]
  urnext
endcode

code: F1-
  ld    dword [esp-4],1
  fisub dword [esp-4]
  urnext
endcode

code: F**2
  fmul st,st
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various constants
;;
code: 0.E
  fldz
  urnext
endcode

code: 1.E
  fld1
  urnext
endcode

code: 2.E
  ld    dword [esp-4],2
  fild  dword [esp-4]
  urnext
endcode

code: 10.E
  ld   dword [esp-4],10
  fild dword [esp-4]
  urnext
endcode

code: FPI
  fldpi
  urnext
endcode

code: FLG2
  fldlg2
  urnext
endcode

code: FLN2
  fldln2
  urnext
endcode

code: FL2T
  fldl2t
  urnext
endcode

code: FL2E
  fldl2e
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU stack <-> memory
;;
;; store 8-byte float at the given address
code: DF!  ( addr -- )
  fstp  qword [TOS]
  pop   TOS
  urnext
endcode

;; load 8-byte float from the given address
code: DF@  ( addr -- )
  fld   qword [TOS]
  pop   TOS
  urnext
endcode

;; store 4-byte float at the given address
code: SF!  ( addr -- )
  fstp  dword [TOS]
  pop   TOS
  urnext
endcode

;; load 4-byte float from the given address
code: SF@  ( addr -- )
  fld   dword [TOS]
  pop   TOS
  urnext
endcode

code: F80MEM>F  ( addr -- )
  fld   tbyte [TOS]
  pop   TOS
  urnext
endcode

code: F>F80MEM  ( addr -- )
  fstp  tbyte [TOS]
  pop   TOS
  urnext
endcode

;; 10-byte transfers were deliberately omited

: FLOATS  ( n -- n*8 )  3 lshift ;
: FLOAT+  ( addr -- addr+8 )  8 + ;

: DFLOATS  ( n -- n*8 )  3 lshift ;
: DFLOAT+  ( addr -- addr+8 )  8 + ;

: SFLOATS  ( n -- n*4 )  2 lshift ;
: SFLOAT+  ( addr -- addr+4 )  cell+ ;

alias DF! F!
alias DF@ F@

: F,  ( -- )  ( f:value -- )
  1 floats n-allot f!
;

: FCONSTANT  \ name
  create f, create; does> f@
;

;; they are immediate because they're doing nothing at all
: FALIGN ; immediate
: FALIGNED ; immediate
: SFALIGN ; immediate
: SFALIGNED ; immediate
: DFALIGN ; immediate
: DFALIGNED ; immediate

;; this will be either mine, or ans-compatible
: FVARIABLE
  [IFDEF] ANS-VARIABLE
    0.e
  [ELSE]
    fdepth ERR-FSTACK-UNDERFLOW not-?error
  [ENDIF]
  create f, create;
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; comparisons
;;
code: F0=
  push  TOS
  xor   TOS,TOS
  ftst
  ffree st
  fincstp
  fstsw ax
  sahf
  setz  cl
  urnext
endcode

code: F0<
  push  TOS
  xor   TOS,TOS
  ftst
  ffree st
  fincstp
  fstsw ax
  sahf
  setb  cl
  urnext
endcode

code: F0>
  push  TOS
  xor   TOS,TOS
  ftst
  ffree st
  fincstp
  fstsw ax
  sahf
  seta  cl
  urnext
endcode

code: F0<=
  push  TOS
  xor   TOS,TOS
  ftst
  ffree st
  fincstp
  fstsw ax
  sahf
  setbe cl
  urnext
endcode

code: F0>=
  push  TOS
  xor   TOS,TOS
  ftst
  ffree st
  fincstp
  fstsw ax
  sahf
  setae cl
  urnext
endcode


code: F=
  push  TOS
  xor   TOS,TOS
  fcompp
  fstsw ax
  sahf
  sete  cl
  urnext
endcode

code: F<
  push  TOS
  xor   TOS,TOS
  fcompp
  fstsw ax
  sahf
  seta  cl
  urnext
endcode

code: F>
  push  TOS
  xor   TOS,TOS
  fcompp
  fstsw ax
  sahf
  setb  cl
  urnext
endcode

code: F<=
  push  TOS
  xor   TOS,TOS
  fcompp
  fstsw ax
  sahf
  setae cl
  urnext
endcode

code: F>=
  push  TOS
  xor   TOS,TOS
  fcompp
  fstsw ax
  sahf
  setbe cl
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utils
;;
code: FMAX  ( -- )
  fcom  st1
  fstsw ax
  sahf
  jr    b,@f
  fxch  st1
@@:
  ffree st
  fincstp
  urnext
endcode

code: FMIN  ( -- )
  fcom  st1
  fstsw ax
  sahf
  jr    a,@f
  fxch  st1
@@:
  ffree st
  fincstp
  urnext
endcode

code: FNEGATE  ( -- )
  fchs
  urnext
endcode

code: FABS
  fabs
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple operations
;;
code: FCOS  ( -- )
  fcos
  urnext
endcode

code: FSIN  ( -- )
  fsin
  urnext
endcode

code: FSINCOS  ( -- )
  fsincos
  urnext
endcode

code: F*  ( -- )
  fmulp st1
  urnext
endcode

code: F+  ( -- )
  faddp st1
  urnext
endcode

code: F-  ( -- )
  fsubp st1
  urnext
endcode

code: F/  ( -- )
  fdivp st1
  urnext
endcode

code: FSQRT  ( -- )
  fsqrt
  urnext
endcode

code: FINT  ( -- )
  frndint
  urnext
endcode


code: FLN  ( -- )
  fldln2
  fxch  st1
  fyl2x
  urnext
endcode

code: FLNP1  ( -- )
  fld1
  faddp st1
  fldln2
  fxch  st1
  fyl2x
  urnext
endcode

code: FLOG  ( -- )  ( f:n0 -- f:n1 )
  fldlg2
  fxch  st1
  fyl2x
  urnext
endcode


;; e^(x) = 2^(x * LOG{2}e)
code: FEXP  ( -- )
  ;; normalize
  fldl2e
  fmulp st1
  ;; set rounding mode to truncate
  fstcw word [esp-4]
  ld    eax,[esp-4]
  and   ah,0xF3
  or    ah,0x0C
  ld    [esp-8],eax
  fldcw word [esp-8]
  ;; get integer and fractional parts
  fld   st0
  frndint
  fxch  st1
  fsub  st0,st1
  ;; exponentiate fraction
  f2xm1
  fld1
  faddp st1
  ;; scale in integral part
  fscale
  ;; clean up
  fxch  st1
  fcomp st1
  ;; restore mode
  fldcw word [esp-4]
  urnext
endcode

: FEXPM1  ( -- )
  FEXP F1-
;

;; power should be positive
code: F**  ( -- )
  fxch  st1
  fyl2x
  fld1
  fld   st1
  fprem
  f2xm1
  faddp st1   ;; this was "fadd"
  fscale
  fxch  st1
  fstp  st
  urnext
endcode

code: FTAN  ( -- )
  fptan
  fdivp st1
  urnext
endcode

code: FATAN  ( -- )
  fld1
  fpatan
  urnext
endcode

code: FATAN2  ( -- )
  fpatan
  urnext
endcode

code: FACOS  ( -- )
  fld1
  fld   st1
  fmul  st,st
  fsubp st1
  fsqrt
  fxch  st1
  fpatan
  urnext
endcode

code: FASIN  ( -- )
  fld1
  fld   st1
  fmul  st,st
  fsubp st1
  fsqrt
  fpatan
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some non-standard words
;;
code: FRAD->DEG
  ld    dword [esp-4],180
  fimul dword [esp-4]
  fldpi
  fdivp st1
  urnext
endcode

code: FDEG->RAD
  fldpi
  fmulp st1
  ld    dword [esp-4],180
  fidiv dword [esp-4]
  urnext
endcode

code: FLOG2  ( F: r1 -- r2 )
  fld1
  fxch  st1
  fyl2x
  urnext
endcode

code: F[LOG]  ;; using 2 stack slots
  fldlg2
  fxch  st1
  fyl2x
  frndint
  urnext
endcode


code: FD=  ( dl dh -- flag )
  xchg  TOS,[esp]
  push  TOS
  ficom dword [esp]
  fstsw ax
  sahf
  pop   TOS
  pop   TOS
  sete  cl
  movzx TOS,cl
  urnext
endcode

code: FD<  ( dl dh -- flag )
  xchg  TOS,[esp]
  push  TOS
  ficom dword [esp]
  fstsw ax
  sahf
  pop   TOS
  pop   TOS
  setb  cl
  movzx TOS,cl
  urnext
endcode

code: FD>  ( dl dh -- flag )
  xchg  TOS,[esp]
  push  TOS
  ficom dword [esp]
  fstsw ax
  sahf
  pop   TOS
  pop   TOS
  seta  cl
  movzx TOS,cl
  urnext
endcode

code: FD<=  ( dl dh -- flag )
  xchg  TOS,[esp]
  push  TOS
  ficom dword [esp]
  fstsw ax
  sahf
  pop   TOS
  pop   TOS
  setbe cl
  movzx TOS,cl
  urnext
endcode

code: FD>=  ( dl dh -- flag )
  xchg  TOS,[esp]
  push  TOS
  ficom dword [esp]
  fstsw ax
  sahf
  pop   TOS
  pop   TOS
  setae cl
  movzx TOS,cl
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU state management
;;
28 constant FPU-ENV-SIZE

code: FENV!  ( addr -- )
  fnstenv [TOS]
  pop   TOS
  urnext
endcode

code: FENV@  ( addr -- )
  fldenv [TOS]
  pop   TOS
  urnext
endcode

;; overestimate a little
108 constant FPU-SAVE-SIZE

code: FSAVE  ( addr -- )
  fnsave [TOS]
  pop   TOS
  urnext
endcode

code: FRESTORE  ( addr -- )
  frstor [TOS]
  pop   TOS
  urnext
endcode


code: F+INF  ( -- )
  ld    eax,.plusinf
  fld   dword [eax]
  urnext
.plusinf: dd 0x7f80_0000
endcode

code: F-INF  ( -- )
  ld    eax,.plusinf
  fld   dword [eax]
  urnext
.plusinf: dd 0xff80_0000
endcode

;; quiet
code: FNAN  ( -- )
  ld    eax,.plusinf
  fld   dword [eax]
  urnext
.plusinf: dd 0x7fc0_0000
endcode

;; signaling
code: FSNAN  ( -- )
  ld    eax,.plusinf
  fld   dword [eax]
  urnext
.plusinf: dd 0x7f80_0001
endcode

code: F-SGN?  ( -- sign )  ( f:n -- f:n )
  push  TOS
  fst   dword [esp-4]
  xor   TOS,TOS
  cp    dword [esp-4],0
  jr    z,@f
  ld    cl,1
  jr    ns,@f
  ld    TOS,0xffff_ffff
@@:
  urnext
endcode

code: F-FINITE?  ( -- flag )  ( f:n -- f:n )
  push  TOS
  fst   dword [esp-4]
  xor   TOS,TOS
  and   dword [esp-4],0x7f800000
  cp    dword [esp-4],0x7f800000
  setne cl
  urnext
endcode

: F-NAN?  ( -- flag )  ( f:n -- f:n )
  f@f32 1 lshift 0xff000000 u>
;

: F-INF?  ( -- false // 1 // -1 )  ( f:n -- f:n )
  f@f32 dup 1 lshift 0xff000000 <> if drop 0 endif
  sgn
;

: (FPU-INT-CREATOR)  ( modecfa -- ) ( f:n -- f:n ) \ name
  create ,
 does>
  FPU-GETCW >r @execute FINT r> FPU-SETCW
;

' FPU-LOW-MODE   (FPU-INT-CREATOR) FLOOR ( F:r1 -- F:r2 )
' FPU-ROUND-MODE (FPU-INT-CREATOR) FROUND ( F:r1 -- F:r2 )
' FPU-TRUNC-MODE (FPU-INT-CREATOR) FTRUNC ( F:r1 -- F:r2 )


also (env-queries) definitions
;; the maximum depth of the separate floating-point stack
;; on systems with the environmental restriction of keeping
;; floating-point items on the data stack, n = 0
: FLOATING-STACK  ( -- n )  8 ;

;; k8: i don't fuckin' know what this should return, and where
\ : (ENV?-MAX-FLOAT)  ( -- n )  F+INF ;

previous definitions
