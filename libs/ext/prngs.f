;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some additional prngs (won't be loaded by "all.f")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm
get-current also prng definitions


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; XorShift*-64/32
;;
code: XORSHIFT*-64/32  ( statelo statehi -- newstatelo newstatehi u32prv )
  ;; shift: >>12, <<25; >>27
  pop   ebx
  ld    edx,ecx
  ld    eax,ebx
  shrd  eax,edx,0xc
  shr   edx,0xc
  xor   ecx,edx
  xor   ebx,eax
  ld    edx,ecx
  ld    eax,ebx
  shld  edx,eax,0x19
  shl   eax,0x19
  xor   ecx,edx
  xor   ebx,eax
  ld    edx,ecx
  ld    eax,ebx
  shrd  eax,edx,0x1b
  shr   edx,0x1b
  xor   ecx,edx
  xor   ebx,eax
  ;; ECX: hi
  ;; EBX: lo
  ;; push new state
  push  ebx
  push  ecx
  ;; mul by 0x2545F491_4F6CDD1D
  ld    edx,0x2545F491
  ld    eax,0x4F6CDD1D
  imul  ecx,eax
  imul  edx,ebx
  add   ecx,edx
  mul   ebx
  add   ecx,edx
  ;;ld    TOS,eax
  ;; we need only high 32 bits, and they are already in ECX
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PCG32EX

;; generate 32-bit random number
;; inc is stream id; 2^63 streams are supported (bit 63 of stream number is irrelevant)
;;
;; code for:
;; oldstate = state;
;; state = oldstate*6364136223846793005UL+((inc<<1)|1);
;; u32 xorshifted = ((oldstate>>18)^oldstate)>>27;
;; u32 rot = oldstate>>59;
;; res = (xorshifted>>rot)|(xorshifted<<((-rot)&31));
code: PCG32EX-NEXT  ( statelo statehi inclo inchi -- newstatelo newstatehi inclo inchi u32prv )
  push  TOS
  push  EIP    ;; it will be used in the code
  sub   esp,8  ;; and two temp vars
  ;; [esp+0]: tmpvar0
  ;; [esp+4]: tmpvar1
  ;; [esp+8]: EIP
  ;; [esp+12]: inchi
  ;; [esp+16]: inclo
  ;; [esp+20]: statehi
  ;; [esp+24]: statelo
  ld    edx,[esp+20]  ;; statehi
  ld    eax,[esp+24]  ;; statelo
  ld    [esp+0],edx   ;; tmpvar0
  imul  edx,edx,0x4c957f2d
  imul  ecx,eax,0x5851f42d
  add   ecx,edx
  ld    edx,0x4c957f2d
  ld    [esp+4],eax   ;; tmpvar1
  mul   edx
  add   edx,ecx
  ld    ebx,[esp+16]  ;; inclo
  ld    ecx,[esp+12]  ;; inchi
  scf
  rcl   ebx,1
  rcl   ecx,1
  add   eax,ebx
  adc   edx,ecx
  ld    [esp+20],edx  ;; statehi
  ld    edx,[esp+0]   ;; tmpvar0
  ld    [esp+24],eax  ;; statelo
  ld    eax,[esp+4]   ;; tmpvar1
  shrd  eax,edx,0x12
  shr   edx,0x12
  xor   eax,[esp+4]   ;; tmpvar1
  xor   edx,[esp+0]   ;; tmpvar0
  shrd  eax,edx,0x1b
  shr   edx,0x1b
  ld    esi,eax
  ld    edx,[esp+0]   ;; tmpvar0
  ld    eax,[esp+4]   ;; tmpvar1
  shr   edx,0x1b
  ld    eax,edx
  ld    edi,eax
  ld    eax,esi
  ld    ecx,edi
  xor   edx,edx
  shr   eax,cl
  ld    ecx,edi
  neg   ecx
  ld    edx,esi
  and   ecx,0x1f
  shl   edx,cl
  or    eax,edx
  ld    TOS,eax  ;; u32prv
  add   esp,8
  pop   EIP
  urnext
endcode

: PCG32EX-SEED-U64  ( dlo dhi inclo inchi -- statelo statehi strmlo strmhi )
  0 0 2swap pcg32ex-next drop 2nrot d+ 2swap pcg32ex-next drop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bob Jenkins small PRNG -- http://burtleburtle.net/bob/rand/smallprng.html
;;
(* slightly better:
  e = a-ROT(b, 23);
  a = b^ROT(c, 16);
  b = c+ROT(d, 11);
  c = e+d;
  d = e+a; -- u32prv
*)

code: BJEX-NEXT  ( a b c d -- a b c d u32prv )
  push  TOS
  ;; [esp+0]: d
  ;; [esp+4]: c
  ;; [esp+8]: b
  ;; [esp+12]: a
  ld    edi,[esp+12]
  ld    edx,[esp+8]
  ld    ecx,edx  ;; save b, to use it later
  rol   edx,23
  sub   edi,edx
  ;; ECX: b
  ;; EDI: e
  ld    eax,[esp+4]
  ld    ebx,eax  ;; save c, to use it later
  rol   eax,16
  xor   ecx,eax
  ld    [esp+12],ecx
  ;; ECX: a
  ;; EBX: c
  ;; EDI: e
  ld    eax,[esp+0]
  ld    edx,eax  ;; save d, to use it later
  rol   eax,11
  add   eax,ebx
  ld    [esp+8],eax
  ;; ECX: a
  ;; EBX: c
  ;; EDX: d
  ;; EDI: e
  add   edx,edi
  ld    [esp+4],edx
  add   TOS,edi
  ld    [esp+0],TOS
  urnext
endcode

: BJEX-SEED-U32  ( u -- a b c d )
  ;; this is how BJ does it for 32-bit seeds
  0xf1ea5eed swap dup dup  ;; initial seed
  20 for bjex-next drop endfor ;; skip first 20 values, to perturb seed
;

;; k8: this sux, but who cares
: BJEX-SEED-U64  ( du -- a b c d )
  u32hash swap u32hash xor bjex-seed-u32
;


previous set-current
