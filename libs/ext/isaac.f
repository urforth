;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: asm
get-current also prng definitions


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ISAAC random number generator
;; (c) Bob Jenkins, March 1996, Public Domain
;; You may use this code in any way you wish, and it is free.  No warrantee.
;; x86 assembler translation by Ketmar Dark (from "readable.c")
;;
;; ISAAC state `mm` is 259 dwords (cells)
;; last 3 dwords are `aa`, `bb` and `cc`
;; `rr` is 256 dwords of output
code: ISAAC  ( mm rr -- )
  ld    edi,TOS    ;; rr
  xchg  esi,[esp]  ;; mm (and old ESI is saved at [esp])

  ;; modify `cc`
  ld    eax,[esi+0x408]  ;; cc
  inc   eax
  ld    [esi+0x408],eax  ;; cc
  ;; modify `bb`
  add   [esi+0x404],eax  ;; bb

  ;; main loop
  xor   ecx,ecx
.mainloop:
  ;; modify aa
  ld    eax,[esi+0x400]  ;; aa
  ld    ebx,ecx
  and   ebx,3
  jr    nz,@f
  ;; case 0
  shl   eax,13
  jr    .doneaa
@@:
  dec   bl
  jr    nz,@f
  ;; case 1
  shr   eax,6
  jr    .doneaa
@@:
  dec   bl
  jr    nz,@f
  ;; case 2
  shl   eax,2
  jr    .doneaa
@@:
  ;; case 3
  shr   eax,16
.doneaa:
  xor   eax,[esi+0x400]  ;; shifted_aa^aa
  ;; new_aa+mm[(i+128)%256]
  lea   edx,[ecx+128]
  movzx edx,dl
  add   eax,[esi+edx*4]
  ld    [esi+0x400],eax  ;; aa
  ld    ebx,[esi+ecx*4]  ;; x=mm[i]
  ;; EAX is aa
  ;; EBX is x
  ;; EDX will be (x>>2)%256
  add   eax,[esi+0x404]  ;; +bb
  ld    edx,ebx
  shr   edx,2
  movzx edx,dl
  add   eax,[esi+edx*4]  ;; EAX is y
  ld    [esi+ecx*4],eax  ;; update m[i]
  ;; EAX is y
  ;; EBX is x
  ;; update bb
  shr   eax,10
  movzx eax,al
  ld    eax,[esi+eax*4]
  add   eax,ebx
  ld    [esi+0x404],eax  ;; bb
  ;; EAX is new bb
  ;; update rr
  stosd
  ;; next loop iteration
  inc   ecx
  test  ch,ch
  jr    z,.mainloop

  ;; done
  pop   esi
  pop   TOS
  urnext
endcode

0x40c constant isaac-state-size
0x400 constant isaac-output-size

: isaac-aa  ( mm -- addr ) 256 +cells ;
: isaac-bb  ( mm -- addr ) 257 +cells ;
: isaac-cc  ( mm -- addr ) 258 +cells ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; initialization of the random number generator with or without a seed
;; copyright 1997 Pierre Henri Michel Abbat as a derivative work
;; anyone may use this code freely, as long as credit is given
0 value isaac-mm
0 value isaac-rr

: 7-roll
  swap >r swap >r swap >r swap >r swap >r swap >r swap >r
  r> r> r> r> r> r> r>
;

: isc-8@ 8 +cells 8 for cell- dup @ swap endfor drop ;
: isc-8! 8 cells over + swap do i ! cell +loop ;
: isc-8+! 8 cells over + swap do i +! cell +loop ;

: (nextnum)  nrot over + 2swap tuck + swap 2swap rot 7-roll ;

: mix ( h g f e d c b a - h' g' f' e' d' c' b' a' )
  over $0b lshift xor  (nextnum)
  over $02 rshift xor  (nextnum)
  over $08 lshift xor  (nextnum)
  over $10 rshift xor  (nextnum)
  over $0a lshift xor  (nextnum)
  over $04 rshift xor  (nextnum)
  over $08 lshift xor  (nextnum)
  over $09 rshift xor  (nextnum)
;

;; initializes isaac
;; if the flag is 0, use a default initialization; otherwise, use the contents of isaac-rr to compute the seed
: isaac-init-ex  ( mm rr flag -- )
  >r ( save flag )
  to isaac-rr to isaac-mm
  isaac-mm isaac-state-size erase

  $9e3779b9 dup 2dup 2dup 2dup mix mix mix mix
  r@ ifnot isaac-rr isaac-output-size erase endif

  isaac-rr isaac-output-size over + swap do
    i isc-8+! i isc-8@ mix i isaac-mm + isaac-rr - dup >r isc-8! r> isc-8@
  8 cells +loop
  r> if
    isaac-mm isaac-output-size over + swap do
      i isc-8+! i isc-8@ mix i isc-8! i isc-8@
    8 cells +loop
  endif
  2drop 2drop 2drop 2drop
;

;; init isaac `mm` with 256-cell `rr`
: isaac-init  ( mm rr -- )  true isaac-init-ex ;


previous set-current
