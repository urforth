\ DEBUG-INFO-OFF

\ 80386 Disassembler
\ Andrew McKewan, April 1994
\ Tom Zimmer,  05/18/94 port to Win32f
\ Modified to word in decimal 08/03/94 10:04 tjz
\ 06-??-1995 SMuB NEXT sequence defined in FKERNEL
\ 06-21-1995 SMuB removed redundant COUNT calls from txb, lxs.
\ 04-??-1997 Extended by C.L. to include P6 and MMX instructions
\ 26-07-2001 Fixed MVX (Maksimov)
\ 11-05-2004 Fixed FDA and CMV (Serguei Jidkov)
\ 01-19-2015 Jos, Extended for XMM instructions (adaptation by Ketmar Dark)
\ 25-08-2020 Fixed bug in ENTER (Ketmar Dark)


;; hide it all
vocabulary disx86
also disx86 definitions


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
260 constant maxstring

: cincr  ( n -- [n] = [n]+1 )
  1 swap c+!
;

: c+place  ( ch c-addr -- )
  dup cincr bcount + 1- c!
;

: place  ( a u dest -- )
\ put a u at dest as counted string
  2dup 2>r  1+ swap move              \ move first to handle overlap
  2r> c!                              \ then store count character
;

: +place  ( a u dest -- )
\ append string a u to counted string
  2dup 2>r                            \   at dest
  bcount + swap move
  2r> c+!
;

[IFNDEF] dabs
: (dabs)  ( d -- )
  dup 0< if
    bitnot swap bitnot
    ;; ( dhi dlo )
    dup 1+         ;; ( dhi dlo dlo+1 )
    over           ;; ( dhi dlo dlo+1 dlo )
    u<             ;; ( dhi dlo flag )
    >r 1+ swap r>  ;; ( dlo+1 dhi flag )
    if 1+ endif
  endif
;
[ELSE]
alias dabs (dabs)
[ENDIF]

: (d.) ( d -- addr len )
  tuck (dabs) <# #s rot sign #>
;

: h.r ( u n -- )
  base @ >r hex u.r r> base !
;

: h.n ( n1 n2 -- )
  base @ >r hex >r
  0 <# r>
  dup 0 > if
    0 do # loop
  else
    drop
  endif
  #> type
  r> base !
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 value default-16bit?

: default-16bit ( -- )  true to default-16bit? ;
: default-32bit ( -- )  false to default-16bit? ;

' drop defered show-name   ( cfa -- )      \ display nearest symbol

0 value base-addr

\ use my z80-like syntax?
true value k8syntax?
true value k8syntax-regs?


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ vocabulary disx86
\ disx86 also definitions

decimal

: dis-wpeek ( addr -- addr word[addr] )
  dup w@
;

: dis-cfetch ( addr -- addr+1 byte[addr] )
  bcount
;

: dis-c@ ( addr -- byte[addr] )
  c@
;

create s-buf MAXSTRING allot


: 0>s  ( -- )
  \ reset s-buf
  s-buf 0c!
;

: >s  ( a1 n1 -- )
  s-buf +place
;

\ strip trailing spaces from s-buf
: s-xstrip ( -- )
  s-buf bcount
  begin
    dup
  while
    2dup + 1- c@ 32 > if
      swap 1- c! exit
    endif
    1-
  repeat
  swap 1- c!
;

: s-lastch-addr ( -- addr )
  s-buf bcount + 1-
;

: s-lastch@ ( -- ch )
  s-lastch-addr c@
;

: s-chop ( -- )
  s-buf bcount 1- 0 max swap 1- c!
;

: emit>s  ( c1 -- )
  s-buf c+place
;

: sspaces  ( n1 -- )
  dup 0 > if 0 do bl emit>s loop else drop endif
;

: sspace  ( -- )
  bl emit>s
;

: s-end-mnemo  ( -- )
  s-buf c@ 8 < if
    begin
      s-buf c@ 8 <
    while
      sspace
    repeat
  else
    \ want at least one space
    sspace
  endif
;

: s>  ( -- a1 n1 )
  s-buf bcount
;


: (.s")  ( addr n -- )
  s-buf +place
;

: .s"  ( 'text' -- )
  [compile] "
  compile (.s")
; immediate


: d.r>s  ( d w -- )
  >r (d.) r> over - sspaces >s
;

: disasm-s>d  ( n -- d )
  dup 0< if -1 else 0 endif
;

: .r>s  ( n w -- )
  >r  disasm-s>d  r>  d.r>s
;

: u.r>s  ( u w -- )
  0 swap d.r>s
;

: h.>s  ( u -- )
  base @ swap  hex 0 (d.) >s ( sspace )   base !
;

: h.r>s  ( n1 n2 -- )
  base @ >r hex >r
  \ 0 <# #s #>
  <#u #s #>
  r> over - sspaces >s
  r> base !
;

: 0h.r>s  ( n1 n2 -- )
  base @ >r hex >r
  \ 0 <# #s #>
  <#u #s #>
  r> over -
  dup 0> if
    0 do 48 emit>s loop
  else
    drop
  endif
  >s
  r> base !
;


: ?.name>s  ( cfa -- )
  .s" 0x"
  8 0h.r>s
;

' ?.name>s to show-name



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main disassembler code
;;

false value size
false value 16-bit-data
false value 16-bit-addr
false value prefix-op
false value prefix-op-a16-d16  \ 0, 1, 2
false value prefix-seg
false value mmx-reg
false value xmm-reg  \ for a 32-bit environment
false value data-size-prefix    \ 0:don't need it; 1:"byte"; 2:"word"; 4:"dword"
false value disp-as-reg-offset


: .,  ( -- )  .s" ," ;

\ k8: use this as hex prefix
: .#  ( -- )  .s" 0x" ;

: .,#  ( -- )  ., .# ;
: .[ [char] [ emit>s ;
: .] [char] ] emit>s ;

: .datasize  ( -- )
  data-size-prefix case
    1 of .s" byte " endof
    2 of .s" word " endof
    4 of .s" dword " endof
    16-bit-data prefix-op-a16-d16 2 = logand if 0 to prefix-op-a16-d16 .s" word " endif
  endcase
  0 to data-size-prefix
;

: .segpfx  ( -- )
  prefix-seg case
    1 of .s" cs:" endof
    2 of .s" ds:" endof
    3 of .s" ss:" endof
    4 of .s" es:" endof
    5 of .s" gs:" endof
    6 of .s" fs:" endof
  endcase
  false to prefix-seg
;

: set-data-size-with-bit ( bit -- )
  if
    16-bit-data if 2 else 4 endif
  else
    1
  endif
  to data-size-prefix
;

: @+  ( addr -- addr n )  dup cell+ swap @ ;
: W@+ ( addr -- addr n )  dup 2+ swap W@ ;

: sext  ( byte -- n )  dup $80 and if $FFFFFF00 or then ;

: mod/sib
  ( mod-r-r/m -- r/m r mod ) \ r including general, special, segment, MMX
  ( mod-op-r/m -- r/m op mod )
  ( s-i-b -- b i s )
    255 and 8 /mod 8 /mod
;

: ???   ( n1 -- )
  .s" ???" drop
;

: ss. ( n adr len w )  >r drop  swap r@ * +  r> >s ; \ sspace ;

: tttn ( code -- ) 15 and S" o nob aee nebea s nsp npl geleg " 2 ss. s-xstrip ;

: sreg  ( sreg -- )  3 rshift 7 and S" escsssdsfsgsXXXX" 2 ss. ;
: creg  ( eee --  )  3 rshift 7 and S" cr0???cr2cr3cr4?????????" 3 ss. ;
: dreg  ( eee --  )  3 rshift 7 and S" dr0dr1dr2dr3??????dr6dr7" 3 ss. ;
: treg  ( eee --  )  3 rshift 7 and S" ?????????tr3tr4tr5tr6tr7" 3 ss. ; \ obsolete
: mreg  ( n -- )  7 and S" mm0mm1mm2mm3mm4mm5mm6mm7" 3 ss. ;

: reg8  ( n -- )  7 and S" alcldlblahchdhbh" 2 ss. ;
: reg16 ( n -- )  7 and S" axcxdxbxspbpsidi" 2 ss. ;
: reg32 ( n -- )
  7 and
  k8syntax-regs? if
    dup 1 = if .s" TOS" drop exit endif
    dup 5 = if .s" ERP" drop exit endif
    dup 6 = if .s" EIP" drop exit endif
  endif
  S" eaxecxedxebxespebpesiedi" 3 ss.
;
: .xmmreg  ( n -- )  7 and S" xmm0xmm1xmm2xmm3xmm4xmm5xmm6xmm7" 4 ss. ; \ 1
: reg16/32  ( n -- )
  16-bit-data if reg16 else reg32 then
;
: reg  ( a n -- a )
  xmm-reg if
    .xmmreg
  else
    mmx-reg if
      mreg
    else
      size
      if reg16/32 else reg8 then
    then
  endif
;

: [base16]  ( r/m -- )
  .datasize .segpfx
  4- S" [si][di][bp][bx]" 4 ss.
  \ r/m = 4 , 5 , 6 , 7
;

: [ind16]  ( r/m -- )
  .datasize .segpfx
  S" [bx+si][bx+di][bp+si][bp+di]" 7 ss.
  \ r/m = 0  ,   1  ,   2  ,   3
;

: [reg16] ( r/m -- )
  dup 4 < if [ind16] else [base16] then
;

: [reg32]  ( n -- )
  .datasize .segpfx
  7 and
  k8syntax-regs? if
    dup 1 = if .s" [TOS]" drop exit endif
    dup 5 = if .s" [ERP]" drop exit endif
    dup 6 = if .s" [EIP]" drop exit endif
  endif
  S" [eax][ecx][edx][ebx][esp][ebp][esi][edi]" 5 ss.
;

\ : [reg]    ( r/m -- )  16-bit-addr
\                        if   [reg16]
\                        else [reg32]
\                        then sspace ;

\ : [reg] ( n -- )
\        7 and
\        16-bit-addr
\        if      S" [bx+si] [bx+di] [bp+si] [bp+di] [si] [di] [bp] [bx]"
\                rot 0
\                ?do     bl skip bl scan
\                loop    bl skip 2dup bl scan nip - >s 2 sspaces
\        else    S" [eax][ecx][edx][ebx][esp][ebp][esi][edi]" 5 ss. sspace
\        then    ;

(*
: [reg*2]  ( i -- )  .datasize S" [eax*2][ecx*2][edx*2][ebx*2][XXX*2][ebp*2][esi*2][edi*2]" 7 ss. ;
: [reg*4]  ( i -- )  .datasize S" [eax*4][ecx*4][edx*4][ebx*4][XXX*4][ebp*4][esi*4][edi*4]" 7 ss. ;
: [reg*8]  ( i -- )  .datasize S" [eax*8][ecx*8][edx*8][ebx*8][XXX*8][ebp*8][esi*8][edi*8]" 7 ss. ;
*)

: [reg*n] ( i n -- )
  over 7 and 4 = if
    nip .datasize .segpfx .s" [XXX]"
  else
    swap [reg32]
  endif
  s-chop
  [char] * emit>s
  [char] 0 + emit>s
  .]
;

: [reg*2]  ( i -- )  2 [reg*n] ;
: [reg*4]  ( i -- )  4 [reg*n] ;
: [reg*8]  ( i -- )  8 [reg*n] ;

: [index-has-scaled]  ( sib -- add-disp-flag )
  mod/sib over 4 =
  if
    \ no esp scaled index
    2drop
    0
  else
    s-lastch@ [char] ] = if s-lastch-addr s-chop else 0 endif >r
    case ( s )
      0 of [reg32] endof
      1 of [reg*2] endof
      2 of [reg*4] endof
      3 of [reg*8] endof
    endcase
    r> ?dup if [char] + swap c! endif
    1
  then
  nip
;

: [index]  ( sib -- )
  [index-has-scaled] drop
;

: .+sign  ( val -- val )  dup 0x8000_0000 u> if abs [char] - else [char] + endif emit>s ;

: .dispnum  ( val -- )
  .+sign dup 10 < if 0 .r>s else .# 0 h.r>s endif
;

: (.dispvalue)  ( val -- )
  dup data-size-prefix or if .dispnum else drop endif
;

: .disp-value  ( val -- )
  \ disp-as-reg-offset if dup 0>= if .s" +" endif 0 .r>s else show-name endif
  data-size-prefix if .datasize .segpfx .[ endif
  disp-as-reg-offset if (.dispvalue) else show-name endif
  data-size-prefix if .] endif
;

: disp8-value  ( adr -- adr' value )
  dis-cfetch c>s
;
: disp8  ( adr -- adr' )
  disp8-value .disp-value
;

: disp16-value  ( adr -- adr' value )
  w@+  ( w>s )
  dup 0x8000 u>= if 0x10000 - endif
;
: disp16 ( adr -- adr' )
  disp16-value .disp-value
;

: disp32-value ( adr -- adr' value )
  @+ ( body> )
;
: disp32 ( adr -- adr' )
  disp32-value .disp-value
;

: disp16/32  ( adr -- adr' )
  data-size-prefix if .datasize endif
  0 to data-size-prefix .segpfx .[ 16-bit-addr if disp16 else disp32 then .]
;

: imm8-nocomma  ( adr -- adr' )  .# dis-cfetch h.>s ;
: imm8  ( adr -- adr' )  ., imm8-nocomma ;

\ : imm16  ( adr -- adr' )  .,# w@+ h.>s ;

: imm16/32-nocomma  ( adr -- adr' ) .# 16-bit-data if w@+ else @+ endif h.>s ;
: imm16/32  ( adr -- adr' )  ., imm16/32-nocomma ;

: sib  ( adr mod -- adr )
  >r dis-cfetch tuck 7 and 5 = r@ 0= and
  if
    \ disp32 swap [index] rdrop  \ ebp base and mod = 00
    disp32-value >r
    swap [index-has-scaled] if
      \ need to add disp as scale
      s-chop
      r> (.dispvalue) \ r@ 0>= if [char] + emit>s endif r> 0 .r>s
      .]
    else
      rdrop
    endif
    rdrop
  else
    r> case ( mod )
      1 of disp8-value true  endof
      2 of disp32-value true endof
      otherwise drop false false
    endcase
    swap >r >r
    swap dup [reg32] [index]
    r> if
      s-chop
      r> (.dispvalue) \ r@ 0>= if [char] + emit>s endif r> 0 .r>s
      .]
    else
      rdrop
    endif
  then
;

\ : [*]  ( sib --  )
\       .s" sib = " h.>s ;

\ : sib ( adr ext -- adr' )
\ ?? wrong version
\        swap dis-cfetch >r swap  6 rshift 3 and
\        ?dup if 1 = if disp8 else disp32 then then
\        r> dup 7 and dup 5 =
\        if      drop [*]
\        else    [reg]
\                dup $38 and $20 =
\                if      drop
\                else    .s" [" dup 3 rshift reg32 -1 s-buf c+!
\                        5 rshift 6 and
\                        dup 6 = if 2+ then
\                       ?dup if .s" *" 0 .r>s  then .s" ] "
\                then
\        then ;

: mod-reg-predisp ( -- )
  s-lastch@ [char] ] = if
    0 to data-size-prefix
    1 to disp-as-reg-offset
    s-chop
  endif
;
: mod-reg-postdisp ( -- )
  disp-as-reg-offset if s-xstrip .] endif
;

: mod-r/m32  ( adr r/m mod -- adr' )
  dup 3 =
  if
    drop reg                \ mod = 3, register case
  else
    over 4 =
    if
      nip sib               \ r/m = 4, sib case
    else
      2dup 0= swap 5 = and  \ mod = 0, r/m = 5,
      if
        .s" dword " .segpfx .[
        0 to data-size-prefix
        2drop disp32        \ disp32 case
        s-xstrip .]
      else
        rot swap   >r
        \ case ( mod )
        \   1 of disp8  endof
        \   2 of disp32 endof
        \ endcase
        swap [reg32]
        \ moved here
        r>
        case ( mod )
          1 of mod-reg-predisp disp8  mod-reg-postdisp endof
          2 of mod-reg-predisp disp32 mod-reg-postdisp endof
        endcase
      then
    then
  then
;

: mod-r/m16  ( adr r/m mod -- adr' )
  2dup 0= swap 6 = and
  if
    2drop disp16  \ disp16 case
  else
    case ( mod )
      0 of [reg16]                  endof
      1 of swap disp8  swap [reg16] endof
      2 of swap disp16 swap [reg16] endof
      3 of reg                      endof
    endcase
  then
;

: mod-r/m  ( adr modr/m -- adr' )  mod/sib nip 16-bit-addr if mod-r/m16 else mod-r/m32 then ;

\ : mod-r/m  ( adr ext -- adr' )
\        dup $C7 and 5 =                \ 32bit displacement
\        16-bit-addr 0= and              \ and not 16bit addressing
\        if      drop disp32 .s" [] "
\                EXIT
\        then
\        dup $C0 and $C0 < over 7 and 4 = and
\        16-bit-addr 0= and              \ and not 16bit addressing
\        if      sib
\                EXIT
\        then
\        dup $C7 and 6 =                \ 16bit displacement
\        16-bit-addr and                 \ and 16bit addressing
\        if      drop disp32 .s" [] "
\                EXIT
\        then
\        dup 6 rshift
\        case
\          0 of  .s" 0 " [reg]  endof
\          1 of  swap disp8  swap [reg]  endof
\          2 of  swap disp32 swap [reg]  endof
\          3 of  reg  endof
\        endcase ;

: r/m8      0 to size mod-r/m ;
: r/m16/32  1 to size mod-r/m ;
: r/m16     true to 16-bit-data r/m16/32 ;

: r,r/m  ( adr -- adr' )  dis-cfetch dup 3 rshift reg ., mod-r/m ;

: r/m,r  ( adr -- adr' )  dis-cfetch dup >r mod-r/m ., r> 3 rshift reg ;

: r/m  ( adr op -- adr' )  2 and if r,r/m else r/m,r then ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Simple Opcodes --------------------
;;

: inh   ( -<name>- )
  create
  bl word
  \ count here place here count cell+ allot drop
  count cell+ allot drop
  does>
    count >s
    drop \ instruction code
    \ s-end-mnemo
;

inh clc  clc
inh stc  stc
inh cld  cld
inh std  std
\ inh rpnz repnz
\ inh repz repz
inh cbw  cbw
inh cdq  cdq
inh daa  daa
inh das  das
inh aaa  aaa
inh aas  aas
\ inh lock lock
inh inb  insb
inh osb  outsb
inh sah  sahf
inh lah  lahf
\ inh aam  aam
\ inh aad  aad
inh hlt  hlt
inh cmc  cmc
inh xlt  xlat
inh cli  cli
inh sti  sti

inh clt clts
inh inv invd
inh wiv wbinvd
inh ud2 ud2
inh wmr wrmsr
inh rtc rdtsc
inh rmr rdmsr
inh rpc rdpmc
inh ems emms
inh rsm rsm
inh cpu cpuid
inh ud1 ud1
\ inh lss lss
\ inh lfs lfs
\ inh lgs lgs

\ inh d16: d16:
\ inh a16: a16:
\ inh es:  es:
\ inh cs:  cs:
\ inh ds:  ds:
\ inh fs:  fs:
\ inh gs:  gs:

: aam  ( adr code -- adr' )
  .s" aam" drop dis-cfetch drop
;

: aad  ( adr code -- adr' )
  .s" aad" drop dis-cfetch drop
;

: d16  ( adr code -- adr' )
  drop \ .s" d16:"
  true to 16-bit-data
  true to prefix-op
  2 to prefix-op-a16-d16
;

: a16  ( adr code -- adr' )
  drop \ .s" a16:"
  true to 16-bit-addr
  true to prefix-op
  1 to prefix-op-a16-d16
;

: rpz  ( adr code -- adr' )
  drop .s" repnz"
  true to prefix-op
;

: rep  ( adr code -- adr' )
  drop .s" repz"
  true to prefix-op
;

: lok  ( adr code -- adr' )  \ This should have error checking added
  drop .s" lock"
  true to prefix-op
;

: cs:  ( adr code -- adr' )
  drop \ .s" cs:"
  true to prefix-op
  1 to prefix-seg
;

: ds:  ( adr code -- adr' )
  drop \ .s" ds:"
  true to prefix-op
  2 to prefix-seg
;

: ss:  ( adr code -- adr' )
  drop \ .s" ss:"
  true to prefix-op
  3 to prefix-seg
;

: es:  ( adr code -- adr' )
  drop \ .s" es:"
  true to prefix-op
  4 to prefix-seg
;

: gs:  ( adr code -- adr' )
  drop \ .s" gs:"
  true to prefix-op
  5 to prefix-seg
;

: fs:  ( adr code -- adr' )
  drop \ .s" fs:"
  true to prefix-op
  6 to prefix-seg
;

: isd  ( adr code -- adr' )
  drop 16-bit-data
  if   .s" insw"
  else .s" insd"
  endif
  s-end-mnemo
;

: osd  ( adr code -- adr' )
  drop 16-bit-data
  if   .s" outsw"
  else .s" outsd"
  endif
  s-end-mnemo
;

: inp  ( addr code -- addr' )
  .s" in" s-end-mnemo
  1 and
  if
    16-bit-data
    if   .s" ax,"
    else .s" eax,"
    endif
  else    .s" al,"
  endif
  dis-cfetch h.>s
;

: otp  ( addr code -- addr' )
  .s" out" s-end-mnemo
  1 and
  if
    dis-cfetch h.>s 16-bit-data
    if   .s" ,ax"
    else .s" ,eax"
    endif
  else
    dis-cfetch h.>s .s" ,al"
  endif
;

: ind
  ( addr code -- addr' )
  .s" in" s-end-mnemo
  1 and
  if
    16-bit-data
    if   .s" ax,dx"
    else .s" eax,dx"
    endif
  else
    .s" al,dx"
  endif
;

: otd  ( addr code -- addr' )
  .s" out" s-end-mnemo
  1 and
  if
    16-bit-data
    if   .s" dx,ax"
    else .s" dx,eax"
    endif
  else
    .s" dx,al"
  endif
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- ALU Opcodes --------------------
;;

: .alu  ( n -- )
  7 and
  k8syntax? over 7 = logand
  if
    drop .s" cp"
  else
    S" addor adcsbbandsubxorcmp" 3 ss.
  endif
  s-end-mnemo
;

: alu  ( adr op -- adr' )
  \ k8: data size; comment it to avoid prefixes on [reg],reg
  dup 1 and set-data-size-with-bit
  dup 3 rshift .alu r/m
;

: ali  ( adr op -- adr' )
  >r dis-cfetch
  dup 3 rshift .alu

  \ k8: data size
  r@ 1 and set-data-size-with-bit

  mod-r/m
  r> 3 and ?dup
  if
    1 =
    if
      imm16/32
    else
      .,# dis-cfetch sext
      base @ >r hex
      0 .r>s  \ sspace
      r> base !
    then
  else
    imm8
  then
;

: ala  ( adr op -- adr' )
  dup 3 rshift .alu
  1 and if 0 reg imm16/32 else 0 reg8 imm8 then
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Test/Xchg --------------------
;;

: txb  ( addr op -- addr' )
  dup 3 and S" testtestxchgxchg" 4 ss. s-end-mnemo
  1 and
  if   1 to size r,r/m  \ SMuB removed COUNT
  else 0 to size r,r/m  \ SMuB removed COUNT
  endif
;

: tst  ( addr op -- addr' )
  .s" test" s-end-mnemo
  1 and if
    16-bit-data
    if   .s" ax"
    else .s" eax"
    endif
    imm16/32
  else
    .s" al" imm8
  endif
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Inc/Dec ----------------------
;;

: inc  ( addr op -- addr' )  .s" inc" s-end-mnemo reg16/32 ;
: dec  ( addr op -- addr' )  .s" dec" s-end-mnemo reg16/32 ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Push/Pop --------------------
;;

: psh  ( addr op -- addr' )  .s" push" s-end-mnemo reg16/32 ;
: pop  ( addr op -- addr' )  .s" pop" s-end-mnemo reg16/32 ;
: pss  ( addr op -- addr' )  .s" push" s-end-mnemo sreg ;
: pps  ( addr op -- addr' )  .s" pop" s-end-mnemo sreg ;

: psa  ( addr op -- addr' )
  drop
  .s" pusha"
  16-bit-data ifnot .s" d" endif
  s-end-mnemo
;

: ppa  ( addr op -- addr' )
  drop
  .s" popa"
  16-bit-data ifnot .s" d" endif
  s-end-mnemo
;

: psi  ( addr op -- addr' )
  .s" push" s-end-mnemo
  2 and if imm8-nocomma else imm16/32-nocomma endif
;

: psf  ( addr op -- addr' )
  drop
  .s" pushf"
  16-bit-data ifnot .s" d" endif
  s-end-mnemo
;

: ppf  ( addr op -- addr' )
  drop
  .s" popf"
  16-bit-data ifnot .s" d" endif
  s-end-mnemo
;

: 8F.  ( addr op -- addr' )  drop dis-cfetch .s" pop" s-end-mnemo r/m16/32 ;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Move --------------------
;;

: mov-mnemo  ( -- )
  k8syntax? ifnot .s" mov" else .s" ld" endif
  s-end-mnemo
;

: mov  ( addr op -- addr' )  mov-mnemo r/m ;

: mri  ( addr op -- addr' ) ( mov register, imm )
  mov-mnemo
  dup 8 and if reg16/32 imm16/32 else reg8 imm8 endif
;

: mvi ( adr op -- adr' )   ( mov mem, imm )
  mov-mnemo
  \ drop
  1 and set-data-size-with-bit
  dis-cfetch mod-r/m size if imm16/32 else imm8 endif
;

: mrs  ( addr op -- addr' )
  ( 16-bit-data) true if
    mov-mnemo
    drop
    1 to size
    dis-cfetch dup mod-r/m .,
    sreg
  else
    ???
  endif
;

: msr  ( addr op -- addr' )
  ( 16-bit-data) true if
    mov-mnemo
    drop
    1 to size
    dis-cfetch dup sreg .,
    mod-r/m
  else
    ???
  endif
;

: mrc  ( addr op -- addr' )  mov-mnemo drop dis-cfetch dup reg32 ., creg ;
: mcr  ( addr op -- addr' )  mov-mnemo drop dis-cfetch dup creg ., reg32 ;
: mrd  ( addr op -- addr' )  mov-mnemo drop dis-cfetch dup reg32 ., dreg ;
: mdr  ( addr op -- addr' )  mov-mnemo drop dis-cfetch dup dreg ., reg32 ;
: mrt  ( addr op -- addr' )  mov-mnemo drop dis-cfetch dup reg32 ., treg ;  \ obsolete
: mtr  ( addr op -- addr' )  mov-mnemo drop dis-cfetch dup treg ., reg32 ;  \ obsolete

: mv1  ( addr op -- addr' )
  mov-mnemo
  1 and if
    16-bit-data
    if   .s" ax,"
    else .s" eax,"
    endif
  else
    .s" al,"
  endif
  disp16/32
;

: mv2  ( addr op -- addr' )
  mov-mnemo
  \ @@@ Bh fixed bug here
  swap disp16/32 ., swap
  1 and if
    16-bit-data
    if   .s" ax"
    else .s" eax"
    endif
  else
    .s" al"
  endif
;

: lea  ( addr op -- addr' )  .s" lea" s-end-mnemo drop  1 to size r,r/m ;

: lxs  ( addr op -- addr' )
  1 and
  if   .s" lds"
  else .s" les"
  endif
  s-end-mnemo
  r,r/m  \ SMuB removed COUNT
;

: bnd  ( addr op -- addr' )  .s" bound" s-end-mnemo drop  1 to size r,r/m ;

: arp  ( addr op -- addr' )  .s" arpl" s-end-mnemo drop  1 to size true to 16-bit-data r,r/m ;

: mli  ( addr op -- addr' )
  1 to size
  .s" imul" s-end-mnemo
  $69 = if r,r/m imm16/32 else r,r/m imm8 endif
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Jumps and Calls --------------------
;;

: rel8  ( addr op -- addr' )  dis-cfetch sext over + ( h.>s ) base-addr - show-name  ;

: rel16/32  ( addr op -- addr' ) 16-bit-addr if w@+ else @+ endif over + base-addr - show-name ;

: jsr  ( addr op -- addr' )  .s" call" s-end-mnemo drop rel16/32 ;

: jmp-jp-mnemo ( -- )  k8syntax? if .s" jp" else .s" jmp" endif ;

: jmp  ( addr op -- addr' )
  jmp-jp-mnemo
  s-end-mnemo
  2 and if rel8 else rel16/32 then
;

: .jxx  ( addr op -- addr' )
  k8syntax? if
    .s" jr" s-end-mnemo tttn .,
  else
    .s" j" tttn
    s-end-mnemo
  endif
;

: bra  ( addr op -- addr' )  .jxx rel8 ;
: lup  ( addr op -- addr' ) 3 and S" loopnzloopz loop  jecxz " 6 ss. s-end-mnemo rel8 ;
: lbr  ( addr op -- addr' ) .jxx rel16/32 ;

: rtn  ( addr op -- addr' )
  \ .s" ret near"
  .s" ret"
  1 and 0= if s-end-mnemo w@+ h.>s endif
;

: rtf  ( addr op -- addr' )
  .s" ret far"
  1 and 0= if sspace w@+ h.>s endif
;

: ent  ( addr op -- addr' )  drop .s" enter" s-end-mnemo w@+ 0 .r>s ., dis-cfetch 0 .r>s ;

: cis  ( addr op -- addr' )
  $9a =
  if   .s" call"
  else jmp-jp-mnemo
  endif
  s-end-mnemo
  16-bit-data
  if      .s" ptr16:16 "
  else    .s" ptr16:32 "
  endif
  dis-cfetch mod-r/m
;

: nt3  ( addr op -- addr' )  drop .s" int3" ;

: int  ( addr op -- addr' )  drop .s" int" s-end-mnemo dis-cfetch .# h.>s ;

inh lev leave
inh irt  iret
inh nto  into

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- string ops --------------------
;;

: str
  inh does>
    count >s
    1 and if .s" d" else .s" b" then
;

str mvs movs
str cps cmps
str sts stos
str lds lods
str scs scas

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Exchange --------------------
;;

: xga  ( addr op -- addr' )
  dup 0x90 = if
    drop .s" nop"
  else
    .s" xchg" s-end-mnemo .s" eax," reg16/32
  endif
;

\ : xch  ( addr op -- addr' )  .s" xchg" s-end-mnemo drop r,r/m ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Shifts & Rotates --------------------
;;

: .shift ( n -- )  7 and S" rolrorrclrcrshlshrxxxsar" 3 ss. s-end-mnemo ;

: shf  ( addr op -- addr' )
  >r dis-cfetch
  dup 3 rshift .shift
  mod-r/m .,
  r> $D2 and
  case
    $C0 of dis-cfetch h.>s endof
    $D0 of 1 h.>s endof
    $D2 of 1 reg8 endof
  endcase
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Extended Opcodes --------------------
;;

: wf1  ( addr -- addr' )
  1+ dis-cfetch dup
  $0c0 < if
    dup 3 rshift 7 and
    case
      6 of .s" fstenv" s-end-mnemo mod-r/m endof
      7 of .s" fstcw" s-end-mnemo .s" word " mod-r/m endof
      2drop 2- dup .s" fwait"
    endcase
  else
    drop 2- .s" fwait"
  endif
;

: wf2  ( addr -- addr' )
  1+ dis-cfetch
  case
    $e2 of .s" fclex" endof
    $e3 of .s" finit" endof
    swap 2- swap .s" fwait"
  endcase
;

: wf3  ( addr -- addr' )
  1+ dis-cfetch dup 3 rshift 7 and
  case
    6 of .s" fsave" s-end-mnemo mod-r/m endof
    7 of .s" fstsw" s-end-mnemo .s" word " mod-r/m endof
    2drop 2- dup .s" fwait"
  endcase
;

: wf4  ( addr -- addr' )
  1+ dis-cfetch $e0 =
  if
    .s" fstsw" s-end-mnemo .s" ax"
  else
    2- .s" fwait"
  endif
;

: fwaitops  ( addr op -- addr' )
  case
    $d9 of wf1 endof
    $db of wf2 endof
    $dd of wf3 endof
    $df of wf4 endof
    .s" fwait"
  endcase
;

: w8f  ( addr op -- addr' )
  drop dup c@ dup $f8 and $d8 =
  if fwaitops else drop .s" wait" endif
;

: falu1  ( xopcode -- )
  3 rshift 7 and
  S" fadd fmul fcom fcompfsub fsubrfdiv fdivr"
  5 ss. s-end-mnemo
;

: falu5  ( xopcode -- )
  3 rshift 7 and
  s" fadd fmul ???? ???? fsubrfsub fdivrfdiv "
  5 ss. s-end-mnemo
;

: sti.  ( op -- )  7 and .s" st(" 1 .r>s .s" )" ;
\ : sti.  ( op -- )  7 and .s" st" 1 .r>s ;

\ : sti.st   ( op -- )
\        7 and
\        .s" ST(" 1 .r>s .s" )" .s"  ST " ;

: fd8  ( addr opcode -- addr' )
  drop dis-cfetch dup falu1
  dup $c0 <
  if
    .s" float " mod-r/m
  else
    dup $f0 and $d0 =
    if sti. else .s" st," sti. endif
  endif
;

: fdc  ( addr opcode -- addr' )
  drop dis-cfetch
  dup dup $c0 <
  if
    falu1 .s" double " mod-r/m
  else
    falu5 sti. .s" ,st"
  endif
;

: fnullary-f   ( op -- )
  $0f and dup 8 < if
    S" f2xm1  fyl2x  fptan  fpatan fxtractfprem1 fdecstpfincstp"
  else
    8-
    S" fprem  fyl2xp1fsqrt  fsincosfrndintfscale fsin   fcos   "
    endif
  7 ss.
  s-end-mnemo
;

: fnullary-e  ( op -- )
  $0f and dup 8 < if
    S" fchs   fabs   ???    ???    ftst   fxam   ???    ???    "
  else
    8-
    S" fld1   fldl2t fldl2e fldpi  fldlg2 fldln2 fldz   ???    "
  endif
  7 ss.
  s-end-mnemo
;

: fnullary  ( op -- )
  dup $ef > if fnullary-f EXIT endif
  dup $e0 < if
    $d0 = if
      .s" fnop"
    else
      dup ???
    endif
    EXIT
  endif
  fnullary-e
;


\ : falu2   ( op -- )
\        3 rshift 7 and
\        S" fld    ???    fst    fstp   fldenv fldcw  fnstenvfnstcw "
\        7 ss. ;

: fd9  ( addr op -- addr' )
  drop dis-cfetch dup $c0 < if
    dup $38 and
    case
      $00 of .s" fld" s-end-mnemo .s" float " endof
      $10 of .s" fst" s-end-mnemo .s" float " endof
      $18 of .s" fstp" s-end-mnemo .s" float " endof
      $20 of .s" fldenv" s-end-mnemo endof
      $28 of .s" fldcw" s-end-mnemo .s" word " endof
      $30 of .s" fnstenv" s-end-mnemo endof
      $38 of .s" fnstcw" s-end-mnemo .s" word " endof
      dup ??? s-end-mnemo
    endcase
    mod-r/m
  else
    dup $d0 < if
      dup $c8 <
      if .s" fld" else .s" fxch" endif
      s-end-mnemo
      sti.
    else
      fnullary
    endif
  endif
;

: falu3  ( op -- )
  3 rshift 7 and
  S" fiadd fimul ficom ficompfisub fisubrfidiv fidivr"
  6 ss.
  s-end-mnemo
;

: fcmova  ( op -- )
  3 rshift 7 and
  S" fcmovb fcmove fcmovbefcmovu ???    ???    ???    ???    "
  7 ss.
  s-end-mnemo
;

: fda  ( addr op -- )
  drop dis-cfetch dup $c0 <
  if
    dup falu3 .s" dword " mod-r/m
  else
    dup  \ 11-05-2004 Fixed FDA and CMV (Serguei Jidkov)
    $e9 = if
      .s" fucompp" drop
    else
      dup fcmova sti.
    endif
  endif
;

: falu7  ( op -- )
  3 rshift 7 and
  S" faddp fmulp ???   ???   fsubrpfsubp fdivrpfdivp "
  6 ss.
  s-end-mnemo
;

: fde  ( addr op -- addr' )
  drop dis-cfetch dup $c0 < if
    dup falu3 .s" word " mod-r/m
  else
    dup $d9 = if
      .s" fcompp" drop
    else
      dup falu7 sti.
    then
  endif
;

: fcmovb  ( op -- )
  3 rshift 7 and
  S" fcmovnb fcmovne fcmovnbefcmovnu ???     fucomi  fcomi   ???     "
  8 ss.
  s-end-mnemo
;

: fdb  ( addr op -- addr' )
  drop dis-cfetch dup $c0 < if
    dup $38 and case
      $00 of .s" fild" s-end-mnemo .s" dword " endof
      $10 of .s" fist" s-end-mnemo .s" dword " endof
      $18 of .s" fistp" s-end-mnemo .s" dword " endof
      $28 of .s" fld" s-end-mnemo .s" extended " endof
      $38 of .s" fstp" s-end-mnemo .s" extended " endof
      dup ??? s-end-mnemo
    endcase
    mod-r/m
  else
    case
      $e2 of .s" fnclex" endof
      $e3 of .s" fninit" endof
      dup dup fcmovb sti.
    endcase
  endif
;

: falu6  ( op -- )
  3 rshift 7 and
  S" ffree ???   fst   fstp  fucom fucomp???   ???   "
  6 ss.
  s-end-mnemo
;

: fdd  ( addr op -- addr' )
  drop dis-cfetch dup $c0 < if
    dup $38 and
    case
      $00 of .s" fld" s-end-mnemo .s" double "  endof
      $10 of .s" fst" s-end-mnemo .s" double "  endof
      $18 of .s" fstp" s-end-mnemo .s" double "  endof
      $20 of .s" frstor" s-end-mnemo endof
      $30 of .s" fnsave" s-end-mnemo endof
      $38 of .s" fnstsw" s-end-mnemo .s" word " endof
      dup ??? s-end-mnemo
    endcase
    mod-r/m
  else
    dup falu6 sti.
  endif
;

: fdf   ( addr op -- addr' )
  drop dis-cfetch dup $c0 < if
    dup $38 and
    case
      $00 of .s" fild" s-end-mnemo .s" word " endof
      $10 of .s" fist" s-end-mnemo .s" word " endof
      $18 of .s" fistp" s-end-mnemo .s" word " endof
      $20 of .s" fbld" s-end-mnemo .s" tbyte " endof
      $28 of .s" fild" s-end-mnemo .s" qword " endof
      $30 of .s" fbstp" s-end-mnemo .s" tbyte " endof
      $38 of .s" fistp" s-end-mnemo .s" qword " endof
      dup ??? s-end-mnemo
    endcase
    mod-r/m
  else
    dup $e0 = if
      .s" fnstsw" s-end-mnemo .s" ax " drop
    else
      dup $38 and
      case
        $00 of .s" ffreep" s-end-mnemo sti. endof
        $28 of .s" fucomip" s-end-mnemo sti. endof
        $30 of .s" fcomip" s-end-mnemo sti. endof
        ???
      endcase
    endif
  endif
;

: gp6  ( addr op -- addr' )
  drop dis-cfetch dup 3 rshift
  7 and S" sldtstr lldtltr verrverw??? ???" 4 ss.
  s-end-mnemo
  r/m16
;

: gp7  ( addr op -- addr' )
  drop dis-cfetch dup 3 rshift
  7 and dup S" sgdt  sidt  lgdt  lidt  smsw  ???   lmsw  invlpg" 6 ss.
  s-end-mnemo
  4 and 4 = if r/m16 else r/m16/32 then
;

: btx.  ( n -- )
  3 rshift
  3 and S" bt btsbtrbtc" 3 ss.
  s-end-mnemo
;

: gp8  ( addr op -- addr' )
  drop dis-cfetch dup btx.
  r/m16/32 imm8
;

: lar  ( addr op -- addr' )  .s" lar" s-end-mnemo drop r,r/m ;
: lsl  ( addr op -- addr' )  .s" lsl" s-end-mnemo drop r,r/m ;
: lss  ( addr op -- addr' )  .s" lss" s-end-mnemo drop r,r/m ;
: lfs  ( addr op -- addr' )  .s" lfs" s-end-mnemo drop r,r/m ;
: lgs  ( addr op -- addr' )  .s" lgs" s-end-mnemo drop r,r/m ;
: btx  ( addr op -- addr' )  btx. r/m,r ;
: sli  ( addr op -- addr' )  .s" shld" s-end-mnemo drop r/m,r imm8 ;
: sri  ( addr op -- addr' )  .s" shrd" s-end-mnemo drop r/m,r imm8 ;
: slc  ( addr op -- addr' )  .s" shld" s-end-mnemo drop r/m,r .s" ,cl" ;
: src  ( addr op -- addr' )  .s" shrd" s-end-mnemo drop r/m,r .s" ,cl" ;
: iml  ( addr op -- addr' )  .s" imul" s-end-mnemo drop r,r/m ;
: cxc  ( addr op -- addr' )  .s" cmpxchg" s-end-mnemo 1 and to size r/m,r ;

: mvx  ( addr op -- addr' )
  dup 8 and if .s" movsx" else .s" movzx" then
  s-end-mnemo
  1 and >r
  dis-cfetch mod/sib r>  \ size bit
  if
    swap reg32 .,  \ word to dword case
    3 = if reg16
    else
      .s" word "
      DROP DUP 1- C@  \ 26-07-2001 Fixed MVX (Maksimov)
      mod-r/m
    then
  else
    swap reg16/32 .,  \ byte case
    3 = if reg8
    else
      .s" byte "
      DROP DUP 1- C@  \ 26-07-2001 Fixed MVX (Maksimov)
      mod-r/m
    then
  then
;

: xad  ( addr op -- addr' )  .s" xadd" s-end-mnemo 1 and to size r/m,r ;
: bsf  ( addr op -- addr' )  .s" bsf" s-end-mnemo drop r,r/m ;
: bsr  ( addr op -- addr' )  .s" bsr" s-end-mnemo drop r,r/m ;
: cx8  ( addr op -- addr' )  .s" cmpxchg8b" s-end-mnemo drop dis-cfetch r/m16/32 ;
: bsp  ( addr op -- addr' )  .s" bswap" s-end-mnemo reg32 ;

: F6.  ( addr op -- addr' )
\ ??
  >r dis-cfetch
  dup 3 rshift 7 and dup >r S" testXXXXnot neg mul imuldiv idiv" 4 ss.
  s-end-mnemo
  mod-r/m r> 0= if
    r@ 1 and if imm16/32 else imm8 then
  then
  rdrop
;

: FE.  ( addr op -- addr' )
  drop dis-cfetch
  dup 3 rshift 7 and
  case
    0 of .s" inc" endof
    1 of .s" dec" endof
    ???
  endcase
  s-end-mnemo
  r/m8
;

: FF.  ( addr op -- addr' )
  drop dis-cfetch
  dup 3 rshift 7 and
  case
    0 of .s" inc" s-end-mnemo endof
    1 of .s" dec" s-end-mnemo endof
    2 of .s" call" s-end-mnemo endof
    3 of .s" call" s-end-mnemo .s" far " endof
    4 of jmp-jp-mnemo s-end-mnemo endof
    5 of jmp-jp-mnemo s-end-mnemo .s" far " endof
    6 of .s" push " s-end-mnemo endof
    ??? s-end-mnemo
  endcase
  r/m16/32
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; --------------------- conditional move ---------------
;;

: set  ( adr op -- ) .s" set" tttn s-end-mnemo dis-cfetch r/m8 ;
: cmv  ( adr op -- ) .s" cmov" tttn s-end-mnemo ( dis-cfetch ) r,r/m ;  \ 11-05-2004 Fixed FDA and CMV (Serguei Jidkov)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; --------------------- MMX Operations -----------------
;;

: mmx-size  ( op -- )  3 and S" bwdq" 1 ss. ;

: upl  ( adr op -- adr' )  3 and S" punpcklbwpunpcklwdpunpckldq" 9 ss. s-end-mnemo r,r/m ;
: uph  ( adr op -- adr' )  3 and S" punpckhbwpunpckhwdpunpckhdq" 9 ss. s-end-mnemo r,r/m ;
: cgt  ( adr op -- adr' )  .s" pcmpgt" mmx-size s-end-mnemo r,r/m ;
: ceq  ( adr op -- adr' )  .s" pcmpeq" mmx-size s-end-mnemo r,r/m ;

: psh. ( op -- )
  $30 and
  case
    $10 of .s" psrl" endof
    $20 of .s" psra" endof
    $30 of .s" psll" endof
  endcase
;

: gpa  ( adr op -- adr' )  >r dis-cfetch dup psh. r> mmx-size s-end-mnemo mreg imm8 ;
: puw  ( adr op -- adr' )  .s" packusdw" s-end-mnemo drop r,r/m ;
: psb  ( adr op -- adr' )  .s" packsswb" s-end-mnemo drop r,r/m ;
: psw  ( adr op -- adr' )  .s" packssdw" s-end-mnemo drop r,r/m ;

: mpd  ( adr op -- adr' )
  .s" movd" s-end-mnemo
  drop dis-cfetch mod/sib
  swap mreg ., 3 =
  if reg32 else mod-r/m then
;

: mdp  ( adr op -- adr' )
  .s" movd" s-end-mnemo
  drop dis-cfetch mod/sib
  3 =
  if swap reg32 else swap mod-r/m then
  ., mreg
;

: mpq  ( adr op -- adr' )  .s" movq" s-end-mnemo drop r,r/m ;
: mqp  ( adr op -- adr' )  .s" movq" s-end-mnemo drop r/m,r ;
: shx  ( adr op -- adr' )  dup psh. mmx-size s-end-mnemo r,r/m ;
: mll  ( adr op -- adr' )  .s" pmullw" s-end-mnemo drop r,r/m ;
: mlh  ( adr op -- adr' )  .s" pmulhw" s-end-mnemo drop r,r/m ;
: mad  ( adr op -- adr' )  .s" pmaddwd" s-end-mnemo drop r,r/m ;
: sus  ( adr op -- adr' )  .s" psubus" mmx-size s-end-mnemo r,r/m ;
: sbs  ( adr op -- adr' )  .s" psubs" mmx-size s-end-mnemo r,r/m ;
: sub  ( adr op -- adr' )  .s" psub" mmx-size s-end-mnemo r,r/m ;
: aus  ( adr op -- adr' )  .s" paddus" mmx-size s-end-mnemo r,r/m ;
: ads  ( adr op -- adr' )  .s" padds" mmx-size s-end-mnemo r,r/m ;
: add  ( adr op -- adr' )  .s" padd" mmx-size s-end-mnemo r,r/m ;
: pad  ( adr op -- adr' )  .s" pand" s-end-mnemo drop r,r/m ;
: por  ( adr op -- adr' )  .s" por" s-end-mnemo drop r,r/m ;
: pan  ( adr op -- adr' )  .s" pandn" s-end-mnemo drop r,r/m ;
: pxr  ( adr op -- adr' )  .s" pxor" s-end-mnemo drop r,r/m ;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -------------------- Opcode Table --------------------
;;

: ops $10 0 do ' , loop ;

create op-table2

\     0   1   2   3    4   5   6   7    8   9   A   B    C   D   E   F
ops  gp6 gp7 lar lsl  ??? ??? clt ???  inv wiv ??? ud2  ??? ??? ??? ???  \ 0
ops  ??? ??? ??? ???  ??? ??? ??? ???  ??? ??? ??? ???  ??? ??? ??? ???  \ 1
ops  mrc mrd mcr mdr  mrt ??? mtr ???  ??? ??? ??? ???  ??? ??? ??? ???  \ 2
ops  wmr rtc rmr rpc  ??? ??? ??? ???  ??? ??? ??? ???  ??? ??? ??? ???  \ 3

ops  cmv cmv cmv cmv  cmv cmv cmv cmv  cmv cmv cmv cmv  cmv cmv cmv cmv  \ 4
ops  ??? ??? ??? ???  ??? ??? ??? ???  ??? ??? ??? ???  ??? ??? ??? ???  \ 5
ops  upl upl upl puw  cgt cgt cgt psb  uph uph uph psw  ??? ??? mpd mpq  \ 6
ops  ??? gpa gpa gpa  ceq ceq ceq ems  ??? ??? ??? ???  ??? ??? mdp mqp  \ 7

ops  lbr lbr lbr lbr  lbr lbr lbr lbr  lbr lbr lbr lbr  lbr lbr lbr lbr  \ 8
ops  set set set set  set set set set  set set set set  set set set set  \ 9
ops  pss pps cpu btx  sli slc ??? ???  pss pps rsm btx  sri src ??? iml  \ A
ops  cxc cxc lss btx  lfs lgs mvx mvx  ??? ud1 gp8 btx  bsf bsr mvx mvx  \ B

ops  xad xad ??? ???  ??? ??? ??? cx8  bsp bsp bsp bsp  bsp bsp bsp bsp  \ C
ops  ??? shx shx shx  ??? mll ??? ???  sus sus ??? pad  aus aus ??? pan  \ D
ops  ??? shx shx ???  ??? mlh ??? ???  sbs sbs ??? por  ads ads ??? pxr  \ E
ops  ??? ??? shx shx  ??? mad ??? ???  sub sub sub ???  add add add ???  \ F
\     0   1   2   3    4   5   6   7    8   9   A   B    C   D   E   F

: 0F.  ( adr code -- )
  drop dis-cfetch dup
  dup $70 and $50 $80 within to mmx-reg
  cells op-table2 + @execute
  0 to mmx-reg
  0 to xmm-reg
;

create op-table

\     0   1   2   3    4   5   6   7    8   9   A   B    C   D   E   F
ops  alu alu alu alu  ala ala pss pps  alu alu alu alu  ala ala pss 0F.  \ 0
ops  alu alu alu alu  ala ala pss pps  alu alu alu alu  ala ala pss pps  \ 1
ops  alu alu alu alu  ala ala es: daa  alu alu alu alu  ala ala cs: das  \ 2
ops  alu alu alu alu  ala ala ss: aaa  alu alu alu alu  ala ala ds: aas  \ 3

ops  inc inc inc inc  inc inc inc inc  dec dec dec dec  dec dec dec dec  \ 4
ops  psh psh psh psh  psh psh psh psh  pop pop pop pop  pop pop pop pop  \ 5
ops  psa ppa bnd arp  fs: gs: d16 a16  psi mli psi mli  inb isd osb osd  \ 6
ops  bra bra bra bra  bra bra bra bra  bra bra bra bra  bra bra bra bra  \ 7

ops  ali ali ??? ali  txb txb txb txb  mov mov mov mov  mrs lea msr 8F.  \ 8
ops  xga xga xga xga  xga xga xga xga  cbw cdq cis w8f  psf ppf sah lah  \ 9
ops  mv1 mv1 mv2 mv2  mvs mvs cps cps  tst tst sts sts  lds lds scs scs  \ A
ops  mri mri mri mri  mri mri mri mri  mri mri mri mri  mri mri mri mri  \ B

ops  shf shf rtn rtn  lxs lxs mvi mvi  ent lev rtf rtf  nt3 int nto irt  \ C
ops  shf shf shf shf  aam aad ??? xlt  fd8 fd9 fda fdb  fdc fdd fde fdf  \ D
ops  lup lup lup lup  inp inp otp otp  jsr jmp cis jmp  ind ind otd otd  \ E
ops  lok ??? rpz rep  hlt cmc F6. F6.  clc stc cli sti  cld std FE. FF.  \ F
\     0   1   2   3    4   5   6   7    8   9   A   B    C   D   E   F


\ -------------------------- SSE2 Operations -------------------------------

\ -- swap reg fields in mod-r/m byte
: swap-regs ( u1 -- u2 )
  \ LOCALS| mod-r/m |
  >r
  r@ ( mod-r/m ) 7 and 3 lshift
  r@ ( mod-r/m ) 3 rshift 7 and  or
  r@ ( mod-r/m ) $C0 and or
  rdrop
;

: ?swap-regs  ( u1 -- u2 )  DUP $C0 AND $C0 = IF swap-regs ENDIF ;

: modbyte  ( mod-r-r/m -- r/m r mod )   ( r including general, special, segment, MMX )
           ( mod-op-r/m -- r/m op mod )
  0xff AND 8 /MOD 8 /MOD
;

(* defined above
: mod-r/m ( addr modr/m -- addr' )
  modbyte NIP ( [mod-r-r/m] -- r/m mod )
  dis.default-16bit? if mod-r/m16 else mod-r/m32 then
;
*)

\ : ::imm8        ( addr -- addr' ) ., dis-cfetch h.>s  ;
: stab          ( pos - )         s-buf c@ - 1 max sspaces ;
: rstab         ( -- )            0x8 stab ;
: R:sse2        ( -- )            true TO xmm-reg  false TO mmx-reg ;
: R:reg         ( a n -- a )      7 and reg ;
: 0f-prefix?    ( adr -- adr' flag ) dup 1+ c@ 0xF = ;

: xm-r/m,r      ( addr -- addr' ) rstab R:sse2 r/m,r ;

: xm-r,r/m      ( addr -- addr' ) rstab R:sse2 r,r/m ;

: r32/m,xmmr    ( addr -- addr' )                             \ the register is always XMM
                rstab dis-cfetch ?swap-regs
                true to xmm-reg
                dup >r mod-r/m ., r> 3 rshift R:reg ;

: xmmr,r32/m    ( addr -- addr' )                             \ dest register is XMM
                rstab true  to xmm-reg dis-cfetch dup 3 rshift R:reg
                ., false to xmm-reg  r/m16/32 ;

: r,xmm         ( addr -- addr' )                              \ 1st=r32 2nd=XMM
                rstab false to xmm-reg dis-cfetch dup 3 rshift
                reg32 ., .xmmreg ;

: .cmp-sse ( adr -- adr' )
    dup 1+ dis-c@
     case
            0 of .s" cmpeq"    endof
            1 of .s" cmplt"    endof
            2 of .s" cmple"    endof
            3 of .s" cmpunord" endof
            4 of .s" cmpneq"   endof
            5 of .s" cmpnlt"   endof
            6 of .s" cmpnle"   endof
            7 of .s" cmpord"   endof
     endcase ;

: dis-cmpps ( adr -- adr' ) .cmp-sse .s" ps" xm-r,r/m 1+ ;
: dis-cmpss ( adr -- adr' ) .cmp-sse .s" ss" xm-r,r/m 1+ ;
: dis-cmppd ( adr -- adr' ) .cmp-sse .s" pd" xm-r,r/m 1+ ;
: dis-cmpsd ( adr -- adr' ) .cmp-sse .s" sd" xm-r,r/m 1+ ;

: save-adr       ( adr flag -- flag adr adr  )   true swap dup  ;
: restore-adr  ( true adr adr1 --  false adr adr adr )
   2drop nip false swap dup dup ;

: get-adrfl ( flag adr adr'  -- adrfl flag )
   rot
     if    nip true
     else  drop false
     then ;

: ?dis-3Aext  ( adr -- adr' )
    dis-cfetch
        case
          $40 of .s" dpps" xm-r,r/m  imm8  endof
          $41 of .s" dppd" xm-r,r/m  imm8  endof
        endcase
 ;

: ?dis-660f ( adr flag -- adr' flag )
   if save-adr 2 + dis-cfetch
      case
          $10 of .s" movupd"    xm-r,r/m        endof
          $11 of .s" movupd"    r32/m,xmmr      endof
          $12 of .s" movlpd"    xm-r,r/m        endof
          $13 of .s" movlpd"    r32/m,xmmr      endof
          $14 of .s" unpcklpd"  xmmr,r32/m      endof
          $15 of .s" unpckhpd"  xmmr,r32/m      endof
          $16 of .s" movhpd"    xm-r,r/m        endof
          $17 of .s" movhpd"    r32/m,xmmr      endof
          $28 of .s" movapd"    xm-r,r/m        endof
          $29 of .s" movapd"    r32/m,xmmr      endof
          $2e of .s" ucomisd"   xm-r,r/m        endof
          $2f of .s" comisd"    xm-r,r/m        endof
          $3a of  ?dis-3Aext                    endof
          $51 of .s" sqrtpd"    xm-r,r/m        endof
          $54 of .s" sqrtpd"    xm-r,r/m        endof
          $54 of .s" andpd"     xm-r,r/m        endof
          $55 of .s" andnpd"    xm-r,r/m        endof
          $56 of .s" orpd"      xm-r,r/m        endof
          $57 of .s" xorpd"     xm-r,r/m        endof
          $58 of .s" addpd"     xm-r,r/m        endof
          $59 of .s" mulpd"     xm-r,r/m        endof
          $5a of .s" cvtps2ps"  xm-r,r/m        endof
          $5b of .s" cvtps2dq"  xm-r,r/m        endof
          $5c of .s" subpd"     xm-r,r/m        endof
          $5d of .s" minpd"     xm-r,r/m        endof
          $5e of .s" divpd"     xm-r,r/m        endof
          $5f of .s" maxpd"     xm-r,r/m        endof
          $6e of .s" movd"      xm-r,r/m        endof
          $7e of .s" movd"      r32/m,xmmr      endof
          $6f of .s" movqda"    xmmr,r32/m      endof
          $7f of .s" movqda"    r32/m,xmmr      endof
          $c2 of  dis-cmppd                     endof
          $c6 of .s" shufpd"  xm-r,r/m  imm8  endof
          $d7 of .s" pmovmskb"  r,xmm           endof
                 restore-adr
        endcase  get-adrfl
   else   false    \ no 66 0f
   then ;

: ?dis-0f ( adr flag -- adr' flag )
   if  true swap 1+ dis-cfetch
      case
          $10 of .s" movups"    xm-r,r/m        endof
          $11 of .s" movups"    r32/m,xmmr      endof
          $14 of .s" unpcklps"  xmmr,r32/m      endof
          $15 of .s" unpckhps"  xmmr,r32/m      endof
          $28 of .s" movaps"    xm-r,r/m        endof
          $29 of .s" movaps"    r32/m,xmmr      endof
          $2a of .s" movaps"    xmmr,r32/m      endof
          $2e of .s" ucomisd"   xm-r,r/m        endof
          $2f of .s" comiss"    xm-r,r/m        endof
          $51 of .s" sqrtps"    xm-r,r/m        endof
          $52 of .s" rsqrtps"   xm-r,r/m        endof
          $53 of .s" rcpps"     xm-r,r/m        endof
          $54 of .s" andps"     xm-r,r/m        endof
          $55 of .s" andnps"    xm-r,r/m        endof
          $56 of .s" orps"      xm-r,r/m        endof
          $57 of .s" xorps"     xm-r,r/m        endof
          $58 of .s" addps"     xm-r,r/m        endof
          $59 of .s" mulps"     xm-r,r/m        endof
          $5a of .s" cvtps2pd"  xm-r,r/m        endof
          $5b of .s" cvtdq2ps"  xm-r,r/m        endof
          $5c of .s" subps"     xm-r,r/m        endof
          $5d of .s" minps"     xm-r,r/m        endof
          $5e of .s" divps"     xm-r,r/m        endof
          $5f of .s" maxps"     xm-r,r/m        endof
          $c2 of  dis-cmpps                     endof
          $c6 of .s" shufps"  xm-r,r/m    imm8  endof
              rot drop false nrot
        endcase   swap
   else   false    \ no 0f
   then ;

: ?dis-f20f ( adr flag -- adr' flag )
   if  save-adr 2 + dis-cfetch
      case
          $10 of .s" movsd"     xm-r,r/m        endof
          $11 of .s" movsd"     xm-r/m,r        endof
          $2a of .s" cvtsi2sd"  xmmr,r32/m      endof
          $51 of .s" sqrtsd"    xm-r,r/m        endof
          $52 of .s" rsqrtsd"   xm-r,r/m        endof
          $58 of .s" addsd"     xm-r,r/m        endof
          $59 of .s" mulsd"     xm-r,r/m        endof
          $5a of .s" cvtsd2ss"  xm-r,r/m        endof
          $5c of .s" subsd"     xm-r,r/m        endof
          $5d of .s" minsd"     xm-r,r/m        endof
          $5e of .s" divsd"     xm-r,r/m        endof
          $5f of .s" maxsd"     xm-r,r/m        endof
          $c2 of  dis-cmpsd                     endof
          $e6 of .s" cvtpd2dq"  xmmr,r32/m      endof
                 restore-adr
      endcase    get-adrfl
   else  false    \ no f2 0f
   then ;

: ?dis-f30f ( adr flag -- adr' flag )
   if  save-adr 2 + dis-cfetch \ f a0 a1
      case
        $10 of .s" movss"     xm-r,r/m endof
        $11 of .s" movss"     xm-r/m,r endof
        $2a of .s" cvtsi2ss"  xmmr,r32/m endof
        $51 of .s" sqrtss"    xm-r,r/m endof
        $52 of .s" rsqrtss"   xm-r,r/m endof
        $53 of .s" rcpss"     xm-r,r/m endof
        $58 of .s" addss"     xm-r,r/m endof
        $59 of .s" mulss"     xm-r,r/m endof
        $5a of .s" cvtss2sd"  xm-r,r/m endof
        $5b of .s" cvttps2dq" xm-r/m,r endof
        $5c of .s" subss"     xm-r,r/m endof
        $5d of .s" minss"     xm-r,r/m endof
        $5e of .s" divss"     xm-r,r/m endof
        $5f of .s" maxss"     xm-r,r/m endof
        $6f of .s" movdqu"    xm-r,r/m endof
        $7f of .s" movdqu"    r32/m,xmmr endof
        $c2 of dis-cmpss endof
        $e6 of .s" cvtdq2pd"  xmmr,r32/m endof
                 restore-adr
        endcase  get-adrfl
   else    false    \ no f3 0f
   then ;


: pf-coded? ( adr -- adr' flag )
   dup c@
      case
        0x66 of 0f-prefix? ?dis-660f endof
        0xf2 of 0f-prefix? ?dis-f20f endof
        0xf3 of 0f-prefix? ?dis-f30f endof
      false swap
      endcase ;

: prefix-coded? ( adr -- adr' flag )
   pf-coded?
     if   true
     else dup dup c@ 0xf = ?dis-0f
              if    rot drop true
              else  drop false
              then
     then ;

\ ------------------- END OF SSE2 Operations -------------------------------


: op-2byte 0xffff and , ' , ;

: inh-sized   ( -<name>- )
  create
  bl word
  count cell+ allot drop
  bl word
  count cell+ allot drop
  does>
    count
    default-16bit? if + count endif
    >s
    drop \ instruction code
    \ s-end-mnemo
;


inh-sized cwd    cwd    cdw
inh-sized cbwx   cbw    cwb
inh-sized cmpsx  cmpsw  cmpsd
inh-sized lodsx  lodsw  lodsd
inh-sized movsx  movsw  movsd
inh-sized stosx  stosw  stosd
inh-sized scasx  scasw  scasd
inh-sized insx   insw   insw
inh-sized outsx  outsw  outsd
inh-sized popax  popa   popad
inh-sized pushax pusha  pushad
inh-sized popfx  popf   popfd
inh-sized pushfx pushf  pushfd
inh-sized iretx  iretw  iret

\ FIXME: 16-bit is broken
create 2byte-oplist
0x9966 op-2byte cwd
0x9866 op-2byte cbwx
0xA566 op-2byte movsx
0xA766 op-2byte cmpsx
0xAB66 op-2byte stosx
0xAF66 op-2byte scasx
0xAD66 op-2byte lodsx
0x6D66 op-2byte insx
0x6F66 op-2byte outsx
0x6166 op-2byte popax
0x6066 op-2byte pushax
0x9D66 op-2byte popfx
0x9C66 op-2byte pushfx
0xCF66 op-2byte iretx
0 ,

: (dis-op-init-flags)  ( -- )
  false to data-size-prefix
  false to disp-as-reg-offset
  false to mmx-reg
  false to xmm-reg
;

: (dis-op-done-flags)  ( -- )
  prefix-op ifnot
    default-16bit? dup to 16-bit-data to 16-bit-addr
    false to prefix-seg
    false to prefix-op-a16-d16
  endif
;

: (dis-known-wtable)  ( addr -- addr' true // addr false )
  dis-wpeek 2byte-oplist ( addr wp tbladdr )
  begin 2dup w@ - while dup w@ while 2 +cells repeat
  nip dup w@ if
    \ dis-wpeek bswap-word 1 and to size
    cell+ @ 0 swap execute 2+ true
  else
    drop false
  endif
;

: (dis-op-intr)  ( adr -- adr' )
  0>s
  false to prefix-op  \ SMuB
  (dis-known-wtable) ifnot
    dis-cfetch
    dup 1 and to size
    dup cells op-table +  @execute
  endif
;

: dis-op  ( adr -- adr' )
  (dis-op-init-flags)
  6 for
    (dis-op-intr) prefix-op ifnot break endif
    prefix-seg prefix-op-a16-d16 or ifnot break endif
  endfor
  (dis-op-done-flags)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level stuff
;;

: .s-buf  ( -- )  s-buf bcount type ;

0 value next-inst

;; on error, "next-inst" is unchanged
: (.one-inst)  ( -- errorflag )
  next-inst   ;; addr
  dup dis-op  ;; addr eaddr
  over base-addr - .hex8 space  ;; type address
  swap 2dup - 16 u> if 2drop true endif
  over to next-inst  ;; update next instruction address
  2dup do i c@ .hex2 space loop
  - 12 swap - 3 * spaces
  s-buf bcount type
  false
;

: .inst  ( adr -- adr' )
  to next-inst
  (.one-inst) if 666 throw endif
  next-inst
;


\ @@@ BH fixed bugs in dis-xx


\ : dis-db ( adr -- adr' )  0>s .s" db " dis-cfetch h.>s .s-buf ;
\ : dis-dw ( adr -- adr' )  0>s .s" dw " W@+ h.>s .s-buf ;
\ : dis-dd ( adr -- adr' )  0>s .s" dd " @+ h.>s .s-buf ;
\ : dis-ds ( adr -- adr' )  0>s .s" string " $22 emit>s bcount 2dup >s + $22 emit>s .s-buf ;

: disasm-one  ( adr -- adr' )
  disx86:.inst cr
;

previous definitions


: disasm-one  ( adr -- adr' )
  disx86:.inst
;

: disasm-word  ( cfa -- )
  dup word-type?
  ;; ( cfa type )
  case
    word-type-forth of  ." forth word" cr endof
    word-type-const of  ." constant" cr endof
    word-type-var   of  ." variable" cr endof
    word-type-value of  ." value" cr endof
    word-type-defer of  ." defer" cr endof
    word-type-does  of  ." doer" cr endof
    \ looks like a code word
    ;; ( cfa type )
    endcr ." === DISASM FOR '" over cfa->nfa id. ." '===\n"  ;; '
    ;; ( cfa type )
    over dup word-code-end
    ;; ( cfa type cfa endaddr )
    ?dup if
      ;; ( cfa type cfa endaddr )
      swap to disx86:next-inst
      begin
        ;; ( cfa type codeend )
        disx86:next-inst over u<
      while
        disx86:(.one-inst) cr
        if 666 throw endif
      repeat
      ;; ( cfa type codeend )
    endif
    drop
    endcr ." -------------------\n"
  endcase
  drop
;
