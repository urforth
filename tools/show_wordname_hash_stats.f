: bucket-count  ( -- bkcount )  1 wlist-hash-bits lshift ;

0 var word-count
0 value buckets

: count-bucket-items  ( bkptr -- )
  0 swap
  begin
    @ ?dup
  while
    swap 1+ swap
  repeat
;

: show-stats  ( vocid -- )
  dup vocid->htable to buckets
  dup vocid-hashed? ifnot 0 to buckets endif

  endcr
  ." VOC: " dup vocid.

  word-count 0!
  [: drop word-count 1+! false ;] foreach-word drop

  ."  -- " word-count @ . ." words, "

  buckets if
    0 0x7fff_ffff  \ max and min in bucket
    0  \ buckets used
    bucket-count 0 do
      i cells buckets + count-bucket-items
      ?dup if
        >r
        1+  \ update total used
        rot r@ max
        rot r@ min
        rot
        rdrop
      endif
    loop
    ;; ( bkmin bkmax bkused )
    nrot 2dup 2>r rot  \ for average
    0 .r ." /" bucket-count . ." buckets, " . ." min, " . ." max, average per bucket: "
    \ show average
    2r> + 2/ 0 .r cr
  else
    ." no hashtable\n"
  endif
;

: all-vocs  ( -- )
  [: ( vocid -- endflag )
    show-stats false
  ;] foreach-voc drop
;

all-vocs

(*
vocid-of forth show-stats
[IFDEF] c4s vocid-of c4s show-stats [ENDIF]
[IFDEF] prng vocid-of prng show-stats [ENDIF]
[IFDEF] disx86 vocid-of disx86 show-stats [ENDIF]
[IFDEF] asmx86 vocid-of asmx86 show-stats [ENDIF]
[IFDEF] asmx86:lexer vocid-of asmx86:lexer show-stats [ENDIF]
[IFDEF] asmx86:instructions vocid-of asmx86:instructions show-stats [ENDIF]
[IFDEF] asm-labman vocid-of asm-labman show-stats [ENDIF]
[IFDEF] asm-meta vocid-of asm-meta show-stats [ENDIF]
*)

bye
