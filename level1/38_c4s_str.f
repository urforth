;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary-nohash C4S
voc-set-active C4S

;; terminate c4s with zero (but don't increment length)
: zterm  ( addr -- )
  count + 0c!
;

;; copy c4s
: copy  ( addrsrc addrdest -- )
  >r dup count-only cell+ r> swap 0 max cmove
;

;; copy counted string to c4s
: copy-counted  ( addrsrc count addrdest -- )
  swap 0 max swap 2dup !
  cell+ swap cmove
;
alias copy-counted copy-a-c

;; cat counted string to c4s
: cat-counted  ( addr count addrdest -- )
  over +if
    dup >r count + swap dup >r 0 max cmove
    r> r> +!
  else 2drop drop endif
;
alias cat-counted cat-a-c

;; cat c4s to another c4s
: cat  ( addrsrc addrdest -- )
  swap count rot cat-counted
;

;; append char to c4s
: cat-char  ( char addr -- )
  dup >r count + c! r> 1+!
;

;; append slash to c4s (but only if it doesn't end with slash, and is not empty)
;; useful for path manipulation
: add-slash  ( addr -- )
  dup count-only +if
    dup count + dup 1- c@ [char] / = ifnot [char] / swap c! 1+! else 2drop endif
  else drop endif
;

: skip-aligned  ( addr -- nextaddr )  count + 3 + -4 and ;
: skip-alignedz  ( addr -- nextaddr )  count + 4+ -4 and ;

voc-set-active FORTH
