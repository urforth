;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$uservar "(EXC-FRAME-PTR)" ua_ofs_exc_frame_ptr 0
(hidden)

;; reset all exception frames
: (EXC0!)  ( -- )  ... (exc-frame-ptr) 0! ; (hidden)

;; this should save all necessary data to the return stack
;; the format is: ( ...data restorecfa )
;;   restorecfa:  ( restoreflag -- )
;;   if "restoreflag" is 0, drop the data
: (CATCH-SAVER)  ( -- )  ... ; (hidden)


: CATCH  ( i * x xt -- j * x 0 | i * x n )
  ;; this is using return stack to hold previous catch frame
  ;; of course, this prevents Very Smart return stack manipulation, but idc (for now)
  ;; exception frame consists of:
  ;;   return-to-catch EIP (return stack TOS)
  ;;   sp (frame points here)
  ;;   prev_self
  ;;   prev_frame_ptr
  ;;   return-to-catch-caller EIP
  ;; create exception frame
  (exc-frame-ptr) @ >r
  ;; section to save various custom data
  0 >r (catch-saver)
  ;;(self@) >r
  ;;(locptr@) >r
  ;; custom data section end
  sp@ >r
  rp@ (exc-frame-ptr) !  ;; update exception frame pointer
  execute  ;; and execute
  ;; we will return here only if no exception was thrown
  rdrop begin r> ?dup while false swap execute repeat
  ;;3 nrdrop  ;; drop spdepth, locptr, self
  r> (exc-frame-ptr) !   ;; restore previous exception frame
  0        ;; exception code (none)
;


: THROW  ( k * x n -- k * x | i * x n )
  ?dup if
    ;; check if we have exception frame set
    (exc-frame-ptr) @ ?dup ifnot
      ;; panic!
      (exc0!)
      state 0!  ;; just in case
      fatal-error  ;; err-throw-without-catch (error)
      1 n-bye ;; just in case
    endif
    ;; check if return stack is not exhausted
    rp@ cell- over u> if
      ;; panic!
      (exc0!)
      state 0!  ;; just in case
      err-throw-chain-corrupted fatal-error
      1 n-bye ;; just in case
    endif
    rp!  ;; restore return stack
    r> swap >r  ;; exchange return stack top and data stack top (save exception code, and pop sp to data stack)
    ;; blindly restore data stack (let's hope it is not too badly trashed)
    sp! drop  ;; drop the thing that was CFA
    r>  ;; restore exception code
    ;; restore custom data
    begin r> ?dup while true swap execute repeat
    ;;r> (locptr!)
    ;;r> (self!)
    ;; restore previous exception frame
    r> (exc-frame-ptr) !
    ;; now EXIT will return to CATCH caller
  endif
;
