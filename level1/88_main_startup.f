;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; index of the next CLI arg to process
;; set to 0, negative or ARGC to stop further processing
$value "CLI-ARG-NEXT" 1

: cli-arg-skip  ( -- )  1 +to cli-arg-next ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; environment vars

(*
: (dump-env)  ( -- )
  envp @ begin dup c@ while dup type-asciiz cr zcount + 1+ repeat drop
; (hidden)
*)


: get-env  ( addr count -- addr count true // false )
  dup +if
    envp @
    begin dup c@ while
      >r 2dup r@ zcount [char] = str-trim-at-char s=ci if
        2drop r> zcount [char] = str-skip-after-char true exit
      endif
      r> zcount + 1+
    repeat drop
  endif
  2drop false
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: argv-str  ( argnum -- addr count )
  dup 0 argc within if argv cells^ @ zcount else drop NullString endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; filename is in tloader:fnpad
: (cold-try-load-file)  ( -- successflag )
    \ endcr ." ::: trying: " tloader:fnpad count type cr
  tloader:fnpad-file? ifnot false exit endif
  tloader:fnpad count os:o-rdonly 0 os:open dup -if drop false exit endif
  tload-verbose >r
  tload-verbose-rc to tload-verbose ;; be silent
  ['] tloader:tload-fd catch
  r> to tload-verbose
  throw
  true
    \ ." loaded!\n"
; (hidden)


;; load "<binname>.rc" (if present)
: (cold-load-rc)  ( -- ??? )
  true tloader:build-liblist-name (cold-try-load-file) drop   ;; try list at home dir
  false tloader:build-liblist-name (cold-try-load-file) drop  ;; try list at binary dir
  ;; check for "--naked"
  argc 1 > if
    argc 1 do
      \ TODO: create special dictionary for cli args, and simply execute them
      i argv-str " --naked" s=ci if unloop exit endif
      1  ;; argskip
      i argv-str " --verbose-libs" s=ci if 1 to tload-verbose-libs endif
      i argv-str " --quiet-libs" s=ci if 0 to tload-verbose-libs endif
      i argv-str " --verbose-rc" s=ci if 1 to tload-verbose-rc endif
      i argv-str " --quiet-rc" s=ci if 0 to tload-verbose-rc endif
      i argv-str " --eval" s=ci if drop 2 endif
      i argv-str " -e" s=ci if drop 2 endif
    +loop
  endif
  ;; no "--naked", load .rc
  tloader:build-rc-name (cold-try-load-file) drop
; (hidden)


;; process CLI args
: (cold-cli-args)  ( -- ??? )
  ;; don't use DO ... LOOP here, use CLI-ARG-NEXT
  begin cli-arg-next 1 argc within while
    ;; empty arg?
    cli-arg-next argv-str cli-arg-skip
    ?dup ifnot drop
    else
      ( addr count )
      ;; "-..." args?
      over c@ [char] - = if
        ;; "-e" or "--eval" ?
        2dup " -e" s=ci nrot " --eval" s=ci or
        ;; note that argv is dropped here
        if  ;; set TIB and eval
          cli-arg-next argv-str cli-arg-skip evaluate
        endif
        ;; unknown "-..." args are skipped
      else tload endif
    endif
  repeat
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (urversion->pad)  ( -- addr count )
  base @ >r decimal
  " UrForth level " pad c4s:copy-counted
  urforth-level <#u bl hold #s #> pad c4s:cat-counted
  " v" pad c4s:cat-counted
  urforth-version 24 rshift 0xff and <#u [char] . hold #s #> pad c4s:cat-counted
  urforth-version 16 rshift 0xff and <#u [char] . hold #s #> pad c4s:cat-counted
  urforth-version 0x7fff and <#u #s #> pad c4s:cat-counted
  urforth-version 0x8000 and if " -beta" pad c4s:cat-counted endif
  r> base !
  pad count
;

: (host-os-name)  ( -- addr count )
  $if URFORTH_OS = URFORTH_LINUX_X86
    " Linux/x86"
  $else
    $if URFORTH_OS = URFORTH_WIN32
      " Win32"
    $else
      $error "unknown OS!"
    $endif
  $endif
;

: (.os)  ( -- )  40 emit (host-os-name) type 41 emit ;
: (.version)  ( -- )  (urversion->pad) type ;
: (.banner)  ( -- )  endcr (.version) space (.os) cr ; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called to print a banner after processing CLI args
$defer ".BANNER" cfa "(.banner)"

;; called (once!) to load .rc file
$defer "INIT-LOAD-RC" cfa "(cold-load-rc)"

;; called (once!) to process CLI args
$defer "PROCESS-CLI-ARGS" cfa "(cold-cli-args)"

;; main program loop, should never return
$defer "MAIN-LOOP" cfa "quit"  ;; this word should never return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: COLD  ( ??? )
  abort-cleanup
  .banner
  main-loop
  ;; just in case it returns
  bye
; (noreturn)

: (COLD-FIRSTTIME)  ( ??? )
  sp0! rp0!
  tib-allocate-default
  pad-allocate
  1 to cli-arg-next
  (abort-cleanup-min)
  (startup-init)
  abort-cleanup
  init-load-rc
  process-cli-args
  here (dp-protect)  ;; why not
  cold
  ;; just in case it returns
  bye
; (hidden) (noreturn)
