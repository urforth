;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: :  ;; yay, colon of colon! ;-)
  compiler:?exec
  (dbginfo-reset)
  create-header
  compiler:start-compile-forth-word
  compiler:(ctlid-colon)
;

(* not decided yet
: macro:
  compiler:?exec
  (dbginfo-reset)
  create-header
  compiler:start-compile-forth-word
  (set-macro)
  compiler:(ctlid-colon)
;
*)

;; lol
: (:NONAME-NAMED)  ( addr count -- )
  compiler:?exec
  0 max-word-name-length 1- clamp
  dup if  ;; copy to pad, prepend zero byte
    (create-header-buf) 0!
    0 (create-header-buf) c1s:cat-char
    (create-header-buf) c1s:cat-counted
    (create-header-buf) bcount pad swap dup >r cmove pad r>
  endif
  (create-header-nocheck) (hidden)
  compiler:start-compile-forth-word
  latest-cfa
  compiler:(ctlid-colon)
;

: :NONAME  NullString (:NONAME-NAMED) ;
: :NONAME(  [char] ) parse (:NONAME-NAMED) ;

: ;  ;; and semicolon ;-)
  compiler:?comp
  compiler:(ctlid-colon) compiler:(ctlid-does) compiler:?any-pair
  compile exit
  compiler:end-compile-forth-word
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DOES>!)  ( execcfa wordcfa -- )
  $if URFORTH_ALIGN_PFA
  align-here
  $endif
  here swap compiler:(call!)  ;; replace CFA CALL with the call to the code we'll build
  ;; build machine code:
  0xb8 c, ,                ;; mov  eax,value
  0x87 c, 0x0c c, 0x24 c,  ;; xchg ecx,[esp]
  $if URFORTH_ALIGN_PFA
  0x83 c, 0xc1 c, 0x03 c,  ;; add ecx,3
  $endif
  0xff c, 0xe0 c,          ;; jmp  eax
; (hidden)

: (DOES>)  ( wordcfa -- )
  $if URFORTH_ALIGN_PFA
  align-here
  $endif
  here swap compiler:(call!)  ;; replace CFA CALL with the call to the code we'll build
  ;; build machine code:
  ;;   mov   eax,value
  ;;   jmp   fword_par_urforth_nocall_dodoes
  0xb8 c, ;; mov  eax,...
  r> ,    ;;   value
  (URFORTH-DODOES-ADDR) compiler:(jmp,)
  create;
; (hidden)

: DOES>  ( -- pfa )
  compiler:?comp compiler:?non-macro
  \ compiler:(ctlid-colon) compiler:?pairs
  compiler:(ctlid-colon) compiler:(ctlid-does) compiler:?any-pair
  compile latest-cfa
  compile (does>)  ;; and compile the real word
  optimiser:optimise-jumps
  $if URFORTH_ALIGN_PFA
  check-align-here
  $endif
  optimiser:jstack-init
  compiler:(ctlid-does)
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: [:
  state @ if
    ;; compiling
    compiler:?non-macro
    compiler:(CTLID-CBLOCK)
  else
    ;; interpreting, use temporary dp
    dp-temp @ err-temp-here-already ?error
    (setup-dp-temp)  ;; compile to temporary area
    " (TEMP-INTERPRET-CBLOCK)" (create-temp-header-nocheck)  ;; create temp word header, because why not?
    [compile] ]
    dp-temp @ 2 +cells  ;; slip "litcblock" and its arg
    compiler:(CTLID-CBLOCK-INTERP)
  endif
  compiler:cblock-start swap
  $if URFORTH_ALIGN_PFA
  check-align-here
  $endif
; immediate

: ;]
  compiler:?comp
  compiler:(CTLID-CBLOCK-INTERP) compiler:?pair if
    ;; used from the interpreter
    compiler:cblock-end
    state 0!
    dp-temp-reset
  else
    ;; inside a word
    compiler:(CTLID-CBLOCK) compiler:?pairs
    compiler:cblock-end
  endif
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (NOTIMPL)  ( -- )
  err-not-implemented error
  ;; just in case it returns
  1 n-bye
; (hidden) (noreturn)


: (CREATE-VALUE-STR)  ( n addr count -- )
  create-header-named (URFORTH-DOVALUE-ADDR) compiler:(cfa-call,) , create; smudge
;

: VALUE  ( n -- )
  depth ERR-STACK-UNDERFLOW not-?error
  create-header (URFORTH-DOVALUE-ADDR) compiler:(cfa-call,) , create; smudge
;

: VALUE^  ( cfa -- valaddr )  cfa->pfa ;

: (DEFER)  ( cfa -- )
  create-header (URFORTH-DODEFER-ADDR) compiler:(cfa-call,) , create; smudge
;

: DEFERED  ( cfa -- )
  depth ERR-STACK-UNDERFLOW not-?error
  (defer)
;

: DEFER  ( -- )
  depth 0< ERR-STACK-UNDERFLOW ?error
  ['] noop (defer)
;

: DEFER-NOT-YET  ( -- )
  depth 0< ERR-STACK-UNDERFLOW ?error
  ['] (NOTIMPL) (defer)
;


: (DEFER?)  ( cfa -- cfa true // false )
  dup c@ 0xe8 = ifnot drop false exit endif
  dup 1+ compiler:(disp32@)
  dup (URFORTH-DODEFER-ADDR) =
  swap (URFORTH-DOVALUE-ADDR) = or
  dup ifnot nip endif
; (hidden)

: (defer-check)  ( defercfa -- defercfa )
  (defer?) ERR-NOT-DEFER not-?error
;


$constant "(xto-type:to)" 0
(hidden)
$constant "(xto-type:+to)" 1
(hidden)
$constant "(xto-type:-to)" -1
(hidden)
$constant "(xto-type:to^)" 666
(hidden)

: (value-xto-type->cfa)  ( type -- memwordcfa )
  case
    0 of ['] ! endof
    666 of 0 endof
    0 <of ['] -! endof
    0 >of ['] +! endof
    err-bad-reladdr error ;; it doesn't matter
  endcase
; (hidden)

;; augment this with your "to" processors
;; your processor should either don't change the stack, or
;; consume all 4 values, put "true", and perform "exit"
: value-xto-schook  ( value type addr count -- true // value type addr count false )  ... false ;

: (value-do-to)  ( value type addr count -- )
  value-xto-schook ifnot xfind-required-ex drop (defer-check)
    state @ if
      swap case
        0 of compile LITTO! endof
        666 of compile LIT^TO endof
        0 <of compile LIT-TO! endof
        0 >of compile LIT+TO! endof
        err-bad-reladdr error ;; it doesn't matter
      endcase reladdr,
    else cfa->pfa swap (value-xto-type->cfa) execute-tail endif
  endif
;

: to  ( value -- )  (( name ))  0 parse-name (value-do-to) ; immediate
: +to  ( value -- )  (( name ))  1 parse-name (value-do-to) ; immediate
: -to  ( value -- )  (( name ))  -1 parse-name (value-do-to) ; immediate
: to^  ( -- dataaddr )  (( name ))  666 parse-name (value-do-to) ; immediate

alias TO IS


: DEFER-@  ( -- dataaddr )
  -find-required (defer-check)
  state @ if compile lit^to reladdr, compile @ else cfa->pfa @ endif
; immediate


: DEFER@  ( defercfa -- cfa )
  state @ if compile (defer-check) compile cfa->pfa compile @
  else (defer-check) cfa->pfa @ endif
; immediate

: DEFER!  ( cfa defercfa -- )
  state @ if compile (defer-check) compile cfa->pfa compile !
  else (defer-check) cfa->pfa ! endif
; immediate

alias DEFER-@ ACTION-OF


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: BUFFER:  ( size -- )  \ name
  depth ERR-STACK-UNDERFLOW not-?error
  create smudge allot create; smudge
;

: VAR  ( n -- )  \ name
  depth ERR-STACK-UNDERFLOW not-?error
  \ create smudge , create; smudge
  create , immediate does> [compile] literal
;

: VARIABLE  ( -- )  \ name
  depth 0< ERR-STACK-UNDERFLOW ?error
  0 var
;

: 2VARIABLE  ( -- )  \ name
  depth 0< ERR-STACK-UNDERFLOW ?error
  create smudge 0 , 0 , create; smudge
;

: 2CONSTANT  ( -- )  \ name
  depth 2 < ERR-STACK-UNDERFLOW ?error
  create smudge [ 2 cells ] literal n-allot 2! create; smudge
 does> 2@
;

: 2CONSTANT-LE  ( -- )  \ name
  depth 2 < ERR-STACK-UNDERFLOW ?error
  create smudge [ 2 cells ] literal n-allot 2!le create; smudge
 does> 2@le
;

$if URFORTH_TLS_TYPE
: USERVAR  ( n -- )
  depth ERR-STACK-UNDERFLOW not-?error
  (user-area-used) @ dup cell+ (user-area-max-size) u> ERR-OUT-OF-USER-AREA ?error
  create-header (URFORTH-DOUSERVAR-ADDR) compiler:(cfa-call,) dup , create; smudge
  2dup (user-area-default) + !
  (tls-base-addr) + !
  cell (user-area-used) +!
;

: USERALLOT  ( n -- )
  dup (user-area-max-size) u> ERR-OUT-OF-USER-AREA ?error
  dup (user-area-used) @ + (user-area-max-size) u> ERR-OUT-OF-USER-AREA ?error
  (tls-base-addr) (user-area-used) @ + over erase  ;; clear it
  (user-area-used) +!
;

: USERVAR-@OFS  ( cfa -- )  cfa->pfa @ ;
$else
alias var uservar
alias allot userallot
$endif

: (constant-cfa-pfa,)  ( n -- )
  (URFORTH-DOCONST-ADDR) compiler:(cfa-call,) , create; smudge
; (hidden)

: (constant-named)  ( n addr count -- )
  create-header-named (constant-cfa-pfa,)
; (hidden)

: CONSTANT  ( n -- )
  depth ERR-STACK-UNDERFLOW not-?error
  create-header (constant-cfa-pfa,)
  \ create , immediate does> @ [compile] literal
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create alias for some word
: (ALIAS-STR)  ( cfa addr count -- )
  create-header-named
  compiler:(jmp,) ;; emit jump to the old cfa
  create; smudge
; (hidden)

;; will not inherit word attributes
: (ALIAS-NO-ATTRS)  ( cfa -- )  \ word
  parse-name (alias-str)
; (hidden)

;; will inherit word attributes "immediate" and "hidden"
: (ALIAS)  ( cfa -- )  \ word
  dup >r (alias-no-attrs)
  ;; copy "hidden" and "immediate" flags
  r> cfa->ffa ffa@
  [ (wflag-hidden) (wflag-immediate) or ] literal and >r
  latest-cfa cfa->ffa
  dup ffa@ [ (wflag-hidden) (wflag-immediate) or ] literal ~and r> or
  swap ffa!
; (hidden)

: ALIAS  ( -- )  \ oldword newword
  -find-required
  (alias)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word replace mechanics
: (REPLACE)  ( oldcfa newcfa -- )
  ;; it is possible to replace any word with any word, because why not?
  ;; the only requirement is oldcfa word size: it should have at least 5 bytes
  over cfa->nfa nfa->sfa @ 5 u< err-cannot-replace ?error
  over forth:(dp-protected?) over forth:(dp-protected?) or if 2dup umax forth:(dp-protect-cfa) endif
  ;; patch jump to newcfa at oldcfa
  swap compiler:(jmp!)
; (hidden)

: REPLACE  ( -- )  \ oldword newword
  -find-required
  -find-required
  (replace)
;
