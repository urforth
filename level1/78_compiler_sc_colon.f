;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; scattering colon implementation
;; based on the idea by M.L. Gassanenko ( mlg@forth.org )
;; written from scratch by Ketmar Dark
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; placeholder for scattered colon
;; it will compile two branches:
;; the first branch will jump to the first "..:" word (or over the two branches)
;; the second branch is never taken, and works as a pointer to the latest branch addr in the list
;; this way, each extension word will simply fix the last branch address, and update list tail
;; at the creation time, second branch points to the first branch
: ...  ( -- )
  compiler:?comp compiler:?non-macro
  latest-cfa dup word-type? word-type-forth <> ERR-ELLIPSIS-FORTH ?error
  cfa->pfa here <> ERR-ELLIPSIS-FIRST ?error
  compile branch compiler:(<j-mark) compiler:(mark-j>)
  compile branch swap compiler:(<j-resolve)
  compiler:(resolve-j>)
  (set-scolon)
; immediate


;; start scattered colon extension code
;; TODO: better error checking!
;; this does very simple sanity check, and remembers the address of the tail pointer
: ..:  ( -- )  \ word
  compiler:?exec compiler:?non-macro
  -find-required dup word-type? word-type-forth <> ERR-ELLIPSIS-FORTH ?error dup >r
  dup cfa-scolon? ERR-ELLIPSIS-FIRST not-?error
  ;; more sanity checks, just in case
  cfa->pfa dup @ ['] branch <> ERR-ELLIPSIS-FIRST ?error
  2 +cells dup @ ['] branch <> ERR-ELLIPSIS-FIRST ?error
  cell+  ;; pointer to the tail pointer
  ;; create "named noname" for debugger
  r> cfa->nfa id-count (:noname-named) compiler:(ctlid-colon) compiler:?pairs
  \ [compile] :noname
  cfa->pfa ;; :noname leaves our cfa
  compiler:(CTLID-SC-COLON)  ;; pttp ourpfa flag
;

;; this ends the extension code
;; it patches jump at which list tail points to jump to our pfa, then
;; it compiles jump right after the list tail, and then
;; it updates the tail to point at that jump address
: ;..  ( -- )
  compiler:?comp compiler:?non-macro
  compiler:(CTLID-SC-COLON) compiler:?pairs
  ( pttp ourpfa )  over forth:(dp-protected?) >r
  over compiler:(branch-addr@) compiler:(branch-addr!)
  >r compile branch here 0 , r@ cell+ swap compiler:(branch-addr!)
  here cell- r> compiler:(branch-addr!)
  ;; we're done here
  compiler:end-compile-forth-word
  r> if here forth:(dp-protect) endif
; immediate


;; this ends the extension code
;; makes the code first in the jump list
;; jumps to the destination of the first jump
;; patches the first jump so it points to our nonamed code
;; patches tail pointer so it points to our jump
: <;..  ( -- )
  compiler:?comp compiler:?non-macro
  compiler:(CTLID-SC-COLON) compiler:?pairs
  ( pttp ourpfa )  over forth:(dp-protected?) >r
  >r  ( pttp | ourpfa )
  ;; get first jump destination
  compile branch here 0 ,  ( pttp jpatchaddr )
  over 2 -cells compiler:(branch-addr@) over compiler:(branch-addr!)  ;; fix our jump
  over 2 -cells r> swap compiler:(branch-addr!)  ;; fix first jump
  ;; patch original jump if there are no items in scattered chain yet
    \ endcr ." pptp:" over .hex8 ."  jpa:" dup .hex8 over compiler:(branch-addr@) ."  pptp-j:" .hex8 cr
  over dup compiler:(branch-addr@) 2 +cells = if
      \ endcr ." !!!patch!\n"
    swap compiler:(branch-addr!)
  else 2drop endif
  \ 2drop \ swap compiler:(branch-addr!)
  ;; we're done here
  compiler:end-compile-forth-word
  r> if here forth:(dp-protect) endif
; immediate
