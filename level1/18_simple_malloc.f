;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; very simple, slow and wasteful memory allocator
;; DO NOT USE UNLESS REALLY NECESSARY!

: (simple-malloc-can-replace?)  ( -- flag )  true ; (hidden)

: (simple-malloc-align)  ( addr -- addr )  1- 4095 or 1+ ; (hidden)

;; 0 is NOT valid!
: (simple-malloc-valid-addr?)  ( addr -- flag )
  dup 4095 u> over 4095 and cell = and if (code-base-addr) dp-last-addr @ bounds? not
  else drop false endif
; (hidden)


;; get memory from the OS
;; WARNING! do not use this to allocate a lot of small chunks!
;;          the granularity is 4096 bytes, so you will waste
;;          a lot of memory this way!
;; first cell of the allocated memory is fill block size (cell+, aligned)
;; it is ok to allocate 0 bytes (it will return non-0 address that shold be freed)
;; returned value is always (aligned at 4096)+4
;; you can use "THROW" after malloc
: simple-malloc  ( usize -- addr 0 // 0 throw-errcode )
  dup 0xffff_d000 u> if drop 0 err-out-of-memory
  else
    cell+ (simple-malloc-align) dup os:prot-r/w os:mmap
    if tuck ! cell+ 0 else 2drop 0 err-out-of-memory endif
  endif
;

;; addr can be 0
: simple-free  ( addr -- 0 // throw-errcode )
  dup if
    dup (simple-malloc-valid-addr?) if cell- dup @ os:munmap if err-invalid-malloc else 0 endif
    else drop err-invalid-malloc endif
  endif
;

;; WARNING! rellocing to zero bytes WILL FREE memory! (and return 0 as newaddr)
: simple-realloc  ( addr newsize -- newaddr 0 // addr throw-errcode )
  dup ifnot drop dup simple-free dup ifnot nip dup endif exit endif  ;; zero newsize
  over ifnot nip simple-malloc exit endif  ;; zero addr
  over (simple-malloc-valid-addr?) ifnot drop err-invalid-malloc exit endif
  dup 0xffff_d000 u> if drop err-out-of-memory exit endif
  cell+ (simple-malloc-align)  ;; align newsize
  2dup swap cell- @ = if drop 0  ;; nothing to do
  else over cell- dup @ rot dup >r os:mremap if nip r> over ! cell+ 0 else drop rdrop err-out-of-memory endif
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; memory allocation API
;; make sure that you will set it as soon as possible
;; the kernel is not using it, so it is safe to call kernel words
;; call "mman-can-replace?" before seting your own allocator
;; WARNING! not thread-safe (i.e. replace allocator BEFORE doing MT)
;;
;; high-level interface will make sure that the protocol is valid
;; i.e. won't call (mem-free) with zero size, will sort out calls
;; to (mem-realloc), and will never call (mem-alloc) with 0 size
;; it will also fix return values
;;
;; low-level memory manager words should not THROW
;;

$variable "(mem-can-replace?)" cfa "(simple-malloc-can-replace?)"  ;; ( -- flag )
(hidden)
;; high-level API will never called this with zero size
$variable "(mem-alloc)" cfa "simple-malloc"                        ;; ( usize -- addr 0 // garbage throw-errcode )
(hidden)
;; high-level API will never called this with zero addr
$variable "(mem-free)" cfa "simple-free"                           ;; ( addr -- 0 // throw-errcode )
(hidden)
;; high-level API will never called this with zero addr or zero newsize
$variable "(mem-realloc)" cfa "simple-realloc"                     ;; ( addr newsize -- newaddr 0 // garbage throw-errcode )
(hidden)


: mman-can-replace?  ( -- flag )  (mem-can-replace?) @execute-tail ;

;; ANS/F2012
;; accepts 0, and allocates the smallest possible chunk in this case
;; returned addr is 0 on any error
: allocate  ( size -- addr res )
  dup ifnot 1+ endif  ;; normalize size
  (mem-alloc) @execute
  dup if nip 0 swap endif  ;; normalize return addr
;

: free  ( addr -- res )  dup if (mem-free) @execute-tail endif ;

;; frees on zero size, allocates on zero address
;; returns original addr on any error
: resize  ( addr newsize -- newaddr 0 // addr throw-errcode )
  dup ifnot drop dup free dup ifnot nip dup endif exit endif  ;; zero newsize
  over ifnot nip allocate exit endif  ;; zero addr
  over >r (mem-realloc) @execute
  dup if nip r> swap else rdrop endif  ;; normalize return addr
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; use this when you need ASCIIZ string for various APIs
;; usage pattern:
;;  ensure-asciiz >r ... r> free-asciiz
;;
: ensure-asciiz  ( addr count -- addr allocid )
  dup -0if 2drop NullString exit endif
  ;; check if it is 0-terminated
  2dup + c@ ifnot drop 0 exit endif
  dup 1+ allocate throw  ;; allocate temp buffer
  dup >r                 ;; save allocid
  2dup swap 1+ erase     ;; it is easier this way
  swap move r> dup       ;; copy and return the result
;

: free-asciiz  ( allocid -- )  free throw ;

