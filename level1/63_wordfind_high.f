;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; high-level word lookup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; save current search flags to return stack
: (WFIND-FLAGS>R)  ( -- )
  r> (voc-find-flags-mask) @ >r >r
; (hidden)

;; restore current search flags from return stack
: (WFIND-R>FLAGS)  ( -- )
  r> r> (voc-find-flags-mask) ! >r
; (hidden)


;; hidden words won't be found unless current vocabulary is the top context one
;; this doesn't do any colon (namespace) resolving
;; it is basically used for the first search, and iterates over vocabulary stack
;; it doesn't look in CURRENT
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: WFIND-BASIC  ( addr count -- cfa -1 // cfa 1 // false )
  (wfind-flags>r) (wflag-smudge) voc-find-add-mask  ;; skip smudge words
  2>r context ;; search context stack
  ( ctxptr | addr count )
  begin dup @ dup while  ( ctxptr voclptr | addr count )
    ;; if voc-to-search is the current one, allow hidden words
    (wflag-hidden) over current @ = if voc-find-reset-mask else voc-find-add-mask endif
    ( ctxptr voclptr | addr count )
    2r@ rot voc-find-str ?dup if  ( ctxptr cfa immflag )
      2rdrop rot drop (wfind-r>flags) exit
    endif
    ( ctxptr | addr count )
    ;; if voc-to-search is the current one, look into parents
    dup @ current @ = if
      ;; hidden words already allowed above
      dup @ begin vocid->parent @ ?dup while
        ( ctxptr voclptr | savedmask addr count )
        dup 2r@ rot voc-find-str ?dup if  ( ctxptr voclptr cfa immflag )
          2rdrop 2swap 2drop (wfind-r>flags) exit
        endif
      repeat
    endif
    cell-  ;; up context stack
  repeat
  2drop 2rdrop (wfind-r>flags) false
;


;; inner loop over (multiple) colons
;; splitted to separate word for clarity
;; input string is the full string, with vocabulary name unstripped
;; i.e. "forth:words"
;; vocid is vocabulary id for that string (i.e. "forth" vocid)
;; the code will immediately strip vocabulary name
;; note that the string MUST contain a colon, no checks are made
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: (WFIND-VOC-LOOP)  ( addr count vocid -- cfa -1 // cfa 1 // false )
  ;; make sure that we can find any hidden word this way
  (wfind-flags>r) (wflag-hidden) voc-find-reset-mask
  >r  ( addr count | vocid )
  begin
    ;; we'll come here with guaranteed colon
    [char] : str-skip-after-char
    2dup r@ voc-find-str ?dup if  ;; i found her!
      ( addr count cfa immflag | vocid )
      2swap 2drop rdrop (wfind-r>flags) exit
    endif
    ( addr count | vocid )
    2dup [char] : str-trim-at-char ?dup ifnot  ;; no more colons
      ( addr count addr | vocid )
      2drop drop rdrop false (wfind-r>flags) exit
    endif
    ;; vocabulary recursion?
    ( addr count vocname-addr vocname-count | vocid )
    r@ voc-find-str ifnot  ;; not found
      ( addr count | vocid )
      2drop rdrop false (wfind-r>flags) exit
    endif
    ( addr count cfa | vocid )
    ;; is it a vocabulary?
    dup word-type? word-type-voc = ifnot
      drop 2drop rdrop false (wfind-r>flags) exit
    endif
    rdrop voc-cfa->vocid >r
    ( addr count | newvocid )
    ;; vocabulary name will be stripped with the above code
  again
;


;; check for several know special vocabulary names
: (WFIND-SPECIAL-NAME?)  ( addr count -- vocid true // false )
  ;; first, FORTH vocabulary should be accessible from anywhere (just in case)
  2dup " FORTH" s=ci if
    2drop  ;; drop vocname
    ['] FORTH voc-cfa->vocid
    true exit
  endif
  ;; second, CURRENT vocabulary should be accessible too (because it may not be in the search list)
  2dup " CURRENT" s=ci if
    2drop  ;; drop vocname
    current @
    true exit
  endif
  ;; no more (for now)
  2drop
  false
;


;; this is The Word that should be used for vocabulary searches
;; this does namespace resolution
;; if "a:b" is not a known word, try to search "b" in dictionary "a"
;; things like "a:b:c" are allowed too
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: WFIND  ( addr count -- cfa -1 // cfa 1 // false )
  ;; try full word first
  2dup wfind-basic ?dup if 2swap 2drop exit endif
  ;; first colon
  2dup [char] : str-trim-at-char ?dup ifnot
    ;; no colon
    drop 2drop false exit
  endif
  ;; try to find a vocabulary
  ( addr count vocname-addr vocname-count )
  2dup wfind-basic ifnot
    ;; not found; check for some special names
    (wfind-special-name?) ifnot
      2drop false exit
    endif
  else
    ;; check if it is a vocabulary
    dup word-type? word-type-voc = ifnot
      ;; not a vocabulary, try some special names
      drop  ;; we don't need CFA anymore
      (wfind-special-name?) ifnot
        2drop false exit
      endif
    else
      voc-cfa->vocid
      nrot 2drop   ;; drop vocname
    endif
  endif
  ( addr count vocid )
  (wfind-voc-loop)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: -find  ( -- cfa -1 // cfa 1 // false )  \ word
  parse-name wfind
;

: xfind  ( addr count -- cfa immflag true // addr count false )
  2dup wfind dup if 2swap 2drop true endif
;

: xfind-required-ex  ( addr count -- cfa immflag )
  xfind ifnot
    ?endcr if space endif ." \`" type ." \`? "
    err-word-expected error
  endif
;

: -find-required-ex  ( -- cfa immflag )  \ word
  parse-name [execute-tail] xfind-required-ex
;

: -find-required  ( -- cfa )  \ word
  -find-required-ex drop
;

: has-word?  ( addr count -- flag )
  wfind dup if nip endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (voc-search-with-mask)  ( addr count vocid flags -- addr count false // cfa -1 // cfa 1 )
  (wfind-flags>r) voc-find-replace-mask
  >r 2dup r> voc-find-str dup if 2swap 2drop endif
  (wfind-r>flags)
;

;; this searches a vocabulary for a word, ignoring hidden and smudged
: voc-search  ( addr count vocid -- addr count false // cfa -1 // cfa 1 )
  [ (wflag-smudge) (wflag-hidden) or (wflag-vocab) or (wflag-codeblock) or ] literal
  (voc-search-with-mask)
;


;; this searches a vocabulary for a word, ignoring hidden, smudged and immediate
: voc-search-noimm  ( addr count vocid -- addr count false // cfa -1 // cfa 1 )
  [ (wflag-smudge) (wflag-hidden) or (wflag-vocab) or (wflag-codeblock) or (wflag-immediate) or ] literal
  (voc-search-with-mask)
;
