;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;$uservar "XEMIT" ua_ofs_xemit 0
;; ( ch -- )

$uservar "(EMIT)" ua_ofs_emit cfa "(STDTTY-EMIT)"
;; ( ch -- )

;; this doesn't count columns
$uservar "(XEMIT)" ua_ofs_xemit cfa "(STDTTY-XEMIT)"
;; ( ch -- )

$uservar "(TYPE)" ua_ofs_emit_type cfa "(STDTTY-TYPE)"
;; ( addr count -- )

;; this doesn't count columns
$uservar "(XTYPE)" ua_ofs_emit_xtype cfa "(STDTTY-XTYPE)"
;; ( addr count -- )

$uservar "(CR)" ua_ofs_emit_cr cfa "(STDTTY-CR)"
 ( -- )

$uservar "(BELL)" ua_ofs_emit_bell cfa "(STDTTY-BELL)"
;; ( -- )

$uservar "(ENDCR)" ua_ofs_emit_endcr cfa "(STDTTY-ENDCR)"
;; ( -- )

;; should "ENDCR" do "CR"?
$uservar "(?ENDCR)" ua_ofs_emit_isendcr cfa "(STDTTY-?ENDCR)"
;; ( -- flag )

$uservar "(RESET-EMIT-COL)" ua_ofs_emit_resetcol cfa "(STDTTY-RESET-EMIT-COL)"
;; ( -- )

$uservar "(KEY)" ua_ofs_emit_key cfa "(STDTTY-GETCH)"
;; ( -- ch )

$uservar "(KEY?)" ua_ofs_emit_iskey cfa "(STDTTY-KEY?)"
;; ( -- flag )

$uservar "(EKEY)" ua_ofs_emit_ekey 0
;; ( -- ch )

$uservar "(EKEY?)" ua_ofs_emit_isekey 0
;; ( -- flag )


: emit  ( ch -- ) (emit) @execute-tail ;
: xemit  ( ch -- ) (xemit) @execute-tail ;
: type  ( addr count -- ) (type) @execute-tail ;
: xtype  ( addr count -- ) (xtype) @execute-tail ;
: cr  (  -- ) (cr) @execute-tail ;
: bell  ( -- ) (bell) @execute-tail ;
: endcr  ( -- ) (endcr) @execute-tail ;
: ?endcr  ( -- flag ) (?endcr) @execute-tail ;
: reset-emitcol  ( -- ) (reset-emit-col) @execute-tail ;
: key  ( -- key ) (key) @execute-tail ;
: key?  ( -- flag ) (key?) @ dup if execute-tail endif ;
: ekey  ( -- key ) (ekey) @ ?dup if execute-tail else key endif ;
: ekey?  ( -- flag ) (ekey?) @ ?dup if execute-tail else key? endif ;


$uservar "(SAFE-EMIT-HIGH-ASCII?)" ua_ofs_safe_emit_highascii 1

: (safe-for-emit)  ( ch -- ch true // 63 false )
  0xff and
  dup bl < if  ;; < 32: allow tab, cr, lf
    dup 9 = over nl = or over 13 = or ?dup ifnot drop [char] ? false endif
  else
    dup (safe-emit-high-ascii?) @ if 127 0xa0 within else 127 >= endif
    if drop [char] ? false else true endif
  endif
; (hidden)

: safe-emit  ( ch -- )  (safe-for-emit) drop emit ;
: safe-emit-printable  ( ch -- )  (safe-for-emit) drop dup bl < if drop [char] ? endif emit ;
: safe-emit-xdump  ( ch -- )  (safe-for-emit) if dup bl <= if drop [char] . endif else drop [char] . endif emit ;


: type-with  ( addr length emitcfa -- )  over +if nrot bounds do i c@ over execute loop else 2drop endif drop ;

\ : type  ( addr length -- )  ['] emit type-with ;

: safe-type  ( addr length -- )  ['] safe-emit type-with ;
: safe-type-printable  ( addr length -- )  ['] safe-emit-printable type-with ;

: space  ( -- )  bl emit ;
: spaces  ( n -- )  for space endfor ;

;; type asciiz string
: type-asciiz  ( addr -- )  zcount type ;
: safe-type-asciiz  ( addr -- )  zcount safe-type ;
: safe-type-printable-asciiz  ( addr -- )  zcount safe-type-printable ;


: dump  ( addr count -- )
  endcr begin dup +while
    over .hex8 [char] : emit
    2dup 8 for space dup +if over c@ .hex2 else 2 spaces endif /char endfor space
    8 for space dup +if over c@ .hex2 else 2 spaces endif /char endfor 2 spaces
    2drop 2dup 8 for dup +if over c@ safe-emit-xdump else space endif /char endfor space
    8 for dup +if over c@ safe-emit-xdump else space endif /char endfor
    2drop 16 /string cr
  repeat 2drop
;


: (.")  ( -- )
  r@ bcount type r> bcount + 3 or 1+ >r
; (hidden) (arg-c1strz)
