;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: (on-bye)  ... ;

code: (N-BYE)  ( exitcode -- )
  push  TOS
  mov   eax,252  ;; sys_exit_group
  mov   ebx,TOS
  syscall
  pop   TOS
  mov   eax,1
  mov   ebx,TOS
  syscall
endcode
(noreturn)


: bye  ( -- ) (on-bye) 0 (n-bye) ; (noreturn)
: n-bye  ( n -- ) >r (on-bye) r> (n-bye) ; (noreturn)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary OS
voc-set-active OS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dynamic?
$if URFORTH_DYNAMIC_BINARY
code: DLCLOSE  ( handle-or-0 -- )
  jecxz .fucked
  ;; push registers
  push  EIP
  push  ERP
  ;; call
  push  TOS
  call  [elfimp_dlclose]
  add   esp,4   ;; remove args
  pop   ERP
  pop   EIP
.fucked:
  pop   TOS
  urnext
endcode

code: (DLOPEN-ASCIIZ)  ( addr -- handle-or-0 )
  jecxz .fucked
  ;; save registers
  push  EIP
  push  ERP
  ;; call function
  ;;ld    eax,1+256+8   ;; RTLD_LAZY+RTLD_GLOBAL+RTLD_DEEPBIND
  push  1+256+8   ;; RTLD_LAZY+RTLD_GLOBAL+RTLD_DEEPBIND
  push  TOS
  call  [elfimp_dlopen]
  add   esp,4*2       ;; remove arguments
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
.fucked:
  urnext
endcode

: DLOPEN  ( addr count -- handle-or-0 )
  ensure-asciiz >r
  (DLOPEN-ASCIIZ)
  r> free-asciiz
;

code: (DLSYM-ASCIIZ)  ( addr handle -- address-or-0 )
  pop   edi
  test  edi,edi
  jr    z,.fucked
  ;; save registers
  push  EIP
  push  ERP
  ;; call function
  push  edi
  push  TOS
  call  [elfimp_dlsym]
  add   esp,4*2       ;; remove arguments
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
.fucked:
  xor   TOS,TOS
  urnext
endcode

: DLSYM  ( addr count handle -- handle-or-0 )
  nrot ensure-asciiz >r
  swap (DLSYM-ASCIIZ)
  r> free-asciiz
;

$constant "RTLD-DEFAULT" 0
$constant "RTLD-NEXT" -1

;; call a C function
;; negative or zero argcount means "pop nothing"
code: CINVOKE ( ... addr argcount -- res )
  pop   eax
  pushr TOS
  pushr EIP
  call  eax
  mov   TOS,eax
  popr EIP
  popr eax
  cp    eax,0
  jr    le,@f
  lea   esp,[esp+eax*4]
@@:
  urnext
endcode
$endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (FUNLINK-ASCIIZ)  ( addr -- errcode-or-0 )
  jecxz .fucked
  ld    ebx,TOS
  ld    eax,10
  syscall
  ld    TOS,eax
  urnext
.fucked:
  ld    TOS,-666
  urnext
endcode

: UNLINK  ( addr count -- errcode-or-0 )
  ensure-asciiz >r
  (FUNLINK-ASCIIZ)
  r> free-asciiz
;


code: (FRENAME-ASCIIZ)  ( addrold addrnew -- errcode-or-0 )
  pop   ebx
  jecxz .fucked
  test  ebx,ebx
  jr    z,.fucked
  ld    eax,38
  syscall
  ld    TOS,eax
  urnext
.fucked:
  ld    TOS,-666
  urnext
endcode

: RENAME  ( addrold countold addrnew countnew -- errcode-or-0 )
  ensure-asciiz >r
  nrot ['] ensure-asciiz forth:catch ?dup if r> free-asciiz forth:throw endif >r
  swap (FRENAME-ASCIIZ)
  r> free-asciiz r> free-asciiz
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: CLOSE  ( fd -- flag )
  ;; syscall
  ld    eax,6
  ld    ebx,TOS
  syscall
  ld    TOS,eax
  urnext
endcode

code: (OPEN-ASCIIZ)  ( addr flags crperm -- fd-or-minusone )
  pop   edx   ;; flags
  pop   edi   ;; addr
  ld    eax,TOS
  ;; EAX: crperm
  ;; EDX: flags (r, w, r/w, flags)
  ;; EDI: addr
  test  edi,edi
  jr    z,.fucked
  ld    ebx,edi
  ld    ecx,edx
  ld    edx,eax
  ld    eax,5
  syscall
  ld    TOS,eax
  urnext
.fucked:
  ld    TOS,-666
  urnext
endcode

: OPEN  ( addr count flags crperm -- handle-or-<0 )
  2swap ensure-asciiz >r nrot
  (OPEN-ASCIIZ)
  r> free-asciiz
;

code: (READ)  ( addr count fd -- count )
  pop   edx   ;; count
  pop   edi   ;; addr
  ld    eax,3
  ld    ebx,TOS
  ld    ecx,edi
  syscall
  ld    TOS,eax
  urnext
endcode

code: (WRITE)  ( addr count fd -- count )
  pop   edx   ;; count
  pop   edi   ;; addr
  ld    eax,4
  ld    ebx,TOS
  ld    ecx,edi
  syscall
  ld    TOS,eax
  urnext
endcode

: (do-rw)  ( addr count fd cfa -- count )
  begin 2over 2over execute dup -4 ( EINTR) = while drop repeat
  >r 2drop 2drop r>
; (hidden)

: read   ( addr count fd -- count/err )  ['] (read) (do-rw) ;
: write  ( addr count fd -- count/err )  ['] (write) (do-rw) ;

code: LSEEK  ( ofs whence fd -- res )
  ld    ebx,TOS
  pop   edx
  pop   ecx
  ld    eax,19
  syscall
  ld    TOS,eax
  urnext
endcode

code: LLSEEK  ( ofslo ofshi whence fd -- ofslo ofshi res )
  ld    ebx,TOS      ;; fd
  pop   edi          ;; whence
  push  EIP
  push  ERP
  ld    ecx,[esp+8]  ;; lo
  ld    edx,[esp+12] ;; hi
  lea   esi,[esp+8]  ;; put position there
  ld    eax,140
  syscall
  pop   ERP
  pop   EIP
  ld    TOS,eax
  ;; swap result
  ld    edx,[esp+0]
  xchg  edx,[esp+4]
  ld    [esp+0],edx
  urnext
endcode

code: FSYNC  ( fd -- res )
  ld    ebx,TOS      ;; fd
  ld    eax,118
  syscall
  ld    TOS,eax
  urnext
endcode

code: FSYNC-DATA  ( fd -- res )
  ld    ebx,TOS      ;; fd
  ld    eax,148
  syscall
  ld    TOS,eax
  urnext
endcode

code: FTRUNC  ( size fd -- res )
  ld    ebx,TOS
  pop   ecx
  ld    eax,93
  syscall
  ld    TOS,eax
  urnext
endcode

code: FTRUNC64  ( sizelo sizehi fd -- res )
  ld    ebx,TOS      ;; fd
  pop   edx
  pop   ecx
  ld    eax,194
  syscall
  ld    TOS,eax
  urnext
endcode



$constant "O-RDONLY" 0
$constant "O-WRONLY" 1
$constant "O-RDWR"   2

$constant "O-CREAT"     0x000040  ;; 0100
$constant "O-EXCL"      0x000080  ;; 0200
$constant "O-NOCTTY"    0x000100  ;; 0400
$constant "O-TRUNC"     0x000200  ;; 01000
$constant "O-APPEND"    0x000400  ;; 02000
$constant "O-NONBLOCK"  0x000800  ;; 04000
$constant "O-DSYNC"     0x001000  ;; 010000
$constant "O-SYNC"      0x101000  ;; 04010000
$constant "O-RSYNC"     0x101000  ;; 04010000
$constant "O-DIRECTORY" 0x010000  ;; 0200000
$constant "O-NOFOLLOW"  0x020000  ;; 0400000
$constant "O-CLOEXEC"   0x080000  ;; 02000000

$constant "O-ASYNC"     0x002000  ;; 020000
$constant "O-DIRECT"    0x004000  ;; 040000
$constant "O-LARGEFILE" 0x008000  ;; 0100000
$constant "O-NOATIME"   0x040000  ;; 01000000
$constant "O-PATH"      0x200000  ;; 010000000
$constant "O-TMPFILE"   0x410000  ;; 020200000
$constant "O-NDELAY"    0x000800  ;; 04000

$constant "O-CREATE-FLAGS-NOMODE" 0x000240
$constant "O-CREATE-WRONLY-FLAGS" 0x000241
$constant "O-CREATE-MODE-NORMAL"  0x1A4

$constant "SEEK-SET" 0
$constant "SEEK-CUR" 1
$constant "SEEK-END" 2

$constant "S-ISUID" 0x800  ;; 04000
$constant "S-ISGID" 0x400  ;; 02000
$constant "S-ISVTX" 0x200  ;; 01000
$constant "S-IRUSR" 0x100  ;; 0400
$constant "S-IWUSR" 0x080  ;; 0200
$constant "S-IXUSR" 0x040  ;; 0100
$constant "S-IRWXU" 0x1c0  ;; 0700
$constant "S-IRGRP" 0x020  ;; 0040
$constant "S-IWGRP" 0x010  ;; 0020
$constant "S-IXGRP" 0x008  ;; 0010
$constant "S-IRWXG" 0x038  ;; 0070
$constant "S-IROTH" 0x004  ;; 0004
$constant "S-IWOTH" 0x002  ;; 0002
$constant "S-IXOTH" 0x001  ;; 0001
$constant "S-IRWXO" 0x007  ;; 0007


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "STAT.dev"         0  ;; rd 1
$constant "STAT.ino"         4  ;; rd 1
$constant "STAT.mode"        8  ;; rw 1
$constant "STAT.nlink"      10  ;; rw 1
$constant "STAT.uid"        12  ;; rw 1
$constant "STAT.gid"        14  ;; rw 1
$constant "STAT.rdev"       16  ;; rd 1
$constant "STAT.size"       20  ;; rd 1
$constant "STAT.blksize"    24  ;; rd 1
$constant "STAT.blocks"     28  ;; rd 1
$constant "STAT.atime"      32  ;; rd 1
$constant "STAT.atime_nsec" 36  ;; rd 1
$constant "STAT.mtime"      40  ;; rd 1
$constant "STAT.mtime_nsec" 44  ;; rd 1
$constant "STAT.ctime"      48  ;; rd 1
$constant "STAT.ctime_nsec" 52  ;; rd 1
;; $constant "STAT.__unused4"     56  ;; rd 1
;; $constant "STAT.__unused5"     60  ;; rd 1
$constant "#STAT"      64

$constant "STAT64.dev"         0  ;; rq 1, rb 4
$constant "STAT64._ino"       12  ;; rd 1
$constant "STAT64.mode"       16  ;; rd 1
$constant "STAT64.nlink"      20  ;; rd 1
$constant "STAT64.uid"        24  ;; rd 1
$constant "STAT64.gid"        28  ;; rd 1
$constant "STAT64.rdev"       32  ;; rq 1, rb 4
$constant "STAT64.size"       44  ;; rq 1
$constant "STAT64.blksize"    52  ;; rd 1
$constant "STAT64.blocks"     56  ;; rq 1
$constant "STAT64.atime"      64  ;; rd 1
$constant "STAT64.atime_nsec" 68  ;; rd 1
$constant "STAT64.mtime"      72  ;; rd 1
$constant "STAT64.mtime_nsec" 76  ;; rd 1
$constant "STAT64.ctime"      80  ;; rd 1
$constant "STAT64.ctime_nsec" 84  ;; rd 1
$constant "STAT64.ino"        88  ;; rq 1
$constant "#STAT64"           96


$constant "S-IFMT"   0o0170000

$constant "S-IFDIR"  0o0040000
$constant "S-IFCHR"  0o0020000
$constant "S-IFBLK"  0o0060000
$constant "S-IFREG"  0o0100000
$constant "S-IFIFO"  0o0010000
$constant "S-IFLNK"  0o0120000
$constant "S-IFSOCK" 0o0140000


\ statbuf should be #STAT bytes
code: (STAT)  ( nameaddrz statbuf -- errcode )
  pop   ebx
  ld    eax,106
  syscall
  ld    TOS,eax
  urnext
endcode

\ statbuf should be #STAT64 bytes
code: (STAT64)  ( nameaddrz statbuf -- errcode )
  pop   ebx
  ld    eax,195
  syscall
  ld    TOS,eax
  urnext
endcode


: STAT  ( nameaddr namecount statbuf -- errcode )  nrot ensure-asciiz >r swap (stat) r> free-asciiz ;
: STAT64  ( nameaddr namecount statbuf -- errcode )  nrot ensure-asciiz >r swap (stat64) r> free-asciiz ;

: STAT-MODE  ( addr count -- mode true // false )
  #stat ralloca dup >r stat if false else r@ stat.mode + w@ true endif
  rdrop #stat rdealloca
;

: STAT-SIZE  ( addr count -- size true // false )
  #stat ralloca dup >r stat if false else r@ stat.size + @ true endif
  rdrop #stat rdealloca
;

: STAT-SIZE64  ( addr count -- sizelo sizehi true // false )
  #stat64 ralloca dup >r stat64 if false else r@ stat64.size + 2@le true endif
  rdrop #stat64 rdealloca
;

;; is regular file?
: FILE?  ( addr count -- flag )  stat-mode if S-IFREG and notnot else false endif ;
;; is directory?
: DIR?  ( addr count -- flag )  stat-mode if S-IFDIR and notnot else false endif ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get seconds since epoch
code: TIME  ( -- seconds )
  push  TOS
  ld    eax,13   ;; sys_time
  xor   ebx,ebx  ;; result only in eax
  syscall
  ld    TOS,eax
  urnext
endcode

$constant "CLOCK-REALTIME"            0
$constant "CLOCK-MONOTONIC"           1
$constant "CLOCK-PROCESS-CPUTIME-ID"  2
$constant "CLOCK-THREAD-CPUTIME-ID"   3
$constant "CLOCK-MONOTONIC-RAW"       4
$constant "CLOCK-REALTIME-COARSE"     5
$constant "CLOCK-MONOTONIC-COARSE"    6
$constant "CLOCK-BOOTTIME"            7
$constant "CLOCK-REALTIME-ALARM"      8
$constant "CLOCK-BOOTTIME-ALARM"      9

$constant "NANOSECONDS/SECOND"  1000000000
$constant "NANOSECONDS/MSEC"    1000000

;; get seconds and nanoseconds (since some random starting point)
code: CLOCK-GETTIME  ( clockid -- seconds nanoseconds )
  sub   esp,4+4  ;; timespec
  ld    eax,265  ;; sys_clock_gettime
  ld    ebx,TOS  ;; clockid
  ld    ecx,esp
  syscall
  test  eax,eax
  pop   eax
  pop   TOS
  jr    z,.done
  xor   TOS,TOS
  xor   eax,eax
.done:
  push  eax
  urnext
endcode


code: NANOSLEEP  ( seconds nanoseconds -- errcode )
  xchg  TOS,dword [esp]
  push  TOS
  ;; [esp+0]: timespec
  ld    eax,162
  ld    ebx,esp
  ld    ecx,ebx  ;; fill the same struct
  syscall
  add   esp,4+4
  ld    TOS,eax
  urnext
endcode

: MSSLEEP  ( msecs -- )
  dup +if 1000 u/mod swap nanoseconds/msec u* nanosleep drop
  else drop
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "(GETTICKCOUNT-START-SECS)" 0
(hidden)

..: forth:(startup-init)  ( -- )
  clock-monotonic clock-gettime drop
  dup 1 > if 1- endif
  to (gettickcount-start-secs)
;..


: GETTICKCOUNT  ( -- msecs )
  clock-monotonic clock-gettime
  nanoseconds/msec u/
  swap (gettickcount-start-secs) - 1000 u*
  +
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: GET-PID  ( -- pid )
  push  TOS
  ld    eax,20
  syscall
  ld    TOS,eax
  urnext
endcode

code: GET-TID  ( -- tid )
  push  TOS
  ld    eax,224
  syscall
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; request and reply
$constant "POLL-IN"     0x0001  ;; read ready
$constant "POLL-PRI"    0x0002  ;; some exceptional condition
$constant "POLL-OUT"    0x0004  ;; write ready
;; reply only
$constant "POLL-ERR"    0x0008  ;; error, or read end of the pipe closed (set for write end)
$constant "POLL-HUP"    0x0010  ;; hangup/connection closed (there still may be data to read)
$constant "POLL-NVAL"   0x0020  ;; invalid fd

$constant "POLL-RDNORM" 0x0040  ;; man says that this is the same as "POLL-IN"
$constant "POLL-RDBAND" 0x0080  ;; priority data can be read
$constant "POLL-WRNORM" 0x0100  ;; man says that this is the same as "POLL-OUT"
$constant "POLL-WRBAND" 0x0200  ;; priority data can be written

$constant "POLL-MSG"    0x0400  ;; man says that is is accepted, but does nothing
$constant "POLL-RDHUP"  0x2000  ;; stream socket peer closed connection, or shut down writing half of connection

;; special timeouts
$constant "POLL-INFINITE" -1
$constant "POLL-NOWAIT" 0

$constant "#POLLFD" 8

code: POLLFD.FD  ( addr -- addr+0 )
  urnext
endcode

code: POLLFD.EVENTS  ( addr -- addr+4 )
  add   TOS,4
  urnext
endcode

code: POLLFD.REVENTS  ( addr -- addr+6 )
  add   TOS,6
  urnext
endcode

;; returs -errno or number of records changed
code: POLL  ( pollfdarrptr count mstime -- res )
  ld    edx,TOS
  pop   ecx
  pop   ebx
  ld    eax,168
  syscall
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "MAP-FILE"            0x0000_0000
$constant "MAP-SHARED"          0x0000_0001
$constant "MAP-PRIVATE"         0x0000_0002
$constant "MAP-SHARED_VALIDATE" 0x0000_0003
$constant "MAP-TYPE"            0x0000_000f
$constant "MAP-FIXED"           0x0000_0010
$constant "MAP-ANON"            0x0000_0020
$constant "MAP-ANONYMOUS"       0x0000_0020
$constant "MAP-NORESERVE"       0x0000_4000
$constant "MAP-GROWSDOWN"       0x0000_0100
$constant "MAP-DENYWRITE"       0x0000_0800
$constant "MAP-EXECUTABLE"      0x0000_1000
$constant "MAP-LOCKED"          0x0000_2000
$constant "MAP-POPULATE"        0x0000_8000
$constant "MAP-NONBLOCK"        0x0001_0000
$constant "MAP-STACK"           0x0002_0000
$constant "MAP-HUGETLB"         0x0004_0000
$constant "MAP-SYNC"            0x0008_0000
$constant "MAP-FIXED_NOREPLACE" 0x0010_0000

$constant "PROT-NONE"  0
$constant "PROT-READ"  1
$constant "PROT-WRITE" 2
$constant "PROT-EXEC"  4
$constant "PROT-R/W"   3
$constant "PROT-RWX"   7

$constant "PROT-GROWSDOWN" 0x01000000
$constant "PROT-GROWSUP"   0x02000000


code: MMAP  ( size protflags -- addr true // error false )
  ld    edx,TOS          ;; protflags
  pop   ecx              ;; size
  push  EIP
  push  ERP
  ld    esi,0x0000_0022  ;; we always doing anon private alloc
  xor   ebp,ebp          ;; offset, it is ignored, but why not
  xor   edi,edi          ;; fd (-1)
  dec   edi
  xor   ebx,ebx          ;; address
  ld    eax,192
  syscall
  pop   ERP
  pop   EIP
  push  eax
  cp    eax,0xffff_f000
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: MMAP-FD  ( size protflags offset fd -- addr true // error false )
  push  EIP
  push  ERP
  ld    edi,TOS          ;; fd
  ld    esi,0x0000_0001  ;; map file, shared
  ld    ebp,[esp+8]      ;; offset
  ld    edx,[esp+12]     ;; protflags
  ld    ecx,[esp+16]     ;; size
  xor   ebx,ebx          ;; address
  ld    eax,192
  syscall
  pop   ERP
  pop   EIP
  add   esp,4*3
  push  eax
  cp    eax,0xffff_f000
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: MUNMAP  ( addr size -- res )
  pop   ebx
  ld    eax,91
  syscall
  ld    TOS,eax
  urnext
endcode

;; use "MMAP-ERROR?" to check for errors
;; can move block
code: MREMAP  ( addr oldsize newsize -- newaddr true // error false )
  ld    edx,TOS
  pop   ecx
  pop   ebx
  push  EIP
  ld    eax,163
  ld    esi,1    ;; MREMAP_MAYMOVE
  xor   edi,edi  ;; new addres; doesn't matter
  syscall
  pop   EIP
  push  eax
  cp    eax,0xffff_f000
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: MPROTECT  ( addr size protflags -- res )
  ld    edx,TOS
  pop   ecx
  pop   ebx
  ld    eax,125
  syscall
  ld    TOS,eax
  urnext
endcode

code: MLOCK  ( addr size -- res )
  pop   ebx
  ld    eax,150
  syscall
  ld    TOS,eax
  urnext
endcode

code: MUNLOCK  ( addr size -- res )
  pop   ebx
  ld    eax,151
  syscall
  ld    TOS,eax
  urnext
endcode

code: MUNLOCK-ALL  ( -- res )
  push  TOS
  ld    eax,153
  syscall
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$if URFORTH_TLS_TYPE = URFORTH_TLS_TYPE_FS

$constant "CSIGNAL"              0x000000ff
$constant "CLONE-VM"             0x00000100
$constant "CLONE-FS"             0x00000200
$constant "CLONE-FILES"          0x00000400
$constant "CLONE-SIGHAND"        0x00000800
$constant "CLONE-PIDFD"          0x00001000
$constant "CLONE-PTRACE"         0x00002000
$constant "CLONE-VFORK"          0x00004000
$constant "CLONE-PARENT"         0x00008000
$constant "CLONE-THREAD"         0x00010000
$constant "CLONE-NEWNS"          0x00020000
$constant "CLONE-SYSVSEM"        0x00040000
$constant "CLONE-SETTLS"         0x00080000
$constant "CLONE-PARENT-SETTID"  0x00100000
$constant "CLONE-CHILD-CLEARTID" 0x00200000
$constant "CLONE-DETACHED"       0x00400000
$constant "CLONE-UNTRACED"       0x00800000
$constant "CLONE-CHILD-SETTID"   0x01000000
$constant "CLONE-NEWCGROUP"      0x02000000
$constant "CLONE-NEWUTS"         0x04000000
$constant "CLONE-NEWIPC"         0x08000000
$constant "CLONE-NEWUSER"        0x10000000
$constant "CLONE-NEWPID"         0x20000000
$constant "CLONE-NEWNET"         0x40000000
$constant "CLONE-IO"             0x80000000

;; settidptr: for CLONE-PARENT_SETTID (edx)
;; tlsptr: tls info pointer for CLONE-SETTLS (esi)
;; childtidptr: for CLONE-CHILD-CLEARTID and CLONE-CHILD-SETTID
code: CLONE  ( cfa sp rp flags tlsptr settidptr childtidptr -- err )
  push  TOS
  push  ERP
  push  EIP
  ;; [esp+8]: childtidptr
  ;; [esp+12]: settidptr
  ;; [esp+16]: tlsptr
  ;; [esp+20]: flags
  ;; [esp+24]: rp
  ;; [esp+28]: sp
  ;; [esp+32]: cfa
  ;; this works like this:
  ;;   setup new ERP
  ;;   push cfa to new ERP
  ;;   call clone
  ;;   on error, exit with error code
  ;;   if EAX is 0: we're in child
  ;;     setup ts to tlsptr (if there is any)
  ;;     get cfa from ERP, jump to it
  ;;   if EAX is !0: we're in parent
  ;;     restore Exx, drop args, return EAX
  ;; setup new ERP
  ld    ERP,[esp+24]  ;; rp
  ;; push cfa to new ERP
  ld    eax,[esp+32]  ;; cfa
  pushr eax
  ;; save tls entry index (we cannot access the stack in child)
  xor   eax,eax
  test  dword [esp+20],0x00080000 ;; CLONE-SETTLS
  jr    z,@f
  ld    eax,[esp+16] ;; tlsptr
  test  eax,eax
  jr    z,@f
  ld    eax,[eax]    ;; entry index
@@:
  pushr eax
  ld    eax,120      ;; sys_clone
  ld    ebx,[esp+20] ;; flags
  ld    ecx,[esp+28] ;; sp
  ld    edx,[esp+12] ;; settidptr
  ld    esi,[esp+16] ;; tlsptr
  ld    edi,[esp+8]  ;; childtidptr
  test  esi,esi
  syscall
  ;; error?
  cp    eax,0xffff_f000
  jr    nc,.error
  ld    dword [pfa "(mt-active?)"],1
  ;; child?
  test  eax,eax
  jr    nz,.parent
  ;; setup TS
  popr  eax
  test  eax,eax
  jr    z,@f
  lea   eax,[eax*8+3]
  ld    ts,eax
@@:
  ;; we're in child: get cfa, jump there
  popr  eax
  ;; thread stack contains two args
  sub   esp,4
  pop   TOS
  jp    eax
.parent:
  ;; we're in parent
.error:
  pop   EIP
  pop   ERP
  add   esp,4*7
  ld    TOS,eax
  urnext
endcode

code: (trd-exit)  ( code -- )
  ;; free tls
  ld    ebp,TOS
  ld    ebx,gs:[ua_ofs_baseaddr]
  ld    ecx,gs:[ua_ofs_fullsize]
  ld    eax,91   ;; unmap
  syscall
  ld    eax,1    ;; exit
  ld    ebx,ebp  ;; let's hope it survived
  syscall
endcode
(hidden) (noreturn)
$endif

code: EXIT-THREAD  ( exitcode -- )
  mov   eax,1
  mov   ebx,TOS
  syscall
endcode
(noreturn)


voc-set-active FORTH

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
alias OS:GETTICKCOUNT MS@


;; returns current address if the request is invalid
code: (BRK)  ( newaddr -- newaddr )
  ld    eax,45  ;; brk
  ld    ebx,TOS ;; new address
  syscall
  ld    TOS,eax
  urnext
endcode

: (BRK-HERE)  ( -- curraddr )
  0 (brk)
;

;; throws OOM error
: BRK-ALLOC  ( size -- addr )
  dup 0< err-out-of-memory ?error
  (brk-here)  ( size addr )
  swap ?dup if  ( addr size )
    over + dup (brk)
    u< err-out-of-memory ?error
  endif
;
