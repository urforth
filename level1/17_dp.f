;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$constant "#dp-temp-sbuf" 8

: save-dp-temp  ( sbufaddr -- )
  (dp-temp-base) @ over ! cell+
  dp-temp @ swap !
;

: restore-dp-temp  ( sbufaddr -- )
  dup @ (dp-temp-base) ! cell+
  @ dp-temp !
;

: swap-dp-temp  ( sbufaddr -- )
  dup @ over cell+ @  ( sbufaddr base dptemp )
  rot save-dp-temp
  dp-temp ! (dp-temp-base) !
;

: alloc-dp-temp  ( size -- addr )
  4096 max 4095 + -4096 and dup os:prot-rwx os:mmap err-out-of-memory not-?error
  swap cell- over !
;

: free-dp-temp  ( addr -- )
  ?dup if dup @ os:munmap err-out-of-memory ?error endif
;

;; it should be allocated with "alloc-dp-temp"
: setup-dp-temp  ( addr -- )
  dup (dp-temp-base) ! cell+ dp-temp !
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; allocat dp-temp buffer if necessary, setup "dp-temp"
: (setup-dp-temp)  ( -- )
  (dp-temp-buf) @ ?dup ifnot
    0 ( minimum size) alloc-dp-temp
    dup (dp-temp-buf) !
  endif
  setup-dp-temp
; (hidden)

: (dp-temp-size)  ( -- size )  (dp-temp-base) @ @ ; (hidden)
: (dp-temp-end)  ( -- size )  (dp-temp-base) @ dup @ + ; (hidden)

: dp-temp-addr?  ( addr -- flag )  (dp-temp-base) @ dup if dup @ over + 1- bounds? else nip endif ;
: dp-temp-reset  ( -- ) dp-temp 0! (dp-temp-base) 0! ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; allocate some memory at HERE (DP or DP-TEMP), return starting address
: N-ALLOT  ( n -- start-addr )
  dup 0< err-negative-allot ?error
  dup 0x00ffffff u> err-out-of-memory ?error  ;; 16MB is quite huge allocation chunk ;-)
  (main-thread?) ERR-INVALID-THREAD not-?error
  dp-temp @ ?dup if  ;; allocate in dp-temp  ( n dpaddr )
    over (dp-temp-size) u>= err-out-of-memory ?error
    2dup + 128 + (dp-temp-end) u>= err-out-of-memory ?error
    swap dp-temp +!
  else  ;; allocate in normal dictionary
    dp @ 2dup + dp-last-addr @ 512 - u> err-out-of-memory ?error
    swap dp +!
  endif
;

: check-align-here  ( -- )
  here 3 and err-align-violation ?error
;

: align-here  ( -- )
  here 3 and ?dup if 4 swap - dup n-allot swap erase endif
;

: ALLOT  ( n -- )  n-allot drop ;

: USED  ( -- count )  dp @ (code-base-addr) - ;
: UNUSED  ( -- count )  dp-last-addr @ dp @ - 512 - 0 max ;  \ with some safety margin

: REAL-HERE  ( -- addr )  dp @ ;

code: HERE  ( -- addr )
  push  TOS
  ld    TOS,[pfa "dp-temp"]
  or    TOS,TOS
  cmovz TOS,[pfa "dp"]
  urnext
endcode


: (dp-protected?)  ( addr -- flag )
  (code-base-addr) dp-protected @ 1- bounds?
; (hidden)

;; "addr" is first unprotected addr
: (dp-protect)  ( addr -- )
  dp-protected @ umax dp-protected !
; (hidden)

: (dp-protect-cfa)  ( cfa -- )
  dup cfa-wsize + (dp-protect)
; (hidden)


;; pad is uservar
: PAD  ( -- addr )  pad-area @ ;

;; new thread has no pad
: pad-allocate  ( -- )
  pad ifnot
    #pad-area-resv #pad-area +
    os:get-pid os:get-tid = if
      ;; main thread
      brk-alloc
    else
      os:prot-r/w os:mmap ERR-OUT-OF-MEMORY not-?error
    endif
    #pad-area-resv + pad-area ! pad 1- (hld) !
  endif
;

;; should be called when thread exits
: pad-deallocate  ( -- )
  pad if
    os:get-pid os:get-tid = if
      pad-area @ #pad-area-resv -  #pad-area-resv #pad-area + os:munmap drop
      pad-area 0! (hld) 0!
    endif
  endif
;


: C,  ( c -- )
 $if URFORTH_DEBUG_INFO
  (dbginfo-add-here)
 $endif
  1 n-allot c!
;

: W,  ( w -- )
 $if URFORTH_DEBUG_INFO
  (dbginfo-add-here)
$endif
  2 n-allot w!
;

: ,  ( n -- )
 $if URFORTH_DEBUG_INFO
  (dbginfo-add-here)
$endif
  cell n-allot !
;

;; create a relocation for the given addr
: (rel-create)  ( addr -- )
  (code-base-addr) here 1- bounds? err-bad-reladdr not-?error
; (hidden)

;; this can be customized for special builds
;; use this instead of "," to compile address that needs a relocation
: reladdr,  ( addr -- )
  ( dup if) dup (code-base-addr) here 32767 + bounds? ( dup ifnot dbg endif) err-bad-reladdr not-?error ( endif)
  here swap , (rel-create)
;
