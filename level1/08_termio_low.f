;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$value "STDIN-FD"  0
$value "STDOUT-FD" 1
$value "STDERR-FD" 2

$variable "(STDTTY-EMIT-COL)" 0
(hidden)


code: (STDTTY-XEMIT)  ( ch -- )
  push  EIP
  push  TOS
.again:
  ld    eax,4    ;; function
  ld    ebx,[pfa "stdout-fd"]
  ld    ecx,esp  ;; address
  ld    edx,1    ;; length
  syscall
  cp    eax,-4
  jr    z,.again
  pop   TOS
  pop   EIP
.done:
  pop   TOS
  urnext
endcode
(hidden)

code: (STDTTY-XTYPE)  ( addr length -- )
  ;; positive length is guaranteed by the caller, but...
  ld    edx,TOS
  pop   ecx
  push  EIP
  cp    edx,0
  jr    le,.done
.again:
  push  ecx
  push  edx
  ld    ebx,[pfa "stdout-fd"]
  ld    eax,4
  syscall
  pop   edx
  pop   ecx
  cp    eax,-4
  jr    z,.again
  or    eax,eax
  jr    s,.done
  sub   edx,eax
  jr    be,.done
  add   ecx,eax
  jr    .again
.done:
  pop   EIP
  pop   TOS
  urnext
endcode

;; returns -1 on EOF, or [0..255]
code: (STDTTY-GETCH)  ( -- ch )
  push  TOS
  xor   eax,eax
  push  EIP
  push  eax
.again:
  ld    eax,3     ;; read
  ld    ebx,[pfa "stdin-fd"]
  ld    ecx,esp   ;; address
  ld    edx,1     ;; length
  syscall
  cp    eax,-4
  jr    z,.again
  pop   TOS       ;; read char
  pop   EIP
  cp    eax,1
  ld    ebx,-1
  cmovnz TOS,ebx
  urnext
endcode
(hidden)


: (STDTTY-RESET-EMIT-COL)  ( -- )  (stdtty-emit-col) 0! ;

: (STDTTY-EMIT-FIX-COL)  ( ch -- ch )
  dup case
    8 of (stdtty-emit-col) @ 1- 0 max (stdtty-emit-col) ! endof
    9 of (stdtty-emit-col) @ 7 or 1+ (stdtty-emit-col) ! endof
    10 of (stdtty-reset-emit-col) endof
    13 of (stdtty-reset-emit-col) endof
    127 of endof
    otherwise bl >= (stdtty-emit-col) +!  ;; FIXME: assumes that `true` is 1
  endcase
; (hidden)

: (STDTTY-BELL)    ( -- )       7 (stdtty-xemit) ; (hidden)
: (STDTTY-EMIT)    ( ch -- )    (stdtty-emit-fix-col) (stdtty-xemit) ; (hidden)
: (STDTTY-CR)      ( -- )       (stdtty-reset-emit-col) nl (stdtty-xemit) ; (hidden)
: (STDTTY-ENDCR)   ( -- )       (stdtty-emit-col) @ if (stdtty-cr) endif ; (hidden)
: (STDTTY-?ENDCR)  ( -- flag )  (stdtty-emit-col) @ 0<> ; (hidden)

: (STDTTY-TYPE)  ( addr length -- )
  ;; positive length is guaranteed by the caller, but...
  dup +if
    2dup bounds do i c@ (stdtty-emit-fix-col) drop loop
    (stdtty-xtype)
  else 2drop endif
; (hidden)


: (STDTTY-KEY?)  ( -- flag )
  os:#pollfd alloca >r 0
  begin
    drop stdin-fd r@ os:pollfd.fd !
    os:poll-in r@ os:pollfd.events !  ;; this also clears revents
    r@ 1 0 os:poll dup -4 <>
  until
  0> rdrop >r os:#pollfd dealloca r>
; (hidden)
