;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; swap bytes of the low word
;; high word is untouched
code: BSWAP-WORD  ( u -- u )
  xchg  cl,ch
  urnext
endcode


;; swap all dword bytes
code: BSWAP-DWORD  ( u -- u )
  bswap TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: CELLS  ( count -- count*4 )
  shl   TOS,2
  urnext
endcode

code: BYTES->CELLS  ( count -- [count+3]/4 )
  add   TOS,3
  shr   TOS,2
  urnext
endcode

code: +CELLS  ( addr count -- addr+count*4 )
  pop   eax
  lea   TOS,[eax+TOS*4]
  urnext
endcode

code: -CELLS  ( addr count -- addr-count*4 )
  pop   eax
  neg   TOS
  lea   TOS,[eax+TOS*4]
  urnext
endcode

code: CELL+  ( count -- count+4 )
  add   TOS,4
  urnext
endcode

code: CELL-  ( count -- count-4 )
  sub   TOS,4
  urnext
endcode

;; useful for array indexing
code: CELLS^  ( count addr -- addr+count*4 )
  pop   eax
  lea   TOS,[TOS+eax*4]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: 1+  ( n -- n+1 )
  inc   TOS
  urnext
endcode

code: 1-  ( n -- n-1 )
  dec   TOS
  urnext
endcode

code: 2+  ( n -- n+2 )
  add   TOS,2
  urnext
endcode

code: 2-  ( n -- n-2 )
  sub   TOS,2
  urnext
endcode

code: 4+  ( n -- n+4 )
  add   TOS,4
  urnext
endcode

code: 4-  ( n -- n-4 )
  sub   TOS,4
  urnext
endcode

code: 8+  ( n -- n+8 )
  add   TOS,8
  urnext
endcode

code: 8-  ( n -- n-8 )
  sub   TOS,8
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; same as 0=
code: NOT  ( n -- !n )
  test  TOS,TOS
  setz  cl
  movzx TOS,cl
  urnext
endcode

;; same as 0<>
code: NOTNOT  ( n -- !!n )
  test  TOS,TOS
  setnz cl
  movzx TOS,cl
  urnext
endcode

code: BITNOT  ( n -- ~n )
  not   TOS
  urnext
endcode

code: AND  ( n0 n1 -- n0&n1 )
  pop   eax
  xchg  eax,TOS
  and   TOS,eax
  urnext
endcode

code: SWAP-AND  ( n0 n1 -- n1&n0 )
  pop   eax
  and   TOS,eax
  urnext
endcode

code: ~AND  ( n0 n1 -- n0&~n1 )
  not   TOS
  pop   eax
  xchg  eax,TOS
  and   TOS,eax
  urnext
endcode

code: SWAP-~AND  ( n0 n1 -- n1&~n0 )
  pop   eax
  not   eax
  and   TOS,eax
  urnext
endcode

code: OR  ( n0 n1 -- n0|n1 )
  pop   eax
  or    TOS,eax
  urnext
endcode

code: XOR  ( n0 n1 -- n0^n1 )
  pop   eax
  xor   TOS,eax
  urnext
endcode


code: BIT-SET  ( u0 bitnum -- u0|[1<<bitnum] )
  pop   eax
  bts   eax,TOS
  ld    TOS,eax
  urnext
endcode

code: BIT-RESET  ( u0 bitnum -- u0&~[1<<bitnum] )
  pop   eax
  btr   eax,TOS
  ld    TOS,eax
  urnext
endcode

code: BIT?  ( u0 bitnum -- u0&~[1<<bitnum]<>0 )
  pop   eax
  bt    eax,TOS
  setc  cl
  movzx TOS,cl
  urnext
endcode


code: LOGAND  ( n0 n1 -- n0&&n1 )
  pop   eax
  xor   edx,edx
  xor   ebx,ebx
  test  eax,eax
  setnz dl
  test  TOS,TOS
  setnz bl
  ld    cl,dl
  and   cl,bl
  movzx TOS,cl
  urnext
endcode

code: LOGOR  ( n0 n1 -- n0||n1 )
  pop   eax
  xor   edx,edx
  or    TOS,eax
  setnz dl
  movzx TOS,dl
  urnext
endcode


code: LSHIFT  ( n0 n1 -- n0<<n1 )
  pop   eax
  cmp   TOS,32
  jr    nc,.zero
  ;; assume that TOS is in ECX
  shl   eax,cl
  mov   TOS,eax
  urnext
.zero:
  xor   TOS,TOS
  urnext
endcode

code: RSHIFT  ( n0 n1 -- n0>>n1 )
  pop   eax
  cmp   TOS,32
  jr    nc,.zero
  ;; assume that TOS is in ECX
  shr   eax,cl
  mov   TOS,eax
  urnext
.zero:
  xor   TOS,TOS
  urnext
endcode

code: ARSHIFT  ( n0 n1 -- n0>>n1 )
  pop   eax
  cmp   TOS,32
  jr    nc,.toobig
  ;; assume that TOS is in ECX
  sar   eax,cl
  mov   TOS,eax
  urnext
.toobig:
  mov   TOS,-1
  cmp   eax,0x80000000
  adc   TOS,0
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: LROTATE  ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   eax,cl
  ld    TOS,eax
  urnext
endcode

code: RROTATE  ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   eax,cl
  ld    TOS,eax
  urnext
endcode

code: LROTATE-WORD  ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   ax,cl
  ld    TOS,eax
  urnext
endcode

code: RROTATE-WORD  ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   ax,cl
  ld    TOS,eax
  urnext
endcode

code: LROTATE-BYTE  ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   al,cl
  ld    TOS,eax
  urnext
endcode

code: RROTATE-BYTE  ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   al,cl
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: +  ( n0 n1 -- n0+n1 )
  pop   eax
  add   TOS,eax
  urnext
endcode

code: -  ( n0 n1 -- n0-n1 )
  pop   eax
  ;; EAX=n0
  ;; TOS=n1
  sub   eax,TOS
  mov   TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: 2*  ( n -- n*2 )
  shl   TOS,1
  urnext
endcode

code: 2/  ( n -- n/2 )
  sar   TOS,1
  urnext
endcode


code: 2U*  ( n -- n*2 )
  shl   TOS,1
  urnext
endcode

code: 2U/  ( n -- n/2 )
  shr   TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: SGN  ( n -- -1/0/1 )
  jecxz @f
  test  TOS,TOS
  ld    TOS,1
  jr    ns,@f
  ld    TOS,-1
@@:
  urnext
endcode

;; avoid overflows
code: NCMP  ( n0 n1 -- -1/0/1 )
  pop   eax
  cp    eax,TOS
  ld    TOS,0
  jr    z,@f
  ld    TOS,1
  jr    g,@f
  ld    TOS,-1
@@:
  urnext
endcode

;; avoid overflows
code: UCMP  ( u0 u1 -- -1/0/1 )
  pop   eax
  cp    eax,TOS
  ld    TOS,0
  jr    z,@f
  ld    TOS,1
  jr    nc,@f
  ld    TOS,-1
@@:
  urnext
endcode

code: NEGATE  ( n -- -n )
  neg   TOS
  urnext
endcode

code: ABS  ( n -- |n| )
  test  TOS,TOS
  jr    ns,@f
  neg   TOS
@@:
  urnext
endcode


code: UMIN  ( u0 u1 -- umin )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    eax,TOS
  cmovc TOS,eax
  urnext
endcode

code: UMAX  ( u0 u1 -- umax )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    TOS,eax
  cmovc TOS,eax
  urnext
endcode

code: MIN  ( n0 n1 -- nmin )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    TOS,eax
  cmovg TOS,eax
  urnext
endcode

code: MAX  ( n0 n1 -- nmax )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    TOS,eax
  cmovl TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: C>S  ( n-8-bit -- n )
  movsx TOS,cl
  urnext
endcode

code: C>U  ( u-8-bit -- u )
  movzx TOS,cl
  urnext
endcode

code: W>S  ( n-16-bit -- n )
  movsx TOS,cx
  urnext
endcode

code: W>U  ( u-16-bit -- u )
  movzx TOS,cx
  urnext
endcode


;; with clamping
code: S>C  ( n -- n-8-bit )
  test  TOS,TOS
  jr    s,.negative
  cp    TOS,0x80
  jr    c,.done
  ld    cl,0x7f
  jr    .done
.negative:
  cp    TOS,0xffffff80
  jr    nc,.done
  ld    cl,0x80
.done:
  movsx TOS,cl
  urnext
endcode

;; with clamping
code: U>C  ( u -- u-8-bit )
  cp    TOS,0x100
  jr    c,.done
  ld    cl,0xff
.done:
  movzx TOS,cl
  urnext
endcode


;; with clamping
code: S>W  ( n -- n-16-bit )
  test  TOS,TOS
  jr    s,.negative
  cp    TOS,0x8000
  jr    c,.done
  ld    cx,0x7fff
  jr    .done
.negative:
  cp    TOS,0xffff8000
  jr    nc,.done
  ld    cx,0x8000
.done:
  movsx TOS,cx
  urnext
endcode

;; with clamping
code: U>W  ( u -- u-16-bit )
  cp    TOS,0x10000
  jr    c,.done
  mov   cx,0xffff
.done:
  movzx TOS,cx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 32-bit integer hash
;; http://burtleburtle.net/bob/hash/integer.html
(*
code: U32HASH  ( u -- u )
  ;; a -= (a<<6);
  ld    eax,TOS
  shl   eax,6
  sub   TOS,eax
  ;; a ^= (a>>17);
  ld    eax,TOS
  shr   eax,17
  xor   TOS,eax
  ;; a -= (a<<9);
  ld    eax,TOS
  shl   eax,9
  sub   TOS,eax
  ;; a ^= (a<<4);
  ld    eax,TOS
  shl   eax,4
  xor   TOS,eax
  ;; a -= (a<<3);
  ld    eax,TOS
  shl   eax,3
  sub   TOS,eax
  ;; a ^= (a<<10);
  ld    eax,TOS
  shl   eax,10
  xor   TOS,eax
  ;; a ^= (a>>15);
  ld    eax,TOS
  shr   eax,15
  xor   TOS,eax
  urnext
endcode
*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 32-bit integer hash
;; http://burtleburtle.net/bob/hash/integer.html
code: U32HASH  ( u -- u )
  ;; a = (a+0x7ed55d16)+(a<<12);
  ld    eax,TOS
  add   TOS,0x7ed55d16
  shl   eax,12
  add   TOS,eax
  ;; a = (a^0xc761c23c)^(a>>19);
  ld    eax,TOS
  xor   TOS,0xc761c23c
  shr   eax,19
  xor   TOS,eax
  ;; a = (a+0x165667b1)+(a<<5);
  ld    eax,TOS
  add   TOS,0x165667b1
  shl   eax,5
  add   TOS,eax
  ;; a = (a+0xd3a2646c)^(a<<9);
  ld    eax,TOS
  add   TOS,0xd3a2646c
  shl   eax,9
  xor   TOS,eax
  ;; a = (a+0xfd7046c5)+(a<<3);
  ld    eax,TOS
  add   TOS,0xfd7046c5
  shl   eax,3
  add   TOS,eax
  ;; a = (a^0xb55a4f09)^(a>>16);
  ld    eax,TOS
  xor   TOS,0xb55a4f09
  shr   eax,16
  xor   TOS,eax
  urnext
endcode

;; fold 32-bit hash to 16-bit hash
code: UHASH32->16  ( u32hash -- u16hash )
  ld    eax,TOS
  shr   eax,16
  add   ecx,eax
  movzx TOS,cx
  urnext
endcode

code: UHASH16->8  ( u16hash -- u8hash )
  add   cl,ch
  movzx TOS,cl
  urnext
endcode

code: UHASH32->8  ( u32hash -- u8hash )
  ld    eax,TOS
  shr   eax,16
  add   ecx,eax
  add   cl,ch
  movzx TOS,cl
  urnext
endcode
