;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; fun observation: cmov seems to be slower than jumps
URF_USE_CMOV_IN_BRANCHES equ 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: NOOP  ( -- )
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: LIT-FALSE  ( -- 0 )
  push  TOS
  xor   TOS,TOS
  urnext
endcode
(hidden)

code: LIT-TRUE  ( -- 1 )
  push  TOS
  xor   TOS,TOS
  inc   TOS
  urnext
endcode
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: LIT0  ( -- 0 )
  push  TOS
  xor   TOS,TOS
  urnext
endcode
(hidden)

code: LIT1  ( -- 1 )
  push  TOS
  xor   TOS,TOS
  inc   TOS
  urnext
endcode
(hidden)

code: LIT-1  ( -- -1 )
  push  TOS
  xor   TOS,TOS
  dec   TOS
  urnext
endcode
(hidden)

code: LIT  ( -- n )
  push  TOS
  lodsd
  ld    TOS,eax
  urnext
endcode
(arg-lit) (hidden)

$ifnot URFORTH_ALIGN_PFA
code: LITU8  ( -- n )
  push  TOS
  lodsb
  movzx TOS,al
  urnext
endcode
(arg-u8) (hidden)

code: LITS8  ( -- n )
  push  TOS
  lodsb
  movsx TOS,al
  urnext
endcode
(arg-s8) (hidden)

code: LITU16  ( -- n )
  push  TOS
  lodsw
  movzx TOS,ax
  urnext
endcode
(arg-u16) (hidden)

code: LITS16  ( -- n )
  push  TOS
  lodsw
  movsx TOS,ax
  urnext
endcode
(arg-s16) (hidden)
$endif


code: LITC4STR  ( -- addr count )
  push  TOS
  lodsd
  push  EIP
  mov   TOS,eax
  add   EIP,eax
  ;; skip trailing zero and align
  or    EIP,3
  inc   EIP
  urnext
endcode
(arg-c4strz) (hidden)

code: LITC1STR  ( -- addr count )
  push  TOS
  lodsb
  movzx TOS,al
  push  EIP
  add   EIP,TOS
  ;; skip trailing zero and align
  or    EIP,3
  inc   EIP
  urnext
endcode
(arg-c1strz) (hidden)

code: LITCFA
  push  TOS
  lodsd
  mov   TOS,eax
  urnext
endcode
(arg-cfa) (hidden)

code: LITCBLOCK
  ;; next cell is continue address
  ;; leave next next cell address as cfa
  push  TOS
  lodsd
  ld    TOS,EIP
  ld    EIP,eax
  urnext
endcode
(arg-cblock) (hidden)

;; used in "TO"
code: LITTO!  ( value -- )
  lodsd
  add   eax,URFORTH_CFA_SIZE  ;; skip cfa
  ld    [eax],TOS
  pop   TOS
  urnext
endcode
(arg-cfa) (hidden)

;; used in "TO"
code: LIT^TO  ( -- dataaddr )
  lodsd
  add   eax,URFORTH_CFA_SIZE  ;; skip cfa
  push  TOS
  ld    TOS,eax
  urnext
endcode
(arg-cfa) (hidden)

;; used in "+TO"
code: LIT+TO!  ( value -- )
  lodsd
  add   eax,URFORTH_CFA_SIZE  ;; skip cfa
  add   [eax],TOS
  pop   TOS
  urnext
endcode
(arg-cfa) (hidden)

;; used in "-TO"
code: LIT-TO!  ( value -- )
  lodsd
  add   eax,URFORTH_CFA_SIZE  ;; skip cfa
  sub   [eax],TOS
  pop   TOS
  urnext
endcode
(arg-cfa) (hidden)

code: LIT-EXECTAIL  ( -- )
  lodsd
  popr  EIP
  jp    eax
endcode
(arg-cfa) (noreturn) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: BRANCH
  lodsd
  mov   EIP,eax
  urnext
endcode
(arg-branch) (hidden)

code: 0BRANCH
  lodsd
  test  TOS,TOS
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovz EIP,eax
 $else
  jr    nz,@f
  mov   EIP,eax
@@:
 $endif
  urnext
endcode
(arg-branch) (hidden)

code: TBRANCH
  lodsd
  test  TOS,TOS
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovnz EIP,eax
 $else
  jr    z,@f
  mov   EIP,eax
@@:
 $endif
  urnext
endcode
(arg-branch) (hidden)

;; branch if positive or zero
code: +0BRANCH
  lodsd
  cp    TOS,0
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovge EIP,eax
 $else
  jr    l,@f
  ld    EIP,eax
@@:
 $endif
  urnext
endcode
(arg-branch) (hidden)

;; branch if negative or zero
code: -0BRANCH
  lodsd
  cp    TOS,0
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovle EIP,eax
 $else
  jr    g,@f
  ld    EIP,eax
@@:
 $endif
  urnext
endcode
(arg-branch) (hidden)

;; branch if positive (not zero)
code: +BRANCH
  lodsd
  cp    TOS,0
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovg EIP,eax
 $else
  jr    le,@f
  ld    EIP,eax
@@:
 $endif
  urnext
endcode
(arg-branch) (hidden)

;; branch if negative (not zero)
code: -BRANCH
  lodsd
  cp    TOS,0
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovl EIP,eax
 $else
  jr    ge,@f
  ld    EIP,eax
@@:
 $endif
  urnext
endcode
(arg-branch) (hidden)

;; used in "CASE": drops additional value if branch is NOT taken
code: 0BRANCH-DROP
  lodsd
  test  TOS,TOS
  pop   TOS
 $if URF_USE_CMOV_IN_BRANCHES
  cmovz EIP,eax
  jr    z,@f
  ;; branch not taken, drop one more data value
  pop   TOS
@@:
 $else
  jr    nz,@f
  mov   EIP,eax
  urnext
@@:
  ;; branch not taken, drop one more data value
  pop   TOS
 $endif
  urnext
endcode
(arg-branch) (hidden)

;; if two values on the stack are equal, drop them, and take a branch
;; if they aren't equal, do nothing
code: ?DO-BRANCH
  lodsd
  cp    TOS,[esp]
  jr    nz,@f
  ;; values are equal, drop them, and take a branch
  pop   TOS
  pop   TOS
  mov   EIP,eax
@@:
  urnext
endcode
(arg-branch) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: EXECUTE
  mov   eax,TOS
  pop   TOS
  jp    eax
endcode

;; tail call (will not return to the caller)
code: EXECUTE-TAIL
  mov   eax,TOS
  popr  EIP
  pop   TOS
  jp    eax
endcode
(noreturn)

code: @EXECUTE
  mov   eax,[TOS]
  pop   TOS
  jp    eax
endcode

;; tail call (will not return to the caller)
code: @EXECUTE-TAIL
  mov   eax,[TOS]
  popr  EIP
  pop   TOS
  jp    eax
endcode
(noreturn)

code: OVERRIDE-EXECUTE  ( ... xtoken -- ... )
  mov   eax,TOS
  pop   TOS
  pushr EIP
  ld    EIP,eax
  urnext
endcode

code: EXIT
  popr  EIP
  urnext
endcode
(noreturn)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is support for "FOR"
;; it checks if the value on the stack is positive
;; if it is not (or zero), it drops the value, and jumps
;; if the value is positive, it prepares the stack in the
;; same manner as "(DO)" does
code: (FOR)
  lodsd
  cp    TOS,1
  jr    ge,.initloop
  ;; skip it
  mov   EIP,eax
  jr    .done
.initloop:
  ;; prepare stack as "(DO)" does (so `I` and `(LOOP)` could work)
  ld    edx,0x80000000
  sub   edx,TOS
  sub   ERP,4+4
  ld    [ERP],edx
  ld    [ERP+4],edx
.done:
  pop   TOS
  urnext
endcode
(arg-branch) (hidden)

;; loops from start to limit-1
code: (DO)  ( limit start -- | limit counter )
  ;; ANS loops
  pop   eax
  ld    edx,0x80000000
  sub   edx,eax
  add   TOS,edx
  sub   ERP,4+4
  ld    [ERP+4],edx  ;; 80000000h-to
  ld    [ERP],TOS    ;; 80000000h-to+from
  pop   TOS
  urnext
endcode
(hidden)

code: (+LOOP)  ( delta -- | limit counter )
  ;; ANS loops
  ;; most of the time we need jump address, so always load it
  ;; it also frees us from "add EIP,4" on exit
  lodsd
  add   TOS,[ERP]
  jr    o,.done
  ;; next iteration
  ld    [ERP],TOS
  mov   EIP,eax
  pop   TOS
  urnext
.done:
  add   ERP,4+4
  pop   TOS
  urnext
endcode
(arg-branch) (hidden)

code: (LOOP)  ( -- | limit counter )
  ;; ANS loops
  ;; this is faster version of "(+LOOP)"
  ;; most of the time we need jump address, so always load it
  ;; it also frees us from "add EIP,4" on exit
  lodsd
  add   dword [ERP],1
  jr    o,.done
  ;; next iteration
  mov   EIP,eax
  urnext
.done:
  add   ERP,4+4
  urnext
endcode
(arg-branch) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; as i moved high-level compiler out of the core, we should do it there
;;alias "BREAK" cfa "LEAVE"
;;immediate

;; removes loop arguments from return stack
;; can be used as: UNLOOP EXIT
;; "BREAK" compiles this word before branching out of the loop
code: UNLOOP  ( | limit counter -- )
  add   ERP,4+4
  urnext
endcode

code: I  ( -- counter )
  push  TOS
  ld    TOS,[ERP]
  sub   TOS,[ERP+4]
  urnext
endcode

code: J  ( -- counter )
  push  TOS
  ld    TOS,[ERP+8]
  sub   TOS,[ERP+8+4]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: TRUE   ( -- 1 )  state @ if compile lit-true else 1 endif ; immediate
: FALSE  ( -- 0 )  state @ if compile lit-false else 0 endif ; immediate
