;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: NULLSTRING  ( addr 0 -- )
  push  TOS
  push  nullstring_data
  xor   TOS,TOS
  urnext
nullstring_data: dw 0,0
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; length of asciiz string
code: ZCOUNT  ( addr -- addr count )
  push  TOS
  mov   edi,TOS
  xor   al,al
  cp    [edi],al
  jr    nz,@f
  xor   TOS,TOS
  urnext
@@:
  mov   edx,-1
  mov   ecx,edx
  repnz scasb
  sub   edx,ecx
  mov   ecx,edx
  dec   ecx
  urnext
endcode

: zcount-only  ( addr -- count )
  zcount nip
;


alias @ COUNT-ONLY  ( addr -- count )

code: COUNT  ( addr -- addr+4 count )
  ;;UF dup count_only swap cellinc swap exit
  ld    eax,[TOS]
  add   TOS,4
  push  TOS
  ld    TOS,eax
  urnext
endcode


alias W@ WCOUNT-ONLY  ( addr -- count )

code: WCOUNT  ( addr -- addr+2 count )
  movzx eax,word [TOS]
  add   TOS,2
  push  TOS
  mov   TOS,eax
  urnext
endcode

alias C@ CCOUNT-ONLY  ( addr -- count )
alias C@ BCOUNT-ONLY  ( addr -- count )

code: BCOUNT  ( addr -- addr+1 count )
  movzx eax,byte [TOS]
  inc   TOS
  push  TOS
  mov   TOS,eax
  urnext
endcode

alias BCOUNT CCOUNT


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adjust the character string at c-addr1 by n characters.
;; the resulting character string, specified by c-addr2 u2,
;; begins at c-addr1 plus n characters and is u1 minus n characters long.
;; doesn't check length, allows negative n.
: /STRING  ( c-addr1 u1 n -- c-addr2 u2 )
  dup >r - swap r> + swap
;

;; checks length, doesn't strip anything from an empty string
: /CHAR  ( c-addr1 u1 -- c-addr+1 u1-1 )
  1- dup -if drop 0 else swap 1+ swap endif
;

;; checks length, doesn't strip anything from an empty string
: /2CHARS  ( c-addr1 u1 -- c-addr+2 u1-2 )
  2- dup -if drop 0 else swap 2+ swap endif
;


;; if n1 is greater than zero, n2 is equal to n1 less the number of
;; spaces at the end of the character string specified by c-addr n1.
;; if n1 is zero or the entire string consists of spaces, n2 is zero.
: -TRAILING  ( c-addr n1 -- c-addr n2 )
  0 max begin dup while 2dup + 1- c@ bl <= while 1- repeat
;

;; strips leading blanks
: -LEADING  ( c-addr1 n1 -- c-addr2 n2 )
  0 max begin dup while over c@ bl <= while /char repeat
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; converts some escape codes in-place
;; used for `."` and `"`
;; the resulting string is never bigger than a source one
;; this will not preserve the trailing zero byte
code: STR-UNESCAPE  ( addr count -- addr count )
  jecxz .quit
  ld    edi,[esp]
  push  ecx

.scanloop:
  ld    al,92
  repnz scasb
  jecxz .done  ;; nope
  jr    nz,.done
  ;; edi is after backslash
  ;; ecx is number of chars left after backslash
  ;; found backslash, check next char
  movzx eax,byte [edi]
  ld    ah,al
  or    ah,32  ;; lowercase it
  ;; '\r'?
  cp    ah,'r'
  jr    nz,@f
  ld    al,13
  jr    .replace_one
@@:
  ;; '\n'?
  cp    ah,'n'
  jr    nz,@f
  ld    al,10
  jr    .replace_one
@@:
  ;; '\t'?
  cp    ah,'t'
  jr    nz,@f
  ld    al,9
  jr    .replace_one
@@:
  ;; '\b'? (bell)
  cp    ah,'b'
  jr    nz,@f
  ld    al,7
  jr    .replace_one
@@:
  ;; '\e'? (escape)
  cp    ah,'e'
  jr    nz,@f
  ld    al,27
  jr    .replace_one
@@:
  ;; '\z'? (zero)
  cp    ah,'z'
  jr    nz,@f
  xor   al,al
  jr    .replace_one
@@:
  ;; '\`'? (double quote)
  cp    ah,'`'
  jr    nz,@f
  ld    al,'"'  ;; '
  jr    .replace_one
@@:
  ;; 'xHH'?
  cp    ah,'x'
  jr    z,.esc_hex
  jr    .replace_one
.loop_cont:
  dec   ecx
  jr    nz,.scanloop

.done:
  pop   ecx
.quit:
  urnext

.replace_one:
  ld    [edi-1],al
  dec   dword [esp]
  dec   ecx
  jecxz .done
  ;; move
  push  esi
  push  edi
  push  ecx
  ld    esi,edi
  inc   edi
.domove:
  xchg  esi,edi
  rep movsb
  pop   ecx
  pop   edi
  pop   esi
  jr    .scanloop

.esc_hex:
  cp    ecx,2
  jr    c,.loop_cont
  movzx eax,byte [edi+1]
  call  .hexdigit
  jr    c,.loop_cont
  ;; save original string position
  push  esi
  ld    esi,edi
  ;; skip 'x'
  inc   edi   ;; skip 'x'
  dec   ecx
  dec   dword [esp+4]
  ;; skip first digit
  inc   edi
  dec   ecx
  dec   dword [esp+4]
  jecxz .esc_hex_done
  ld    ah,al
  ld    al,[edi]
  call  .hexdigit
  jr    nc,@f
  ld    al,ah
  jr    .esc_hex_done
@@:
  ;; combine two hex digits
  shl   ah,4
  or    al,ah
  ;; skip second digit
  inc   edi
  dec   ecx
  dec   dword [esp+4]
.esc_hex_done:
  ld    [esi-1],al
  jecxz .esc_hex_quit
  ;; remove leftover chars
  ;; ECX: chars left
  ;; ESI: position after backslash
  ;; EDI: rest position
  ;; old ESI is on the stack
  push  esi   ;; to be restored in EDI
  push  ecx
  xchg  esi,edi
  rep movsb
  pop   ecx
  pop   edi   ;; get back to backslash
  pop   esi   ;; restore old ESI
  jp    .scanloop

.esc_hex_quit:
  pop   esi
  jr    .done

.hexdigit:
  sub   al,'0'
  jr    c,.hexdigit_done
  cp    al,10
  ccf
  jr    nc,.hexdigit_done
  sub   al,7
  jr    c,.hexdigit_done
  cp    al,16
  ccf
  jr    nc,.hexdigit_done
  ;; maybe its lowercase?
  cp    al,42
  jr    c,.hexdigit_done
  sub   al,32
  cp    al,16
  ccf
.hexdigit_done:
  ret
endcode
