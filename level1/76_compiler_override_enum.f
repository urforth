;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; word overriding mechanics
;; this is different from REPLACE -- you can call old word if you want to

;; get xtoken for the previous override, to use in override chain
;; if the word is not overriden, returns 0
: (GET-OVERRIDE-OLDCFA)  ( oldcfa -- xtoken // 0 )
  dup word-type? WORD-TYPE-OVERRIDEN =
  if 1+ compiler:(disp32@) cfa->pfa else drop 0 endif
;

: (OVERRIDE-OLDCFA-NEWCFA)  ( oldcfa newcfa -- )
  ;; check if newcfa is a Forth word
  dup word-type? WORD-TYPE-FORTH <> err-cannot-override ?error
  ;; check if oldcfa is a Forth word, or an overriden word
  ;; override chain works right for overriden words
  over word-type? dup WORD-TYPE-FORTH =
  if  ( oldcfa newcfa wtype )
    drop  over forth:(dp-protected?) over forth:(dp-protected?) or if 2dup umax forth:(dp-protect-cfa) endif
    ;; fix new word CFA to use par_urforth_nocall_dooverride
    (URFORTH-DOOVERRIDE-ADDR) over compiler:(call!)
    ;; fix old word CFA so it will call new CFA instead
    swap compiler:(call!)
  else  ( oldcfa newcfa wtype )
    ;; override chain
    WORD-TYPE-OVERRIDEN <> err-cannot-override ?error
    over forth:(dp-protected?) over forth:(dp-protected?) or if 2dup umax forth:(dp-protect-cfa) endif
    ;; fix new word CFA to use par_urforth_nocall_dooverride
    (URFORTH-DOOVERRIDE-ADDR) over compiler:(call!)
    ;; fix old word CFA so it will call new CFA instead
    swap compiler:(call!)
  endif
;

: OVERRIDE  ( -- )  \ oldword newword
  -find-required
  -find-required
  (override-oldcfa-newcfa)
;

;; get xtoken for the previous override, to use in override chain
;; if the word is not overriden, returns 0
: GET-OVERRIDE  ( -- xtoken // 0 )  \ oldword
  -find-required
  (get-override-oldcfa)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; enums
;; this is wrong place for this code, but meh...

vocabulary-nohash (enums) (hidden)
voc-set-active (enums)

;; etype: 0 -- bitenum, otherwise delta

: value  ( etype evalue -- etype enextvalue )  \ name
  dup constant
  over ?dup if + else ?dup if 1 lshift else 1 endif endif
;

: }  ( etype evalue -- )
  2drop previous
;

: set      ( etype evalue newvalue -- etype newvalue )  nip ;
: set-bit  ( etype evalue newbit -- etype 1<<newbit )  nip 1 swap lshift ;
: -set     ( etype evalue delta -- etype evalue-delta ) - ;
: +set     ( etype evalue delta -- etype evalue+delta ) + ;

voc-set-active forth

: enum{  ( -- etype enextvalue )  1 0 also (enums) ;
: bitenum{  ( -- etype enextvalue )  0 1 also (enums) ;
