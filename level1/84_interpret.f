;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; scattered extensions should simply pass "addr count" unchanged (or changed ;-)
;; chain handler should perform "exit" on success
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: INTERPRET-WFIND  ( addr count -- cfa 1 // cfa -1 // addr count false )
  ... 2dup wfind dup if 2swap 2drop endif
;

;; scattered extensions should simply pass "addr count" unchanged (or changed ;-)
;; chain handler should perform "exit" on success
: INTERPRET-NOT-FOUND  ( addr count -- true // addr count false ) ... false ;

;; count cannot be negative
;; count should be 0 to EOF (INTERPRET will call refill in this case)
: INTERPRET-NEXT-WORD  ( -- addr count )
  ... parse-skip-comments parse-name
;

;; helper for common vocab search tech used in uroof and locals
;; it may be useful for other modules too, so why don't put it here?
;; vocid non-hidden non-immediate methods:
;;   ( -- addr count false // true ) -- return word name to interpret on "false"
;; special method:
;;  ( addr count -- addr count false // true )
;; specialcfa can be 0
;; use it like:
;;   vocid: myspecials ['] myother (intepret-voc-call-helper) if exit endif
;;
: (intepret-voc-call-helper)  ( addr count vocid specialcfa -- addr count false // cfa 1 true )
  >r voc-search-noimm if rdrop else r> dup ifnot exit endif endif
  execute dup if drop ['] noop 1 true endif  ;; run "noop" as immediate, and exit from the caller
;

: INTERPRET ( ??? )
  begin
    (sp-check) ifnot err-stack-underflow error 1 n-bye ( just in case ) endif
    ;; read name, ignoring comments, and refilling if necessary
    begin interpret-next-word dup not-while
      2drop
      ;; for default TIB (i.e. terminal session), "QUIT" will do refill for us
      tib-default? if exit endif
      refill ifnot exit endif
    repeat
    ( addr count )
    interpret-wfind dup ifnot
      ;; unknown word, try to parse it as a number
      drop 2dup number if
        nrot 2drop
        [compile] literal
      else
        interpret-not-found ifnot
          ?endcr if space endif type err-unknown-word error
        endif
      endif
    else  ;; i found her!
      ( cfa immflag[1] )
      0> state @ 0= or if execute else compile, endif
    endif
  again
;


: (.OK)  ( -- )
  (sp-check) ?endcr if space endif
  if state @ if ." [ok]" else ." ok" endif
  else err-stack-underflow error-message state 0! endif
  false >r  ;; "bracket opened" flag
  ;; show stack depth
  depth ?dup if
    r> 1+ >r
    ."  ("
    base @ >r 0 .r r> base !
  endif
  ;; show BASE if it is not decimal
  base @ 10 <> if
    r> ifnot ."  (" endif
    true >r
    ." ; base:" base @ dup >r decimal 0 .r r> base !
  endif
  r> if [char] ) emit endif
  cr
;

: .OK ... (.OK) ;

: QUIT  ( ??? )
  begin
    rp0!
    ;;state 0!  ;; nope, we want multiline definitions to work
    .ok
    tib-reset
    ;; there is no reason to keep debug info activated
    (dbginfo-reset)
    tload-verbose-default to tload-verbose
    refill ifnot bye endif
    interpret
  again
; (noreturn)


: EVALUATE  ( addr count -- ... )
  dup +if
    tibstate>r
    tib-set-to
    interpret
    r>tibstate
  else
    2drop
  endif
;
