;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: SP@  ( -- sp )
  push  TOS
  mov   TOS,esp
  urnext
endcode

code: SP!  ( n -- )
  mov   esp,TOS
  pop   TOS
  urnext
endcode

code: SP0!  ( -- )
  mov   esp,ts:[ua_ofs_sp0]
  xor   TOS,TOS
  urnext
endcode

code: (SP-CHECK)  ( -- ok-flag )
  cp    esp,ts:[ua_ofs_sp0]
  jr    na,.ok
  mov   esp,ts:[ua_ofs_sp0]
  xor   TOS,TOS
  push  TOS
  urnext
.ok:
  push  TOS
  mov   TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DUP  ( n -- n n )
  push  TOS
  urnext
endcode

code: 2DUP  ( n0 n1 -- n0 n1 n0 n1 )
  mov   eax,[esp]
  ;; TOS: n1
  ;; EAX: n0
  push  TOS
  push  eax
  urnext
endcode

code: ?DUP  ( n0 -- n0 n0  ||  0 -- 0 )
  jecxz @f
  push  TOS
@@:
  urnext
endcode

code: DROP  ( n0 -- )
  pop   TOS
  urnext
endcode

code: 2DROP  ( n0 n1 -- )
  pop   TOS
  pop   TOS
  urnext
endcode

code: SWAP  ( n0 n1 -- n1 n0 )
  xchg  [esp],TOS
  urnext
endcode

code: 2SWAP  ( n0 n1 n2 n3 -- n2 n3 n0 n1 )
  ;; TOS=n3
  pop   eax       ;; EAX=n2
  pop   edx       ;; FRG=n1
  xchg  [esp],eax ;; EAX=n0
  push  TOS
  push  eax
  mov   TOS,edx
  urnext
endcode

code: OVER  ( n0 n1 -- n0 n1 n0 )
  push  TOS
  mov   TOS,[esp+4]
  urnext
endcode

code: 2OVER  ( n0 n1 n2 n3 -- n0 n1 n2 n3 n0 n1 )
  ;; TOS=n3
  push  TOS
  mov   eax,[esp+12]
  mov   TOS,[esp+8]
  push  eax
  urnext
endcode

code: ROT  ( n0 n1 n2 -- n1 n2 n0 )
  pop   edx
  pop   eax
  push  edx
  push  TOS
  mov   TOS,eax
  urnext
endcode

code: NROT  ( n0 n1 n2 -- n2 n0 n1 )
  pop   edx
  pop   eax
  push  TOS
  push  eax
  mov   TOS,edx
  urnext
endcode

alias NROT -ROT


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SWAP DROP
code: NIP  ( n1 n2 -- n2 )
  pop   eax
  urnext
endcode

;; SWAP OVER
code: TUCK  ( n1 n2 -- n2 n1 n2 )
  pop   eax
  push  TOS
  push  eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DEPTH  ( -- stack-depth-before-this-call )
  push  TOS
  ld    TOS,ts:[ua_ofs_sp0]
  sub   TOS,esp
  sar   TOS,2
  dec   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; remove idx, copy nth item; 0 PICK is the same as DUP
code: PICK  ( ... idx -- ... n[top-idx-1] )
  ld    TOS,[esp+TOS*4]
  urnext
endcode

alias PICK PEEK

;; remove idx and val, set nth item (numbered as in PICK)
code: POKE  ( ... val idx -- ... )
  pop   eax
  ld    [esp+TOS*4],eax
  pop   TOS
  urnext
endcode

;; remove idx, move item; 0 ROLL is the same as NOOP
code: ROLL  ( ... idx -- ... n[top-idx-1] )
  jecxz .quit
  ld    edx,esi
  ld    eax,[esp+TOS*4]
  lea   esi,[esp+TOS*4]
  ld    edi,esi
  sub   esi,4
  std
  rep movsd
  cld
  ld    esi,edx
  ld    [esp],eax
.quit:
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: ALLOCA  ( bytes -- addr )
  add   TOS,3
  and   TOS,-4
  sub   esp,TOS
  ld    TOS,esp
  urnext
endcode

code: DEALLOCA  ( bytes -- )
  add   TOS,3
  and   TOS,-4
  add   esp,TOS
  pop   TOS
  urnext
endcode
