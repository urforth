;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; default abort calls ABORT-CLEANUP, and then MAIN-LOOP
;; threading code replaces it
$uservar "(ABORT-CLEANUP-PTR)" ua_ofs_abort_cleanup_ptr 0

;; ye good olde ABORT, vectorized
;; this word should never return
;; note that by default it cleans exception frames, and goes straight to QUIT
$uservar "(ABORT-PTR)" ua_ofs_abort_ptr 0

;; called when the system needs to abort with error message
;; this word should never return
;; it uses THROW to throw an error
;; ( errcode )
$uservar "(ERROR-PTR)" ua_ofs_error_ptr 0

;; this one completely ignores any exceptions, cleans exception frames, and aborts
;; ( errcode )
$uservar "(FATAL-ERROR-PTR)" ua_ofs_fatal_error_ptr 0

;; this is called to reset FPU
$defer "FPU-RESET" cfa "noop"

$value "ERROR-TYPE-LINE?" 1

(*
$constant "(#user-abort-msg)" 63
$uservar "(user-abort-msg)" ua_ofs_user_abort_msg 0
(hidden)
$userallot 60
*)
$constant "(#user-abort-msg)" 4000
$uservar "(user-abort-msg-page)" ua_ofs_user_abort_msg_page 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; you can register your thread inits here
;; WARNING! registering is system-wide!
: (thread-init)  ( -- )  ... ; (hidden)

;; you can register your thread cleanups here
;; WARNING! registering is system-wide!
: (thread-cleanup)  ( -- )  ... ; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "ERR-UNKNOWN-WORD"                   -1
$constant "ERR-STACK-UNDERFLOW"                -2
$constant "ERR-STACK-OVERFLOW"                 -3
$constant "ERR-R-STACK-UNDERFLOW"              -4
$constant "ERR-R-STACK-OVERFLOW"               -5
$constant "ERR-OUT-OF-MEMORY"                  -6
$constant "ERR-WORD-REDEFINED"                 -7
$constant "ERR-FILE-NOT-FOUND"                 -8
$constant "ERR-FILE-READ-ERROR"                -9
$constant "ERR-FILE-WRITE-ERROR"              -10
$constant "ERR-FILE-TOO-BIG"                  -11
$constant "ERR-COMPILATION-ONLY"              -12
$constant "ERR-EXECUTION-ONLY"                -13
$constant "ERR-UNPAIRED-CONDITIONALS"         -14
$constant "ERR-UNFINISHED-DEFINITION"         -15
$constant "ERR-IN-PROTECTED-DICT"             -16  ;; for "forget" (which will prolly never materialize)
$constant "ERR-NOT-DEFER"                     -17
$constant "ERR-INVALID-WORD-NAME"             -18
$constant "ERR-WORD-EXPECTED"                 -19
$constant "ERR-USER-ABORT"                    -20
$constant "ERR-BAD-RELADDR"                   -21
$constant "ERR-INVALID-MALLOC"                -22
$constant "ERR-VOCABULARY-STACK-OVERFLOW"     -23
$constant "ERR-CHAR-EXPECTED"                 -24
$constant "ERR-STRING-EXPECTED"               -25
$constant "ERR-NUMBER-EXPECTED"               -26
$constant "ERR-VOCABULARY-EXPECTED"           -27
$constant "ERR-INVALID-BREAK-CONT"            -28
$constant "ERR-NONNAKED-SYSTEM"               -29
$constant "ERR-NOT-IMPLEMENTED"               -30
$constant "ERR-NEGATIVE-ALLOT"                -31
$constant "ERR-NO-TEMP-HERE"                  -32
$constant "ERR-TEMP-HERE-ALREADY"             -33
$constant "ERR-CANNOT-OVERRIDE"               -34
$constant "ERR-CANNOT-REPLACE"                -35
$constant "ERR-INPUT-TOO-LONG"                -36
$constant "ERR-THROW-WITHOUT-CATCH"           -37
$constant "ERR-THROW-CHAIN-CORRUPTED"         -38
$constant "ERR-UNBALANCED-IFDEF"              -39
$constant "ERR-STRING-TOO-LONG"               -40
$constant "ERR-SEGFAULT"                      -41
$constant "ERR-MATH-OVERFLOW"                 -42
$constant "ERR-MATH-ZERO-DIVIDE"              -43
$constant "ERR-SAVE-CHAIN-STACK"              -44
$constant "ERR-FSTACK-UNDERFLOW"              -45
$constant "ERR-UNKNOWN-LIBRARY"               -46
$constant "ERR-ELLIPSIS-FORTH"                -47
$constant "ERR-ELLIPSIS-FIRST"                -48
$constant "ERR-INVALID-INPUT-SIZE"            -49
$constant "ERR-OUT-OF-USER-AREA"              -50
$constant "ERR-INVALID-THREAD"                -51
$constant "ERR-DISPATCH-ERROR"                -52
$constant "ERR-NONMACRO-ONLY"                 -53
$constant "ERR-UNDEFINED-INSTRUCTION"         -54
$constant "ERR-ALIGNED-NAME-TOO-LONG"         -55
$constant "ERR-ALIGN-VIOLATION"               -56

$constant "ERR-USER-ERROR" -669


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (abort-message)  ( -- addr count ) (user-abort-msg-page) @ ?dup if count else NullString endif ;

: (abort-buffer)  ( -- addr )  (user-abort-msg-page) @ ?dup ifnot 4096 os:prot-r/w os:mmap ifnot err-out-of-memory throw endif dup (user-abort-msg-page) ! dup 0! endif ;

: (abort-msg-reset)  ( -- )  (user-abort-msg-page) @ ?dup if 0! endif ;
: (abort-msg-emit)  ( ch -- )  dup 32 < over 127 = or if drop [char] ? endif  (abort-buffer) @ (#user-abort-msg) < if (abort-buffer) c4s:cat-char else drop endif ;
: (abort-msg-type)  ( addr count -- )  dup +if (#user-abort-msg) min bounds do i c@ (abort-msg-emit) loop else 2drop endif ;

: (abort-with-built-msg-errcode)  ( errcode -- )  (tib-fname>error-fname) throw ;
: (abort-with-built-msg)  ( -- )  err-user-abort [execute-tail] (abort-with-built-msg-errcode) ;

: (abort-with-msg)  ( addr count -- )
  0 (#user-abort-msg) clamp (abort-buffer) c4s:copy-counted
  [execute-tail] (abort-with-built-msg)
;

alias (abort-msg-reset) <abort
alias (abort-msg-emit) abort-emit
alias (abort-msg-emit) abort-safe-emit
alias (abort-msg-type) abort-type
alias (abort-with-built-msg) abort>
: dispatch-abort>  ( -- )  err-dispatch-error [execute-tail] (abort-with-built-msg-errcode) ;

..: (thread-cleanup)  ( -- )  (user-abort-msg-page) @ ?dup if 4096 os:munmap drop (user-abort-msg-page) 0! endif ;..


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
create (ERROR-MSG-TABLE) (hidden)
  ERR-UNKNOWN-WORD                  error-table-msg," pardon?"
  ERR-STACK-UNDERFLOW               error-table-msg," stack underflow"
  ERR-STACK-OVERFLOW                error-table-msg," stack overflow"
  ERR-R-STACK-UNDERFLOW             error-table-msg," return stack underflow"
  ERR-R-STACK-OVERFLOW              error-table-msg," return stack overflow"
  ERR-OUT-OF-MEMORY                 error-table-msg," out of memory"
  ERR-WORD-REDEFINED                error-table-msg," is not unique"
  ERR-FILE-NOT-FOUND                error-table-msg," file not found"
  ERR-FILE-READ-ERROR               error-table-msg," file read error"
  ERR-FILE-WRITE-ERROR              error-table-msg," file write error"
  ERR-FILE-TOO-BIG                  error-table-msg," file too big"
  ERR-COMPILATION-ONLY              error-table-msg," compilation only"
  ERR-EXECUTION-ONLY                error-table-msg," execution only"
  ERR-UNPAIRED-CONDITIONALS         error-table-msg," conditionals not paired"
  ERR-UNFINISHED-DEFINITION         error-table-msg," definition not finished"
  ERR-IN-PROTECTED-DICT             error-table-msg," in protected dictionary"
  ERR-NOT-DEFER                     error-table-msg," not a DEFER word"
  ERR-INVALID-WORD-NAME             error-table-msg," invalid word name"
  ERR-WORD-EXPECTED                 error-table-msg," known word expected"
  ERR-USER-ABORT                    error-table-msg," user abort"  ;; actually, this will show (abort-message) instead
  ERR-BAD-RELADDR                   error-table-msg," reladdr out of bounds"
  ERR-INVALID-MALLOC                error-table-msg," invalid malloc/realloc/free"
  ERR-VOCABULARY-STACK-OVERFLOW     error-table-msg," vocabulary stack overflow"
  ERR-CHAR-EXPECTED                 error-table-msg," character expected"
  ERR-STRING-EXPECTED               error-table-msg," string expected"
  ERR-INVALID-BREAK-CONT            error-table-msg," invalid break/continue"
  ERR-NONNAKED-SYSTEM               error-table-msg," non-naked system"
  ERR-NOT-IMPLEMENTED               error-table-msg," not implemented"
  ERR-NEGATIVE-ALLOT                error-table-msg," negative ALLOT is not allowed"
  ERR-NO-TEMP-HERE                  error-table-msg," not allowed in transient HERE"
  ERR-TEMP-HERE-ALREADY             error-table-msg," transient HERE already in use"
  ERR-CANNOT-OVERRIDE               error-table-msg," cannot override non-Forth word"
  ERR-CANNOT-REPLACE                error-table-msg," no code space to replace word"
  ERR-INPUT-TOO-LONG                error-table-msg," input too long"
  ERR-THROW-WITHOUT-CATCH           error-table-msg," THROW without CATCH"
  ERR-THROW-CHAIN-CORRUPTED         error-table-msg," THROW chain corrupted"
  ERR-UNBALANCED-IFDEF              error-table-msg," unbalanced ifdefs"
  ERR-USER-ERROR                    error-table-msg," user-defined error"
  ERR-STRING-TOO-LONG               error-table-msg," string too long"
  ERR-SEGFAULT                      error-table-msg," segmentation fault"
  ERR-MATH-OVERFLOW                 error-table-msg," arithmetic overflow"
  ERR-MATH-ZERO-DIVIDE              error-table-msg," division by zero"
  ERR-SAVE-CHAIN-STACK              error-table-msg," unbalanced stack in save chain"
  ERR-FSTACK-UNDERFLOW              error-table-msg," fpu stack underflow"
  ERR-UNKNOWN-LIBRARY               error-table-msg," unknown library"
  ERR-ELLIPSIS-FORTH                error-table-msg," `...` must be in a Forth word"
  ERR-ELLIPSIS-FIRST                error-table-msg," `...` must be the first in a Forth word"
  ERR-INVALID-INPUT-SIZE            error-table-msg," invalid size in RESTORE-INPUT"
  ERR-OUT-OF-USER-AREA              error-table-msg," out of user area"
  ERR-INVALID-THREAD                error-table-msg," use resource in invalid thread"
  ERR-DISPATCH-ERROR                error-table-msg," cannot dynamicaly dispatch class method"
  ERR-NONMACRO-ONLY                 error-table-msg," definition must be non-macro"
  ERR-UNDEFINED-INSTRUCTION         error-table-msg," undefined CPU instruction"
  ERR-ALIGNED-NAME-TOO-LONG         error-table-msg," aligned word name too long"
  ERR-ALIGN-VIOLATION               error-table-msg," align violation (internal compiler error)"
error-table-end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (find-errmsg)  ( id tbladdr -- addr count true // id false )
  swap >r
  begin dup @ while
    dup @ r@ = if rdrop cell+ zcount true exit endif
  cell+ zcount + 1+ repeat
  drop r> false
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; err-unknown-word is "unknown word" -- the word should be already dumped,
;; and we won't show the usual "ERROR:" prompt
;; err-user-abort will type user abort message buffer
;; err-dispatch-error will type user abort message buffer
: ERROR-MESSAGE  ( errcode -- )
  dup err-unknown-word = if  ;; unknown word
    (error-msg-table) (find-errmsg) ifnot drop ." pardon?" endif
    [char] ? emit space type
  else  ;; look for error message in the table
    ?endcr if space endif
    ." ERROR"
    dup err-user-abort = if ." : " (abort-message) ?dup ifnot drop " unknown user error" endif type (abort-msg-reset)
    else dup err-dispatch-error = (abort-message) nip logand if (abort-message) type (abort-msg-reset)
    else (error-msg-table) (find-errmsg) if ." : " type
    else ." #" base @ swap decimal 0 .r base !
    endif endif endif
  endif
;


: ERROR-LINE.  ( -- )
  (tib-error-line#) ?dup if
    ."  around line #"
    base @ >r decimal . r> base !
    (tib-error-fname) count ?dup
    if ." of file \`" type 34 emit space else drop endif
  endif
  error-type-line? if endcr (tib-type-error-line) cr endif
  (tib-clear-error)
;

;; this does "ABORT"
: (FATAL-ERROR)  ( errcode -- )
  decimal  ;; because why not
  error-message
  error-line.
  ;; print stack depthes
  base @ >r decimal space
  ." D:" sp0 @ sp@ - 2 arshift .
  ." R:" rp0 @ rp@ - 2 arshift .
  cr r> base !
  abort
  ;; just in case it returns
  1 n-bye
; (noreturn) (hidden)


;; and this one tries to throw
: (ERROR) ( errcode -- )
  (tib-fname>error-fname)
  (exc-frame-ptr) @ if throw endif
  ;; no exception frame set, do fatal
  fatal-error
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; issue an error message with the given code if the boolean flag is true
: ?ERROR  ( flag code -- )  swap if error endif drop ;
: NOT-?ERROR  ( flag code -- )  swap ifnot error endif drop ;


;; this is also used in "(COLD-FIRSTTIME)"
: (ABORT-CLEANUP-MIN)  ( -- )
  sp0!
  r@ rp0! >r  ;; this is so we will be able to return to the caller
  state 0!
  dp-temp-reset
  (exc0!)  ;; clear all exception frames
  ;; 0 (self!) 0 (locptr!) -- this is done in the corresponding modules now
  ;; temp-pool-reset
  fpu-reset
  tib-reset
  only forth definitions
;


$uservar "(ABORT-CLEANUP-RET)" ua_ofs_abort_cleanup_ret 0
(hidden)

..: (ABORT-CLEANUP)  ( -- )
  sp0! r@ (abort-cleanup-ret) ! ;; this is so we will be able to return to the caller
  (abort-cleanup-min)
  (dbginfo-reset)
  tload-verbose-default to tload-verbose
  (abort-cleanup-ret) @ >r
;..


: (ABORT)  ( -- )
  (abort-cleanup)
  main-loop
  ;; just in case it returns
  bye
; (noreturn) (hidden)


code: abort-cleanup  ( )
  jp    ts:[ua_ofs_abort_cleanup_ptr]
endcode

code: abort  ( )
  jp    ts:[ua_ofs_abort_ptr]
endcode

code: error  ( code )
  jp    ts:[ua_ofs_error_ptr]
endcode

code: fatal-error  ( code )
  jp    ts:[ua_ofs_fatal_error_ptr]
endcode

..: (startup-init)  ( -- )
  ['] (abort-cleanup) (abort-cleanup-ptr) !
  ['] (abort) (abort-ptr) !
  ['] (error) (error-ptr) !
  ['] (fatal-error) (fatal-error-ptr) !
;..


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dump data stack (unlimited depth)
: .STACK  ( -- )
  depth dup -if ." stack underflowed" cr drop exit endif
  ?dup ifnot ." stack empty" cr exit endif
  dup ." stack depth: " . cr
  0 do
    depth 1- i - pick
    dup . ." | " u. cr
  loop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: error-table-msg,"  ( code -- )  \ msg"
  ;; UrForth level 0 uses byte for error code, UrForth level 1 uses dword
  ,  ;; store code
  34 parse dup n-allot swap move
  0 c, ;; terminating zero for the string
;


;; compile end of list
: error-table-end  ( -- )
  ;; UrForth level 0 uses two zero bytes, UrForth level 1 uses 4
  0 ,
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: abort"  ( -- )  ;; "
  34 (parse-and-unescape) [compile] sliteral ['] (abort-with-msg) state @ if compile, else execute-tail endif
; immediate

: (?abort-compiler)  ( iftrue? -- )  ( flag iftrue? -- )
  state @ if
    if [compile] if else [compile] ifnot endif
    [compile] abort"  ;; "
    [compile] endif
  else
    ifnot not endif 34 (parse-and-unescape) rot if (abort-with-msg) else 2drop endif
  endif
;

: ?abort"  ( flag -- )  ;; "
  true (?abort-compiler)
; immediate

: not-?abort"  ( flag -- )  ;; "
  false (?abort-compiler)
; immediate
