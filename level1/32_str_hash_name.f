;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: (STR-NAME-HASH-CODEBLOCK)
  ;; IN:
  ;;   EDI: address
  ;;   ECX: count
  ;; OUT:
  ;;   EAX: u32hash
  ;;   EDI,ECX,flags: destroyed
str_name_hash_edi_ecx:
  xor   eax,eax
  jecxz .done
 $if URFORTH_NAME_HASH_TYPE = URFORTH_NAME_HASH_JOAAT
  ;; Bob Jenkins' One-At-A-Time
.hashloop:
  ;; hash += *key
  movzx edx,byte [edi]
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  add   eax,edx
  ;; hash += hash<<10
  ld    edx,eax
  shl   eax,10
  add   eax,edx
  inc   edi
  ;; hash ^= hash>>6
  ld    edx,eax
  shr   eax,6
  xor   eax,edx
  dec   ecx
  jr    nz,.hashloop
  ;;loop  .hashloop
  ;; final permutation
  ;; hash += hash<<3
  ld    edx,eax
  shl   eax,3
  add   eax,edx
  ;; hash ^= hash>>11
  ld    edx,eax
  shr   eax,11
  xor   eax,edx
  ;; hash += hash<<15
  ld    edx,eax
  shl   eax,15
  add   eax,edx
 $endif
 $if URFORTH_NAME_HASH_TYPE = URFORTH_NAME_HASH_ELF
  ;; ELFHASH
.hashloop:
  ;; hash <<= 4
  shl   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  add   eax,edx
  ;; high = hash&0xF0000000
  ld    edx,eax
  and   edx,0xf0000000
  ld    ebx,edx
  inc   edi
  ;; hash ^= high>>24
  rol   edx,8
  xor   eax,edx
  ;; hash &= ~high
  not   ebx
  and   eax,ebx
  dec   ecx
  jr    nz,.hashloop
  ;;loop  .hashloop
 $endif
 $if URFORTH_NAME_HASH_TYPE = URFORTH_NAME_HASH_ROT
  ;; simple rotating hash
.hashloop:
  ;; hash = lrot(hash, 4)
  rol   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  inc   edi
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  xor   eax,edx
  ;;add   eax,edx
  dec   ecx
  jr    nz,.hashloop
  ;; hash ^= (hash>>10)^(hash>>20)
  ld    ecx,eax
  shr   ecx,10
  ld    edx,eax
  shr   edx,20
  xor   eax,ecx
  xor   eax,edx
 $endif
.done:
  ret

 $if URFORTH_NAME_HASH_TYPE > URFORTH_NAME_HASH_ROT
  $error "what name hash?!"
 $endif

  $if WLIST_HASH_BITS
  ;; IN:
  ;;   EAX: u32hash
  ;; OUT:
  ;;   EAX: u8hash-masked
  ;;   flags: destroyed
str_name_hash_fold_mask_eax:
  push  ecx
  ;; fold u32->u16
  ld    ecx,eax
  shr   eax,16
  add   ax,cx
  ;; fold u16->u8
  add   al,ah
  ;; mask
  $if WLIST_HASH_MASK # 255
    and   al,WLIST_HASH_MASK
  $endif
  movzx eax,al
  pop   ecx
  ret
  $endif
endcode
(hidden) (codeblock)


;; this is used to calculate word name hashes
code: STR-NAME-HASH  ( addr count -- u32hash )
  pop   edi
  call  str_name_hash_edi_ecx
  ld    TOS,eax
  urnext
endcode

$if WLIST_HASH_BITS
;; this is used to calculate word name hashes
code: STR-NAME-HASH-FOLDED-MASKED  ( addr count -- maskedhash )
  pop   edi
  call  str_name_hash_edi_ecx
  call  str_name_hash_fold_mask_eax
  ld    TOS,eax
  urnext
endcode

;; this is used to calculate word name hashes
code: NAME-HASH-FOLD-MASK  ( u32hash -- maskedhash )
  ld    eax,TOS
  call  str_name_hash_fold_mask_eax
  ld    TOS,eax
  urnext
endcode
$endif
