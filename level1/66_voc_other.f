;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: VOCID-OF  ( -- vocid )
  -find-required
  dup word-type? WORD-TYPE-VOC <> err-vocabulary-expected ?error
  ( cfa )
  voc-cfa->vocid
;

;; returns vocabulary latest-ptr
: VOCID:  ( -- vocid )  \ name
  -find-required
  dup word-type? WORD-TYPE-VOC <> ERR-VOCABULARY-EXPECTED ?error
  ( cfa )
  state @ if
    [compile] cfaliteral
    compile voc-cfa->vocid
  else voc-cfa->vocid endif
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary structure
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;;   dd headernfa (can be zero or -1; -1 means "no hashtable")
;;   hashtable (if enabled)

$if WLIST_HASH_BITS
: (NEW-HASHTABLE)  ( -- )
  1 WLIST-HASH-BITS lshift cells dup n-allot swap erase
; (hidden)
$else
: (NEW-HASHTABLE)  ( -- )
; immediate
$endif

;; headernfa can be zero for anonymous
: (NEW-WORDLIST-EX)  ( headernfa usehash -- vocid )
  >r    ;; save "usehash" flag
  here  ;; this is vocid
  0 ,             ;; latest
  here
  (VOC-LINK) @ ,  ;; voclink
  (VOC-LINK) !
  0 ,             ;; parent
  swap dup ifnot , else reladdr, endif  ;; headernfa
  ;; words hashtable buckets
 $if WLIST_HASH_BITS
  r> if (NEW-HASHTABLE) else -1 , endif
 $else
  rdrop -1 ,
 $endif
; (hidden)

;; can be used to init transient vocabularies
;; tempvocs will not be included in (voc-link) list
;; don't forget to switch to dp-temp if necessary
: (new-temp-voc-ex)  ( usehash -- vocid )
  here ;; vocid
  0 ,  ;; latest
  0 ,  ;; voclink
  0 ,  ;; parent
  0 ,  ;; headernfa
 $if WLIST_HASH_BITS
  swap if (NEW-HASHTABLE) else -1 , endif
 $else
  nip -1 ,
 $endif
; (hidden)

: new-temp-voc  ( -- vocid )  true (new-temp-voc-ex) ;
: new-temp-voc-nohash  ( -- vocid )  false (new-temp-voc-ex) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create new anonymous wordlist (currently NOHASH, but this may change)
: wordlist         ( -- vocid )  0 false (new-wordlist-ex) ;
: wordlist-hashed  ( -- vocid )  0 true (new-wordlist-ex) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is the first word compiled to vocabulary header
;; it should be followed by vocid (aka wordlist address)
: (VOCAB-DOES-CODE)  ( -- )
  ;; code argument is vocid, get it
  ;; also note that we will not continue execution (it is meaningless)
  r> @  ;; vocid
  (set-context)
; (hidden) (arg-vocid)


;; vocabulary support
: VOCABULARY-EX  ( usehash -- )
  create-header
  ;; toggle "vocabulary" flag
  (WFLAG-VOCAB) (toggle-latest-wflag)
  ;; vocabulary is using normal Forth CFA
  (urforth-doforth-addr) compiler:(cfa-call,)
  compile (VOCAB-DOES-CODE)
  ;; we will patch vocid here
  here >r 0 ,
  latest-nfa swap (new-wordlist-ex) r> !
  create; smudge
;

: VOCABULARY  ( -- )  true vocabulary-ex ;
: VOCABULARY-NOHASH  ( -- )  false vocabulary-ex ;


;; new vocabulary will see all definitions from CURRENT vocabulary
: NESTED-VOCABULARY-EX  ( usehash -- )
  vocabulary-ex
  current @ latest-cfa voc-cfa->vocid vocid-parent!
;

: NESTED-VOCABULARY  ( -- )  true nested-vocabulary-ex ;
: NESTED-VOCABULARY-NOHASH  ( -- )  false nested-vocabulary-ex ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; call CFA for each vocabulary in system
;; passed vocid is suitable for "FOREACH-WORDS" and "(VOC.)"
;; cfa: ( vocid -- endflag ) -- return non-zero to exit with this value
: FOREACH-VOC  ( cfa -- endflag )
  (voc-link) begin @ dup while
    ;; make sure that data stack doesn't contain our data,
    ;; so cblock can use it to count something, for example
    2dup 2>r cell- swap execute ?dup if 2rdrop exit endif 2r>
  repeat nip
;

;; call CFA for each word in vocabulary
;; vocid is what "FOREACH-VOC" is passing to cblock
;; note that no words are skipped (i.e. SMUDGE and HIDDEN words are enumerated too)
;; use "context @" to get CONTEXT vocabulary vocid
;; use "current @" to get CURRENT vocabulary vocid (the top one)
;; use "VOCID-OF name" to get vocid from vocabulary name
;; cfa: ( nfa -- endflag ) -- return non-zero to exit with this value
: FOREACH-WORD  ( vocid cfa -- endflag )
  swap begin @ dup while
    ;; make sure that data stack doesn't contain our data,
    ;; so cblock can use it to count something, for example
    2dup 2>r lfa->nfa swap execute ?dup if 2rdrop exit endif 2r>
  repeat nip
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; shows all known vocabularies
: VOC-LIST  ( -- )
  ." KNOWN VOCABULARIES:\n"
  [:  ( vocid -- endflag )
    2 spaces vocid. cr
    false
  ;] foreach-voc drop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: WORDS  ( -- )
  endcr ." --- WORDS OF " context @ vocid. ."  ---\n"
  context @
  [:  ( nfa -- exitflag )
    ;; skip SMUDGE and HIDDEN words
    dup nfa->ffa ffa@
    [ (WFLAG-SMUDGE) (WFLAG-HIDDEN) or ] literal and
    if drop else id. space endif
    0
  ;] foreach-word drop
  endcr
;

: (ALL-WORDS)  ( -- )
  endcr ." --- ALL WORDS OF " context @ vocid. ."  ---\n"
  context @
  [:  ( nfa -- exitflag )
    ;; skip SMUDGE and HIDDEN words
    dup id.
    nfa->ffa ffa@
    dup (wflag-smudge) and if ." {smudge}" endif
    dup (wflag-hidden) and if ." {hidden}" endif
    space drop
    false
  ;] foreach-word drop
  endcr
; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: WORD-CODE-END  ( cfa -- addr )  cfa->nfa nfa->sfa @ ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (WORD-VOCID-IP->NFA)  ( ip latestptr -- nfa-or-0 )
  [:  ( ip nfa -- ip 0 // ip nfa )
      ;; UF 2dup swap dothex8 space dup dothex8 space dup nfa2sfa @ dothex8 space iddot cr
    2dup dup nfa->sfa @ 1- bounds?
    ( ip nfa flag )
    ifnot drop false endif
  ;] foreach-word nip
; (hidden)


: WORD-IP->NFA  ( ip -- nfa-or-0 )
  [:  ( ip voclptr -- ip 0 // ip nfa )
    over swap (word-vocid-ip->nfa)
  ;] foreach-voc nip
;
