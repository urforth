;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: RECURSE  ( -- )
  compiler:?comp compiler:?non-macro latest-cfa compile,
; immediate

: RECURSE-TAIL  ( -- )
  compiler:?comp compiler:?non-macro latest-cfa compile lit-exectail reladdr,
; immediate


: [CHAR]  ( -- ch )  \ word
  parse-name 1 <> err-char-expected ?error
  c@ [compile] literal
; immediate


: '  ( -- cfa )  \ word
  -find-required
;

: [']  ( -- cfa )  \ word
  -find-required
  [compile] cfaliteral
; immediate


: (COMPILE-CFA-LITERAL)  ( cfa -- )
  [compile] cfaliteral
  compile compile,
;

: COMPILE  ( -- )  \ word
  compiler:?comp -find-required
  (compile-cfa-literal)
; immediate

: [COMPILE]  ( -- )  \ word
  compiler:?comp -find-required compile,
; immediate

: [EXECUTE-TAIL]  ( -- )  \ word
  compiler:?comp -find-required compile lit-exectail reladdr,
; immediate


;; ANS idiocity, does this:
;; if the next word is immediate, compiles it in the current word
;; if the next word is not immediate, compiles "compile nextword"
: POSTPONE  ( -- )
  compiler:?comp -find-required-ex -if
    ;; not immediate, do what "COMPILE" does
    (compile-cfa-literal)
  else
    ;; immediate, do what "[COMPILE]" does
    compile,
  endif
; immediate


: (  ( -- )
  41 parse 2drop
; immediate

: \
  parse-skip-to-eol
; immediate

: \\
  parse-skip-to-eol
; immediate

: //
  parse-skip-to-eol
; immediate

: ;;
  parse-skip-to-eol
; immediate

;; multiline comment
;; (* .... *)
: (*  ( -- )  ;; *)
  skip-comment-multiline
; immediate

;; nested multiline comment
;; (( .... ))
: ((  ( -- ) ;; ))
  skip-comment-multiline-nested
; immediate


;; this copies string to PAD if it needs to be unscaped, or
;; if we're using default TIB
: (parse-and-unescape)  ( ch -- addr count )
  parse dup ifnot exit endif
  tib-default? ifnot
    ;; check for backslash
    2dup [char] \ str-char-index
    ifnot exit endif
    drop
  endif
  dup #pad-area cell- u> err-string-too-long ?error
  over >r >r pad r@ move
  pad r@ str-unescape
  ;; it can never be bigger that the original, so it is save to compare here
  2dup r> r> 2dup 2>r s= if
    2drop 2r>
  else
    2rdrop
  endif
;

: "  ( -- addr count )  \ word  ;; "
  34 (parse-and-unescape) [compile] sliteral
; immediate

alias " S"


: ."  ( -- )  \ word  ;; "
  34 (parse-and-unescape)
  state @ if
    ['] (.") compiler:custom-c1sliteral,
  else
    type
  endif
; immediate

: .(  ( -- )
  [char] ) (parse-and-unescape) type
; immediate
