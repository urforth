;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is the only thing we need to print unsigned double numbers
code: UDS/MOD  ( ud1 u1 --> ud2 u2 )
  ld    edi,TOS
  pop   eax
  pop   ebx
  ;; EDI=u1
  ;; EAX=ud1-high
  ;; EBX=ud1-low
  xor   edx,edx
  div   edi
  xchg  eax,ebx
  div   edi
  push  eax
  push  ebx
  ld    TOS,edx
  urnext
endcode

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is the only thing (besides the one above) we need to print signed double numbers
code: DABS  ( d -- |d| )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  test  ecx,ecx
  jr    ns,@f
  not   ecx
  not   edx
  add   edx,1
  adc   ecx,0
@@:
  ;; push ECX:EDX
  push  edx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rest of the double math

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DNEGATE  ( d -- -d )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  not   ecx
  not   edx
  add   edx,1
  adc   ecx,0
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: DSGN  ( d -- -1/0/1 )
  pop   edx
  ld    eax,edx
  or    eax,TOS
  jr    z,@f
  test  TOS,TOS
  ld    TOS,1
  jr    ns,@f
  ld    TOS,-1
@@:
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: S>D  ( n -- d )
  ld    eax,TOS
  cdq
  push  eax
  ld    TOS,edx
  urnext
endcode

code: U>D  ( u -- ud )
  push  TOS
  xor   TOS,TOS
  urnext
endcode

;; with overflow clamping
code: D>U  ( ud -- u )
  test  TOS,TOS
  pop   TOS
  jr    z,@f
  ld    TOS,-1
@@:
  urnext
endcode

;; with overflow clamping
code: D>S  ( d -- n )
  pop   eax
  jecxz .poscheck
  ;; overflow or negative
  test  TOS,TOS
  jr    ns,.posoverflow
  ;; definitely negative
  inc   TOS
  jr    nz,.negoverflow
  test  eax,eax
  jr    s,.good
.negoverflow:
  ld    TOS,0x80000000
  jr    .done
.poscheck:
  ;; positive, check for positive overflow
  test  eax,eax
  jr    ns,.good
.posoverflow:
  ;; positive overflow
  ld    eax,0x7fffffff
.good:
  ld    TOS,eax
.done:
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: 2AND  ( d0lo d0hi d1lo d1hi -- d0lo&d1lo d0hi&d1hi )
  pop   eax
  and   [esp],TOS
  and   [esp+4],eax
  pop   TOS
  urnext
endcode

code: 2OR  ( d0lo d0hi d1lo d1hi -- d0lo|d1lo d0hi|d1hi )
  pop   eax
  or    [esp],TOS
  or    [esp+4],eax
  pop   TOS
  urnext
endcode

code: 2XOR  ( d0lo d0hi d1lo d1hi -- d0lo^d1lo d0hi^d1hi )
  pop   eax
  xor   [esp],TOS
  xor   [esp+4],eax
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DLSHIFT  ( u cnt -- )
  pop   edx
  pop   eax
  cmp   TOS,32
  jr    ae,.bigshift
  shld  edx,eax,cl
  shl   eax,cl
.done:
  push  eax
  ld    TOS,edx
  urnext
.bigshift:
  cmp   TOS,64
  jr    ae,.zero
  ld    edx,eax
  sub   TOS,32
  shl   edx,cl
  jr    .onlyhigh
.zero:
  xor   edx,edx
.onlyhigh:
  xor   eax,eax
  jr    .done
endcode

code: DRSHIFT  ( u cnt -- )
  pop   edx
  pop   eax
drshift_main:
  cmp   TOS,32
  jr    ae,.bigshift
  shrd  eax,edx,cl
  shr   edx,cl
.done:
  push  eax
  ld    TOS,edx
  urnext
.bigshift:
  cp    TOS,64
  jr    ae,.zero
  ld    eax,edx
  sub   TOS,32
  shr   eax,cl
  jr    .onlylow
.zero:
  xor   eax,eax
.onlylow:
  xor   edx,edx
  jr    .done
endcode

code: DARSHIFT  ( u cnt -- )
  pop   edx
  pop   eax
  cmp   TOS,32
  jr    ae,.bigshift
  shrd  eax,edx,cl
  sar   edx,cl
.done:
  push  eax
  ld    TOS,edx
  urnext
.bigshift:
  or    edx,edx
  jr    ns,drshift_main
  ld    eax,edx
  xor   edx,edx
  dec   edx
  cp    TOS,64
  jr    ae,.minusone
  sub   TOS,32
  sar   eax,cl
  jr    .done
.minusone:
  xor   eax,eax
  dec   eax
  jr    .done
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UM*  ( u0 u1 -- ud )
  pop   eax
  xchg  eax,TOS
  mul   TOS
  push  eax
  mov   TOS,edx
  urnext
endcode

code: UM/MOD  ( ud1 u1 -- umod ures )
  pop   edx
  pop   eax
  div   TOS
  push  edx
  mov   TOS,eax
  urnext
endcode

code: UM/  ( ud1 u1 -- ures )
  pop   edx
  pop   eax
  div   TOS
  mov   TOS,eax
  urnext
endcode

code: UMMOD  ( ud1 u1 -- umod )
  pop   edx
  pop   eax
  div   TOS
  mov   TOS,edx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: M*  ( n0 n1 -- d )
  pop   eax
  xchg  eax,TOS
  imul  TOS
  push  eax
  mov   TOS,edx
  urnext
endcode

code: M/MOD  ( d1 n1 -- nmod nres )
  pop   edx
  pop   eax
  idiv  TOS
  push  edx
  mov   TOS,eax
  urnext
endcode

code: M/  ( d1 n1 -- nres )
  pop   edx
  pop   eax
  idiv  TOS
  mov   TOS,eax
  urnext
endcode

code: MMOD  ( d1 n1 -- nmod )
  pop   edx
  pop   eax
  idiv  TOS
  mov   TOS,edx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D2*  ( d -- d*2 )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  shl   edx,1
  rcl   ecx,1
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: D2/  ( d -- d/2 )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  sar   ecx,1
  rcr   edx,1
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: D2U/  ( d1 -- d/2 )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  shr   ecx,1
  rcr   edx,1
  ;; push ECX:EDX
  push  edx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D0=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  or    ecx,edx
  cp    ecx,1
  ;;  C: ECX==0
  ;; NC: ECX!=0
  ld    ecx,0
  adc   ecx,0
  urnext
endcode

code: D0!=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  or    ecx,edx
  cp    ecx,1
  ;;  C: ECX==0
  ;; NC: ECX!=0
  ld    ecx,1
  sbb   ecx,0
  urnext
endcode

code: D0<  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,1
  sbb   ecx,0
  urnext
endcode

code: D0>  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  ld    eax,ecx
  or    eax,edx
  jr    z,@f
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,0
  adc   ecx,0
@@:
  urnext
endcode

code: D0<=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  ld    eax,ecx
  or    eax,edx
  jr    z,@f
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,1
  sbb   ecx,0
  urnext
@@:
  ld    TOS,1
  urnext
endcode

code: D0>=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  ld    eax,ecx
  or    eax,edx
  jr    z,@f
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,0
  adc   ecx,0
  urnext
@@:
  ld    TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D+  ( d1 d2 -- d )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  add   eax,ebx
  adc   edx,ecx
  ;; push EDX:EAX
  push  eax
  ld    TOS,edx
  urnext
endcode

code: D-  ( d1 d2 -- d )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  ;; push EDX:EAX
  push  eax
  ld    TOS,edx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D=  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  or    eax,edx
  jr    z,@f
  ld    TOS,0
  urnext
@@:
  ld    TOS,1
  urnext
endcode

code: D<>  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  or    eax,edx
  jr    z,@f
  ld    TOS,1
  urnext
@@:
  ld    TOS,0
  urnext
endcode

code: D<  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d1-d2: d1<d2:C; d1>d2:nc
  sub   eax,ebx
  sbb   edx,ecx
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: D>  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d2-d1
  sub   ebx,eax
  sbb   ecx,edx
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: DU<  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d1-d2
  sub   eax,ebx
  sbb   edx,ecx
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: DU>  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d2-d1
  sub   ebx,eax
  sbb   ecx,edx
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: DU<=  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   ebx,eax
  sbb   ecx,edx
  ld    TOS,1
  sbb   TOS,0
  urnext
endcode

code: DU>=  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  ld    TOS,1
  sbb   TOS,0
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DMAX  ( d1 d2 -- max[d1,d2] )  2over 2over d< if 2swap endif 2drop ;
: DMIN  ( d1 d2 -- min[d1,d2] )  2over 2over d> if 2swap endif 2drop ;
: 2ROT  ( x1 x2 x3 x4 x5 x6 -- x3 x4 x5 x6 x1 x2 )  2>r 2swap 2r> 2swap ;
: 2NROT ( x1 x2 x3 x4 x5 x6 -- x5 x6 x1 x2 x3 x4 )  2swap 2>r 2swap 2r> ;

: M+  ( d1|ud1 n -- d2|ud2 )  s>d d+ ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UDS*  ( ud1 u --> ud2 )
  ld    edi,TOS
  pop   ebx
  pop   ecx
  ld    eax,ecx
  mul   edi
  push  edx
  ld    ecx,eax
  ld    eax,ebx
  mul   edi
  pop   edx
  add   eax,edx
  push  ecx
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 32-bit sqrt (because why not?)
: SQRT ( u -- u )
  0 0  16 for
    >r d2* d2* r> 2* >r
    r@ 2* 1+ 2dup u>= if - r> 1+ >r else drop endif r>
  endfor
  nip nip
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UD*UD  ( ud0lo ud0hi ud1lo ud1hi -- ud2lo ud2hi )
  push  TOS
  pop   ecx
  pop   ebx
  pop   edx
  pop   eax
  imul  ecx,eax
  imul  edx,ebx
  add   ecx,edx
  mul   ebx
  add   ecx,edx
  push  eax
  urnext
endcode

alias UD*UD D*
