;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various modules can add other words
;; floating point module does this, for example

vocabulary-nohash (env-queries) (hidden)
voc-set-active (env-queries)

;; string with the system version
: URFORTH  ( -- addr count )  (URVERSION->PAD) ;
;; maximum size of a counted string, in characters
: /COUNTED-STRING  ( -- n )  0x000f_ffff ;
;; size of the pictured numeric output string buffer, in characters
: /HOLD  ( -- n )  #pad-area-resv ;
;; size of the scratch area pointed to by PAD, in characters
: /PAD  ( -- n )  #pad-area ;
;; size of one address unit, in bits
: ADDRESS-UNIT-BITS  ( -- n )  1 ;
;; true if floored division is the default (UrForth is symmetric)
: FLOORED  ( -- n )  false ;
;; maximum value of any character in the implementation-defined character set
: MAX-CHAR  ( -- n )  255 ;
;; largest usable signed double number
: MAX-D  ( -- d )  0xffff_ffff 0x7fff_ffff ;
;; largest usable signed integer
: MAX-N  ( -- n )  0x7fff_ffff ;
;; largest usable unsigned integer
: MAX-U  ( -- u )  0xffff_ffff ;
;; largest usable unsigned double number
: MAX-UD  ( -- ud )  0xffff_ffff 0xffff_ffff ;
;; maximum size of the return stack, in cells
: RETURN-STACK-CELLS  ( -- n )  #rp @ ;
;; maximum size of the data stack, in cells
: STACK-CELLS  ( -- n )  #sp @ ;

voc-set-active forth


: ENVIRONMENT?  ( addr count -- flag // false // i*x true )
  dup 1 250 within ifnot 2drop false exit endif
  vocid: (env-queries) voc-search-noimm if execute true
  else has-word?  ;; otherwise check a word
  endif
;
