;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary tloader
voc-set-active tloader

: fnpad  ( -- filenamepad )  pad #pad-area + 1030 - pad max ;

;; first cell: pointer to the next
;; word-counted string (0-terminated, terminator is not counted)
$variable "incdirs-head" 0
$variable "incdirs-tail" 0

;; c4str, with trailing slash
$value "last-include-dir-c4s" 0


;; string must not be at HERE
: add-include-dir  ( addr count -- )
  dup +if
    dup 3 + cell+ n-allot
    ;; put link
    incdirs-tail @  ( addr count buf tlnk )
    dup if 2dup ! endif drop
    dup incdirs-tail !
    0 over ! cell+
    ;; put string
    2dup w! 2+
    2dup + 0c!  ;; put trailing zero
    swap move
    ;; fix head if necessary
    incdirs-head @ ifnot
      incdirs-tail @
      incdirs-head !
    endif
  else
    2drop
  endif
;

: show-include-dirs  ( -- )
  ." === inclide directories (in the search order) ===\n"
  incdirs-head begin @ ?dup while
  dup cell+ wcount type cr repeat
;


;; expands path at fnpad, as cell-counted string
;; assumes that the path is non-empty, and it doesn't at fnpad already
;; doesn't zero-terminate string
: (expand-path>fnpad)  ( addr count -- )
  fnpad 0!
  0 max dup ifnot 2drop " ." endif
  ;; "!" means "binary file dir, as specified in argv[0]"
  over c@ [char] ! = if
    /char
    0 forth:argv-str str-extract-path ?dup ifnot drop " ./" endif
    fnpad c4s:cat-counted
  else over c@ [char] ~ = if
    ;; "~" means either the same as "!", or $HOME (if followed by a slash)
    dup 1 > if over 1+ c@ [char] / = else false endif
    if ;; $HOME
      2 /string
      " HOME" forth:get-env ifnot " ./" endif
    else ;; the same as "!"
      /char
      0 forth:argv-str str-extract-path ?dup ifnot drop " ./" endif
    endif
    fnpad c4s:cat-counted fnpad c4s:add-slash
  endif endif
  fnpad c4s:cat-counted
;


: fnpad-dir?   ( -- flag )  fnpad c4s:zterm fnpad count os:dir? ;
: fnpad-file?  ( -- flag )  fnpad c4s:zterm fnpad count os:file? ;

: need-expand?  ( ch -- flag ) dup [char] / = over [char] ~ = rot [char] ! = or or not ;


;; if succeed, fnpad will contain build file name
;; on failure, fnpad is destroyed
;; we will try:
;;   dir/name.f (as file)
;;   dir/name (as file)
;;   dir/name (as dir, will add "/all.f")
: try-name-at-path  ( nameaddr namecount diraddr dircount -- successflag )
  (expand-path>fnpad) fnpad c4s:add-slash
  dup +if
    over c@ need-expand? if fnpad c4s:cat-counted else (expand-path>fnpad) endif
    fnpad @ >r  ;; save generated filename length
    ;; try to append ".f" if it is not there
    fnpad count str-extract-ext " .f" s=ci ifnot " .f" fnpad c4s:cat-counted endif
      \ endcr ." ::: " fnpad count type cr
    fnpad-file? if rdrop true exit endif
    r> fnpad !  ;; restore original filename
    ;; if directory, append "/all.f"
    fnpad-dir? if fnpad c4s:add-slash " all.f" fnpad c4s:cat-counted endif
    fnpad-file?
  else
    2drop false
  endif
;


;; if succeed, fnpad will contain build file name
;; on failure, fnpad is destroyed
: find-include-file  ( addr count -- successflag )
  ;; try current dir
  2dup " ./" try-name-at-path if 2drop true exit endif
  ;; try last tload dir
  last-include-dir-c4s if
    2dup last-include-dir-c4s count try-name-at-path if 2drop true exit endif
  endif
  incdirs-head begin @ ?dup while
    >r 2dup r@ cell+ wcount try-name-at-path if rdrop 2drop true exit endif
    r>
  repeat
  2drop false
;


: build-rc-name  ( -- )
  " !" (expand-path>fnpad)
  0 forth:argv-str str-extract-base-name fnpad c4s:cat-counted
  " .rc" fnpad c4s:cat-counted
;

: build-liblist-name  ( athome -- )
  if " ~/urforth/libs/00-lib-list.f" else " !libs/00-lib-list.f" endif
  (expand-path>fnpad)
;


;; also, leaves file name as c4str at fnpad
: find-open-file  ( addr count -- fd )
  dup 0<= if 2drop err-file-not-found error endif
  2dup find-include-file ifnot
    endcr ." file: \`" type ." \` " err-file-not-found error
    err-file-not-found error
  endif
  2drop
  fnpad count os:o-rdonly 0 os:open
  dup -if drop endcr ." cannot open file: \`" fnpad count type ." \` " err-file-not-found error endif
;


..: (abort-cleanup)  ( -- )
  0 to last-include-dir-c4s
;..


;; if user specified no dirs, add
;; this is so because "SAVE" will save our dirlist too
..: (startup-init)  ( -- )
  incdirs-head @ ifnot
    " !libs/" add-include-dir
    ;; " !libs/ext" add-include-dir
    ;; " !libs/ans/" add-include-dir
    ;; " ~urforth/libs/" add-include-dir
    " ~/.urforth/libs/" add-include-dir
  endif
  \ show-include-dirs
;..


: save-curr-name-incdir  ( -- allocid )
  ;; oldincdir count oldcurrnamedata
  (tib-curr-fname) count-only 2 +cells simple-malloc throw
  dup >r
  last-include-dir-c4s over ! cell+
  (tib-curr-fname) count rot c4s:copy-counted
  r>
;

: restore-curr-name-incdir  ( allocid -- )
  ;; oldincdir count oldcurrnamedata
  dup @ to last-include-dir-c4s
  dup cell+ count (tib-curr-fname) c4s:copy-counted
  simple-free throw
;

: skip-shebang  ( -- )
  #tib @ 2 > if tib @ w@ 0x2123 = if parse-skip-to-eol endif endif
;

;; prerequisite: fnpad must contain valid file name
: tload-fd  ( fd -- )
  tload-verbose if endcr ." loading: " fnpad count type cr endif
  >r  ( | fd )
  0 os:seek-end r@ os:lseek  ( size | fd )
  dup -if r> os:close 2drop err-file-read-error error endif
  ;; seek back to file start
  0 os:seek-set r@ os:lseek if r> os:close 2drop err-file-read-error error endif
  ;; allocate temp pool space
  ;; this will also take care about too big files
  dup cell+ simple-malloc if r> os:close 2drop err-out-of-memory error endif
  r> over >r >r  ( size addr | allocid-file fd )
  2dup + 0!  ;; write zeroes at the end (just in case)
  2dup swap r@ os:read  ;; load file
  ( size addr readbytes | allocid-file fd )
  r> os:close drop  ;; close file
  ( size addr readbytes | allocid-file )
  rot over - if r> simple-free drop err-file-read-error error endif
  ['] save-curr-name-incdir catch ?dup if r> simple-free drop throw endif >r
  ( addr readbytes | allocid-file allocid-oldincname )
  ;; store file path
  fnpad count str-extract-path ?dup ifnot drop " ./" endif
  dup cell+ simple-malloc if r> simple-free drop r> simple-free drop err-out-of-memory error endif
  dup >r c4s:copy-counted r@ to last-include-dir-c4s
  fnpad count (tib-set-fname)
  ( addr readbytes | allocid-file allocid-oldincname allocid-newpath )
  tibstate>r (tib-set-to) tib-line# 1! skip-shebang
  ['] forth:interpret catch
  r>tibstate
  ( | tibstate allocid-file allocid-oldincname allocid-newpath )
  r> simple-free drop  ;; free newpath
  r> restore-curr-name-incdir
  r> simple-free drop
  throw
;

voc-set-active forth


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; load forth source file
;; this loads the whole file into temp pool

: TLOAD  ( addr count -- )
  tloader:find-open-file tloader:tload-fd
;

: tload>forth  ( addr count -- )
  (vocorder>r) only forth definitions ['] tload catch (r>vocorder) throw
;


alias TLOAD INCLUDED
: INCLUDE  ( -- )
  parse-name tload
;


;; SPF-compatible
: (REQUIRE-STR)  ( fileaddr filecount wordaddr wordcount -- )
  has-word? ifnot tload else 2drop endif
;

: REQUIRE  ( -- )  \ word filename
  parse-name has-word?
  parse-name rot if 2drop else tload endif
;

: REQUIRE>FORTH  ( -- )  \ word filename
  parse-name has-word?
  parse-name rot if 2drop else tload>forth endif
;
