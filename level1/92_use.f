;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary (known-libs) (hidden)
voc-set-active (known-libs)

: (require-it)  ( fileaddr filecount wordaddr wordcount -- )
  (vocorder>r) only forth definitions ['] (require-str) catch (r>vocorder) throw
; (hidden)

: (comp-c1string)  ( addr count -- )
  dup 255 u> ERR-STRING-TOO-LONG ?error
    \ endcr ."   s: <" 2dup type ." >\n"
  dup 2+ n-allot  ( addr count buf )
  2dup c!
  dup >r
  1+ swap move
  r> bcount + 0c!
; (hidden)

: (lib-type-require:)  ( -- )
  parse-name
  vocid: (known-libs) voc-search if
    drop parse-name 2drop parse-name 2drop exit
  endif
    \ endcr ." lib: <" 2dup type ." >\n"
  create-named smudge  ;; unsmudge
  parse-name (known-libs):(comp-c1string)  \ filepath
  parse-name (known-libs):(comp-c1string)  \ wordname
  create; smudge
 does>  ( pfa -- )
  bcount 2dup + 1+ bcount (known-libs):(require-it)
; (hidden)

voc-set-active forth


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ name file word
: lib-type-require:  ( -- )
  get-current (current-create-mode) 2>r
  vocid: (known-libs) set-current <public-words>
  ['] (known-libs):(lib-type-require:) catch
  2r> to (current-create-mode) set-current throw
;


: (USE-LIB)  ( addr count -- )
  vocid: (known-libs) voc-search if execute
  else endcr type ." ? " ERR-UNKNOWN-LIBRARY error endif
; (hidden)


: use-lib:  ( -- )  \ name
  parse-name (use-lib)
; immediate

: use-libs:  ( -- )  \ name
  begin (tib-last-read-char@) nl = not-while
    parse-skip-blanks-no-eol
    tib-peekch bl >
  while
    parse-name
    (use-lib)
  repeat
; immediate
