;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary PRNG
voc-set-active PRNG

(*
absolutely non-scientific stupid benchmark:

  bjprng         : 115,356,877 values/second
  bjprng-3rots   : 115,109,543 values/second
  xorshift*-64/32: 114,961,650 values/second
  pcg32          : 107,718,882 values/second
  pcg32ex        :  95,801,375 values/second

as you can see, 2-rot bjprng is the fastest one, and it is quite good.
pcg32 (with fixed stream) is quite fast, and it is good too.
so i left only two of those in the kernel, and moved others to lib.
you can use
  include !libs/ext/prngs.f
to include rest of the generators.
*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generate 64-bit seed (not cryptographically strong!)
;; also, don't call this repeatedly, it will produce bad seeds
: GEN-DSEED  ( dlo dhi -- )
  os:clock-monotonic os:clock-gettime u32hash swap u32hash xor os:get-pid xor u32hash  ;; low seed
  os:clock-monotonic os:clock-gettime u32hash swap u32hash xor ;; high seed
;

;; useful to store 16-byte seeds
code: 4!  ( a b c d addr -- )
  lea   edi,[TOS+12]
  std
  pop   eax
  stosd
  pop   eax
  stosd
  pop   eax
  stosd
  pop   eax
  stosd
  cld
  pop   TOS
  urnext
endcode

code: 4@  ( addr -- a b c d )
  xchg  esi,TOS
  lodsd
  push  eax
  lodsd
  push  eax
  lodsd
  push  eax
  lodsd
  push  eax
  xchg  esi,TOS
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; generate 32-bit random number, with first stream
;; it uses smaller 64 bit state, and slightly faster
;;
;; code for:
;; oldstate = state;
;; state = oldstate*6364136223846793005UL+((42<<1)|1);
;; u32 xorshifted = ((oldstate>>18)^oldstate)>>27;
;; u32 rot = oldstate>>59;
;; res = (xorshifted>>rot)|(xorshifted<<((-rot)&31));
code: PCG32-NEXT  ( statelo statehi -- newstatelo newstatehi u32prv )
  push  TOS
  push  EIP    ;; it will be used in the code
  sub   esp,8  ;; and two temp vars
  ;; [esp+0]: tmpvar0
  ;; [esp+4]: tmpvar1
  ;; [esp+8]: EIP
  ;; [esp+12]: statehi
  ;; [esp+16]: statelo
  ld    edx,[esp+12]  ;; statehi
  ld    eax,[esp+16]  ;; statelo
  ld    [esp+0],edx   ;; tmpvar0
  imul  edx,edx,0x4c957f2d
  imul  ecx,eax,0x5851f42d
  add   ecx,edx
  ld    edx,0x4c957f2d
  ld    [esp+4],eax   ;; tmpvar1
  mul   edx
  add   edx,ecx
  add   eax,85        ;; inclo
  adc   edx,0         ;; inchi
  ld    [esp+12],edx  ;; statehi
  ld    edx,[esp+0]   ;; tmpvar0
  ld    [esp+16],eax  ;; statelo
  ld    eax,[esp+4]   ;; tmpvar1
  shrd  eax,edx,0x12
  shr   edx,0x12
  xor   eax,[esp+4]   ;; tmpvar1
  xor   edx,[esp+0]   ;; tmpvar0
  shrd  eax,edx,0x1b
  shr   edx,0x1b
  ld    esi,eax
  ld    edx,[esp+0]   ;; tmpvar0
  ld    eax,[esp+4]   ;; tmpvar1
  shr   edx,0x1b
  ld    eax,edx
  ld    edi,eax
  ld    eax,esi
  ld    ecx,edi
  xor   edx,edx
  shr   eax,cl
  ld    ecx,edi
  neg   ecx
  ld    edx,esi
  and   ecx,0x1f
  shl   edx,cl
  or    eax,edx
  ld    TOS,eax  ;; u32prv
  add   esp,8
  pop   EIP
  urnext
endcode

: PCG32-SEED-U64  ( dlo dhi -- statelo statehi )
  0 0 pcg32-next drop d+ pcg32-next drop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bob Jenkins small PRNG -- http://burtleburtle.net/bob/rand/smallprng.html
;;
(* original:
  e = a-ROT(b, 27);
  a = b^ROT(c, 17);
  b = c+d;
  c = e+d;
  d = e+a; -- u32prv
*)
code: BJ-NEXT  ( a b c d -- a b c d u32prv )
  push  TOS
  ;; [esp+0]: d
  ;; [esp+4]: c
  ;; [esp+8]: b
  ;; [esp+12]: a
  ld    eax,[esp+8]
  ld    edx,eax  ;; save b, to use it later
  ld    edi,[esp+12]
  rol   eax,27
  sub   edi,eax
  ;; EDX: b
  ;; ECX: d
  ;; EDI: e
  ld    eax,[esp+4]
  ld    ebx,eax  ;; save c, to use it later
  rol   eax,17
  xor   edx,eax
  ld    [esp+12],edx
  ;; EBX: c
  ;; ECX: d
  ;; EDI: e
  add   ebx,ecx
  ld    [esp+8],ebx
  ;; ECX: d
  ;; EDI: e
  add   ecx,edi
  ld    [esp+4],ecx
  ;; EDI: e
  add   edi,[esp+12]
  ld    TOS,edi
  ld    [esp+0],edi
  urnext
endcode

: BJ-SEED-U32  ( u -- a b c d )
  ;; dup u32hash bj-seed-u64
  ;; this is how BJ does it for 32-bit seeds
  0xf1ea5eed swap dup dup  ;; initial seed
  20 for bj-next drop endfor ;; skip first 20 values, to perturb seed
;

;; k8: this sux, don't use it!
: BJ-SEED-U64  ( du -- a b c d )
  u32hash swap u32hash xor bj-seed-u32
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; produce biased result [0..range)
;; this is faster than division, tho
;; it multiplies range and prv, and takes the high 32 bits of 64-bit result
;; this (seemingly) performs worser than modulo on PRNGs with non-32-bit range
;; if you need biased ranged result, and you are unsure, use "UMOD"
code: SMALL-BIASED-RANGE ( u32prv urange -- u1 )
  pop   eax
  mul   TOS
  ld    TOS,edx
  urnext
endcode


voc-set-active FORTH
