;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary-nohash C1S
voc-set-active C1S

;; terminate c1s with zero (but don't increment length)
: zterm  ( addr -- )
  bcount + 0c!
;

;; make sure that srccount fits into byte length
: clamp-count  ( srccount destcount -- srccount-clamped )
  over +if
    0 max dup 255 < if
      over + dup 255 u> if 255 - - 0 max else drop endif
    else 2drop 0 endif
  else 2drop 0 endif
;

;; copy c1s
: copy  ( addrsrc addrdest -- )
  >r dup bcount-only 1+ r> swap cmove
;

;; copy counted string to c1s
: copy-counted  ( addrsrc count addrdest -- )
  swap 0 255 clamp swap
  2dup c!
  1+ swap cmove
;

;; cat counted string to c1s
: cat-counted  ( addr count addrdest -- )
  over +if
    dup >r bcount-only clamp-count
    r@ bcount + swap dup >r cmove
    r> r> +c!
  else 2drop drop endif
;

;; cat c1s to another c1s
: cat  ( addrsrc addrdest -- )
  swap bcount rot cat-counted
;

;; append char to c1s
: cat-char  ( char addr -- )
  dup bcount-only 255 < if
    dup >r bcount + c! r> 1+c!
  else 2drop endif
;

;; append slash to c1s (but only if it doesn't end with slash, and is not empty)
;; useful for path manipulation
: add-slash  ( addr -- )
  dup bcount-only 1 255 within if
    dup bcount + dup 1- c@ [char] / = ifnot [char] / swap c! 1+c! else 2drop endif
  else drop endif
;

: skip-aligned  ( addr -- nextaddr )  bcount + 3 + -4 and ;
: skip-alignedz  ( addr -- nextaddr )  bcount + 4+ -4 and ;

voc-set-active FORTH
