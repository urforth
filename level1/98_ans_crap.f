;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ANS morons
alias 1+ CHAR+  ( count -- count+1 )
alias 1- CHAR-  ( count -- count-1 )

;; they are immediate because they're doing nothing at all
: CHARS  ( count -- count )  ; immediate-noop
: ALIGN  ( -- )  ; immediate-noop
: ALIGNED  ( addr -- addr )  ; immediate-noop

alias bitnot invert
alias CFA->PFA >BODY
