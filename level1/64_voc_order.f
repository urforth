;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; shows current search order
: ORDER  ( -- )
  ." CONTEXT:"
  context
  begin dup (forth-voc-stack) u>= while
    dup @ space vocid.
    cell-
  repeat
  drop
  space ." (ROOT)\nCURRENT: "
  current @ vocid.
  ."  (mode: "
   (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) and if ." private" else ." public" endif
  ." )\n"
;

;; makes all newly created words public
: <public-words>  ( -- )
  (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) ~and to (CURRENT-CREATE-MODE)
;

;; makes all newly created words hidden
: <hidden-words>  ( -- )
  (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) or to (CURRENT-CREATE-MODE)
;

;; sets top context vocabulary as the current one
;; resets current mode to public
: DEFINITIONS  ( -- )
  context @ current ! <public-words>
;

;; makes top context vocabulary as the only one, and makes it current too
;; resets current mode to public
: ONLY  ( -- )
  (forth-voc-stack) to context
  ['] forth voc-cfa->vocid context !
  definitions
;

;; duplicates top context vocabulary, so it could be replaced with another one
;; resets current mode to public
: ALSO  ( -- )
  ;; check for vocstack overflow
  context cell+ dup (forth-voc-stack-end) u>= ERR-VOCABULARY-STACK-OVERFLOW ?error
  context @ over !
  to context
;

;; drop topmost context vocabulary
;; resets current mode to public
: PREVIOUS  ( -- )
  ;; check for vocstack underflow
  context (forth-voc-stack) u>= if
    context cell- to context
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: forth-wordlist  ( -- vocid )
  vocid: forth
;

;; 1: immediate; -1: normal
: search-wordlist  ( addr count vocid -- cfa 1 // cfa -1 // 0 )
  (wfind-flags>r)
  [ (wflag-smudge) (wflag-hidden) or ] literal voc-find-replace-mask
  voc-find-str
  (wfind-r>flags)
;

: get-order  ( -- vocids... vocid-count )
  (forth-voc-stack) begin dup context u<= while dup @ swap cell+ repeat drop
  context (forth-voc-stack) - 2 rshift 1+  ;; counter
;

: set-order  ( vocids... vocid-count -- )
  only dup +if
    dup >r  ;; save counter, so we will drop all arguments in any case
    (forth-voc-stack-end) (forth-voc-stack) - 2 rshift  ;; maximum
      \ endcr ." maxvocs: " dup . cr
    min
    ;; set context
    dup 1- (forth-voc-stack) cells^ to context
    (forth-voc-stack) swap
    1 swap do i pick over ! cell+ -1 +loop
    drop
    ;; drop all args
    r> 0 do drop loop
  else
    drop
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (vocorder>r)  ( -- | vocids... vocid-count current )
  r>  (forth-voc-stack) begin dup context u<= while dup @ >r cell+ repeat drop
  context (forth-voc-stack) - 2 rshift 1+ >r  ;; counter
  current @ >r  >r
;

: (r>vocorder)  ( | vocids... vocid-count current -- )
  r>  r> current ! r@ if
    r@ 1- (forth-voc-stack) cells^ dup to context
    r> begin dup while 1- swap r> over ! cell- swap repeat 2drop
  else
    rdrop only
  endif
  >r
;
