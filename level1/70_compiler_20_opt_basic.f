;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

UROPT_DEBUG_ALIAS equ 0
UROPT_DEBUG_BROPT equ 0

vocabulary OPTIMISER
voc-set-active OPTIMISER


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; replace alias call with referred address
: optimise-alias-call  ( cfa -- true // cfa false )
  begin
    ;; check for jmp
    dup c@ 0xe9 = ifnot false exit endif
      \ endcr ." ALIAS?: " dup cfa-wsize . cr
    ;; should be exactly 5/8 bytes
    dup cfa-wsize (#cfa) = ifnot false exit endif
    $if UROPT_DEBUG_ALIAS
      endcr ." ALIAS: " dup cfa->nfa id. ."  -> " dup 1+ compiler:(disp32@) cfa->nfa id. cr
    $endif
    ;; get new cfa address, and check if it is an alias too
    1+ compiler:(disp32@)
  again
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; branch stack contands addresses of branch instructions
;; end of block is 0

256 constant #jstack
#jstack cells buffer: jstack
-1 value jsp

: jpush  ( n -- )
  jsp dup -if 2drop
  else dup #jstack < if dup 1+ to jsp jstack cells^ !
  else 2drop -1 to jsp  ;; overflow
    endcr ." WARNING: branch optimiser stack overflow in \`" latest-nfa id. ." \`\n"
  endif endif
;

: jpop  ( -- n // 0 )
  jsp 1- dup -if drop 0
  else dup to jsp jstack cells^ @ endif
;

: jpush-branch  ( -- )  here [execute-tail] jpush ;

: jstack-reset  ( -- )  -1 to jsp ;
: jstack-init  ( -- )  forth:(opt-branches?) if 0 else -1 endif to jsp ;
: jstack-subinit  ( -- )  forth:(opt-branches?) if jsp 0 max to jsp 0 jpush endif ;

: jstack-pop-frame  ( -- )
  jsp +if begin jpop not-until else jstack-reset endif
;


;; type: 0 is unconditional branch; 1 is conditional branch
;; cfa: ( addr type -- stopcode )
: jstack-foreach  ( cfa -- stopcode )
  jsp 1- dup -if drop false
  else
    0 swap do
      jstack i +cells @ ?dup ifnot break endif
      dup @ ['] branch <> rot dup >r execute
      ?dup if rdrop unloop exit endif
    r> -1 +loop drop false
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$if UROPT_DEBUG_BROPT
false value (was-jump-fix)
$endif
false value (optimise-jumps-again)

: optimise-jumps  ( -- )
  jsp +if
    begin
      false to (optimise-jumps-again)
      [:  ( addr type -- stopflag )
        ifnot
          $if UROPT_DEBUG_BROPT
            false to (was-jump-fix)
            \ endcr ." ***[ " latest-nfa id. ."  ]***\n"
          $endif
          [:  ( addr braddr type -- addr stopflag )
            >r 2dup cell+ @ = if  ;; braddr jumps to addr, reroute it
              $if UROPT_DEBUG_BROPT
                (was-jump-fix) ifnot endcr ." === processing JUMP at 0x" over .hex8 ."  <" latest-nfa id. ." > ===\n" true to (was-jump-fix) endif
                endcr ."   fixing " r@ if ." X" else space endif ." BRN at 0x" dup .hex8 cr
              $endif
              cell+ over cell+ @  2dup swap @ <> if  ( addr braddr+4 [addr+4] )
                swap !  true to (optimise-jumps-again)
              else 2drop endif
            else drop endif
            rdrop false
          ;] jstack-foreach 2drop
        else drop endif
        false
      ;] jstack-foreach drop
    (optimise-jumps-again) not-until
    jstack-pop-frame
  else jstack-reset endif
;


voc-set-active FORTH
