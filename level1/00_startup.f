;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: (URFORTH-STARTUP-CODEBLOCK)
urforth_entry_point:
  ;; save argc
  ld    ecx,[esp]
  ld    [pfa "argc"],ecx
  ;; save argv
  ld    eax,esp
  add   eax,4
  ld    [pfa "argv"],eax
  ;; calc envp address
  inc   ecx    ;; skip final 0 argument
  shl   ecx,2
  add   eax,ecx
  ;; store envp
  ld    [pfa "envp"],eax

  ;; move data stack up a little (so underflows won't destroy cli args and such)
  sub   esp,64*4

  ;; probe both stacks, to ensure that we can use "sub esp" without problems
  ld    eax,esp  ;; so we will be able to restore ESP
  mov   ecx,DSTACK_SIZE+RSTACK_SIZE+2048  ;; and two extra pages, because why not
.stack_probe_loop:
  push  eax
  sub   esp,4096+4  ;; compensate previous push
  sub   ecx,1024    ;; 4096/4
  jr    nc,.stack_probe_loop
  ;; restore stack
  mov   esp,eax

  ;; fuck. so-called "modern" distros are all broken, and we cannot use BRK to extend our image
 $if 0
  ;; allocate memory for dictionary
  ;; binary size should be equal to DP
  ld    eax,[pfa "(init-mem-size)"]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  dec   ebx
  ld    [pfa "dp-last-addr"],ebx
 $else
  ld    eax,[elfhead_codesize_addr+4]  ;; virtual size
  ld    ebx,eax
  sub   ebx,[elfhead_codesize_addr]    ;; disk size
  jp    be,.startup_oom
  cp    ebx,65536
  jp    be,.startup_oom
  lea   eax,[eax+urforth_code_base_addr-1]
  ld    [pfa "dp-last-addr"],eax
  ;; set it here, why not
  ld    eax,[elfhead_codesize_addr]    ;; disk size
  add   eax,urforth_code_base_addr
  ld    [pfa "dp-protected"],eax
 $endif

  ;; fix "next" codepoint (just in case)
 $if URFORTH_DEBUG
  ;; put 0xff 0xe0 there (jmp eax)
  ld  eax,0xe0_ff
  ld  dword [urforth_next],eax
 $endif

 $if URFORTH_TLS_TYPE = URFORTH_TLS_TYPE_FS
  ;; allocate one page for uservars
  ld    eax,192  ;; mmap2
  xor   ebx,ebx  ;; address
  ld    ecx,URFORTH_MAX_USERAREA_SIZE
  ld    edx,3    ;; r/w
  ld    esi,0x22 ;; anon, private
  ld    edi,-1   ;; fd
  push  ebp
  xor   ebp,ebp
  int   0x80
  pop   ebp
  cp    eax,0xffff_f000
  jp    nc,.startup_oom
  ;; copy default values
  ld    esi,ua_default_values
  ld    edi,eax
  ld    ecx,URFORTH_MAX_USERAREA_CELLS
  rep movsd
  ;; setup base address
  ld    [eax+ua_ofs_baseaddr],eax
  ;; allocate thread data area
  ;; entry 6 is usually reserved for stack
  ;; entries 7 and 8 seems to be free
  sub   esp,4*4
  ld    dword [esp],-1     ;; get free TLS entry
  ld    dword [esp+4],eax  ;; base address
  ld    dword [esp+8],1    ;; limit, in pages
  ld    dword [esp+12],0b1_0_1_0_00_1
  ld    eax,243
  ld    ebx,esp
  int   0x80
  test  eax,eax
  jp    nz,.startup_oom
  ;; load TS with the created selector
  ld    eax,[esp]
  ld    [pfa "(user-tls-entry-index)"],eax
  lea   eax,[eax*8+3]
  ld    ts,eax
  ;; free temp buffer
  add   esp,4*4
 $endif

  ;; allocate memory for debugger/segfault stack
  ld    eax,1024
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ;; align it
  and   ebx,0xfffffff0
  ld    [urfsegfault_stack_bottom],ebx

  call  urforth_setup_segfault_handler

 $if 0
  ld    eax,5
  ld    ebx,.pverstr
  xor   ecx,ecx
  ld    edx,ecx
  syscall
  cp    eax,0
  jp    s,.startup_oom
  push  eax
  ld    eax,3
  ld    ebx,[esp]
  ld    ecx,[pfa "dp"]
  ld    edx,[pfa "dp-last-addr"]
  sub   edx,ecx
  jp    c,.startup_oom
  cp    edx,8192
  jp    c,.startup_oom
  syscall
  cp    eax,0
  jp    s,.startup_oom
  ld    ecx,eax
  ld    edi,[pfa "dp"]
  push  ecx
  push  edi
.locase_loop:
  ld    eax,[edi]
  ;;and   eax,0xdfdfdfdf
  or    eax,0x20202020
  stosd
  sub   ecx,4
  jr    nc,.locase_loop
  pop   edi
  pop   ecx
.xloop:
  ld    al,'m'
  repnz scasb
  jecxz .xloop_done
  ld    esi,edi
  lodsb
  inc   al
  cp    al,'i'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'c'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'r'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'o'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'s'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'o'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'f'+1
  jr    nz,.xloop
  lodsb
  inc   al
  cp    al,'t'+1
  jp    z,.startup_oom
  jr    .xloop
.xloop_done:
  pop   ebx
  ld    eax,6
  syscall
 $endif

  ;; setup return stack
  ld    ERP,esp
  sub   ERP,RSTACK_SIZE*4

  ;; save initial stack ponters
  ld    ts:[ua_ofs_sp0],esp
  ld    ts:[ua_ofs_rp0],ERP

  ;; and setup initial stack size (it is different from default thread stack sizes)
  ld    ts:[ua_ofs_spsize],DSTACK_SIZE
  ld    ts:[ua_ofs_rpsize],RSTACK_SIZE

  ;; patch "main thread" flag
  ld    dword ts:[ua_ofs_is_main_thread],1

  ;; and start execution
  jp    cfa "(COLD-FIRSTTIME)"

;; allocate memory via BRK
;; IN:
;;   EAX: size
;; OUT:
;;   EAX: start address (first byte)
;;   EBX: end address (*after* the last byte)
;;   other registers and flags are dead (except ESP)
.brkalloc:
  ;; save size (overallocate a little, for safety)
  add   eax,64
  push  eax

  ;; get current BRK address
  ld    eax,45       ;; brk
  xor   ebx,ebx
  syscall
  push  eax
  ;; [esp]: start address
  ;; [esp+4]: alloc size

  ;; allocate
  ld    ebx,eax
  add   ebx,[esp+4]
  ld    eax,45       ;; brk
  syscall
  ld    ebx,eax      ;; EBX=end address
  ;; check for OOM
  ld    eax,[esp]    ;; start address
  add   eax,[esp+4]  ;; alloc size
  cp    ebx,eax
  jr    c,.startup_oom
  ;; start address
  pop   eax
  ;; calc end address, to be precise
  pop   ebx
  ;; remove overallocated safety margin from end address
  sub   ebx,64
  add   ebx,eax
  ret

$if 0
.pverstr: db "/proc/version",0
$endif
.fatal_oom_msg: db "FATAL: out of memory!",10
.fatal_oom_msg_len equ $-.fatal_oom_msg

.startup_oom:
  ;; print error and exit
  mov   eax,4     ;; write
  mov   ebx,2     ;; stderr
  mov   ecx,.fatal_oom_msg
  mov   edx,.fatal_oom_msg_len
  syscall

  mov   eax,1     ;; exit
  mov   ebx,1
  syscall
endcode
(hidden) (codeblock)
