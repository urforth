;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; temp debug buffer just stores dword line and dword PC

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; final info buffer:
;;
;; header:
;;   dw itemcount        ; files with more that 64kb lines? 'cmon!
;;   dw firstline        ; files with more that 64kb lines? 'cmon!
;;   dd filename-c1strz  ; this points to special hidden buffer word, or 0
;;
;; items:
;;   db pcoffs   ; offset from the previous PC (first: from the CFA)
;;   db lineofs  ; offset from the previous line
;;
;; if lineofs is 255, next word is 16-bit line offset
;; if pcofs is 255, next word is 16-bit line offset
;;
;; the compiler doesn't store each PC, it only stores line changes
;; that is, the range for the line lasts until the next item
;; items should be sorted by PC (but the code should not fail on
;; unsorted data)
$if URFORTH_DEBUG_INFO

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "(DBGBUF-MAXSIZE)" 1024*64
(hidden)

$value "(DBGINFO-ENABLED?)" 1
(hidden)
$value "(DBGINFO-ACTIVE?)" 0
(hidden)

$value "(DBGBUF-BASE-ADDR)" 0
(hidden)
$value "(DBGBUF-END-ADDR)" 0
(hidden)

$variable "(DBGBUF-CURR-ADDR)" 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; list of known source files
;; structure:
;;   dd nextptr     ;; or 0
;;   dd dpptr       ;; in code area

$variable "(dbg-file-list-head)" 0
(hidden)

;; does no checks
;; also, list of know files is not stored in SAVEd binary
: (dbg-add-file)  ( addr count -- straddr )
  2 cells brk-alloc
  (dbg-file-list-head) @ over !
  dup (dbg-file-list-head) ! cell+
  here swap !
  ;; copy string
  dup 1+ n-allot dup >r
  2dup c! 1+ swap 0 max cmove
  r>
; (hidden)

;; this can return 0
: (dbg-find-or-add-file)  ( addr count -- straddr )
  dp-temp @ if 0 exit endif  ;; no file info for temp definitions
  str-extract-name ?dup if
    (dbg-file-list-head)
    begin @ ?dup while >r
      2dup r@ cell+ @ bcount s= if
        2drop r> cell+ @ exit
      endif
    r> repeat
    ;; add new
    (dbg-add-file)
  else drop 0 endif
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: debug-info-off  ( -- )
  false to (dbginfo-enabled?)
  false to (dbginfo-active?)
  (dbgbuf-base-addr) ?dup if 0! endif
;

: debug-info-on  ( -- )
  (dbgbuf-base-addr) if true to (dbginfo-enabled?) else debug-info-off endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-RESET)  ( -- )
  (dbgbuf-base-addr) (dbgbuf-curr-addr) !
  false to (dbginfo-active?)
  ;; set first line to 0 (to ease checks in "add-pc")
  (dbgbuf-base-addr) ?dup if 0! endif
; (hidden)

: (dbginfo-reset-activate)  ( -- )  (dbginfo-reset) true to (dbginfo-active?) ; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-ADD-PC)  ( pc line -- )
  ;; never put zero line
  ?dup ifnot drop exit endif
  ;; special? -1 items means "out of room"
  (dbgbuf-curr-addr) @ ?dup ifnot 2drop exit endif
  ( pc line dbgbufaddr )
  ;; check if the line is the same (if we have no lines, there will be zero)
  2dup @ = if drop 2drop exit endif
  ;; check if we have enough room
  dup 3 +cells (dbgbuf-end-addr) u> if
    ;; out of buffer, abort debug info generation
    drop 2drop (dbgbuf-curr-addr) 0!
    exit
  endif
  ;; put line and pc
  ( pc line dbgbufaddr )
  tuck ! cell+  ;; line
  tuck ! cell+  ;; pc
  ;; put current line number to the next item (for equality check above)
  dup 2 -cells @ over !
  (dbgbuf-curr-addr) !
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-ADD-HERE)  ( -- )
  ;; put debug info
  dp-temp @ ifnot  ;; no debug info for temporary words
    (dbginfo-active?) (dbginfo-enabled?) logand if
      here tib-curr-line (dbginfo-add-pc)
    endif
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create compressed debug info, set dfa

;; let's use globals here, because why not?
$value "(dbginfo-cpbuf-start)" 0
$value "(dbginfo-cpbuf-curpos)" 0
$value "(dbginfo-cpbuf-currline)" 0
$value "(dbginfo-cpbuf-currpc)" 0

: (dbginfo-build-byte)  ( b -- )
  (dbginfo-cpbuf-curpos) c!
  (dbginfo-cpbuf-curpos) 1+ to (dbginfo-cpbuf-curpos)
;

: (dbginfo-build-word)  ( w -- )
  dup 0xff and (dbginfo-build-byte)
  8 rshift (dbginfo-build-byte)
;

: (dbginfo-build-dword)  ( dw -- )
  dup 0xffff and (dbginfo-build-word)
  16 rshift (dbginfo-build-word)
;


;; prerequisite: debug info must exist, and must not be empty
: (dbginfo-build-compressed)  ( destbuf -- destbuf size )
  ;; build file name (do it here, because it may allocate at here)
  (tib-curr-fname) count (dbg-find-or-add-file) >r
  ;; init vars
  dup to (dbginfo-cpbuf-start)
  to (dbginfo-cpbuf-curpos)
  ;; get starting line
  (dbgbuf-base-addr) @ to (dbginfo-cpbuf-currline)
  ;; get starting PC
  latest-cfa to (dbginfo-cpbuf-currpc)
  ;; create header
  ;; item count (we know that in advance)
  (dbgbuf-curr-addr) @ (dbgbuf-base-addr) - 3 rshift (dbginfo-build-word)
  ;; first line
  (dbginfo-cpbuf-currline) (dbginfo-build-word)
  ;; file name
  r> (dbginfo-build-dword)
  (dbgbuf-curr-addr) @ (dbgbuf-base-addr) do
    ;;   db pcoffs   ; offset from the previous PC (first: from the CFA)
    ;;   db lineofs  ; offset from the previous line
    ;;
    ;; if lineofs is 255, next word is 16-bit line offset
    ;; if pcofs is 255, next word is 16-bit line offset
    ;; PC
    i cell+ @ (dbginfo-cpbuf-currpc) - dup 255 < if
      ;; 8-bit offset
      (dbginfo-build-byte)
    else
      ;; 16-bit offset
      255 (dbginfo-build-byte)
      (dbginfo-build-word)
    endif
    ;; update current PC
    i cell+ @ to (dbginfo-cpbuf-currpc)
    ;; line number
    i @ (dbginfo-cpbuf-currline) - dup 255 < if
      ;; 8-bit offset
      (dbginfo-build-byte)
    else
      ;; 16-bit offset
      255 (dbginfo-build-byte)
      (dbginfo-build-word)
    endif
    ;; update current line
    i @ to (dbginfo-cpbuf-currline)
  2 cells +loop
  ;; return addr and size
  (dbginfo-cpbuf-start) (dbginfo-cpbuf-curpos) over -
;

: (dbginfo-finalize-and-copy)  ( -- )
  (dbginfo-active?) (dbginfo-enabled?) logand if
    (dbgbuf-base-addr) ?dup if
      @ if
        ;; save current PC
        (dbgbuf-curr-addr) @ 0! (dbginfo-add-here)
        ;; compressed should never be bigger than the original
        (dbgbuf-curr-addr) @ (dbgbuf-base-addr) - 4 +cells simple-malloc throw dup >r
        (dbginfo-build-compressed)
        ;; save here to debug info field
        here latest-nfa nfa->dfa !
        dup n-allot  ( addr bytes rva-newaddr )
        swap 0 max cmove
        r> simple-free throw
      endif
    endif
  endif
  ;; always deactivate
  (dbginfo-reset)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
..: (startup-init)  ( -- )
  (dbgbuf-maxsize) ?dup if
    brk-alloc
    dup to (dbgbuf-base-addr) (dbgbuf-maxsize) + 1- to (dbgbuf-end-addr)
    (dbg-file-list-head) 0!
  else
    0 to (dbgbuf-base-addr)
    0 to (dbgbuf-end-addr)
    false to (dbginfo-enabled?)
    false to (dbginfo-active?)
  endif
  (dbgbuf-base-addr) ?dup if 0! endif
;..

$else

\ this may be used by the external tools (assembler, for example)
: (dbginfo-reset) ; (hidden) immediate-noop
: (dbginfo-reset-activate) ; (hidden) immediate-noop

$endif
