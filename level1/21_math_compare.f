;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: =  ( n0 n1 -- n0=n1? )
  pop   eax
  cp    eax,TOS
  sete  cl
  movzx TOS,cl
  urnext
endcode

code: <>  ( n0 n1 -- n0<>n1? )
  pop   eax
  cp    eax,TOS
  setne cl
  movzx TOS,cl
  urnext
endcode

code: <  ( n0 n1 -- n<n1? )
  pop   eax
  cmp   eax,TOS
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: >  ( n0 n1 -- n0>n1? )
  pop   eax
  cmp   eax,TOS
  setg  cl
  movzx TOS,cl
  urnext
endcode

code: <=  ( n0 n1 -- n<=n1? )
  pop   eax
  cmp   eax,TOS
  setle cl
  movzx TOS,cl
  urnext
endcode

code: >=  ( n0 n1 -- n0>=n1? )
  pop   eax
  cmp   eax,TOS
  setge cl
  movzx TOS,cl
  urnext
endcode

code: U<  ( u0 u1 -- u0<u1? )
  pop   eax
  cmp   eax,TOS
  setb  cl
  movzx TOS,cl
  urnext
endcode

code: U>  ( u0 u1 -- u0>u1? )
  pop   eax
  cmp   eax,TOS
  seta  cl
  movzx TOS,cl
  urnext
endcode

code: U<=  ( u0 u1 -- u0<=u1? )
  pop   eax
  cmp   eax,TOS
  setbe cl
  movzx TOS,cl
  urnext
endcode

code: U>=  ( u0 u1 -- u0>=u1? )
  pop   eax
  cmp   eax,TOS
  setae cl
  movzx TOS,cl
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; same as NOT
code: 0= ; ( n0 -- n=0? )
  test  TOS,TOS
  setz  cl
  movzx TOS,cl
  urnext
endcode

;; same as NOTNOT
code: 0<>  ( n0 -- n<>0? )
  test  TOS,TOS
  setnz cl
  movzx TOS,cl
  urnext
endcode

code: 0<  ( n0 -- n<0? )
  cp    TOS,0
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: 0>  ( n0 -- n>0? )
  cp    TOS,0
  setg  cl
  movzx TOS,cl
  urnext
endcode

code: 0<=  ( n0 -- n<=0? )
  cp    TOS,0
  setle cl
  movzx TOS,cl
  urnext
endcode

code: 0>=  ( n0 -- n>=0? )
  cp    TOS,0
  setge cl
  movzx TOS,cl
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ooh...
;; : BOUNDS  ( addr count -- addr+count addr )  over + swap ;
code: BOUNDS  ( addr count -- addr+count addr )
  ld    eax,TOS    ;; EAX=count
  ld    TOS,[esp]  ;; TOS=addr
  add   [esp],eax  ;; addr+count
  urnext
endcode

;; n >= a and n < b
;; : WITHIN  ( n a b -- flag )  over - >r - r> u< ;
code: WITHIN  ( n a b -- flag )
  pop   ebx     ;; a
  pop   eax     ;; n
  ;; EAX: n
  ;; EBX: a
  ;; TOS: b
  ld    edx,eax
  sub   TOS,ebx  ;; b=b-a
  sub   edx,ebx  ;; t=n-a
  sub   edx,TOS  ;; t=t-b
  sbb   TOS,TOS  ;; b=b-b-carry
  neg   TOS      ;; because our "true" is 1, not -1
  urnext
endcode

;; un >= ua and un < ub
code: UWITHIN  ( un ua ub -- flag )
  pop   ebx      ;; ua
  pop   eax      ;; u
  ;; EAX: u
  ;; EBX: ua
  ;; TOS: ub
  cmp   eax,TOS  ;; u : ub
  ld    TOS,0
  jr    ae,@f
  cmp   ebx,eax
  jr    a,@f
  inc   TOS
@@:
  urnext
endcode

;; u >= ua and u <= ub (unsigned compare)
code: BOUNDS?  ( u ua ub -- flag )
  pop   ebx      ;; ua
  pop   eax      ;; u
  ;; EAX: u
  ;; EBX: ua
  ;; TOS: ub
  cmp   eax,TOS  ;; u : ub
  ld    TOS,0
  jr    a,@f
  cmp   ebx,eax
  jr    a,@f
  inc   TOS
@@:
  urnext
endcode


code: CLAMP  ( n a b -- n-clamped )
  pop   eax
  pop   ebx
  xchg  ebx,TOS
  ;; EAX: a
  ;; EBX: b
  ;; TOS: n
  cp    TOS,eax
  cmovl TOS,eax
  cp    TOS,ebx
  cmovg TOS,ebx
  urnext
endcode

code: UCLAMP  ( u ua ub -- u-clamped )
  pop   eax
  pop   ebx
  xchg  ebx,TOS
  ;; EAX: a
  ;; EBX: b
  ;; TOS: n
  cp    TOS,eax
  cmovb TOS,eax
  cp    TOS,ebx
  cmova TOS,ebx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; the following words are used in various CASE-OF
;;

code: (OF=)  ( n0 n1 -- n0 n0=n1 )
  ld    eax,[esp]
  cp    eax,TOS
  sete  cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF<>)  ( n0 n1 -- n0 n0<>n1? )
  ld    eax,[esp]
  cp    eax,TOS
  setne cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF<)  ( n0 n1 -- n0 n0<n1 )
  ld    eax,[esp]
  cmp   eax,TOS
  setl  cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF>)  ( n0 n1 -- n0>n1? )
  ld    eax,[esp]
  cmp   eax,TOS
  setg  cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF<=)  ( n0 n1 -- n<=n1? )
  ld    eax,[esp]
  cmp   eax,TOS
  setle cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF>=)  ( n0 n1 -- n0>=n1? )
  ld    eax,[esp]
  cmp   eax,TOS
  setge cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF-U<)  ( u0 u1 -- u0<u1? )
  ld    eax,[esp]
  cmp   eax,TOS
  setb  cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF-U>)  ( u0 u1 -- u0>u1? )
  ld    eax,[esp]
  cmp   eax,TOS
  seta  cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF-U<=)  ( u0 u1 -- u0<=u1? )
  ld    eax,[esp]
  cmp   eax,TOS
  setbe cl
  movzx TOS,cl
  urnext
endcode
(hidden)

code: (OF-U>=)  ( u0 u1 -- u0>=u1? )
  ld    eax,[esp]
  cmp   eax,TOS
  setae cl
  movzx TOS,cl
  urnext
endcode
(hidden)


code: (OF-AND)  ( n0 n1 -- n0 n0&n1 )
  ld    eax,[esp]
  xchg  eax,TOS
  and   TOS,eax
  urnext
endcode
(hidden)

code: (OF-~AND)  ( n0 n1 -- n0&~n1 )
  not   TOS
  ld    eax,[esp]
  xchg  eax,TOS
  and   TOS,eax
  urnext
endcode
(hidden)

;; n >= a and n < b
code: (OF-WITHIN)  ( n a b -- a flag )
  pop   ebx
  ld    eax,[esp]
  ld    edx,eax
  sub   TOS,ebx
  sub   edx,ebx
  sub   edx,TOS
  sbb   TOS,TOS
  neg   TOS      ;; because our "true" is 1, not -1
  urnext
endcode
(hidden)

;; u >= ua and u < ub (unsigned compare)
code: (OF-UWITHIN)  ( u ua ub -- n flag )
  pop   ebx
  ld    eax,[esp]
  cmp   eax,TOS
  ld    TOS,0
  jr    ae,@f
  cmp   ebx,eax
  jr    a,@f
  inc   TOS
@@:
  urnext
endcode
(hidden)

;; u >= ua and u <= ub (unsigned compare)
code: (OF-BOUNDS)  ( u ua ub -- n flag )
  pop   ebx
  ld    eax,[esp]
  cmp   eax,TOS
  ld    TOS,0
  jr    a,@f
  cmp   ebx,eax
  jr    a,@f
  inc   TOS
@@:
  urnext
endcode
(hidden)
