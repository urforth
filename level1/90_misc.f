;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; brk-buffer chain; we have to restore all those buffers on loading
;; record format:
;;   dd prev
;;   dd valpfa
;;   dd size
$variable "(brk-buf-chain)" 0
(hidden)

: (add-brk-buffer)  ( size valpfa -- )
  3 cells n-allot
  ;; prev
  (brk-buf-chain) @ over !
  dup (brk-buf-chain) ! cell+
  dup nrot ! cell+
  !
 $if 0
  ;; debug code
  endcr ." (save-add-brk-buffer): valpfa=0x" (brk-buf-chain) @ cell+ @ .hex8
  ." ; size=" (brk-buf-chain) @ 2 +cells @ .hex8
  ." ; name=" (brk-buf-chain) @ cell+ @ pfa->cfa cfa->nfa id. cr
 $endif
; (hidden)


: (restore-brk-buffers)  ( size valpfa -- )
  (brk-buf-chain)
  begin
    @ ?dup
  while
    dup
    cell+ dup @ swap  ;; valpfa
    cell+ @           ;; size
    brk-alloc swap !
   $if 0
    ;; debug code
    >r
    endcr ." (save-restore-brk-buffers): valpfa=0x" r@ cell+ @ .hex8
    ." ; size=" r@ 2 +cells @ .hex8
    ." ; name=" r@ cell+ @ pfa->cfa cfa->nfa id. cr
    r>
   $endif
  repeat
; (hidden)

..: (startup-init)  (restore-brk-buffers) ;..


;; this allocates memory with BRK
: BRK-BUFFER:  ( size -- )
  dup >r  ;; we should notify saver about new buffer here, hence the save
  brk-alloc value
  r> latest-cfa cfa->pfa (add-brk-buffer)
;
