;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

format ELF executable 3
$entry urforth_entry_point

;; this is defined by the target compiler
;;urforth_code_base_addr equ $

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
URFORTH_DEBUG equ URFORTH_DEBUGGER_ENABLED
URFORTH_DEBUG_INFO equ URFORTH_DEBUG_INFO_ENABLED
URFORTH_EXTRA_STACK_CHECKS equ 0

;; various compile-time options
WLIST_HASH_BITS equ 6

;; size in items
DSTACK_SIZE equ 65536
RSTACK_SIZE equ 65536


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; register usage:
;;   ebp: return stack
;;   esp: data stack
;;   esi: instruction pointer
;;   ecx: TOS
;;
;; direction flag must NOT be set!
db "Alice!"

;; let it always be here
$if URFORTH_DEBUG
  ;; universal dispatcher for "next" command
  ;; debugger will patch jump to itself here
  ;; when debugger is not active, this will be simple "jmp eax"
  ;; startup code will patch it to "jmp eax"
  ;; reserve 32 bytes for this, because why not?
  $align 32,0
$label urforth_next
$asm db 0xff,0xe0
$asm rb 30
(*
  ;; breakpoint
$label urforth_next_ptr
  dd urforth_next_normal
  ;; normal next
$label urforth_next_normal
$asm jp    eax
*)
$endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some sanity checks
$if WLIST_HASH_BITS < 0
  $error "invalid WLIST_HASH_BITS: should be [0..8]!"
$endif

$if WLIST_HASH_BITS > 8
  $error "invalid WLIST_HASH_BITS: should be [0..8]"
$endif

;; sorry! one day i'll implement shifts in asm evaluator
$if WLIST_HASH_BITS = 1
WLIST_HASH_MASK equ 0x01
WLIST_HASH_CELLS equ 2
$endif

$if WLIST_HASH_BITS = 2
WLIST_HASH_MASK equ 0x03
WLIST_HASH_CELLS equ 4
$endif

$if WLIST_HASH_BITS = 3
WLIST_HASH_MASK equ 0x07
WLIST_HASH_CELLS equ 8
$endif

$if WLIST_HASH_BITS = 4
WLIST_HASH_MASK equ 0x0f
WLIST_HASH_CELLS equ 16
$endif

$if WLIST_HASH_BITS = 5
WLIST_HASH_MASK equ 0x1f
WLIST_HASH_CELLS equ 32
$endif

$if WLIST_HASH_BITS = 6
WLIST_HASH_MASK equ 0x3f
WLIST_HASH_CELLS equ 64
$endif

$if WLIST_HASH_BITS = 7
WLIST_HASH_MASK equ 0x7f
WLIST_HASH_CELLS equ 128
$endif

$if WLIST_HASH_BITS = 8
WLIST_HASH_MASK equ 0xff
WLIST_HASH_CELLS equ 256
$endif

WLIST_HASH_BYTES equ WLIST_HASH_CELLS*4


;; create "FORTH" vocabulary header first, because why not
$vocabheader "FORTH" forth_wordlist_vocid

$include 00_startup.f
$include 01_segfault_handler.f
$include 02_debugger.f
$include 03_do_codeblocks.f
$include 05_base_vars.f
$include 07_syscall.f
$include 08_termio_low.f
$include 10_litbase.f
$include 14_dstack.f
$include 15_rstack.f
$include 16_peekpoke.f
$include 17_dp.f
$include 18_simple_malloc.f
$include 19_dbg_info.f
$include 20_math_base.f
$include 21_math_compare.f
$include 22_math_muldiv.f
$include 24_math_double.f
$include 28_print_number.f
$include 30_count_str.f
$include 31_ur_str_ext.f
$include 32_str_hash_name.f
$include 34_str_hash_more.f
$include 36_str_ext_asm.f
$include 38_c4s_str.f
$include 39_c1s_str.f
$include 40_termio_high.f
$include 42_tib.f
$include 46_os_face.f
$include 50_error.f
$include 52_exceptions.f
$include 60_wordmisc.f
$include 61_wordfind_low.f
$include 62_create_vocbase.f
$include 63_wordfind_high.f
$include 64_voc_order.f
$include 66_voc_other.f
$include 68_parse.f
$include 70_compiler_helpers.f
$include 71_compiler_if_begin_do.f
$include 72_compiler_mid.f
$include 74_compiler_high.f
$include 76_compiler_override_enum.f
$include 78_compiler_sc_colon.f
$include 82_tload.f
$include 84_interpret.f
$include 86_save.f
$include 88_main_startup.f
$include 89_cond_comp.f
$include 90_misc.f
$include 92_use.f
$include 94_envquery.f
$include 96_prng.f
$include 98_ans_crap.f

$include 99_misc_tests.f

  db "Miriel!"

;; WARNING! it cannot be "equ"!
$label urforth_code_end
