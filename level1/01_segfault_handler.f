;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: (URFORTH-SEGFAULT-CODEBLOCK)

urfsegfault_output_fd: dd 2

urfsegfault_dump_ddepth: dd 16  ;; default depth of the data stack dump-1
urfsegfault_dump_rdepth: dd 24  ;; default depth of the data stack dump-1

urfsegfault_dbg_retaddr: dd 0
urfsegfault_reg_eax: dd 0
urfsegfault_reg_ebx: dd 0
urfsegfault_reg_ecx: dd 0
urfsegfault_reg_edx: dd 0
urfsegfault_reg_esi: dd 0
urfsegfault_reg_edi: dd 0
urfsegfault_reg_ebp: dd 0
urfsegfault_reg_esp: dd 0
urfsegfault_reg_flags: dd 0

;; set in startup code
urfsegfault_stack_bottom: dd 0

urfsegfault_throw_error: dd 1
urfsegfault_stacktrace: dd 1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print char from AL
;; all registers are preserved, including flags
;;
urfsegfault_emit_al:
  save_all_regs
  push  eax       ;; we will write from here
  ld    eax,4     ;; write
  ld    ebx,[urfsegfault_output_fd]
  ld    ecx,esp   ;; address
  ld    edx,1     ;; length
  syscall
  pop   eax
  restore_all_regs
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print string from ECX, length in EDX
;; all registers are preserved, including flags
;;
urfsegfault_emit_str_ecx_edx:
  save_all_regs
  ld    eax,4     ;; write
  ld    ebx,[urfsegfault_output_fd]
  syscall
  restore_all_regs
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print string from ECX, length in EDX
;; all registers are preserved, including flags
;;
urfsegfault_emit_str_ecx_edx_safe:
  save_all_regs
.prloop:
  or    edx,edx
  jr    z,.done
  movzx eax,byte [ecx]
  cp    al,32
  jr    nc,@f
  ld    al,'?'
@@:
  call  urfsegfault_emit_al
  inc   ecx
  dec   edx
  jr    .prloop
.done:
  restore_all_regs
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print AL as hex
;; all registers are preserved, including flags
;;
urfsegfault_emit_hex_al:
  pushfd
  push  eax
  push  eax
  shr   al,4
  Nibble2Hex
  call  urfsegfault_emit_al
  pop   eax
  Nibble2Hex
  call  urfsegfault_emit_al
  pop   eax
  popfd
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print AX as hex
;; all registers are preserved, including flags
;;
urfsegfault_emit_hex_ax:
  pushfd
  push  eax
  push  eax
  shr   eax,8
  call  urfsegfault_emit_hex_al
  pop   eax
  call  urfsegfault_emit_hex_al
  pop   eax
  popfd
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print EAX as hex
;; all registers are preserved, including flags
;;
urfsegfault_emit_hex_eax:
  pushfd
  push  eax
  push  eax
  shr   eax,16
  call  urfsegfault_emit_hex_ax
  pop   eax
  call  urfsegfault_emit_hex_ax
  pop   eax
  popfd
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print EAX as signed decimal
;; all registers are preserved, including flags
;; i could do this without divs, but meh
;;
urfsegfault_emit_dec_eax:
  save_all_regs
  or    eax,eax
  jr    ns,@f
  urfsegfault_emit '-'
  neg   eax
@@:
  call  .prloop
  restore_all_regs
  ret
.prloop:
  ld    ecx,10
  xor   edx,edx
  div   ecx
  ;; EAX: quotient; EDX: remainder
  or    eax,eax
  jr    z,@f
  ;; recurse
  push  eax
  push  edx
  call  .prloop
  pop   edx
  pop   eax
@@:
  xchg  eax,edx
  ;; EDX: quotient; EAX: remainder
  add   al,'0'
  call  urfsegfault_emit_al
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get source file name from nfa
;;
;; IN:
;;   EAX: nfa
;; OUT:
;;   EAX: byte-counted string (or 0)
;; other registers are preserved (except flags)
;;
urfsegfault_nfa_get_fname:
  ld    eax,[eax-20]
  or    eax,eax
  jr    z,.none
  ld    eax,[eax+4]
.none:
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get source file name from nfa
;;
;; IN:
;;   EAX: nfa
;; OUT:
;;   none
;; all registers are preserved (except flags)
;;
urfsegfault_nfa_write_fname:
  push  eax
  push  ecx
  push  esi
  call  urfsegfault_nfa_get_fname
  or    eax,eax
  jr    z,.nofname
  movzx ecx,byte [eax]
  jecxz .nofname
  lea   esi,[eax+1]
  urfsegfault_emit ':'
.nloop:
  lodsb
  urfsegfault_emit al
  loop  .nloop
.nofname:
  pop   esi
  pop   ecx
  pop   eax
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; move to cfa from nfa
;;
;; IN:
;;   EAX: nfa
;; OUT:
;;   EAX: cfa
;; other registers are preserved (except flags)
;;
urfsegfault_nfa2cfa:
  push  ecx
  movzx ecx,byte [eax]
  add   eax,ecx
  $if URFORTH_ALIGN_CFA
  add   eax,4
  or    eax,3
  inc   eax
  $else
  add   eax,4+1  ;; lenflags, trailing length
  $endif
  pop   ecx
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; move to nfa from cfa
;;
;; IN:
;;   EAX: cfa
;; OUT:
;;   EAX: nfa
;; other registers are preserved (except flags)
;;
urfsegfault_cfa2nfa:
  push  ecx
  movzx ecx,byte [eax-1]
  add   ecx,4+1  ;; lenflags, trailing length
  sub   eax,ecx
  pop   ecx
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; type word name
;; correctly handles the case when EAX == 0
;;
;; IN:
;;   EAX: nfa
;; OUT:
;; all registers are preserved (except flags)
;;
urfsegfault_nfaprint:
  push  eax
  push  ecx
  push  edx
  or    eax,eax
  jr    z,.noword
  ;; load length to edx
  movzx edx,byte [eax]
  or    edx,edx
  jr    z,.nonamed
  add   eax,4   ;; skip length and flags
  ld    ecx,eax ;; starting address
  call  urfsegfault_emit_str_ecx_edx_safe
.nonamed:
  pop   edx
  pop   ecx
  pop   eax
  ret
.noword:
  urfsegfault_printstr "<???>"
  jr    .nonamed


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; skip word argument
;; correctly handles the case when EAX == 0
;;
;; IN:
;;   EAX: nfa
;;   ESI: code pointer
;; OUT:
;;   EAX: dead
;;   ESI: (new) code pointer
;; all registers are preserved (except flags)
;;
urfsegfault_nfa_skip_arg:
  or    eax,eax
  jr    nz,.cont
  ret
.cont:
  ;; load arg type
  movzx eax,byte [eax+1]

urfsegfault_nfa_skip_arg_type_in_eax:
  ;; branch?
  cp    al,WARG_BRANCH
  jr    nz,@f
  add   esi,4
  jr    .done
@@:
  ;; 4 byte literal?
  cp    al,WARG_LIT
  jr    nz,@f
  add   esi,4
  jr    .done
@@:
  ;; 1 byte unsigned literal?
  cp    al,WARG_U8
  jr    nz,@f
  inc   esi
  jr    .done
@@:
  ;; 1 byte signed literal?
  cp    al,WARG_S8
  jr    nz,@f
  inc   esi
  jr    .done
@@:
  ;; 2 byte unsigned literal?
  cp    al,WARG_U16
  jr    nz,@f
  add   esi,2
  jr    .done
@@:
  ;; 2 byte signed literal?
  cp    al,WARG_S16
  jr    nz,@f
  add   esi,2
  jr    .done
@@:
  ;; cell-counted string?
  cp    al,WARG_C4STRZ
  jr    nz,@f
  lodsd
  add   esi,eax
  ;; skip trailing zero byte and align
  or    esi,3
  inc   esi
  jr    .done
@@:
  ;; byte-counted string?
  cp    al,WARG_C1STRZ
  jr    nz,@f
  lodsb
  movzx eax,al
  add   esi,eax
  ;; skip trailing zero byte and align
  or    esi,3
  inc   esi
  jr    .done
@@:
  ;; cfa?
  cp    al,WARG_CFA
  jr    nz,@f
  add   esi,4
  jr    .done
@@:
  ;; cblock?
  cp    al,WARG_CBLOCK
  jr    nz,@f
  lodsd
  ld    esi,eax
  jr    .done
@@:
  ;; vocid?
  cp    al,WARG_VOCID
  jr    nz,@f
  add   esi,4
@@:
.done:
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; type word argument
;; correctly handles the case when EAX == 0
;;
;; IN:
;;   EAX: nfa
;;   ESI: code pointer
;; OUT:
;;   ESI: (new) code pointer
;; all registers are preserved (except flags)
;;
urfsegfault_nfa_print_arg:
  push  eax
  push  ecx
  push  edx
  or    eax,eax
  jr    z,.done
  ;; load arg type
  ld    cl,byte [eax+1]
  jecxz .done
  ;; branch?
  cp    cl,WARG_BRANCH
  jr    nz,@f
  urfsegfault_emit ' '
  lodsd
  call  urfsegfault_emit_hex_eax
.done:
  pop   edx
  pop   ecx
  pop   eax
  ret
@@:
  ;; literal?
  cp    cl,WARG_LIT
  jr    nz,@f
  lodsd
.printlit:
  urfsegfault_emit ' '
  call  urfsegfault_emit_dec_eax
  jr    .done
@@:
  ;; 1 byte unsigned literal?
  cp    cl,WARG_U8
  jr    nz,@f
  lodsb
  movzx eax,al
  jr    .printlit
@@:
  ;; 1 byte signed literal?
  cp    cl,WARG_S8
  jr    nz,@f
  lodsb
  movsx eax,al
  jr    .printlit
@@:
  ;; 2 byte unsigned literal?
  cp    cl,WARG_U16
  jr    nz,@f
  lodsw
  movzx eax,ax
  jr    .printlit
@@:
  ;; 1 byte signed literal?
  cp    cl,WARG_S16
  jr    nz,@f
  lodsw
  movsx eax,ax
  jr    .printlit
@@:
  ;; cell-counted string?
  cp    cl,WARG_C4STRZ
  jr    nz,@f
  urfsegfault_printstr ' "'  ;; '
  lodsd
  ld    edx,eax
  ld    ecx,esi
  add   esi,eax
  ;; skip trailing zero byte and align
  or    esi,3
  inc   esi
  call  urfsegfault_emit_str_ecx_edx_safe
  urfsegfault_emit '"'  ;; '
  jr    .done
@@:
  ;; byte-counted string?
  cp    cl,WARG_C1STRZ
  jr    nz,@f
  urfsegfault_printstr ' "'  ;; '
  lodsb
  movzx eax,al
  ld    edx,eax
  ld    ecx,esi
  add   esi,eax
  ;; skip trailing zero byte and align
  or    esi,3
  inc   esi
  call  urfsegfault_emit_str_ecx_edx_safe
  urfsegfault_emit '"'  ;; '
  jp    .done
@@:
  ;; cfa?
  cp    cl,WARG_CFA
  jr    nz,@f
  urfsegfault_emit ' '
  lodsd
  ;;call  urfsegfault_find_by_addr
  call  urfsegfault_cfa2nfa
  call  urfsegfault_nfaprint
  jp    .done
@@:
  ;; cblock?
  cp    cl,WARG_CBLOCK
  jr    nz,@f
  urfsegfault_printstr " {cblock}"
  lodsd
  ld    esi,eax
  jp    .done
@@:
  ;; vocid?
  cp    cl,WARG_VOCID
  jr    nz,@f
  urfsegfault_printstr " {vocid}"
  lodsd
  jp    .done
@@:
  jp    .done


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find line for the PC
;;
;; IN:
;;   EAX: pc
;;   ESI: nfa
;; OUT:
;;   EAX: line
;; other registers are preserved (except flags)
;;
urfsegfault_find_pc_line_nfa:
  push  edi
  push  esi
  push  ecx
  push  edx
  push  ebx
  or    esi,esi
  jr    z,.fail
  ;; load debug info address
  ;; EDI will contain current PC
  ld    edi,esi
  ;; move EDI to CFA
  movzx ebx,byte [edi]
  add   edi,ebx
  $if URFORTH_ALIGN_CFA
  add   edi,4
  or    edi,3
  inc   edi
  $else
  add   edi,5  ;; skip lenflags, and trailing len
  $endif
  ;; load pointer to debug info
  ld    esi,[esi-20]
  or    esi,esi
  jr    z,.fail
  ;; load and check number of items
  movzx ecx,word [esi]
  ;; check number of items (just in case)
  jecxz .fail
  ;; latest line we've seen will be in EDX
  movzx edx,word [esi+2]
  ;; skip header (we aren't using file name pointer yet)
  add   esi,2+2+4
  ;; check if PC is higher
  cmp   eax,edi
  jr    c,.done
.scanloop:
  ;; get next PC
  movzx ebx,byte [esi]
  inc   esi
  cp    bl,255
  jr    nz,@f
  movzx ebx,word [esi]
  add   esi,2
@@:
  add   edi,ebx
  cp    eax,edi
  jr    c,.done
  ;; get next line
  movzx ebx,byte [esi]
  inc   esi
  cp    bl,255
  jr    nz,@f
  movzx ebx,word [esi]
  add   esi,2
@@:
  add   edx,ebx
  loop  .scanloop
.fail:
  xor   edx,edx
.done:
  ld    eax,edx
  pop   ebx
  pop   edx
  pop   ecx
  pop   esi
  pop   edi
  ret


;; the following two will be set (ONLY!) by successfull call to `urfsegfault_find_by_addr`
urfsegfault_fba_nfa: dd 0
urfsegfault_fba_end: dd 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find which word owns the specified address
;;
;; IN:
;;   EAX: address
;;   ESI: vocptr (at latestptr)
;; OUT:
;;   EAX: nfa or 0
;; ZERO SET if EAX is 0
;; other registers are preserved (except flags)
;;
urfsegfault_find_by_addr_in_voc:
  push  esi
  push  edi
  ;; ESI: lfa
.findvoc_loop:
  ;; follow lfa
  ld    esi,[esi]
  or    esi,esi
  jr    z,.not_found
  ;; load sfa
  ld    edi,[esi-8]
  ;; check for invalid sfa
  cp    esi,edi
  jr    nc,.findvoc_loop
  ;; EAX>=ESI?
  cp    eax,esi
  jr    c,.findvoc_loop
  ;; EAX<EDI?
  cp    eax,edi
  jr    nc,.findvoc_loop
  ;; i found her!
  ;; move ESI to nfa
  add   esi,8
  ld    [urfsegfault_fba_nfa],esi
  ld    [urfsegfault_fba_end],edi
.not_found:
  ld    eax,esi
  or    eax,eax   ;; fix zero flag
  pop   edi
  pop   esi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find which word owns the specified address
;; this assumes that all words are sequential in memory
;;
;; IN:
;;   EAX: address
;; OUT:
;;   EAX: nfa or 0
;; ZERO SET if EAX is 0
;; other registers are preserved (except flags)
;;
urfsegfault_find_by_addr:
  push  esi
  push  ecx
  ld    ecx,eax
  ld    esi,[pfa "(voc-link)"]
.vocloop:
  or    esi,esi
  jr    z,.quit
  sub   esi,4         ;; move to latestptr
  ld    eax,ecx
  call  urfsegfault_find_by_addr_in_voc
  ld    esi,[esi+4]   ;; load next voclink
  jr    z,.vocloop
  pop   ecx
  pop   esi
  ret
.quit:
  xor   eax,eax
  pop   ecx
  pop   esi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print return stack
;;
urfsegfault_cmd_printrstack:
  ;; print first 8 items, that should be enough
  ;; print return stack depth
  urfsegfault_printstr "=== RETURN STACK: "
  ld    eax,ts:[ua_ofs_rp0]
  sub   eax,[urfsegfault_reg_ebp]
  jr    nc,@f
  ;; underflow
  urfsegfault_printstrnl " UNDERFLOWED ==="
@@:
  sar   eax,2   ;; divide by cell
  push  eax
  urfsegfault_printdec eax
  urfsegfault_printstrnl " CELLS DEPTH ==="
  ;; print current word
  urfsegfault_printstr "**"
  ld    eax,[urfsegfault_reg_esi]
  sub   eax,4   ;; because we just loaded it
  call  urfsegfault_find_by_addr
  push  eax
  call  urfsegfault_nfaprint
  ;; print line number if we know it
  ld    esi,[esp]
  ld    eax,[urfsegfault_reg_esi]
  sub   eax,4   ;; because we just loaded it
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "  {"
  call  urfsegfault_emit_dec_eax
  ld    eax,[esp]
  call  urfsegfault_nfa_write_fname
  urfsegfault_printstr "}"
@@:
  pop   esi
  urfsegfault_cr
  ;;
  pop   ecx
  ld    esi,[urfsegfault_reg_ebp]
  or    ecx,ecx
  jr    nz,@f
  ret
@@:
  cp    ecx,[urfsegfault_dump_rdepth]
  jr    c,.printloop
  ld    ecx,[urfsegfault_dump_rdepth]

.printloop:
  ld    eax,[esi]
  urfsegfault_printstr "  "
  call  urfsegfault_find_by_addr
  push  eax   ;; for line number
  call  urfsegfault_nfaprint

  ;; print hex value
  ld    eax,[esi]
  urfsegfault_printstr "  | "
  call  urfsegfault_emit_hex_eax
  ;; print decimal value
  ld    eax,[esi]
  urfsegfault_printstr " | "
  call  urfsegfault_emit_dec_eax

  ;; print line number if we know it
  xchg  esi,[esp]
  sub   eax,4  ;; pushed value is "next to execute"
  push  esi
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "  {"
  call  urfsegfault_emit_dec_eax
  ld    eax,[esp]
  call  urfsegfault_nfa_write_fname
  urfsegfault_printstr "}"
@@:
  add   esp,4
  pop   esi

  urfsegfault_cr
  add   esi,4
  dec   ecx
  jp    nz,.printloop
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print data stack
;;
urfsegfault_cmd_printdstack:
  ;; print first 8 items, that should be enough
  ;; also, print them in backwards order (i.e. TOS will be printed last)
  ;; print data stack depth
  urfsegfault_printstr "=== DATA STACK: "
  ld    eax,ts:[ua_ofs_sp0]
  sub   eax,[urfsegfault_reg_esp]
  jr    nc,@f
  ;; underflow
  urfsegfault_printstrnl " UNDERFLOWED ==="
  ret
@@:
  sar   eax,2   ;; divide by cell
  push  eax
  urfsegfault_printdec eax
  urfsegfault_printstrnl " CELLS DEPTH ==="
  pop   ecx
  or    ecx,ecx
  jp    z,urfsegfault_cmd_printdstack_quit0

  urfsegfault_printstr "ESP="
  ld    eax,[urfsegfault_reg_esp]
  urfsegfault_printhex eax
  urfsegfault_cr

  cp    ecx,1
  jp    z,urfsegfault_cmd_printdstack_tos
  cp    ecx,[urfsegfault_dump_ddepth]
  jr    c,@f
  ld    ecx,[urfsegfault_dump_ddepth]
@@:
  ;; now print cells
  dec   ecx   ;; TOS
  ld    esi,[urfsegfault_reg_esp]
  shl   ecx,2
  add   esi,ecx
  shr   ecx,2
@@:
  or    ecx,ecx
  jr    z,@f
  sub   esi,4
  ld    eax,[esi]
  urfsegfault_printstr "  "
  urfsegfault_printhex esi
  urfsegfault_printstr ":  "
  urfsegfault_printhex eax
  urfsegfault_printstr " | "
  urfsegfault_printdec eax
  urfsegfault_cr
  dec   ecx
  jr    @b
@@:
urfsegfault_cmd_printdstack_tos:
  ;; print TOS
  urfsegfault_printstr "  "
  urfsegfault_printstr "  TOS   "
  urfsegfault_printstr ":  "
  urfsegfault_printhex [urfsegfault_reg_ecx]
  urfsegfault_printstr " | "
  urfsegfault_printdec [urfsegfault_reg_ecx]
  urfsegfault_cr

urfsegfault_cmd_printdstack_quit0:
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; debugger segfault handler
;;
SA_NOCLDSTOP  equ 1
SA_NOCLDWAIT  equ 2
SA_SIGINFO    equ 4
SA_ONSTACK    equ 0x08000000
SA_RESTART    equ 0x10000000
SA_NODEFER    equ 0x40000000
SA_RESETHAND  equ 0x80000000
SA_RESTORER   equ 0x04000000


;; sadly, we cannot return to OS to allow it reset "signal handled" flag, so we have to use "SA_NODEFER".
;; this is COMPLETELY WRONG, and should never be done like this, but tbh, anything i'll do to return
;; from SEGFAULT is a hackery anyway. it is not supposed to be used like this.
;; we can prolly use some "sys_sigreturn" magic, but it is as fragile as any other thing.

urfd_sigact:
urfd_sigact.sa_handler:  ;;dd 0  ;; union with .sa_sigaction
urfd_sigact.sa_sigaction: dd urfsegfault_segfault_action
urfd_sigact.sa_mask:      dd 0  ;; 0xffffffff
urfd_sigact.sa_flags:     dd SA_SIGINFO+SA_NODEFER  ;; "nodefer", because we will never correctly return from here
urfd_sigact.sa_restorer:  dd 0

;; struc siginfo {
;;   .si_signo   dword 1
;;   .si_errno   dword 1
;;   .si_code    dword 1
;;   ; for segfaults
;;   ;._addr      dword 1
;;   ._pad       dword 29
;;   if 0
;;   ._kill:     ;kill
;;   ._timer:    ;timer
;;   .__rt:      ;_rt
;;   ._sigchld:  ;sigchld
;;   ._sigfault: ;sigfault
;;   ._sigpoll:  ;sigpoll
;;   ._pad       dword 29
;;   end if
;; }

sigcontext_t.gs            equ  0  ;; word 1
sigcontext_t.__gsh         equ  2  ;; word 1
sigcontext_t.fs            equ  4  ;; word 1
sigcontext_t.__fsh         equ  6  ;; word 1
sigcontext_t.es            equ  8  ;; word 1
sigcontext_t.__esh         equ 10  ;; word 1
sigcontext_t.ds            equ 12  ;; word 1
sigcontext_t.__dsh         equ 14  ;; word 1
sigcontext_t.edi           equ 16  ;; dword 1
sigcontext_t.esi           equ 20  ;; dword 1
sigcontext_t.ebp           equ 24  ;; dword 1
sigcontext_t.esp           equ 28  ;; dword 1
sigcontext_t.ebx           equ 32  ;; dword 1
sigcontext_t.edx           equ 36  ;; dword 1
sigcontext_t.ecx           equ 40  ;; dword 1
sigcontext_t.eax           equ 44  ;; dword 1
sigcontext_t.trapno        equ 48  ;; dword 1
sigcontext_t.err           equ 52  ;; dword 1
sigcontext_t.eip           equ 56  ;; dword 1
sigcontext_t.cs            equ 60  ;; word 1
sigcontext_t.__csh         equ 62  ;; word 1
sigcontext_t.eflags        equ 64  ;; dword 1
sigcontext_t.esp_at_signal equ 68  ;; dword 1
sigcontext_t.ss            equ 72  ;; word 1
sigcontext_t.__ssh         equ 74  ;; word 1
sigcontext_t.fpstate       equ 78  ;; dword 1
sigcontext_t.oldmask       equ 82  ;; dword 1
sigcontext_t.cr2           equ 86  ;; dword 1
sigcontext_t._size_        equ 88

sigaltstack_t.ss_sp    equ  0  ;; dword 1
sigaltstack_t.ss_flags equ  4  ;; dword 1
sigaltstack_t.ss_size  equ  8  ;; dword 1
sigaltstack_t._size_   equ 12

sigset_t.sig    equ 0  ;; dword 2
sigset_t._size_ equ 8

ucontext_t.uc_flags    equ 0  ;; dword 1
ucontext_t.uc_link     equ 4  ;; dword 1
ucontext_t.uc_stack    equ 8
ucontext_t.uc_mcontext equ ucontext_t.uc_stack+sigaltstack_t._size_
ucontext_t.uc_sigmask  equ ucontext_t.uc_mcontext+sigcontext_t._size_
ucontext_t._size_      equ ucontext_t.uc_sigmask+sigset_t._size_


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;db  2  ;; SIGINT
urfsegfault_segfault_signames:
  ;; signum, errcode-addr, errmessage
  ;; segfault
  db 11  ;; SIGSEGV
  dd pfa "ERR-SEGFAULT"
  db "SEGFAULT",0
  ;; sigfpe
  db  8  ;; SIGFPE
  dd pfa "ERR-MATH-ZERO-DIVIDE"
  db "DIVISION BY ZERO",0
  ;; sigill
  db 4  ;; SIGILL
  dd pfa "ERR-UNDEFINED-INSTRUCTION"
  db "BAD INSTRUCTION",0
  ;; no more
  db  0  ;; no more

urfsegfault_segfault_errcode_addr: dd pfa "ERR-SEGFAULT"

urfsegfault_segfault_action:
  ld    ebp,esp
  ;; switch to our own stack
  ld    esp,[urfsegfault_stack_bottom]
  cld

  ;; signum     : dword [ebp+4]
  ;; siginfoptr : dword [ebp+8]
  ;; ucontextptr: dword [ebp+12]

  ;; copy registers from context to debugger data structures
  ld    esi,[ebp+12]
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  ld    [urfsegfault_dbg_retaddr],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eax]
  ld    [urfsegfault_reg_eax],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ebx]
  ld    [urfsegfault_reg_ebx],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ecx]
  ld    [urfsegfault_reg_ecx],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.edx]
  ld    [urfsegfault_reg_edx],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esi]
  ld    [urfsegfault_reg_esi],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.edi]
  ld    [urfsegfault_reg_edi],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ebp]
  ld    [urfsegfault_reg_ebp],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esp]
  ld    [urfsegfault_reg_esp],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eflags]
  ld    [urfsegfault_reg_flags],eax

  ;; need a trace?
  cp    dword [urfsegfault_stacktrace],0
  jp    z,.skiptrace

  urfsegfault_printstr "***TRAP: "

  ld    esi,urfsegfault_segfault_signames
.nameloop:
  lodsb
  or    al,al
  jr    z,.signotfound
  cp    al,byte [ebp+4]
  jr    z,.sigfound
  lodsd
.zeroloop:
  lodsb
  or    al,al
  jr    nz,.zeroloop
  jr    .nameloop
.signotfound:
  ld    esi,urfsegfault_segfault_signames+1
.sigfound:
  lodsd
  ld    [urfsegfault_segfault_errcode_addr],eax
.printloop:
  lodsb
  or    al,al
  jr    z,.printdone
  urfsegfault_emit al
  jr    .printloop
.printdone:

  ;;urfsegfault_printstr "***TRAP: SEGFAULT!"

  ld    esi,[ebp+12]

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  urfsegfault_printstr "  EIP="
  urfsegfault_printhex eax

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esp]
  urfsegfault_printstr " ESP="
  urfsegfault_printhex eax

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ebp]
  urfsegfault_printstr " EBP="
  urfsegfault_printhex eax

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esi]
  urfsegfault_printstr "  ESI="
  urfsegfault_printhex eax

  urfsegfault_cr

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  call  urfsegfault_find_by_addr
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "*** CURRENT WORD: "
  call  urfsegfault_nfaprint
  ;; print line number if we know it
  push  eax
  ld    esi,eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "  {"
  call  urfsegfault_emit_dec_eax
  ld    eax,[esp]
  call  urfsegfault_nfa_write_fname
  urfsegfault_printstr "}"
@@:
  pop   eax
  urfsegfault_cr

  call  urfsegfault_cmd_printdstack
  call  urfsegfault_cmd_printrstack

.skiptrace:
  cp    dword [urfsegfault_throw_error],0
  jr    nz,.throwit

.byebye:
  ld    eax,1
  ld    ebx,1
  syscall

.throwit:
  ;; restore stacks
  ld    ERP,[urfsegfault_reg_ebp]
  ld    esp,[urfsegfault_reg_esp]
  ld    TOS,[urfsegfault_reg_ecx]
  push  TOS
  ;;ld    TOS,[pfa "ERR-SEGFAULT"]
  ld    TOS,[urfsegfault_segfault_errcode_addr]
  ld    TOS,[TOS]
  ld    eax,cfa "error"
  jp    eax


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called by the startup code
;;
urforth_setup_segfault_handler:
  save_all_regs
  cld
  ld    esi,urfsegfault_segfault_signames
.sigloop:
  lodsb
  or    al,al
  jr    z,.done
  movzx ebx,al   ;; signal number
  ld    eax,67   ;; syscall number
  ld    ecx,urfd_sigact
  xor   edx,edx  ;; ignore old info
  push  esi
  syscall
  pop   esi
  or    eax,eax
  jr    z,.noerr
  urfsegfault_printstrnl "TRAP: cannot setup signal handler."
.noerr:
  lodsd
.skipmsg:
  lodsb
  or    al,al
  jr    nz,.skipmsg
  jr    .sigloop
.done:
  restore_all_regs
  ret
endcode
(hidden) (codeblock)
