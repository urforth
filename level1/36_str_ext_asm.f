;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search the string specified by c-addr1 u1 for the string specified by c-addr2 u2.
;; if flag is true, a match was found at c-addr3 with u3 characters remaining.
;; if flag is false there was no match and c-addr3 is c-addr1 and u3 is u1.
code: SEARCH  ( c-addr1 u1 c-addr2 u2 -- c-addr3 u3 flag )
  push  TOS
  pushr EIP
  mov   ebx,[esp]      ;; u2
  cp    ebx,0
  jr    z,.exitfound   ;; this is what tester wants
  jr    le,.notfound
  mov   edx,[esp+8]    ;; u1
  cp    edx,0
  jr    le,.notfound
  mov   edi,[esp+12]   ;; c-addr1
  add   edx,edi        ;; EDX is end address
.scanloop:
  mov   esi,[esp+4]    ;; c-addr2
  lodsb
  mov   ecx,edx
  sub   ecx,edi
  jr    be,.notfound
  repnz scasb
  jr    nz,.notfound   ;; no first char found
  cmp   ebx,1
  jr    z,.found       ;; our pattern is one char, and it was found
  mov   ecx,ebx
  dec   ecx
  mov   eax,edx
  sub   eax,edi
  cmp   eax,ecx
  jr    c,.notfound    ;; the rest is shorter than a pattern
  push  edi
  repz  cmpsb
  pop   edi
  jr    nz,.scanloop
.found:
  dec   edi            ;; exact match found
  sub   edx,edi
  mov   [esp+12],edi   ;; c-addr1
  mov   [esp+8],edx    ;; u1
.exitfound:
  mov   TOS,1
  jr    @f
.notfound:
  xor   TOS,TOS
@@:
  add   esp,8
  popr  EIP
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UPCASE-CHAR  ( ch -- ch )  ;; koi8
  ld    eax,TOS  ;; to avoid register dependency
  call  upcase_subr
  movzx TOS,al
  urnext
upcase_subr:
  cp    al,0x80
  jr    nc,.highascii
  cp    al,'a'
  jr    c,.done
  cp    al,'z'+1
  jr    nc,.done
  sub   al,32
.done:
  ret
.highascii:
  cp    al,0xc0
  jr    nc,.highkoi
  cp    al,0xa3  ;; yo small
  jr    nz,.done
  add   al,0x10
  ret
.highkoi:
  or    al,32
  ret
endcode

code: UPCASE-STR  ( addr count -- )
  pop   edi
  cp    TOS,0
  jr    le,.done
.convloop:
  movzx eax,byte [edi]
  ld    ah,al
  call  upcase_subr
  cp    al,ah
  jr    z,@f
  ld    byte [edi],al
@@:
  inc   edi
  dec   ecx
  jr    nz,.convloop
.done:
  pop   TOS
  urnext
endcode

code: LOCASE-CHAR  ( ch -- ch )
  ld    eax,TOS  ;; to avoid register dependency
  call  locase_subr
  movzx TOS,al
  urnext
locase_subr:
  cp    al,0x80
  jr    nc,.highascii
  cp    al,'A'
  jr    c,.done
  cp    al,'Z'+1
  jr    nc,.done
  add   al,32
.done:
  ret
.highascii:
  cp    al,0xe0
  jr    nc,.highkoi
  cp    al,0xb3  ;; yo big
  jr    nz,.done
  sub   al,0x10
  ret
.highkoi:
  sub   al,32
  ret
endcode

code: LOCASE-STR  ( addr count -- )
  pop   edi
  cp    TOS,0
  jr    le,.done
.convloop:
  movzx al,byte [edi]
  ld    ah,al
  call  locase_subr
  cp    al,ah
  jr    z,@f
  ld    byte [edi],al
@@:
  inc   edi
  dec   ecx
  jr    nz,.convloop
.done:
  pop   TOS
  urnext
endcode

;; this may be slower than the table solution, but it is smaller too
code: IS-ALPHA?  ( ch -- flag )
  ld    eax,TOS  ;; to avoid register dependency
  xor   TOS,TOS
  cp    al,0xc0
  jr    nc,.donealpha
  cp    al,0xa3
  jr    z,.donealpha
  cp    al,0xb3
  jr    z,.donealpha
  cp    al,'A'
  jr    c,.done
  cp    al,'Z'+1
  jr    c,.donealpha
  cp    al,'a'
  jr    c,.done
  cp    al,'z'+1
  jr    nc,.done
.donealpha:
  inc   TOS
.done:
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: MEMEQU  ( addr1 addr2 size -- equflag )
  jecxz .zerolen
  pop   edi
  xchg  esi,[esp]
  repz cmpsb
  pop   esi
  setz  cl
  movzx TOS,cl
  urnext
.zerolen:
  add   esp,4*2
  inc   TOS
  urnext
endcode

code: MEMEQU-CI  ( addr1 addr2 size -- equflag )
  jecxz .zerolen
  pop   edi
  xchg  esi,[esp]
.cmploop:
  lodsb
  ld    ah,byte [edi]
  inc   edi
  ;; it may work
  cp    al,ah
  jr    nz,.trycase
.caseequ:
  dec   ecx
  jr    nz,.cmploop
  ;; success
  pop   esi
  ld    TOS,1
  urnext

.trycase:
  xchg  al,ah  ;; AL:[addr2]; AH:[addr1]
  call  locase_subr
  cmp   al,ah
  jr    z,.caseequ
  xchg  al,ah  ;; AL:[addr1]; AH:[addr2]
  call  locase_subr
  cmp   al,ah
  jr    z,.caseequ
  ;; failure
  pop   esi
  xor   TOS,TOS
  urnext

.zerolen:
  add   esp,4*2
  inc   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -1, 0 or 1
code: MEMCMP  ( addr1 addr2 size -- n )
  jecxz .zerolen
  pop   edi
  xchg  esi,[esp]
  repz cmpsb
  pop   esi
  jr    z,.done
  ld    TOS,1
  jr    nc,.done
  ld    TOS,-1
.done:
  urnext
.zerolen:
  add   esp,4*2
  urnext
endcode

;; -1, 0 or 1
code: MEMCMP-CI  ( addr1 addr2 size -- n )
  jecxz .zerolen
  pop   edi
  xchg  esi,[esp]
.cmploop:
  lodsb
  ld    ah,byte [edi]
  inc   edi
  ;; it may work
  cp    al,ah
  jr    nz,.trycase
.caseequ:
  dec   ecx
  jr    nz,.cmploop
  ;; success
  pop   esi
  urnext

.trycase:
  xchg  al,ah  ;; AL:[addr2]; AH:[addr1]
  call  locase_subr
  cmp   al,ah
  jr    z,.caseequ
  xchg  al,ah  ;; AL:[addr1]; AH:[addr2]
  call  locase_subr
  cmp   al,ah
  jr    z,.caseequ
  ;; failure
  pop   esi
  ld    TOS,1
  jr    nc,.done
  ld    TOS,-1
.done:
  urnext

.zerolen:
  add   esp,4*2
  inc   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: S=  ( addr0 count0 addr1 count1 -- flag )  rot over - if drop 2drop false else memequ endif ;
: S=CI  ( addr0 count0 addr1 count1 -- flag )  rot over - if drop 2drop false else memequ-ci endif ;

: COMPARE  ( c-addr1 u1 c-addr2 u2 -- n )  rot 2dup 2>r umin memcmp ?dup ifnot 2r> swap ucmp else 2rdrop endif ;
: COMPARE-CI  ( c-addr1 u1 c-addr2 u2 -- n )  rot 2dup 2>r umin memcmp-ci ?dup ifnot 2r> swap ucmp else 2rdrop endif ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search the string specified by addr size for the char ch.
;; if flag is true, a match was found at addr1 with size1 characters remaining.
;; if flag is false there was no match and addr1 is addr and size1 is size.
;; if size1 is zero, flag is always false
code: MEMCHR  ( addr size ch -- addr1 size1 flag )
  ld    eax,TOS
  ld    TOS,[esp]
  jecxz .zerolen
  ld    edi,[esp+4]
  repnz scasb
  jr    nz,.notfound
  dec   edi
  inc   ecx
  ld    [esp],ecx
  ld    [esp+4],edi
  ld    TOS,1
  urnext
.notfound:
  xor   TOS,TOS
.zerolen:
  urnext
endcode


code: MEMCHR-CI  ( addr size ch -- addr1 size1 flag )
  ld    eax,TOS
  call  locase_subr
  ld    ah,al
  ld    TOS,[esp]
  jecxz .zerolen
  ld    edi,[esp+4]
.cmploop:
  ld    al,byte [edi]
  cp    al,ah
  jr    z,.found
  call  locase_subr
  cp    al,ah
  jr    z,.found
  inc   edi
  dec   ecx
  jr    nz,.cmploop
  ;; not found
.zerolen:
  urnext
.found:
  ld    [esp],TOS
  ld    [esp+4],edi
  ld    TOS,1
  urnext
endcode
