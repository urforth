;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; patches word size to HERE
;; call this after creating various arrays
;; because compiler should know their size
: create;  ( -- )
  latest-lfa dp-temp-addr? dp-temp @ ifnot not endif
  if here latest-nfa nfa->sfa ! endif  ;; patch size
;


256 buffer: (create-header-buf)
(hidden)


;; common part, used to build word names
: (create-build-name)  ( addr count -- )
  here >r  ;; remember HERE (we will need to fix some name fields later)
  ;; compile counter (we'll add flags and other things later)
  ;; it is guaranteed that all fields except length are zero here
  ;; (due to word header layout, and length check in callers)
  dup ,
  ;; copy parsed word to HERE (and allocate name bytes for it)
  dup if
    dup n-allot swap move
    ;; change the case of a new name?
    create-case? @ ?dup if r@ count rot +if upcase-str else locase-str endif endif
    ;; replace starting zero with space
    r@ 4+ c@ ifnot bl r@ 4+ c! endif
  else
    2drop
  endif
  (wflag-smudge) (current-create-mode) or  ;; put flags (ffa is 16 bits at nfa+2)
  r@ nfa->ffa tfa!     ;; it is safe to poke here, ffa is empty
  $if URFORTH_ALIGN_CFA_PFA
    0 c, r> c@ begin here 3 and while 1+ 0 c, repeat
    dup 255 u> err-aligned-name-too-long ?error
    here 1- c!
  $else
    r> c@ c,  ;; put length again (trailing length byte)
  $endif
; (hidden)


: (create-temp-name?)  ( addr count -- flag )
  +if c@ 0= else drop true endif
; (hidden)

: (create-check-dp-temp)  ( addr count -- addr count )
  dp-temp @ if 2dup (create-temp-name?) ifnot
    current @ dp-temp-addr? err-no-temp-here not-?error
  endif endif
;

;; this is used to create a header for the temporary codeblock in dp-temp
;; we need this so standard diagnostic words won't be confused
;; it won't change "latest", though
: (create-temp-header-nocheck)  ( addr count -- )
  0 max-word-name-length clamp 255 min  ;; sanitize length, just in case
  $if URFORTH_ALIGN_HEADERS
  align-here
  $endif
  0 ,  ;; allocate dfa
  0 ,  ;; allocate sfa
  0 ,  ;; allocate bfa
  0 ,  ;; allocate lfa
  0 ,  ;; namehash
  (create-build-name)
; (hidden)


;; this can be used to create nameless words (with 0 name length)
;; nameless words are linked with LFA, but never put into any hash bucket
;; they also have zero namehash
: (create-header-nocheck)  ( addr count -- )
  (create-check-dp-temp)
  0 max-word-name-length clamp 255 min  ;; sanitize length, just in case
  dup >r (create-header-buf) swap move (create-header-buf) r>
  $if URFORTH_ALIGN_HEADERS
  align-here
  $endif
  0 ,  ;; allocate dfa
  0 ,  ;; allocate sfa
  0 ,  ;; allocate bfa (it will be patched later)
  here ;; remember HERE (it will become the new latest)
  latest-lfa , ;; put lfa
  current @ !  ;; update latest
  ;; zero-length name, or name starts with zero byte means "do not register in hash table"
  dup if over c@ else false endif
  if  ;; ok, put into hash table
    2dup str-name-hash dup , ;; put name hash
    ;; fix bfa
    $if WLIST_HASH_BITS
      ( addr count hash )
      ;; check if vocab has hashtable
      current @ vocid-hashed? if
        name-hash-fold-mask current @ vocid->htable cells^  ;; calc bucket address
        dup @  ;; load old bfa link  ( addr count bkptr oldbfa )
        here nfa->bfa rot !  ;; store current bfa address
        here nfa->bfa !      ;; update bfa
      else drop endif
    $else
      drop  ;; we don't really need any hash
    $endif
  else
    0 ,  ;; namehash
  endif
  (create-build-name)
  create;  ;; setup initial size, why not
; (hidden)


;; A defining word used in the form: CREATE cccc
;; by such words as CODE and CONSTANT to create a dictionary header for
;; a Forth definition. Puts no code field.
;; The new word is created in the current vocabulary.
;; Note that SMUDGE bit is set (i.e. the word is invisible).
: (create-header)  ( addr count -- )
  (create-check-dp-temp)
  ;; check for duplicate word?
  warning-redefine @ if
    ;; look only in the current dictionary
    2dup current @ voc-find-str if
      ?endcr if space endif cfa->nfa ." \`" id. ." \` redefined"
      (tib-fname>error-fname) error-line. cr
    endif
  endif
  ( addr count )
  ;; check length
  dup 1 max-word-name-length bounds? ifnot
    ?endcr if space endif ." \`" type ." \` " err-invalid-word-name error
  endif
  (create-header-nocheck)
; (hidden)


: create-header-named  ( addr count -- )
  (create-header)
;

: create-header  ( -- )  \ word
  parse-name create-header-named
;

;; the same as "CREATE", but without parsing
: create-named  ( addr count -- )
  create-header-named (URFORTH-DOVAR-ADDR) compiler:(cfa-call,) create; smudge
;

: create-named-in  ( addr count vocid -- )
  get-current >r set-current ['] create-named catch r> set-current throw
;

;; creates new word definition.
;; the code field contains the routine that returns the address of the word's parameter field.
;; the new word is created in the current vocabulary.
: create  ( -- )  \ word
  parse-name create-named
;

: create-in  ( vocid -- )  \ word
  parse-name rot create-named-in
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sadly, has to be here, because it is used by word searching code
enum{
  value WORD-TYPE-OTHER
  value WORD-TYPE-FORTH
  value WORD-TYPE-VAR
  value WORD-TYPE-CONST
  value WORD-TYPE-VALUE
  value WORD-TYPE-DEFER
  value WORD-TYPE-DOES
  value WORD-TYPE-VOC
  ;; this word is new override (overriden words are not marked as special, yet they become non-Forth, i.e. "other")
  value WORD-TYPE-OVERRIDE
  value WORD-TYPE-OVERRIDEN
}

create (WORD-CFA-TABLE) (hidden)
  dd  ur_DOFORTH,pfa "WORD-TYPE-FORTH"
  dd  ur_DOCONST,pfa "WORD-TYPE-CONST"
  dd  ur_DOVAR,pfa "WORD-TYPE-VAR"
  dd  ur_DOVALUE,pfa "WORD-TYPE-VALUE"
  dd  ur_DODEFER,pfa "WORD-TYPE-DEFER"
  dd  ur_DODOES,pfa "WORD-TYPE-DOES"
  dd  ur_DOOVERRIDE,pfa "WORD-TYPE-OVERRIDE"
  dd  0
create;

: WORD-TYPE?  ( cfa -- type )
  dup c@ 0xe8 = if
    ;; check "vocabulary" flag
    dup cfa-vocab? if drop word-type-voc exit endif
    ;; calculate call address
    1+ compiler:(disp32@)
    ;; check for overriden word
    dup (code-base-addr) u> over real-here u< and if
      ;; it should be safe to peek
      dup c@ 0xe8 = if
        dup 1+ compiler:(disp32@)
        (URFORTH-DOOVERRIDE-ADDR) = if drop word-type-overriden exit endif
      endif
    endif
    ;; check table
    >r (word-cfa-table)
    begin dup @ ?dup while  ( tbladdr codeaddr | cfadest )
      r@ = if rdrop cell+ @ @ exit endif
    2 +cells repeat
    ( tbladdr | cfadest )
    rdrop
  endif
  drop
  word-type-other
;
