;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$uservar "(HLD)" ua_ofs_hld 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: HEX     ( -- )  16 base ! ;
: DECIMAL ( -- )  10 base ! ;
: OCTAL   ( -- )  8 base ! ;
: BINARY  ( -- )  2 base ! ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (HLD-INIT-ADDR)  ( -- addr )  pad 1-  ;

: HOLD  ( ch -- )
  (hld) @ 1- dup (hld) ! c!
;

;; prepend the string
: HOLDS  ( addr count -- )
  begin dup +while 1- 2dup + c@ hold repeat 2drop
;

: SIGN  ( n -- )
  -if [char] - hold endif
;

: <#  ( d -- d )
  (hld-init-addr) (hld) !
;

: <#U  ( n -- d )
  0 <#
;

: #>  ( d -- addr count )
  2drop (hld) @ (hld-init-addr) over -
;

: #  ( d -- n )
  base @ uds/mod dup 9 > if 7 + endif
  48 + hold
;

: #S  ( d -- d )
  begin # 2dup or not-until
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (U.)  ( u -- addr count )  <#u #s #> ;
: (.)  ( n -- addr count )  dup abs <#u #s rot sign #> ;

: (x.r)  ( fldlen addr count -- )  rot over - spaces type ;

: U.R  ( u fldlen -- )  swap (u.) (x.r) ;
: .R  ( n fldlen -- )  swap (.) (x.r) ;

: U.  ( u -- )  (u.) type space ;
: .  ( n -- )  (.) type space ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (UD.)  ( ud -- addr count )  <# #s #> ;
: (D.)  ( d -- addr count )  dup nrot dabs <# #s rot sign #> ;

: UD.R  ( ud fldlen -- )  nrot (ud.) (x.r) ;
: D.R  ( d fldlen -- )  nrot (d.) (x.r) ;

: UD.  ( ud -- ) (ud.) type space ;
: D.  ( ud -- ) (d.) type space ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 8 bytes
$uservar "(hex-print-buf)" ua_ofs_hexprbuf 0
(hidden)
$userallot 4

;; uses (hex-print-buf)
code: (.HEX8)  ( u -- addr count )
  lea   edi,[ua_ofs_hexprbuf+8]
  ld    ebx,TOS
  ld    ecx,8    ;; digits
.digloop:
  ld    eax,ebx  ;; to break register dependency
  Nibble2Hex
  dec   edi
  ld    ts:[edi],al
  shr   ebx,4
  dec   ecx
  jr    nz,.digloop
 $if URFORTH_TLS_TYPE = URFORTH_TLS_TYPE_FS
  ;; convert from TLS to real address
  add   edi,ts:[ua_ofs_baseaddr]
 $endif
  push  edi
  ld    TOS,8
  urnext
endcode

: .HEX8  ( u -- )  (.hex8) type ;

: (.HEX2)  ( u -- addr count )  (.hex8) drop 6 + 2 ;
: .HEX2  ( u -- )  (.hex2) type ;

: (.HEX4)  ( u -- addr count )  (.hex8) drop 4 + 4 ;
: .HEX4  ( u -- )  (.hex4) type ;
