;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: STR-HASH-ELF  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash <<= 4
  shl   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  add   eax,edx
  ;; high = hash&0xF0000000
  ld    edx,eax
  and   edx,0xf0000000
  ld    ebx,edx
  inc   edi
  ;; hash ^= high>>24
  rol   edx,8
  xor   eax,edx
  ;; hash &= ~high
  not   ebx
  and   eax,ebx
  dec   ecx
  jr    nz,.hashloop
  ;;loop  .hashloop
.done:
  ld    TOS,eax
  urnext
endcode


code: STR-HASH-JOAAT  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash += *key
  movzx edx,byte [edi]
  add   eax,edx
  ;; hash += hash<<10
  ld    edx,eax
  shl   eax,10
  add   eax,edx
  inc   edi
  ;; hash ^= hash>>6
  ld    edx,eax
  shr   eax,6
  xor   eax,edx
  dec   ecx
  jr    nz,.hashloop
  ;;loop  .hashloop
  ;; final permutation
  ;; hash += hash<<3
  ld    edx,eax
  shl   eax,3
  add   eax,edx
  ;; hash ^= hash>>11
  ld    edx,eax
  shr   eax,11
  xor   eax,edx
  ;; hash += hash<<15
  ld    edx,eax
  shl   eax,15
  add   eax,edx
.done:
  ld    TOS,eax
  urnext
endcode


code: STR-HASH-ROT  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash = lrot(hash, 4)
  rol   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  inc   edi
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  xor   eax,edx
  dec   ecx
  jr    nz,.hashloop
  ;; this mixing allows using power-of-two tables with masking
  ;; hash ^= (hash>>10)^(hash>>20)
  ld    ecx,eax
  shr   ecx,10
  ld    edx,eax
  shr   edx,20
  xor   eax,ecx
  xor   eax,edx
.done:
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tests
;; ;; 0 [IF]
;; ;; \ ' STR-HASH-DSFORTH disasm-word
;; ;; .( testing hashing functions...\n)
;; ;;
;; ;; : (str-hash-nassert) ( v0 v1 -- )
;; ;;   2dup - if
;; ;;     ." assertion failed!\n"
;; ;;     ." EXPECTED: $" .hex8 cr
;; ;;     ."      GOT: $" .hex8 cr
;; ;;     1 bye
;; ;;   endif
;; ;;   2drop
;; ;; ;
;; ;;
;; ;; s" a" str-hash-dsforth  $00000061 (str-hash-nassert)
;; ;; s" ab" str-hash-dsforth $00000024 (str-hash-nassert)
;; ;; s" The quick brown fox jumps over the lazy dog" str-hash-dsforth  $00000063 (str-hash-nassert)
;; ;;
;; ;; s" a" str-hash-elf  $00000061 (str-hash-nassert)
;; ;; s" ab" str-hash-elf $00000672 (str-hash-nassert)
;; ;; s" The quick brown fox jumps over the lazy dog" str-hash-elf  $04280C57 (str-hash-nassert)
;; ;;
;; ;; s" a" str-hash-joaat  0xca2e9442 (str-hash-nassert)
;; ;; s" ab" str-hash-joaat $45E61E58 (str-hash-nassert)
;; ;; s" The quick brown fox jumps over the lazy dog" str-hash-joaat  0x519e91f5 (str-hash-nassert)
;; ;;
;; ;; .( done testing hashing functions...\n)
;; ;; [ENDIF]
