;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word header format:
;; note than name hash is ALWAYS calculated with ASCII-uppercased name
;; (actually, bit 5 is always reset for all bytes, because we don't need the
;; exact uppercase, only something that resembles it)
;; bfa points to next bfa or to 0 (this is "hash bucket pointer")
;; before nfa, we have such "hidden" fields:
;;   dd dfa       ; pointer to the debug data; can be 0 if debug info is missing
;;   dd sfa       ; points *after* the last word byte
;;   dd bfa       ; next word in hashtable bucket; it is always here, even if hashtable is turned off
;;                ; if there is no hashtable, this field is not used
;; lfa:
;;   dd lfa       ; previous word LFA or 0 (lfa links points here)
;;   dd namehash  ; it is always here, and always calculated, even if hashtable is turned off
;; nfa:
;;   dd flags-and-name-len   ; see below
;;   db name      ; no terminating zero or other "termination flag" here
;;   db namelen   ; yes, name length again, so CFA->NFA can avoid guessing
;;   machine code follows
;;   here we usually have CALL to word handler
;;   0xE8, 4-byte displacement
;;   (displacement is calculated from here)
;;
;; first word cell contains combined name length (low byte), argtype and flags (other bytes)
;; layout:
;;   db namelen
;;   db argtype
;;   dw flags
;;
;; i.e. we have 16 bits for flags, and 256 possible argument types. why not.
;;
;; flags:
;;  bit 0: immediate
;;  bit 1: smudge
;;  bit 2: noreturn
;;  bit 3: hidden
;;  bit 4: codeblock
;;  bit 5: vocabulary
;;  bit 6: main scattered colon word (with "...")
;;  bit 7: macro (it *may* be inlined)
;;
;;  argtype is the type of the argument that this word reads from the threaded code.
;;  possible argument types:
;;    0: none
;;    1: branch address
;;    2: cell-size numeric literal
;;    3: cell-counted string with terminating zero (not counted)
;;    4: cfa of another word
;;    5: cblock
;;    6: vocid
;;    7: byte-counted string with terminating zero (not counted)
;;    8: unsigned byte
;;    9: signed byte
;;   10: unsigned word
;;   11: signed word


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist structure
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;;   dd header-nfa (can be 0 for anonymous wordlists)
;;   hashtable (if enabled)

$constant "(VOC-HEADER-SIZE-CELLS)" 4
(hidden)

$constant "(WFLAG-IMMEDIATE)" FLAG_IMMEDIATE
$constant "(WFLAG-SMUDGE)"    FLAG_SMUDGE
$constant "(WFLAG-NORETURN)"  FLAG_NORETURN
$constant "(WFLAG-HIDDEN)"    FLAG_HIDDEN
$constant "(WFLAG-CODEBLOCK)" FLAG_CODEBLOCK
$constant "(WFLAG-VOCAB)"     FLAG_VOCAB
$constant "(WFLAG-SCOLON)"    FLAG_SCOLON
$constant "(WFLAG-MACRO)"     FLAG_MACRO

$constant "(WARG-NONE)"    WARG_NONE
$constant "(WARG-BRANCH)"  WARG_BRANCH
$constant "(WARG-LIT)"     WARG_LIT
$constant "(WARG-C4STRZ)"  WARG_C4STRZ
$constant "(WARG-CFA)"     WARG_CFA
$constant "(WARG-CBLOCK)"  WARG_CBLOCK
$constant "(WARG-VOCID)"   WARG_VOCID
$constant "(WARG-C1STRZ)"  WARG_C1STRZ
$constant "(WARG-U8)"      WARG_U8
$constant "(WARG-S8)"      WARG_S8
$constant "(WARG-U16)"     WARG_U16
$constant "(WARG-S16)"     WARG_S16


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word searching
;; CURRENT:
;;   searched first; also, new words will go here
;;
;; CONTEXT:
;;   stack of vocabularies, searched after the CURRENT
;;
URFORTH_VOCSTACK_SIZE equ 16
$constant "VOC-STACK-SIZE" URFORTH_VOCSTACK_SIZE
$constant "MAX-WORD-NAME-LENGTH" URFORTH_MAX_WORD_NAME_LENGTH

;; this will be bitored with flags in all "create" words
$value "(CURRENT-CREATE-MODE)" 0
(hidden)

$variable "WARNING-REDEFINE" 1
;; 0: don't change case; 1: upcase; -1: locase
$variable "CREATE-CASE?" 0

;; vocabulary stack
create (VOCAB-STACK) (hidden)
  ;; this indicates end of the stack (should always be there, it is used as a sentinel)
  dd  0
  ;; this one is always here, it is so-called "root" vocabulary (last resort)
  ;; it is usually system FORTH vocabulary
  ;; CONTEXT may point here
$label forth_voc_stack_root
  dd  forth_wordlist_vocid
  ;; replaceable stack begins here
$label forth_voc_stack
  dd  forth_wordlist_vocid
  rd  URFORTH_VOCSTACK_SIZE-1
$label forth_voc_stack_end
create;

$constant "(FORTH-VOC-STACK)" forth_voc_stack
(hidden)
$constant "(FORTH-VOC-STACK-END)" forth_voc_stack_end
(hidden)

;; target compiler already has it, so let's use it too
$value "CURRENT" image_current_var_addr
;; this actually points to the last pushed vocabulary
$value "CONTEXT" forth_voc_stack  ;;pfa "(vocab-stack)"+8


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CFA->something
;;
code: CFA->PFA  ( cfa -- pfa )
  add   TOS,URFORTH_CFA_SIZE  ;; skip call
  urnext
endcode

code: CFA->NFA  ( cfa -- nfa )
  movzx eax,byte [TOS-1]
  add   eax,1+4      ;; trailing length, lenflags
  sub   TOS,eax
  urnext
endcode

code: CFA->FFA  ( cfa -- nfa )
  movzx eax,byte [TOS-1]
  add   eax,1+2      ;; trailing length, lenargtype
  sub   TOS,eax
  urnext
endcode

code: CFA->LFA  ( cfa -- lfa )
  movzx eax,byte [TOS-1]
  add   eax,1+4+4+4  ;; trailing length, lenflags, namehash, lfa
  sub   TOS,eax
  urnext
  sub   TOS,4
  urnext
endcode

;; calculate word size from cfa
code: CFA-WSIZE  ( cfa -- size )
  ;; move to sfa
  movzx eax,byte [TOS-1]
  add   eax,1+4+16    ;; trailing length, lenflags, offset to sfa
  ld    edx,TOS
  sub   TOS,eax
  ld    TOS,[TOS]
  sub   TOS,edx
  urnext
endcode

;; calculate pfa size (from word header)
: PFA-SIZE  ( pfa -- size )
  pfa->cfa cfa-wsize (#cfa) - 0 max
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PFA->something
;;
code: PFA->CFA  ( pfa -- cfa )
  sub   TOS,URFORTH_CFA_SIZE  ;; undo skip call
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NFA->something (and NFA utils)
;;
code: NFA-COUNT  ( nfa -- addr count )
  movzx eax,byte [TOS]
  add   TOS,4  ;; leadlenflags
  push  TOS
  ld    TOS,eax
  urnext
endcode

code: NFA->CFA  ( nfa -- cfa )
  movzx eax,byte [TOS]
  $if URFORTH_ALIGN_CFA
  add   eax,4
  or    eax,3
  inc   eax
  $else
  add   eax,1+4      ;; trailing length, lenflags
  $endif
  add   TOS,eax
  urnext
endcode

;; to debugptr field
code: NFA->DFA  ( nfa -- dfa )
  sub   TOS,20
  urnext
endcode

;; to word size field
code: NFA->SFA  ( nfa -- sfa )
  sub   TOS,16
  urnext
endcode

;; to bucketptr field
code: NFA->BFA  ( nfa -- bfa )
  sub   TOS,12
  urnext
endcode

;; to word size field
code: NFA->LFA  ( nfa -- sfa )
  sub   TOS,8
  urnext
endcode

;; to name hash field
code: NFA->HFA  ( nfa -- hfa )
  sub   TOS,4
  urnext
endcode

;; flag fields area; 2 bytes
code: NFA->FFA  ( nfa -- ffa )
  add   TOS,2
  urnext
endcode

;; argument type area; 1 byte
code: NFA->TFA  ( nfa -- tfa )
  inc   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LFA->something
;;
code: LFA->CFA  ( lfa -- cfa )
  movzx eax,byte [TOS+8]
  $if URFORTH_ALIGN_CFA
  add   eax,1+4+4+3  ;; trailing length, lenflags, namehash, lfa
  or    eax,3
  inc   eax
  $else
  add   eax,1+4+4+4  ;; trailing length, lenflags, namehash, lfa
  $endif
  add   TOS,eax
  urnext
endcode

code: LFA->NFA  ( lfa -- nfa )
  add   TOS,8
  urnext
endcode

code: LFA->HFA  ( lfa -- hfa )
  add   TOS,4
  urnext
endcode

code: LFA->FFA  ( lfa -- ffa )
  add   TOS,4+4+2
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BFA->something
;;
code: BFA->NFA  ( bfa -- nfa )
  add   TOS,4+4+4
  urnext
endcode

code: BFA->LFA  ( bfa -- lfa )
  add   TOS,4
  urnext
endcode

code: BFA->HFA  ( bfa -- hfa )
  add   TOS,4+4
  urnext
endcode

code: BFA->FFA  ( bfa -- ffa )
  add   TOS,4+4+4+2
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HFA->something
;;
code: HFA->BFA  ( bfa -- hfa )
  sub   TOS,4+4
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: FFA@  ( ffa -- ffa-value )
  movzx TOS,word [TOS]
  urnext
endcode

code: TFA@  ( tfa -- tfa-value )
  movzx TOS,byte [TOS]
  urnext
endcode

code: FFA!  ( ffa-value ffa -- )
  pop   eax
  ld    word [TOS],ax
  pop   TOS
  urnext
endcode

code: TFA!  ( tfa-value tfa -- )
  pop   eax
  ld    byte [TOS],al
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: LATEST-LFA  ( -- lfa )  current @ @ ;
: LATEST-NFA  ( -- nfa )  latest-lfa lfa->nfa ;
: LATEST-CFA  ( -- cfa )  latest-lfa lfa->cfa ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (cfa-check-flag)  ( cfa flag -- bool-set )  swap cfa->ffa ffa@ swap and notnot ; (hidden)

: CFA-HIDDEN?  ( cfa -- flag )  (wflag-hidden) (cfa-check-flag) ;
: CFA-VOCAB?  ( cfa -- flag )  (wflag-vocab) (cfa-check-flag) ;
: CFA-MACRO?  ( cfa -- flag )  (wflag-macro) (cfa-check-flag) ;
: CFA-IMMEDIATE?  ( cfa -- flag )  (wflag-immediate) (cfa-check-flag) ;
: CFA-SCOLON?  ( cfa -- flag )  (wflag-scolon) (cfa-check-flag) ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (toggle-latest-wflag)  ( flag -- )  latest-nfa nfa->ffa swap wtoggle ; (hidden)
: (reset-latest-wflag)  ( flag -- )  latest-nfa nfa->ffa dup w@ ( flag addr oldflg ) rot ~and swap w! ; (hidden)
: (set-latest-wflag)  ( flag -- )  latest-nfa nfa->ffa dup w@ ( flag addr oldflg ) rot or swap w! ; (hidden)

: (vocid-toggle-latest-wflag)  ( vocid flag -- )  swap vocid-latest-nfa nfa->ffa swap wtoggle ; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (SET-SCOLON)  ( -- ) (wflag-scolon) (set-latest-wflag) ; (hidden)

: SMUDGE     ( -- )  (wflag-smudge) (toggle-latest-wflag) ;
: IMMEDIATE  ( -- )  (wflag-immediate) (toggle-latest-wflag) ;

: VOCID-SMUDGE     ( vocid -- )  (wflag-smudge) (vocid-toggle-latest-wflag) ;
: VOCID-IMMEDIATE  ( vocid -- )  (wflag-immediate) (vocid-toggle-latest-wflag) ;

: SET-SMUDGE     ( -- )  (wflag-smudge) (set-latest-wflag) ;
: SET-IMMEDIATE  ( -- )  (wflag-immediate) (set-latest-wflag) ;
: RESET-SMUDGE     ( -- )  (wflag-smudge) (reset-latest-wflag) ;
: RESET-IMMEDIATE  ( -- )  (wflag-immediate) (reset-latest-wflag) ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (HIDDEN)     ( -- )  (WFLAG-HIDDEN) (set-latest-wflag) ;
: (PUBLIC)     ( -- )  (WFLAG-HIDDEN) (reset-latest-wflag) ;
: (NORETURN)   ( -- )  (WFLAG-NORETURN) (toggle-latest-wflag) ;
: (CODEBLOCK)  ( -- )  (WFLAG-CODEBLOCK) (set-latest-wflag) ;

: (SET-MACRO)      ( -- )  (WFLAG-HIDDEN) (set-latest-wflag) ; (hidden)
: (RESET-MACRO)    ( -- )  (WFLAG-HIDDEN) (reset-latest-wflag) ; (hidden)
: (LATEST-MACRO?)  ( -- flag )  latest-nfa nfa->ffa ffa@ (wflag-macro) and notnot ; (hidden)

: (ARG-NONE)    ( -- )  (WARG-NONE) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-BRANCH)  ( -- )  (WARG-BRANCH) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-LIT)     ( -- )  (WARG-LIT) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-C4STRZ)  ( -- )  (WARG-C4STRZ) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-CFA)     ( -- )  (WARG-CFA) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-CBLOCK)  ( -- )  (WARG-CBLOCK) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-VOCID)   ( -- )  (WARG-VOCID) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-C1STRZ)  ( -- )  (WARG-C1STRZ) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-U8)      ( -- )  (WARG-U8) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-S8)      ( -- )  (WARG-S8) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-U16)     ( -- )  (WARG-U16) latest-nfa nfa->tfa tfa! ; (hidden)
: (ARG-S16)     ( -- )  (WARG-S16) latest-nfa nfa->tfa tfa! ; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (set-context)  ( vocid -- )
  ;; puts this one to the context stack, replacing the top one
  context (forth-voc-stack) u< if  ;; we don't have any vocabularies in the stack, push one
    (forth-voc-stack) to context
  endif
  ;; replace top context vocabulary
  context !
; (hidden)

: get-current  ( -- vocid )  current @ ;
: set-current  ( vocid -- )  current ! ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: id-count  ( nfa -- nameaddr namelen )  dup c@ swap cell+ swap ;
: id.  ( nfa -- )  id-count safe-type ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: voc-cfa->vocid  ( cfa -- vocid )  cfa->pfa cell+ @ ;

: vocid-latest-lfa  ( vocid -- latest-lfa )  @ ;
: vocid-latest-nfa  ( vocid -- latest-nfa )  vocid-latest-lfa lfa->nfa ;
: vocid-latest-cfa  ( vocid -- latest-cfa )  vocid-latest-lfa lfa->cfa ;

: vocid->voclink  ( vocid -- voclincaddr )  cell+ ;
: vocid->parent  ( vocid -- parentaddr )  2 +cells ;
: vocid-parent!  ( parentvocid vocid -- )  vocid->parent ! ;
: vocid-parent@  ( parentvocid vocid -- )  vocid->parent @ ;
: vocid->headnfa  ( vocid -- headernfaaddr )  3 +cells ;
;; returns 0 for anonymous vocabs
: vocid-headnfa@  ( vocid -- headernfaaddr//0 )  dup if vocid->headnfa @ endif ;
: vocid-headnfa!  ( headernfaaddr vocid -- )  vocid->headnfa ! ;
: vocid->htable  ( vocid -- hashtableaddr )  4 +cells ;

: vocid-hashed?  ( vocid -- flag )
 $if WLIST_HASH_BITS
  vocid->htable @ 1+ notnot
 $else
  drop false
 $endif
;

: vocid-name  ( vocid -- addr count )  vocid-headnfa@ ?dup if id-count else here 0 ( dummy) endif ;
: vocid.  ( vocid -- )  vocid-name ?dup ifnot drop " (anonymous)" endif type ;
