;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; we have asm, so i see no reason to keep those
$if 0
code: (SYSCALL-0)  ( num -- res )
  ld    eax,TOS
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-1)  ( arg0 num -- res )
  ld    eax,TOS
  pop   ebx
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-2)  ( arg0 arg1 num -- res )
  ld    eax,TOS
  pop   ecx
  pop   ebx
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-3)  ( arg0 arg1 arg2 num -- res )
  ld    eax,TOS
  pop   edx
  pop   ecx
  pop   ebx
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-4)  ( arg0 arg1 arg2 arg3 num -- res )
  ;; this cannot be called recursively anyway
  ld    [fword_syscall4_eip_store],EIP
  ld    eax,TOS
  pop   esi
  pop   edx
  pop   ecx
  pop   ebx
  syscall
  ld    EIP,[fword_syscall4_eip_store]
  ld    TOS,eax
  urnext
fword_syscall4_eip_store: dd 0
endcode

code: (SYSCALL-5)  ( arg0 arg1 arg2 arg3 arg4 num -- res )
  ;; this cannot be called recursively anyway
  ld    [fword_syscall5_eip_store],EIP
  ld    eax,TOS
  pop   edi
  pop   esi
  pop   edx
  pop   ecx
  pop   ebx
  syscall
  ld    EIP,[fword_syscall5_eip_store]
  ld    TOS,eax
  urnext
fword_syscall5_eip_store: dd 0
endcode
$endif
