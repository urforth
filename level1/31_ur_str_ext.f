;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: STR-CHAR-INDEX  ( addr count char -- index true // false )
  pop   eax
  pop   edi
  xchg  eax,TOS
  ;; EAX: char
  ;; TOS: count
  ;; EDI: addr
  cp    TOS,1
  jr    l,@f
  lea   edx,[edi+1]  ;; to calc index
  repne scasb
  jr    nz,@f
  sub   edi,edx
  push  edi
  ld    TOS,1
  urnext
@@:
  xor   TOS,TOS
  urnext
endcode

code: STR-LAST-CHAR-INDEX  ( addr count char -- index true // false )
  pop   eax
  pop   edi
  xchg  eax,TOS
  ;; EAX: char
  ;; TOS: count
  ;; EDI: addr
  cp    TOS,1
  jr    l,@f
  lea   edx,[edi-1]  ;; to calc index
  lea   edi,[edi+TOS-1]
  std
  repne scasb
  cld
  jr    nz,@f
  sub   edi,edx
  push  edi
  ld    TOS,1
  urnext
@@:
  xor   TOS,TOS
  urnext
endcode


;; `count` will be 0 if there is no char
;; `count` will include char
;; `addr` is not changed in any case
;; this trims string after the last occurence of `char`
code: STR-TRIM-AFTER-LAST-CHAR  ( addr count char -- addr count )
  pop   eax
  xchg  eax,TOS
  ld    edi,[esp]
  cp    TOS,1
  jr    l,@f
  lea   edi,[edi+TOS-1]
  std
  repne scasb
  cld
  jr    nz,@f
  inc   TOS
  urnext
@@:
  xor   TOS,TOS
  urnext
endcode


;; `count` will be 0 if there is no char
;; `count` will not include a colon
;; `addr` is not changed in any case
;; this trims string after the first occurence of `char`
code: STR-TRIM-AFTER-CHAR  ( addr count char -- addr count )
  pop   eax
  xchg  eax,TOS
  ld    edi,[esp]
  cp    TOS,1
  jr    l,@f
  ld    edx,TOS  ;; to calculate the final length
  repne scasb
  jr    nz,@f
  sub   edx,TOS
  ld    TOS,edx
  urnext
@@:
  xor   TOS,TOS
  urnext
endcode


;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AFTER-LAST-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-after-last-char
  nip dup >r
  - swap r> + swap
;

;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AFTER-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-after-char
  nip dup >r
  - swap r> + swap
;


;; `count` will be 0 if there is no char
;; `count` will not include char
;; `addr` is not changed in any case
: STR-TRIM-AT-LAST-CHAR  ( addr count char -- addr count )
  str-trim-after-last-char dup if 1- endif
;

;; `count` will be 0 if there is no char
;; `count` will not include char
;; `addr` is not changed in any case
: STR-TRIM-AT-CHAR  ( addr count char -- addr count )
  str-trim-after-char dup if 1- endif
;


;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AT-LAST-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-at-last-char
  nip dup >r
  - swap r> + swap
;

;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AT-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-at-char
  nip dup >r
  - swap r> + swap
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; count can be 0 if no path there
;; trailing '/' is included
: STR-EXTRACT-PATH  ( addr count -- addr count )
  [char] / str-trim-after-last-char
;

;; count can be 0 if no name there (and `addr` is not changed)
: STR-EXTRACT-NAME  ( addr count -- addr count )
  [char] / str-skip-after-last-char
;

;; count can be 0 if no extension there (and `addr` is not changed)
;; dot is included
: STR-EXTRACT-EXT  ( addr count -- addr count )
  2dup str-extract-name ?dup
  ifnot  ( addr count dummyaddr )
    2drop 0 exit
  endif
  ( addr count naddr ncount )
  [char] . str-skip-at-last-char ?dup
  ifnot  ( addr count dummyaddr )
    2drop 0
  else  ( addr count extaddr extcount )
    2swap 2drop
  endif
;

;; count can be 0 if no base name there (and `addr` is not changed)
: STR-EXTRACT-BASE-NAME  ( addr count -- addr count )
  ;; get base name
  2dup str-extract-name ?dup
  ifnot 2drop 0 exit endif  ;; no file name, nothing to extract
  ;; trim at extension
  ( addr count nameaddr namecount )
  2dup [char] . str-trim-at-last-char
  ( addr count nameaddr namecount bnaddr bncount )
  ?dup ifnot  ( addr count nameaddr namecount dummyaddr )
    drop 2>r 2drop 2r> exit
  endif
  ( addr count nameaddr namecount bnaddr bncount )
  2>r 2drop 2drop 2r>
;
