;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$constant "WLIST-HASH-BITS" WLIST_HASH_BITS
$constant "ALIGN-WORD-HEADERS?" URFORTH_ALIGN_HEADERS

$constant "URFORTH-LEVEL" 1

;; bit 15 is "beta" flag
;; this gives us versions up to 255.255.32767
$constant "URFORTH-VERSION" 0x00_01_0001

$constant "URFORTH-OS" URFORTH_OS

$constant "URFORTH-OS-LINUX/X86" URFORTH_LINUX_X86
$constant "URFORTH-OS-WIN32" URFORTH_WIN32

;; should segfault handler throw an error instead of exiting?
$value "(TRAP-THROW)" urfsegfault_throw_error
(hidden)

;; print stack trace before doing an action?
$value "(TRAP-STACKTRACE)" urfsegfault_stacktrace
(hidden)

;; is multithreading active?
;; this will be set to `true` if at least one thread was created
;; this will never be reset (except in SAVEd images)
$value "(MT-ACTIVE?)" 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; user area size is 4KB (hardcoded)

;; user area
$align 16,0
$label ua_default_values
  rb  URFORTH_MAX_USERAREA_SIZE

$if URFORTH_TLS_TYPE
;; will be patched by the startup/thread creation code
;; it should always be at zero offset
$uservar "(USER-BASE-ADDR)" ua_ofs_baseaddr 0
(hidden)

;; this will be set by thread creation code
;; this is the full size of tls+stacks allocation, that can be used in OS:MUNMAP
$uservar "(USER-FULL-SIZE)" ua_ofs_fullsize 0
(hidden)
$endif

;; patched by the startup code
$uservar "(MAIN-THREAD?)" ua_ofs_is_main_thread 0
(hidden)

$uservar "SP0"  ua_ofs_sp0 0
$uservar "RP0"  ua_ofs_rp0 0
;; default size for thread stacks (note: main thread has different-sized stacks)
;; with those sizes, it rougly fits into one 4KB page (including userarea)
$uservar "#SP"  ua_ofs_spsize 500
$uservar "#RP"  ua_ofs_rpsize 240

$uservar "BASE" ua_ofs_base 10

;; this can be used in various OOP implementations
;; the kernel doesn't use it
;; the reason it is there is because THROW will save/restore it
$uservar "(SELF)" ua_ofs_self 0
(hidden)

;; this can be used in various locals implementations
;; the kernel doesn't use it (yet)
;; the reason it is there is because THROW will save/restore it
$uservar "(LOCPTR)" ua_ofs_locptr 0
(hidden)

$uservar "PAD-AREA" ua_ofs_padarea 0

;; this will be patched by the startup/thread creation code
$uservar "(DEFAULT-TIB)" ua_ofs_deftib 0
(hidden)

$constant "(DEFAULT-#TIB)" 4090
(hidden)

$if URFORTH_TLS_TYPE
;; default user area values
;; this will be created and set by the metacompiler
$constant "(USER-AREA-DEFAULT)"  ua_default_values
(hidden)
$constant "(USER-AREA-MAX-SIZE)" URFORTH_MAX_USERAREA_SIZE
(hidden)
$variable "(USER-AREA-USED)"     ur_userarea_default_size
(hidden)

$if URFORTH_TLS_TYPE = URFORTH_TLS_TYPE_FS
$value "(USER-TLS-ENTRY-INDEX)" 0
(hidden)
$endif
$endif

$constant "HAS-TLS?" URFORTH_TLS_TYPE


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pad is allocated with BRK; "#PAD-AREA-RESV" is the space BEFORE "PAD-AREA"
;; $variable "PAD-AREA" 0  ;; will be set by startup code
$constant "#PAD-AREA-RESV" 2040
$constant "#PAD-AREA" 2048


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; optimiser options

;; optimise branch to branch to branch?
$value "(OPT-BRANCHES?)" 1
(hidden)

;; should optimiser replace alias calls with aliased words?
;; this doesn't affect REPLACED words, tho
;; actually, for this to work, the word size should be exactly 5/8 bytes, and
;; starts with jmp
$value "(OPT-ALIASES?)" 0
(hidden)

;; should replaced words be rerouted in the same ways as aliases?
;; this may break further replacing, so it is turned off
;; not yet implemented
;;$value "(OPT-REPLACES?)" 0
;;(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$variable "STATE" 0

$variable "DP" urforth_code_end
(hidden)
$variable "DP-LAST-ADDR" urforth_dict_lastaddr  ;; last valid dictionary address
(hidden)
$variable "DP-PROTECTED" urforth_dict_protected  ;; first unprotected address
(hidden)

;; this is used to temporarily change HERE
;; MUST be inside "(DP-TEMP-BASE)", because first dp-temp-base cell is used to check size
$variable "DP-TEMP" 0
(hidden)
$variable "(DP-TEMP-BASE)" 0  ;; first cell is size
(hidden)
$variable "(DP-TEMP-BUF)"  0
(hidden)

;; voclink always points to another voclink (or contains 0)
$variable "(VOC-LINK)" forth_wordlist_voclink  ;; voclink always points to wordlist voclink (or contains 0)
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BSS reserve on SAVE
$value "(SAVE-BSS-RESERVE)" URFORTH_BSS_RESERVE
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "ARGC" 0
$value "ARGV" 0
$value "ENVP" 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "HAS-DEBUGGER?" URFORTH_DEBUG
$constant "HAS-DEBUG-INFO?" URFORTH_DEBUG_INFO
$constant "ALIGN-HEADERS?" URFORTH_ALIGN_HEADERS
$constant "ALIGN-CFA?" URFORTH_ALIGN_CFA
$constant "ALIGN-PFA?" URFORTH_ALIGN_PFA
$constant "(#CFA)" URFORTH_CFA_SIZE
(hidden)

$constant "(CODE-BASE-ADDR)" urforth_code_base_addr
(hidden)

;; two dwords -- disk and memory
$constant "(ELF-HEADER-CODE-SIZE-ADDR)" elfhead_codesize_addr
(hidden)

$constant "(ELF-DYNAMIC?)" URFORTH_DYNAMIC_BINARY
(hidden)

$if URFORTH_DYNAMIC_BINARY
$constant "(CODE-IMPORTS-ADDR)" elfhead_impstart
(hidden)
;; in bytes
$constant "(CODE-IMPORTS-SIZE)" elfhead_implen
(hidden)
$endif

$constant "(CODE-ENTRY-ADDR)" urforth_entry_point
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "TLOAD-VERBOSE-LIBS" 1
$value "TLOAD-VERBOSE" 0
$value "TLOAD-VERBOSE-DEFAULT" 0
$value "TLOAD-VERBOSE-RC" 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; moved to "10_litbase.f"
;; $constant "TRUE"  1
;; $constant "FALSE" 0

$constant "CELL" 4
$constant "BL" 32
$constant "NL" 10


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; all things is this chain will be called before and after saving the image
: (startup-init) ... ; (hidden)

;; this chain will be executed on abort, to cleanup the things
: (ABORT-CLEANUP)  ( -- )  ... ; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; for speed
code: (TLS-BASE-ADDR)  ( -- addr )
  push  TOS
 $if URFORTH_TLS_TYPE = URFORTH_TLS_TYPE_FS
  ld    TOS,ts:[ua_ofs_baseaddr]
 $else
  xor   TOS,TOS
 $endif
  urnext
endcode
(hidden)

code: (self@)  ( -- value )
  push  TOS
  ld    TOS,ts:[ua_ofs_self]
  urnext
endcode

code: (self!)  ( value -- )
  ld    ts:[ua_ofs_self],TOS
  pop   TOS
  urnext
endcode

code: (locptr@)  ( -- value )
  push  TOS
  ld    TOS,ts:[ua_ofs_locptr]
  urnext
endcode

code: (locptr!)  ( value -- )
  ld    ts:[ua_ofs_locptr],TOS
  pop   TOS
  urnext
endcode
