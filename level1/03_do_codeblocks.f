;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; aligned PFA forces aligned CFA (metacompiler does this)

code: (URFORTH-DOXXX-CODEBLOCKS)
  align 16,$90
ur_doforth:
  pushr EIP
  pop   EIP
  $if URFORTH_ALIGN_PFA
  add   EIP,3
  $endif
  urnext

  align 16,$90
ur_doconst:
  xchg  TOS,[esp]
  $if URFORTH_ALIGN_PFA
  add   TOS,3
  $endif
  mov   TOS,[TOS]
  urnext

  align 16,$90
ur_dovar:
  xchg  TOS,[esp]
  $if URFORTH_ALIGN_PFA
  add   TOS,3
  $endif
  urnext

  align 16,$90
ur_dovalue:
  xchg  TOS,[esp]
  $if URFORTH_ALIGN_PFA
  add   TOS,3
  $endif
  mov   TOS,[TOS]
  urnext

  align 16,$90
ur_dodefer:
  pop   eax
  $if URFORTH_ALIGN_PFA
  add   eax,3
  $endif
  mov   eax,[eax]
  jp    eax

  align 16,$90
ur_dodoes:
  ;; pfa is on the stack
  ;; EAX is new VM IP
  xchg  TOS,[esp]
  pushr EIP
  $if URFORTH_ALIGN_PFA
  add   TOS,3
  $endif
  mov   EIP,eax
  urnext

  align 16,$90
ur_dooverride:
  pushr EIP
  pop   EIP
  $if URFORTH_ALIGN_PFA
  add   EIP,3
  $endif
  xchg  TOS,[esp]
  $if URFORTH_ALIGN_PFA
  add   TOS,3
  $endif
  urnext

  align 16,$90
ur_douservar:
  xchg  TOS,[esp]
  $if URFORTH_ALIGN_PFA
  add   TOS,3
  $endif
 $if URFORTH_TLS_TYPE = URFORTH_TLS_TYPE_FS
  ld    TOS,[TOS]   ;; offset
  add   TOS,ts:[0]  ;; add user area base address
 $endif
  urnext

;; use this subroutine to call a forth word from a machine code
;; EAX should point to the cfa
;; TOS and other things should be set accordingly
;; direction flag should be cleared
;; no registers are preserved
  align 16,$90
ur_mc_fcall:
  ;; move mc return address to rstack
  pop   edx
  pushr edx
  ;; push current EIP to rstack
  pushr EIP
  ;; set new EIP
  mov   EIP,.justexit

  ;; turn off debugger temporarily, because the debugger is using this
  $if URFORTH_DEBUG
  ld    edx,[urfdebug_active_flag]
  pushr edx
  ;; special flag, means "no breakpoint checks"
  ld    dword [urfdebug_active_flag],-1
  $endif

  ;; and execute the word
  jp    eax
.justexit:
  dd  .fakeret_code
.fakeret_code:
  ;; restore debugger state
  $if URFORTH_DEBUG
  popr  edx
  ld    dword [urfdebug_active_flag],edx
  $endif

  ;; restore EIP
  popr  EIP
  ;; restore return address
  popr  eax
  ;; and jump there
  jp    eax
endcode
(hidden) (codeblock)


$constant "(URFORTH-DOFORTH-ADDR)" ur_doforth
$constant "(URFORTH-DOCONST-ADDR)" ur_doconst
$constant "(URFORTH-DOVAR-ADDR)" ur_dovar
$constant "(URFORTH-DOVALUE-ADDR)" ur_dovalue
$constant "(URFORTH-DODEFER-ADDR)" ur_dodefer
$constant "(URFORTH-DODOES-ADDR)" ur_dodoes
$constant "(URFORTH-DOOVERRIDE-ADDR)" ur_dooverride
$constant "(URFORTH-DOUSERVAR-ADDR)" ur_douservar
$constant "(URFORTH-FCALL-ADDR)" ur_mc_fcall
