;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; compile call to cfa
;; do not use comma directly, so high-level words will survive any threaded code changes
;; also, do not use this ONLY to compile calls to other words!
;; this is because of possible future optimiser
: COMPILE,  ( cfa -- )
  forth:(opt-aliases?) if optimiser:optimise-alias-call if exit endif endif reladdr,
;

;; compile CFA it is is not zero
: ?COMPILE,  ( cfa//0 -- )  ?dup if compile, endif ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: [  ( -- )  state 0! ; immediate
: ]  ( -- )  state 1! ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary COMPILER
voc-set-active COMPILER

: (?N-STACK)  ( depth -- )  depth 1- > err-unfinished-definition ?error ;

: ?STACK  ( -- )  0 [execute-tail] (?n-stack) ;
: ?COMP  ( -- )  ?stack state @ err-compilation-only not-?error ;
: ?EXEC  ( -- )  ?stack state @ err-execution-only ?error ;
: ?NON-MACRO  ( -- )  ?stack (latest-macro?) err-nonmacro-only ?error ;

: ?PAIR  ( n1 n2 -- true // n1 false )  2 (?n-stack) 2dup = if 2drop true else drop false endif ;
: ?PAIRS  ( n1 n2 -- )  2 (?n-stack) <> err-unpaired-conditionals ?error ;
: ?ANY-PAIR  ( id v0 v1 -- )  3 (?n-stack) >r over <> swap r> <> and err-unpaired-conditionals ?error ;
: ?PAIRS-ANY-KEEPID  ( id v0 v1 -- id )  3 (?n-stack)  >r over <>  over r> <> and err-unpaired-conditionals ?error ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; usage:
;;  compile 0branch
;;  (mark>)
;;  ...
;;  (resolve>)
;;
;;  (<mark)
;;  ...
;;  compile branch
;;  (<resolve)

;; write "branch to destaddr" address to addr
;; : (BRANCH-ADDR!)  ( destaddr addr -- )  !  ; (hidden)
alias ! (BRANCH-ADDR!)
(hidden)

;; read branch address
alias @ (BRANCH-ADDR@)  ( addr -- dest )
(hidden)


;; reserve room for branch address, return addr suitable for "(RESOLVE-J>)"
: (MARK-J>)  ( -- addr )
  here 0 ,
; (hidden)

;; compile "forward jump" from address to HERE
;; addr is the result of "(MARK-J>)"
: (RESOLVE-J>)  ( addr -- )
  here swap (branch-addr!)
; (hidden)


;; return addr suitable for "(<J-RESOLVE)"
: (<J-MARK)  ( -- addr )
  here
; (hidden)

;; patch "forward jump" address to HERE
;; addr is the result of "(<J-MARK)"
: (<J-RESOLVE)  ( addr -- )
  cell n-allot (branch-addr!)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; compile "start cblock"
: cblock-start  ( -- mark )
  compile LITCBLOCK
  (mark-j>)
  (URFORTH-DOFORTH-ADDR) compiler:(cfa-call,)
  optimiser:jstack-subinit
;

: cblock-end  ( mark -- )
  compile exit
  (resolve-j>)
  optimiser:optimise-jumps
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: forth-word-prologue  ( -- )
  (dbginfo-reset-activate)
  $if URFORTH_ALIGN_CFA
  check-align-here
  $endif
  (URFORTH-DOFORTH-ADDR) compiler:(cfa-call,)
  $if URFORTH_ALIGN_PFA
  check-align-here
  $endif
  optimiser:jstack-init
;

: forth-word-epilogue  ( -- )
  create;
 $if URFORTH_DEBUG_INFO
  ;; save debug info (this also resets and deactivates it)
  (dbginfo-finalize-and-copy)
 $endif
  optimiser:optimise-jumps
;

: start-compile-forth-word  ( -- )
  forth-word-prologue [compile] ]
;

: end-compile-forth-word  ( -- )
  forth-word-epilogue
  smudge [compile] [
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; write 4-byte displacement for CALL/JMP jmpdest instruction to addr
;; addr should point after the instruction, at the first displacement byte
: (DISP32!)  ( jmpdest addr -- )
  dup cell+ rot swap - swap !
; (hidden)

: (DISP32@)  ( addr -- jumpdest )
  dup @ swap cell+ +
; (hidden)

;; write 5-byte CALL calldest machine instruction to addr
: (CALL!)  ( calldest addr -- )
  0xe8 over c! 1+  ;; write CALL, advance address
  (disp32!)
; (hidden)

;; write 5-byte JMP jmpdest machine instruction to addr
: (JMP!)  ( jmpdest addr -- )
  0xe9 over c! 1+  ;; write JMP, advance address
  (disp32!)
; (hidden)

;; compile 5-byte CALL calldest machine instruction to HERE
: (CFA-CALL,)  ( calldest -- )
  $if URFORTH_ALIGN_CFA
  check-align-here
  $endif
  5 n-allot (call!)
  $if URFORTH_ALIGN_PFA
  align-here
  $endif
; (hidden)

;; compile 5-byte JMP jmpdest machine instruction to HERE
: (JMP,)  ( jmpdest -- )
  5 n-allot (jmp!)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: c4str-unaligned,  ( addr count -- )
  dup forth:unused 2u/ u>= ERR-STRING-TOO-LONG ?error
  ;; check if the string is at "here" (just in case)
  over here here cell+ 1- bounds? if  ;; move forward to make room for counter
    dup >r here cell+ swap move
    here cell+ r>
  endif
  dup cell+ n-allot  ( addr count destaddr )
  2dup ! cell+  ;; length
  swap move     ;; string itself
;

: c4str,  ( addr count -- )  c4str-unaligned, align-here ;

: c4strz-unaligned,  ( addr count -- ) c4str-unaligned, 0 c, ;
: c4strz,  ( addr count -- )  c4strz-unaligned, align-here ;

: c1str-unaligned,  ( addr count -- )
  dup forth:unused 2u/ u>= ERR-STRING-TOO-LONG ?error
  dup 255 u> ERR-STRING-TOO-LONG ?error
  ;; check if the string is at "here" (just in case)
  over here = if  ;; move forward to make room for counter
    dup >r here 1+ swap move
    here 1+ r>
  endif
  dup 1+ n-allot  ( addr count destaddr )
  2dup c! 1+    ;; length
  swap move     ;; string itself
;

: c1str,  ( addr count -- )  c1str-unaligned, align-here ;

: c1strz-unaligned,  ( addr count -- )  c1str-unaligned, 0 c, ;
: c1strz,  ( addr count -- )  c1strz-unaligned, align-here ;


;; byte-counted
: custom-c1sliteral,  ( addr count cfa -- )
  ?dup if
    over 255 u> ERR-STRING-TOO-LONG ?error
    >r  ;; save cfa
    ;; check if the string is at "here" (just in case)
    over here here cell+ bounds? if  ;; move forward to make room for "litstr" and other things
      dup >r here cell+ swap move
      here cell+ r>
    endif
    r> compile,  ;; literal word
  endif
  c1strz,
;

: custom-c4sliteral,  ( addr count cfa -- )
  ?dup if
    over forth:unused 2u/ u>= ERR-STRING-TOO-LONG ?error
    >r  ;; save cfa
    ;; check if the string is at "here" (just in case)
    over here here 2 +cells 1- bounds? if  ;; move forward to make room for "litstr" and other things
      dup >r here 2 +cells swap move
      here 2 +cells r>
    endif
    r> compile,  ;; literal word
  endif
  c4strz,
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: literal,  ( n -- )
  dup case
     0 of drop compile lit0 endof
     1 of drop compile lit1 endof
    -1 of drop compile lit-1 endof
  $if URFORTH_ALIGN_PFA
    ;; no 8/16 bit literals
  $else
         0   256 within-of compile litu8 c, endof
      -128   128 within-of compile lits8 c, endof
         0 65536 within-of compile litu16 w, endof
    -32768 32768 within-of compile lits16 w, endof
  $endif
    compile lit ,
  endcase
;

: 2literal,  ( dlo dhi -- )  swap compiler:literal, compiler:literal, ;
: addrliteral,  ( addr -- )  compile lit reladdr, ;
: cfaliteral,  ( cfa -- )  compile litcfa reladdr, ;
: c4sliteral,  ( addr count -- )  ['] litc4str custom-c4sliteral, ;
: c1sliteral,  ( addr count -- )  ['] litc1str custom-c1sliteral, ;
;; puts byte-counted string literal if possible
: sliteral,  ( addr count -- )
  dup 0 255 bounds? if ['] litc1str custom-c1sliteral,
  else ['] litc4str custom-c4sliteral, endif
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; used to dynamically find and call named word with temporary "previous"
;; can be used to override things like "exit" to perform additional cleanup
: late-chain-prevvoc  ( addr count -- ... )
  context >r previous 2dup wfind dup ifnot ?endcr if space endif type err-unknown-word error endif
  2swap 2drop
  r> to context state @ ifnot drop 1 endif
  +if execute-tail else compile, endif
;

;; used to dynamically find and call named word
;; can be used to override things like ";" to perform additional cleanup
;; this doesn't do temp "previous" (because ";" usually does this by itself)
: late-chain  ( addr count -- ... )
  2dup wfind dup ifnot ?endcr if space endif type err-unknown-word error endif
  2swap 2drop
  state @ ifnot drop 1 endif
  +if execute-tail else compile, endif
;

voc-set-active FORTH


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is better done with custom creation word, but metacompiler cannot do that...
: LITERAL  ( n -- )  state @ if compiler:literal, endif ; immediate
: 2LITERAL  ( dlo dhi -- )  state @ if compiler:2literal, endif ; immediate
: ADDRLITERAL  ( n -- )  state @ if compiler:addrliteral, endif ; immediate
: CFALITERAL  ( cfa -- ) state @ if compiler:cfaliteral, endif ; immediate
: C4SLITERAL  ( addr count -- )  state @ if compiler:c4sliteral, endif ; immediate
: C1SLITERAL  ( addr count -- ) state @ if compiler:c1sliteral, endif ; immediate
;; puts byte-counted string literal if possible
: SLITERAL  ( addr count -- )  state @ if compiler:sliteral, endif ; immediate
