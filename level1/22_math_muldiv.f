;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: *  ( n0 n1 -- n0*n1 )
  pop   eax
  xchg  eax,TOS
  imul  TOS,eax
  urnext
endcode

;; remainder has the same sign as the original number
;; this is true for all signed divisions and modulos
;; (except SM/REM and FM/MOD)
code: /  ( n0 n1 -- n0/n1 )
  pop   eax
  ;; TOS=n1
  ;; EAX=n0
  cdq
  idiv  TOS
  mov   TOS,eax
  urnext
endcode

code: MOD  ( n0 n1 -- n0%n1 )
  pop   eax
  ;; TOS=n1
  ;; EAX=n0
  cdq
  idiv  TOS
  mov   TOS,edx
  urnext
endcode

code: /MOD  ( n0 n1 -- n0%n1 n0/n1 )
  pop   eax
  ;; TOS=n1
  ;; EAX=n0
  cdq
  idiv  TOS
  push  edx
  mov   TOS,eax
  urnext
endcode

code: U*  ( u0 u1 -- n0*n1 )
  pop   eax
  xchg  eax,TOS
  mul   TOS
  mov   TOS,eax
  urnext
endcode

code: U/  ( u0 u1 -- n0/n1 )
  pop   eax
  ;; TOS=n1
  ;; EAX=n0
  xor   edx,edx
  div   TOS
  mov   TOS,eax
  urnext
endcode

code: UMOD  ( u0 u1 -- n0%n1 )
  pop   eax
  ;; TOS=n1
  ;; EAX=n0
  xor   edx,edx
  div   TOS
  mov   TOS,edx
  urnext
endcode

code: U/MOD  ( u0 u1 -- umod ures )
  pop   eax
  ;; TOS=n1
  ;; EAX=n0
  xor   edx,edx
  div   TOS
  push  edx
  mov   TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: */  ( n0 n1 n2 -- n0*n1/n2 )
  pop   eax
  pop   ebx
  ;; TOS=n2
  ;; EAX=n1
  ;; EBX=n0
  imul  ebx
  idiv  TOS
  ld    TOS,eax
  urnext
endcode


code: */MOD  ( n0 n1 n2 -- n0*n1/n2 n0*n1%n2 )
  pop   eax
  pop   ebx
  ;; TOS=n2
  ;; EAX=n1
  ;; EBX=n0
  imul  ebx
  idiv  TOS
  push  edx
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: U*/  ( u0 u1 u2 -- u0*u1/u2 )
  pop   eax
  pop   ebx
  ;; TOS=u2
  ;; EAX=u1
  ;; EBX=u0
  mul  ebx
  div  TOS
  ld    TOS,eax
  urnext
endcode


code: U*/MOD  ( u0 u1 u2 -- u0*u1/u2 u0*u1%u2 )
  pop   eax
  pop   ebx
  ;; TOS=u2
  ;; EAX=u1
  ;; EBX=u0
  mul  ebx
  div  TOS
  push  edx
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rounds toward zero
code: SM/REM  ( d n -- nmod ndiv )  \ ANS
  pop   edx
  pop   eax
  idiv  TOS
  push  edx
  ld    TOS,eax
  urnext
endcode

;; rounds toward negative infinity
code: FM/MOD  ( d n -- nmod ndiv )  \ ANS
  pop   edx
  pop   eax
  ld    ebx,edx
  idiv  TOS
  test  edx,edx
  jr    z,@f
  xor   ebx,TOS
  jr    ns,@f
  dec   eax
  add   edx,TOS
@@:
  push  edx
  ld    TOS,eax
  urnext
endcode
