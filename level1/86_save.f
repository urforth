;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pre- and post-save hooks
;; WARNING! *NEVER* throw any errors from the following scattered chains!

;; use scattered append (;..)
: (pre-save) ... ;
;; use scattered prepend (<;..)
: (post-save) ... ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; save current image to ELF executable file
;; we're using the fact that our ELF header is loaded into memory, and is writeable
;; metacompiler will prolly need to rebuild it from scratch, but for now... meh
: (SAVE)  ( fd -- successflag )
  ;; just in case, check fd
  dup -if drop false exit endif
  ;; move fd to return stack
  >r
  ;; fix code segment size
  ;; write code from code start to real HERE
  real-here (code-base-addr) - dup (elf-header-code-size-addr) !
  1- 4095 or 1+  (save-bss-reserve) 65536 max 1- 4095 or 1+ +  ;; BSS size (rounded to 4096)
  (elf-header-code-size-addr) cell+ !  ;; reserve BSS
  ;; everything in our header is ok now, including entry point (it isn't changed)
 $if URFORTH_DYNAMIC_BINARY
  ;; write everything up until import table
  (code-base-addr) (code-imports-addr) (code-base-addr) - r@ os:write
  (code-imports-addr) (code-base-addr) - = ifnot rdrop false exit endif
  ;; write zero bytes for imports: this is where import addresses will be put by ld.so
  ;; use HERE for this
  real-here (code-imports-size) erase
  real-here (code-imports-size) r@ os:write
  (code-imports-size) = ifnot rdrop false exit endif
  ;; write code from imports end to real here
  (code-imports-addr) (code-imports-size) + real-here over - r@ over >r os:write
 $else
  ;; write the whole code chunk
  (code-base-addr) real-here over - r@ over >r os:write
 $endif
  r>  ;; restore write result and number of bytes written
  = ifnot rdrop false exit endif
  ;; you may not believe me, but we're done!
  rdrop    ;; got rid of fd
  true     ;; exit with success
; (hidden)


: SAVE  ( addr count -- successflag )
  ;; create output file
  os:o-wronly os:o-creat or os:o-trunc or  ;; flags
  os:s-irwxu os:s-irgrp or os:s-ixgrp or os:s-iroth or os:s-ixoth or  ;; mode
  os:open
  ;; check success
  dup -if drop false exit endif
  (pre-save)
  \ FIXME: this may break some MT code; but you should never save when MT is active anyway
  (mt-active?) >r 0 to (mt-active?)
  dup ['] (save) catch
  r> to (mt-active?)
  throw
  ( fd successflag )
  swap os:close 0=
  logand
  (post-save)
;
