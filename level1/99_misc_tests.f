;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

$if 0
vocabulary voc2

voc-set-active voc2

: v2 ." boo!\n" ;

voc-set-active forth

: v2 ." foo!\n" ;

;; this multiplies number by PI
: pi ( n -- n*pi) 355 113 */ ;
$endif


$if 0
: scatter-test ... ." init complete\n" ;

..: scatter-test ." init phase1\n" ;..
..: scatter-test ." init phase2\n" ;..
..: scatter-test ." init phase3\n" ;..
..: scatter-test ." init phase4\n" ;..
$endif

$if 0
10 value (val)
2 var (test)
$endif

$if 0
10 value (val)
(val) var (test)
42 (test) +!
10 (test) -!
$endif
