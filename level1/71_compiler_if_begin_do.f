;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

voc-set-active COMPILER


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; each of these has one argument
enum{
  1 set
  value (CTLID-IF)  (hidden)
  value (CTLID-ELSE)  (hidden)

  value (CTLID-BEGIN)  (hidden)
  value (CTLID-WHILE)  (hidden)

  value (CTLID-CASE)  (hidden)
  value (CTLID-OF)  (hidden)
  value (CTLID-ENDOF)  (hidden)
  value (CTLID-OTHERWISE)  (hidden)

  value (CTLID-DO)  (hidden)
  value (CTLID-DO-BREAK)  (hidden)
  value (CTLID-DO-CONTINUE)  (hidden)

  value (CTLID-CBLOCK)  (hidden)
  value (CTLID-CBLOCK-INTERP)  (hidden)

  value (CTLID-?DO)  (hidden)

  value (CTLID-COLON)  (hidden)
  value (CTLID-DOES)  (hidden)
  value (CTLID-SC-COLON)  (hidden)

  666 +set
  value (XX-CTLID-LAST)  (hidden)
}

(XX-CTLID-LAST) var (XX-CTLID-NEXT-USER)  (hidden)

: allocate-ctlid  ( -- id )  (xx-ctlid-next-user) @ (xx-ctlid-next-user) 1+! ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; there might be a lot of "while" blocks, pop them all
;; compile jump back to begin
: (END-BEGIN)  ( pairs... jumpcfa -- )
  ;; this is done recursively, because this way i can get rid of `par_resolve_jfwd_over_branch`
  ;; also, we don't have working loops at this point, so recursion is the only choice ;-)
  ?stack
  over (CTLID-BEGIN) = if
    optimiser:jpush-branch
    compile,
    (CTLID-BEGIN) ?pairs
    (<j-resolve)
  else
    swap
    (CTLID-WHILE) ?pairs
    swap >r recurse r>
    (resolve-j>)
  endif
; (hidden)

: (COMP-WHILE)  ( jumpcfa )
  ?comp
  >r (CTLID-BEGIN) (CTLID-WHILE) ?pairs-any-keepid r>
  optimiser:jpush-branch
  compile,
  (mark-j>)
  (CTLID-WHILE)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; alas, i had to use one global var
;; <>0: drop when we'll see CASE
;; set to 0 by (CTLID-OF) or (CTLID-OTHERWISE)
$value "(B/C-CASE-DROP)" 0
(hidden)

;; workhorse for break/continue
;; type:
;;   0: break
;;   1: continue
: (BREAK/CONTINUE)  ( type )
  ?comp
  0 2>r  ;; type and counter
  ;; drop on case by default
  1 to (b/c-case-drop)
  begin
    ?stack
    ;; check for valid ctlid
    dup (CTLID-DO-CONTINUE) > over (CTLID-IF) < or ERR-INVALID-BREAK-CONT ?error
    ;; while not begin and not do
    dup (CTLID-BEGIN) = over (CTLID-DO) = or not
  while
    ;; move to rstack
      ;; DEBUG
      ;; 2dup pardottype "SAVE: ctlid: " dot  pardottype " addr: " udot cr
    ;; process case:
    ;;   if we're in (CTLID-CASE) or in (CTLID-ENDOF), compile DROP
    dup (CTLID-CASE) = if
      (b/c-case-drop) if
          ;; DEBUG
          ;; pardottype " compiling DROP (" dup dot pardottype ")" cr
        compile drop
      endif
      ;; drop on next case by default
      1 to (b/c-case-drop)
    endif
    dup (CTLID-OF) = over (CTLID-OTHERWISE) = or
    if 0 to (b/c-case-drop) endif  ;; don't drop on next case by default
    2r> 2swap >r >r 2+ 2>r
  repeat
  ;; return stack contains saved values and counter
  dup (CTLID-DO) = if
    ;; do...loop
    ;; check type
    1 rpick  ;; peek the type
    if
        ;; DEBUG
        ;; pardottype "DO/LOOP: continue" cr
      ;; coninue: jump to (LOOP)
      optimiser:jpush-branch
      compile branch
      (mark-j>)
      (CTLID-DO-CONTINUE)
    else
      ;; break: drop do args, jump over (LOOP)
        ;; DEBUG
        ;; pardottype "DO/LOOP: break" cr
      compile unloop  ;; remove do args
      optimiser:jpush-branch
      compile branch
      (mark-j>)
      (CTLID-DO-BREAK)
    endif
  else
    (CTLID-BEGIN) ?pairs
    ;; check type
    1 rpick  ;; i.e. peek the type
    if
      ;; coninue
        ;; DEBUG
        ;; pardottype "BEGIN: continue" cr
      dup            ;; we still need the address
      optimiser:jpush-branch
      compile branch
      (<j-resolve)
      (CTLID-BEGIN)    ;; restore ctlid
    else
      ;; break
        ;; DEBUG
        ;; pardottype "BEGIN: break" cr
      (CTLID-BEGIN)    ;; restore ctlid
      optimiser:jpush-branch
      compile branch
      (mark-j>)
      (CTLID-WHILE)
    endif
  endif

  ;; move saved values back to the data stack
  r> rdrop  ;; drop type
    ;; DEBUG
    ;; dup pardottype "RESTORE " dot pardottype "items" cr
  begin ?dup while r> swap 1- repeat
    ;; DEBUG
    ;; dup . over udot cr
; (hidden)


: (END-LOOP)  ( endloopcfa )
  ;; this is done recursively, because this way i can get rid of `par_resolve_jfwd_over_branch`
  ?stack
  over (CTLID-DO) = if
    \ optimiser:jpush-branch  ;; this compiles "(loop)" kind, it won't be optimised anyway
    compile,
    (CTLID-DO) ?pairs
    (<j-resolve)
    ;; resolve ?DO jump, if it is there
    dup (CTLID-?DO) = if drop (resolve-j>) endif
  else
    ;; "continue" should be compiled before recursion, and "break" after it
    swap
    dup (CTLID-DO-CONTINUE) = if
      ;; patch "continue" branch
      (CTLID-DO-CONTINUE) ?pairs
      swap (resolve-j>)
      recurse
    else
      (CTLID-DO-BREAK) ?pairs
      swap >r recurse r>
      ;; here, loop branch already compiled
      (resolve-j>)
    endif
  endif
; (hidden)


: (X-OF)  ( ... word-to-compare )
  ?comp
  >r  ;; save XOF args
  (CTLID-CASE) (CTLID-ENDOF) ?pairs-any-keepid   ;; we should be in normal CASE
  \ compile over  ;; special compare words will do this for us
  r> compile,  ;; comparator
  \ optimiser:jpush-branch  ;; there is no real reason to check such jumps; and we may have A LOT of them
  compile 0branch-drop
  (mark-j>)
  (CTLID-OF)
; (hidden)

: (END-CASE)
  dup (CTLID-OTHERWISE) = if
    ;; "otherwise", no drop needed
    (CTLID-OTHERWISE) ?pairs
    0 ?pairs  ;; check dummy argument
  else
    ;; no "otherwise", compile DROP
    compile drop
  endif
  ;; patch branches
  begin
    ?stack
    dup (CTLID-CASE) <>
  while
    (CTLID-ENDOF) ?pairs
    (resolve-j>)
  repeat
  (CTLID-CASE) ?pairs
  0 ?pairs  ;; check dummy argument
;


voc-set-active FORTH


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: IF
  compiler:?comp
  optimiser:jpush-branch
  compile 0branch
  compiler:(mark-j>)
  compiler:(CTLID-IF)
; immediate

: IFNOT
  compiler:?comp
  optimiser:jpush-branch
  compile tbranch
  compiler:(mark-j>)
  compiler:(CTLID-IF)
; immediate

;; if negative (not zero)
: -IF
  compiler:?comp
  optimiser:jpush-branch
  compile +0branch
  compiler:(mark-j>)
  compiler:(CTLID-IF)
; immediate

;; if positive (not zero)
: +IF
  compiler:?comp
  optimiser:jpush-branch
  compile -0branch
  compiler:(mark-j>)
  compiler:(CTLID-IF)
; immediate

;; if negative or zero
: -0IF
  compiler:?comp
  optimiser:jpush-branch
  compile +branch
  compiler:(mark-j>)
  compiler:(CTLID-IF)
; immediate

;; if positive or zero
: +0IF
  compiler:?comp
  optimiser:jpush-branch
  compile -branch
  compiler:(mark-j>)
  compiler:(CTLID-IF)
; immediate

: ELSE
  compiler:?comp compiler:(CTLID-IF) compiler:?pairs
  optimiser:jpush-branch
  compile branch
  compiler:(mark-j>)
  swap compiler:(resolve-j>)
  compiler:(CTLID-ELSE)
; immediate

: ENDIF
  compiler:?comp
  compiler:(CTLID-IF) compiler:(CTLID-ELSE) compiler:?any-pair
  compiler:(resolve-j>)
; immediate

alias endif then


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; you can use as many "while" blocks as you want to
;; any loop can be finished with AGAIN/REPEAT/UNTIL
;; "BREAK" and "CONTINUE" cannot be used inside conditionals yet
;;

: BEGIN
  compiler:?comp
  compiler:(<j-mark)
  compiler:(CTLID-BEGIN)
; immediate

;; repeats while the condition is false
: UNTIL
  compiler:?comp
  ['] 0branch compiler:(end-begin)
; immediate

;; repeats while the condition is true
: NOT-UNTIL
  compiler:?comp
  ['] tbranch compiler:(end-begin)
; immediate

;; repeats while the number if positive
: -UNTIL
  compiler:?comp
  ['] +0branch compiler:(end-begin)
; immediate

;; repeats while the number if negative
: +UNTIL
  compiler:?comp
  ['] -0branch compiler:(end-begin)
; immediate

;; repeats while the number if positive or zero
: -0UNTIL
  compiler:?comp
  ['] +branch compiler:(end-begin)
; immediate

;; repeats while the number if negative or zero
: +0UNTIL
  compiler:?comp
  ['] -branch compiler:(end-begin)
; immediate

: AGAIN
  compiler:?comp
  ['] branch compiler:(end-begin)
; immediate

alias AGAIN REPEAT

: WHILE
  ['] 0branch compiler:(comp-while)
; immediate

: NOT-WHILE
  ['] tbranch compiler:(comp-while)
; immediate

: -WHILE
  ['] +0branch compiler:(comp-while)
; immediate

: +WHILE
  ['] -0branch compiler:(comp-while)
; immediate

: -0WHILE
  ['] +branch compiler:(comp-while)
; immediate

: +0WHILE
  ['] -branch compiler:(comp-while)
; immediate


: CONTINUE
  1 compiler:(break/continue)
; immediate

: BREAK
  0 compiler:(break/continue)
; immediate

;; this has to be here
alias BREAK LEAVE


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; data stack:
;;   0 (CTLID-CASE)
;;     addr (CTLID-OF) -- when in "OF"
;;     addr (CTLID-ENDOF) -- when "ENDOF" compiled
;;     0 (CTLID-OTHERWISE) -- when "OTHERWISE" compiled
;; note that "(CTLID-ENDOF)"s will be accumulated, and resolved in "ENDCASE"
;;

: CASE
  compiler:?comp
  0 compiler:(CTLID-CASE)  ;; with dummy argument
; immediate

: OF  ['] forth:(of=) compiler:(x-of) ; immediate
: NOT-OF  ['] forth:(of<>) compiler:(x-of) ; immediate
: <OF  ['] forth:(of<) compiler:(x-of) ; immediate
: <=OF  ['] forth:(of<=) compiler:(x-of) ; immediate
: >OF  ['] forth:(of>) compiler:(x-of) ; immediate
: >=OF  ['] forth:(of>=) compiler:(x-of) ; immediate
: U<OF  ['] forth:(of-U<) compiler:(x-of) ; immediate
: U<=OF  ['] forth:(of-U<=) compiler:(x-of) ; immediate
: U>OF  ['] forth:(of-U>) compiler:(x-of) ; immediate
: U>=OF  ['] forth:(of-U>=) compiler:(x-of) ; immediate
: &OF  ['] forth:(of-and) compiler:(x-of) ; immediate
: AND-OF  ['] forth:(of-and) compiler:(x-of) ; immediate
: ~AND-OF  ['] forth:(of-~and) compiler:(x-of) ; immediate
: WITHIN-OF  ['] forth:(of-within) compiler:(x-of) ; immediate
: UWITHIN-OF  ['] forth:(of-uwithin) compiler:(x-of) ; immediate
: BOUNDS-OF  ['] forth:(of-bounds) compiler:(x-of) ; immediate

: ENDOF
  compiler:?comp compiler:(CTLID-OF) compiler:?pairs
  optimiser:jpush-branch
  compile branch
  compiler:(mark-j>)
  swap compiler:(resolve-j>)
  compiler:(CTLID-ENDOF)
; immediate

: OTHERWISE
  compiler:(CTLID-CASE) compiler:(CTLID-ENDOF) compiler:?pairs-any-keepid
  0 compiler:(CTLID-OTHERWISE)
; immediate

: ENDCASE
  compiler:?comp
  compiler:(end-case)
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DO
  compiler:?comp
  compile (do)
  compiler:(<j-mark)
  compiler:(CTLID-DO)
; immediate

: LOOP
  compiler:?comp
  ['] (loop) compiler:(end-loop)
; immediate

: +LOOP
  compiler:?comp
  ['] (+loop) compiler:(end-loop)
; immediate

: ?DO
  compiler:?comp
  optimiser:jpush-branch
  compile ?do-branch
  compiler:(mark-j>)
  compiler:(CTLID-?DO)
  [compile] do
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; "FOR" is for loops with "step by 1", from 0
: FOR
  compiler:?comp
  optimiser:jpush-branch
  compile (for)
  compiler:(mark-j>)
  compiler:(CTLID-?DO)
  compiler:(<j-mark)
  compiler:(CTLID-DO)
; immediate

: ENDFOR
  [compile] loop
; immediate
