;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; flags for "(VOC-FIND-FLAGS-MASK)"
;; word flags is 16 bit, use other bits for flags
;; it is still possible to use simple "and" to check the mask
bitenum{
  24 set-bit
  value (VOC-FIND-CASE-SENSITIVE)
}

;; "(VOC-FIND-*)" will and flags-mask with word flags, and
;; will skip the word if the result is non-zero
;; default value is "ignore SMUDGE"
$uservar "(VOC-FIND-FLAGS-MASK)" ua_ofs_vocfind_flags_mask FLAG_SMUDGE


: (voc-find-case-sens?)  ( -- flags ) (voc-find-flags-mask) @ (voc-find-case-sensitive) and ; (hidden)
;; returns non-zero (not strictrly true)
: (voc-find-test-mask)  ( wordflags -- skipit ) (voc-find-flags-mask) @ and ;

: voc-find-get-mask  ( mask -- )  (voc-find-flags-mask) w@ ;
: voc-find-add-mask  ( mask -- )  0xffff and (voc-find-flags-mask) or! ;
: voc-find-reset-mask  ( mask -- )  0xffff and (voc-find-flags-mask) ~and! ;
: voc-find-replace-mask  ( mask -- )  (voc-find-flags-mask) w! ;

: voc-find-get-flags  ( flags -- )  (voc-find-flags-mask) @ 0xffff ~and ;
: voc-find-add-flags  ( flags -- )  0xffff ~and (voc-find-flags-mask) or! ;
: voc-find-reset-flags  ( flags -- )  0xffff ~and (voc-find-flags-mask) ~and! ;
: voc-find-replace-flags  ( flags -- )  16 rshift (voc-find-flags-mask) 2+ w! ;


;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: voc-find-str-linear  ( addr count vocid -- cfa -1 // cfa 1 // false )
  nrot 2dup str-name-hash >r rot
  ( addr count lfa | hash )
  begin @ dup while
    ;; check hash
    dup lfa->hfa @ r@ = if
      >r 2dup r@ lfa->nfa id-count
      (voc-find-case-sens?) if s= else s=ci endif
      if ;; i found her!  ( addr count | lfa )
        r@ lfa->ffa ffa@ (voc-find-test-mask)
        ifnot 2drop r@ lfa->cfa r> lfa->ffa ffa@
              (wflag-immediate) and if 1 else -1 endif rdrop exit endif
      endif r>
    endif
  repeat rdrop nrot 2drop
;

 $if WLIST_HASH_BITS
;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: voc-find-str-hashed  ( addr count vocid -- cfa -1 // cfa 1 // false )
    \ (voc-find-str-linear) exit
  nrot 2dup str-name-hash dup >r
  name-hash-fold-mask (voc-header-size-cells) + cells >r rot r> +
  ( addr count bfa | hash )
  begin @ dup while
    ;; check hash
    dup bfa->hfa @ r@ = if
      ;; check name
      >r 2dup r@ bfa->nfa id-count
      (voc-find-case-sens?) if s= else s=ci endif
      if ;; i found her!  ( addr count | hash bfa )
        r@ bfa->ffa ffa@ (voc-find-test-mask)
        ifnot 2drop r@ bfa->lfa lfa->cfa r> bfa->ffa ffa@
              (wflag-immediate) and if 1 else -1 endif rdrop exit endif
      endif r>
    endif
  repeat rdrop nrot 2drop
;

;; returns `1` if cfa is immediate, or `-1` if it is a normal word
: VOC-FIND-STR  ( addr count vocid -- cfa -1 // cfa 1 // false )
  dup vocid-hashed? if ['] voc-find-str-hashed else ['] voc-find-str-linear endif execute-tail
;

$else
;; no hashtables at all
alias voc-find-str-hashed voc-find-str
$endif
