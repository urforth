;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; yep, this is not thread-local
$value "(NUMBER-#-BASE)" 16
(hidden)
$value "(NUMBER-LEADING-SIGN?)" 1
(hidden)


;; converts char to digit, without base checks
: (char->digit)  ( ch -- digit true // false )
  dup case
    [char] 0 [char] 9 bounds-of [char] 0 - true endof
    [char] A [char] Z bounds-of [char] A - 10 + true endof
    [char] a [char] z bounds-of [char] a - 10 + true endof
    drop false swap  ;; so ch will be dropped
  endcase
; (hidden)

;; converts the ascii character c (using base n1) to its binary equivalent n2,
;; accompanied by a true flag. if the conversion is invalid, leaves only a false flag.
: digit  ( c n1 -- n2 true // false )
  dup +if
    swap (char->digit) ifnot drop false  ;; invalid digit
    else 2dup <= if 2drop false else nip true endif
    endif
  else 2drop false endif  ;; negative or zero base
;

: digit?  ( ch base -- flag )
  digit dup if nip endif
;


;; convert the ASCII text beginning at addr with regard to BASE.
;; the new value is accumulated into unsigned number u0, being left as u1.
;; addr1 and count1 are unparsed part of the string
;; will never read more than count bytes
;; doesn't skip any spaces, doesn't parse prefixes and signs
;; but skips '_'
: number-parse-simple  ( addr count u0 -- addr1 count1 u1 )
  over +if
    >r  ( addr count | u )
    ;; first must be a digit
    over c@ base @ digit? if
      ;; main loop
      begin dup while  ( addr count | u )
        over c@
        dup [char] _ = if drop  ;; skip '_'
        else  ;; try digit
          base @ digit ifnot break endif
          r> base @ u* + >r
        endif
        /char
      repeat
    endif
    r>
  endif
;


;; convert the ASCII text beginning at addr with regard to BASE.
;; the new value is accumulated into unsigned double number ud0, being left as ud1.
;; addr1 and count1 are unparsed part of the string
;; will never read more than count bytes
;; doesn't skip any spaces, doesn't parse prefixes and signs
;; but skips '_'
: dnumber-parse-simple  ( addr count ud0lo ud0hi -- addr1 count1 ud1lo ud1hi )
  2over nip +if
    2>r  ( addr count | ud )
    ;; first must be a digit
    over c@ base @ digit? if
      ;; main loop
      begin dup while  ( addr count | u )
        over c@
        dup [char] _ = if drop  ;; skip '_'
        else  ;; try digit
          base @ digit ifnot break endif
          2r> base @ uds* rot u>d d+ 2>r
        endif
        /char
      repeat
    endif
    2r>
  endif
;

: >number  ( ud1 c-addr1 u1 -- ud2 c-addr2 u2 )
  2swap dnumber-parse-simple 2swap
;


;; simple sigils
: number-parse-pfx-sigil  ( addr count -- addr count newbase )
  dup -0if 0 exit endif
  over c@ case
    [char] $ of 16 endof
    [char] # of (number-#-base) dup 1 37 within ifnot drop 0 endif endof
    [char] % of 2 endof
    0 swap
  endcase
  dup if >r /char r> endif
;

: (number-base-char?)  ( char checkdigit hexchar -- newbase // 0 )
  swap if over base @ digit? if 2drop 0 exit endif endif
  >r upcase-char
  case
    r> of 16 endof
    [char] O of 8 endof
    [char] B of 2 endof
    [char] D of 10 endof
    0 swap
  endcase
;

;; checkdigit: check if for a valid digit in a current base (not needed for "&...")
;; hexchar: character for hexadecimal (they're different for "&..." and "0...")
: (number-parse-pfx-0&)  ( addr count checkdigit hexchar -- addr count newbase )
  2>r over 1+ c@ 2r> (number-base-char?)
  dup if >r /2chars r> endif
; (hidden)

: number-parse-pfx-0x&  ( addr count -- addr count newbase )
  dup 2 > ifnot 0 exit endif
  over c@ case
    [char] 0 of true [char] X endof
    [char] & of false [char] H endof
    0 swap
  endcase
  dup if (number-parse-pfx-0&) endif
;

;; check suffixes
: number-parse-sfx  ( addr count -- addr count newbase )
  dup 2 > ifnot 0 exit endif
  2dup + 1- c@ true [char] H (number-base-char?)
  dup if swap 1- swap endif
;


;; will return base according to prefix/suffix, and remove pfx/sfx from the string
;; returns 0 if no special base change found
: number-parse-pfx-sfx  ( addr count -- addr count newbase )
  number-parse-pfx-sigil ?dup if exit endif
  number-parse-pfx-0x& ?dup if exit endif
  number-parse-sfx
;

: number-check-sign  ( addr count -- addr count negflag )
  false >r
  dup +if
    over c@ case
      [char] - of /char rdrop true >r endof
      [char] + of /char endof
    endcase
  endif
  r>
;

;; parse prefix/suffix, and possible sign
;; count is non-zero
;; newbase can be zero if it isn't changed
: number-parse-pfx-sfx-sign  ( addr count -- addr count negflag newbase )
  (number-leading-sign?) if
    number-check-sign >r number-parse-pfx-sfx r> swap
  else  ;; ans/2012 idiocity
    number-parse-pfx-sfx >r number-check-sign r>
  endif
; (hidden)


;; convert a character string left at addr to a signed number, using the current numeric base
: number  ( addr count -- n true // false )
  dup 0<= if 2drop false exit endif  ;; check length
  ;; ok, we have at least one valid char
  number-parse-pfx-sfx-sign
  base @ >r ?dup if base ! endif
  nrot  ( negflag addr count )
  ;; zero count means "nan"
  dup 0> ifnot  ;; no number chars besides a prefix
    r> base !   ;; restore base
    2drop drop false exit  ;; exit with failure
  endif
  0 number-parse-simple r> base !  ( negflag addr count u )
  ;; if not fully parsed, it is nan
  swap if drop 2drop false exit endif
  ( negflag addr u )
  nip swap if negate endif
  true
;


;; compares ch with delimiter delim
;; for BL delimiter, coerces all control chars to space
;; will be called only when c is BL
: (parsebl=)  ( delim ch -- flag )  nip bl <= ; (hidden)

;; scans TIB, returns parsed word
;; doesn't do any copying
;; trailing delimiter is skipped
: (parse)  ( c skip-leading-delim? -- addr count )
  swap 0xff and ?dup ifnot bl endif swap  ;; protection for zero delimiter
  over bl = if ['] (parsebl=) else ['] = endif >r  ;; get compare word cfa
  ;; skip leading delimiters?
  if begin dup tib-peekch r@ execute while tib-getch not-until endif
  >in @  swap 0  ;; save starting position, push dummy char
  begin ( delim oldch ) drop tib-getch dup while ( delim ch ) 2dup r@ execute until
  rdrop >r drop  ;; we need to subtract 1 if last char is not zero
  >in @ over - r> if 1- 0 max endif swap tib @ + swap
    \ endcr over .hex8 space dup . 2dup type space (tib-last-read-char@) . cr
; (hidden)


: parse-skip-blanks  ( -- )  begin tib-peekch ?dup while bl <= while tib-skipch repeat ;
: parse-skip-blanks-no-eol  ( -- )  begin tib-peekch ?dup while dup bl > swap nl = or not-while tib-skipch repeat ;
\ this doesn't skip EOL itself
: parse-skip-to-eol  ( -- )  (tib-last-read-char@) nl = ifnot begin tib-peekch ?dup while nl <> while tib-skipch repeat endif ;

;; multiline comment
;; (* .... *) -- opening eaten
: skip-comment-multiline  ( -- )  begin tib-getch ?dup while [char] * = tib-peekch [char] ) = and until tib-skipch ;

;; nested multiline comment
;; (( .... )) -- opening eaten
: skip-comment-multiline-nested  ( -- )
  1  ;; current comment level
  begin tib-getch ?dup while
    8 lshift tib-peekch or case
      0x2828 of tib-skipch 1+ endof  ;; ((
      0x2929 of tib-skipch 1- endof  ;; ))
    endcase
  dup not-until
  drop
;


: (parse-skip-comments-eol)  ( -- )
  tib-skipch  ;; setup (tib-last-read-char)
  parse-skip-to-eol
; (hidden)

: (parse-skip-comments-multi)  ( skipcfa -- )
  tib-skipch tib-skipch  ;; skip starting chars
  execute-tail
; (hidden)

: parse-skip-comments  ( -- )
  begin
    parse-skip-blanks
    false  tib-peekch 8 lshift  1 tib-peekch-n or case
      0x3b3b of (parse-skip-comments-eol) endof  ;; ;;
      0x2f2f of (parse-skip-comments-eol) endof  ;; //
      0x282a of ['] skip-comment-multiline (parse-skip-comments-multi) endof  ;; (*
      0x2828 of ['] skip-comment-multiline-nested (parse-skip-comments-multi) endof  ;; ((
      otherwise 2drop true
    endcase
  until
;


: (word-or-parse)  ( c skip-leading-delim? -- here )
  (parse)
  1020 umin             ;; truncate length
  dup here !            ;; set counter
  here cell+ swap move  ;; copy string
  here count + 0c!      ;; put trailing zero byte
  here
; (hidden)

;; WARNING! it is using "HERE", so "DP-TEMP" is in effect
;; artificial word length limit: 1020 chars
;; longer words will be properly scanned, but truncated
;; adds trailing zero after the string (but doesn't include it in count)
;; string is cell-counted
: word  ( c  -- wordhere )  true (word-or-parse) ;
: parse-to-here  ( c -- wordhere )  false (word-or-parse) ;

: parse  ( c -- addr count )  false (parse) ;  ;; parse, don't skip leading delimiters
: parse-name  ( -- addr count )  bl true (parse) ;  ;; parse, skip leading delimiters
