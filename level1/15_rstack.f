;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

code: RP@  ( -- rp )
  push  TOS
  mov   TOS,ERP
  urnext
endcode

code: RP!  ( n -- )
  mov   ERP,TOS
  pop   TOS
  urnext
endcode

code: RP0!  ( -- )
  mov   ERP,ts:[ua_ofs_rp0]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: RDUP  ( -- | n -- n n )
  peekr eax
  pushr eax
  urnext
endcode

code: RDROP ( -- | n -- )
  dropr
  urnext
endcode

code: >R  ( n -- | -- n )
  pushr TOS
  pop   TOS
  urnext
endcode

code: R>  ( -- n | n -- )
  push  TOS
  popr  TOS
  urnext
endcode

code: R@  ( -- n | n -- n )
  push  TOS
  peekr TOS
  urnext
endcode


code: 2RDROP  ( -- | n0 n1 -- )
  add   ERP,4*2
  urnext
endcode

code: 2>R  ( n0 n1 -- | -- n0 n1 )
  pop   eax   ;; n0
  pushr eax
  pushr TOS
  pop   TOS
  urnext
endcode

code: 2R>  ( -- n0 n1 | n0 n1 -- )
  push  TOS
  popr  TOS
  popr  eax
  push  eax
  urnext
endcode

code: 2R@  ( -- n0 n1 | n0 n1 )
  push  TOS
  push  dword [ERP+4]
  mov   TOS,dword [ERP]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: RDEPTH  ( -- rstack-depth-before-this-call )
  push  TOS
  ld    TOS,ts:[ua_ofs_rp0]
  sub   TOS,ERP
  sar   TOS,2
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; remove idx, copy item from return stack; 0 RPICK is the same as R@
code: RPICK  ( idx -- n[rtop-idx-1] )
  ld    TOS,[ERP+TOS*4]
  urnext
endcode

alias RPICK RPEEK

;; remove idx and val, set nth item (numbered as in RPICK)
code: RPOKE  ( val idx -- )
  pop   eax
  ld    [ERP+TOS*4],eax
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: NRDROP ( cells -- )
  lea   ERP,[ERP+TOS*4]
  pop   TOS
  urnext
endcode


code: RALLOCA  ( bytes -- addr )
  add   TOS,3
  and   TOS,-4
  sub   ERP,TOS
  ld    TOS,ERP
  urnext
endcode

code: RDEALLOCA  ( bytes -- )
  add   TOS,3
  and   TOS,-4
  add   ERP,TOS
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; local variables support code
;; it can be implemented without asm (and "libs/locals.f" contains such implementation),
;; but i moved it here for speed reasons
;;

;; locals frame:
;;   ...locals
;;   dd prevlocptr  -- locptr points here

code: free-local-frame  ( -- )
  ld    eax,ts:[ua_ofs_locptr]
  ld    edx,[eax]    ;; prevlocptr
  ld    ts:[ua_ofs_locptr],edx
  lea   ERP,[eax+4]  ;; drop local frame
  urnext
endcode
(* reference implementation:
: free-local-frame  ( -- )  (locptr@) dup @ (locptr!) cell+ r> swap rp! >r ;
*)


;; allocate local frame on rstack, copy args, zero uncopied slots
;; totalcells should include one cell for prevlocptr
code: alloc-local-frame  ( argcells totalcells -- )
  lea   edx,[ERP-4]  ;; new locptr
  lea   eax,[TOS*4]
  sub   ERP,eax
  ;; fix prevlocptr
  ld    eax,ts:[ua_ofs_locptr]
  ld    [edx],eax    ;; set prevlocptr
  ld    ts:[ua_ofs_locptr],edx
  ;; clear unused part
  ld    edi,ERP
  pop   edx  ;; argcells
  stc
  sbb   ecx,edx
  jr    z,.noclear
  xor   eax,eax
  rep stosd
.noclear:
  ld    ecx,edx
  jecxz .done
  ld    edx,EIP
  ld    esi,esp
  lea   esp,[esp+ecx*4]
  rep movsd
  ld    EIP,edx
.done:
  pop   TOS
  urnext
endcode
(* reference implementation:
: alloc-local-frame  ( argcells totalcells -- )
  ac tc newlocptr
  rp@ r> 2over ralloca 2drop >r
  (locptr@) over ! dup (locptr!)
  cells erase
  ;; copy args
  ?dup if >r sp@ (locptr@) r@ 1- cells dup swap - swap cmove r> dealloca endif
;
*)


code: (local-addr)  ( cellofs -- addr )
  ld    eax,ts:[ua_ofs_locptr]
  neg   TOS
  lea   TOS,[eax+TOS*4]
  urnext
endcode

code: (local-load)  ( cellofs -- value )
  ld    eax,ts:[ua_ofs_locptr]
  neg   TOS
  lea   TOS,[eax+TOS*4]
  ld    TOS,[TOS]
  urnext
endcode
