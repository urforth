;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; try to use more complex code for CMOVE, so we'll be able to move by dwords?
URFORTH_OPT_CMOVE equ 1


code: @  ( addr -- [addr] ) ;; -- 4 bytes
  mov   TOS,[TOS]
  urnext
endcode

code: W@  ( addr -- [addr] ) ;; -- 2 bytes
  movzx TOS,word [TOS]
  urnext
endcode

code: C@  ( addr -- [addr] ) ;; -- 1 byte
  movzx TOS,byte [TOS]
  urnext
endcode

code: !  ( value addr -- [addr]=value ) ;; -- 4 bytes
  pop   eax   ;; value
  mov   [TOS],eax
  pop   TOS
  urnext
endcode

code: W!  ( value addr -- [addr]=value ) ;; -- 2 bytes
  pop   eax   ;; value
  mov   word [TOS],ax
  pop   TOS
  urnext
endcode

code: C!  ( value addr -- [addr]=value ) ;; -- 1 byte
  pop   eax   ;; value
  mov   byte [TOS],al
  pop   TOS
  urnext
endcode

;; sane little-endian
code: 2@LE  ( addr -- [addr] [addr+4] ) ;; -- 8 bytes
  push  dword [TOS]
  mov   TOS,[TOS+4]
  urnext
endcode

;; sane little-endian
code: 2!LE  ( n0 n1 addr -- [addr]=n0 [addr+4]=n1 ) ;; -- 8 bytes
  pop   eax   ;; d-high
  pop   edx   ;; d-low
  mov   [TOS],edx
  mov   [TOS+4],eax
  pop   TOS
  urnext
endcode

;; ANS big-endian
code: 2@  ( addr -- [addr+4] [addr] ) ;; -- 8 bytes
  push  dword [TOS+4]
  mov   TOS,[TOS]
  urnext
endcode

;; ANS big-endian
code: 2!  ( n0 n1 addr -- [addr]=n1 [addr+4]=n0 ) ;; -- 8 bytes
  pop   eax   ;; d-high
  pop   edx   ;; d-low
  mov   [TOS],eax
  mov   [TOS+4],edx
  pop   TOS
  urnext
endcode


code: 0!  ( addr -- [addr]=0 ) ;; -- 4 bytes
  mov   dword [TOS],0
  pop   TOS
  urnext
endcode

code: 0W!  ( addr -- [addr]=0 ) ;; -- 2 bytes
  mov   word [TOS],0
  pop   TOS
  urnext
endcode

code: 0C!  ( addr -- [addr]=0 ) ;; -- 1 byte
  mov   byte [TOS],0
  pop   TOS
  urnext
endcode


code: 1!  ( addr -- [addr]=1 ) ;; -- 4 bytes
  mov   dword [TOS],1
  pop   TOS
  urnext
endcode

code: 1W!  ( addr -- [addr]=1 ) ;; -- 2 bytes
  mov   word [TOS],1
  pop   TOS
  urnext
endcode

code: 1C!  ( addr -- [addr]=1 ) ;; -- 1 byte
  mov   byte [TOS],1
  pop   TOS
  urnext
endcode


code: +!  ( value addr -- [addr]=value ) ;; -- 4 bytes
  pop   eax   ;; value
  add   [TOS],eax
  pop   TOS
  urnext
endcode

code: +W!  ( value addr -- [addr]=value ) ;; -- 2 bytes
  pop   eax   ;; value
  add   word [TOS],ax
  pop   TOS
  urnext
endcode

code: +C!  ( value addr -- [addr]=value ) ;; -- 1 byte
  pop   eax   ;; value
  add   byte [TOS],al
  pop   TOS
  urnext
endcode


code: -!  ( value addr -- [addr]=value ) ;; -- 4 bytes
  pop   eax   ;; value
  sub   [TOS],eax
  pop   TOS
  urnext
endcode

code: -W!  ( value addr -- [addr]=value ) ;; -- 2 bytes
  pop   eax   ;; value
  sub   word [TOS],ax
  pop   TOS
  urnext
endcode

code: -C!  ( value addr -- [addr]=value ) ;; -- 1 byte
  pop   eax   ;; value
  sub   byte [TOS],al
  pop   TOS
  urnext
endcode


code: 1+!  ( addr -- [addr]=[addr]+1 ) ;; -- 4 bytes
  inc   dword [TOS]
  pop   TOS
  urnext
endcode

code: 1+W!  ( addr -- [addr]=[addr]+1 ) ;; -- 2 bytes
  inc   word [TOS]
  pop   TOS
  urnext
endcode

code: 1+C!  ( addr -- [addr]=[addr]+1 ) ;; -- 1 byte
  inc   byte [TOS]
  pop   TOS
  urnext
endcode


code: 1-!  ( addr -- [addr]=[addr]-1 ) ;; -- 4 bytes
  dec   dword [TOS]
  pop   TOS
  urnext
endcode

code: 1-W!  ( addr -- [addr]=[addr]-1 ) ;; -- 2 bytes
  dec   word [TOS]
  pop   TOS
  urnext
endcode

code: 1-C!  ( addr -- [addr]=[addr]-1 ) ;; -- 1 byte
  dec   byte [TOS]
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
alias +W! W+!
alias +C! C+!
alias -W! W-!
alias -C! C-!

alias 1+W! 1W+!
alias 1+C! 1C+!
alias 1-W! 1W-!
alias 1-C! 1C-!


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: OR!  ( value addr -- [addr]|=value ) ;; -- 4 bytes
  pop   eax
  or    dword [TOS],eax
  pop   TOS
  urnext
endcode

code: XOR!  ( value addr -- [addr]^=value ) ;; -- 4 bytes
  pop   eax
  xor   dword [TOS],eax
  pop   TOS
  urnext
endcode

code: AND!  ( value addr -- [addr]^=value ) ;; -- 4 bytes
  pop   eax
  and   dword [TOS],eax
  pop   TOS
  urnext
endcode

code: ~AND!  ( value addr -- [addr]^=~value ) ;; -- 4 bytes
  pop   eax
  not   eax
  and   dword [TOS],eax
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; byte [addr] ^= value
code: CTOGGLE  ( addr value -- )
  pop   eax
  xor   [eax],cl
  pop   TOS
  urnext
endcode

;; word [addr] ^= value
code: WTOGGLE  ( addr value -- )
  pop   eax
  xor   [eax],cx
  pop   TOS
  urnext
endcode

;; dword [addr] ^= value
code: TOGGLE  ( addr value -- )
  pop   eax
  xor   [eax],TOS
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: CMOVE  ( source dest count -- )
  pop   eax     ;; dest
  pop   edx     ;; src
  jecxz .nothing
  push  esi     ;; this is used as EIP
  mov   esi,edx
  mov   edi,eax
  ;; TOS is ECX, and this is our length too
 $if URFORTH_OPT_CMOVE
.testagain:
  test  edi,3
  jr    z,.okaligned
.alignloop:
  movsb
  dec   ecx
  jr    z,.done
  test  edi,3
  jr    nz,.alignloop
.okaligned:
  ld    eax,ecx
  shr   ecx,2
  jr    z,.skipfast
  rep   movsd
.skipfast:
  and   eax,3
  jr    z,.done
  ld    ecx,eax
  rep   movsb
.done:
 $else
  rep movsb
 $endif
  pop   esi
.nothing:
  pop   TOS
  urnext
endcode

;; can be used to make some room
;; moves from the last byte to the first one
code: CMOVE>  ( source dest count -- )
  pop   eax     ;; dest
  pop   edx     ;; src
  jecxz .nothing
  push  esi     ;; this is used as EIP
  mov   esi,edx
  mov   edi,eax
  ;; TOS is ECX, and this is our length too
  ;; move pointers
  dec   ecx
  add   esi,ecx
  add   edi,ecx
  inc   ecx
  std
  rep movsb
  cld
  pop   esi
.nothing:
  pop   TOS
  urnext
endcode

;; uses CMOVE or CMOVE> (i.e. works like libc `memmove`)
;; negative length does nothing (i.e. you cannot MOVE more that 2GB of data)
: MOVE  ( from to len -- )
  dup -0if drop 2drop exit endif
  >r 2dup = if 2drop rdrop exit endif ;; same destination, nothing to do
  2dup u< if  ;; from < to: may need to use CMOVE> (copy backwards)
    ;; check for overlapping regions (because CMOVE is faster)
    ;; this ignores wraparound, but copying data with wraparound is a bug anyway
    over r@ + over u> if  ;; use "CMOVE>", because regions are overlapping
      r> cmove>
    else  ;; regions aren't overlapping, use normal CMOVE
      r> cmove
    endif
  else  ;; from > to: use CMOVE (normal forward copy)
    r> cmove
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: FILL  ( addr count byte -- )
  movzx eax,cl
  pop   TOS
  pop   edi
  cp    TOS,0
  jr    le,.done
 $if URFORTH_OPT_CMOVE
  ;; fill EAX with AL
  ld    ah,al
  ld    edx,eax
  shl   edx,16
  or    eax,edx
  test  edi,3
  jr    z,.okaligned
.alignloop:
  stosb
  dec   ecx
  jr    z,.done
  test  edi,3
  jr    nz,.alignloop
.okaligned:
  ld    edx,ecx
  shr   ecx,2
  jr    z,.skipfast
  rep stosd
.skipfast:
  and   edx,3
  jr    z,.done
  ld    ecx,edx
 $endif
  rep stosb
.done:
  pop   TOS
  urnext
endcode

: ERASE  ( addr count -- )  0 fill ;
: BLANK  ( addr count -- )  bl fill ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; atomic operations
;; "cas" always returns old value, even if there was no swap
;; "casex" returns "was swap" flag

code: cas-u8  ( cmpvalu8 newvalu8 addr -- oldvalu8 )
  pop   edx     ;; newval
  pop   eax     ;; cmpval
  lock cmpxchg byte [TOS],dl
  movzx TOS,al  ;; old value
  urnext
endcode

code: cas-u16  ( cmpvalu16 newvalu16 addr -- oldvalu16 )
  pop   edx     ;; newval
  pop   eax     ;; cmpval
  lock cmpxchg word [TOS],dx
  movzx TOS,ax  ;; old value
  urnext
endcode

code: cas-u32  ( cmpvalu32 newvalu32 addr -- oldvalu32 )
  pop   edx     ;; newval
  pop   eax     ;; cmpval
  lock cmpxchg dword [TOS],edx
  ld    TOS,eax
  urnext
endcode


code: casex-u8  ( cmpvalu8 newvalu8 addr -- oldvalu8 wasswap )
  pop   edx     ;; newval
  pop   eax     ;; cmpval
  lock cmpxchg byte [TOS],dl
  movzx eax,al  ;; old value
  push  eax
  setz  cl
  movzx TOS,cl
  urnext
endcode

code: casex-u16  ( cmpvalu16 newvalu16 addr -- oldvalu16 wasswap )
  pop   edx     ;; newval
  pop   eax     ;; cmpval
  lock cmpxchg word [TOS],dx
  movzx eax,ax  ;; old value
  push  eax
  setz  cl
  movzx TOS,cl
  urnext
endcode

code: casex-u32  ( cmpvalu32 newvalu32 addr -- oldvalu32 wasswap )
  pop   edx     ;; newval
  pop   eax     ;; cmpval
  lock cmpxchg dword [TOS],edx
  push  eax
  setz  cl
  movzx TOS,cl
  urnext
endcode


code: xchg-u8  ( newvalu8 addr -- oldvalu8 )
  pop   eax     ;; newval
  lock xchg byte [TOS],al
  movzx TOS,al  ;; old value
  urnext
endcode

code: xchg-u16  ( newvalu16 addr -- oldvalu16 )
  pop   eax     ;; newval
  lock xchg word [TOS],ax
  movzx TOS,ax  ;; old value
  urnext
endcode

code: xchg-u32  ( newvalu32 addr -- oldvalu32 )
  pop   eax     ;; newval
  lock cmpxchg dword [TOS],eax
  ld    TOS,eax
  urnext
endcode
