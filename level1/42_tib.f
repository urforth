;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; "TIB" and "#TIB" will be set by the startup code
$uservar "TIB"       ua_ofs_tib 0
$uservar "#TIB"      ua_ofs_tib_size 0
$uservar ">IN"       ua_ofs_in 0
$uservar "TIB-LINE#" ua_ofs_tibline  0

;; "TIB-GETCH" will set this if last read char is not EOL
;; parsing words will set this too, as if they're using "TIB-GETCH"
$uservar "(TIB-LAST-READ-CHAR)" ua_ofs_tiblastchar 0

;; $uservar "(TIB-ERROR-LINE-C4S)" ua_ofa_tib_errline_c4s 0
$variable "(TIB-ERROR-LINE-C4S)" 0
$variable "(TIB-ERROR-LINE->IN)" 0

;; current file we are interpreting
;; c4str
$value "(TIB-CURR-FNAME)" 0
(hidden)

$value "(TIB-ERROR-FNAME)" 0
(hidden)
$value "(TIB-ERROR-LINE#)" 0
(hidden)

$value "(TIB-CURR-FNAME-DEFAULT)" 0
(hidden)

$constant "(#TIB-CURR-FNAME)" 260
(hidden)

;; size of TIB save buffer
$constant "#TIB-SAVE-BUFFER" 5*4


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: TIB-CURR-LINE  ( -- linenum )
  tib-line# @ (tib-last-read-char@) nl = -  \ FIXME: WARNING! assumes that `true` is `1`!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
..: (startup-init)  ( -- )
  (#tib-curr-fname) brk-alloc dup to (tib-curr-fname) to (tib-curr-fname-default)
  (#tib-curr-fname) brk-alloc dup to (tib-error-fname) 0! (tib-curr-fname) 0!
;..


;; returns 0 on out-of-bounds
: tib-at@  ( offs -- ch )
  dup +0if
    dup #tib @ < if
      tib @ ?dup if + c@ exit endif
    endif
  endif drop 0
;

;; replaces 0 with defch
: tib-at@-defch  ( offs defch -- ch )  swap tib-at@ ?dup if nip endif ;


: (tib-free-errline)  ( -- )
  (tib-error-line-c4s) @ ?dup if
    4096 os:munmap drop (tib-error-line-c4s) 0!
    (tib-error-line->in) 0!
  endif
;

: (tib-clear-error)  ( -- )
  (tib-error-fname) 0! 0 to (tib-error-line#)
  (tib-free-errline)
;

: (tib-set-fname)  ( addr count -- )
  0 (#tib-curr-fname) clamp (tib-curr-fname) c4s:copy-counted
  (tib-clear-error)
; (hidden)

: tib-pos>bol  ( pos -- pos )
  0 max begin dup 1- nl tib-at@-defch nl <> while 1- repeat
;

: tib-pos>eol  ( pos -- pos )
  0 max begin dup nl tib-at@-defch nl <> while 1+ repeat
;

;; copy current TIB line to error buffer
: (tib-line>errline)  ( -- )
  tib @ if
    >in @ (tib-last-read-char@) nl = if 1- endif 0 #tib @ clamp
    dup tib-pos>bol >in @ over - (tib-error-line->in) !
    swap tib-pos>eol over - 0 4090 clamp
    (tib-error-line-c4s) @ ?dup ifnot
      4096 os:prot-r/w os:mmap ifnot drop exit endif
      dup (tib-error-line-c4s) !
    endif
    rot tib @ + nrot c4s:copy-counted
  else  ;; tib is empty
    (tib-free-errline)
  endif
;

: (tib-fname>error-fname)  ( -- )
  (tib-line>errline)
  (tib-curr-fname) ?dup ifnot (tib-error-fname) ?dup if 0! endif exit endif
  (tib-error-fname) ?dup ifnot drop exit endif
  c4s:copy
  tib-line# @ dup if (tib-last-read-char@) nl = if 1- 1 max endif endif
  to (tib-error-line#)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (tib-type-highlighted-line)  ( addr count hipos -- )
  over +if
    ." \x1b[4m<"
    >r over r> + nrot bounds do
      dup i = if ." \x1b[7m" endif
      i c@ safe-emit-printable
      dup i = if ." \x1b[27m" endif
    loop
    ." >\x1b[24m"
  else
    2drop
  endif
  drop
; (hidden)

: (tib-type-error-line)  ( -- )
  (tib-error-line-c4s) @ ?dup if count (tib-error-line->in) @ (tib-type-highlighted-line) endif
; (hidden)

: (tib-type-curr-line)  ( -- )
  #tib @ ifnot exit endif
  >in @ (tib-last-read-char@) nl = if 1- endif
  0 #tib @ clamp
  dup tib-pos>bol >in @ over - >r
  swap tib-pos>eol over - 0 4090 clamp
  swap tib @ + swap r> (tib-type-highlighted-line)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; by default, threads has no allocated tib
;; call this to ensure that the tib is allocated
: tib-allocate-default  ( -- )
  (default-tib) @ ifnot
    (default-#tib) cell+ brk-alloc (default-tib) !
    tib @ ifnot (default-#tib) #tib ! (default-tib) @ tib ! endif
    tib @ dup 0! (default-#tib) + 0!
  endif
;

;; this doesn't reset line
: (tib-set-to)  ( addr count -- )
  #tib ! tib ! >in 0! bl (tib-last-read-char) !
;

;; this resets line
: tib-set-to  ( addr count -- )
  (tib-set-to) tib-line# 0!
;

: TIBSTATE-SAVE  ( bufaddr -- )
  tib @ over ! cell+
  #tib @ over ! cell+
  >in @ over ! cell+
  tib-line# @ over ! cell+
  (tib-last-read-char@) swap !
;

: TIBSTATE-RESTORE  ( bufaddr -- )
  dup @ tib ! cell+
  dup @ #tib ! cell+
  dup @ >in ! cell+
  dup @ tib-line# ! cell+
  @ (tib-last-read-char) !
;


: TIBSTATE>R  ( -- | -- savedtibstate )
  r> #tib-save-buffer ralloca tibstate-save >r
;

: R>TIBSTATE  ( -- | savedtibstate -- )
  r> rp@ tibstate-restore #tib-save-buffer rdealloca >r
;

: RDROP-TIBSTATE  ( -- | savedtibstate -- )
  r> #tib-save-buffer rdealloca >r
;

: SAVE-INPUT  ( -- ... n )
  tib @ #tib @ >in @ tib-line# @ (tib-last-read-char@) 5
;

: RESTORE-INPUT  ( ... 5 -- )
  5 <> ERR-INVALID-INPUT-SIZE ?error
  (tib-last-read-char) ! tib-line# ! >in ! #tib ! tib !
;

: DROP-SAVED-INPUT  ( ... 5 -- )
  5 <> ERR-INVALID-INPUT-SIZE ?error
  2drop 2drop drop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: TIB-DEFAULT?  ( -- bool )
  tib @ (default-tib) @ =
;

;; reset TIB to the default one
: TIB-RESET ( -- )
  (default-tib) @ dup tib !
  if (default-#tib) else 0 endif #tib !
  >in 0!
  tib-line# 0!
  bl (tib-last-read-char) !
  ;;k8: no, don't clear it; it will be cleared by tloader in abort sequence
  ;; 0 to tloader:last-include-dir-c4s
  (tib-curr-fname-default) to (tib-curr-fname)
;


code: tib-in^  ( -- addr )
  push  TOS
  ld    TOS,ts:[ua_ofs_tib]
  add   TOS,ts:[ua_ofs_in]
  urnext
endcode


code: (tib-last-read-char@)  ( -- ch )
  push  TOS
  ld    TOS,ts:[ua_ofs_tiblastchar]
  urnext
endcode

;; will never be negative
;; 0 means "END-OF-TIB"
code: TIB-PEEKCH  ( -- ch-or-0 )
  push  TOS
  xor   TOS,TOS
  ld    eax,ts:[ua_ofs_in]
  cp    eax,ts:[ua_ofs_tib_size]
  jr    nc,.done
  add   eax,ts:[ua_ofs_tib]
  movzx TOS,byte [eax]
.done:
  urnext
endcode

;; will never be negative
;; 0 means "END-OF-TIB"
code: TIB-PEEKCH-N  ( chofs -- ch-or-0 )
  ld    eax,TOS
  xor   TOS,TOS
  add   eax,ts:[ua_ofs_in]
  jr    s,.done
  cp    eax,ts:[ua_ofs_tib_size]
  jr    nc,.done
  add   eax,ts:[ua_ofs_tib]
  movzx TOS,byte [eax]
.done:
  urnext
endcode

;; will never be negative
;; 0 means "END-OF-TIB" (and doesn't advance >IN)
code: TIB-GETCH  ( -- ch-or-0 )
  push  TOS
  xor   TOS,TOS
  ld    eax,ts:[ua_ofs_in]
  cp    eax,ts:[ua_ofs_tib_size]
  jr    nc,.done
  ld    edx,eax
  inc   edx
  add   eax,ts:[ua_ofs_tib]
  movzx TOS,byte [eax]
  ;; zero always ends the input
  jecxz .done
  ld    ts:[ua_ofs_tiblastchar],TOS
  ld    ts:[ua_ofs_in],edx
  ;; update current line
  cp    cl,10
  jr    nz,.done
  ld    eax,ts:[ua_ofs_tibline]
  test  eax,eax
  jr    z,.done
  inc   eax
  ld    ts:[ua_ofs_tibline],eax
.done:
  urnext
endcode

: tib-skipch  ( -- )  tib-getch drop ;

;; -1 means "buffer overflow"
: (STD-ACCEPT)  ( addr maxlen fromrefill -- readlen // -1 )
  drop  ;; we can't do anything with "fromrefill" flag here
  0  ( addr maxlen currcount )
  begin key
    ;; eof or cr or lf?
    dup -1 = over 10 = or over 13 = or
  not-while  >r ( addr maxlen currcount | char )
    ;; can we put it?
    2dup > if  ;; yep, store
      rot r> over c! 1+ nrot 1+
    else  ;; nope
      rdrop  ;; drop char, we have no room for it
      ;; need a bell?
      2dup = if bell 1+ ( no more bells) endif
    endif
  repeat
  ( addr maxlen currcount char )
  +0if reset-emitcol ( because OS did cr -- i hope ) endif
  ;; check for overflow
  2dup < if 2drop drop -1 ( oops, overflow ) else nrot 2drop endif
; (hidden)

;; -1 means "buffer overflow"
( addr maxlen fromrefill -- readlen // -1 )
$defer "(ACCEPT)"  cfa "(STD-ACCEPT)"


;; -1 means "buffer overflow"
: ACCEPT  ( addr maxlen -- readlen // -1 )
  dup 0 <= err-input-too-long ?error
  false (accept)
;

;; either refills TIB and sets flag to true, or does nothing and sets flag to false
: REFILL  ( -- flag )
  tib-default? ifnot
    false
  else
    begin
      tib @ #tib @ 1- true (accept)
      dup 0<
    while
      drop
      endcr ." ERROR: ACCEPT buffer overflow\n"
    repeat
      ;;tib @ over type 124 emit dup . cr
    ;; put trailing zero
    tib @ + 0c!
    >in 0!
    true
  endif
;
