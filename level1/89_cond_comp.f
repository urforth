;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary (concomp-flow)  (hidden)
voc-set-active (concomp-flow)

: [IF]  ( level -- newlevel )  1+ ;
alias [IF] [IFNOT]
alias [IF] [IFDEF]
alias [IF] [IFNDEF]

: [ELSE] ( level -- newlevel ) 1- dup if 1+ endif ;

: [ENDIF]  ( level -- newlevel )  1- ;
alias [ENDIF] [THEN]

voc-set-active forth

: [ELSE]  ( -- )
  1 ;; level
  begin
    begin
      parse-skip-comments parse-name dup
      ifnot refill not ERR-UNBALANCED-IFDEF ?error endif
    dup until
    vocid: (concomp-flow) voc-search-noimm if execute else 2drop endif
  dup not-until drop
; immediate


: [ENDIF]  ( -- )  ; immediate
alias [ENDIF] [THEN]


: [IF]  ( cond -- )
  ifnot [compile] [ELSE] endif
; immediate

: [IFNOT]  ( cond -- )
  if [compile] [ELSE] endif
; immediate

: [IFDEF]  ( -- )  \ word
  parse-name has-word? ifnot [compile] [ELSE] endif
; immediate

: [IFNDEF]  ( -- )  \ word
  parse-name has-word? if [compile] [ELSE] endif
; immediate

: [DEFINED]  ( -- flag )  \ word
  parse-name has-word?
; immediate

: [UNDEFINED]  ( -- flag )  \ word
  parse-name has-word? not
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sorry, i should find a better place for this
$value "(.lib-info)" 0
(hidden)

$value "(.lib-sttime)" 0
(hidden)

$value "(.lib-used)" 0
(hidden)

;;$value "(.lib-tmp-mark)" 0
;; 1024 brk-alloc constant (.lib-info)

: (lib-current.)  ( -- )
  (.lib-info) ?dup if
    4 +cells count
    dup 0> over 1020 <= and if
      safe-type
    else
      2drop ." ???"
    endif
  else
    ." ???"
  endif
; (hidden)

: (.lib-put-cell)  ( addr value -- addr+4 )
  over ! cell+
;  (hidden)

: .LIB-START"  ( -- sttime stunused )  ;;"
  ;; (.lib-info) ifnot 1024 brk-alloc to (.lib-info) endif
  34 parse 1020 umin  ;; (.lib-info) c4s:COPY-counted
  dup 5 +cells simple-malloc throw dup >r
  r> over >r  ;; save buffer start
  (.lib-put-cell)               ;; mark
  (.lib-info) (.lib-put-cell)   ;; prev info pointer
  ;; update info pointer
  r> to (.lib-info)
  (.lib-sttime) (.lib-put-cell) ;; old sttime
  (.lib-used) (.lib-put-cell)   ;; old used
  ( addr count cbufptr )
  over (.lib-put-cell)          ;; counter
  swap cmove                    ;; libname string
  TLOAD-VERBOSE-LIBS 1 > if
    ." *** compiling library \`" (lib-current.) ." \`\n"
  endif
  unused to (.lib-used)
  os:gettickcount to (.lib-sttime)
;


: .LIB-END  ( -- )  ;;"
  TLOAD-VERBOSE-LIBS if
    os:gettickcount
    ." *** compiled library \`" (lib-current.) ." \`, size is "
    (.lib-used) unused - . ." bytes, "
    (.lib-sttime) - . ." msecs\n"
  endif
  ;; restore previous
  (.lib-info) 2 +cells @ to (.lib-sttime)
  (.lib-info) 3 +cells @ to (.lib-used)
  (.lib-info) cell+ @   ;; old pointer
  (.lib-info) @ simple-free throw
  to (.lib-info)
;
