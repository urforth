0 value show-details?

: bucket-count  ( -- bkcount )  1 wlist-hash-bits lshift ;

0 var word-count
0 value vocid
0 value buckets
bucket-count cells buffer: bucket-items

: count-bucket-items  ( bkptr -- )
  0 swap
  begin
    @ ?dup
  while
    swap 1+ swap
  repeat
;

: setup-word-count  ( vocid -- )
  word-count 0!
  [: drop word-count 1+! false ;] foreach-word drop
;

: setup-buckets-info  ( vocid -- )
  vocid->htable to buckets
  bucket-items bucket-count cells erase
  bucket-count 0 do
    i cells buckets + count-bucket-items
    i cells bucket-items + !
  loop
;

: voc-setup  ( vocid -- )
  to vocid
  vocid setup-word-count
  vocid setup-buckets-info
;


: has-bucket-with?  ( count -- flag )
  bucket-count 0 do
    i cells bucket-items + @ over = if drop true unloop exit endif
  loop
  drop false
;

: show-details  ( -- )
  0
  bucket-count 0 do  i cells bucket-items + @ max  loop
  1 swap do
    i has-bucket-with? if
      endcr
      i 3 .r ." : "
      bucket-count 0 do
        i cells bucket-items + @ j = if i . endif
      loop
    endif
    -1
  +loop
;


: show-stats  ( vocid -- )
  voc-setup

  endcr
  ." VOC: " vocid voc.
  ."  -- " word-count @ . ." words, "

  0 0x7fff_ffff  \ max and min in bucket
  0  \ buckets used
  bucket-count 0 do
    i cells bucket-items + @
    ?dup if
      >r
      1+  \ update total used
      rot r@ max
      rot r@ min
      rot
      rdrop
    endif
  loop
  ;; ( bkmin bkmax bkused )
  nrot 2dup 2>r rot  \ for average
  0 .r ." /" bucket-count . ." buckets, " . ." min, " . ." max, average per bucket: "
  \ show average
  2r> + 2/ 0 .r cr

  show-details? if show-details endif
;


: ?show-stats  ( addr count -- )
  wfind if
    voc-cfa->vocid show-stats
  endif
;

cli-arg-next argv-str s" --details" s=ci to show-details?


s" forth" ?show-stats
s" disx86" ?show-stats
s" asmx86" ?show-stats
s" asmx86:lexer" ?show-stats
s" asmx86:instructions" ?show-stats
s" asm-labman" ?show-stats
s" asm-meta" ?show-stats

.stack
bye
