;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_var "BASE",base,10

urword_var "(HLD)",vholdptr,0
urword_hidden


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "HEX",hex
;; ( -- )
  mov   eax,16
  mov   [fvar_base_data],eax
  urnext
urword_end

urword_code "DECIMAL",decimal
;; ( -- )
  mov   eax,10
  mov   [fvar_base_data],eax
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "HOLD",hold
;; ( ch -- )
  UF vholdptr @ 1dec dup vholdptr ! cpoke
urword_end

urword_forth "SIGN",sign
;; ( n -- )
  UF 0 less
  ur_if
    UF 45 hold
  ur_endif
urword_end

urword_forth "<#",numopen
;; ( d -- d )
  UF pad vholdptr !
urword_end

urword_forth "<#U",numopenn
;; ( n -- d )
  UF 0 numopen
urword_end

urword_forth "#>",numclose
;; ( d -- addr count )
  UF 2drop vholdptr @ pad over -
urword_end

urword_forth "#",numdigit
;; ( d -- n )
  UF base @ udsdivmod dup 9 great
  ur_if
    UF 7 +
  ur_endif
  UF 48 + hold
urword_end

urword_forth "#S",numalldigits
;; ( d -- d )
  ur_begin
    UF numdigit
    ;;UF 2dup d0equ
    UF 2dup or 0equal
  ur_until
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth ".R",dotr
;; ( n fldlen -- )
  UF rpush dup rpush abs numopenn numalldigits rpop 0x80000000 uless
  ur_ifnot
    UF 45 hold
  ur_endif
  UF numclose
  ;; ( addr count )
  UF rpop over - spaces type
urword_end

urword_forth "U.R",udotr
;; ( u fldlen -- )
  UF rpush numopenn numalldigits numclose
  ;; ( addr count )
  UF rpop over - spaces type
urword_end

urword_forth ".",dot
;; ( n -- )
  UF 0 dotr space
urword_end

urword_forth "U.",udot
;; ( n -- )
  UF 0 udotr space
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth ".HEX8",dothex8
;; ( n -- )
  UF base @ rpush hex
  UF numopenn numdigit numdigit numdigit numdigit numdigit numdigit numdigit numdigit numclose type
  UF rpop base !
urword_end
