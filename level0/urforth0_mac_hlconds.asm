;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urcond_popval = 0
;; virtual stack size, in bytes
urcond_stack_size = 256  ;; should be enough for everyone

virtual at 0
urforth_conctl::
  dd  urcond_stack_size  ;; this is SP
  rb  urcond_stack_size
  rd  16                 ;; just in case
end virtual


macro urcond_check_balance {
  local csp
  load csp dword from urforth_conctl:0
  if csp <> urcond_stack_size
    display "***",10
    display "*** UNBALANCED CONDITIONALS IN WORD: "
    display urforth_last_word_name
    display 10,"***",10
    err "unbalanced conditionals"
  end if
}

macro urcond_drop {
  local csp
  load csp dword from urforth_conctl:0
  if csp = urcond_stack_size
    err "cond stack underflow"
  end if
  csp = csp+4
  store dword csp at urforth_conctl:0
}

macro urcond_pop {
  local csp,vv
  load csp dword from urforth_conctl:0
  if csp = urcond_stack_size
    err "cond stack overflow"
  end if
  load vv dword from urforth_conctl:csp
  csp = csp+4
  store dword csp at urforth_conctl:0
  urcond_popval = vv
}

macro urcond_push n {
  local csp
  load csp dword from urforth_conctl:0
  if csp = 4
    err "cond stack overflow"
  end if
  csp = csp-4
  store dword n at urforth_conctl:csp
  store dword csp at urforth_conctl:0
}

macro urcond_swap {
  local csp
  local v0,v1
  load csp dword from urforth_conctl:0
  if csp > urcond_stack_size-8
    err "cond stack underflow (swap)"
  end if
  load v0 dword from urforth_conctl:csp
  csp = csp+4
  load v1 dword from urforth_conctl:csp
  store dword v0 at urforth_conctl:csp
  csp = csp-4
  store dword v1 at urforth_conctl:csp
}

macro urcond_over {
  local csp
  local v
  load csp dword from urforth_conctl:0
  if csp+4 = urcond_stack_size
    err "cond stack underflow (swap)"
  end if
  csp = csp+4
  load v dword from urforth_conctl:csp
  urcond_push v
}

macro urcond_here {
  urcond_push $
}

macro urcond_poke {
  local addr,value

  ;;local csp
  ;;load csp dword from urforth_conctl:0
  ;;if csp+8 > urcond_stack_size
  ;;  err "cond stack underflow (poke)"
  ;;end if
  ;;load addr dword from urforth_conctl:csp
  ;;csp = csp+4
  ;;load value dword from urforth_conctl:csp
  ;;csp = csp+4
  ;;store dword csp at urforth_conctl:0

  urcond_pop
  addr = urcond_popval
  urcond_pop
  value = urcond_popval

  store dword value at addr
}

macro urcond_comma {
  local n
  urcond_pop
  n = urcond_popval
  dd  n
}

macro urcond_add delta {
  local n
  load csp dword from urforth_conctl:0
  if csp = urcond_stack_size
    err "cond stack overflow"
  end if
  load n dword from urforth_conctl:csp
  n = n+delta
  store dword n at urforth_conctl:csp
}

macro urcond_compback {
  local n
  urcond_pop
  n = urcond_popval
  dd  n
}


macro urcond_compfwd {
  urcond_here
  urcond_swap
  urcond_poke
}

macro urcond_pairs pval {
  local n
  ;load n dword from urfhi_csp
  ;urfhi_csp = urfhi_csp+4
  urcond_pop
  n = urcond_popval
  if n <> pval
    err "unbalanced UrForth conditionals!"
  end if
}


macro ur_begin {
  urcond_here
  urcond_push 1
}

macro ur_until {
  urcond_pairs 1
  urcall 0branch
  urcond_compback
}

macro ur_again {
  urcond_pairs 1
  urcall branch
  urcond_compback
}

macro ur_endif {
  urcond_pairs 2
  urcond_compfwd
}

macro ur_then {
  urendif
}

macro ur_if {
  urcall 0branch
  urcond_here
  urcond_push 0
  urcond_comma
  urcond_push 2
}

macro ur_ifnot {
  urcall tbranch
  urcond_here
  urcond_push 0
  urcond_comma
  urcond_push 2
}

macro ur_else {
  urcond_pairs 2
  urcall branch
  urcond_here
  urcond_push 0
  urcond_comma
  urcond_swap
  urcond_push 2
  ur_endif
  urcond_push 2
}

macro ur_while {
  ur_if
  urcond_add 2
}

macro ur_not_while {
  ur_ifnot
  urcond_add 2
}

macro ur_repeat {
  local v0,v1
  ; >r >r
  ;load v0 dword from urfhi_csp
  ;urfhi_csp = urfhi_csp+4
  ;load v1 dword from urfhi_csp
  ;urfhi_csp = urfhi_csp+4
  urcond_pop
  v0 = urcond_popval
  urcond_pop
  v1 = urcond_popval
  ; again
  ur_again
  ; r> r>
  urcond_push v1
  urcond_push v0
  urcond_add -2
  ur_endif
}

macro ur_do {
  urcall  par_do
  urcond_here
  urcond_push 3
}

macro ur_loop {
  urcond_pairs 3
  urcall  par_loop
  urcond_compback
}

macro ur_ploop {
  urcond_pairs 3
  urcall  par_ploop
  urcond_compback
}


macro ur_cblock {
  urcall  cblocklit
  urcond_here
  urcond_push 0
  urcond_comma
  urcond_push 69
  call fword_par_urforth_nocall_doforth
}

macro ur_cblock_end {
  urcall  exit
  urcond_pairs 69
  urcond_compfwd
}
