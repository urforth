;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_value "(DEFAULT-#TIB)",par_default_tib_size,1024
urword_hidden

;; this will be patched by the startup code
urword_value "(DEFAULT-TIB)",par_default_tib,0
urword_hidden


urword_var "TIB",      tib,      0  ;fdata_tib
urword_var "#TIB",     tibsize,  0  ;fdata_tib_end-fdata_tib
urword_var ">IN",      inptr,    0
urword_var "TIB-LINE#",tiblineno,0

;; "TIB-GETCH" will set this if last read char is not EOL
;; parsing words will set this too, as if they're using "TIB-GETCH"
urword_var "(TIB-LAST-READ-CHAR)",par_last_read_char,0

urword_value "(LAST-TLOAD-PATH-ADDR)",par_last_tload_path,0

;; size of TIB save buffer
urword_const "#TIB-SAVE-BUFFER",tib_save_buffer_size,4+4+4+4+4

;; current file we are interpreting
urword_const "(#TIB-CURR-FNAME)",par_tib_curr_fname_size,4096
urword_hidden
;; c4str
urword_value "(TIB-CURR-FNAME)",par_tib_curr_fname,0
urword_hidden


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "TIBSTATE>R",par_tibstate_rpush
  rpushmem [fvar_tib_data]
  rpushmem [fvar_tibsize_data]
  rpushmem [fvar_inptr_data]
  rpushmem [fvar_tiblineno_data]
  rpushmem [fvar_par_last_read_char_data]
  urnext
urword_end

urword_code "R>TIBSTATE",par_tibstate_rpop
  rpopmem  [fvar_par_last_read_char_data]
  rpopmem  [fvar_tiblineno_data]
  rpopmem  [fvar_inptr_data]
  rpopmem  [fvar_tibsize_data]
  rpopmem  [fvar_tib_data]
  urnext
urword_end

urword_code "RDROP-TIBSTATE",par_tibstate_rdrop
  add   ERP,4+4+4+4+4
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "TIB-DEFAULT?",tib_is_default
  UF tib @ par_default_tib equal
urword_end

urword_forth "TIB-RESET",tib_reset
  ; reset TIB
  UF par_default_tib tib !
  UF par_default_tib_size tibsize !
  UF inptr 0poke
  UF tiblineno 0poke
  UF 0
  urto par_last_tload_path
urword_end


if 0
urword_forth "TIB-PEEKCH",tib_peekch
;; ( -- ch-or-0 )
  UF inptr @ tibsize @ uless
  ur_if
    UF tib @ inptr @ + cpeek
    UF qdup
    ur_ifnot
      UF 32
    ur_endif
  ur_else
    UF 0
  ur_endif
urword_end

urword_forth "TIB-GETCH",tib_getch
;; ( -- ch-or-0 )
  UF tib_peekch dup
  ur_if
    UF 1 inptr addpoke
    UF dup 10 equal
    ur_if
      UF tiblineno @
      ur_if
        UF 1 tiblineno addpoke
      ur_endif
    ur_endif
    UF dup par_last_read_char !
  ur_endif
urword_end

else

urword_code "TIB-PEEKCH",tib_peekch
;; ( -- ch-or-0 )
;; will never be negative
  push  TOS
  xor   TOS,TOS
  ld    eax,[fvar_inptr_data]
  cp    eax,dword [fvar_tibsize_data]
  jr    nc,@f
  add   eax,[fvar_tib_data]
  movzx TOS,byte [eax]
  ; convert zero to 32
  ld    eax,32
  or    cl,cl
  cmovz ecx,eax
@@:
  urnext
urword_end

urword_code "TIB-GETCH",tib_getch
;; ( -- ch-or-0 )
;; will never be negative
  push  TOS
  xor   TOS,TOS
  ld    eax,[fvar_inptr_data]
  cp    eax,dword [fvar_tibsize_data]
  jr    nc,@f
  add   eax,[fvar_tib_data]
  movzx TOS,byte [eax]
  ; convert zero to 32
  ld    eax,32
  or    cl,cl
  cmovz ecx,eax
  ; update last read char
  ld    [fvar_par_last_read_char_data],TOS
  ; update position
  inc   dword [fvar_inptr_data]
  ; update current line
  cp    cl,10
  jr    nz,@f
  cp    dword [fvar_tiblineno_data],0
  jr    z,@f
  inc   dword [fvar_tiblineno_data]
@@:
  urnext
urword_end
end if


if 0
urword_forth "TIB-CALC-CURRLINE",tib_calc_currline
;; ( -- linenum-1 )
;; returns line number for the current TIB position
  UF 0
  UF inptr @
  ur_if
    UF inptr @ tib @ +  tib @
    ur_do
      UF i cpeek 10 equal
      ur_if
        UF 1inc
      ur_endif
    ur_loop
  ur_endif
else
urword_code "TIB-CALC-CURRLINE",tib_calc_currline
;; ( -- linenum-1 )
;; returns line number for the current TIB position
  push  TOS
  ld    edi,[fvar_tib_data]
  ld    ecx,[fvar_inptr_data]
  ld    al,10
  xor   edx,edx   ; line counter
@@:
  cp    ecx,1
  jr    l,@f
  repne scasb
  jr    nz,@f
  inc   edx
  jr    @b
@@:
  ld    TOS,edx
  urnext
end if
urword_end


urword_forth "ACCEPT",accept
;; ( addr maxlen -- readlen // -1 )
  UF dup 0 lessequ errid_input_too_long qerror
  UF 0
  ;; ( addr maxlen currcount )
  ur_begin
    UF key
    ; eof or cr or lf?
    UF dup 0xffffffff equal
    UF over 10 equal or
    UF over 13 equal or
    UF not
  ur_while
    ;; ( addr maxlen currcount char )
    UF rpush
    ;; can we put it?
    UF 2dup great
    ur_if
      ;; yep, store
      ;; ( addr maxlen currcount | char )
      UF rot rpop over cpoke 1inc nrot 1inc
    ur_else
      ;; nope
      UF rdrop  ;; drop char, we have no room for it
      ;; need a bell?
      UF 2dup equal
      ur_if
        UF bell
        UF 1inc
      ur_endif
    ur_endif
  ur_repeat
  ;; ( addr maxlen currcount char )
  UF 0xffffffff nequ
  ur_if
    UF par_reset_emitcol  ;; because OS did cr (i hope)
  ur_endif
  ;; check for overflow
  UF 2dup less
  ur_if
    ;; oops, overflow
    UF 2drop drop 0xffffffff
  ur_else
    UF nrot 2drop
  ur_endif
urword_end


urword_forth "REFILL",refill
;; ( -- flag )
;; either refills TIB and sets flag to true, or does nothing and sets flag to false
  UF tib_is_default
  ur_if
    ur_begin
      UF tib @ tibsize @ 1dec accept
      UF dup 0less
    ur_while
      UF drop
      UF endcr pardottype "ERROR: ACCEPT buffer overflow" cr
    ur_repeat
      ;UF tib @ over type 124 emit dup . cr
    ;; put trailing zero
    UF tib @ + 0poke
    UF inptr 0poke
    UF 1
  ur_else
    UF 0
  ur_endif
urword_end
