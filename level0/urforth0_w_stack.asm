;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "SP@",spget
  push  TOS
  mov   TOS,esp
  urnext
urword_end

urword_code "RP@",rpget
  push  TOS
  mov   TOS,ERP
  urnext
urword_end

urword_code "SP!",spset
  mov   esp,TOS
  pop   TOS
  urnext
urword_end

urword_code "RP!",rpset
  mov   ERP,TOS
  pop   TOS
  urnext
urword_end

urword_code "SP0!",spset0
  mov   esp,[fvar_sp0_data]
  xor   TOS,TOS
  urnext
urword_end

urword_code "RP0!",rpset0
  mov   ERP,[fvar_rp0_data]
  urnext
urword_end

urword_code "(SP-CHECK)",par_spcheck
  mov   eax,esp
  cmp   eax,[fvar_sp0_data]
  jr    na,.ok
  mov   esp,[fvar_sp0_data]
  xor   TOS,TOS
  push  TOS
  urnext
.ok:
  push  TOS
  mov   TOS,1
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "RDUP",rdup
  rpeek eax
  rpush eax
  urnext
urword_end

urword_code "RDROP",rdrop
  rdrop
  urnext
urword_end

urword_code ">R",rpush
  rpush TOS
  pop   TOS
  urnext
urword_end

urword_code "R>",rpop
  push  TOS
  rpop  TOS
  urnext
urword_end

urword_code "R@",rpeek
  push  TOS
  rpeek TOS
  urnext
urword_end


urword_code "2RDROP",2rdrop
  add   ERP,4*2
  urnext
urword_end

urword_code "2>R",2rpush
;; ( n0 n1 -- || -- n0 n1 )
  pop   eax   ; n0
  rpush eax
  rpush TOS
  pop   TOS
  urnext
urword_end

urword_code "2R>",2rpop
;; ( -- n0 n1 || n0 n1 -- )
  push  TOS
  rpop  TOS
  rpop  eax
  push  eax
  urnext
urword_end

urword_code "2R@",2rpeek
;; ( -- n0 n1 || n0 n1 )
  push  TOS
  push  dword [ERP+4]
  mov   TOS,dword [ERP]
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "DUP",dup
  push  TOS
  urnext
urword_end

urword_code "2DUP",2dup
;; ( n0 n1 -- n0 n1 n0 n1 )
  mov   eax,[esp]
  ; TOS: n1
  ; EAX: n0
  push  TOS
  push  eax
  urnext
urword_end

urword_code "?DUP",qdup
  or    TOS,TOS
  jz    @f
  push  TOS
@@:
  urnext
urword_end

;; drop if zero
urword_code "?DROP",qdrop
  or    TOS,TOS
  jr    nz,@f
  pop   TOS
@@:
  urnext
urword_end

urword_code "DROP",drop
  pop   TOS
  urnext
urword_end

urword_code "2DROP",2drop
  pop   TOS
  pop   TOS
  urnext
urword_end

urword_code "SWAP",swap
  xchg  [esp],TOS
  urnext
urword_end

urword_code "2SWAP",2swap
;; ( n0 n1 n2 n3 -- n2 n3 n0 n1 )
  ; TOS=n3
  pop   eax       ; EAX=n2
  pop   FREEREG   ; FRG=n1
  xchg  [esp],eax ; EAX=n0
  push  TOS
  push  eax
  mov   TOS,FREEREG
  urnext
urword_end

urword_code "OVER",over
  push  TOS
  mov   TOS,[esp+4]
  urnext
urword_end

urword_code "2OVER",2over
;; ( n0 n1 n2 n3 -- n0 n1 n2 n3 n0 n1 )
  ; TOS=n3
  push  TOS
  mov   eax,[esp+12]
  mov   TOS,[esp+8]
  push  eax
  urnext
urword_end

urword_code "ROT",rot
  pop   FREEREG
  pop   eax
  push  FREEREG
  push  TOS
  mov   TOS,eax
  urnext
urword_end

urword_code "NROT",nrot
  pop   FREEREG
  pop   eax
  push  TOS
  push  eax
  mov   TOS,FREEREG
  urnext
urword_end

urword_alias "-ROT",nrot_alias,nrot


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SWAP DROP
urword_code "NIP",nip
;; ( n1 n2 -- n2 )
  pop   eax
  urnext
urword_end

;; SWAP OVER
urword_code "TUCK",tuck
;; ( n1 n2 -- n2 n1 n2 )
  pop   eax
  push  TOS
  push  eax
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "DEPTH",depth
;; ( -- stack-depth-before-this-call )
  push  TOS
  ld    TOS,[fvar_sp0_data]
  sub   TOS,esp
  sar   TOS,2
  dec   TOS
  urnext
urword_end

urword_code "RDEPTH",rdepth
;; ( -- rstack-depth-before-this-call )
  push  TOS
  ld    TOS,[fvar_rp0_data]
  sub   TOS,ebp
  sar   TOS,2
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "PICK",pick
;; ( ... idx -- ... n[top-idx-1] )
;; remove idx, copy item; 0 PICK is the same as DUP
  ld    TOS,[esp+TOS*4]
  urnext
urword_end

urword_code "ROLL",roll
;; ( ... idx -- ... n[top-idx-1] )
;; remove idx, move item; 0 ROLL is the same as NOOP
  jecxz .quit
  ld    FREEREG,esi
  ld    eax,[esp+TOS*4]
  lea   esi,[esp+TOS*4]
  ld    edi,esi
  sub   esi,4
  std
  rep movsd
  cld
  ld    esi,FREEREG
  ld    [esp],eax
.quit:
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "RPICK",rpick
;; ( ... idx -- ... n[top-idx-1] )
;; remove idx, copy item from return stack; 0 RPICK is the same as R@
  ld    TOS,[ebp+TOS*4]
  urnext
urword_end
