#!/bin/sh

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"
fasm -m 29000 urforth0.asm
res=$?
cd "$odir"
exit $res
