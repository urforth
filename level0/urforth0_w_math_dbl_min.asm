;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; double math words required by the kernel
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "UDS/MOD",udsdivmod
;; ( ud1 u1 --> ud2 u2 )
  ld    edi,TOS
  pop   eax
  pop   ebx
  ;; EDI=u1
  ;; EAX=ud1-high
  ;; EBX=ud1-low
  xor   edx,edx
  div   edi
  xchg  eax,ebx
  div   edi
  push  eax
  push  ebx
  ld    TOS,edx
  urnext
urword_end
