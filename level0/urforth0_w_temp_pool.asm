;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; very simple pool allocator for temporary data
;; allocates bytes from the end of the memory
urword_var "(TEMP-POOL-TOP)",temp_pool_top,0
urword_hidden
urword_const "(TEMP-POOL-BOT-OFS)",temp_pool_bot_ofs,1024
urword_hidden

urword_forth "TEMP-POOL-RESET",temp_pool_reset
;; ( -- )
;; resets temporary pool (i.e. deallocates everything)
  UF dpend @ temp_pool_bot_ofs - temp_pool_top !
urword_end

urword_forth "TEMP-POOL-MARK",temp_pool_mark
;; ( -- addr )
;; returns temp pool "mark", which can be used in "TEMP-POOL-RELEASE"
  UF temp_pool_top @
urword_end

urword_forth "TEMP-POOL-HERE",temp_pool_here
;; ( -- addr )
;; returns temp pool "here", i.e. current top
;; can be used to avoid keeping it elsewhere
  UF temp_pool_top @
urword_end

urword_forth "TEMP-POOL-RELEASE",temp_pool_release
;; ( addr -- )
;; deallocates temp pool bytes
;; aborts on invalid mark values
  ; trying to allocate something instead of freeing it?
  UF dup temp_pool_top @ uless errid_invalid_temp_pool_release qerror
  ; trying to go beyound the end of the pool?
  UF dup dpend @ temp_pool_bot_ofs - ugreat
  UF errid_invalid_temp_pool_release qerror
  ; set this as new pool top
  UF temp_pool_top !
urword_end

urword_forth "TEMP-POOL-ALLOC",temp_pool_alloc
;; ( size -- addr )
;; deallocates temp pool bytes
;; aborts on invalid mark values
  ; sanity check for size
  UF dup 0 less errid_invalid_temp_pool_allocation qerror
  UF dup unused great errid_invalid_temp_pool_allocation qerror
  ; check if we'll have less than 64KB of room between the temp pool and HERE
  UF dup here + 65536 + temp_pool_top @ ugreat errid_out_of_temp_pool qerror
  ; allocate
  UF temp_pool_top subpoke
  ; return the address of the allocated memory
  UF temp_pool_here
urword_end

; for code words

urword_code "(NO-CALL-TEMP-POOL-CODESUBS)",par_no_call_temp_pool_subs
urword_hidden
urword_codeblock
;; OUT: EAX=mark
;; other registers are preserved (except flags)
urpool_mark:
  ld    eax,[fvar_temp_pool_top_data]
  ret

;; IN: EAX=mark
;; OUT: nothing
;; all registers are preserved (except flags)
;; TODO: sanity checks
urpool_release:
  ld    [fvar_temp_pool_top_data],eax
  ret

;; IN: EAX=size
;; OUT: EDI=address
;; all other registers (including EAX) are preserved (except flags)
;; TODO: sanity checks
urpool_alloc:
  ld    edi,[fvar_temp_pool_top_data]
  sub   edi,eax
  jr    c,urpool_alloc_error
  cmp   edi,[fvar_temp_pool_top_data]
  jr    nc,urpool_alloc_error
  ld    eax,[fvar_dp_temp_data]
  or    eax,eax
  cmovz eax,[fvar_dp_data]
  add   eax,65536
  cmp   edi,eax
  jr    c,urpool_alloc_error
  ; success
  ld    [fvar_temp_pool_top_data],edi
  ret

urpool_alloc_error:
  ld    TOS,ERR_OUT_OF_TEMP_POOL
  ld    eax,fword_error
  call  ur_mc_fcall
  ; it will never return
urword_end
