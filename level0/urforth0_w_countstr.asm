;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "ZCOUNT",zcount
;; ( addr -- addr count )
;; length of asciiz string
  push  TOS
  mov   edi,TOS
  xor   al,al
  cp    [edi],al
  jr    nz,@f
  xor   TOS,TOS
  urnext
@@:
  mov   edx,-1
  mov   ecx,edx
  repnz scasb
  sub   edx,ecx
  mov   ecx,edx
  dec   ecx
  urnext
urword_end


urword_alias "COUNT-ONLY",count_only,peek
;; ( addr -- count )

urword_code "COUNT",count
;; ( addr -- addr+4 count )
  ;UF dup count_only swap cellinc swap exit
  ld    eax,[TOS]
  add   TOS,4
  push  TOS
  ld    TOS,eax
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "S=",strequ
;; ( addr0 count0 addr1 count1 -- flag )
  ; compare lengthes
  cp    TOS,[esp+4]
  jr    nz,.lenfail
  or    ecx,ecx
  jr    z,.lenzero
  ; perform memcmp
  pop   edi
  pop   eax
  xchg  esi,[esp]
  ; now EIP is saved on the stack
  repz  cmpsb
  ; restore EIP
  pop   esi
  jcxz  .success_check
  xor   TOS,TOS
  urnext

.success_check:
  jz    .success
  xor   TOS,TOS
  urnext

.lenfail:
  ; early failure
  add   esp,4*3
  xor   TOS,TOS
  urnext

.lenzero:
  ; early success
  add   esp,4*3
.success:
  ld    TOS,1
  urnext
urword_end

; ascii case-insensitive compare
urword_code "S=CI",strequ_ci
;; ( addr0 count0 addr1 count1 -- flag )
  ; compare lengthes
  cp    TOS,[esp+4]
  jr    nz,.lenfail
  or    ecx,ecx
  jr    z,.lenzero
  ; perform memcmp
  pop   edi
  pop   eax
  xchg  esi,[esp]
  ; now EIP is saved on the stack
.cmploop:
  lodsb
  ld    ah,[edi]
  inc   edi
  ; it may work
  cp    al,ah
  jr    nz,.trycase
.caseequ:
  loop  .cmploop
  ; success
  ; restore EIP
  pop   esi
  ld    TOS,1
  urnext

.trycase:
  cp    al,'a'
  jr    c,@f
  cp    al,'z'+1
  jr    nc,@f
  sub   al,32
@@:
  cp    ah,'a'
  jr    c,@f
  cp    ah,'z'+1
  jr    nc,@f
  sub   ah,32
@@:
  cmp   al,ah
  jr    z,.caseequ
  ; failure
  ; restore EIP
  pop   esi
  xor   TOS,TOS
  urnext

.lenfail:
  ; early failure
  add   esp,4*3
  xor   TOS,TOS
  urnext

.lenzero:
  ; early success
  add   esp,4*3
  ld    TOS,1
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "UPCASE-CHAR",upcase_char
;; ( ch -- ch )
  and   TOS,0xff
  cp    cl,'a'
  jr    c,@f
  cp    cl,'z'+1
  jr    nc,@f
  sub   cl,32
@@:
  urnext
urword_end

urword_code "UPCASE-STR",upcase_str
;; ( addr count -- )
  pop   edi
  test  TOS,0x80000000
  jr    nz,fword_upcase_str_done
  or    TOS,TOS
  jr    z,fword_upcase_str_done
fword_upcase_str_loop:
  ld    al,[edi]
  cp    al,'a'
  jr    c,@f
  cp    al,'z'+1
  jr    nc,@f
  sub   byte [edi],32
@@:
  inc   edi
  loop  fword_upcase_str_loop
fword_upcase_str_done:
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; converts some escape codes in-place
;; used for `."` and `"`
;; the resulting string is never bigger that the source one
;; this will not preserve the trailing zero byte
urword_code "STR-UNESCAPE",str_unescape
;; ( addr count -- addr count )
  ;call  .dbg
  jecxz .quit
  ld    edi,[esp]
  push  ecx

.scanloop:
  ld    al,92
  repnz scasb
  jecxz .done  ; nope
  jr    nz,.done
  ; edi is after backslash
  ; ecx is number of chars left after backslash
  ; found backslash, check next char
  ld    al,[edi]
  ; '\r'?
  cp    al,'r'
  jr    nz,@f
  ld    al,13
  jr    .replace_one
@@:
  ; '\n'?
  cp    al,'n'
  jr    nz,@f
  ld    al,10
  jr    .replace_one
@@:
  ; '\t'?
  cp    al,'t'
  jr    nz,@f
  ld    al,9
  jr    .replace_one
@@:
  ; '\b'? (bell)
  cp    al,'b'
  jr    nz,@f
  ld    al,7
  jr    .replace_one
@@:
  ; '\e'? (escape)
  cp    al,'e'
  jr    nz,@f
  ld    al,27
  jr    .replace_one
@@:
  ; '\z'? (zero)
  cp    al,'z'
  jr    nz,@f
  xor   al,al
  jr    .replace_one
@@:
  ; '\`'? (double quote)
  cp    al,'`'
  jr    nz,@f
  ld    al,'"'
  jr    .replace_one
@@:
  ; 'xHH'?
  cp    al,'x'
  jr    z,.esc_hex
  cp    al,'X'
  jr    z,.esc_hex
  jr    .replace_one
.loop_cont:
  loop  .scanloop

.done:
  pop   ecx
.quit:
  urnext

.esc_hex:
  cp    ecx,2
  jr    c,.loop_cont
  ld    al,[edi+1]
  call  .hexdigit
  jr    c,.loop_cont
  ; save original string position
  push  esi
  ld    esi,edi
  ; skip 'x'
  inc   edi   ; skip 'x'
  dec   ecx
  dec   dword [esp+4]
  ; skip first digit
  inc   edi
  dec   ecx
  dec   dword [esp+4]
  jecxz .esc_hex_done
  ld    ah,al
  ld    al,[edi]
  call  .hexdigit
  jr    nc,@f
  ld    al,ah
  jr    .esc_hex_done
@@:
  ; combine two hex digits
  shl   ah,4
  or    al,ah
  ; skip second digit
  inc   edi
  dec   ecx
  dec   dword [esp+4]
.esc_hex_done:
  ld    [esi-1],al
  jecxz .esc_hex_quit
  ; remove leftover chars
  ; ECX: chars left
  ; ESI: position after backslash
  ; EDI: rest position
  ; old ESI is on the stack
  push  esi   ; to be restored in EDI
  push  ecx
  xchg  esi,edi
  rep movsb
  pop   ecx
  pop   edi   ; get back to backslash
  pop   esi   ; restore old ESI
  jr    .scanloop

.esc_hex_quit:
  pop   esi
  jr    .done

.replace_one:
  ld    [edi-1],al
  dec   dword [esp]
  dec   ecx
  jecxz .done
  ; move
  push  esi
  push  edi
  push  ecx
  ld    esi,edi
  inc   edi
.domove:
  xchg  esi,edi
  rep movsb
  pop   ecx
  pop   edi
  pop   esi
  jr    .scanloop

.hexdigit:
  sub   al,'0'
  jr    c,.hexdigit_done
  cp    al,10
  ccf
  jr    nc,.hexdigit_done
  sub   al,7
  jr    c,.hexdigit_done
  cp    al,16
  ccf
  jr    nc,.hexdigit_done
  ; maybe its lowercase?
  cp    al,42
  jr    c,.hexdigit_done
  sub   al,32
  cp    al,16
  ccf
.hexdigit_done:
  ret

.dbg:
  dprint_hex_al
  dprint_char ' '
  dprint_hex ecx
  dprint_cr
  ret
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "STR-TRIM-AFTER-LAST-CHAR",str_trim_after_last_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char
;; `count` will include char
;; `addr` is not changed in any case
  ; i can do it faster, but meh...
  ld    eax,TOS
  pop   TOS
  ld    edi,[esp]
  jecxz .notfound
  test  TOS,0x80000000
  jr    nz,.notfound
  add   edi,TOS
.scanloop:
  dec   edi
  cp    byte [edi],al
  jr    z,.done
  loop  .scanloop
.notfound:
  xor   TOS,TOS
  urnext
.done:
  jecxz .notfound
  urnext
urword_end


urword_code "STR-TRIM-AFTER-CHAR",str_trim_after_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char
;; `count` will not include a colon
;; `addr` is not changed in any case
  ; i can do it faster, but meh...
  ld    eax,TOS
  pop   TOS
  ld    edi,[esp]
  jecxz .notfound
  test  TOS,0x80000000
  jr    nz,.notfound
  xor   edx,edx
.scanloop:
  cp    byte [edi],al
  jr    z,.done
  inc   edi
  inc   edx
  loop  .scanloop
.notfound:
  xor   TOS,TOS
  urnext
.done:
  inc   edx
  ld    TOS,edx
  urnext
urword_end


urword_forth "STR-SKIP-AFTER-LAST-CHAR",str_skip_after_last_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
  UF rpush 2dup rpop str_trim_after_last_char
  UF nip dup rpush
  UF - swap rpop + swap
urword_end

urword_forth "STR-SKIP-AFTER-CHAR",str_skip_after_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
  UF rpush 2dup rpop str_trim_after_char
  UF nip dup rpush
  UF - swap rpop + swap
urword_end


urword_forth "STR-TRIM-AT-LAST-CHAR",str_trim_at_last_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char
;; `count` will not include char
;; `addr` is not changed in any case
  UF str_trim_after_last_char dup
  ur_if
    UF 1dec
  ur_endif
urword_end

urword_forth "STR-TRIM-AT-CHAR",str_trim_at_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char
;; `count` will not include char
;; `addr` is not changed in any case
  UF str_trim_after_char dup
  ur_if
    UF 1dec
  ur_endif
urword_end


urword_forth "STR-SKIP-AT-LAST-CHAR",str_skip_at_last_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
  UF rpush 2dup rpop str_trim_at_last_char
  UF nip dup rpush
  UF - swap rpop + swap
urword_end

urword_forth "STR-SKIP-AT-CHAR",str_skip_at_char
;; ( addr count char -- addr count )
;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
  UF rpush 2dup rpop str_trim_at_char
  UF nip dup rpush
  UF - swap rpop + swap
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "STR-EXTRACT-PATH",str_extract_path
;; ( addr count -- addr count )
;; count can be 0 if no path there
;; trailing '/' is included
  UF 47 str_trim_after_last_char
urword_end

urword_forth "STR-EXTRACT-NAME",str_extract_name
;; ( addr count -- addr count )
;; count can be 0 if no name there (and `addr` is not changed)
  UF 47 str_skip_after_last_char
urword_end

urword_forth "STR-EXTRACT-EXT",str_extract_ext
;; ( addr count -- addr count )
;; count can be 0 if no extension there (and `addr` is not changed)
;; dot is included
  UF 2dup str_extract_name qdup
  ur_ifnot
    ;; ( addr count dummyaddr )
    UF 2drop 0 exit
  ur_endif
  ;; ( addr count naddr ncount )
  UF 46 str_skip_at_last_char qdup
  ur_ifnot
    ;; ( addr count dummyaddr )
    UF 2drop 0
  ur_else
    ;; ( addr count extaddr extcount )
    UF 2swap 2drop
  ur_endif
urword_end

urword_forth "STR-EXTRACT-BASE-NAME",str_extract_base_name
;; ( addr count -- addr count )
;; count can be 0 if no base name there (and `addr` is not changed)
  ;; get base name
  UF 2dup str_extract_name qdup
  ur_ifnot
    ;; no file name, nothing to extract
    UF 2drop 0 exit
  ur_endif
  ;; trim at extension
  ;; ( addr count nameaddr namecount )
  UF 2dup 46 str_trim_at_last_char
  ;; ( addr count nameaddr namecount bnaddr bncount )
  UF qdup
  ur_ifnot
    ;; ( addr count nameaddr namecount dummyaddr )
    UF drop 2rpush 2drop 2rpop exit
  ur_endif
  ;; ( addr count nameaddr namecount bnaddr bncount )
  UF 2rpush 2drop 2drop 2rpop
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "C4S-ZTERM",c4s_zterm
;; ( addr -- )
  UF count + 0poke
urword_end

urword_forth "C4S-COPY",c4s_copy
;; ( addrsrc addrdest -- )
  UF rpush dup count_only cellinc rpop swap cmove
urword_end

urword_forth "C4S-COPY-A-C",c4s_copy_ac
;; ( addrsrc count addrdest -- )
  UF 2dup poke
  UF cellinc swap cmove
urword_end
urword_alias "C4S-COPY-COUNTED",c4s_copy_counted,c4s_copy_ac

urword_forth "C4S-CAT-A-C",c4s_cat_ac
;; ( addr count addrdest -- )
  UF over 0great
  ur_if
    UF dup rpush count + swap dup rpush cmove
    UF rpop rpop addpoke
  ur_else
    UF 2drop drop
  ur_endif
urword_end
urword_alias "C4S-CAT-COUNTED",c4s_cat_counted,c4s_cat_ac
