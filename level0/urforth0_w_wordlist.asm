;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

; flags for "(XCFIND-FLAGS)"
XCFIND_FLAG_CASE_SENSITIVE  = 0x01
XCFIND_FLAG_SKIP_MASK_CHECK = 0x02

urword_const "(*VOC-FIND-CASE-SENSITIVE)",xcfind_flags_case_sensitive,XCFIND_FLAG_CASE_SENSITIVE
;urword_hidden
urword_const "(*VOC-FIND-SKIP-MASK-CHECK)",xcfind_flags_skip_mask_check,XCFIND_FLAG_SKIP_MASK_CHECK
;urword_hidden

urword_var "(VOC-FIND-FLAGS)",xcfind_flags,0
;urword_hidden

; only first (low) word is relevant
; "(XCFIND)" fill bitand this with flags byte, and will
; ignore all words with non-zero result
; default value is "ignore SMUDGE"
urword_var "(VOC-FIND-IGNORE-MASK)",xcfind_ignore_mask,FLAG_SMUDGE
;urword_hidden


urword_const "(WFLAG-IMMEDIATE)",par_wflag_immediate,FLAG_IMMEDIATE
;urword_hidden
urword_const "(WFLAG-SMUDGE)",par_wflag_smudge,FLAG_SMUDGE
;urword_hidden
urword_const "(WFLAG-NORETURN)",par_wflag_noreturn,FLAG_NORETURN
;urword_hidden
urword_const "(WFLAG-HIDDEN)",par_wflag_hidden,FLAG_HIDDEN
;urword_hidden
urword_const "(WFLAG-CODEBLOCK)",par_wflag_codeblock,FLAG_CODEBLOCK
;urword_hidden
urword_const "(WFLAG-VOCAB)",par_wflag_vocab,FLAG_VOCAB
;urword_hidden

urword_const "(WARG-NONE)",par_warg_none,WARG_NONE
;urword_hidden
urword_const "(WARG-BRANCH)",par_warg_branch,WARG_BRANCH
;urword_hidden
urword_const "(WARG-LIT)",par_warg_lit,WARG_LIT
;urword_hidden
urword_const "(WARG-C4STRZ)",par_warg_c4strz,WARG_C4STRZ
;urword_hidden
urword_const "(WARG-CFA)",par_warg_cfa,WARG_CFA
;urword_hidden
urword_const "(WARG-CBLOCK)",par_warg_cblock,WARG_CBLOCK
;urword_hidden


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "CFA->PFA",cfa2pfa
;; ( cfa -- pfa )
  add   TOS,5     ; skip call
  urnext
urword_end

urword_code "PFA->CFA",pfa2cfa
;; ( pfa -- cfa )
  sub   TOS,5     ; undo skip call
  urnext
urword_end

urword_code "CFA->NFA",cfa2nfa
;; ( cfa -- nfa )
  movzx eax,byte [TOS-1]
  add   eax,1+4      ; trailing length, lenflags
  sub   TOS,eax
  urnext
urword_end

urword_code "NFA->CFA",nfa2cfa
;; ( nfa -- cfa )
  movzx eax,byte [TOS]
  add   eax,1+4      ; trailing length, lenflags
  add   TOS,eax
  urnext
urword_end

urword_code "CFA->LFA",cfa2lfa
;; ( cfa -- lfa )
  movzx eax,byte [TOS-1]
  add   eax,1+4+4+4  ; trailing length, lenflags, namehash, lfa
  sub   TOS,eax
  urnext
  sub   TOS,4
  urnext
urword_end

urword_code "LFA->CFA",lfa2cfa
;; ( lfa -- cfa )
  movzx eax,byte [TOS+8]
  add   eax,1+4+4+4  ; trailing length, lenflags, namehash, lfa
  add   TOS,eax
  urnext
urword_end

urword_code "LFA->NFA",lfa2nfa
;; ( lfa -- cfa )
  add   TOS,8
  urnext
urword_end

urword_code "NFA-COUNT",nfacount
;; ( nfa -- addr count )
  movzx eax,byte [TOS]
  add   TOS,4  ; leadlenflags
  push  TOS
  ld    TOS,eax
  urnext
urword_end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "NFA->DFA",nfa2dfa
;; ( nfa -- dfa )
;; to debugptr field
  sub   TOS,20
  urnext
urword_end

urword_code "NFA->SFA",nfa2sfa
;; ( nfa -- sfa )
;; to word size field
  sub   TOS,16
  urnext
urword_end

urword_code "NFA->BFA",nfa2bfa
;; ( nfa -- bfa )
;; to bucketptr field
  sub   TOS,12
  urnext
urword_end

urword_code "NFA->LFA",nfa2lfa
;; ( nfa -- sfa )
;; to word size field
  sub   TOS,8
  urnext
urword_end

urword_code "NFA->HFA",nfa2hfa
;; ( nfa -- hfa )
;; to name hash field
  sub   TOS,4
  urnext
urword_end

urword_code "NFA->FFA",nfa2ffa
;; ( nfa -- ffa )
;; flag fields area; 2 bytes
  add   TOS,2
  urnext
urword_end

urword_code "NFA->TFA",nfa2tfa
;; ( nfa -- tfa )
;; argument type area; 1 byte
  inc   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "BFA->NFA",bfa2nfa
;; ( bfa -- nfa )
  add   TOS,4+4+4
  urnext
urword_end

urword_code "BFA->LFA",bfa2lfa
;; ( bfa -- lfa )
  add   TOS,4
  urnext
urword_end

urword_code "BFA->HFA",bfa2hfa
;; ( bfa -- hfa )
  add   TOS,4+4
  urnext
urword_end

urword_code "HFA->BFA",hfa2bfa
;; ( bfa -- hfa )
  sub   TOS,4+4
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "FFA@",ffapeek
;; ( ffa -- ffa-value )
  movzx TOS,word [TOS]
  urnext
urword_end

urword_code "TFA@",tfapeek
;; ( tfa -- tfa-value )
  movzx TOS,byte [TOS]
  urnext
urword_end

urword_code "FFA!",ffapoke
;; ( ffa-value ffa -- )
  pop   eax
  ld    word [TOS],ax
  pop   TOS
  urnext
urword_end

urword_code "TFA!",tfapoke
;; ( tfa-value tfa -- )
  pop   eax
  ld    byte [TOS],al
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "CFA-IMMEDIATE?",cfa_is_imm
  UF cfa2nfa nfa2ffa ffapeek par_wflag_immediate and notnot
urword_end

urword_forth "CFA-HIDDEN?",cfa_is_hid
  UF cfa2nfa nfa2ffa ffapeek par_wflag_hidden and notnot
urword_end

urword_forth "CFA-VOCAB?",cfa_is_vocab
  UF cfa2nfa nfa2ffa ffapeek par_wflag_vocab and notnot
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; the following words should be vectorized for metacompiler
urword_forth "LATEST-LFA",latestlfa
;; ( -- lfa )
  UF current @ @
urword_end

urword_forth "LATEST-NFA",latestnfa
;; ( -- nfa )
  UF latestlfa lfa2nfa
urword_end

urword_forth "LATEST-CFA",latestcfa
;; ( -- cfa )
  UF latestlfa lfa2cfa
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(TOGGLE-LATEST-WFLAG)",par_toggle_wflag
urword_hidden
;; ( flag -- )
  UF latestnfa nfa2ffa swap wtoggle
urword_end

urword_forth "(RESET-LATEST-WFLAG)",par_reset_wflag
urword_hidden
;; ( flag -- )
  UF latestnfa nfa2ffa
  UF dup wpeek
  ;; ( flag addr oldflg )
  UF rot bitnot and swap wpoke
urword_end

urword_forth "(SET-LATEST-WFLAG)",par_set_wflag
urword_hidden
;; ( flag -- )
  UF latestnfa nfa2ffa
  UF dup wpeek
  ;; ( flag addr oldflg )
  UF rot or swap wpoke
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "SMUDGE",smudge
  UF par_wflag_smudge par_toggle_wflag
urword_end

urword_forth "IMMEDIATE",immediate
  UF par_wflag_immediate par_toggle_wflag
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(XCFIND) ( addr count voclwptr -- cfa tf  ok)
;         ( addr count voclwptr -- ff     bad)
urword_code "VOC-FIND-STR-NOHASH",xcfind_nohash
  pop   eax   ; count
  pop   edi   ; addr
  push  eax   ; store length at [esp]
  ; TOS=latest-ptr
  ; EDI=addr
  ; EAX=count (also at [esp])

  ; check length
  cmp   eax,256
  jr    nc,.toolong
  or    eax,eax
  jr    z,.toolong  ; too short, actually; zero-length words are not allowed, and cannot be found

.search_loop:
  ; follow lfa
  mov   TOS,[TOS]
  jecxz .notfound
  ; TOS=lfa

  ; check "ignore mask" flag
  test  byte [fvar_xcfind_flags_data],XCFIND_FLAG_SKIP_MASK_CHECK
  jr    nz,.skip_mask_check
  ; check ignore mask
  mov   ax,word [TOS+2+8]
  test  ax,word [fvar_xcfind_ignore_mask_data]
  ; skip if we have some bits set
  ; default setting: skip SMUDGE words
  jr    nz,.search_loop
.skip_mask_check:

  ; compare name length
  movzx eax,byte [TOS+8]
  cmp   al,byte [esp]
  jr    nz,.search_loop
  ; note that EAX (length) will never be zero here
  ; this is invariant that is checked at the entry
  ; (because 0-length strings are rejected)
  push  edi       ; we'll need it later
  push  ecx       ; we'll need it later
  ; skip lfa, hash, and length cell
  add   TOS,4+4+4
  ; compare bytes
  mov   edx,ecx   ; EDX is name bytes
  mov   ecx,eax   ; ECX is counter
  ; EDI=straddr
  ; EDX=name
  ; ECX=namelen
.loop_cmp:
  mov   al,byte [edi]
  ; check "case sensitive" flag
  test  byte [fvar_xcfind_flags_data],XCFIND_FLAG_CASE_SENSITIVE
  jr    nz,.nocaseconv
  cmp   al,'a'
  jc    .nocaseconv
  cmp   al,'z'+1
  jnc   .nocaseconv
  sub   al,32
.nocaseconv:
  cmp   al,[edx]
  jnz   .cmpfailed
  inc   edi
  inc   edx
  loop  .loop_cmp
.loop_done:
  ; name matches, we're done
  ; stack: count addr nfa
  ; we don't need any of that
  add   esp,4+4+4
  ; EDX=after-name
  inc   edx       ; skip trailing length
  push  edx       ; cfa
  mov   TOS,1     ; true
  jp    .done

.cmpfailed:
  pop   ecx
  pop   edi
  jr    .search_loop

.toolong:
  xor   TOS,TOS
.notfound:
  add   esp,4     ; drop saved string length
.done:
  urnext
urword_end


if WLIST_HASH_BITS
;(XCFIND) ( addr count voclwptr -- cfa tf  ok)
;         ( addr count voclwptr -- ff     bad)
urword_code "VOC-FIND-STR",xcfind
  pop   eax   ; count
  pop   edi   ; addr

  ;; check length
  or    eax,eax  ;; zero-length words cannot be found
  jr    nz,@f
  xor   TOS,TOS
  urnext
@@:
  cp    eax,256
  jr    c,@f
  ;; invalid length, no need to search for anything
  xor   TOS,TOS
  urnext
@@:

  ; calculate word hash
  ; store count (it will become [esp+4])
  push  eax

  push  ecx
  push  edi
  ld    ecx,eax
  call  str_name_hash_edi_ecx
  ld    edx,eax
  pop   edi
  pop   ecx

  ; store hash at [esp] (and count is at [esp+4] now)
  push  edx

  ; fold hash, and move TOS to bucket link
  ld    eax,edx
  call  str_name_hash_fold_mask_eax
  add   eax,3
  shl   eax,2
  add   TOS,eax

  ; TOS=bfa-ptr
  ; EDI=addr

.search_loop:
  ; follow bfa
  mov   TOS,[TOS]
  jecxz .notfound
  ; TOS=bfa

  ; check "ignore mask" flag
  test  byte [fvar_xcfind_flags_data],XCFIND_FLAG_SKIP_MASK_CHECK
  jr    nz,.skip_mask_check
  ; check ignore mask
  mov   ax,word [TOS+2+12]
  test  ax,word [fvar_xcfind_ignore_mask_data]
  ; skip if we have some bits set
  ; default setting: skip SMUDGE words
  jr    nz,.search_loop
.skip_mask_check:

  ; compare name hash
  ld    edx,[TOS+8]
  cp    edx,dword [esp]
  jr    nz,.search_loop

  ; compare name length
  movzx eax,byte [TOS+12]
  cmp   al,byte [esp+4]
  jr    nz,.search_loop

  ; note that EAX (length) will never be zero here
  ; this is invariant that is checked at the entry
  ; (because 0-length strings are rejected)
  push  edi       ; we'll need it later
  push  ecx       ; we'll need it later
  ; skip bfa, lfa, hash, and length cell
  add   TOS,4+4+4+4
  ; compare bytes
  mov   edx,ecx   ; EDX is name bytes
  mov   ecx,eax   ; ECX is counter
  ; EDI=straddr
  ; EDX=name
  ; ECX=namelen
.loop_cmp:
  mov   al,[edi]
  ; check "case sensitive" flag
  test  byte [fvar_xcfind_flags_data],XCFIND_FLAG_CASE_SENSITIVE
  jr    nz,.nocaseconv
  cmp   al,'a'
  jc    .nocaseconv
  cmp   al,'z'+1
  jnc   .nocaseconv
  sub   al,32
.nocaseconv:
  cmp   al,[edx]
  jnz   .cmpfailed
  inc   edi
  inc   edx
  loop  .loop_cmp
.loop_done:
  ; name matches, we're done
  ; stack: count hash addr nfa
  ; we don't need any of that
  add   esp,4+4+4+4
  ; EDX=after-name
  inc   edx       ; skip trailing length
  push  edx       ; cfa
  mov   TOS,1     ; true
  urnext

.cmpfailed:
  pop   ecx
  pop   edi
  jr    .search_loop

.notfound:
  add   esp,4+4   ; drop saved string length and hash
  urnext

urword_end
else
urword_alias "VOC-FIND-STR",xcfind,xcfind_nohash
end if

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; read next word from the input stream
;; search it in the dictionary
;; hidden words won't be found unless current vocabulary is the top context one
urword_forth "(WFIND-STR-BASIC)",par_wfind_str_basic
urword_hidden
;; ( addr count -- cfa tf  ok)
;; ( addr count -- ff      bad)
  ; save find mask
  UF xcfind_ignore_mask @ rpush
  ; fix "skip hidden" flag
  ; skip smudge words
  UF par_wflag_smudge xcfind_ignore_mask orpoke
  ; do not search in CURRENT
  UF 2rpush
  ; search context stack
  UF context
  ;; ( ctxptr | addr count )
  ur_begin
    UF dup @ qdup
  ur_while
    ;; ( ctxptr voclptr | addr count )
    ; if voc-to-search is the current one, allow hidden words
    UF dup current @ equal
    ur_if
      ;; allow hidden words
      UF par_wflag_hidden xcfind_ignore_mask notandpoke
    ur_else
      ;; no hidden words
      UF par_wflag_hidden xcfind_ignore_mask orpoke
    ur_endif
    ;; ( ctxptr voclptr | addr count )
    UF 2rpeek rot xcfind
    ur_if
      ;; ( ctxptr cfa )
      UF nip 2rdrop 1
      ; restore search mask
      UF rpop xcfind_ignore_mask !
      UF exit
    ur_endif
    ;; ( ctxptr | addr count )
    ;; if voc-to-search is the current one, look into parents
    UF dup @ current @ equal
    ur_if
      ;; hidden words already allowed above
      UF dup @
      ur_begin
        UF vocid2parent @ qdup
      ur_while
        ;; ( ctxptr voclptr | savedmask addr count )
        UF dup 2rpeek rot xcfind
        ur_if
          ;; ( ctxptr voclptr cfa )
          UF nrot 2drop 2rdrop 1
          ; restore search mask
          UF rpop xcfind_ignore_mask !
          UF exit
        ur_endif
      ur_repeat
    ur_endif
    ;; up context stack
    UF celldec
  ur_repeat
  UF drop 2rdrop 0
  ; restore search mask
  UF rpop xcfind_ignore_mask !
urword_end

;; doesn't do any namespace resolution
urword_defer "WFIND-BASIC",wfind_basic,par_wfind_str_basic


urword_forth "(WFIND-FLAGS>R)",par_wfind_save_flags
urword_hidden
  UF rpop
  UF xcfind_flags @ rpush
  UF xcfind_ignore_mask @ rpush
  UF rpush
urword_end

urword_forth "(WFIND-R>FLAGS)",par_wfind_restore_flags
urword_hidden
  UF rpop
  UF rpop xcfind_ignore_mask !
  UF rpop xcfind_flags !
  UF rpush
urword_end


;; this does namespace resolution
;; if "a:b" is not a known word, try to search "b" in dictionary "a"
;; things like "a:b:c" are allowed too
urword_forth "(WFIND-STR)",par_wfind_str
urword_hidden
;; ( addr count -- cfa tf  ok)
;; ( addr count -- ff      bad)
  ;; try full word first
  UF 2dup par_wfind_str_basic
  ur_if
    UF rpush 2drop rpop 1 exit
  ur_endif
  ; first colon
  UF 2dup 58 str_trim_at_char qdup
  ur_ifnot
    ; no colon
    UF 2drop drop 0 exit
  ur_endif
  ; try to find a vocabulary
  UF par_wfind_str_basic
  ur_ifnot
    ; not found
    UF 2drop 0 exit
  ur_endif
  ; check if it is a vocabulary
  UF dup word_type wtype_voc equal
  ur_ifnot
    ; not a vocabulary
    UF 2drop drop 0 exit
  ur_endif
  ;; make sure that we can find any hidden word this way
  UF par_wfind_save_flags
  ;; fix flags
  UF xcfind_flags_skip_mask_check xcfind_flags notandpoke
  ;; fix mask
  UF par_wflag_hidden xcfind_ignore_mask notandpoke
  ;; go on
  UF rpush 58 str_skip_after_char rpop
  UF rpush
  ;; ( addr count | vocab )
  ur_begin
    UF 2dup rpeek voc_cfa2vocid xcfind
    ur_if
      ; i found her!
      ;; ( addr count cfa | vocab )
      UF rpush 2drop rpop rdrop 1 par_wfind_restore_flags exit
    ur_endif
    ;; ( addr count | vocab )
    UF 2dup 58 str_trim_at_char qdup
    ur_ifnot
      ; no more colon
      ;; ( addr count addr | vocab )
      UF 2drop drop rdrop 0 par_wfind_restore_flags exit
    ur_endif
    ;; vocabulary recursion
    ;; ( addr count addr count | vocab )
    UF rpeek voc_cfa2vocid xcfind
    ur_ifnot
      ; not found
      ;; ( addr count | vocab )
      UF 2drop rdrop 0 par_wfind_restore_flags exit
    ur_endif
    ;; ( addr count newvocabcfa | vocab )
    UF rdrop rpush
    UF 58 str_skip_after_char
  ur_again
urword_end

urword_defer "WFIND-STR",wfind_str,par_wfind_str


urword_forth "-FIND",mfind
;; ( -- cfa true // false )  \ word
  UF parse_name wfind_str
urword_end

urword_forth "-FIND-REQUIRED",mfind_required
;; ( -- cfa )  \ word
  UF parse_name
  UF 2dup wfind_str
  ur_ifnot
    ;; ( addr count )
    UF qendcr
    ur_if
      UF space
    ur_endif
    UF 34 emit type 34 emit
    UF errid_word_expected qerror
  ur_else
    UF nrot 2drop
  ur_endif
urword_end

urword_forth "HAS-WORD?",has_word
;; ( addr count -- flag )
  UF wfind_str
  ur_if
    UF drop 1
  ur_else
    UF 0
  ur_endif
urword_end
