;; Ketmar's common utility functions
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


; i love Z80!
ld fix mov
cp fix cmp
ccf fix cmc  ; complement carry
scf fix stc  ; set carry

macro jmp cond*,lbl {
  if lbl eq
    ; normal jump
    jmp cond
  else
    j#cond lbl
  end if
}

macro jp cond*,lbl {
  if lbl eq
    ; normal jump
    jmp cond
  else
    j#cond lbl
  end if
}

macro jr cond*,lbl {
  if lbl eq
    ; normal jump
    jmp cond
  else
    j#cond lbl
  end if
}


macro ret cond {
  local jlbl,dolbl
  if cond eq
    ; normal ret
    ret
  else
    j#cond dolbl
    jmp    jlbl
    dolbl = $
    ret
    jlbl = $
  end if
}


macro syscall {
  int 0x80
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; display n as hex number, in compile-time
;;
macro display_hex n {
  local bits,d
  bits = 32
  repeat bits/4
    d = "0" + n shr (bits-%*4) and 0Fh
    if d > "9"
      d = d+"A"-"9"-1
    end if
    display d
  end repeat
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; display n as unsigned decimal number, in compile-time
;;
macro display_dec n {
  local cnt,divisor,still,d,nn
  ; this can be done recursive, but...
  still = 1
  cnt = 0
  divisor = 10
  nn = n
  while still <> 0
    cnt = cnt+1
    nn = nn/10
    if nn = 0
      still = 0
    else
      divisor = divisor*10
    end if
  end while
  ; cnt -- number of digits
  ; divisor -- guess what
  while divisor <> 1
    divisor = divisor/10
    nn = n/divisor
    d = "0"+(nn mod 10)
    display d
  end while
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; convert low 4 bits of AL to hex digit ready to print
;; in: AL: nibble
;; out: AL: hex digit ready to print (uppercased)
;; it's voodoo %-) can somebody explain this code? %-)
;; heh, it's one byte shorter than the common snippets i've seen
;; many times in teh internets.
;; actually,, this is the code from my Z80 library. %-)
macro Nibble2Hex {
  and   al,0Fh
  cmp   al,0Ah
  sbb   al,69h
  das   ; yeah, yeah, das ist fantastich!
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; display asciiz string at the given address, in compile time
;;
macro display_asciiz_at addr {
  local endstr,chr
  endstr = 0
  while endstr = 0
    load chr byte from addr
    addr = addr+1
    if chr = 0
      endstr = 1
    else
      display chr
    end if
  end while
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
O_ACCMODE  = 0003o
O_RDONLY   = 0000o
O_WRONLY   = 0001o
O_RDWR     = 0002o
O_CREAT    = 0100o
O_EXCL     = 0200o
O_NOCTTY   = 0400o
O_TRUNC    = 1000o
O_APPEND   = 2000o
O_NONBLOCK = 4000o

S_ISUID    = 4000o
S_ISGID    = 2000o
S_ISVTX    = 1000o
S_IRUSR    = 0400o
S_IWUSR    = 0200o
S_IXUSR    = 0100o
S_IRGRP    = 0040o
S_IWGRP    = 0020o
S_IXGRP    = 0010o
S_IROTH    = 0004o
S_IWOTH    = 0002o
S_IXOTH    = 0001o


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
if 0
elf_hash_value = 0

macro elf_hash_str str* {
  local bpos,hash,high,xstr,b
virtual at 0x100000
xstr: db str,0
  hash = 0
  bpos = 0
  load b byte from xstr+bpos
  while b <> 0
    hash = ((hash shl 4)+b) and 0xffffffff
    high = hash and 0xf0000000
    hash = hash xor (high shr 24)
    hash = hash and (high xor 0xffffffff)
    bpos = bpos+1
    load b byte from xstr+bpos
  end while
end virtual
  elf_hash_value = hash
}


elf_hash_str "a"
assert elf_hash_value = 0x00000061
elf_hash_str "ab"
assert elf_hash_value = 0x00000672
elf_hash_str "The quick brown fox jumps over the lazy dog"
assert elf_hash_value = 0x04280C57
end if
