;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_var "(EXC-FRAME-PTR)",par_exc_frame_ptr,0
urword_hidden


urword_forth "CATCH",catch
;; ( i * x xt -- j * x 0 | i * x n )
  ;; this is using return stack to hold previous catch frame
  ;; of course, this prevents Very Smart return stack manipulation, but idc (for now)
  ;; exception frame consists of:
  ;;   return-to-catch EIP (return stack TOS)
  ;;   sp (frame points here)
  ;;   prev_frame_ptr
  ;;   return-to-catch-caller EIP
  ;; create exception frame
  UF par_exc_frame_ptr @ rpush
  UF spget rpush
    ;; UF spget cellinc endcr dothex8 cr dotstack
  UF rpget par_exc_frame_ptr !  ;; update exception frame pointer
  UF execute  ;; and execute
  ;; we will return here only if no exception was thrown
  UF rdrop    ;; drop spdepth
  UF rpop par_exc_frame_ptr !   ;; restore previous exception frame
  UF 0        ;; exception code (none)
urword_end


urword_forth "THROW",throw
;; ( k * x n -- k * x | i * x n )
  UF qdup
  ur_if
      ;; UF spget endcr dothex8 cr dotstack
    ;; check if we have exception frame set
    UF par_exc_frame_ptr @ qdup
    ur_ifnot
      ;; panic!
      UF par_exc_frame_ptr 0poke
      UF state 0poke  ;; just in case
      UF errid_throw_without_catch par_error
      UF 1 nbye ;; just in case
    ur_endif
    ;; check if return stack is not exhausted
    UF rpget celldec over ugreat
    ur_if
      ;; panic!
      UF par_exc_frame_ptr 0poke
      UF state 0poke  ;; just in case
      UF errid_throw_chain_corrupted par_error
      UF 1 nbye ;; just in case
    ur_endif
    ;; restore return stack
    UF rpset
    ;; exchange return stack top and data stack top (save exception code, and pop sp to data stack)
    UF rpop swap rpush
    ;; blindly restore data stack (let's hope it is not too badly trashed)
    UF spset drop  ;; drop the thing that was CFA
      ;; UF spget endcr dothex8 space rpeek dothex8 cr
    ;; restore exception code
    UF rpop
    ;; restore previous exception frame
    UF rpop par_exc_frame_ptr !
    ;; now EXIT will return to CATCH caller
  ur_endif
urword_end
