;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "BSWAP-WORD",bswap_word
;; ( u -- u )
;; swap bytes of the low word
;; high word is untouched
  xchg  cl,ch
  urnext
urword_end


urword_code "BSWAP-DWORD",bswap_dword
;; ( u -- u )
;; swap all dword bytes
  bswap TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "CELLS",cells
;; ( count -- count*4 )
  shl   TOS,2
  urnext
urword_end

urword_code "+CELLS",addcells
;; ( addr count -- addr+count*4 )
  shl   TOS,2
  pop   eax
  add   TOS,eax
  urnext
urword_end

urword_code "-CELLS",subcells
;; ( addr count -- addr-count*4 )
  shl   TOS,2
  pop   eax
  sub   eax,TOS
  ld    TOS,eax
  urnext
urword_end

;; useful for array indexing
urword_code "CELLS^",cells_addr
;; ( count addr -- addr+count*4 )
  pop   eax
  lea   TOS,[TOS+eax*4]
  urnext
urword_end

urword_code "CELL+",cellinc
;; ( count -- count+4 )
  add   TOS,4
  urnext
urword_end

urword_code "CELL-",celldec
;; ( count -- count-4 )
  sub   TOS,4
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "1+",1inc
;; ( n -- n+1 )
  inc   TOS
  urnext
urword_end

urword_code "1-",1dec
;; ( n -- n-1 )
  dec   TOS
  urnext
urword_end

urword_code "2+",2inc
;; ( n -- n+2 )
  add   TOS,2
  urnext
urword_end

urword_code "2-",2dec
;; ( n -- n-2 )
  sub   TOS,2
  urnext
urword_end

urword_code "4+",4inc
;; ( n -- n+4 )
  add   TOS,4
  urnext
urword_end

urword_code "4-",4dec
;; ( n -- n-4 )
  sub   TOS,4
  urnext
urword_end

urword_code "8+",8inc
;; ( n -- n+8 )
  add   TOS,8
  urnext
urword_end

urword_code "8-",8dec
;; ( n -- n-8 )
  sub   TOS,8
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "NOT",not
;; ( n -- !n )
  cmp   TOS,1
  ;  C: TOS == 0
  ; NC: TOS != 0
  mov   TOS,0
  adc   TOS,0
  urnext
urword_end

urword_code "NOTNOT",notnot
;; ( n -- !!n )
  cmp   TOS,1
  ;  C: TOS == 0
  ; NC: TOS != 0
  mov   TOS,1
  sbb   TOS,0
  urnext
urword_end

urword_code "BITNOT",bitnot
;; ( n -- ~n )
  xor   TOS,0xffffffff
  urnext
urword_end

urword_code "AND",and
;; ( n0 n1 -- n0&n1 )
  pop   eax
  xchg  eax,TOS
  and   TOS,eax
  urnext
urword_end

urword_code "OR",or
;; ( n0 n1 -- n0|n1 )
  pop   eax
  xchg  eax,TOS
  or    TOS,eax
  urnext
urword_end

urword_code "XOR",xor
;; ( n0 n1 -- n0^n1 )
  pop   eax
  xchg  eax,TOS
  xor   TOS,eax
  urnext
urword_end


urword_code "LOGAND",land
;; ( n0 n1 -- n0&&n1 )
  pop   eax
  or    al,al
  setnz al
  or    cl,cl
  setnz cl
  and   cl,al
  and   ecx,0xff
  urnext
urword_end

urword_code "LOGOR",lor
;; ( n0 n1 -- n0||n1 )
  pop   eax
  or    TOS,eax
  or    TOS,TOS
  setnz cl
  and   ecx,0xff
  urnext
urword_end


urword_code "LSHIFT",lshift
;; ( n0 n1 -- n0<<n1 )
  pop   eax
  cmp   TOS,32
  jnc   fword_shl_zero
  ; assume that TOS is in ECX
  shl   eax,cl
  mov   TOS,eax
  urnext
fword_shl_zero:
  xor   TOS,TOS
  urnext
urword_end

urword_code "RSHIFT",rshift
;; ( n0 n1 -- n0>>n1 )
  pop   eax
  cmp   TOS,32
  jnc   .zero
  ; assume that TOS is in ECX
  shr   eax,cl
  mov   TOS,eax
  urnext
.zero:
  xor   TOS,TOS
  urnext
urword_end

urword_code "ARSHIFT",arshift
;; ( n0 n1 -- n0>>n1 )
  pop   eax
  cmp   TOS,32
  jnc   .toobig
  ; assume that TOS is in ECX
  sar   eax,cl
  mov   TOS,eax
  urnext
.toobig:
  mov   TOS,-1
  cmp   eax,0x80000000
  adc   TOS,0
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "LROTATE",lrotate
;; ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   eax,cl
  ld    TOS,eax
  urnext
urword_end

urword_code "RROTATE",rrotate
;; ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   eax,cl
  ld    TOS,eax
  urnext
urword_end

urword_code "LROTATE-WORD",lrotate_word
;; ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   ax,cl
  ld    TOS,eax
  urnext
urword_end

urword_code "RROTATE-WORD",rrotate_word
;; ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   ax,cl
  ld    TOS,eax
  urnext
urword_end

urword_code "LROTATE-BYTE",lrotate_byte
;; ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   al,cl
  ld    TOS,eax
  urnext
urword_end

urword_code "RROTATE-BYTE",rrotate_byte
;; ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   al,cl
  ld    TOS,eax
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "+",add
;; ( n0 n1 -- n0+n1 )
  pop   eax
  add   TOS,eax
  urnext
urword_end

urword_code "-",sub
;; ( n0 n1 -- n0-n1 )
  pop   eax
  ; EAX=n0
  ; TOS=n1
  sub   eax,TOS
  mov   TOS,eax
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "2*",2imul
;; ( n -- n*2 )
  shl   TOS,1
  urnext
urword_end

urword_code "2/",2idiv
;; ( n -- n/2 )
  sar   TOS,1
  urnext
urword_end


urword_code "2U*",2umul
;; ( n -- n*2 )
  shl   TOS,1
  urnext
urword_end

urword_code "2U/",2udiv
;; ( n -- n/2 )
  shr   TOS,1
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "NEGATE",negate
;; ( n -- -n )
  neg   TOS
  urnext
urword_end

urword_code "ABS",abs
;; ( n -- |n| )
  test  TOS,0x80000000
  jz    @f
  neg   TOS
@@:
  urnext
urword_end


urword_code "UMIN",umin
;; ( u0 u1 -- umin )
  pop   eax
  ; EAX=u0
  ; TOS=u1
  cp    eax,TOS
  cmovc TOS,eax
  urnext
urword_end

urword_code "UMAX",umax
;; ( u0 u1 -- umax )
  pop   eax
  ; EAX=u0
  ; TOS=u1
  cp    TOS,eax
  cmovc TOS,eax
  urnext
urword_end

urword_code "MIN",min
;; ( n0 n1 -- nmin )
  pop   eax
  ; EAX=u0
  ; TOS=u1
  cp    TOS,eax
  cmovg TOS,eax
  urnext
urword_end

urword_code "MAX",max
;; ( n0 n1 -- nmax )
  pop   eax
  ; EAX=u0
  ; TOS=u1
  cp    TOS,eax
  cmovl TOS,eax
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "C>S",c2s
;; ( n-8-bit -- n )
  movsx TOS,cl
  urnext
urword_end

urword_code "C>U",c2u
;; ( u-8-bit -- u )
  movzx TOS,cl
  urnext
urword_end

urword_code "S>C",s2c
;; ( n -- n-8-bit )
;; with clamping
  test  TOS,0x80000000
  jr    nz,.negative
  cp    TOS,0x80
  jr    c,.done
  ld    TOS,0x7f
  jr    .done
.negative:
  cp    TOS,0xffffff80
  jr    nc,.done
  ld    TOS,0x80
.done:
  movsx TOS,cl
  urnext
urword_end

urword_code "U>C",u2c
;; ( u -- u-8-bit )
;; with clamping
  cp    TOS,0x100
  jr    c,.done
  mov   cl,0xff
.done:
  movzx TOS,cl
  urnext
urword_end
