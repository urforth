;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word searching
;; CURRENT:
;;   searched first; also, new words will go here
;;
;; CONTEXT:
;;   stack of vocabularies, searched after the CURRENT
;;
URFORTH_VOCSTACK_SIZE = 16
urword_const "VOC-STACK-SIZE",vocstack_size,URFORTH_VOCSTACK_SIZE

; this will be bitored with flags in (CREATE)
urword_value "(CURRENT-CREATE-MODE)",current_mode,0
urword_hidden

urword_var "CURRENT",current,forth_voc_lfa_address
; this actually points to the last pushed vocabulary
urword_value "CONTEXT",context,forth_voc_stack

urword_var "WARNING-REDEFINE",warning_redefine,1
urword_var "CREATE-UPCASE?",create_upcaseq,1

urword_var "(VOCAB-STACK)",sysvar_vocab_stack
urword_hidden
; vocabulary stack
  ; this indicated end of the stack
  dd  0
  ; this one is always here, it is so-called "root" vocabulary (last resort)
  ; it is usually system FORTH vocabulary
  ; CONTEXT may point here
forth_voc_stack_root:
  dd  forth_voc_lfa_address
  ; replaceable stack begins here
forth_voc_stack:
  dd  forth_voc_lfa_address
  dd  URFORTH_VOCSTACK_SIZE-1 dup(0)
forth_voc_stack_end = $
urword_end_array

urword_const "(FORTH-VOC-STACK)",par_forth_voc_stack,forth_voc_stack
urword_hidden
urword_const "(FORTH-VOC-STACK-END)",par_forth_voc_stack_end,forth_voc_stack_end
urword_hidden


; voclink always points to another voclink (or contains 0)
urword_var "(VOC-LINK)",voclink,forth_voc_voclink_address
urword_hidden


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "ID-COUNT",idcount
;; ( nfa -- nameaddr namelen )
  UF dup cpeek swap cellinc swap
urword_end

urword_forth "ID.",iddot
;; ( nfa -- )
  UF idcount safetype
urword_end


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; patches word size to HERE
urword_forth "CREATE;",par_create_patch_size
urword_hidden
  ; patch size
  UF here latestnfa nfa2sfa !
urword_end

;; A defining word used in the form: CREATE cccc
;; by such words as CODE and CONSTANT to create a dictionary header for
;; a Forth definition. Puts no code field.
;; The new word is created in the current vocabulary.
;; Note that SMUDGE bit is set (i.e. the word is invisible).
urword_forth "(CREATE-STR)",par_create_str
urword_hidden
;; ( addr count -- )
  ;; we cannot create words in temp-here (yet)
  UF dp_temp @ errid_no_temp_here qerror
  ;; sanity check (just in case)
  UF here word_here nequ errid_no_temp_here qerror
  ;; check for duplicate word?
  UF dup
  ur_if
    UF warning_redefine @
    ur_if
      ;; look only in the current dictionary
      UF 2dup current @ xcfind
      ur_if
        UF cfa2nfa iddot pardottype " redefined" error_line_dot cr
      ur_endif
    ur_endif
  ur_endif
  ;; ( addr count )
  ;; check length
  UF dup 250 ugreat
  ur_if
    UF 96 emit type 96 emit 63 emit space
    ;; error
    UF errid_invalid_word_name error
  ur_endif
  ;; align
  UF par_align_here
  ;; allocate dfa
  UF false ,
  ;; allocate sfa
  UF false ,
  ;; allocate bfa (it will be patched later)
  UF false ,
  ;; remember HERE (it will become the new latest)
  UF here
  ;; put lfa
  UF latestlfa ,
  ;; update latest
  UF current @ !
  ;; put name hash
  UF 2dup str_name_hash dup ,
  ;; fix bfa
  if WLIST_HASH_BITS
    UF over
    ur_if
      ;; ( addr count hash )
      ;; fold hash
      UF name_hash_fold_mask
      ;; calc bucket address
      UF 3 + cells current @ +
      ;; ( addr count bkptr )
      ;; load old bfa link
      UF dup @
      ;; ( addr count bkptr oldbfa )
      ;; store current bfa address
      UF here nfa2bfa rot !
      ;; update bfa
      UF here nfa2bfa !
    ur_else
      UF drop
    ur_endif
  else
    UF drop  ;; we don't really need any hash
  end if
  ;; remember HERE (we will need to fix some name fields later)
  UF here rpush
  ;; compile counter (we'll add flags and other things later)
  ;; it is guaranteed that all fields except length are zero here
  ;; (due to word header layout, and length check above)
  UF dup ,
  ;; copy parsed word to HERE (and allocate name bytes for it)
  UF dup n_allot swap move
  ;; uppercase created name?
  UF create_upcaseq @
  ur_if
    UF rpeek count upcase_str
  ur_endif
  ;; put flags (ffa is 16 bits at nfa+2)
  UF par_wflag_smudge current_mode or
  UF rpeek nfa2ffa tfapoke  ; it is safe to poke here, ffa is empty
  ;; put length again (trailing length byte)
  UF rpeek cpeek ccomma
  ;; we don't need nfa address anymore
  UF rdrop
  ;; setup initial size, why not
  UF par_create_patch_size
urword_end

urword_forth "(CREATE)",par_create
urword_hidden
  UF parse_name par_create_str
urword_end


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; system FORTH vocabulary
urword_code "FORTH",vocab_forth
urword_vocab
  call  fword_vocab_forth_does
forth_voc_lfa_address:
  dd  urforth_last_word_lfa_late
forth_voc_voclink_address:
  dd  0  ; voclink
  dd  0  ; parent
  ;; words hashtable buckets (will be filled by FASM)
  if WLIST_HASH_BITS
forth_voc_hashtable:
  db  WLIST_HASH_BYTES dup(0)
  end if
  ; does code
fword_vocab_forth_does:
  mov   eax,par_vocab_does_code_pfa
  jmp   fword_par_urforth_nocall_dodoes
urword_end

;; sadly, we have to have it here
urword_forth "(VOCAB-DOES-CODE)",par_vocab_does_code
par_vocab_does_code_pfa:
  ;; ( latest-ptr )
  ; puts this one to the context stack, replacing the top one
  UF context
  urlit forth_voc_stack
  UF uless
  ur_if
    ; we don't have any vocabularies in the stack, push one
    urlit forth_voc_stack
    urto context
  ur_endif
  ; replace top context vocabulary
  UF context !
urword_end


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sadly, the following two are used in word searching code
urword_forth "VOC-CFA->VOCID",voc_cfa2vocid
urword_hidden
;; ( cfa -- lfaptr )
  UF cfa2pfa
urword_end

;; move to parent vocabulary field
urword_forth "VOCID->PARENT",vocid2parent
urword_hidden
;; ( lfaptr -- parent )
  UF 2 addcells
urword_end


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sadly, has to be here, because it is used by word searching code
URWORD_TYPE_OTHER     = 0
URWORD_TYPE_FORTH     = 1
URWORD_TYPE_VAR       = 2
URWORD_TYPE_CONST     = 3
URWORD_TYPE_VALUE     = 4
URWORD_TYPE_DEFER     = 5
URWORD_TYPE_DOES      = 6
URWORD_TYPE_VOC       = 7
URWORD_TYPE_OVERRIDE  = 8
URWORD_TYPE_OVERRIDEN = 9

urword_const "WORD-TYPE-OTHER",wtype_other,URWORD_TYPE_OTHER
urword_const "WORD-TYPE-FORTH",wtype_forth,URWORD_TYPE_FORTH
urword_const "WORD-TYPE-CONST",wtype_const,URWORD_TYPE_CONST
urword_const "WORD-TYPE-VAR",  wtype_var,  URWORD_TYPE_VAR
urword_const "WORD-TYPE-VALUE",wtype_value,URWORD_TYPE_VALUE
urword_const "WORD-TYPE-DEFER",wtype_defer,URWORD_TYPE_DEFER
urword_const "WORD-TYPE-DOES", wtype_does, URWORD_TYPE_DOES
urword_const "WORD-TYPE-VOC",  wtype_voc, URWORD_TYPE_VOC
;; this word is new override (overriden words are not marked as special, yet they become non-Forth, i.e. "other")
urword_const "WORD-TYPE-OVERRIDE",wtype_override,URWORD_TYPE_OVERRIDE
urword_const "WORD-TYPE-OVERRIDEN",wtype_overriden,URWORD_TYPE_OVERRIDEN

urword_var "(WORD-CFA-TABLE)",wcfa_table
urword_hidden
  dd  fword_par_urforth_nocall_doforth,URWORD_TYPE_FORTH
  dd  fword_par_urforth_nocall_doconst,URWORD_TYPE_CONST
  dd  fword_par_urforth_nocall_dovar,URWORD_TYPE_VAR
  dd  fword_par_urforth_nocall_dovalue,URWORD_TYPE_VALUE
  dd  fword_par_urforth_nocall_dodefer,URWORD_TYPE_DEFER
  dd  fword_par_urforth_nocall_dodoes,URWORD_TYPE_DOES
  dd  fword_par_urforth_nocall_dooverride,URWORD_TYPE_OVERRIDE
  dd  0
urword_end_array

;; FIXME: add detection of overriden words
;;        this can be done by checking if CFA CALL
;;        points to another CALL, and that CALL is
;;        a CFA of another word
;;        ...or we can simply add a flag for this ;-)
urword_forth "WORD-TYPE?",word_type
;; ( cfa -- type )
  UF dup cpeek 0xe8 equal
  ur_if
    ;; check "vocabulary" flag
    UF dup cfa2nfa nfa2ffa ffapeek
    UF par_wflag_vocab and
    ur_if
      UF drop wtype_voc exit
    ur_endif
    ;; calculate call address
    UF 1inc par_disp32get
    ;; check for overriden word
    UF dup par_code_entry_addr ugreat over real_here uless and
    ur_if
      ;; it should be safe to peek
      UF dup cpeek 0xe8 equal
      ur_if
        UF dup 1inc par_disp32get
        UF cfalit par_urforth_nocall_dooverride equal
        ur_if
          UF drop wtype_overriden exit
        ur_endif
      ur_endif
    ur_endif
    ;; check table
    UF rpush
    urlit fvar_wcfa_table_data
    ur_begin
      UF dup @ qdup
    ur_while
      ;; ( tbladdr codeaddr | cfadest )
      UF rpeek equal
      ur_if
        UF rdrop cellinc @ exit
      ur_endif
      UF 2 addcells
    ur_repeat
    ;; ( tbladdr | cfadest )
    UF rdrop
  ur_endif
  UF drop wtype_other
urword_end
