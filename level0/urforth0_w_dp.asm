;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; allocate some memory at HERE (DP or DP-TEMP), return starting address
urword_forth "N-ALLOT",n_allot
;; ( n -- start-addr )
  UF dup 0less errid_negative_allot qerror
  UF dup 0x00ffffff ugreat errid_out_of_memory qerror  ;; 16MB is quite huge allocation chunk ;-)
  UF dp_temp @ qdup
  ur_if
    ;; ( n start-addr )
    UF dp_temp
  ur_else
    UF dp @ dp
  ur_endif
  ;; ( n start-addr dpaddr )
  ;; check for oom
  UF rpush
  ;; ( n start-addr | dpaddr )
  UF 2dup +
  UF dup dpend @ 32768 - ugreat errid_out_of_memory qerror
  UF temp_pool_here 256 - ugreatequ errid_out_of_memory qerror
  UF swap rpop addpoke
urword_end

urword_forth "(ALIGN-HERE)",par_align_here
urword_hidden
  if URFORTH_ALIGN_HEADERS
  UF here 3 and qdup
  ur_if
    UF 4 swap - dup n_allot swap erase
  ur_endif
  end if
urword_end

urword_forth "ALLOT",allot
;; ( n -- )
  UF n_allot drop
urword_end

urword_code "USED",used
;; ( -- count )
  push  TOS
  mov   TOS,[fvar_dp_data]
  sub   TOS,[fconst_par_code_base_addr_data]
  urnext
urword_end

urword_code "UNUSED",unused
;; ( -- count )
  push  TOS
  mov   TOS,[fvar_dpend_data]
  sub   TOS,[fvar_dp_data]
  sub   TOS,32768   ; reserved area
  urnext
urword_end

urword_code "HERE",here
;; ( -- addr )
  push  TOS
  ld    TOS,[fvar_dp_temp_data]
  or    TOS,TOS
  cmovz TOS,[fvar_dp_data]
  urnext
urword_end

urword_code "REAL-HERE",real_here
;; ( -- addr )
  push  TOS
  ld    TOS,[fvar_dp_data]
  urnext
urword_end

urword_code "PAD",pad
;; ( -- addr )
  push  TOS
  mov   TOS,[fvar_pad_area_data]
  urnext
urword_end


urword_forth "C,",ccomma
;; ( c -- )
  UF dbginfo_add_here
  UF 1 n_allot cpoke
urword_end

urword_forth "W,",wcomma
;; ( w -- )
  UF dbginfo_add_here
  UF 2 n_allot wpoke
urword_end

urword_forth ",",comma
;; ( n -- )
  UF dbginfo_add_here
  UF cell n_allot poke
urword_end
