;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


ERR_UNKNOWN_WORD = 0
ERR_STACK_UNDERFLOW = 1
ERR_STACK_OVERFLOW = 2
ERR_R_STACK_UNDERFLOW = 3
ERR_R_STACK_OVERFLOW = 4
ERR_OUT_OF_MEMORY = 5
ERR_WORD_REDEFINED = 6
ERR_FILE_NOT_FOUND = 7
ERR_FILE_READ_ERROR = 8
ERR_FILE_WRITE_ERROR = 9
ERR_FILE_TOO_BIG = 10
ERR_COMPILATION_ONLY = 11
ERR_EXECUTION_ONLY = 12
ERR_UNPAIRED_CONDITIONALS = 13
ERR_UNFINISHED_DEFINITION = 14
ERR_IN_PROTECTED_DICT = 15  ; for "forget"
ERR_NOT_DEFER = 16
ERR_INVALID_WORD_NAME = 17
ERR_WORD_EXPECTED = 18
ERR_INVALID_TEMP_POOL_RELEASE = 19
ERR_OUT_OF_TEMP_POOL = 20
ERR_INVALID_TEMP_POOL_ALLOCATION = 21
ERR_VOCABULARY_STACK_OVERFLOW = 22
ERR_CHAR_EXPECTED = 23
ERR_STRING_EXPECTED = 24
ERR_NUMBER_EXPECTED = 25
ERR_VOCABULARY_EXPECTED = 26
ERR_INVALID_BREAK_CONT = 27
ERR_NONNAKED_SYSTEM = 28
ERR_NOT_IMPLEMENTED = 29
ERR_NEGATIVE_ALLOT = 30
ERR_NO_TEMP_HERE = 31
ERR_TEMP_HERE_ALREADY = 32
ERR_CANNOT_OVERRIDE = 33
ERR_CANNOT_REPLACE = 34
ERR_INPUT_TOO_LONG = 35
ERR_THROW_WITHOUT_CATCH = 36
ERR_THROW_CHAIN_CORRUPTED = 37
ERR_UNBALANCED_IFDEF = 38
ERR_ELLIPSIS_FORTH = 39
ERR_ELLIPSIS_FIRST = 40
ERR_NONMACRO_ONLY  = 60
ERR_STRING_TOOLONG  = 61
ERR_USER_ERROR = 69

urword_const "ERR-UNKNOWN-WORD",errid_unknown_word,ERR_UNKNOWN_WORD
urword_const "ERR-STACK-UNDERFLOW",errid_stack_underflow,ERR_STACK_UNDERFLOW
urword_const "ERR-STACK-OVERFLOW",errid_stack_overflow,ERR_STACK_OVERFLOW
urword_const "ERR-R-STACK-UNDERFLOW",errid_r_stack_underflow,ERR_R_STACK_UNDERFLOW
urword_const "ERR-R-STACK-OVERFLOW",errid_r_stack_overflow,ERR_R_STACK_OVERFLOW
urword_const "ERR-OUT-OF-MEMORY",errid_out_of_memory,ERR_OUT_OF_MEMORY
urword_const "ERR-WORD-REDEFINED",errid_word_redefined,ERR_WORD_REDEFINED
urword_const "ERR-FILE-NOT-FOUND",errid_file_not_found,ERR_FILE_NOT_FOUND
urword_const "ERR-FILE-READ-ERROR",errid_file_read_error,ERR_FILE_READ_ERROR
urword_const "ERR-FILE-WRITE-ERROR",errid_file_write_error,ERR_FILE_WRITE_ERROR
urword_const "ERR-FILE-TOO-BIG",errid_file_too_big,ERR_FILE_TOO_BIG
urword_const "ERR-COMPILATION-ONLY",errid_compilation_only,ERR_COMPILATION_ONLY
urword_const "ERR-EXECUTION-ONLY",errid_execution_only,ERR_EXECUTION_ONLY
urword_const "ERR-UNPAIRED-CONDITIONALS",errid_unpaired_conditionals,ERR_UNPAIRED_CONDITIONALS
urword_const "ERR-UNFINISHED-DEFINITION",errid_unfinished_definition,ERR_UNFINISHED_DEFINITION
urword_const "ERR-IN-PROTECTED-DICT",errid_in_protected_dict,ERR_IN_PROTECTED_DICT
urword_const "ERR-NOT-DEFER",errid_not_defer,ERR_NOT_DEFER
urword_const "ERR-INVALID-WORD-NAME",errid_invalid_word_name,ERR_INVALID_WORD_NAME
urword_const "ERR-WORD-EXPECTED",errid_word_expected,ERR_WORD_EXPECTED
urword_const "ERR-INVALID-TEMP-POOL-RELEASE",errid_invalid_temp_pool_release,ERR_INVALID_TEMP_POOL_RELEASE
urword_const "ERR-OUT-OF-TEMP-POOL",errid_out_of_temp_pool,ERR_OUT_OF_TEMP_POOL
urword_const "ERR-INVALID-TEMP-POOL-ALLOCATION",errid_invalid_temp_pool_allocation,ERR_INVALID_TEMP_POOL_ALLOCATION
urword_const "ERR-VOCABULARY-STACK-OVERFLOW",errid_voc_stack_overflow,ERR_VOCABULARY_STACK_OVERFLOW
urword_const "ERR-CHAR-EXPECTED",errid_char_expected,ERR_CHAR_EXPECTED
urword_const "ERR-STRING-EXPECTED",errid_string_expected,ERR_STRING_EXPECTED
urword_const "ERR-NUMBER-EXPECTED",errid_number_expected,ERR_NUMBER_EXPECTED
urword_const "ERR-VOCABULARY-EXPECTED",errid_vocab_expected,ERR_VOCABULARY_EXPECTED
urword_const "ERR-INVALID-BREAK-CONT",errid_bad_break_cont,ERR_INVALID_BREAK_CONT
urword_const "ERR-NONNAKED-SYSTEM",errid_nonnaked_system,ERR_NONNAKED_SYSTEM
urword_const "ERR-NOT-IMPLEMENTED",errid_not_implemented,ERR_NOT_IMPLEMENTED
urword_const "ERR-NEGATIVE-ALLOT",errid_negative_allot,ERR_NEGATIVE_ALLOT
urword_const "ERR-NO-TEMP-HERE",errid_no_temp_here,ERR_NO_TEMP_HERE
urword_const "ERR-TEMP-HERE-ALREADY",errid_temp_here_already,ERR_TEMP_HERE_ALREADY
urword_const "ERR-CANNOT-OVERRIDE",errid_cannot_override,ERR_CANNOT_OVERRIDE
urword_const "ERR-CANNOT-REPLACE",errid_cannot_replace,ERR_CANNOT_REPLACE
urword_const "ERR-INPUT-TOO-LONG",errid_input_too_long,ERR_INPUT_TOO_LONG
urword_const "ERR-THROW-WITHOUT-CATCH",errid_throw_without_catch,ERR_THROW_WITHOUT_CATCH
urword_const "ERR-THROW-CHAIN-CORRUPTED",errid_throw_chain_corrupted,ERR_THROW_CHAIN_CORRUPTED
urword_const "ERR-UNBALANCED-IFDEF",errid_unbalanced_ifdef,ERR_UNBALANCED_IFDEF
urword_const "ERR-USER-ERROR",errid_user_error,ERR_USER_ERROR
urword_const "ERR-ELLIPSIS-FORTH",errid_ellipsis_forth,ERR_ELLIPSIS_FORTH
urword_const "ERR-ELLIPSIS-FIRST",errid_ellipsis_first,ERR_ELLIPSIS_FIRST
urword_const "ERR-NONMACRO-ONLY",errid_nonmacro_only,ERR_NONMACRO_ONLY
urword_const "ERR-STRING-TOO-LONG",errid_string_toolong,ERR_STRING_TOOLONG

urword_forth "(FIND-ERRMSG)",par_find_message
urword_hidden
;; ( id tbladdr -- addr count 1 )
;; ( id tbladdr -- id         0 )
  UF swap rpush
  ur_begin
    UF dup wpeek
  ur_while
    UF dup cpeek rpeek equal
    ur_if
      ; i found her!
      UF rdrop 1inc zcount 1 exit
    ur_endif
    ; skip
    UF 1inc zcount + 1inc
  ur_repeat
  UF drop rpop 0
urword_end


urword_var "(ERROR-MSG-TABLE)",par_err_msg_table
urword_hidden
  db ERR_UNKNOWN_WORD,"pardon?",0
  db ERR_STACK_UNDERFLOW,"stack underflow",0
  db ERR_STACK_OVERFLOW,"stack overflow",0
  db ERR_R_STACK_UNDERFLOW,"return stack underflow",0
  db ERR_R_STACK_OVERFLOW,"return stack overflow",0
  db ERR_OUT_OF_MEMORY,"out of memory",0
  db ERR_WORD_REDEFINED,"is not unique",0
  db ERR_FILE_NOT_FOUND,"file not found",0
  db ERR_FILE_READ_ERROR,"file read error",0
  db ERR_FILE_WRITE_ERROR,"file write error",0
  db ERR_FILE_TOO_BIG,"file too big",0
  db ERR_COMPILATION_ONLY,"compilation only",0
  db ERR_EXECUTION_ONLY,"execution only",0
  db ERR_UNPAIRED_CONDITIONALS,"conditionals not paired",0
  db ERR_UNFINISHED_DEFINITION,"definition not finished",0
  db ERR_IN_PROTECTED_DICT,"in protected dictionary",0
  db ERR_NOT_DEFER,"not a DEFER word",0
  db ERR_INVALID_WORD_NAME,"invalid word name",0
  db ERR_WORD_EXPECTED,"known word expected",0
  db ERR_INVALID_TEMP_POOL_RELEASE,"invalid temp pool release",0
  db ERR_OUT_OF_TEMP_POOL,"out of memory for temp pool",0
  db ERR_INVALID_TEMP_POOL_ALLOCATION,"invalid temp pool request",0
  db ERR_VOCABULARY_STACK_OVERFLOW,"vocabulary stack overflow",0
  db ERR_CHAR_EXPECTED,"character expected",0
  db ERR_STRING_EXPECTED,"string expected",0
  db ERR_INVALID_BREAK_CONT,"invalid break/continue",0
  db ERR_NONNAKED_SYSTEM,"non-naked system",0
  db ERR_NOT_IMPLEMENTED,"not implemented",0
  db ERR_NEGATIVE_ALLOT,"negative ALLOT is not allowed",0
  db ERR_NO_TEMP_HERE,"not allowed in transient HERE",0
  db ERR_TEMP_HERE_ALREADY,"transient HERE already in use",0
  db ERR_CANNOT_OVERRIDE,"cannot override non-Forth word",0
  db ERR_CANNOT_REPLACE,"no code space to replace word",0
  db ERR_INPUT_TOO_LONG,"input too long",0
  db ERR_THROW_WITHOUT_CATCH,"THROW without CATCH",0
  db ERR_THROW_CHAIN_CORRUPTED,"THROW chain corrupted",0
  db ERR_UNBALANCED_IFDEF,"unbalanced ifdefs",0
  db ERR_ELLIPSIS_FORTH,"`...` must be in a Forth word",0
  db ERR_ELLIPSIS_FIRST,"`...` must be the first in a Forth word",0
  db ERR_USER_ERROR,"user-defined error",0
  db ERR_NONMACRO_ONLY,"definition must be non-macro",0
  db ERR_STRING_TOOLONG,"string too long",0
  db 0,0
urword_end_array


urword_forth "ERROR-MESSAGE",error_message
  ; error 0 is "unknown word"; usually called with the word in HERE
  ; the word should be already dumped, and we won't show the usual "ERROR:" prompt
  UF dup errid_unknown_word equal
  ur_if
    ; unknown word
    UF par_err_msg_table par_find_message
    ur_ifnot
      UF drop strlit "pardon?"
    ur_endif
    UF 63 emit space type
  ur_else
    ; look for error message in the table
    UF qendcr
    ur_if
      UF space
    ur_endif
    UF pardottype "ERROR"
    UF par_err_msg_table par_find_message
    ur_if
      UF pardottype ": " type
    ur_else
      UF pardottype " #"
      UF base @ swap decimal 0 dotr base !
    ur_endif
  ur_endif
urword_end


urword_forth "ERROR-LINE.",error_line_dot
  ;UF tib_calc_currline qdup
  UF tiblineno @ qdup
  ur_if
    UF pardottype " around line #"
    ;; if we just read CR, go back one line
    UF par_last_read_char @ dup 10 equal swap 13 equal or
    ur_if
      UF 1dec 1 max
    ur_endif
    UF base @ rpush decimal dot rpop base !
    UF par_tib_curr_fname count qdup
    ur_if
      UF pardottype 'of file "' type 34 emit space
    ur_else
      UF drop
    ur_endif
  ur_endif
urword_end

urword_forth "(ERROR)",par_error
urword_noreturn
urword_hidden
;; ( code )
    ;ur_bp
  UF decimal  ; because why not
  UF error_message
  UF error_line_dot
  ; print stacks depth
    UF base @ rpush decimal
    UF pardottype " D:"
    UF sp0 @ spget - 2 sar dot
    UF pardottype "R:"
    UF rp0 @ rpget - 2 sar dot
    ;UF pardottype ")"
    UF cr
    UF rpop base !
  UF cr abort
  ;; just in case it returns
  UF 1 nbye
urword_end


urword_forth "?ERROR",qerror
;; ( flag code -- )
;; issue an error message number n, if the boolean flag is true
  UF swap
  ur_if
    UF error
  ur_endif
  UF drop
urword_end


urword_forth "(ABORT-CLEANUP)",par_abort_cleanup
urword_hidden
  UF spset0
  UF rpeek rpset0
  UF state 0poke
  UF temp_pool_reset
  UF debug_info_reset
  UF tload_verbose_default
  urto tload_verbose
  UF dp_temp 0poke
  UF rpush
urword_end

urword_forth "(ABORT)",par_abort
urword_noreturn
urword_hidden
  UF abort_cleanup
  UF main_loop
  ;; just in case it returns
  UF bye
urword_end


urword_forth ".STACK",dotstack
;; dump data stack (unlimited depth)
  UF depth dup 0 less
  ur_if
    UF pardottype "stack underflowed" cr drop exit
  ur_endif
  UF qdup
  ur_ifnot
    UF pardottype "stack empty" cr exit
  ur_endif
  UF dup pardottype "stack depth: " dot cr
  UF 0
  ur_do
    UF depth 1dec i - pick
    UF dup dot pardottype "| " udot cr
  ur_loop
urword_end
