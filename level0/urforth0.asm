;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
include "k8utils.inca"

format ELF executable 3
entry urforth_entry_point

segment interpreter readable
  db "/lib/ld-linux.so.2",0


segment dynamic readable
; the only thing we need is .so management functions, so we'll create
; a very simple import table for "libdl.so", with 3 imports:
; "dlopen", "dlclose", "dlsym"

  DT_NULL    = 0
  DT_NEEDED  = 1
  DT_HASH    = 4
  DT_STRTAB  = 5
  DT_SYMTAB  = 6
  DT_STRSZ   = 10
  DT_SYMENT  = 11
  DT_REL     = 17
  DT_RELSZ   = 18
  DT_RELENT  = 19

  ELF32_SYM_SIZE = 4+4+4+1+1+2
  ELF32_REL_SIZE = 4+4

; create minimalistic Elf header with libdl.so import
  dd DT_NEEDED,elfhead_str_libdl-elfhead_strtab
  dd DT_STRTAB,elfhead_strtab
  dd DT_STRSZ,elfhead_strsz
  dd DT_SYMTAB,elfhead_symtab
  dd DT_SYMENT,ELF32_SYM_SIZE
  dd DT_REL,elfhead_rel
  dd DT_RELSZ,elfhead_relsz
  dd DT_RELENT,ELF32_REL_SIZE
  dd DT_HASH,elfhead_hash
  dd DT_NULL,0

segment executable readable writeable
urforth_code_base_addr = $$ and 0xfffff000

elfhead_symtab:
  ; NULL import, should always be here
    dd elfhead_str_null-elfhead_strtab
    dd 0     ; value
    dd 0     ; size
    db 0x12  ; (STB_GLOBAL<<4)|STT_FUNC
    db 0     ; other
    dw 0     ; shndx
  ; import "dlopen"
    dd elfhead_str_dlopen-elfhead_strtab
    dd 0     ; value
    dd 0     ; size
    db 0x12  ; (STB_GLOBAL<<4)|STT_FUNC
    db 0     ; other
    dw 0     ; shndx
  ; import "dlclose"
    dd elfhead_str_dlclose-elfhead_strtab
    dd 0     ; value
    dd 0     ; size
    db 0x12  ; (STB_GLOBAL<<4)|STT_FUNC
    db 0     ; other
    dw 0     ; shndx
  ; import "dlsym"
    dd elfhead_str_dlsym-elfhead_strtab
    dd 0     ; value
    dd 0     ; size
    db 0x12  ; (STB_GLOBAL<<4)|STT_FUNC
    db 0     ; other
    dw 0     ; shndx

elfhead_strtab:
  elfhead_str_null db 0
  elfhead_str_libdl db "libdl.so",0
  elfhead_str_dlopen db "dlopen",0
  elfhead_str_dlclose db "dlclose",0
  elfhead_str_dlsym db "dlsym",0
elfhead_strsz = $-elfhead_strtab

  ; importer will use this to fix relocations
elfhead_rel:
  ; dlopen
    dd elfimp_dlopen
    dd 0x0101   ; high bit is symbol index, low bit is R_386_32
  ; dlclose
    dd elfimp_dlclose
    dd 0x0201   ; high bit is symbol index, low bit is R_386_32
  ; dlsym
    dd elfimp_dlsym
    dd 0x0301   ; high bit is symbol index, low bit is R_386_32
elfhead_relsz = $-elfhead_rel

  ; fake import hash table with one bucket
elfhead_hash:
  dd 1,4       ; size of bucket and size of chain
  dd 0         ; fake bucket, just one hash value
  times 4 dd % ; chain for all symbol table entries


elfhead_size = $-urforth_code_base_addr
;display_hex elfhead_size
;display 10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Forth system wants it like this
;segment readable writeable executable
; ld.so will add import symbol offsets here
elfhead_impstart = $
elfimp_dlopen dd 0
elfimp_dlclose dd 0
elfimp_dlsym dd 0
elfhead_impend = $
elfhead_implen = elfhead_impend-elfhead_impstart

db 'Alice!'


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
include "urforth_dprint.asm"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
URFORTH_DEBUG = 0
;; use `urcall urforth_bp` or `ur_bp` for manual breakpoint

URFORTH_EXTRA_STACK_CHECKS = 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
URFORTH_ALIGN_HEADERS = 0

WLIST_HASH_BITS = 6

; size in items
DSTACK_SIZE = 65536
RSTACK_SIZE = 65536

TTYLOW_ALLOW_BUFFERED = 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; register usage:
;;   ebp: return stack
;;   esp: data stack
;;   esi: instruction pointer
;;   ecx: TOS
;;
;; direction flag must NOT be set!


;; WARNING! simply changing this definitions won't work!
;;          they are here just to make the code looks better!
TOS equ ecx
EIP equ esi
ERP equ ebp
FREEREG equ edx


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
if WLIST_HASH_BITS < 0
  err "invalid WLIST_HASH_BITS: should be [0..8]"
end if

if WLIST_HASH_BITS > 8
  err "invalid WLIST_HASH_BITS: should be [0..8]"
end if

WLIST_HASH_CELLS = 1 shl WLIST_HASH_BITS
WLIST_HASH_BYTES = WLIST_HASH_CELLS*4
WLIST_HASH_MASK = (1 shl WLIST_HASH_BITS)-1

include "urforth0_mac.asm"
include "urforth0_mac_hlconds.asm"

;; TIB buffer is placed after the main memory allocated with BRK
;; so we will overallocate a little
;;;
;;;urword_var "(TIB-BUFFER)",par_tib_buffer
;;;urword_hidden
;;;; for debug, i'll put counter here
;;;; this also used as compile-time structured stack
;;;  dd  ?
;;;fdata_tib:
;;;  db  1024 dup(?)
;;;fdata_tib_end = $
;;;  db  ?
;;;  db  ?
;;;urfhi_csp = fdata_tib_end
;;;urword_end_array

urword_const "(ELF-EP)",par_elf_ep,urforth_entry_point
urword_hidden


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(URFORTH-SEGFAULT-CODEBLOCK)",par_urforth_nocall_segfault
urword_hidden
urword_codeblock
include "urforth0_segfault.asm"
urword_end

if URFORTH_DEBUG
urword_code "(URFORTH-DEBUGGER-CODEBLOCK)",par_urforth_nocall_debugger
urword_hidden
urword_codeblock
  include "urforth0_dbg.asm"
  ; breakpoint
urforth_next_ptr: dd urforth_next_normal
  ; normal next
urforth_next_normal:
  jp    eax
urword_end
end if


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(URFORTH-STARTUP-CODEBLOCK)",par_urforth_nocall_startup
urword_hidden
urword_codeblock
urforth_entry_point:
  ; save argc
  ld    ecx,[esp]
  ld    [fval_argc_data],ecx
  ; save argv
  ld    eax,esp
  add   eax,4
  ld    [fconst_argv_data],eax
  ; calc envp address
  inc   ecx    ; skip final 0 argument
  shl   ecx,2
  add   eax,ecx
  ; store envp
  ld    [fconst_envp_data],eax

  xor   eax,eax
  ; push several values for safety (so data stack underflow won't destroy cli args and such)
  ld    ecx,64
@@:
  push  eax
  loop  @b

 if 0
  ; allocate memory for dictionary
  ; binary size should be equal to DP
  ld    eax,[fval_init_mem_size_data]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [fvar_dpend_data],ebx
 end if

  ; allocate memory for TIB
  ld    eax,[fval_par_default_tib_size_data]
  ; some more bytes for safety
  add   eax,128
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  add   eax,32  ; reserve some TIB memory for various needs
  ld    [fval_par_default_tib_data],eax

  ; allocate memory for PAD
  ld    eax,[fconst_pad_area_resv_size_data]
  add   eax,[fconst_pad_area_size_data]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  add   eax,[fconst_pad_area_resv_size_data]
  ld    [fvar_pad_area_data],eax

  ; allocate memory for debug buffer
  ld    eax,[fval_dbuf_size_data]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [fval_dbuf_addr_data],eax

  ; allocate memory for tty buffer
  if TTYLOW_ALLOW_BUFFERED
  ld    dword [fval_par_ttylow_buffer_pos_data],0
  ld    dword [fval_par_ttylow_buffer_addr_data],0
  cp    dword [fval_par_ttylow_use_buffer_data],1
  jr    c,@f
  ld    eax,[fval_par_ttylow_buffer_size_data]
  or    eax,eax
  jr    z,@f
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [fval_par_ttylow_buffer_addr_data],eax
  end if

  ; allocate memory for current file we are interpreting
  ld    eax,[fconst_par_tib_curr_fname_size_data]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [fval_par_tib_curr_fname_data],eax
  ld    dword [eax],0  ; no file

  ; allocate memory for debugger/segfault stack
  ld    eax,1024
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ;; align it
  and   ebx,0xfffffff0
  ld    [urfsegfault_stack_bottom],ebx

  ; prepare data stack (use "push" to trigger stack pages allocation)
  mov   edx,esp   ; save current stack pointer
  xor   eax,eax

  mov   ecx,DSTACK_SIZE
.dstack_clear_loop:
  push  eax
  loop  .dstack_clear_loop

  ; prepare return stack (use "push" to trigger stack pages allocation)
  mov   ERP,esp   ; setup initial return stack pointer
  mov   ecx,RSTACK_SIZE
.rstack_clear_loop:
  push  eax
  loop  .rstack_clear_loop

  ; restore stack
  mov   esp,edx

  ; save stack bottoms
  mov   [fvar_sp0_data],esp
  mov   [fvar_rp0_data],ERP

  call  urforth_setup_segfault_handler

  ; and start execution
  jmp   fword_cold_firsttime

;; allocate memory via BRK
;; IN:
;;   EAX: size
;; OUT:
;;   EAX: start address (first byte)
;;   EBX: end address (*after* the last byte)
;;   other registers and flags are dead (except ESP)
.brkalloc:
  ; save size (overallocate a little, for safety)
  add   eax,64
  push  eax

  ; get current BRK address
  ld    eax,45       ; brk
  xor   ebx,ebx
  syscall
  push  eax
  ; [esp]: start address
  ; [esp+4]: alloc size

  ; allocate
  ld    ebx,eax
  add   ebx,[esp+4]
  ld    eax,45       ; brk
  syscall
  ld    ebx,eax      ; EBX=end address
  ; check for OOM
  ld    eax,[esp]    ; start address
  add   eax,[esp+4]  ; alloc size
  cp    ebx,eax
  jp    c,.startup_oom
  ; start address
  pop   eax
  ; calc end address, to be precise
  pop   ebx
  ; remove overallocated safety margin from end address
  sub   ebx,64
  add   ebx,eax
  ret

.startup_oom:
  ; print error and exit
  mov   eax,4     ; write
  mov   ebx,2     ; stderr
  mov   ecx,.fatal_oom_msg
  mov   edx,.fatal_oom_msg_len
  syscall

  mov   eax,1     ; exit
  mov   ebx,1
  syscall

.fatal_oom_msg: db "FATAL: out of memory!",10
.fatal_oom_msg_len = $-.fatal_oom_msg
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(URFORTH-DOFORTH-CODEBLOCK)",par_urforth_nocall_doforth
urword_hidden
urword_codeblock
;_doforth:
  rpush EIP
  pop   EIP
  urnext
urword_end

urword_code "(URFORTH-DOCONST-CODEBLOCK)",par_urforth_nocall_doconst
urword_hidden
urword_codeblock
;_doconst:
  xchg  TOS,[esp]
  mov   TOS,[TOS]
  urnext
urword_end

urword_code "(URFORTH-DOVAR-CODEBLOCK)",par_urforth_nocall_dovar
urword_hidden
urword_codeblock
;_dovar:
  xchg  TOS,[esp]
  urnext
urword_end

urword_code "(URFORTH-DOVALUE-CODEBLOCK)",par_urforth_nocall_dovalue
urword_hidden
urword_codeblock
;_dovalue:
  ;jmp   _doconst
  xchg  TOS,[esp]
  mov   TOS,[TOS]
  urnext
urword_end

urword_code "(URFORTH-DODEFER-CODEBLOCK)",par_urforth_nocall_dodefer
urword_hidden
urword_codeblock
;_dodefer:
  pop   eax
  mov   FREEREG,[eax]
  jmp   FREEREG
urword_end

urword_code "(URFORTH-DODOES-CODEBLOCK)",par_urforth_nocall_dodoes
urword_hidden
urword_codeblock
;_dodoes:
  ; pfa is on the stack
  ; EAX is new VM IP
  xchg  TOS,[esp]
  rpush EIP
  mov   EIP,eax
  urnext
urword_end

urword_code "(URFORTH-DOOVERRIDE-CODEBLOCK)",par_urforth_nocall_dooverride
urword_hidden
urword_codeblock
;_dooverride:
  rpush EIP
  pop   EIP
  xchg  TOS,[esp]
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; use this subroutine to call a forth word from a machine code
;; EAX should point to the cfa
;; TOS and other things should be set accordingly
;; direction flag should be cleared
;; no registers are preserved
ur_mc_fcall:
  ; move mc return address to rstack
  pop   FREEREG
  rpush FREEREG
  ; push current EIP to rstack
  rpush EIP
  ; set new EIP
  mov   EIP,ur_mc_fcall_justexit
  ; turn off debugger temporarily, because the debugger is using this
  if URFORTH_DEBUG
  ld    FREEREG,[urfdebug_active_flag]
  rpush FREEREG
  ; special flag, means "no breakpoint checks"
  ld    dword [urfdebug_active_flag],0xffffffff
  end if
  ; and execute the word
  jp    eax
ur_mc_fcall_justexit:
  dd  ur_mc_fcall_fakeret_code
ur_mc_fcall_fakeret_code:
  ; restore debugger state
  if URFORTH_DEBUG
  rpop  FREEREG
  ld    dword [urfdebug_active_flag],FREEREG
  end if
  ; restore EIP
  rpop  EIP
  ; restore return address
  rpop  eax
  ; and jump there
  jp    eax


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
if URFORTH_DEBUG
urword_code "(URFORTH-BP)",urforth_bp
urword_hidden
  call  urfdebug_activate_it
  urnext
urword_end
end if

if URFORTH_DEBUG
urword_alias "DBG",activate_debugger,urforth_bp
else
urword_code "DBG",activate_debugger
  urnext
urword_end
end if

if URFORTH_DEBUG
urword_const "(HAS-DEBUGGER?)",par_has_debugger,1
urword_const "(NEXT-REF-ADDR)",par_next_ref_addr,urforth_next_ptr
else
urword_const "(HAS-DEBUGGER?)",par_has_debugger,0
;; so i can avoid conditional compilation
;; it MUST be 0
urword_const "(NEXT-REF-ADDR)",par_next_ref_addr,0
end if


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
include "urforth0_w_syscall.asm"
include "urforth0_w_termio_low.asm"

urword_const "TRUE",true,1
urword_const "FALSE",false,0

urword_const "CELL",cell,4
urword_const "BL",bl,32

urword_value "(DEBUGGER-ON-STACK-UNDERFLOW)",par_dbg_on_sunder,0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
include "urforth0_w_litbase.asm"
include "urforth0_w_stack.asm"
include "urforth0_w_peekpoke.asm"
include "urforth0_w_exceptions.asm"
include "urforth0_w_math_base.asm"
include "urforth0_w_math_muldiv.asm"
include "urforth0_w_math_compare.asm"
include "urforth0_w_math_dbl_min.asm"

include "urforth0_w_osface.asm"

include "urforth0_w_countstr.asm"
include "urforth0_w_cstr_hash.asm"

include "urforth0_w_termio_high.asm"

include "urforth0_w_wordlist.asm"

include "urforth0_w_dp.asm"
include "urforth0_w_temp_pool.asm"
include "urforth0_w_dbginfo.asm"

include "urforth0_w_tib.asm"

include "urforth0_w_numbers.asm"
include "urforth0_w_parse.asm"

include "urforth0_w_errors.asm"

include "urforth0_w_compiler_helpers.asm"
include "urforth0_w_compiler_mid.asm"

include "urforth0_w_create_and_vocs.asm"

include "urforth0_w_save.asm"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "BRK-ALLOC",brk_alloc
;;  ( size -- addr )
;; throws OOM error
  UF dup 0less errid_out_of_memory qerror
  UF par_brkq  ;; ( size addr )
  UF swap qdup
  ur_if
    ;; ( addr size )
    UF over + dup par_sbrk
    UF uless errid_out_of_memory qerror
  ur_endif
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "ARGV-STR",argv_str
;; ( argnum -- addr count )
  UF dup 0 less over argc greatequ or
  ur_if
    UF drop pad 0
  ur_else
    UF 4 umul argv + @ zcount
  ur_endif
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "INTERPRET",interpret
  ur_begin
    UF par_spcheck
    ur_ifnot
      if URFORTH_DEBUG
      UF par_dbg_on_sunder
      ur_if
        ur_bp
      ur_endif
      end if
      UF errid_stack_underflow error
      UF 1 nbye  ;; just in case
    ur_endif
   if 0
    UF parse_name_ex
    ;; ( addr count )
    UF qdup
    ur_ifnot
      UF drop exit  ;; replace with "BREAK"
    ur_endif
   else
    ur_begin
      UF parse_name_ex
      ;; ( addr count )
      UF qdup
    ur_not_while
      UF drop
      ;; for default TIB (i.e. terminal session), "QUIT" will do it for us
      UF tib_is_default
      ur_if
        UF exit ;; replace with "BREAK"
      ur_endif
      UF refill
      ur_ifnot
        UF exit ;; replace with "BREAK"
      ur_endif
    ur_repeat
   end if
    ;; ( addr count )
    UF 2dup wfind_str
    ur_if
      ; i found her!
      UF nrot 2drop
      ; ( cfa )
      ; get flags
      UF dup cfa2nfa nfa2ffa ffapeek
      urlit FLAG_IMMEDIATE
      UF and
      UF state @ 0 equal  ; !0 means "compile"
      UF or
      ur_if
        ; execute
        UF execute
      ur_else
        ; compile
        UF compile_comma
      ur_endif
    ur_else
      ; unknown word, try to parse it as a number
      UF 2dup number
      ur_if
        UF nrot 2drop
        UF literal
      ur_else
        UF type errid_unknown_word error
      ur_endif
    ur_endif
  ur_again
urword_end


urword_forth ".OK",dotok
  UF par_spcheck
  ur_if
    UF pardottype "ok"
  ur_else
    UF errid_stack_underflow error_message
  ur_endif
  UF false rpush  ;; "bracket opened" flag
  ; show stack depth
  UF depth qdup
  ur_if
    UF rpop 1inc rpush
    UF pardottype " ("
    UF base @ rpush 0 dotr rpop base !
  ur_endif
  ; show BASE if it is not decimal
  UF base @ 10 nequ
  ur_if
    UF rpop
    ur_ifnot
      UF pardottype " ("
    ur_endif
    UF true rpush
    UF pardottype "; base:" base @ dup rpush decimal 0 dotr rpop base !
  ur_endif
  UF rpop
  ur_if
    UF 41 emit
  ur_endif
urword_end

urword_forth "QUIT",quit
urword_noreturn
  ur_begin
    UF rpset0
    ;UF state 0poke  ; nope, we want multiline definitions to work
    UF dotok cr
    UF tib_reset
    ; there is no reason to keep debug info activated
    UF debug_info_reset
    UF tload_verbose_default
    urto tload_verbose
    UF refill
    ur_ifnot
      UF bye
    ur_endif
    UF interpret
  ur_again
urword_end


urword_forth "EVALUATE",evaluate
;; ( addr count -- ... )
  UF dup 0 great
  ur_if
    UF par_tibstate_rpush
    UF tibsize !
    UF tib !
    UF inptr 0poke
    UF tiblineno 0poke
    UF interpret
    UF par_tibstate_rpop
  ur_else
    UF 2drop
  ur_endif
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(COLD-BUILD-RC-NAME)",par_cold_build_rc_name
urword_hidden
  UF 0 argv_str str_extract_path qdup
  ur_ifnot
    UF drop strlit "./"
  ur_endif
  UF par_tload_fnpad c4s_copy_ac
  UF 0 argv_str str_extract_base_name par_tload_fnpad c4s_cat_ac
  UF strlit ".rc" par_tload_fnpad c4s_cat_ac
  UF par_tload_fnpad c4s_zterm
urword_end


urword_forth "(COLD-LOAD-RC)",par_cold_load_rc
urword_hidden
  ;; check for "--naked"
  UF argc 1 great
  ur_if
    UF argc 1
    ur_do
      UF i argv_str strlit "--naked" strequ_ci
      ur_if
        UF unloop exit
      ur_endif
      UF 1  ;; argskip
      UF i argv_str strlit "--verbose-libs" strequ_ci
      ur_if
        UF 1
        urto tload_verbose_libs
      ur_endif
      UF i argv_str strlit "--quiet-libs" strequ_ci
      ur_if
        UF 0
        urto tload_verbose_libs
      ur_endif
      UF i argv_str strlit "--verbose-rc" strequ_ci
      ur_if
        UF 1
        urto tload_verbose_rc
      ur_endif
      UF i argv_str strlit "--quiet-rc" strequ_ci
      ur_if
        UF 0
        urto tload_verbose_rc
      ur_endif
      UF i argv_str strlit "--eval" strequ_ci
      ur_if
        UF drop 2
      ur_endif
      UF i argv_str strlit "-e" strequ_ci
      ur_if
        UF drop 2
      ur_endif
    ur_ploop
  ur_endif
  ;; no "--naked", load .rc
  UF par_cold_build_rc_name
  UF par_tload_fnpad count par_is_file
  ur_if
    UF tload_verbose rpush
    ;; be silent
    UF tload_verbose_rc
    urto tload_verbose
    UF par_cold_build_rc_name
    ;; copy it to HERE
    UF par_tload_fnpad here c4s_copy
    UF here count tload
    UF rpop
    urto tload_verbose
  ur_endif
urword_end


urword_forth "CLI-ARG-SKIP",arg_skip
  UF arg_next 1inc
  urto arg_next
urword_end

urword_forth "(COLD-CLI)",par_cold_cli
urword_hidden
  ;; load "urforth.rc" (if present)
  UF par_cold_load_rc
  ;; process CLI args
  ;; don't use DO ... LOOP here, use CLI-ARG-NEXT
  ur_begin
    UF arg_next argc less
    UF arg_next 0great and
  ur_while
    ;; empty arg?
    UF arg_next argv_str arg_skip
    UF qdup
    ur_ifnot
      UF drop
    ur_else
      ;; ( addr count )
      ;; "-..." args?
      UF over cpeek 45 equal
      ur_if
        ;; "-e" or "--eval" ?
        UF 2dup strlit "-e" strequ_ci rpush
        UF strlit "--eval" strequ_ci rpop or
        ;; note that argv is dropped here
        ur_if
          ;; set TIB and eval
          UF arg_next argv_str arg_skip evaluate
        ur_endif
        ;; unknown "-..." args are skipped
      ur_else
        ;; filename arg
        UF tload
      ur_endif
    ur_endif
  ur_repeat
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(.BANNER)",par_dot_banner
urword_hidden
  UF pardottype "UrForth v0.0.1-beta" cr
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called to print a banner after processing CLI args
urword_defer ".BANNER",dot_banner,par_dot_banner

;; called (once!) to process CLI args
urword_defer "PROCESS-CLI-ARGS",process_cli_args,par_cold_cli

;; main program loop, should never return
urword_defer "MAIN-LOOP",main_loop,quit  ;; this word should never return

;; default abort calls ABORT-CLEANUP, and then MAIN-LOOP
urword_defer "ABORT-CLEANUP",abort_cleanup,par_abort_cleanup

;; ye good olde ABORT, vectorized
urword_defer "ABORT",abort,par_abort  ;; this word should never return

;; called when the system needs to abort with error message
;; ( errcode )
urword_defer "ERROR",error,par_error  ;; this word should never return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(COLD-FIRSTTIME)",cold_firsttime
urword_hidden
urword_noreturn
  UF sys_gettickcount_init
  UF cold
  ;; just in case it returns
  UF bye
urword_end


urword_forth "COLD",cold
urword_noreturn
  UF spset0 rpset0
  UF temp_pool_reset tib_reset
  UF tload_verbose_default
  urto tload_verbose
  UF dp_temp 0poke
  UF process_cli @
  ur_if
    UF process_cli 0poke  ; no more
    UF process_cli_args
  ur_endif
  UF dot_banner
  UF main_loop
  ;; just in case it returns
  UF bye
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_value "TLOAD-VERBOSE-LIBS",tload_verbose_libs,1
urword_value "TLOAD-VERBOSE",tload_verbose,1
urword_value "TLOAD-VERBOSE-DEFAULT",tload_verbose_default,1
urword_value "TLOAD-VERBOSE-RC",tload_verbose_rc,0


urword_forth "(TLOAD-FNPAD)",par_tload_fnpad
urword_hidden
  UF pad 2048 +
urword_end


urword_forth "(TLOAD-OPEN-ADD-ALL)",par_tload_open_add_all
urword_hidden
;; ( -- )
  UF par_tload_fnpad c4s_zterm
  UF par_tload_fnpad count par_is_dir
  ur_if
    ;; add trailing slash
    UF par_tload_fnpad count + 1dec cpeek 47 equal
    ur_ifnot
      UF strlit "/" par_tload_fnpad c4s_cat_ac
    ur_endif
    UF strlit "all.f" par_tload_fnpad c4s_cat_ac
    UF par_tload_fnpad c4s_zterm
  ur_endif
urword_end

urword_forth "(TLOAD-OPEN)",par_tload_open
urword_hidden
;; ( addr count -- fd )
;; also, leaves file name as c4str at PAD
  UF dup 0lessequ
  ur_if
    UF 2drop errid_file_not_found error
  ur_endif

  ;; ! will be replaced with binary path
  UF over cpeek 33 equal
  ur_if
    UF 0 argv_str str_extract_path qdup
    ur_ifnot
      UF drop strlit "./"
    ur_endif
    UF par_tload_fnpad c4s_copy_ac
    UF 1dec swap 1inc swap
    UF par_tload_fnpad c4s_cat_ac
  ur_else
    UF par_last_tload_path
    ur_if
      UF 2dup 2rpush
      UF par_last_tload_path par_tload_fnpad c4s_copy
      ;; ( addr count | addr count )
      UF par_tload_fnpad c4s_cat_ac
      UF par_tload_open_add_all
      UF par_tload_fnpad count
      ;; ( addr count | addr count )
      UF o_rdonly 0 par_fopen
      UF dup 0greatequ
      ur_if
        UF rdrop rdrop exit
      ur_endif
      UF drop 2rpop
    ur_endif
    UF par_tload_fnpad c4s_copy_ac
    UF par_tload_open_add_all
  ur_endif
  UF par_tload_open_add_all
  UF par_tload_fnpad count o_rdonly 0 par_fopen
  UF dup 0less
  ur_if
    UF drop endcr pardottype "file: " par_tload_fnpad count type errid_file_not_found error
  ur_endif
urword_end


urword_forth "TLOAD",tload
;; ( addr count -- )
;; load forth source file
;; this loads the whole file into temp pool
  UF par_tload_open
  UF tload_verbose
  ur_if
    UF endcr pardottype "loading: " par_tload_fnpad count type cr
  ur_endif
  UF rpush
  ;; ( | fd )
  UF 0 seek_end rpeek par_lseek
  ;; ( size | fd )
  UF dup 0 less
  ur_if
    UF drop rpop par_fclose drop
    UF errid_file_read_error error
  ur_endif
  ; seek back to file start
  ;; ( size | fd )
  UF 0 seek_set rpeek par_lseek
  ur_if
    UF drop rpop par_fclose drop
    UF errid_file_read_error error
  ur_endif
  ; allocate temp pool space
  ; this will also take care about too big files
  ; FIXME: the file won't be closed if it is too big!
  UF rpop temp_pool_mark rpush rpush
  ;; ( size | poolmark fd )
  UF dup cellinc temp_pool_alloc
  ;; ( size addr | poolmark fd )
  ; write zeroes at the end (just in case)
  UF 2dup + 0poke
  ; load file
  UF 2dup swap rpeek par_fread
  ;; ( size addr readbytes | poolmark fd )
  ; close file
  UF rpop par_fclose drop
  ;; ( size addr readbytes | poolmark )
  ;; ( addr readbytes | poolmark )
  UF rot over - errid_file_read_error qerror
  ;; ( addr readbytes | poolmark )
  UF par_tibstate_rpush
  UF tibsize !
  UF tib !
  UF inptr 0poke
  UF 1 tiblineno poke
  ;; ( | poolmark tibstate... )
    ;; UF par_tload_fnpad count type cr
  ;; store file path
  UF par_tload_fnpad count str_extract_path
  UF qdup
  ur_ifnot
    UF drop strlit "./"
  ur_endif
    ;; UF 2dup type cr
  UF dup cellinc temp_pool_alloc
  UF par_last_tload_path rpush
  urto par_last_tload_path
  UF par_last_tload_path c4s_copy_ac
    ;; UF par_last_tload_path count type cr
  UF cell temp_pool_alloc rpop swap poke
  ;; save current file name, and replace it
  UF par_tib_curr_fname count_only cellinc temp_pool_alloc
  UF rpush  ;; old fname at rstack
  UF par_tib_curr_fname rpeek over count_only cellinc move
  UF par_tload_fnpad par_tib_curr_fname over count_only cellinc move
  ;; process it
  UF interpret
  ;; restore old fname
  UF rpop par_tib_curr_fname over count_only cellinc move
  ;; restore path
  UF par_last_tload_path celldec peek
  urto par_last_tload_path
  UF par_tibstate_rpop
  UF rpop temp_pool_release
  ;UF pardottype "done!" cr
  ;UF tib_reset
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "NOOP",noop
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_const "WLIST-HASH-BITS",wlist_hash_bits,WLIST_HASH_BITS

urword_var "DP",dp,fdata_freespace
urword_var "DP-END",dpend,fdata_freespace
urword_var "DP-TEMP",dp_temp,0  ;; this is used to temporarily change HERE
urword_var "SP0",sp0,0
urword_var "RP0",rp0,0

;; pad is allocated with BRK; "#PAD-AREA-RESV" is the space BEFORE "PAD-AREA"
urword_var "PAD-AREA",pad_area,0  ;; will be set by startup code
urword_const "#PAD-AREA-RESV",pad_area_resv_size,2048
urword_const "#PAD-AREA",pad_area_size,4096

urword_value "(INIT-MEM-SIZE)",init_mem_size,1024*1024*2
urword_hidden

urword_var "STATE",state,0

urword_value "ARGC",argc,0
urword_const "ARGV",argv,0
urword_const "ENVP",envp,0

urword_const "URFORTH-LEVEL",par_urforth_level,0

;; index of the next CLI arg to process
;; set to 0, negative or ARGC to stop further processing
urword_value "CLI-ARG-NEXT",arg_next,1

;; this is so COLD will process CLI args only once
urword_var "(PROCESS-CLI-ARGS)",process_cli,1
urword_hidden

; mostly controls number parsing for now: "#" prefix is decimal in shit2012
; bit0: allow 2012 number prefix idiocity and number signs
; bit1: use non-FIG VARIABLE
urword_value "(SHIT-2012-IDIOCITY)",shit2012shit,0
urword_hidden


urword_const "(CODE-BASE-ADDR)",par_code_base_addr,urforth_code_base_addr
urword_hidden

urword_const "(CODE-IMPORTS-ADDR)",par_code_imports_addr,elfhead_impstart
urword_hidden

urword_const "(CODE-IMPORTS-SIZE)",par_code_imports_size,elfhead_implen
urword_hidden

urword_const "(CODE-IMPORTS-END-ADDR)",par_code_imports_end_addr,elfhead_impend
urword_hidden

urword_const "(CODE-ENTRY-ADDR)",par_code_entry_addr,urforth_entry_point
urword_hidden

; it should be here
urforth_last_word_end_late = $
; it should be here
urforth_last_word_lfa_late = urforth_last_word_lfa

db 'Miriel!'


display "UrForth words: "
display_dec urforth_word_count
display 10

fdata_freespace = $


;; patch FORTH vocabulary hash table
if WLIST_HASH_BITS
fht_patch_count = 0
fht_patch_dpos = forth_voc_hashtable
fht_patch_v = 0
while fht_patch_count < WLIST_HASH_BYTES
  load fht_patch_v dword from urforth_forth_hashtable:urforth_forth_hash_offset+fht_patch_count
  store dword fht_patch_v at fht_patch_dpos
  fht_patch_count = fht_patch_count+4
  fht_patch_dpos = fht_patch_dpos+4
end while
end if


;; reserve BSS space, because BRK is completely broken on "modern" distros
rb  1024*1024*1
end_dict = $
store dword end_dict at fvar_dpend_data
