;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "@",peek
;; ( addr -- [addr] ) -- 4 bytes
  mov   TOS,[TOS]
  urnext
urword_end

urword_code "W@",wpeek
;; ( addr -- [addr] ) -- 2 bytes
  movzx TOS,word [TOS]
  urnext
urword_end

urword_code "C@",cpeek
;; ( addr -- [addr] ) -- 1 byte
  movzx TOS,byte [TOS]
  urnext
urword_end

urword_code "!",poke
;; ( value addr -- [addr]=value ) -- 4 bytes
  pop   eax   ; value
  mov   [TOS],eax
  pop   TOS
  urnext
urword_end

urword_code "W!",wpoke
;; ( value addr -- [addr]=value ) -- 2 bytes
  pop   eax   ; value
  mov   word [TOS],ax
  pop   TOS
  urnext
urword_end

urword_code "C!",cpoke
;; ( value addr -- [addr]=value ) -- 1 byte
  pop   eax   ; value
  mov   byte [TOS],al
  pop   TOS
  urnext
urword_end


;; sane little-endian
urword_code "2@LE",2peek
;; ( addr -- [addr] [addr+4] ) -- 8 bytes
  mov   eax,[TOS]
  push  eax
  mov   TOS,[TOS+4]
  urnext
urword_end

;; sane little-endian
urword_code "2!LE",2poke
;; ( n0 n1 addr -- [addr]=n0 [addr+4]=n1 ) -- 8 bytes
  pop   eax   ; d-high
  mov   [TOS+4],eax
  pop   eax
  mov   [TOS],eax
  pop   TOS
  urnext
urword_end

;; ANS idiots!
urword_code "2@",2peek_morons
;; ( addr -- [addr+4] [addr] ) -- 8 bytes
  mov   eax,[TOS+4]
  push  eax
  mov   TOS,[TOS]
  urnext
urword_end

;; ANS idiots!
urword_code "2!",2poke_morons
;; ( n0 n1 addr -- [addr]=n1 [addr+4]=n0 ) -- 8 bytes
  pop   eax   ; d-high
  mov   [TOS],eax
  pop   eax
  mov   [TOS+4],eax
  pop   TOS
  urnext
urword_end


urword_code "0!",0poke
;; ( addr -- [addr]=0 ) -- 4 bytes
  mov   dword [TOS],0
  pop   TOS
  urnext
urword_end

urword_code "0W!",0wpoke
;; ( addr -- [addr]=0 ) -- 2 bytes
  mov   word [TOS],0
  pop   TOS
  urnext
urword_end

urword_code "0C!",0cpoke
;; ( addr -- [addr]=0 ) -- 1 byte
  mov   byte [TOS],0
  pop   TOS
  urnext
urword_end


urword_code "1!",1poke
;; ( addr -- [addr]=1 ) -- 4 bytes
  mov   dword [TOS],1
  pop   TOS
  urnext
urword_end

urword_code "1W!",1wpoke
;; ( addr -- [addr]=1 ) -- 2 bytes
  mov   word [TOS],1
  pop   TOS
  urnext
urword_end

urword_code "1C!",1cpoke
;; ( addr -- [addr]=1 ) -- 1 byte
  mov   byte [TOS],1
  pop   TOS
  urnext
urword_end


urword_code "+!",addpoke
;; ( value addr -- [addr]=value ) -- 4 bytes
  pop   eax   ; value
  add   [TOS],eax
  pop   TOS
  urnext
urword_end

urword_code "+W!",addwpoke
;; ( value addr -- [addr]=value ) -- 2 bytes
  pop   eax   ; value
  add   [TOS],ax
  pop   TOS
  urnext
urword_end

urword_code "+C!",addcpoke
;; ( value addr -- [addr]=value ) -- 1 byte
  pop   eax   ; value
  add   [TOS],al
  pop   TOS
  urnext
urword_end


urword_code "-!",subpoke
;; ( value addr -- [addr]=value ) -- 4 bytes
  pop   eax   ; value
  sub   [TOS],eax
  pop   TOS
  urnext
urword_end

urword_code "-W!",subwpoke
;; ( value addr -- [addr]=value ) -- 2 bytes
  pop   eax   ; value
  sub   [TOS],ax
  pop   TOS
  urnext
urword_end

urword_code "-C!",subcpoke
;; ( value addr -- [addr]=value ) -- 1 byte
  pop   eax   ; value
  sub   [TOS],al
  pop   TOS
  urnext
urword_end


urword_code "1+!",1addpoke
;; ( addr -- [addr]=[addr]+1 ) -- 4 bytes
  inc   dword [TOS]
  pop   TOS
  urnext
urword_end

urword_code "1+W!",1addwpoke
;; ( addr -- [addr]=[addr]+1 ) -- 2 bytes
  inc   word [TOS]
  pop   TOS
  urnext
urword_end

urword_code "1+C!",1addcpoke
;; ( addr -- [addr]=[addr]+1 ) -- 1 byte
  inc   byte [TOS]
  pop   TOS
  urnext
urword_end


urword_code "1-!",1subpoke
;; ( addr -- [addr]=[addr]-1 ) -- 4 bytes
  dec   dword [TOS]
  pop   TOS
  urnext
urword_end

urword_code "1-W!",1subwpoke
;; ( addr -- [addr]=[addr]-1 ) -- 2 bytes
  dec   word [TOS]
  pop   TOS
  urnext
urword_end

urword_code "1-C!",1subcpoke
;; ( addr -- [addr]=[addr]-1 ) -- 1 byte
  dec   byte [TOS]
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_alias "W+!",addwpoke_alias,addwpoke
urword_alias "C+!",addcpoke_alias,addcpoke
urword_alias "W-!",subwpoke_alias,subwpoke
urword_alias "C-!",subcpoke_alias,subcpoke

urword_alias "1W+!",1addwpoke_alias,1addwpoke
urword_alias "1C+!",1addcpoke_alias,1addcpoke
urword_alias "1W-!",1subwpoke_alias,1subwpoke
urword_alias "1C-!",1subcpoke_alias,1subcpoke


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "OR!",orpoke
;; ( value addr -- [addr]|=value ) -- 4 bytes
  pop   eax
  or    dword [TOS],eax
  pop   TOS
  urnext
urword_end

urword_code "XOR!",xorpoke
;; ( value addr -- [addr]^=value ) -- 4 bytes
  pop   eax
  xor   dword [TOS],eax
  pop   TOS
  urnext
urword_end

urword_code "AND!",andpoke
;; ( value addr -- [addr]^=value ) -- 4 bytes
  pop   eax
  and   dword [TOS],eax
  pop   TOS
  urnext
urword_end

urword_code "~AND!",notandpoke
;; ( value addr -- [addr]^=~value ) -- 4 bytes
  pop   eax
  xor   eax,0xffffffff
  and   dword [TOS],eax
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "CTOGGLE",ctoggle
;; ( addr value -- )
;; byte [addr] ^= value
  pop   eax
  xor   [eax],cl
  pop   TOS
  urnext
urword_end

urword_code "WTOGGLE",wtoggle
;; ( addr value -- )
;; word [addr] ^= value
  pop   eax
  xor   [eax],cx
  pop   TOS
  urnext
urword_end

urword_code "TOGGLE",toggle
;; ( addr value -- )
;; dword [addr] ^= value
  pop   eax
  xor   [eax],TOS
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "CMOVE",cmove
;; ( source dest count -- )
  pop   eax     ; dest
  pop   FREEREG ; src
  test  TOS,0x80000000
  jr    nz,fword_cmove_done
  or    TOS,TOS
  jr    z,fword_cmove_done
  push  esi     ; this is used as EIP
  mov   esi,FREEREG
  mov   edi,eax
  ; TOS is ECX, and this is our length too
  rep movsb
  pop   esi
fword_cmove_done:
  pop   TOS
  urnext
urword_end

;; can be used to make some room
;; moves from the last byte to the first one
urword_code "CMOVE>",cmove_back
;; ( source dest count -- )
  pop   eax     ; dest
  pop   FREEREG ; src
  test  TOS,0x80000000
  jr    nz,fword_cmove_back_done
  or    TOS,TOS
  jr    z,fword_cmove_back_done
  push  esi     ; this is used as EIP
  mov   esi,FREEREG
  mov   edi,eax
  ; TOS is ECX, and this is our length too
  ; move pointers
  dec   ecx
  add   esi,ecx
  add   edi,ecx
  inc   ecx
  std
  rep movsb
  cld
  pop   esi
fword_cmove_back_done:
  pop   TOS
  urnext
urword_end

urword_forth "MOVE",move
;; ( from to len -- )
;; uses CMOVE or CMOVE>
  UF rpush
  UF 2dup equal
  ur_if
    UF 2drop rdrop
  ur_else
    UF 2dup ugreat
    ur_if
      ;; from > to: use normal CMOVE
      UF rpop cmove
    ur_else
      ;; from <= to: use reverse CMOVE>
      UF rpop cmove_back
    ur_endif
  ur_endif
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "FILL",fill
;: ( addr count byte -- )
  cp    dword [esp],1
  jl    @f
  mov   eax,TOS
  pop   ecx
  pop   edi
  rep stosb
  pop   TOS
  urnext
@@:
  add   esp,4+4
  pop   TOS
  urnext
urword_end

urword_forth "ERASE",erase
;: ( addr count -- )
  UF 0 fill
urword_end

urword_forth "BLANK",blank
;: ( addr count -- )
  UF 32 fill
urword_end
