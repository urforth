;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

urword_value "STDIN-FD",stdin_fd,0
urword_value "STDOUT-FD",stdout_fd,1
urword_value "STDERR-FD",stderr_fd,2

if TTYLOW_ALLOW_BUFFERED
urword_value "(TTY-LOW-USE-BUFFER)",par_ttylow_use_buffer,1
urword_hidden
urword_value "(TTY-LOW-BUFFER-ADDR)",par_ttylow_buffer_addr,0
urword_hidden
urword_value "(TTY-LOW-BUFFER-POS)",par_ttylow_buffer_pos,0
urword_hidden
urword_value "(TTY-LOW-BUFFER-SIZE)",par_ttylow_buffer_size,32768
urword_hidden
end if

urword_var "(EMIT-COL)",par_emit_col,0
urword_hidden

urword_code "(TTY-LOW-FLUSH)",par_ttylow_flush
urword_hidden
;; ( -- )
  call  fttylow_do_flush
  urnext

fttylow_do_flush:
if TTYLOW_ALLOW_BUFFERED
  ld    eax,[fval_par_ttylow_buffer_pos_data]
  or    eax,eax
  jr    z,.done
  push  EIP
  push  TOS
  mov   eax,4    ; function
  mov   ebx,[fval_stdout_fd_data]
  mov   ecx,[fval_par_ttylow_buffer_addr_data]
  mov   edx,[fval_par_ttylow_buffer_pos_data]
  syscall
  pop   TOS
  pop   EIP
  ld    dword [fval_par_ttylow_buffer_pos_data],0
.done:
end if
  ret
urword_end

urword_code "(RESET-EMIT-COL)",par_reset_emitcol
urword_hidden
urword_uses par_ttylow_flush
;; ( -- )
  ;call  fttylow_do_flush
  ld    dword [fvar_par_emit_col_data],0
  urnext
urword_end

;; this will also flush buffer
urword_code "(EMIT-FIX-COL-CODEBLOCK)",par_emit_fixcol_codeblock
;; TOS: ch
;; EAX: dead
urword_hidden
urword_codeblock
urword_uses par_ttylow_flush
emit_fix_col_subr:
  cp    cl,10
  jr    nz,@f
  ;call  fttylow_do_flush
  ld    dword [fvar_par_emit_col_data],0
  ret
@@:
  cp    cl,13
  jr    nz,@f
  ;call  fttylow_do_flush
  ld    dword [fvar_par_emit_col_data],0
  ret
@@:
  if TTYLOW_ALLOW_BUFFERED = 0
  cp    cl,9
  jr    nz,@f
  ld    eax,[fvar_par_emit_col_data]
  or    eax,7
  inc   eax
  ld    [fvar_par_emit_col_data],eax
  ret
@@:
  end if
  cp    cl,8
  jr    nz,@f
  cp    dword [fvar_par_emit_col_data],0
  jr    z,.skipdec
  dec   dword [fvar_par_emit_col_data]
.skipdec:
  ret
@@:
  cp    cl,32
  jr    c,@f
  inc   dword [fvar_par_emit_col_data]
@@:
  ret
urword_end

urword_code "(EMIT-FIX-COL)",par_emit_fixcol
urword_hidden
;; ( ch -- ch )
  call  emit_fix_col_subr
  urnext
urword_end

urword_code "(EMIT)",paremit
urword_uses par_ttylow_flush
;; ( ch -- )
  call  emit_fix_col_subr
  if TTYLOW_ALLOW_BUFFERED
  cp    cl,9
  jr    nz,.normal
  ; tab
.tabloop:
  ld    cl,32
  call  .normal
  ld    eax,[fvar_par_emit_col_data]
  inc   eax
  ld    [fvar_par_emit_col_data],eax
  and   al,7
  jr    nz,.tabloop
  pop   TOS
  urnext

.normal:
  call  .again
.quit:
  pop   TOS
  urnext

.again:
  cp    dword [fval_par_ttylow_use_buffer_data],1
  jr    c,.turnedoff
  ld    eax,[fval_par_ttylow_buffer_pos_data]
  cp    eax,[fval_par_ttylow_buffer_size_data]
  jr    nc,.toomuch
  add   eax,[fval_par_ttylow_buffer_addr_data]
  ld    [eax],cl
  inc   eax
  inc   dword [fval_par_ttylow_buffer_pos_data]
  jr    .done
.toomuch:
  call  fttylow_do_flush
  jr    .again
.turnedoff:
  call  fttylow_do_flush
  end if

  push  EIP
  push  TOS
  mov   eax,4    ; function
  mov   ebx,[fval_stdout_fd_data]
  mov   ecx,esp  ; address
  mov   edx,1    ; length
  syscall
  pop   TOS
  pop   EIP
.done:
  if TTYLOW_ALLOW_BUFFERED
  ret
  else
  pop   TOS
  urnext
  end if
urword_end

urword_code "(CR)",parcr
urword_uses paremit
;; ( -- )
  push  TOS
  ld    TOS,10
  jr    fword_paremit
urword_end

urword_code "(BELL)",parbell
urword_uses paremit
;; ( -- )
  push  EIP
  push  TOS
  ld    eax,7
  push  eax
  mov   eax,4    ; function
  mov   ebx,[fval_stdout_fd_data]
  mov   ecx,esp  ; address
  mov   edx,1    ; length
  syscall
  pop   eax
  pop   TOS
  pop   EIP
  urnext
urword_end

urword_code "(ENDCR)",parendcr
urword_uses parcr
;; ( -- )
  cp    dword [fvar_par_emit_col_data],0
  jr    z,@f
  jr    fword_parcr
@@:
  urnext
urword_end

urword_code "(?ENDCR)",parqendcr
urword_uses parcr
;; ( -- )
  push  TOS
  cp    dword [fvar_par_emit_col_data],0
  setnz cl
  movzx ecx,cl
  urnext
urword_end


if 0
urword_code "(TYPE)",partype
urword_uses par_ttylow_flush
;; ( addr length -- )
  test  TOS,0x80000000
  jnz   .fucked_length
  or    TOS,TOS
  jz    .fucked_length

  push  TOS
  push  esi
  call  fttylow_do_flush
  ld    esi,[esp+4*2]
  ld    eax,TOS
.fixcol_loop:
  ld    cl,[esi]
  call  emit_fix_col_subr
  inc   esi
  dec   eax
  jr    nz,.fixcol_loop
  pop   esi
  pop   TOS

  mov   edx,TOS
  mov   eax,4
  mov   ebx,[fval_stdout_fd_data]
  pop   ecx
  push  EIP
  syscall
  pop   EIP
  pop   TOS
  urnext
.fucked_length:
  pop   TOS
  pop   TOS
  urnext
urword_end
end if


urword_code "(GETCH)",pargetch
urword_uses par_ttylow_flush
;; ( -- ch )
;; returns -1 on EOF
  push  TOS
  call  fttylow_do_flush
  xor   eax,eax
  push  eax
  mov   eax,3     ; read
  ld    ebx,[fval_stdin_fd_data]
  mov   ecx,esp   ; address
  mov   edx,1     ; length
  push  EIP
  syscall
  pop   EIP
  pop   TOS       ; read char
  or    eax,eax
  jnz   @f
  mov   TOS,-1    ; oops
@@:
  urnext
urword_end
