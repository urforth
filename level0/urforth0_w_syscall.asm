;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "BYE",bye
urword_noreturn
urword_uses par_ttylow_flush
  call  fttylow_do_flush
  mov   eax,1
  xor   ebx,ebx
  syscall
urword_end

urword_code "N-BYE",nbye
urword_noreturn
urword_uses par_ttylow_flush
;; ( exitcode -- )
  call  fttylow_do_flush
  mov   eax,1
  mov   ebx,TOS
  syscall
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(SYSCALL-0)",par_syscall_0
;; ( num -- res )
  ld    eax,TOS
  push  EIP   ; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(SYSCALL-1)",par_syscall_1
;; ( arg0 num -- res )
  ld    eax,TOS
  pop   ebx
  push  EIP   ; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(SYSCALL-2)",par_syscall_2
;; ( arg0 arg1 num -- res )
  ld    eax,TOS
  pop   ecx
  pop   ebx
  push  EIP   ; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(SYSCALL-3)",par_syscall_3
;; ( arg0 arg1 arg2 num -- res )
  ld    eax,TOS
  pop   edx
  pop   ecx
  pop   ebx
  push  EIP   ; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(SYSCALL-4)",par_syscall_4
;; ( arg0 arg1 arg2 arg3 num -- res )
  ; this cannot be called recursively anyway
  ld    [fword_syscall4_eip_store],EIP
  ld    eax,TOS
  pop   esi
  pop   edx
  pop   ecx
  pop   ebx
  syscall
  ld    EIP,[fword_syscall4_eip_store]
  ld    TOS,eax
  urnext
fword_syscall4_eip_store: dd 0
urword_end

urword_code "(SYSCALL-5)",par_syscall_5
;; ( arg0 arg1 arg2 arg3 arg4 num -- res )
  ; this cannot be called recursively anyway
  ld    [fword_syscall5_eip_store],EIP
  ld    eax,TOS
  pop   edi
  pop   esi
  pop   edx
  pop   ecx
  pop   ebx
  syscall
  ld    EIP,[fword_syscall5_eip_store]
  ld    TOS,eax
  urnext
fword_syscall5_eip_store: dd 0
urword_end


urword_code "(BRK)",par_sbrk
;; ( newaddr -- newaddr )
;; returns current address if the request is invalid
  push  EIP     ; just in case
  ld    eax,45  ; brk
  ld    ebx,TOS ; new address
  syscall
  ld    TOS,eax
  pop   EIP
  urnext
urword_end

urword_forth "(BRK?)",par_brkq
;; ( -- curraddr )
  UF 0 par_sbrk
urword_end
