;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mid-level target compiler words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 var tc-state
0 var tc-(csp)

: tc-!CSP  ( -- )
  sp@ tc-(csp) !
;

: tc-?CSP  ( -- )
    \ endcr sp@ .hex8 space tc-(csp) @ .hex8 space  sp@ tc-(csp) @ - . cr
  sp@ tc-(csp) @ - err-unfinished-definition ?error
;

: tc-?COMP  ( -- )
  tc-state @ not err-compilation-only ?error
;

: tc-?EXEC  ( -- )
  tc-state @ err-execution-only ?error
;

;; CSP check for loops
: tc-csp-loop  ( -- )
  sp@ tc-(csp) @ u> err-unpaired-conditionals ?error
;

: tc-?pairs  ( n1 n2 -- )
  <> err-unpaired-conditionals ?error
;

: tc-?any-pair  ( id v0 v1 -- )
  >r over <>
  swap r> <>
  and err-unpaired-conditionals ?error
;

: tc-?pairs-any-keepid  ( id v0 v1 -- id )
  >r over <>  ;; ( id v0<>id | v1 )
  over r> <>  ;; ( id v0<>id v1<>id )
  and err-unpaired-conditionals ?error
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; usage:
;;  compile 0branch
;;  (mark>)
;;  ...
;;  (resolve>)
;;
;;  (<mark)
;;  ...
;;  compile branch
;;  (<resolve)

;; write "branch to destaddr" address to addr
: tc-(branch-addr!)  ( rva-destaddr rva-addr -- )
  tc-!
;


;; reserve room for branch address, return addr suitable for "tc-(resolve-j>)"
: tc-(mark-j>)  ( -- rva-addr )
  tc-here 0 tc-,
;

;; compile "forward jump" from address to HERE
;; addr is the result of "tc-(mark-j>)"
: tc-(resolve-j>)  ( rva-addr -- )
  tc-here swap tc-(branch-addr!)
;


;; return addr suitable for "tc-(<j-resolve)"
: tc-(<j-mark)  ( -- rva-addr )
  tc-here
;

;; patch "forward jump" address to HERE
;; addr is the result of "tc-(<j-mark)"
: tc-(<j-resolve)  ( rva-addr -- )
  cell tc-n-allot tc-(branch-addr!)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; each of these has one argument
1 constant tc-(ctlid-if)  (hidden)
2 constant tc-(ctlid-else)  (hidden)

3 constant tc-(ctlid-begin)  (hidden)
4 constant tc-(ctlid-while)  (hidden)

5 constant tc-(ctlid-case)  (hidden)
6 constant tc-(ctlid-of)  (hidden)
7 constant tc-(ctlid-endof)  (hidden)
8 constant tc-(ctlid-otherwise)  (hidden)

9 constant tc-(ctlid-do)  (hidden)
10 constant tc-(ctlid-do-break)  (hidden)
11 constant tc-(ctlid-do-continue)  (hidden)

12 constant tc-(ctlid-cblock)  (hidden)
13 constant tc-(ctlid-cblock-interp)  (hidden)

14 constant tc-(ctlid-?do)  (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; jumpcfa-type:
;;   0: 0branch
;;   1: tbranch
;;  <0: branch
hidden:: tc-(compile-typed-branch)  ( jumpcfa-type  -- )
  case
    0 of tc-compile 0branch endof
    1 of tc-compile tbranch endof
   -1 of tc-compile branch endof
    abort" tc-(compile-typed-branch): wut?!"
  endcase
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is NOT immediate!
: tc-LITERAL
  tc-state @ not-?abort" tc-LITERAL: compile-time only!"
  " LIT" tc-compile,-(str)
  tc-,
; ( immediate )

;; this is NOT immediate!
;; addr is NOT rva!
: tc-(str)  ( addr count -- )
  dup cell+ 1+ tc-n-allot  ;; ( addr count rva-dest )
  2dup tc-! cell+
  tc->real swap 2dup 2>r move 2r> + 0c!
; ( immediate )

;; this is NOT immediate!
;; addr is NOT rva!
: tc-SLITERAL  ( addr count -- )
  tc-state @ not-?abort" tc-SLITERAL: compile-time only!"
  " LITSTR" tc-compile,-(str)
  tc-(str)
; ( immediate )
