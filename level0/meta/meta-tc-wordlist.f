;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist management for metacompiler
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-create;  ( -- )
  tc-here tc-latest-nfa tc-nfa->sfa tc-!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word header format:
;; note than name hash is ALWAYS calculated with ASCII-uppercased name
;; (actually, bit 5 is always reset for all bytes, because we don't need the
;; exact uppercase, only something that resembles it)
;; bfa points to next bfa or to 0 (this is "hash bucket pointer")
;; before nfa, we have such "hidden" fields:
;;   dd dfa       ; pointer to the debug data; can be 0 if debug info is missing
;;   dd sfa       ; points *after* the last word byte
;;   dd bfa       ; next word in hashtable bucket; it is always here, even if hashtable is turned off
;;                ; if there is no hashtable, this field is not used
;; lfa:
;;   dd lfa       ; previous word LFA or 0 (lfa links points here)
;;   dd namehash  ; it is always here, and always calculated, even if hashtable is turned off
;; nfa:
;;   dd flags-and-name-len   ; see below
;;   db name      ; no terminating zero or other "termination flag" here
;;   db namelen   ; yes, name length again, so CFA->NFA can avoid guessing
;;   machine code follows
;;   here we usually have CALL to word handler
;;   0xE8, 4-byte displacement
;;   (displacement is calculated from here)
;;
;; first word cell contains combined name length (low byte), argtype and flags (other bytes)
;; layout:
;;   db namelen
;;   db argtype
;;   dw flags
;;
;; i.e. we have 16 bits for flags, and 256 possible argument types. why not.
;;
;; flags:
;;  bit 0: immediate
;;  bit 1: smudge
;;  bit 2: noreturn
;;  bit 3: hidden
;;  bit 4: codeblock
;;  bit 5: vocabulary
;;
;;  argtype is the type of the argument that this word reads from the threaded code.
;;  possible argument types:
;;    0: none
;;    1: branch address
;;    2: cell-size numeric literal
;;    3: cell-counted string
;;    4: cfa of another word
;;    5: cblock
;;    6: vocid

;; addr is NOT RVA!
: tc-(create-str)  ( addr count -- )
  ;; check length
  dup 1 255 bounds? not-?abort" invalid word name"
  ensure-forth-hashtable
  ;; check for duplicate word?
  true ;; UF warning_redefine @
  if
    ;; look only in the current dictionary
    2dup tc-current tc-@ tc-xcfind
    if
      tc-cfa->nfa tc->real dup c@ swap cell+ swap type ." redefined" error-line. cr
    endif
  endif
  [ tc-align-headers ] [IF]
    begin
      tc-here 3 and
    while
      0 tc-c,
    repeat
  [ENDIF]
  ;; allocate dfa
  0 tc-,
  ;; allocate sfa
  0 tc-,
  ;; allocate bfa (it will be patched later)
  0 tc-,
  ;; remember HERE (it will become the new latest)
  tc-here
  ;; put lfa
  tc-latest-lfa tc-,
  ;; update latest
  tc-current tc-@ tc-!
  ;; put name hash
  2dup tc-str-name-hash-real dup tc-,
  ;; fix bfa
  forth-hashtable-bits if
    ;; ( addr count hash )
    ;; fold hash
    tc-name-hash-fold-mask cells  \ endcr dup ." !!! 0x" .hex8 cr
    ;; calc bucket address
    tc-current tc-@ tc-vocid->htable +
    ;; ( addr count bkptr )
    ;; load old bfa link
    dup tc-@
    ;; ( addr count bkptr oldbfa )
    ;; store current bfa address
    tc-here tc-nfa->bfa rot tc-!
    ;; update bfa
    tc-here tc-nfa->bfa tc-!
  else
    drop  ;; we don't really need any hash
  endif
  ;; remember HERE (we will need to fix some name fields later)
  tc-here >r
  ;; compile counter (we'll add flags and other things later)
  ;; it is guaranteed that all fields except length are zero here
  ;; (due to word header layout, and length check above)
  dup tc-,
  ;; copy parsed word to HERE (and allocate name bytes for it)
  dup tc-n-allot tc->real swap move
  ;; uppercase created name
  r@ tc->real count upcase-str
  ;; put flags (ffa is 16 bits at nfa+2)
  (wflag-smudge) ;; current_mode or
  r@ tc-nfa->ffa tc-tfa!  ;; it is safe to poke here, ffa is empty
  ;; put length again (trailing length byte)
  r@ tc-c@ tc-c,
  ;; we are at CFA, fixup references
  r@ tc->real dup c@ swap 4+ swap
  ;; ( addr count value forward -- )
  0 to asm-labman:do-fix-label-name?
  tc-here false asmx86:asm-Make-Forth-Label
  1 to asm-labman:do-fix-label-name?
  ;; we don't need nfa address anymore
  rdrop
  ;; setup initial size, why not
  tc-create;
;


: tc-(create)  ( -- )  \ word
  parse-name tc-(create-str)
;


;; it is easier to leave this as pasta than to create doers; sorry
: tc-(variable-header-str)  ( addr count -- )
  tc-(create-str) tc-smudge
  tc-compile-call (urforth-dovar-codeblock)
;

: tc-(constant-header-str)  ( addr count -- )
  tc-(create-str) tc-smudge
  tc-compile-call (urforth-doconst-codeblock)
;

: tc-(value-header-str)  ( addr count -- )
  tc-(create-str) tc-smudge
  tc-compile-call (urforth-dovalue-codeblock)
;

: tc-(defer-header-str)  ( addr count -- )
  tc-(create-str) tc-smudge
  tc-compile-call (urforth-dodefer-codeblock)
;

;; this a normal forth code; the magic is in its first word
: tc-(vocab-header-str)  ( addr count -- )
  tc-(create-str) tc-smudge tc-vocab
  tc-compile-call (urforth-doforth-codeblock)
  ;; put vocab does
  " (VOCAB-DOES-CODE)" tc-compile,-(str)
;
