;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; target code writing for assembler
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary metc-meta
also metc-meta definitions

;; used to get current assembling address
;; should return virtual address
: mc-PC  ( -- addr )
  elf-current-pc
;

;; size is never zero or negative
;; returns alloted addres (it will be used with `(asm-c!)`
;; should return virtual address
: mc-n-allot  ( size -- addr )
  dup 0< ?abort" negative tc allot"
  dup elf-target-memory-size u>= ?abort" trying to allocate too many tc bytes"
  elf-current-pc  ;; result
  swap over +     ;; end addr
  dup elf-base-rva elf-target-memory-size + u> ?abort" out of tc memory"
  to elf-current-pc
;

;; the only writing primitive
;; always writing to alloted address
;; accepts virtual address
: mc-c!  ( byte addr -- )
  tc->real forth:c!
;

;; the only reading primitive
;; always reading from alloted address
;; accepts virtual address
: mc-c@  ( addr -- byte )
  tc->real forth:c@
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous definitions

;; plug in our memory r/w module

' metc-meta:mc-PC to asmx86:asm-PC
' metc-meta:mc-n-allot to asmx86:asm-n-allot
' metc-meta:mc-c@ to asmx86:asm-c@
' metc-meta:mc-c! to asmx86:asm-c!
