;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; currently, debug info is very simple, it contains only PC->LINE mapping
;;
;; header:
;;   dd itemcount  ; this may be used as "extended" later, so bit 31 is resv
;;
;; items:
;;   dd pc
;;   dd line
;;
;; itemcount bit 31 should always be 0
;;
;; the compiler doesn't store each PC, it only stores line changes
;; that is, the range for the line lasts until the next item
;; items should be sorted by PC (but the code should not fail on
;; unsorted data)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
true value tc-(DBGINFO-ENABLED?) (hidden)
0 value tc-(DBG-BUF-PTR) (hidden)
1024 64 * value tc-(DBG-BUF-SIZE) (hidden)
false value tc-(DBGINFO-ACTIVE?) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-DEBUG-INFO-ON
  true to tc-(dbginfo-enabled?)
;

: tc-DEBUG-INFO-OFF
  false to tc-(dbginfo-enabled?)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(DBGINFO-RESET)  ( -- )
  tc-(dbg-buf-size) if tc-(dbg-buf-ptr) 0! endif
  false to tc-(dbginfo-active?)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-debug-initialize  ( -- )
  tc-(DBG-BUF-PTR) ?abort" debuginfo writer already initialized"
  tc-(DBG-BUF-SIZE) 0< ?abort" debuginfo writer: invalid config"
  tc-(DBG-BUF-SIZE) ?dup if
    brk-alloc to tc-(DBG-BUF-PTR)
    tc-(DBGINFO-RESET)
  else
    false to tc-(DBGINFO-ENABLED?)
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(DBGINFO-GET-ADDR-SIZE)  ( -- addr bytes true // false )
  tc-(dbginfo-active?) tc-(dbginfo-enabled?) logand ifnot false exit endif
  tc-(dbg-buf-size) ifnot false exit endif
  tc-(dbg-buf-ptr) @ ?dup ifnot false exit endif
  dup -1 = if drop false exit endif
  ;; convert to bytes
  2 cells u* cell+
  ;; address
  tc-(dbg-buf-ptr)
  swap 1
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(DBGINFO-DUMP-AT)  ( addr -- )
  ?dup ifnot exit endif
  dup @ ifnot drop exit endif
  ;; print counter
  >r endcr ." debug info containts "
  r@ @ . ." items\n"
  ;; end
  r@ @ 2 cells u* cell+ r@ +
  ;; start
  r@ cell-
  do
    i .hex8
    ." : pc=0x"
    i @ .hex8
    ."  at "
    i cell+ @ .
    cr
    2 cells
  +loop
  rdrop
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(DBGINFO-DUMP)  ( -- )
  tc-(dbg-buf-size) ifnot
    endcr ." debug info disabled\n"
    2drop exit
  endif
  tc-(dbg-buf-ptr) @ ?dup ifnot
    endcr ." no debug info\n"
    exit
  endif
  -1 = if
    endcr ." debug info overflowed\n"
    exit
  endif
  tc-(dbg-buf-ptr) tc-(dbginfo-dump-at)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(DBGINFO-ADD-PC)  ( pc line -- )
  ;; never put zero line
  ?dup ifnot drop exit endif
  tc-(dbg-buf-size) ifnot 2drop exit endif
  ;; just inited?
  tc-(dbg-buf-ptr) @ ?dup ifnot
    ;; yes, store the item
    ;; assume that we always have room for at least one
    tc-(dbg-buf-ptr) 1!
    swap tc-(dbg-buf-ptr) cell+ !
    tc-(dbg-buf-ptr) 2 +cells !
    exit
  endif
  ;; special? -1 items means "out of room"
  dup -1 = if drop 2drop exit endif
  ;; calculate address of the last item
  1- 2 cells u* cell+ tc-(dbg-buf-ptr) +
  ;; check if the line is the same
  ;; TODO: check for sorted PC here?
  2dup cell+ @ = if
    ;; no need to store this item
    drop 2drop exit
  endif
  ;; advance address
  4 +cells
  ;; check if we have enough room
  dup tc-(dbg-buf-size) tc-(dbg-buf-ptr) + u> if
    ;; out of buffer, abort debug info generation
    drop 2drop -1 tc-(dbg-buf-ptr) ! exit
  endif
  2 -cells
  ;; ok, we have enough room, store new item
  rot over !  cell+ !
  ;; and increment the counter
  1 tc-(dbg-buf-ptr) +!
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(DBGINFO-ADD-HERE)  ( -- )
  ;; put debug info
  tc-(dbginfo-active?) tc-(dbginfo-enabled?) logand if
    elf-current-pc ;; tc-here -- sorry
    tib-line# @ tc-(dbginfo-add-pc)
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
tc-debug-initialize
