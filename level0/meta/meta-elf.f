;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple elf header creation, and writing binary elf file
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (put-b)  ( addr value -- addr+1 )
  over c! 1+
;

: (put-w)  ( addr value -- addr+2 )
  over w! 2+
;

: (put-dw)  ( addr value -- addr+4 )
  over ! cell+
;

: (put-strz)  ( addr count -- addr+count+1 )
  dup 0> if
    over + swap do i c@ (put-b) loop
    0 (put-b)
  else
    drop
  endif
;


0 constant DT_NULL
1 constant DT_NEEDED
4 constant DT_HASH
5 constant DT_STRTAB
6 constant DT_SYMTAB
10 constant DT_STRSZ
11 constant DT_SYMENT
17 constant DT_REL
18 constant DT_RELSZ
19 constant DT_RELENT

4 4 + 4 + 1 + 1 + 2 + constant ELF32_SYM_SIZE
4 4 + constant ELF32_REL_SIZE

: build-elf-header  ( addr -- eaddr )
  ;; signature
  $7F (put-b)
  [char] E (put-b)
  [char] L (put-b)
  [char] F (put-b)

  1 (put-b)     ;; bitness
  1 (put-b)     ;; endianness
  1 (put-b)     ;; header version
  3 (put-b)     ;; abi

  8 0 do 0 (put-b) loop

  2 (put-w)     ;; e_type
  3 (put-w)     ;; e_machine

  1 (put-dw)    ;; e_version
  dup to elf-entry-point-addr
  0 (put-dw)    ;; e_entry
  $34 (put-dw)  ;; e_phoff
  0 (put-dw)    ;; e_shoff
  0 (put-dw)    ;; e_flags

  $34 (put-w)   ;; e_ehsize
  32 (put-w)    ;; e_phentsize
  3 (put-w)     ;; e_phnum
  40 (put-w)    ;; e_shentsize
  0 (put-w)     ;; e_shnum
  0 (put-w)     ;; e_shstrndx

  ;; first segment: interpreter
  3 (put-dw)          ;; type
  $00000094 (put-dw)  ;; foffset
  $08048094 (put-dw)  ;; vaddr
  $08048094 (put-dw)  ;; shit
  $00000013 (put-dw)  ;; fsize
  $00000013 (put-dw)  ;; msize
  $00000004 (put-dw)  ;; flags
  $00000001 (put-dw)  ;; align

  ;; second segment: dynamic imports
  2 (put-dw)          ;; type
  $000000A7 (put-dw)  ;; foffset
  $080480A7 (put-dw)  ;; vaddr
  $080480A7 (put-dw)  ;; shit
  $00000050 (put-dw)  ;; fsize
  $00000050 (put-dw)  ;; msize
  $00000004 (put-dw)  ;; flags
  $00000001 (put-dw)  ;; align

  ;; thirds segment: executable code
  1 (put-dw)          ;; type
  $00000000 (put-dw)  ;; foffset
  $08048000 (put-dw)  ;; vaddr
  $08048000 (put-dw)  ;; shit
  dup to elf-code-size-addr
  $0000D3C5 (put-dw)  ;; fsize
  $0000D3C5 (put-dw)  ;; msize
  $00000007 (put-dw)  ;; flags
  $00001000 (put-dw)  ;; align

  ;; first segment data: write interpreter string
  " /lib/ld-linux.so.2" (put-strz)

  ;; second segment data: write import table
  ;; the only thing we need is .so management functions, so we'll create
  ;; a very simple import table for "libdl.so", with 3 imports:
  ;; "dlopen", "dlclose", "dlsym"
  DT_NEEDED (put-dw)  1 (put-dw) ;; elfhead_str_libdl-elfhead_strtab
  DT_STRTAB (put-dw)  $08048137 (put-dw)  ;; elfhead_strtab
  DT_STRSZ (put-dw)   $0000001F (put-dw)  ;; elfhead_strsz
  DT_SYMTAB (put-dw)  $080480F7 (put-dw)  ;; elfhead_symtab
  DT_SYMENT (put-dw)  ELF32_SYM_SIZE (put-dw)
  DT_REL (put-dw)     $08048156 (put-dw)  ;; elfhead_rel
  DT_RELSZ (put-dw)   $00000018 (put-dw)  ;; elfhead_relsz (put-dw)
  DT_RELENT (put-dw)  ELF32_REL_SIZE (put-dw)
  DT_HASH (put-dw)    $0804816E (put-dw)  ;; elfhead_hash (put-dw)
  DT_NULL (put-dw)    0 (put-dw)

  ;; here starts executable segment
  ;; we're putting rest of import table into it; this is prolly not right, but it works

  ;; import symbol table
  ;; NULL import, should always be here
    0 (put-dw)  ;; name
    0 (put-dw)  ;; value
    0 (put-dw)  ;; size
  $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
    0 (put-b)   ;; other
    0 (put-w)   ;; shndx
  ;; import "dlopen"
   10 (put-dw)  ;; name
    0 (put-dw)  ;; value
    0 (put-dw)  ;; size
  $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
    0 (put-b)   ;; other
    0 (put-w)   ;; shndx
  ;; import "dlclose"
   17 (put-dw)  ;; name
    0 (put-dw)  ;; value
    0 (put-dw)  ;; size
  $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
    0 (put-b)   ;; other
    0 (put-w)   ;; shndx
  ;; import "dlsym"
   25 (put-dw)  ;; name
    0 (put-dw)  ;; value
    0 (put-dw)  ;; size
  $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
    0 (put-b)   ;; other
    0 (put-w)   ;; shndx

  ;; string table
  0 (put-b)
  " libdl.so" (put-strz)
  " dlopen" (put-strz)
  " dlclose" (put-strz)
  " dlsym" (put-strz)

  ;; importer will use this to fix relocations
  ;; dlopen
  $0804818A (put-dw)  ;; offset to elfimp_dlopen
  $0101     (put-dw)  ;; high bit is symbol index, low bit is R_386_32
  ;; dlclose
  $0804818E (put-dw)  ;; offset to elfimp_dlclose
  $0201     (put-dw)  ;; high bit is symbol index, low bit is R_386_32
  ;; dlsym
  $08048192 (put-dw)  ;; offset to elfimp_dlsym
  $0301     (put-dw)  ;; high bit is symbol index, low bit is R_386_32

  ;; fake import hash table with one bucket
  1 (put-dw)  ;; bucket size
  4 (put-dw)  ;; chain size (including NULL import)
  0 (put-dw)  ;; fake bucket, just one hash value
  ;; hashtable bucket
  1 (put-dw)
  2 (put-dw)
  3 (put-dw)
  4 (put-dw)

  ;; loadef will add symbol offets to the following three dwords
  dup real->tc to elf-import-table-rva
  0 (put-dw)  ;; dlopen
  0 (put-dw)  ;; dlclose
  0 (put-dw)  ;; dlsym
  dup real->tc elf-import-table-rva - to elf-import-table-size
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: create-elf-tc-constants  ( -- )
  ;; create some assembler constants
  " urforth_code_base_addr" elf-base-rva asmx86:asm-Make-Constant

  " elfhead_codesize_addr" elf-code-size-addr real->tc asmx86:asm-Make-Constant
  " elfhead_impstart" elf-import-table-rva asmx86:asm-Make-Constant
  " elfhead_implen" elf-import-table-size asmx86:asm-Make-Constant

  " elfimp_dlopen" elf-import-table-rva 0 +cells asmx86:asm-Make-Constant
  " elfimp_dlclose" elf-import-table-rva 1 +cells asmx86:asm-Make-Constant
  " elfimp_dlsym" elf-import-table-rva 2 +cells asmx86:asm-Make-Constant
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: save-elf-binary  ( addr count -- )
  endcr ." code size: " curr-code-size . ." bytes\n"

  curr-code-size elf-code-size-addr !
  curr-code-size elf-code-size-addr cell+ !

  ;; create output file
  o-wronly o-creat or o-trunc or  ;; flags
  s-irwxu s-irgrp or s-ixgrp or s-iroth or s-ixoth or  ;; mode
  (fopen)
  ;; check success
  dup 0< ?abort" cannot create output file"
  >r
  elf-target-memory curr-code-size r@ (fwrite)
  curr-code-size <> ?abort" error writing file"
  r> (fclose) ?abort" error closing file"
;
