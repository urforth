;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; colon, semicolon, variable, etc.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(  [compile] ( ;
: tc-\  ( -- )  [compile] \ ;
: tc-(*  ( -- )  [compile] (* ;  ;; *)
: tc-(+  ( -- )  [compile] (+ ;  ;; +)
: tc-;;  ( -- )  [compile] ;; ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-:
  tc-?exec tc-!csp

  tc-(dbginfo-reset)

  tc-(create)
  tc-compile-call (urforth-doforth-codeblock)
  tc-state 1!

  1 to tc-(dbginfo-active?)
;

: tc-;
  tc-?comp tc-?csp
  tc-compile exit
  ;; set final word size
  tc-create;
  tc-smudge
  tc-state 0!
  ;; save debug info
  tc-(dbginfo-get-addr-size) if
    ;; ( addr bytes )
    ;; save here to debug info field
    tc-here tc-latest-nfa tc-nfa->dfa tc-!
    ;; allocate and copy to here
    dup tc-n-allot  ;; ( addr bytes rva-newaddr )
    tc->real swap cmove
  endif
  ;; deactivate
  tc-(dbginfo-reset)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(*
  the following words aren't really used,
  because we're not really interpreting the source in forth mode

: tc-variable  ( value -- )  \ name
  parse-name tc-(variable-header-str)
  tc-,
;

: tc-constant  ( value -- )  \ name
  parse-name tc-(constant-header-str)
  tc-,
;

: tc-value  ( value -- )  \ name
  parse-name tc-(value-header-str)
  tc-,
;

: tc-defer  ( rva-cfa -- )  \ name
  parse-name tc-(defer-header-str)
  tc-,
;
*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-to  ( value -- )  \ name
  " LITTO!" tc-compile,-(str)
  parse-name tc-cfa,-(str-raw)
;
