;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiler target buffer
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$08048000 value elf-base-rva
0 value elf-entry-point-addr  ;; not rva
0 value elf-code-size-addr  ;; not rva
0 value elf-import-table-rva
0 value elf-import-table-size


;; allocate memory for target image
;; its virtual address is elf-base-rva
;; 1MB should be more than enough
1024 1024 * constant elf-target-memory-size
elf-target-memory-size brk-buffer: elf-target-memory

elf-base-rva value elf-current-pc  ;; rva


: tc->real  ( rva -- addr )
  dup if
    elf-base-rva - elf-target-memory +
  endif
;

: real->tc  ( addr -- rva )
  dup if
    elf-target-memory - elf-base-rva +
  endif
;

: curr-code-size  ( -- size )
  elf-current-pc elf-base-rva -
;
