;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; low-level target compiler words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0x0001 constant tc-(WFLAG-IMMEDIATE)
0x0002 constant tc-(WFLAG-SMUDGE)
0x0004 constant tc-(WFLAG-NORETURN)
0x0008 constant tc-(WFLAG-HIDDEN)
0x0010 constant tc-(WFLAG-CODEBLOCK)
0x0020 constant tc-(WFLAG-VOCAB)

0 constant tc-(WARG-NONE)
1 constant tc-(WARG-BRANCH)
2 constant tc-(WARG-LIT)
3 constant tc-(WARG-C4STRZ)
4 constant tc-(WARG-CFA)
5 constant tc-(WARG-CBLOCK)
6 constant tc-(WARG-VOCID)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create asm macros (to avoid code duplication and sync problems)
" FLAG_IMMEDIATE"  tc-(WFLAG-IMMEDIATE) asmx86:asm-Make-Constant
" FLAG_SMUDGE"     tc-(WFLAG-SMUDGE) asmx86:asm-Make-Constant
" FLAG_NORETURN"   tc-(WFLAG-NORETURN) asmx86:asm-Make-Constant
" FLAG_HIDDEN"     tc-(WFLAG-HIDDEN) asmx86:asm-Make-Constant
" FLAG_CODEBLOCK"  tc-(WFLAG-CODEBLOCK) asmx86:asm-Make-Constant
" FLAG_VOCAB"      tc-(WFLAG-VOCAB) asmx86:asm-Make-Constant

" WARG_NONE"    tc-(WARG-NONE) asmx86:asm-Make-Constant
" WARG_BRANCH"  tc-(WARG-BRANCH) asmx86:asm-Make-Constant
" WARG_LIT"     tc-(WARG-LIT) asmx86:asm-Make-Constant
" WARG_C4STRZ"  tc-(WARG-C4STRZ) asmx86:asm-Make-Constant
" WARG_CFA"     tc-(WARG-CFA) asmx86:asm-Make-Constant
" WARG_CBLOCK"  tc-(WARG-CBLOCK) asmx86:asm-Make-Constant
" WARG_VOCID"   tc-(WARG-VOCID) asmx86:asm-Make-Constant


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; to debugptr field
: tc-NFA->DFA  ( nfa -- dfa )
  20 -
;

;; to word size field
: tc-NFA->SFA  ( nfa -- sfa )
  16 -
;

;; to bucketptr field
: tc-NFA->BFA  ( nfa -- bfa )
  12 -
;

;; to word size field
: tc-NFA->LFA  ( nfa -- sfa )
  8 -
;

;; to name hash field
: tc NFA->HFA  ( nfa -- hfa )
  4-
;

;; argument type area; 1 byte
: tc-NFA->TFA  ( nfa -- tfa )
  1+
;

;; flag fields area; 2 bytes
: tc-NFA->FFA  ( nfa -- ffa )
  2+
;

: tc-NFA->CFA  ( nfa -- cfa )
  dup tc-c@ + 5 +
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-LFA->NFA  ( nfa -- sfa )
  8 +
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-CFA->PFA  ( cfa -- pfa )
  5 +  ;; skip CALL
;

: tc-PFA->CFA  ( pfa -- cfa )
  5 -  ;; undo skip CALL
;

: tc-CFA->NFA  ( cfa -- nfa )
  1- dup tc-c@ - 4-
;

: tc-CFA->LFA  ( cfa -- lfa )
  tc-cfa->nfa tc-nfa->lfa
;

: tc-LFA->CFA  ( lfa -- cfa )
  tc-lfa->nfa tc-nfa->cfa
;

: tc-NFA-COUNT  ( nfa -- addr count )
  dup tc-c@ swap 4+ swap
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-BFA->NFA  ( bfa -- nfa )
  12 +
;

: tc-BFA->FFA  ( bfa -- ffa )
  14 +
;

: tc-BFA->LFA  ( bfa -- lfa )
  4+
;

: tc-BFA->HFA  ( bfa -- hfa )
  8 +
;

: tc-HFA->BFA  ( bfa -- hfa )
  8 -
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-FFA@  ( ffa -- ffa-value )
  tc-w@
;

: tc-TFA@  ( tfa -- tfa-value )
  tc-c@
;

: tc-FFA!  ( ffa-value ffa -- )
  swap 0xffff and swap tc-w!
;

: tc-TFA!  ( tfa-value tfa -- )
  swap 0xff and swap tc-c!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-latest-lfa  ( -- lfa )  tc-current tc-@ tc-@ ;
: tc-latest-nfa  ( -- nfa )  tc-latest-lfa tc-lfa->nfa ;
: tc-latest-cfa  ( -- cfa )  tc-latest-lfa tc-lfa->cfa ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word [addr] ^= value
: tc-wtoggle  ( addr value -- )
  over tc-w@ xor 0xffff and swap tc-w!
;

: tc-(toggle-latest-wflag)  ( flag -- )
  tc-latest-nfa tc-nfa->ffa swap tc-wtoggle
;

: tc-(reset-latest-wflag)  ( flag -- )
  tc-latest-nfa tc-nfa->ffa
  dup tc-w@
  ;; ( flag addr oldflg )
  rot bitnot and swap tc-w!
;

: tc-(set-latest-wflag)  ( flag -- )
  tc-latest-nfa tc-nfa->ffa
  dup tc-w@
  ;; ( flag addr oldflg )
  rot or swap tc-w!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-smudge     ( -- )  tc-(wflag-smudge) tc-(toggle-latest-wflag) ;
: tc-immediate  ( -- )  tc-(wflag-immediate) tc-(toggle-latest-wflag) ;
: tc-hidden     ( -- )  tc-(wflag-hidden) tc-(set-latest-wflag) ;
: tc-public     ( -- )  tc-(wflag-hidden) tc-(reset-latest-wflag) ;
: tc-noreturn   ( -- )  tc-(wflag-noreturn) tc-(set-latest-wflag) ;
: tc-codeblock  ( -- )  tc-(wflag-codeblock) tc-(set-latest-wflag) ;
: tc-vocab      ( -- )  tc-(wflag-vocab) tc-(set-latest-wflag) ;

: tc-arg-none    ( -- )  tc-(WARG-NONE) tc-latest-nfa tc-nfa->tfa tc-c! ;
: tc-arg-branch  ( -- )  tc-(WARG-BRANCH) tc-latest-nfa tc-nfa->tfa tc-c! ;
: tc-arg-lit     ( -- )  tc-(WARG-LIT) tc-latest-nfa tc-nfa->tfa tc-c! ;
: tc-arg-c4strz  ( -- )  tc-(WARG-C4STRZ) tc-latest-nfa tc-nfa->tfa tc-c! ;
: tc-arg-cfa     ( -- )  tc-(WARG-CFA) tc-latest-nfa tc-nfa->tfa tc-c! ;
: tc-arg-cblock  ( -- )  tc-(WARG-CBLOCK) tc-latest-nfa tc-nfa->tfa tc-c! ;
: tc-arg-vocid   ( -- )  tc-(WARG-VOCID) tc-latest-nfa tc-nfa->tfa tc-c! ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; addr is NOT rva!
: tc-xcfind-plain  ( addr count voclptr -- rva-cfa true // false )
  over 1 255 bounds? ifnot 2drop false exit endif
  begin
    tc-@ ?dup
  while
    ;; ( addr count rva-lfa )
      \ >r endcr 2dup type ."  : " r@ tc-lfa->nfa tc-nfa-count swap tc->real swap type space r@ .hex8 cr r>
    >r 2dup r@ tc-lfa->nfa
    ;; check smudge flag
    dup tc-nfa->ffa tc-ffa@ tc-(wflag-smudge) and ifnot
      tc-nfa-count swap tc->real swap s=ci if
        ;; i found her!
        2drop r> tc-lfa->cfa true exit
      endif
    else
      drop 2drop
    endif
    r>
  repeat
  2drop false
;


: tc-xcfind  ( addr count voclptr -- rva-cfa true // false )
  forth-hashtable-bits ifnot tc-xcfind-plain exit endif
  over 1 255 bounds? ifnot 2drop false exit endif
  ;; calculate name hash
  >r 2dup tc-str-name-hash-real
  ;; ( addr count u32hash | voclptr )
  ;; calc bucket address
  dup tc-name-hash-fold-mask cells r> tc-vocid->htable +
  swap >r
  ;; ( addr count bucketaddr | u32hash )
  begin
    tc-@ ?dup
  while
    ;; ( addr count rva-bfa | u32hash )
    ;; check hash
    dup tc-bfa->hfa tc-@ r@ = if
      ;; hash is ok, check name
      ;; no need to check length separately, because string comparison will do it for us
      dup tc-bfa->nfa
      ;; ( addr count rva-bfa rva-nfa | u32hash )
      2over  ;; ( addr count rva-bfa rva-nfa addr count | u32hash )
      rot    ;; ( addr count rva-bfa addr count rva-nfa | u32hash )
      tc-nfa-count swap tc->real swap s=ci if
        ;; ( addr count rva-bfa | u32hash )
        dup tc-bfa->ffa tc-ffa@ tc-(wflag-smudge) and ifnot
          ;; ( addr count rva-bfa | u32hash )
          tc-bfa->nfa tc-nfa->cfa
          nrot 2drop rdrop
          true
          exit
        endif
      endif
    endif
  repeat
  ;; ( addr count | u32hash )
  2drop rdrop
  false
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; WARNING! call this ONLY if you're ABSOLUTELY sure that the word is not in the tc system yet!
;; addr is NOT rva!
: tc-cfa,-(str)-nochecks  ( addr count -- )
  0 to asm-labman:do-fix-label-name?
  2dup
  ;; ( addr count value forward -- )
  0 true asmx86:asm-Make-Forth-Label
  ;; add fixup
  ;; ( addr count size type -- )
  4 asmx86:LABEL-TYPE-CFA asmx86:asm-Label-Fixup
  0 tc-,
  1 to asm-labman:do-fix-label-name?
;

;; WARNING! call this ONLY if you're ABSOLUTELY sure that the word is not in the tc system yet!
;; addr is NOT rva!
: tc-compile,-(str)-nochecks  ( addr count -- )
  0 to asm-labman:do-fix-label-name?
  2dup
  ;; ( addr count value forward -- )
  0 true asmx86:asm-Make-Forth-Label
  ;; add fixup
  ;; ( addr count size type -- )
  4 asmx86:LABEL-TYPE-CFA asmx86:asm-Label-Fixup
  0 tc-,
  1 to asm-labman:do-fix-label-name?
;

;; addr is NOT rva!
: tc-compile,-(str)  ( addr count -- )
  2dup tc-current tc-@ tc-xcfind if
    tc-compile,
      \ endcr ." KNOWN:<" 2dup type ." >\n"
    2drop
  else
      \ endcr ." *UNKNOWN:<" 2dup type ." >\n"
    tc-compile,-(str)-nochecks
  endif
;

;; addr is NOT rva!
: tc-cfa,-(str-raw)  ( addr count -- )
  2dup tc-current tc-@ tc-xcfind if
    tc-, 2drop
  else
    \ FIXME! this WILL break if i'll make "," and "compile," different!
    tc-cfa,-(str)-nochecks
  endif
;

;; addr is NOT rva!
: tc-cfa,-(str)  ( addr count -- )
  " LITCFA" tc-compile,-(str)
  tc-cfa,-(str-raw)
;

: tc-compile  ( -- )  \ word
  parse-name [compile] sliteral
  compile tc-compile,-(str)
; immediate


: tc-compile-call-cfa  ( rva-cfa -- )
  2dup tc-current tc-@ tc-xcfind if
    tc-(call,)
    \ endcr ." WORD FOUND: <" 2dup type ." >\n"
    2drop
  else
    \ endcr ." FORWARD WORD: <" 2dup type ." >\n"
    0 to asm-labman:do-fix-label-name?
    2dup
    ;; ( addr count value forward -- )
    0 true asmx86:asm-Make-Forth-Label
    0xe8 tc-c, ;; CALL
    ;; add fixup
    ;; ( addr count type -- )
    ;; it should not be different from a normal label, so...
    ;; (if it is different, it is a bug)
    ( asmx86:LABEL-TYPE-CFA ) asmx86:LABEL-TYPE-NORMAL asmx86:asm-Jmp-Label-Fixup
    0 tc-,
    1 to asm-labman:do-fix-label-name?
  endif
;

: tc-compile-call  ( -- )  \ word
  parse-name [compile] sliteral
  compile tc-compile-call-cfa
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ defer-@ asmx86:asm-Get-Forth-Word  defer tc-asm-(old-get-forth-word)

: x-tc-xcfind-must  ( addr count -- rva-cfa )
  tc-current if
    2dup tc-current tc-@ tc-xcfind if
      nrot 2drop exit
    endif
  endif
  endcr ." UNKNOWN WORD: \`" type ." \`\n"
  abort" unknown target system word"
;

: x-tc-xcfind  ( addr count -- rva-cfa true // false )
  tc-current ?dup ifnot 2drop false exit endif
  tc-@ tc-xcfind
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-GetForthLabel  ( addr count type -- value 1 // value -1 // false )
\ asm-labman:dump-labels
  0 to asm-labman:do-fix-label-name?
  ;; only CFA and PFA references are allowed
  dup asmx86:LABEL-TYPE-CFA = over asmx86:LABEL-TYPE-PFA = or asmx86:ERRID_ASM_INVALID_FORWARD_REF asmx86:not-?asm-error
  >r
  2dup x-tc-xcfind if
    ;; i found her!
    nrot 2drop
    r> asmx86:LABEL-TYPE-PFA = if tc-cfa->pfa endif
    1  ;; defined
  else
    ;; upcase it
    pad 256 + c4s-copy-a-c
    pad 256 + count 2dup upcase-str
    0 r> asmx86:LABEL-TYPE-PFA = if tc-cfa->pfa endif
    dup >r
    ;; ( addr count value forward -- )
    true asmx86:asm-Make-Forth-Label
    r>
    -1  ;; undefined yet
  endif
  1 to asm-labman:do-fix-label-name?
;

' tc-GetForthLabel to asmx86:asm-Get-Forth-Word
