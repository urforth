;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiling support
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
asmx86:asm-Labman-Reinit

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; do not generate instrumented code (yet)
;; not that it matters, because we will replace "NEXT" anyway
0 to asmx86:asm-instrumented-next-addr

;; align headers to dword?
1 value tc-align-headers

" URFORTH_ALIGN_HEADERS" tc-align-headers asmx86:asm-Make-Constant


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
only forth definitions
vocabulary ur-meta
also ur-meta definitions


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
" meta-helper-low.f" tload

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 value prev-include-buf

: (file-error)  ( addr count msgaddr msgcount -- )
  endcr type space 34 emit type 34 emit space forth:error-line. cr
  \ abort
  1 n-bye
;

;; please, don't write such huge words!
: include-file  ( addr count -- )
  dup not-?abort" cannot include nothing"
  ;; save tib state
  #tib-save-buffer cell+ brk-alloc
  prev-include-buf over ! to prev-include-buf
  prev-include-buf cell+ tibstate-save
  ;; prepend current path (remember the original address, tho)
  2dup forth:(last-tload-path-addr) count pad 1024 + c4s-copy-a-c pad 1024 + c4s-cat-a-c
  pad 1024 + count o-rdonly 0 (fopen)
  dup 0< if drop " cannot open file" (file-error) endif
  >r
  ;; ( addr count | fd )
  0 (seek-end) r@ (lseek)
  ;; ( addr count size | fd )
  dup 0< if
    drop r> (fclose) drop
    " cannot get size of file" (file-error)
  endif
  ;; seek back to file start
  ;; ( addr count size | fd )
  0 (seek-set) r@ (lseek)
  if
    drop r> (fclose) drop
    " cannot rewind file" (file-error)
  endif
  ;; allocate temp pool space
  \ TODO: free memory somehow
  ;; ( addr count size | fd )
  dup brk-alloc
  ;; ( addr count size bufaddr | fd )
  ;; load file
  2dup swap r@ (fread)
  ;; ( addr count size bufaddr readbytes | fd )
  rot over = ifnot
    ;; ( addr count bufaddr readbytes | fd )
    drop 2drop r> (fclose) drop
    " cannot get size of file" (file-error)
  endif
  ;; close file
  r> (fclose) drop
  ;; ( addr count bufaddr readbytes )
  #tib !
  tib !
  >in 0!
  tib-line# 1!
  ;; replace current file name (for error reporting)
  dup 2 +cells brk-alloc  ;; ( addr count namebuffaddr )
  forth:(tib-curr-fname) over ! cell+
  to forth:(tib-curr-fname)
  2dup forth:(tib-curr-fname) c4s-copy-a-c
  ;; show message, and go with the new tib
  endcr ." processing: " type cr
  ;; replace current tload path (for relative loads)
  pad 1024 + count dup 2 +cells brk-alloc
  ;;  ( addr count newbuf )
  forth:(last-tload-path-addr) over ! cell+
  dup to forth:(last-tload-path-addr)
  c4s-copy-a-c
  ;; done with bookkeeping
  asmx86:lexer:PrepareLineParser
  asmx86:lexer:NextToken
;

: tc-refill  ( -- flag )
  prev-include-buf ifnot false exit endif
  prev-include-buf dup @ to prev-include-buf
  cell+ tibstate-restore
  ;; restore file name
  forth:(tib-curr-fname) cell- @ to forth:(tib-curr-fname)
  ;; restore tload path
  forth:(last-tload-path-addr) cell- @ to forth:(last-tload-path-addr)
  \ ." resuming: " forth:(tib-curr-fname) count type cr
  true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
" meta-mem.f" tload
" meta-asm-tc.f" tload
" meta-elf.f" tload
" meta-tc-dbginfo.f" tload

" meta-tc-compiler-low.f" tload
" meta-tc-word-utils.f" tload
" meta-tc-wordlist.f" tload
" meta-tc-compiler-mid.f" tload

nested-vocabulary tc-immediates
also tc-immediates definitions

" meta-tc-imm-if-begin-do.f" tload
" meta-tc-imm-colon-var.f" tload
" meta-tc-imm-compile-tick.f" tload

previous definitions

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also asmx86 definitions
nested-vocabulary tc-instrs
also tc-instrs definitions
<public-words>

" meta-asm-commands.f" tload
" meta-asm-commands-macros.f" tload

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous previous definitions

;; now put our instructions into asmx86:instructions vocabulary
vocid-of asmx86:tc-instrs asmx86:Register-Macros-From-VocId


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-find-imm  ( addr count -- addr count false // cfa true )
  ;; save search flags
  (voc-find-flags) @ >r
  (voc-find-ignore-mask) @ >r
  (voc-find-flags) 0!
  (wflag-smudge) (wflag-hidden) or (voc-find-ignore-mask) !
  ;; first, prepend the word with "tc-"
  " TC-" pad c4s-copy-a-c 2dup pad c4s-cat-a-c
  ;; now look for it
  pad count [ vocid-of tc-immediates ] literal voc-find-str
  if
    nrot
      \ endcr ." TC-IMM: " 2dup type cr
    2drop true
  else
    false
  endif
  ;; restore search flags
  r> (voc-find-ignore-mask) !
  r> (voc-find-flags) !
;

;; this is mostly what "normal" interpreter does
;; for now, if we stopped compiling, get out of the interpreter
;; TODO: introduce a special flag for this, and allow to execute normal (not target) words here
: tc-(interpret)  ( -- )
  begin
      (sp-check) ifnot error-line. cr dbg endif
    tc-state @ ifnot break endif
    begin
      parse-name-ex
      ;; ( addr count )
      ?dup
    not-while
      drop refill not-?abort" definition not finished"
    repeat
    ;; ( addr count )
      \ endcr ." TC: " 2dup type cr
    ;; here we'll do some hackery: for compiling mode, try "tc-immediates" vocab first
    tc-state @ if
      tc-find-imm if execute continue endif
    endif
    ;; try to find a tc word
    2dup tc-current tc-@ tc-xcfind
    if
      ;; i found her!
      ;; ( addr count rva-cfa )
      ;; no immediate words are allowed here
      dup tc-cfa->nfa tc-nfa->ffa tc-ffa@ tc-(wflag-immediate) and if
        drop  ;; drop cfa
        endcr ." ERROR: \`" type ." \` is immediate!\n"
        abort" no TC immediate words allowed"
      endif
      nrot 2drop
      ;; ( rva-cfa )
      ;; still compile it to the target area, regardless of mode
      tc-compile,
    else
      ;; unknown word, try to parse it as a number
      2dup number if
        nrot 2drop
        tc-literal
      else
        ;; this should be a forward reference
        tc-compile,-(str)-nochecks
      endif
    endif
  again
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; converts "(token)" to one tk-id
: tc-check-paren-token  ( -- )
  asmx86:tk-delim? ifnot exit endif
  asmx86:lexer:tkvalue [char] ( = ifnot exit endif  ;; )
  tib-peekch upcase-char
  dup [char] A >= swap [char] Z <= and ifnot exit endif
  ;; ok, looks like a valid token start
  asmx86:lexer:NextToken
  asmx86:tk-id? not-?abort" identifier expected"
  ;; check next char
  tib-peekch [char] ) = not-?abort" `)` expected"
  tib-getch drop
  ;; the next one should be blank
  tib-peekch 32 <= not-?abort" blank char expected"
  tib-getch drop
  ;; check token length
  asmx86:lexer:tkvalue count-only 1 253 bounds? not-?abort" token too long"
  ;; append closing paren to the token
  \ [char] ) asmx86:lexer:tkvalue count + c!
  \ asmx86:lexer:tkvalue 1+!
  \ ;; prepend opening paren to the token
  \ asmx86:lexer:tkvalue count over 1+ swap move
  \ [char] ( asmx86:lexer:tkvalue cell+ c!  ;; )
  \ asmx86:lexer:tkvalue 1+!
  ;; prepend dollar to the token
  asmx86:lexer:tkvalue count over 1+ swap move
  [char] $ asmx86:lexer:tkvalue cell+ c!
  asmx86:lexer:tkvalue 1+!
    \ endcr asmx86:lexer:tkvalue count type space asmx86:lexer:tkvalue count-only . cr bye
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(code-word)  ( -- )
  tc-(create)
  ;; skip everything to eol
  [compile] \
  asmx86:lexer:PrepareLineParser
  asmx86:lexer:NextToken
  begin
    tc-check-paren-token
    asmx86:tk-id? if
        \ lexer:tkvalue count endcr type cr
      asmx86:lexer:tkvalue count " ENDCODE" s= if break endif
    endif
    asmx86:(asm-tib)
      (sp-check) ifnot error-line. cr dbg endif
    asmx86:lexer:tktype
    ifnot
      tib-peekch ?dup if
        10 = if tib-getch drop endif
      else
        tc-(dbginfo-reset)
        refill asmx86:ERRID_ASM_ENDCODE_EXPECTED asmx86:not-?asm-error
      endif
      asmx86:lexer:PrepareLineParser
      asmx86:lexer:NextToken
    endif
  repeat
  tc-create;
  ;; latest disasm-word
  asmx86:asm-Check-Undef-Labels
  tc-smudge
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main metacompiler loop
: format  ( -- )
  parse-name " ELF" s=ci not-?abort" not an elf?"
  parse-name " executable" s=ci not-?abort" not an elf executable?"
  ;; parse-name s=ci " 3" not-?abort" not an x86 abi?"
  ;; skip everything to eol
  [compile] \
  1 to asmx86:asm-Labman-Unresolved-As-Forwards?
  asmx86:lexer:PrepareLineParser
  asmx86:lexer:NextToken
  begin
    ;; "code:"?
    asmx86:tk-id? if
      asmx86:lexer:tkvalue count s" code" s=ci if
        \ asmx86:lexer:SkipBlanks
        tib-peekch [char] : = not-?abort" `:` expected"
        tib-getch drop
        ;; the next one should be blank
        tib-peekch 32 <= not-?abort" blank char expected"
        tib-getch drop
        tc-(code-word)
        asmx86:lexer:PrepareLineParser
        asmx86:lexer:NextToken
        continue
      endif
    endif
    ;; ":"?
    asmx86:tk-delim? if
      asmx86:lexer:tkvalue [char] : = if
        ;; the next one should be blank
        tib-peekch 32 <= not-?abort" blank char expected"
        tib-getch drop
        tc-immediates:tc-:
        tc-(interpret)
        asmx86:lexer:PrepareLineParser
        asmx86:lexer:NextToken
        continue
      endif
    endif
    ;; raw assembler
    tc-check-paren-token
    asmx86:(asm-tib)
      (sp-check) ifnot error-line. cr dbg endif
    asmx86:lexer:tktype
    ifnot
      tib-peekch ?dup if
        10 = if tib-getch drop endif
      else
        tc-refill ifnot break endif
      endif
      asmx86:lexer:PrepareLineParser
      asmx86:lexer:NextToken
    endif
  again
  ;; latest disasm-word
  asmx86:asm-Check-Undef-Labels
;

;;previous


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create ELF header
elf-target-memory build-elf-header elf-target-memory - elf-base-rva + to elf-current-pc

previous definitions
alias ur-meta:format format

ur-meta:create-elf-tc-constants

sys-gettickcount
" urforthl1/urforth.f" tload

\ asmx86:asm-Dump-Labels
asmx86:asm-Check-Undef-Labels-Final

" zurforth1.elf" ur-meta:save-elf-binary
sys-gettickcount swap - endcr ." build time: " . ." msecs.\n"
.stack
bye
