;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; low-level target compiler words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-c@  ( rva-addr -- c )  tc->real forth:c@ ;
: tc-w@  ( rva-addr -- w )  tc->real forth:w@ ;
: tc-@   ( rva-addr -- n )  tc->real forth:@ ;

: tc-c!  ( c rva-addr -- )  tc->real forth:c! ;
: tc-w!  ( w rva-addr -- )  tc->real forth:w! ;
: tc-!   ( n rva-addr -- )  tc->real forth:! ;


: tc-here  ( -- ) elf-current-pc ;

: tc-n-allot  ( size -- rva-addr )
  metc-meta:mc-n-allot
;

: tc-c,  ( b -- )  tc-(dbginfo-add-here)  1 tc-n-allot tc-c! ;
: tc-w,  ( w -- )  tc-(dbginfo-add-here)  2 tc-n-allot tc-w! ;
: tc-,   ( n -- )  tc-(dbginfo-add-here)  4 tc-n-allot tc-! ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-COMPILE,  ( rva-cfa -- )
  tc-,
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; write 4-byte displacement for CALL/JMP jmpdest instruction to addr
;; addr should point after the instruction, at the first displacement byte
: tc-(DISP32!)  ( rva-jmpdest rva-addr -- )
  dup cell+ rot swap - swap tc-!
;

: tc-(DISP32@)  ( rva-addr -- rva-jumpdest )
  dup tc-@ swap cell+ +
;

: tc-(BYTE!)  ( byte rva-addr -- )
  tc-c!
;

;; write 5-byte CALL calldest machine instruction to addr
: tc-(CALL!)  ( rva-calldest rva-addr -- )
  0xe8 over tc-(byte!) 1+  ;; write CALL, advance address
  tc-(disp32!)
;

;; write 5-byte JMP jmpdest machine instruction to addr
: tc-(JMP!)  ( rva-jmpdest rva-addr -- )
  0xe9 over tc-(byte!) 1+  ;; write JMP, advance address
  tc-(disp32!)
;

;; compile 5-byte CALL calldest machine instruction to HERE
: tc-(CALL,)  ( rva-calldest -- )
  5 tc-n-allot tc-(call!)
;

;; compile 5-byte JMP jmpdest machine instruction to HERE
: tc-(JMP,)  ( rva-jmpdest -- )
  5 tc-n-allot tc-(jmp!)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist structure
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;;   dd header-nfa (can be 0 for anonymous wordlists)
;;   hashtable (if enabled)

4 constant tc-(voc-header-size-cells)
: tc-vocid->voclink  ( rva-vocid -- rva-voclink )  cell+ ;
: tc-vocid->parent   ( rva-vocid -- rva-parent )  2 +cells ;
: tc-vocid->headnfa  ( rva-vocid -- rva-headernfa )  3 +cells ;
: tc-vocid->htable   ( rva-vocid -- rva-hashtable )  4 +cells ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this will be allocated when the first forth word is defined
;; because hashtable size is controlled by the WLIST_HASH_BITS constant
;; see "ensure-forth-hashtable"
-1 value forth-hashtable-bits


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hack: put it at the end of the memory
;; we'll move it to FORTH dictionary later
0 value tc-current  ;; contains pointer to voclptr

;; create headerless vocabulary structure (see above)
: tc-alloc-vocab-data  ( -- rva-addr )
  forth-hashtable-bits 0< if
    s" WLIST_HASH_BITS" asmx86:asm-Get-Constant not-?abort" WLIST_HASH_BITS is not defined"
    dup 0 8 bounds? not-?abort" WLIST_HASH_BITS is out of range"
    to forth-hashtable-bits
    ." hashtable size: " 1 forth-hashtable-bits lshift cells . ." bytes, at rva 0x" tc-here .hex8 cr
  endif
  ;; calculate vocabulary size
  tc-(voc-header-size-cells) cells
  forth-hashtable-bits if 1 forth-hashtable-bits lshift +cells endif
  dup tc-n-allot  ;; ( size addr )
  dup tc->real rot erase
  ;; save to "forth_wordlist_vocid" label
  dup " forth_wordlist_vocid" rot asmx86:asm-Make-Label
;

;; actually, there is NO reason to not create a vocabulary right away
;; WARNING! TODO: redefine ALL vocabulary words to have pointers to the
;; actual vocabulary positions at their PFA, to avoid threating "FORTH"
;; as something special
: ensure-forth-hashtable  ( -- )
  tc-current ifnot
    ;; we have no tc-current, which means that no vocabularies were created yet
    ;; create one, it will become "FORTH" later
    tc-alloc-vocab-data
    ;; we need indirect pointer to it, so waste one more cell for this
    ;; we will point our target "CURRENT" there too, because why not
    cell tc-n-allot dup to tc-current
    ;; and setup indirection
    tc-!
    ;; create label
    " IMAGE_CURRENT_VAR_ADDR" tc-current asmx86:asm-Make-Label
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-str-name-hash  ( rva-addr count -- u32hash )
  swap tc->real swap str-name-hash
;

: tc-str-name-hash-real  ( addr count -- u32hash )
  str-name-hash
;

: tc-name-hash-fold-mask  ( u32hash -- folded-masked-hash )
  uhash32->8 -1 forth-hashtable-bits lshift bitnot and
;
