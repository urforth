;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional assembler commands
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; app entry point
: ENTRY  ( -- )
  Reset-Instruction
  4 to *OpSize
  Imm
  ;; label?
  *Imm elf-entry-point-addr !
  *OpRel if
    *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
    *ImmName bcount *ImSize *ImmForthType
    ;; HACK!
    elf-current-pc >r
    elf-entry-point-addr real->tc to elf-current-pc
    asm-Label-Fixup
    r> to elf-current-pc
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: $INCLUDE  ( -- )
  tk-str? ERRID_ASM_STRING_EXPECTED not-?asm-error
  lexer:tkvalue count pad c4s-copy-a-c
  \ lexer:NextToken tk-eol? ERRID_ASM_SYNTAX_ERROR not-?asm-error
  pad count include-file
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: $HIDDEN  ( -- )  tc-hidden ;
: $PUBLIC  ( -- )  tc-public ;
: $NORETURN  ( -- )  tc-noreturn ;
: $CODEBLOCK  ( -- )  tc-codeblock ;
: IMMEDIATE  ( -- )  tc-immediate ;

: $ARG_NONE    ( -- )  tc-arg-none ;
: $ARG_BRANCH  ( -- )  tc-arg-branch ;
: $ARG_LIT     ( -- )  tc-arg-lit ;
: $ARG_C4STRZ  ( -- )  tc-arg-c4strz ;
: $ARG_CFA     ( -- )  tc-arg-cfa ;
: $ARG_CBLOCK  ( -- )  tc-arg-cblock ;
: $ARG_VOCID   ( -- )  tc-arg-vocid ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: ($VAR-ETC-CREATE)  ( tc-cfa -- )
  ;; get name
  tk-str? ERRID_ASM_STRING_EXPECTED not-?asm-error
  ;; create word header and CFA
  lexer:tkvalue count 2dup upcase-str rot execute
  lexer:NextToken
;

hidden:: ($VAR-COMPILE-VALUE)  ( -- )
  ;; parse word value
  Reset-Instruction
  4 to *OpSize
  Imm
  ;; label?
  *OpRel if
    ;; create label fixup
    *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
    *ImmName bcount *ImSize *ImmForthType asm-Label-Fixup
  endif
  ;; put value
  *Imm tc-,
;

hidden:: ($VAR-ETC)  ( tc-cfa -- )
  ($VAR-ETC-CREATE)
  ($VAR-COMPILE-VALUE)
  tc-create;
;

: $VARIABLE     ( -- )  ['] tc-(variable-header-str) ($VAR-ETC) ;
: $CONSTANT     ( -- )  ['] tc-(constant-header-str) ($VAR-ETC) ;
: $VALUE        ( -- )  ['] tc-(value-header-str) ($VAR-ETC) ;
: $DEFER        ( -- )  ['] tc-(defer-header-str) ($VAR-ETC) ;

: $VOCABHEADER  ( -- )
  ['] tc-(vocab-header-str) ($VAR-ETC)
  tc-create;
  ;; patch wordlist name pointer
  *OpRel if
    *ImmLabelDefined
  else
    true
  endif
  if
    ;; address is known, fix name pointer
    tc-latest-nfa
    \ tc-here cell- tc-@  ;; get wordlist address
    *Imm                  ;; our wordlist address lives here too ;-)
    tc-vocid->headnfa
    ;; sanity check
    dup tc-@ ?abort" trying to create two headers for one wordlist"
    tc-!
  else
    ;; address is unknown
    ?abort" cannot create vocabulary headers with forwards (yet)"
  endif
;

: $ARRAY     ( -- )  ['] tc-(variable-header-str) ($VAR-ETC-CREATE) ;
: $ENDARRAY  ( -- )  tc-create; ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: $TABLEMSG  ( -- )
  ;; get name
  tk-str? ERRID_ASM_STRING_EXPECTED not-?asm-error
  \ TODO: check if it is a constant
  ;; find constant and put its value
  lexer:tkvalue count x-tc-xcfind-must tc-cfa->pfa tc-@ asm-,
  lexer:NextToken
  ;; message
  tk-str? ERRID_ASM_STRING_EXPECTED not-?asm-error
  lexer:tkvalue count over + swap ?do i c@ asm-c, loop
  0 asm-c,
  lexer:NextToken
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: $ALIAS  ( -- )
  tk-str? not-?abort" old word name expected"
  lexer:tkvalue count x-tc-xcfind not-?abort" old word not found"
  lexer:NextToken

  tk-str? not-?abort" new word name expected"
  lexer:tkvalue count x-tc-xcfind ?abort" new word redefined found"

  lexer:tkvalue count tc-(create-str) tc-smudge
  tc-(jmp,)

  lexer:NextToken
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
-1 value tc-has-debugger?  (hidden)
0 value tc-urforth-next-ptr  (hidden)

hidden:: tc-NEXT  ( -- )
  tc-has-debugger? 0< if
    " URFORTH_DEBUG" asmx86:asm-Get-Constant if
      notnot to tc-has-debugger?
      tc-has-debugger? if
        " urforth_next_ptr" asmx86:asm-Get-Label 1 <> ?abort" \`urforth_next_ptr\` must be defined"
        to tc-urforth-next-ptr
      endif
    endif
  endif

  \ " lodsd" asm-str
  $AD asm-c,
  \ either "jmp eax" of "jmp dword [nextref]"
  $FF asm-c, \ both has prefixes
  tc-has-debugger? 0> if
    $25 asm-c, tc-urforth-next-ptr asm-,
  else
    $E0 asm-c,
  endif
;

replace asmx86:macro-instrs:NEXT tc-NEXT
