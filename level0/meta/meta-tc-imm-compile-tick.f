;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; metacompiler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some compilers
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-compile  ( -- )
  parse-name tc-cfa,-(str)
  " COMPILE," tc-compile,-(str)
;

: tc-[compile]  ( -- )
  parse-name tc-compile,-(str)
;

: tc-[']  ( -- )
  parse-name tc-cfa,-(str)
;

: tc-[char]  ( -- )
  parse-name 1 = not-?abort" character expected"
  c@ tc-literal
;

: tc-(parse-unescape-str)  ( -- addr count )
  34 parse 2dup here swap move nip here swap str-unescape
;

: tc-"  ( -- )  ;; "
  tc-(parse-unescape-str)
  tc-SLITERAL
;

alias tc-" tc-s"

: tc-."  ( -- )  ;; "
  tc-state @ not-?abort" tc-SLITERAL: compile-time only!"
  " (.\`)" tc-compile,-(str)
  tc-(parse-unescape-str)
  tc-(str)
;
