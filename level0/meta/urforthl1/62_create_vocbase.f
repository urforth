;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word searching
;; CURRENT:
;;   searched first; also, new words will go here
;;
;; CONTEXT:
;;   stack of vocabularies, searched after the CURRENT
;;
URFORTH_VOCSTACK_SIZE equ 16
$constant "VOC-STACK-SIZE" URFORTH_VOCSTACK_SIZE

;; this will be bitored with flags in (CREATE)
$value "(CURRENT-CREATE-MODE)" 0
(hidden)

;; target compiler already has it, so let's use it too
$value "CURRENT" image_current_var_addr
;; this actually points to the last pushed vocabulary
$value "CONTEXT" forth_voc_stack  ;;pfa "(vocab-stack)"+8

$variable "WARNING-REDEFINE" 1
$variable "CREATE-UPCASE?" 1

;; vocabulary stack
$array "(VOCAB-STACK)"
(hidden)
  ;; this indicates end of the stack (should always be there, it is used as a sentinel)
  dd  0
  ;; this one is always here, it is so-called "root" vocabulary (last resort)
  ;; it is usually system FORTH vocabulary
  ;; CONTEXT may point here
forth_voc_stack_root:
  dd  forth_wordlist_vocid
  ;; replaceable stack begins here
forth_voc_stack:
  dd  forth_wordlist_vocid
  rd  URFORTH_VOCSTACK_SIZE-1
forth_voc_stack_end:
$endarray

$constant "(FORTH-VOC-STACK)" forth_voc_stack
(hidden)
$constant "(FORTH-VOC-STACK-END)" forth_voc_stack_end
(hidden)


;; voclink always points to another voclink (or contains 0)
$variable "(VOC-LINK)" forth_wordlist_vocid+4  ;; voclink always points to wordlist voclink (or contains 0)
(hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ID-COUNT  ( nfa -- nameaddr namelen )
  dup c@ swap cell+ swap
;

: ID.  ( nfa -- )
  id-count safe-type
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist structure
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;;   dd header-nfa (can be 0 for anonymous wordlists)
;;   hashtable (if enabled)

: VOC-CFA->VOCID  ( cfa -- lfaptr )
  cfa->pfa cell+ @
; (hidden)

$constant "(VOC-HEADER-SIZE-CELLS)" 4
(hidden)

;; move to parent vocabulary field
: VOCID->VOCLINK  ( lfaptr -- vocling )
  cell+
; (hidden)

;; move to parent vocabulary field
: VOCID->PARENT  ( lfaptr -- parent )
  2 +cells
; (hidden)

;; move to parent vocabulary field
: VOCID->HEADNFA  ( lfaptr -- headernfa )
  3 +cells
; (hidden)

;; move to parent vocabulary field
: VOCID->HTABLE  ( lfaptr -- hashtable )
  4 +cells
; (hidden)

: VOCID-NAME  ( lfaptr -- addr count )
  ?dup if
    vocid->headnfa @ ?dup if
      id-count exit
    endif
  endif
  ;; dummy
  here 0
;

: VOCID.  ( lfaptr -- )
  vocid-name ?dup ifnot drop " (anonymous)" endif type
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; patches word size to HERE
: create;  ( -- )
  ;; patch size
  here latest-nfa nfa->sfa !
; (hidden)

;; A defining word used in the form: CREATE cccc
;; by such words as CODE and CONSTANT to create a dictionary header for
;; a Forth definition. Puts no code field.
;; The new word is created in the current vocabulary.
;; Note that SMUDGE bit is set (i.e. the word is invisible).
: (CREATE-STR)  ( addr count -- )
  ;; we cannot create words in temp-here (yet)
  dp-temp @ err-no-temp-here ?error
  ;; sanity check (just in case)
  ;;here word-here <> err-no-temp-here qerror
  ;; check for duplicate word?
  warning-redefine @ if
    ;; look only in the current dictionary
    2dup current @ voc-find-str if
      ?endcr if space endif cfa->nfa ." \`" id. ." \` redefined" error-line. cr
    endif
  endif
  ;; ( addr count )
  ;; check length
  dup 1 255 bounds? ifnot
    ." \`" type ." \` " err-invalid-word-name error
  endif
  ;; align
  (align-here)
  ;; allocate dfa
  0 ,
  ;; allocate sfa
  0 ,
  ;; allocate bfa (it will be patched later)
  0 ,
  ;; remember HERE (it will become the new latest)
  here
  ;; put lfa
  latest-lfa ,
  ;; update latest
  current @ !
  ;; put name hash
  2dup str-name-hash dup ,
  ;; fix bfa
  ;; TODO: replace this with conditional compilation when we'll get 'em
  WLIST-HASH-BITS if
    ;; ( addr count hash )
    ;; fold hash
    name-hash-fold-mask cells
    ;; calc bucket address
    current @ vocid->htable +
    ;; ( addr count bkptr )
    ;; load old bfa link
    dup @
    ;; ( addr count bkptr oldbfa )
    ;; store current bfa address
    here nfa->bfa rot !
    ;; update bfa
    here nfa->bfa !
  else
    drop  ;; we don't really need any hash
  endif
  ;; remember HERE (we will need to fix some name fields later)
  here >r
  ;; compile counter (we'll add flags and other things later)
  ;; it is guaranteed that all fields except length are zero here
  ;; (due to word header layout, and length check above)
  dup ,
  ;; copy parsed word to HERE (and allocate name bytes for it)
  dup n-allot swap move
  ;; uppercase created name?
  create-upcase? @ if r@ count upcase-str endif
  ;; put flags (ffa is 16 bits at nfa+2)
  (wflag-smudge) (current-create-mode) or
  r@ nfa->ffa tfa!  ;; it is safe to poke here, ffa is empty
  ;; put length again (trailing length byte)
  r@ c@ c,
  ;; we don't need nfa address anymore
  rdrop
  ;; setup initial size, why not
  create;
; (hidden)

: (CREATE)
  parse-name (create-str)
; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is the first word compiled to vocabulary header
;; it should be followed by vocid (aka wordlist address)
: (VOCAB-DOES-CODE)  ( -- )
  ;; code argument is vocid, get it
  ;; also note that we will not continue execution (it is meaningless)
  r> @  ;; vocid
  ;; puts this one to the context stack, replacing the top one
  context (forth-voc-stack) u< if
    ;; we don't have any vocabularies in the stack, push one
    (forth-voc-stack) to context
  endif
  ;; replace top context vocabulary
  context !
; (hidden) (arg_vocid)



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sadly, has to be here, because it is used by word searching code
URWORD_TYPE_OTHER     equ 0
URWORD_TYPE_FORTH     equ 1
URWORD_TYPE_VAR       equ 2
URWORD_TYPE_CONST     equ 3
URWORD_TYPE_VALUE     equ 4
URWORD_TYPE_DEFER     equ 5
URWORD_TYPE_DOES      equ 6
URWORD_TYPE_VOC       equ 7
URWORD_TYPE_OVERRIDE  equ 8
URWORD_TYPE_OVERRIDEN equ 9

$constant "WORD-TYPE-OTHER"   URWORD_TYPE_OTHER
$constant "WORD-TYPE-FORTH"   URWORD_TYPE_FORTH
$constant "WORD-TYPE-CONST"   URWORD_TYPE_VAR
$constant "WORD-TYPE-VAR"     URWORD_TYPE_CONST
$constant "WORD-TYPE-VALUE"   URWORD_TYPE_VALUE
$constant "WORD-TYPE-DEFER"   URWORD_TYPE_DEFER
$constant "WORD-TYPE-DOES"    URWORD_TYPE_DOES
$constant "WORD-TYPE-VOC"     URWORD_TYPE_VOC
;; this word is new override (overriden words are not marked as special, yet they become non-Forth, i.e. "other")
$constant "WORD-TYPE-OVERRIDE"  URWORD_TYPE_OVERRIDE
$constant "WORD-TYPE-OVERRIDEN" URWORD_TYPE_OVERRIDEN

$array "(WORD-CFA-TABLE)"
(hidden)
  dd  pfa "(URFORTH-DOFORTH-CODEBLOCK)",URWORD_TYPE_FORTH
  dd  pfa "(URFORTH-DOCONST-CODEBLOCK)",URWORD_TYPE_CONST
  dd  pfa "(URFORTH-DOVAR-CODEBLOCK)",URWORD_TYPE_VAR
  dd  pfa "(URFORTH-DOVALUE-CODEBLOCK)",URWORD_TYPE_VALUE
  dd  pfa "(URFORTH-DODEFER-CODEBLOCK)",URWORD_TYPE_DEFER
  dd  pfa "(URFORTH-DODOES-CODEBLOCK)",URWORD_TYPE_DOES
  dd  pfa "(URFORTH-DOOVERRIDE-CODEBLOCK)",URWORD_TYPE_OVERRIDE
  dd  0
$endarray

: WORD-TYPE?  ( cfa -- type )
  dup c@ 0xe8 = if
    ;; check "vocabulary" flag
    dup cfa->nfa nfa->ffa ffa@
    (wflag-vocab) and if drop word-type-voc exit endif
    ;; calculate call address
    1+ (disp32@)
    ;; check for overriden word
    dup (code-base-addr) u> over real-here u< and if
      ;; it should be safe to peek
      dup c@ 0xe8 = if
        dup 1+ (disp32@)
        ['] (URFORTH-DOOVERRIDE-CODEBLOCK) = if drop word-type-overriden exit endif
      endif
    endif
    ;; check table
    >r (word-cfa-table)
    begin
      dup @ ?dup
    while
      ;; ( tbladdr codeaddr | cfadest )
      r@ = if rdrop cell+ @ exit endif
      2 +cells
    repeat
    ;; ( tbladdr | cfadest )
    rdrop
  endif
  drop
  word-type-other
;
