;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; shows current search order
: ORDER  ( -- )
  ." CONTEXT:"
  context
  begin
    dup (forth-voc-stack) u>=
  while
    dup @ space vocid.
    cell-
  repeat
  drop
  space ." (ROOT)\nCURRENT: "
  current @ vocid. cr
;

;; makes all newly created words public
: PUBLIC:  ( -- )
  (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) bitnot and to (CURRENT-CREATE-MODE)
;

;; makes all newly created words hidden
: PRIVATE:  ( -- )
  (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) or to (CURRENT-CREATE-MODE)
;

;; sets top context vocabulary as the current one
;; resets current mode to public
: DEFINITIONS  ( -- )
  context @ current !
  PUBLIC:
;

;; makes top context vocabulary as the only one, and makes it current too
;; resets current mode to public
: ONLY  ( -- )
  (forth-voc-stack) to context
  definitions
;

;; duplicates top context vocabulary, so it could be replaced with another one
;; resets current mode to public
: ALSO  ( -- )
  ;; check for vocstack overflow
  context cell+ dup (forth-voc-stack-end) u>= ERR-VOCABULARY-STACK-OVERFLOW ?error
  context @ over !
  to context
;

;; drop topmost context vocabulary
;; resets current mode to public
: PREVIOUS  ( -- )
  ;; check for vocstack underflow
  context (forth-voc-stack) u>= if
    context cell- to context
  endif
;
