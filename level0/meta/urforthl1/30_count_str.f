;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; length of asciiz string
code: ZCOUNT  ( addr -- addr count )
  push  TOS
  mov   edi,TOS
  xor   al,al
  cp    [edi],al
  jr    nz,@f
  xor   TOS,TOS
  urnext
@@:
  mov   edx,-1
  mov   ecx,edx
  repnz scasb
  sub   edx,ecx
  mov   ecx,edx
  dec   ecx
  urnext
endcode


$alias "@" "COUNT-ONLY"  ;;( addr -- count )

code: COUNT  ( addr -- addr+4 count )
  ;;UF dup count_only swap cellinc swap exit
  ld    eax,[TOS]
  add   TOS,4
  push  TOS
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: S=  ( addr0 count0 addr1 count1 -- flag )
  ;; compare lengthes
  cp    TOS,[esp+4]
  jr    nz,.lenfail
  or    ecx,ecx
  jr    z,.lenzero
  ;; perform memcmp
  pop   edi
  pop   eax
  xchg  esi,[esp]
  ;; now EIP is saved on the stack
  repz  cmpsb
  ;; restore EIP
  pop   esi
  jcxz  .success_check
  xor   TOS,TOS
  urnext

.success_check:
  jr    z,.success
  xor   TOS,TOS
  urnext

.lenfail:
  ;; early failure
  add   esp,4*3
  xor   TOS,TOS
  urnext

.lenzero:
  ;; early success
  add   esp,4*3
.success:
  ld    TOS,1
  urnext
endcode

;; ascii case-insensitive compare
code: S=CI  ( addr0 count0 addr1 count1 -- flag )
  ;; compare lengthes
  cp    TOS,[esp+4]
  jr    nz,.lenfail
  or    ecx,ecx
  jr    z,.lenzero
  ;; perform memcmp
  pop   edi
  pop   eax
  xchg  esi,[esp]
  ;; now EIP is saved on the stack
.cmploop:
  lodsb
  ld    ah,[edi]
  inc   edi
  ;; it may work
  cp    al,ah
  jr    nz,.trycase
.caseequ:
  loop  .cmploop
  ;; success
  ;; restore EIP
  pop   esi
  ld    TOS,1
  urnext

.trycase:
  cp    al,'a'
  jr    c,@f
  cp    al,'z'+1
  jr    nc,@f
  sub   al,32
@@:
  cp    ah,'a'
  jr    c,@f
  cp    ah,'z'+1
  jr    nc,@f
  sub   ah,32
@@:
  cmp   al,ah
  jr    z,.caseequ
  ;; failure
  ;; restore EIP
  pop   esi
  xor   TOS,TOS
  urnext

.lenfail:
  ;; early failure
  add   esp,4*3
  xor   TOS,TOS
  urnext

.lenzero:
  ;; early success
  add   esp,4*3
  ld    TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UPCASE-CHAR  ( ch -- ch )
  and   TOS,0xff
  cp    cl,'a'
  jr    c,@f
  cp    cl,'z'+1
  jr    nc,@f
  sub   cl,32
@@:
  urnext
endcode

code: UPCASE-STR  ( addr count -- )
  pop   edi
  test  TOS,0x80000000
  jr    nz,.done
  or    TOS,TOS
  jr    z,.done
.convloop:
  ld    al,[edi]
  cp    al,'a'
  jr    c,@f
  cp    al,'z'+1
  jr    nc,@f
  sub   byte [edi],32
@@:
  inc   edi
  loop  .convloop
.done:
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; converts some escape codes in-place
;; used for `."` and `"`
;; the resulting string is never bigger that the source one
;; this will not preserve the trailing zero byte
code: STR-UNESCAPE  ( addr count -- addr count )
  jecxz .quit
  ld    edi,[esp]
  push  ecx

.scanloop:
  ld    al,92
  repnz scasb
  jecxz .done  ;; nope
  jr    nz,.done
  ;; edi is after backslash
  ;; ecx is number of chars left after backslash
  ;; found backslash, check next char
  ld    al,[edi]
  ;; '\r'?
  cp    al,'r'
  jr    nz,@f
  ld    al,13
  jp    .replace_one
@@:
  ;; '\n'?
  cp    al,'n'
  jr    nz,@f
  ld    al,10
  jp    .replace_one
@@:
  ;; '\t'?
  cp    al,'t'
  jr    nz,@f
  ld    al,9
  jp    .replace_one
@@:
  ;; '\b'? (bell)
  cp    al,'b'
  jr    nz,@f
  ld    al,7
  jr    .replace_one
@@:
  ;; '\e'? (escape)
  cp    al,'e'
  jr    nz,@f
  ld    al,27
  jr    .replace_one
@@:
  ;; '\z'? (zero)
  cp    al,'z'
  jr    nz,@f
  xor   al,al
  jr    .replace_one
@@:
  ;; '\`'? (double quote)
  cp    al,'`'
  jr    nz,@f
  ld    al,'"'
  jp    .replace_one
@@:
  ;; 'xHH'?
  cp    al,'x'
  jr    z,.esc_hex
  cp    al,'X'
  jr    z,.esc_hex
  jr    .replace_one
.loop_cont:
  loop  .scanloop

.done:
  pop   ecx
.quit:
  urnext

.esc_hex:
  cp    ecx,2
  jr    c,.loop_cont
  ld    al,[edi+1]
  call  .hexdigit
  jr    c,.loop_cont
  ;; save original string position
  push  esi
  ld    esi,edi
  ;; skip 'x'
  inc   edi   ;; skip 'x'
  dec   ecx
  dec   dword [esp+4]
  ;; skip first digit
  inc   edi
  dec   ecx
  dec   dword [esp+4]
  jecxz .esc_hex_done
  ld    ah,al
  ld    al,[edi]
  call  .hexdigit
  jr    nc,@f
  ld    al,ah
  jr    .esc_hex_done
@@:
  ;; combine two hex digits
  shl   ah,4
  or    al,ah
  ;; skip second digit
  inc   edi
  dec   ecx
  dec   dword [esp+4]
.esc_hex_done:
  ld    [esi-1],al
  jecxz .esc_hex_quit
  ;; remove leftover chars
  ;; ECX: chars left
  ;; ESI: position after backslash
  ;; EDI: rest position
  ;; old ESI is on the stack
  push  esi   ;; to be restored in EDI
  push  ecx
  xchg  esi,edi
  rep movsb
  pop   ecx
  pop   edi   ;; get back to backslash
  pop   esi   ;; restore old ESI
  jp    .scanloop

.esc_hex_quit:
  pop   esi
  jr    .done

.replace_one:
  ld    [edi-1],al
  dec   dword [esp]
  dec   ecx
  jecxz .done
  ;; move
  push  esi
  push  edi
  push  ecx
  ld    esi,edi
  inc   edi
.domove:
  xchg  esi,edi
  rep movsb
  pop   ecx
  pop   edi
  pop   esi
  jp    .scanloop

.hexdigit:
  sub   al,'0'
  jr    c,.hexdigit_done
  cp    al,10
  ccf
  jr    nc,.hexdigit_done
  sub   al,7
  jr    c,.hexdigit_done
  cp    al,16
  ccf
  jr    nc,.hexdigit_done
  ;; maybe its lowercase?
  cp    al,42
  jr    c,.hexdigit_done
  sub   al,32
  cp    al,16
  ccf
.hexdigit_done:
  ret
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; `count` will be 0 if there is no char
;; `count` will include char
;; `addr` is not changed in any case
code: STR-TRIM-AFTER-LAST-CHAR  ( addr count char -- addr count )
  ;; i can do it faster, but meh...
  ld    eax,TOS
  pop   TOS
  ld    edi,[esp]
  jecxz .notfound
  test  TOS,0x80000000
  jr    nz,.notfound
  add   edi,TOS
.scanloop:
  dec   edi
  cp    byte [edi],al
  jr    z,.done
  loop  .scanloop
.notfound:
  xor   TOS,TOS
  urnext
.done:
  jecxz .notfound
  urnext
endcode


;; `count` will be 0 if there is no char
;; `count` will not include a colon
;; `addr` is not changed in any case
code: STR-TRIM-AFTER-CHAR  ( addr count char -- addr count )
  ;; i can do it faster, but meh...
  ld    eax,TOS
  pop   TOS
  ld    edi,[esp]
  jecxz .notfound
  test  TOS,0x80000000
  jr    nz,.notfound
  xor   edx,edx
.scanloop:
  cp    byte [edi],al
  jr    z,.done
  inc   edi
  inc   edx
  loop  .scanloop
.notfound:
  xor   TOS,TOS
  urnext
.done:
  inc   edx
  ld    TOS,edx
  urnext
endcode


;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AFTER-LAST-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-after-last-char
  nip dup >r
  - swap r> + swap
;

;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AFTER-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-after-char
  nip dup >r
  - swap r> + swap
;


;; `count` will be 0 if there is no char
;; `count` will not include char
;; `addr` is not changed in any case
: STR-TRIM-AT-LAST-CHAR  ( addr count char -- addr count )
  str-trim-after-last-char dup if 1- endif
;

;; `count` will be 0 if there is no char
;; `count` will not include char
;; `addr` is not changed in any case
: STR-TRIM-AT-CHAR  ( addr count char -- addr count )
  str-trim-after-char dup if 1- endif
;


;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AT-LAST-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-at-last-char
  nip dup >r
  - swap r> + swap
;

;; `count` will be 0 if there is no char (and `addr` will be unchanged)
;; `addr` will not include char
: STR-SKIP-AT-CHAR  ( addr count char -- addr count )
  >r 2dup r> str-trim-at-char
  nip dup >r
  - swap r> + swap
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; count can be 0 if no path there
;; trailing '/' is included
: STR-EXTRACT-PATH  ( addr count -- addr count )
  [char] / str-trim-after-last-char
;

;; count can be 0 if no name there (and `addr` is not changed)
: STR-EXTRACT-NAME  ( addr count -- addr count )
  [char] / str-skip-after-last-char
;

;; count can be 0 if no extension there (and `addr` is not changed)
;; dot is included
: STR-EXTRACT-EXT  ( addr count -- addr count )
  2dup str-extract-name ?dup
  ifnot
    ;; ( addr count dummyaddr )
    2drop 0 exit
  endif
  ;; ( addr count naddr ncount )
  [char] . str-skip-at-last-char ?dup
  ifnot
    ;; ( addr count dummyaddr )
    2drop 0
  else
    ;; ( addr count extaddr extcount )
    2swap 2drop
  endif
;

;; count can be 0 if no base name there (and `addr` is not changed)
: STR-EXTRACT-BASE-NAME  ( addr count -- addr count )
  ;; get base name
  2dup str-extract-name ?dup
  ifnot
    ;; no file name, nothing to extract
    2drop 0 exit
  endif
  ;; trim at extension
  ;; ( addr count nameaddr namecount )
  2dup [char] . str-trim-at-last-char
  ;; ( addr count nameaddr namecount bnaddr bncount )
  ?dup ifnot
    ;; ( addr count nameaddr namecount dummyaddr )
    drop 2>r 2drop 2r> exit
  endif
  ;; ( addr count nameaddr namecount bnaddr bncount )
  2>r 2drop 2drop 2r>
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: C4S-ZTERM  ( addr -- )
  count + 0!
;

: C4S-COPY  ( addrsrc addrdest -- )
  >r dup count-only cell+ r> swap cmove
;

: C4S-COPY-A-C  ( addrsrc count addrdest -- )
  2dup !
  cell+ swap cmove
;

: C4S-CAT-A-C  ( addr count addrdest -- )
  over 0> if
    dup >r count + swap dup >r cmove
    r> r> +!
  else
    2drop drop
  endif
;
