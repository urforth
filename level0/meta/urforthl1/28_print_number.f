;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$variable "(HLD)" 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: HEX     ( -- )  16 base ! ;
: DECIMAL ( -- )  10 base ! ;
: OCTAL   ( -- )  8 base ! ;
: BINARY  ( -- )  2 base ! ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: HOLD  ( ch -- )
  (hld) @ 1- dup (hld) ! c!
;

: SIGN  ( n -- )
  0< if [char] - hold endif
;

: <#  ( d -- d )
  pad (hld) !
;

: <#U  ( n -- d )
  0 <#
;

: #>  ( d -- addr count )
  2drop (hld) @ pad over -
;

: #  ( d -- n )
  base @ uds/mod dup 9 > if 7 + endif
  48 + hold
;

: #S  ( d -- d )
  begin # 2dup or not-until
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: .R  ( n fldlen -- )
  >r dup >r abs <#u #s r> sign #>
  ;; ( addr count )
  r> over - spaces type
;

: U.R  ( u fldlen -- )
  >r <#u #s #>
  ;; ( addr count )
  r> over - spaces type
;

: .  ( n -- )
  0 .r space
;

: U.  ( n -- )
  0 u.r space
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: .HEX8  ( n -- )
  base @ >r hex
  <#u # # # # # # # # #> type
  r> base !
;
