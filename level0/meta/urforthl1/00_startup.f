;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (URFORTH-STARTUP-CODEBLOCK)
(hidden)
(codeblock)
urforth_entry_point:
  ;; save argc
  ld    ecx,[esp]
  ld    [pfa "argc"],ecx
  ;; save argv
  ld    eax,esp
  add   eax,4
  ld    [pfa "argv"],eax
  ;; calc envp address
  inc   ecx    ;; skip final 0 argument
  shl   ecx,2
  add   eax,ecx
  ;; store envp
  ld    [pfa "envp"],eax

  xor   eax,eax
  ;; push several values for safety (so data stack underflow won't destroy cli args and such)
  ld    ecx,64
@@:
  push  eax
  loop  @b

  ;; allocate memory for dictionary
  ;; binary size should be equal to DP
  ld    eax,[pfa "(init-mem-size)"]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [pfa "dp-end"],ebx

  ;; allocate memory for TIB
  ld    eax,[pfa "(default-#tib)"]
  ;; some more bytes for safety
  add   eax,128
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  add   eax,32  ;; reserve some TIB memory for various needs
  ld    [pfa "(default-tib)"],eax

  ;; allocate memory for PAD
  ld    eax,[pfa "#pad-area-resv"]
  add   eax,[pfa "#pad-area"]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  add   eax,[pfa "#pad-area-resv"]
  ld    [pfa "pad-area"],eax

  ;; allocate memory for debug buffer
  ld    eax,[pfa "(dbg-buf-size)"]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [pfa "(dbg-buf-ptr)"],eax

  ;; allocate memory for current file we are interpreting
  ld    eax,[pfa "(#tib-curr-fname)"]
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ld    [pfa "(tib-curr-fname)"],eax
  ld    [pfa "(tib-curr-fname-default)"],eax
  ld    dword [eax],0  ;; no file

  ;; allocate memory for debugger/segfault stack
  ld    eax,1024
  call  .brkalloc
  ;; EAX: start address
  ;; EBX: end address
  ;; align it
  and   ebx,0xfffffff0
  ld    [urfsegfault_stack_bottom],ebx

  ;; prepare data stack (use "push" to trigger stack pages allocation)
  mov   edx,esp   ;; save current stack pointer
  xor   eax,eax

  mov   ecx,DSTACK_SIZE
.dstack_clear_loop:
  push  eax
  loop  .dstack_clear_loop

  ;; prepare return stack (use "push" to trigger stack pages allocation)
  mov   ERP,esp   ;; setup initial return stack pointer
  mov   ecx,RSTACK_SIZE
.rstack_clear_loop:
  push  eax
  loop  .rstack_clear_loop

  ;; restore stack
  mov   esp,edx

  ;; save stack bottoms
  mov   [pfa "sp0"],esp
  mov   [pfa "rp0"],ERP

  call  urforth_setup_segfault_handler

  ;; and start execution
  jp    cfa "(COLD-FIRSTTIME)"

;; allocate memory via BRK
;; IN:
;;   EAX: size
;; OUT:
;;   EAX: start address (first byte)
;;   EBX: end address (*after* the last byte)
;;   other registers and flags are dead (except ESP)
.brkalloc:
  ;; save size (overallocate a little, for safety)
  add   eax,64
  push  eax

  ;; get current BRK address
  ld    eax,45       ;; brk
  xor   ebx,ebx
  syscall
  push  eax
  ;; [esp]: start address
  ;; [esp+4]: alloc size

  ;; allocate
  ld    ebx,eax
  add   ebx,[esp+4]
  ld    eax,45       ;; brk
  syscall
  ld    ebx,eax      ;; EBX=end address
  ;; check for OOM
  ld    eax,[esp]    ;; start address
  add   eax,[esp+4]  ;; alloc size
  cp    ebx,eax
  jp    c,.startup_oom
  ;; start address
  pop   eax
  ;; calc end address, to be precise
  pop   ebx
  ;; remove overallocated safety margin from end address
  sub   ebx,64
  add   ebx,eax
  ret

.fatal_oom_msg: db "FATAL: out of memory!",10
.fatal_oom_msg_len equ $-.fatal_oom_msg

.startup_oom:
  ;; print error and exit
  mov   eax,4     ;; write
  mov   ebx,2     ;; stderr
  mov   ecx,.fatal_oom_msg
  mov   edx,.fatal_oom_msg_len
  syscall

  mov   eax,1     ;; exit
  mov   ebx,1
  syscall
endcode
