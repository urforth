;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; "TIB" and "#TIB" will be set by the startup code
$variable "TIB"  0
$variable "#TIB" 0
$variable ">IN"  0
$variable "TIB-LINE#" 0

;; "TIB-GETCH" will set this if last read char is not EOL
;; parsing words will set this too, as if they're using "TIB-GETCH"
$value "(TIB-LAST-READ-CHAR)" 0

;; c4str, with trailing slash
$value "(LAST-TLOAD-PATH-ADDR)" 0

;; current file we are interpreting
;; c4str
$value "(TIB-CURR-FNAME)" 0
(hidden)
$value "(TIB-CURR-FNAME-DEFAULT)" 0
(hidden)
$constant "(#TIB-CURR-FNAME)" 4096
(hidden)

;; size of TIB save buffer
$constant "#TIB-SAVE-BUFFER" 5*4


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: TIBSTATE>R  ( -- | -- savedtibstate )
  sub   ERP,5*4
  ld    eax,[pfa "tib"]
  ld    [ERP+0],eax
  ld    eax,[pfa "#tib"]
  ld    [ERP+4],eax
  ld    eax,[pfa ">in"]
  ld    [ERP+8],eax
  ld    eax,[pfa "tib-line#"]
  ld    [ERP+12],eax
  ld    eax,[pfa "(tib-last-read-char)"]
  ld    [ERP+16],eax
  urnext
endcode

code: R>TIBSTATE  ( -- | savedtibstate -- )
  ld    eax,[ERP+0]
  ld    [pfa "tib"],eax
  ld    eax,[ERP+4]
  ld    [pfa "#tib"],eax
  ld    eax,[ERP+8]
  ld    [pfa ">in"],eax
  ld    eax,[ERP+12]
  ld    [pfa "tib-line#"],eax
  ld    eax,[ERP+16]
  ld    [pfa "(tib-last-read-char)"],eax
  add   ERP,5*4
  urnext
endcode

code: RDROP-TIBSTATE
  add   ERP,5*4
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: TIB-DEFAULT?  ( -- bool )
  tib @ (default-tib) =
;

;; reset TIB to the default one
: TIB-RESET ( -- )
  (default-tib) tib !
  (default-#tib) #tib !
  >in 0!
  tib-line# 0!
  0 to (last-tload-path-addr)
  (tib-curr-fname-default) to (tib-curr-fname)
;


;; will never be negative
;; 0 means "END-OF-TIB"
code: TIB-PEEKCH  ( -- ch-or-0 )
  push  TOS
  xor   TOS,TOS
  ld    eax,[pfa ">in"]
  cp    eax,dword [pfa "#tib"]
  jr    nc,@f
  add   eax,[pfa "tib"]
  movzx TOS,byte [eax]
  ;; convert zero to 32
  ld    eax,32
  or    cl,cl
  cmovz ecx,eax
@@:
  urnext
endcode

;; will never be negative
;; 0 means "END-OF-TIB"
code: TIB-GETCH  ( -- ch-or-0 )
  push  TOS
  xor   TOS,TOS
  ld    eax,[pfa ">in"]
  cp    eax,dword [pfa "#tib"]
  jr    nc,@f
  add   eax,[pfa "tib"]
  movzx TOS,byte [eax]
  ;; convert zero to 32
  ld    eax,32
  or    cl,cl
  cmovz ecx,eax
  ;; update last read char
  ld    [pfa "(tib-last-read-char)"],TOS
  ;; update position
  inc   dword [pfa ">in"]
  ;; update current line
  cp    cl,10
  jr    nz,@f
  cp    dword [pfa "tib-line#"],0
  jr    z,@f
  inc   dword [pfa "tib-line#"]
@@:
  urnext
endcode


;; returns line number for the current TIB position
code: TIB-CALC-CURRLINE  ( -- linenum-1 )
  push  TOS
  ld    edi,[pfa "tib"]
  ld    ecx,[pfa ">in"]
  ld    al,10
  xor   edx,edx   ;; line counter
@@:
  cp    ecx,1
  jr    l,@f
  repne scasb
  jr    nz,@f
  inc   edx
  jr    @b
@@:
  ld    TOS,edx
  urnext
endcode


: ACCEPT  ( addr maxlen -- readlen // -1 )
  dup 0 <= err-input-too-long ?error
  0
  ;; ( addr maxlen currcount )
  begin
    key
    ;; eof or cr or lf?
    dup -1 =
    over 10 = or
    over 13 = or
  not-while
    ;; ( addr maxlen currcount char )
    >r
    ;; can we put it?
    2dup > if
      ;; yep, store
      ;; ( addr maxlen currcount | char )
      rot r> over c! 1+ nrot 1+
    else
      ;; nope
      rdrop  ;; drop char, we have no room for it
      ;; need a bell?
      2dup = if
        bell
        1+  ;; no more bells
      endif
    endif
  repeat
  ;; ( addr maxlen currcount char )
  -1 <> if
    reset-emitcol  ;; because OS did cr (i hope)
  endif
  ;; check for overflow
  2dup < if
    ;; oops, overflow
    2drop drop -1
  else
    nrot 2drop
  endif
;


;; either refills TIB and sets flag to true, or does nothing and sets flag to false
: REFILL  ( -- flag )
  tib-default? ifnot
    false
  else
    begin
      tib @ #tib @ 1- accept
      dup 0<
    while
      drop
      endcr ." ERROR: ACCEPT buffer overflow\n"
    repeat
      ;;tib @ over type 124 emit dup . cr
    ;; put trailing zero
    tib @ + 0c!
    >in 0!
    true
  endif
;
