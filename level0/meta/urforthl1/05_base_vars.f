;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "WLIST-HASH-BITS" WLIST_HASH_BITS
$constant "ALIGN-WORD-HEADERS?" URFORTH_ALIGN_HEADERS

$constant "URFORTH-LEVEL" 1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$variable "DP" urforth_code_end
$variable "DP-END" 0
$variable "DP-TEMP" 0  ;; this is used to temporarily change HERE
$variable "SP0" 0
$variable "RP0" 0
$variable "STATE" 0
$variable "BASE" 10


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pad is allocated with BRK; "#PAD-AREA-RESV" is the space BEFORE "PAD-AREA"
$variable "PAD-AREA" 0  ;; will be set by startup code
$constant "#PAD-AREA-RESV" 2048
$constant "#PAD-AREA" 4096


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "(INIT-MEM-SIZE)" 1024*1024*2
(hidden)

$value "(DEFAULT-#TIB)" 1024
(hidden)

;; this will be patched by the startup code
$value "(DEFAULT-TIB)" 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$variable "ARGC" 0
$constant "ARGV" 0
$constant "ENVP" 0

;; index of the next CLI arg to process
;; set to 0, negative or ARGC to stop further processing
$value "CLI-ARG-NEXT" 1

;; this is so COLD will process CLI args only once
$variable "(PROCESS-CLI-ARGS?)" 1
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mostly controls number parsing for now: "#" prefix is decimal in shit2012
;; bit0: allow 2012 number prefix idiocity and number signs
;; bit1: use non-FIG VARIABLE
$value "(SHIT-2012-IDIOCITY)" 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "(CODE-BASE-ADDR)" urforth_code_base_addr
(hidden)

;; two dwords -- disk and memory; should be equal
$constant "(ELF-HEADER-CODE-SIZE-ADDR)" elfhead_codesize_addr
(hidden)

$constant "(CODE-IMPORTS-ADDR)" elfhead_impstart
(hidden)

;; in bytes
$constant "(CODE-IMPORTS-SIZE)" elfhead_implen
(hidden)

$constant "(CODE-ENTRY-ADDR)" urforth_entry_point
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "TLOAD-VERBOSE-LIBS" 1
$value "TLOAD-VERBOSE" 1
$value "TLOAD-VERBOSE-DEFAULT" 1
$value "TLOAD-VERBOSE-RC" 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "TRUE"  1
$constant "FALSE" 0

$constant "CELL" 4
$constant "BL" 32
