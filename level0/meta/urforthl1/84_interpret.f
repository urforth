;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ARGV-STR  ( argnum -- addr count )
  dup 0< over argc @ >= or if
    drop pad 0
  else
    cells argv + @ zcount
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: INTERPRET ( ??? )
  begin
    (sp-check) ifnot
      (*
      if URFORTH_DEBUG
      par_dbg_on_sunder
      if
        bp
      endif
      end if
      *)
      err-stack-underflow error
      1 n-bye  ;; just in case
    endif
    begin
      parse-name-ex
      ;; ( addr count )
      ?dup
    not-while
      drop
      ;; for default TIB (i.e. terminal session), "QUIT" will do it for us
      tib-default? if exit endif
      refill ifnot exit endif
    repeat
    ;; ( addr count )
    2dup wfind-str if
      ;; i found her!
      nrot
        \ endcr ." INTRP-FOUND:<" 2dup type ." >\n"
      2drop
      ;; ( cfa )
      ;; if interpreting or immediate word, execute it
      dup cfa->nfa nfa->ffa ffa@ (wflag-immediate) and
      state @ 0=  ;; !0 means "compile"
      or if
        ;; execute
        execute
      else
        ;; compile
        compile,
      endif
    else
      ;; unknown word, try to parse it as a number
      2dup number if
        nrot 2drop
        [compile] literal
      else
        type err-unknown-word error
      endif
    endif
  again
;


: .OK  ( -- )
  (sp-check) ?endcr if space endif if ." ok" else err-stack-underflow error-message endif
  false >r  ;; "bracket opened" flag
  ;; show stack depth
  depth ?dup if
    r> 1+ >r
    ."  ("
    base @ >r 0 .r r> base !
  endif
  ;; show BASE if it is not decimal
  base @ 10 <> if
    r> ifnot ."  (" endif
    true >r
    ." ; base:" base @ dup >r decimal 0 .r r> base !
  endif
  r> if [char] ) emit endif
;


: QUIT ( ??? )
  begin
    rp0!
    ;;state 0poke  ;; nope, we want multiline definitions to work
    .ok cr
    tib-reset
    ;; there is no reason to keep debug info activated
    (dbginfo-reset)
    tload-verbose-default to tload-verbose
    refill ifnot bye endif
    interpret
  again
; (noreturn)


: EVALUATE  ( addr count -- ... )
  dup 0> if
    tibstate>r
    #tib !
    tib !
    >in 0!
    tib-line# 0!
    interpret
    r>tibstate
  else
    2drop
  endif
;
