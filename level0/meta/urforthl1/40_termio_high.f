;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


$defer "EMIT" cfa "(EMIT)"
;; ( ch -- )

$defer "CR" cfa "(CR)"
;; ( -- )

$defer "BELL" cfa "(BELL)"
;; ( -- )

$defer "ENDCR" cfa "(ENDCR)"
;; ( -- )

;; should "ENDCR" do "CR"?
$defer "?ENDCR" cfa "(?ENDCR)"
;; ( -- flag )

$defer "RESET-EMITCOL" cfa "(RESET-EMIT-COL)"
;; ( -- )

$defer "KEY" cfa "(GETCH)"
;; ( -- ch )


: SAFE-EMIT  ( ch -- )
  0xff and
  dup 32 < if
    ;; < 32: allow tab, cr, lf
    dup 9 =
    over 10 = or
    over 13 = or
    ifnot
      drop 63
    endif
  else
    dup 127 = if drop 63 endif
  endif
  emit
;


: TYPE-WITH  ( addr length emitcfa -- )
  over 0> if
    nrot over + swap do i c@ over execute loop
  else
    2drop
  endif
  drop
;

: TYPE  ( addr length -- )
  ['] emit type-with
;

: SAFE-TYPE  ( addr length -- )
  ['] safe-emit type-with
;

: SPACE  ( -- )
  bl emit
;

: SPACES  ( n -- )
  dup 0> if
    0 do space loop
  else
    drop
  endif
;

;; type asciiz string
: TYPE-ASCIIZ  ( addr -- )
  zcount type
;

;; type asciiz string
: SAFE-TYPE-ASCIIZ  ( addr -- )
  zcount safe-type
;


: (.")  ( -- )
  r@ count type r> count + 1+ >r
; (hidden) (arg_c4strz)
