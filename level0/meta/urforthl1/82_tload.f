;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (TLOAD-FNPAD)  ( -- filenamepad )
  pad 2048 +
; (hidden)


: (TLOAD-OPEN-ADD-ALL)  ( -- )
  (tload-fnpad) c4s-zterm
  (tload-fnpad) count (dir?) if
    ;; add trailing slash
    (tload-fnpad) count + 1- c@ [char] / =
    ifnot
      " /" (tload-fnpad) c4s-cat-a-c
    endif
    " all.f" (tload-fnpad) c4s-cat-a-c
    (tload-fnpad) c4s-zterm
  endif
; (hidden)


;; also, leaves file name as c4str at (tload-fnpad)
: (TLOAD-OPEN)  ( addr count -- fd )
  dup 0<= if 2drop err-file-not-found error endif

  ;; ! will be replaced with binary path
  over c@ [char] ! = if
    0 argv-str str-extract-path ?dup ifnot drop " ./" endif
    (tload-fnpad) c4s-copy-a-c
    1- swap 1+ swap
    (tload-fnpad) c4s-cat-a-c
  else
    (last-tload-path-addr)
    if
      2dup 2>r
      (last-tload-path-addr) (tload-fnpad) c4s-copy
      ;; ( addr count | addr count )
      (tload-fnpad) c4s-cat-a-c
      (tload-open-add-all)
      (tload-fnpad) count
      ;; ( addr count | addr count )
      o-rdonly 0 (fopen)
      dup 0>= if 2rdrop exit endif
      drop 2r>
    endif
    (tload-fnpad) c4s-copy-a-c
    (tload-open-add-all)
  endif
  (tload-open-add-all)
  (tload-fnpad) count o-rdonly 0 (fopen)
  dup 0< if
    drop endcr ." file: \`" (tload-fnpad) count type ." \` " err-file-not-found error
  endif
; (hidden)


;; load forth source file
;; this loads the whole file into temp pool
: TLOAD  ( addr count -- )
  (tload-open)
  tload-verbose if
    endcr ." loading: " (tload-fnpad) count type cr
  endif
  >r
  ;; ( | fd )
  0 (seek-end) r@ (lseek)
  ;; ( size | fd )
  dup 0< if
    drop r> (fclose) drop err-file-read-error error
  endif
  ;; seek back to file start
  ;; ( size | fd )
  0 (seek-set) r@ (lseek) if
    drop r> (fclose) drop err-file-read-error error
  endif
  ;; allocate temp pool space
  ;; this will also take care about too big files
  ;; FIXME: the file won't be closed if it is too big!
  r> tmp-pool-mark >r >r
  ;; ( size | poolmark fd )
  dup cell+ tmp-pool-alloc
  ;; ( size addr | poolmark fd )
  ;; write zeroes at the end (just in case)
  2dup + 0!
  ;; load file
  2dup swap r@ (fread)
  ;; ( size addr readbytes | poolmark fd )
  ;; close file
  r> (fclose) drop
  ;; ( size addr readbytes | poolmark )
  ;; ( addr readbytes | poolmark )
  rot over - err-file-read-error ?error
  ;; ( addr readbytes | poolmark )
  tibstate>r
  #tib !
  tib !
  >in 0!
  1 tib-line# !
  ;; ( | poolmark tibstate... )
    ;; (tload-fnpad) count type cr
  ;; store file path
  (tload-fnpad) count str-extract-path ?dup ifnot drop " ./" endif
    ;; 2dup type cr
  dup cell+ tmp-pool-alloc
  (last-tload-path-addr)
  >r to (last-tload-path-addr)
  (last-tload-path-addr) c4s-copy-a-c
    ;; (last-tload-path-addr) count type cr
  cell tmp-pool-alloc r> swap !
  ;; save current file name, and replace it
  (tib-curr-fname) count-only cell+ tmp-pool-alloc
  >r  ;; old fname at rstack
  (tib-curr-fname) r@ over count-only cell+ move
  (tload-fnpad) (tib-curr-fname) over count-only cell+ move
  ;; process it
  interpret
  ;; restore old fname
  r> (tib-curr-fname) over count-only cell+ move
  ;; restore path
  (last-tload-path-addr) cell- @ to (last-tload-path-addr)
  r>tibstate
  r> tmp-pool-release
  ;;pardottype "done!" cr
  ;;tib_reset
;
