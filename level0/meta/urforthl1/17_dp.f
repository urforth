;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; allocate some memory at HERE (DP or DP-TEMP), return starting address
: N-ALLOT  ( n -- start-addr )
  dup 0< err-negative-allot ?error
  dup 0x00ffffff u> err-out-of-memory ?error  ;; 16MB is quite huge allocation chunk ;-)
  dp-temp @ ?dup if
    ;; ( n start-addr )
    dp-temp
  else
    dp @ dp
  endif
  ;; ( n start-addr dpaddr )
  ;; check for oom
  >r
  ;; ( n start-addr | dpaddr )
  2dup +
  dup dp-end @ 32768 - u> err-out-of-memory ?error
  tmp-pool-here 256 - u>= err-out-of-memory ?error
  swap r> +!
;

$if URFORTH_ALIGN_HEADERS
: (ALIGN-HERE)  ( -- )
  here 3 and ?dup if
    4 swap - dup n-allot swap erase
  endif
;
$else
: (ALIGN-HERE)  ( -- )
;
$endif

: ALLOT  ( n -- )
  n-allot drop
;

code: USED  ( -- count )
  push  TOS
  mov   TOS,[pfa "dp"]
  sub   TOS,[pfa "(code-base-addr)"]
  urnext
endcode

code: UNUSED  ( -- count )
  push  TOS
  mov   TOS,[pfa "dp-end"]
  sub   TOS,[pfa "dp"]
  sub   TOS,32768   ;; reserved area
  urnext
endcode

code: HERE  ( -- addr )
  push  TOS
  ld    TOS,[pfa "dp-temp"]
  or    TOS,TOS
  cmovz TOS,[pfa "dp"]
  urnext
endcode

code: REAL-HERE  ( -- addr )
  push  TOS
  ld    TOS,[pfa "dp"]
  urnext
endcode

code: PAD  ( -- addr )
  push  TOS
  mov   TOS,[pfa "pad-area"]
  urnext
endcode


: C,  ( c -- )
  (dbginfo-add-here)
  1 n-allot c!
;

: W,  ( w -- )
  (dbginfo-add-here)
  2 n-allot w!
;

: ,  ( n -- )
  (dbginfo-add-here)
  cell n-allot !
;
