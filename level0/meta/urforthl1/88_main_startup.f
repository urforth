;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (COLD-BUILD-RC-NAME)  ( -- )
  0 argv-str str-extract-path ?dup ifnot drop " ./" endif
  (tload-fnpad) c4s-copy-a-c
  0 argv-str str-extract-base-name (tload-fnpad) c4s-cat-a-c
  " .rc" (tload-fnpad) c4s-cat-a-c
  (tload-fnpad) c4s-zterm
; (hidden)


: (COLD-LOAD-RC)  ( -- ??? )
  ;; check for "--naked"
  argc @ 1 > if
    argc @ 1 do
      \ TODO: create special dictionary for cli args, and simply execute them
      i argv-str " --naked" s=ci if unloop exit endif
      1  ;; argskip
      i argv-str " --verbose-libs" s=ci if 1 to tload-verbose-libs endif
      i argv-str " --quiet-libs" s=ci if 0 to tload-verbose-libs endif
      i argv-str " --verbose-rc" s=ci if 1 to tload-verbose-rc endif
      i argv-str " --quiet-rc" s=ci if 0 to tload-verbose-rc endif
      i argv-str " --eval" s=ci if drop 2 endif
      i argv-str " -e" s=ci if drop 2 endif
    +loop
  endif
  ;; no "--naked", load .rc
  (cold-build-rc-name)
  (tload-fnpad) count (file?) if
    tload-verbose >r
    ;; be silent
    tload-verbose-rc to tload-verbose
    ;; (cold-build-rc-name)
    ;; copy it to HERE
    (tload-fnpad) here c4s-copy
    here count tload
    r> to tload-verbose
  endif
; (hidden)


: CLI-ARG-SKIP  ( -- )
  cli-arg-next 1+ to cli-arg-next
;

: (COLD-CLI)  ( -- ??? )
  ;; load "urforth.rc" (if present)
  (cold-load-rc)
  ;; process CLI args
  ;; don't use DO ... LOOP here, use CLI-ARG-NEXT
  begin
    cli-arg-next argc @ <
    cli-arg-next 0> and
  while
    ;; empty arg?
    cli-arg-next argv-str cli-arg-skip
    ?dup ifnot
      drop
    else
      ;; ( addr count )
      ;; "-..." args?
      over c@ [char] - =
      if
        ;; "-e" or "--eval" ?
        2dup " -e" s=ci >r
        " --eval" s=ci r> or
        ;; note that argv is dropped here
        if
          ;; set TIB and eval
          cli-arg-next argv-str cli-arg-skip evaluate
        endif
        ;; unknown "-..." args are skipped
      else
        ;; filename arg
        tload
      endif
    endif
  repeat
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (.BANNER)  ( -- )
  ." UrForth v0.1.0-l1-beta\n"
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called to print a banner after processing CLI args
$defer ".BANNER" cfa "(.banner)"

;; called (once!) to process CLI args
$defer "PROCESS-CLI-ARGS" cfa "(cold-cli)"

;; main program loop, should never return
$defer "MAIN-LOOP" cfa "quit"  ;; this word should never return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (COLD-FIRSTTIME)  ( ??? )
  (sys-gettickcount-init)
  cold
  ;; just in case it returns
  bye
; (hidden) (noreturn)


: COLD  ( ??? )
  sp0! rp0!
  tmp-pool-reset
  tib-reset
  tload-verbose-default to tload-verbose
  dp-temp 0!
  (process-cli-args?) @ if
    (process-cli-args?) 0!  ;; no more
    process-cli-args
  endif
  .banner
  main-loop
  ;; just in case it returns
  bye
; (noreturn)
