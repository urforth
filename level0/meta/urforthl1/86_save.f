;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; all things is this chain will be called before and after saving the image
;;   ( beforesafeflag -- )
$value "(pre-post-save-chain)" 0
(hidden)

: (save-add-pp-chain)  ( cfa -- )
  \ FIXME: create proper headers for this?
  ;; new chain will point to here
  here >r
  (pre-post-save-chain) ,   ;; save previous
  ,                         ;; save cfa
  r> to (pre-post-save-chain)  ;; remember new
; (hidden)


: (save-call-pp-chain)  ( beforesafeflag -- )
  (pre-post-save-chain)
  begin
    ?dup
  while
    2dup cell+ @ execute
    @
  repeat
  drop
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; save current image to ELF executable file
;; we're using the fact that our ELF header is loaded into memory, and is writeable
;; metacompiler will prolly need to rebuild it from scratch, but for now... meh
: (SAVE)  ( fd -- successflag )
  ;; just in case, check fd
  dup 0< if drop false exit endif
  ;; move fd to return stack
  >r
  ;; fix code segment size
  ;; write code from code start to real HERE
  real-here (code-base-addr) - dup (elf-header-code-size-addr) ! (elf-header-code-size-addr) cell+ !
  ;; everything in our header is ok now, including entry point (it isn't changed)
  ;; write everything up until import table
  (code-base-addr) (code-imports-addr) (code-base-addr) - r@ (fwrite)
  (code-imports-addr) (code-base-addr) - = ifnot rdrop false exit endif
  ;; write zero bytes for imports: this is where import addresses will be put by ld.so
  ;; use HERE for this
  real-here (code-imports-size) erase
  real-here (code-imports-size) r@ (fwrite)
  (code-imports-size) = ifnot rdrop false exit endif
  ;; setup some variables first, so the new image will process CLI args
  ;; don't bother moving old values to "safe place", data stack is good enough
  cli-arg-next
  (process-cli-args?) @
  1 to cli-arg-next
  (process-cli-args?) 1!
  ;; write code from imports end to real here
  (code-imports-addr) (code-imports-size) + real-here over - r@ over >r (fwrite)
  ;; restore variables, just in case (TOS is write result)
  >r  ;; move write result to the safe place
  (process-cli-args?) !
  to cli-arg-next
  r> r>   ;; restore write result and number of bytes written
  = ifnot rdrop false exit endif
  ;; you may not believe me, but we're done!
  rdrop    ;; got rid of fd
  true     ;; exit with success
; (hidden)


: SAVE  ( addr count -- successflag )
  ;; create output file
  o-wronly o-creat or o-trunc or  ;; flags
  s-irwxu s-irgrp or s-ixgrp or s-iroth or s-ixoth or  ;; mode
  (fopen)
  ;; check success
  dup 0< if drop false exit endif
  true (save-call-pp-chain)
  dup (save)
  ;; ( fd successflag )
  swap (fclose) 0=
  logand
  false (save-call-pp-chain)
;
