;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; currently, debug info is very simple, it contains only PC->LINE mapping
;;
;; header:
;;   dd itemcount  ; this may be used as "extended" later, so bit 31 is resv
;;
;; items:
;;   dd pc
;;   dd line
;;
;; itemcount bit 31 should always be 0
;;
;; the compiler doesn't store each PC, it only stores line changes
;; that is, the range for the line lasts until the next item
;; items should be sorted by PC (but the code should not fail on
;; unsorted data)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "(DBGINFO-ENABLED?)" 1
(hidden)

$value "(DBG-BUF-PTR)" 0
(hidden)
$value "(DBG-BUF-SIZE)" 1024*64
(hidden)

$value "(DBGINFO-ACTIVE?)" 0
(hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DEBUG-INFO-ON
  true to (dbginfo-enabled?)
;

: DEBUG-INFO-OFF
  false to (dbginfo-enabled?)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-RESET)  ( -- )
  (dbg-buf-size) if (dbg-buf-ptr) 0! endif
  false to (dbginfo-active?)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-GET-ADDR-SIZE)  ( -- addr bytes true // false )
  (dbginfo-active?) (dbginfo-enabled?) logand ifnot false exit endif
  (dbg-buf-size) ifnot false exit endif
  (dbg-buf-ptr) @ ?dup ifnot false exit endif
  dup -1 = if drop false exit endif
  ;; convert to bytes
  2 cells u* cell+
  ;; address
  (dbg-buf-ptr)
  swap 1
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-DUMP-AT)  ( addr -- )
  ?dup ifnot exit endif
  dup @ ifnot drop exit endif
  ;; print counter
  >r endcr ." debug info containts "
  r@ @ . ." items\n"
  ;; end
  r@ @ 2 cells u* cell+ r@ +
  ;; start
  r@ cell-
  do
    i .hex8
    ." : pc=0x"
    i @ .hex8
    ."  at "
    i cell+ @ .
    cr
    2 cells
  +loop
  rdrop
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-DUMP)  ( -- )
  (dbg-buf-size) ifnot
    endcr ." debug info disabled\n"
    2drop exit
  endif
  (dbg-buf-ptr) @ ?dup ifnot
    endcr ." no debug info\n"
    exit
  endif
  -1 = if
    endcr ." debug info overflowed\n"
    exit
  endif
  (dbg-buf-ptr) (dbginfo-dump-at)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-ADD-PC)  ( pc line -- )
  ;; never put zero line
  ?dup ifnot drop exit endif
  (dbg-buf-size) ifnot 2drop exit endif
  ;; just inited?
  (dbg-buf-ptr) @ ?dup ifnot
    ;; yes, store the item
    ;; assume that we always have room for at least one
    (dbg-buf-ptr) 1!
    swap (dbg-buf-ptr) cell+ !
    (dbg-buf-ptr) 2 +cells !
    exit
  endif
  ;; special? -1 items means "out of room"
  dup -1 = if drop 2drop exit endif
  ;; calculate address of the last item
  1- 2 cells u* cell+ (dbg-buf-ptr) +
  ;; check if the line is the same
  ;; TODO: check for sorted PC here?
  2dup cell+ @ = if
    ;; no need to store this item
    drop 2drop exit
  endif
  ;; advance address
  4 +cells
  ;; check if we have enough room
  dup (dbg-buf-size) (dbg-buf-ptr) + u> if
    ;; out of buffer, abort debug info generation
    drop 2drop -1 (dbg-buf-ptr) ! exit
  endif
  2 -cells
  ;; ok, we have enough room, store new item
  (*
    UF rpush base @ rpush
    UF 2dup
    urprint "adding pc 0x"
    UF swap dothex8
    urprint " at line "
    UF dot cr
    UF rpop base ! rpop
  *)
  rot over !  cell+ !
  ;; and increment the counter
  1 (dbg-buf-ptr) +!
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (DBGINFO-ADD-HERE)  ( -- )
  ;; put debug info
  (dbginfo-active?) (dbginfo-enabled?) logand if here tib-line# @ (dbginfo-add-pc) endif
;
