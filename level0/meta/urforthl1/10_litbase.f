;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: NOOP  ( -- )
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: LIT  ( -- n )
(arg_lit)
(hidden)
  push  TOS
  lodsd
  ld    TOS,eax
  urnext
endcode


code: LITSTR
(arg_c4strz)
(hidden)
  push  TOS
  lodsd
  push  EIP
  mov   TOS,eax
  add   EIP,eax
  inc   EIP  ;; skip trailing zero
  urnext
endcode

code: LITCFA
(arg_cfa)
(hidden)
  push  TOS
  lodsd
  mov   TOS,eax
  urnext
endcode

code: LITCBLOCK
(arg_cblock)
(hidden)
  ; next cell is continue address
  ; leave next next cell address as cfa
  push  TOS
  lodsd
  ld    TOS,EIP
  ld    EIP,eax
  urnext
endcode

; used in "TO"
code: LITTO!
(arg_cfa)
(hidden)
;; ( value -- )
  lodsd
  add   eax,5   ; skip cfa
  ld    [eax],TOS
  pop   TOS
  urnext
endcode

; used in "+TO"
code: LIT+TO!
(arg_cfa)
(hidden)
;; ( value -- )
  lodsd
  add   eax,5   ; skip cfa
  add   [eax],TOS
  pop   TOS
  urnext
endcode

; used in "-TO"
code: LIT-TO!
(arg_cfa)
(hidden)
;; ( value -- )
  lodsd
  add   eax,5   ; skip cfa
  sub   [eax],TOS
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: BRANCH
(arg_branch)
(hidden)
  lodsd
  mov   EIP,eax
  urnext
endcode

code: 0BRANCH
(arg_branch)
(hidden)
  lodsd
  or    TOS,TOS
  pop   TOS
  jr    nz,@f
  mov   EIP,eax
@@:
  urnext
endcode

code: TBRANCH
(arg_branch)
(hidden)
  lodsd
  or    TOS,TOS
  pop   TOS
  jr    z,@f
  mov   EIP,eax
@@:
  urnext
endcode

;; used in "CASE": drops additional value if branch is NOT taken
code: 0BRANCH-DROP
(arg_branch)
(hidden)
  lodsd
  or    TOS,TOS
  pop   TOS
  jr    nz,@f
  mov   EIP,eax
  urnext
@@:
  ; branch not taken, drop one more data value
  pop   TOS
  urnext
endcode

;; if two values on the stack are equal, drop them, and take a branch
;; if they aren't equal, do nothing
code: ?DO-BRANCH
(arg_branch)
(hidden)
  lodsd
  cp    TOS,[esp]
  jr    nz,@f
  ;; values are equal, drop them, and take a branch
  pop   TOS
  pop   TOS
  mov   EIP,eax
@@:
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: EXECUTE
  mov   eax,TOS
  pop   TOS
  jp    eax
endcode

code: OVERRIDE-EXECUTE
;; ( ... xtoken -- ... )
  mov   eax,TOS
  pop   TOS
  pushr EIP
  ld    EIP,eax
  urnext
endcode

code: (EXECUTE-INTR-CMPL)
(hidden)
;; ( intrcfa cmplcfa -- )
  pop   eax
  ;; TOS: cmplcfa
  ;; EDX: intrcfa
  cp    dword [pfa "state"],0
  cmovnz eax,TOS
  pop   TOS
  jp    eax
endcode


code: EXIT
(noreturn)
  popr  EIP
  urnext
endcode

code: 0?EXIT
;; ( flag -- )
  or    TOS,TOS
  pop   TOS
  jr    nz,@f
  popr  EIP
@@:
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (DO)
(hidden)
;; ( limit start -- | limit counter )
;; loops from start to limit-1
  ; ANS loops
  pop   eax
  ld    edx,0x80000000
  sub   edx,eax
  add   TOS,edx
  sub   ERP,4+4
  ld    [ERP+4],edx  ; 80000000h-to
  ld    [ERP],TOS    ; 80000000h-to+from
  pop   TOS
  urnext
endcode

code: (+LOOP)  ;; +)
(hidden)
;; ( delta -- | limit counter )
fword_par_ploop:
  ; ANS loops
  add   TOS,[ERP]
  jr    o,.done
  ; next iteration
  ld    [ERP],TOS
  lodsd
  mov   EIP,eax
  pop   TOS
  urnext
.done:
  add   esi,4
  add   ERP,4+4
  pop   TOS
  urnext
endcode

code: (LOOP)
(hidden)
;; ( -- | limit counter )
  push  TOS
  ld    TOS,1
  jr    fword_par_ploop
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; as i moved high-level compiler out of the core, we should do it there
;;alias "BREAK" cfa "LEAVE"
;;immediate

code: UNLOOP
;; k8
;; ( | limit counter -- )
;; removes loop arguments from return stack
;; can be used as: UNLOOP EXIT
;; "BREAK" compiles this word before branching out of the loop
  add   ERP,4+4
  urnext
endcode

code: I
;; ( -- counter )
  push  TOS
  ld    TOS,[ERP]
  sub   TOS,[ERP+4]
  urnext
endcode

code: J
;; ( -- counter )
  push  TOS
  ld    TOS,[ERP+8]
  sub   TOS,[ERP+8+4]
  urnext
endcode
