;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$variable "(EXC-FRAME-PTR)" 0
(hidden)


: CATCH  ( i * x xt -- j * x 0 | i * x n )
  ;; this is using return stack to hold previous catch frame
  ;; of course, this prevents Very Smart return stack manipulation, but idc (for now)
  ;; exception frame consists of:
  ;;   return-to-catch EIP (return stack TOS)
  ;;   sp (frame points here)
  ;;   prev_frame_ptr
  ;;   return-to-catch-caller EIP
  ;; create exception frame
  (exc-frame-ptr) @ >r
  sp@ >r
    ;; sp@ cellinc endcr dothex8 cr dotstack
  rp@ (exc-frame-ptr) !  ;; update exception frame pointer
  execute  ;; and execute
  ;; we will return here only if no exception was thrown
  rdrop    ;; drop spdepth
  r> (exc-frame-ptr) !   ;; restore previous exception frame
  0        ;; exception code (none)
;


: THROW  ( k * x n -- k * x | i * x n )
  ?dup if
      ;; sp@ endcr dothex8 cr dotstack
    ;; check if we have exception frame set
    (exc-frame-ptr) @ ?dup
    ifnot
      ;; panic!
      (exc-frame-ptr) 0!
      state 0!  ;; just in case
      err-throw-without-catch (error)
      1 n-bye ;; just in case
    endif
    ;; check if return stack is not exhausted
    rp@ cell- over u> if
      ;; panic!
      (exc-frame-ptr) 0!
      state 0!  ;; just in case
      err-throw-chain-corrupted (error)
      1 n-bye ;; just in case
    endif
    ;; restore return stack
    rp!
    ;; exchange return stack top and data stack top (save exception code, and pop sp to data stack)
    r> swap >r
    ;; blindly restore data stack (let's hope it is not too badly trashed)
    sp! drop  ;; drop the thing that was CFA
      ;; sp@ endcr dothex8 space r@ dothex8 cr
    ;; restore exception code
    r>
    ;; restore previous exception frame
    r> (exc-frame-ptr) !
    ;; now EXIT will return to CATCH caller
  endif
;
