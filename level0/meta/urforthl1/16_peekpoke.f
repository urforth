;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: @  ( addr -- [addr] ) ;; -- 4 bytes
  mov   TOS,[TOS]
  urnext
endcode

code: W@  ( addr -- [addr] ) ;; -- 2 bytes
  movzx TOS,word [TOS]
  urnext
endcode

code: C@  ( addr -- [addr] ) ;; -- 1 byte
  movzx TOS,byte [TOS]
  urnext
endcode

code: !  ( value addr -- [addr]=value ) ;; -- 4 bytes
  pop   eax   ; value
  mov   [TOS],eax
  pop   TOS
  urnext
endcode

code: W!  ( value addr -- [addr]=value ) ;; -- 2 bytes
  pop   eax   ; value
  mov   word [TOS],ax
  pop   TOS
  urnext
endcode

code: C!  ( value addr -- [addr]=value ) ;; -- 1 byte
  pop   eax   ; value
  mov   byte [TOS],al
  pop   TOS
  urnext
endcode


;; sane little-endian
code: 2@LE  ( addr -- [addr] [addr+4] ) ;; --8 bytes
  mov   eax,[TOS]
  push  eax
  mov   TOS,[TOS+4]
  urnext
endcode

;; sane little-endian
code: 2!LE  ( n0 n1 addr -- [addr]=n0 [addr+4]=n1 ) ;; --8 bytes
  pop   eax   ; d-high
  mov   [TOS+4],eax
  pop   eax
  mov   [TOS],eax
  pop   TOS
  urnext
endcode

;; ANS big-endian
code: 2@  ( addr -- [addr+4] [addr] ) ;; --8 bytes
  mov   eax,[TOS+4]
  push  eax
  mov   TOS,[TOS]
  urnext
endcode

;; ANS big-endian
code: 2!  ( n0 n1 addr -- [addr]=n1 [addr+4]=n0 ) ;; --8 bytes
  pop   eax   ; d-high
  mov   [TOS],eax
  pop   eax
  mov   [TOS+4],eax
  pop   TOS
  urnext
endcode


code: 0!  ( addr -- [addr]=0 ) ;; --4 bytes
  mov   dword [TOS],0
  pop   TOS
  urnext
endcode

code: 0W!  ( addr -- [addr]=0 ) ;; --2 bytes
  mov   word [TOS],0
  pop   TOS
  urnext
endcode

code: 0C!  ( addr -- [addr]=0 ) ;; --1 byte
  mov   byte [TOS],0
  pop   TOS
  urnext
endcode


code: 1!  ( addr -- [addr]=1 ) ;; --4 bytes
  mov   dword [TOS],1
  pop   TOS
  urnext
endcode

code: 1W!  ( addr -- [addr]=1 ) ;; --2 bytes
  mov   word [TOS],1
  pop   TOS
  urnext
endcode

code: 1C!  ( addr -- [addr]=1 ) ;; --1 byte
  mov   byte [TOS],1
  pop   TOS
  urnext
endcode


code: +!  ( value addr -- [addr]=value ) ;; --4 bytes
  pop   eax   ; value
  add   [TOS],eax
  pop   TOS
  urnext
endcode

code: +W!  ( value addr -- [addr]=value ) ;; --2 bytes
  pop   eax   ; value
  add   [TOS],ax
  pop   TOS
  urnext
endcode

code: +C!  ( value addr -- [addr]=value ) ;; --1 byte
  pop   eax   ; value
  add   [TOS],al
  pop   TOS
  urnext
endcode


code: -!  ( value addr -- [addr]=value ) ;; --4 bytes
  pop   eax   ; value
  sub   [TOS],eax
  pop   TOS
  urnext
endcode

code: -W!  ( value addr -- [addr]=value ) ;; --2 bytes
  pop   eax   ; value
  sub   [TOS],ax
  pop   TOS
  urnext
endcode

code: -C!  ( value addr -- [addr]=value ) ;; --1 byte
  pop   eax   ; value
  sub   [TOS],al
  pop   TOS
  urnext
endcode


code: 1+!  ( addr -- [addr]=[addr]+1 ) ;; --4 bytes
  inc   dword [TOS]
  pop   TOS
  urnext
endcode

code: 1+W!  ( addr -- [addr]=[addr]+1 ) ;; --2 bytes
  inc   word [TOS]
  pop   TOS
  urnext
endcode

code: 1+C!  ( addr -- [addr]=[addr]+1 ) ;; --1 byte
  inc   byte [TOS]
  pop   TOS
  urnext
endcode


code: 1-!  ( addr -- [addr]=[addr]-1 ) ;; --4 bytes
  dec   dword [TOS]
  pop   TOS
  urnext
endcode

code: 1-W!  ( addr -- [addr]=[addr]-1 ) ;; --2 bytes
  dec   word [TOS]
  pop   TOS
  urnext
endcode

code: 1-C!  ( addr -- [addr]=[addr]-1 ) ;; --1 byte
  dec   byte [TOS]
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$alias "+W!" "W+!"
$alias "+C!" "C+!"
$alias "-W!" "W-!"
$alias "-C!" "C-!"

$alias "1+W!" "1W+!"
$alias "1+C!" "1C+!"
$alias "1-W!" "1W-!"
$alias "1-C!" "1C-!"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: OR!  ( value addr -- [addr]|=value ) ;; --4 bytes
  pop   eax
  or    dword [TOS],eax
  pop   TOS
  urnext
endcode

code: XOR!  ( value addr -- [addr]^=value ) ;; --4 bytes
  pop   eax
  xor   dword [TOS],eax
  pop   TOS
  urnext
endcode

code: AND!  ( value addr -- [addr]^=value ) ;; --4 bytes
  pop   eax
  and   dword [TOS],eax
  pop   TOS
  urnext
endcode

code: ~AND!  ( value addr -- [addr]^=~value ) ;; --4 bytes
  pop   eax
  xor   eax,0xffffffff
  and   dword [TOS],eax
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; byte [addr] ^= value
code: CTOGGLE  ( addr value -- )
  pop   eax
  xor   [eax],cl
  pop   TOS
  urnext
endcode

;; word [addr] ^= value
code: WTOGGLE  ( addr value -- )
  pop   eax
  xor   [eax],cx
  pop   TOS
  urnext
endcode

;; dword [addr] ^= value
code: TOGGLE ; ( addr value -- )
  pop   eax
  xor   [eax],TOS
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: CMOVE  ( source dest count -- )
  pop   eax     ;; dest
  pop   FREEREG ;; src
  test  TOS,0x80000000
  jr    nz,.done
  or    TOS,TOS
  jr    z,.done
  push  esi     ;; this is used as EIP
  mov   esi,FREEREG
  mov   edi,eax
  ;; TOS is ECX, and this is our length too
  rep movsb
  pop   esi
.done:
  pop   TOS
  urnext
endcode

;; can be used to make some room
;; moves from the last byte to the first one
code: CMOVE>  ( source dest count -- )
  pop   eax     ;; dest
  pop   FREEREG ;; src
  test  TOS,0x80000000
  jr    nz,.done
  or    TOS,TOS
  jr    z,.done
  push  esi     ;; this is used as EIP
  mov   esi,FREEREG
  mov   edi,eax
  ;; TOS is ECX, and this is our length too
  ;; move pointers
  dec   ecx
  add   esi,ecx
  add   edi,ecx
  inc   ecx
  std
  rep movsb
  cld
  pop   esi
.done:
  pop   TOS
  urnext
endcode

;; uses CMOVE or CMOVE>
: MOVE  ( from to len -- )
  >r 2dup =
  if
    2drop rdrop
  else
    2dup u>
    if
      ;; from > to: use normal CMOVE
      r> cmove
    else
      ;; from <= to: use reverse CMOVE>
      r> cmove>
    endif
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: FILL ;; ( addr count byte -- )
  cp    dword [esp],1
  jr    l,@f
  mov   eax,TOS
  pop   ecx
  pop   edi
  rep stosb
  pop   TOS
  urnext
@@:
  add   esp,4+4
  pop   TOS
  urnext
endcode

: ERASE  ( addr count -- )
  0 fill
;

: BLANK  ( addr count -- )
  bl fill
;
