;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (URFORTH-SEGFAULT-CODEBLOCK)
(hidden)
(codeblock)

urfsegfault_output_fd: dd 2

urfsegfault_dump_ddepth: dd 16  ; default depth of the data stack dump-1
urfsegfault_dump_rdepth: dd 24  ; default depth of the data stack dump-1

urfsegfault_dbg_retaddr: dd 0
urfsegfault_reg_eax: dd 0
urfsegfault_reg_ebx: dd 0
urfsegfault_reg_ecx: dd 0
urfsegfault_reg_edx: dd 0
urfsegfault_reg_esi: dd 0
urfsegfault_reg_edi: dd 0
urfsegfault_reg_ebp: dd 0
urfsegfault_reg_esp: dd 0
urfsegfault_reg_flags: dd 0

; set in startup code
urfsegfault_stack_bottom: dd 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print char from AL
;; all registers are preserved, including flags
;;
urfsegfault_emit_al:
  save_all_regs
  push  eax       ; we will write from here
  ld    eax,4     ; write
  ld    ebx,[urfsegfault_output_fd]
  ld    ecx,esp   ; address
  ld    edx,1     ; length
  syscall
  pop   eax
  restore_all_regs
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print string from ECX, length in EDX
;; all registers are preserved, including flags
;;
urfsegfault_emit_str_ecx_edx:
  save_all_regs
  ld    eax,4     ; write
  ld    ebx,[urfsegfault_output_fd]
  syscall
  restore_all_regs
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print string from ECX, length in EDX
;; all registers are preserved, including flags
;;
urfsegfault_emit_str_ecx_edx_safe:
  save_all_regs
.prloop:
  or    edx,edx
  jr    z,.done
  ld    al,[ecx]
  cp    al,32
  jr    nc,@f
  ld    al,'?'
@@:
  call  urfsegfault_emit_al
  inc   ecx
  dec   edx
  jr    .prloop
.done:
  restore_all_regs
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print AL as hex
;; all registers are preserved, including flags
;;
urfsegfault_emit_hex_al:
  pushfd
  push  eax
  push  eax
  shr   al,4
  Nibble2Hex
  call  urfsegfault_emit_al
  pop   eax
  Nibble2Hex
  call  urfsegfault_emit_al
  pop   eax
  popfd
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print AX as hex
;; all registers are preserved, including flags
;;
urfsegfault_emit_hex_ax:
  pushfd
  push  eax
  push  eax
  shr   eax,8
  call  urfsegfault_emit_hex_al
  pop   eax
  call  urfsegfault_emit_hex_al
  pop   eax
  popfd
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print EAX as hex
;; all registers are preserved, including flags
;;
urfsegfault_emit_hex_eax:
  pushfd
  push  eax
  push  eax
  shr   eax,16
  call  urfsegfault_emit_hex_ax
  pop   eax
  call  urfsegfault_emit_hex_ax
  pop   eax
  popfd
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print EAX as signed decimal
;; all registers are preserved, including flags
;; i could do this without divs, but meh
;;
urfsegfault_emit_dec_eax:
  save_all_regs
  test  eax,0x80000000
  jr    z,@f
  urfsegfault_emit '-'
  neg   eax
@@:
  call  .prloop
  restore_all_regs
  ret
.prloop:
  ld    ecx,10
  xor   edx,edx
  div   ecx
  ; EAX: quotient; EDX: remainder
  or    eax,eax
  jr    z,@f
  ; recurse
  push  eax
  push  edx
  call  .prloop
  pop   edx
  pop   eax
@@:
  xchg  eax,edx
  ; EDX: quotient; EAX: remainder
  add   al,'0'
  call  urfsegfault_emit_al
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; move to cfa from nfa
;;
;; IN:
;;   EAX: nfa
;; OUT:
;;   EAX: cfa
;; other registers are preserved (except flags)
;;
urfsegfault_nfa2cfa:
  push  ecx
  movzx ecx,byte [eax]
  add   ecx,4+1  ; lenflags, trailing length
  add   eax,ecx
  pop   ecx
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; move to nfa from cfa
;;
;; IN:
;;   EAX: cfa
;; OUT:
;;   EAX: nfa
;; other registers are preserved (except flags)
;;
urfsegfault_cfa2nfa:
  push  ecx
  movzx ecx,byte [eax-1]
  add   ecx,4+1  ; lenflags, trailing length
  sub   eax,ecx
  pop   ecx
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; type word name
;; correctly handles the case when EAX == 0
;;
;; IN:
;;   EAX: nfa
;; OUT:
;; all registers are preserved (except flags)
;;
urfsegfault_nfaprint:
  push  eax
  push  ecx
  push  edx
  or    eax,eax
  jr    z,.noword
  ; load length to edx
  movzx edx,byte [eax]
  or    edx,edx
  jr    z,.nonamed
  add   eax,4   ; skip length and flags
  ld    ecx,eax ; starting address
  call  urfsegfault_emit_str_ecx_edx_safe
.nonamed:
  pop   edx
  pop   ecx
  pop   eax
  ret
.noword:
  urfsegfault_printstr "<???>"
  jr    .nonamed


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; skip word argument
;; correctly handles the case when EAX == 0
;;
;; IN:
;;   EAX: nfa
;;   ESI: code pointer
;; OUT:
;;   EAX: dead
;;   ESI: (new) code pointer
;; all registers are preserved (except flags)
;;
urfsegfault_nfa_skip_arg:
  or    eax,eax
  jr    nz,.cont
  ret
.cont:
  ; load arg type
  movzx eax,byte [eax+1]

urfsegfault_nfa_skip_arg_type_in_eax:
  ; branch?
  cp    al,WARG_BRANCH
  jr    nz,@f
  add   esi,4
  jr    .done
@@:
  ; literal?
  cp    al,WARG_LIT
  jr    nz,@f
  add   esi,4
  jr    .done
@@:
  ; cell-counted string?
  cp    al,WARG_C4STRZ
  jr    nz,@f
  lodsd
  add   esi,eax
  inc   esi     ; skip trailing zero byte
  jr    .done
@@:
  ; cfa?
  cp    al,WARG_CFA
  jr    nz,@f
  add   esi,4
@@:
  ; cblock?
  cp    al,WARG_CBLOCK
  jr    nz,@f
  lodsd
  ld    esi,eax
@@:
.done:
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; type word argument
;; correctly handles the case when EAX == 0
;;
;; IN:
;;   EAX: nfa
;;   ESI: code pointer
;; OUT:
;;   ESI: (new) code pointer
;; all registers are preserved (except flags)
;;
urfsegfault_nfa_print_arg:
  push  eax
  push  ecx
  push  edx
  or    eax,eax
  jr    z,.done
  ; load arg type
  ld    cl,byte [eax+1]
  jecxz .done
  ; branch?
  cp    cl,WARG_BRANCH
  jr    nz,@f
  urfsegfault_emit ' '
  lodsd
  call  urfsegfault_emit_hex_eax
.done:
  pop   edx
  pop   ecx
  pop   eax
  ret
@@:
  ; literal?
  cp    cl,WARG_LIT
  jr    nz,@f
  urfsegfault_emit ' '
  lodsd
  call  urfsegfault_emit_dec_eax
  jr    .done
@@:
  ; cell-counted string?
  cp    cl,WARG_C4STRZ
  jr    nz,@f
  urfsegfault_printstr ' "'
  lodsd
  ld    edx,eax
  ld    ecx,esi
  add   esi,eax
  inc   esi   ;; skip trailing zero byte
  call  urfsegfault_emit_str_ecx_edx_safe
  urfsegfault_emit '"'
  jr    .done
@@:
  ; cfa?
  cp    cl,WARG_CFA
  jr    nz,@f
  urfsegfault_emit ' '
  lodsd
  ;call  urfsegfault_find_by_addr
  call  urfsegfault_cfa2nfa
  call  urfsegfault_nfaprint
  jr    .done
@@:
  ; cblock?
  cp    cl,WARG_CBLOCK
  jr    nz,@f
  urfsegfault_printstr " {cblock}"
  lodsd
  ld    esi,eax
  jp    .done
@@:
  jp    .done


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find line for the PC
;;
;; IN:
;;   EAX: pc
;;   ESI: nfa
;; OUT:
;;   EAX: line
;; other registers are preserved (except flags)
;;
urfsegfault_find_pc_line_nfa:
  push  esi
  push  ecx
  push  edx
  or    esi,esi
  jr    z,.fail
  ; load debug info address
  ld    esi,[esi-20]
  or    esi,esi
  jr    z,.fail
  ; load and check number of items
  ld    ecx,[esi]
  add   esi,4
  ; check number of items (just in case)
  or    ecx,ecx
  jr    z,.fail
  test  ecx,0x80000000
  jr    nz,.fail
  ; latest line we've seen will be in EDX
  xor   edx,edx
.scanloop:
  cmp   eax,[esi]
  jr    c,@f
  ld    edx,[esi+4]
@@:
  add   esi,8
  loop  .scanloop
  ld    eax,edx
  jr    .done
.fail:
  xor   eax,eax
.done:
  pop   edx
  pop   ecx
  pop   esi
  ret


; the following two will be set (ONLY!) by successfull call to `urfsegfault_find_by_addr`
urfsegfault_fba_nfa: dd 0
urfsegfault_fba_end: dd 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find which word owns the specified address
;;
;; IN:
;;   EAX: address
;;   ESI: vocptr (at latestptr)
;; OUT:
;;   EAX: nfa or 0
;; ZERO SET if EAX is 0
;; other registers are preserved (except flags)
;;
urfsegfault_find_by_addr_in_voc:
  push  esi
  push  edi
  ; ESI: lfa
.findvoc_loop:
  ; follow lfa
  ld    esi,[esi]
  or    esi,esi
  jr    z,.not_found
  ; load sfa
  ld    edi,[esi-8]
  ; check for invalid sfa
  cp    esi,edi
  jr    nc,.findvoc_loop
  ; EAX>=ESI?
  cp    eax,esi
  jr    c,.findvoc_loop
  ; EAX<EDI?
  cp    eax,edi
  jr    nc,.findvoc_loop
  ; i found her!
  ; move ESI to nfa
  add   esi,8
  ld    [urfsegfault_fba_nfa],esi
  ld    [urfsegfault_fba_end],edx
.not_found:
  ld    eax,esi
  or    eax,eax   ; fix zero flag
  pop   edi
  pop   esi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find which word owns the specified address
;; this assumes that all words are sequential in memory
;;
;; IN:
;;   EAX: address
;; OUT:
;;   EAX: nfa or 0
;; ZERO SET if EAX is 0
;; other registers are preserved (except flags)
;;
urfsegfault_find_by_addr:
  push  esi
  push  ecx
  ld    ecx,eax
  ld    esi,[pfa "(voc-link)"]
.vocloop:
  or    esi,esi
  jr    z,.quit
  sub   esi,4         ; move to latestptr
  ld    eax,ecx
  call  urfsegfault_find_by_addr_in_voc
  ld    esi,[esi+4]   ; load next voclink
  jr    z,.vocloop
  pop   ecx
  pop   esi
  ret
.quit:
  xor   eax,eax
  pop   ecx
  pop   esi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print return stack
;;
urfsegfault_cmd_printrstack:
  ; print first 8 items, that should be enough
  ; print return stack depth
  urfsegfault_printstr "=== RETURN STACK: "
  ld    eax,[pfa "rp0"]
  sub   eax,[urfsegfault_reg_ebp]
  jr    nc,@f
  ; underflow
  urfsegfault_printstrnl " UNDERFLOWED ==="
@@:
  sar   eax,2   ; divide by cell
  push  eax
  urfsegfault_printdec eax
  urfsegfault_printstrnl " CELLS DEPTH ==="
  ; print current word
  urfsegfault_printstr "**"
  ld    eax,[urfsegfault_reg_esi]
  sub   eax,4   ; because we just loaded it
  call  urfsegfault_find_by_addr
  push  eax
  call  urfsegfault_nfaprint
  ; print line number if we know it
  pop   esi
  ld    eax,[urfsegfault_reg_esi]
  sub   eax,4   ; because we just loaded it
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "  {"
  call  urfsegfault_emit_dec_eax
  urfsegfault_printstr "}"
@@:
  urfsegfault_cr
  ;
  pop   ecx
  ld    esi,[urfsegfault_reg_ebp]
  or    ecx,ecx
  jr    nz,@f
  ret
@@:
  cp    ecx,[urfsegfault_dump_rdepth]
  jr    c,.printloop
  ld    ecx,[urfsegfault_dump_rdepth]

.printloop:
  ld    eax,[esi]
  urfsegfault_printstr "  "
  call  urfsegfault_find_by_addr
  push  eax   ; for line number
  call  urfsegfault_nfaprint

  ; print hex value
  ld    eax,[esi]
  urfsegfault_printstr "  | "
  call  urfsegfault_emit_hex_eax
  ; print decimal value
  ld    eax,[esi]
  urfsegfault_printstr " | "
  call  urfsegfault_emit_dec_eax

  ; print line number if we know it
  xchg  esi,[esp]
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "  {"
  call  urfsegfault_emit_dec_eax
  urfsegfault_printstr "}"
@@:
  pop   esi

  urfsegfault_cr
  add   esi,4
  dec   ecx
  jp    nz,.printloop
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print data stack
;;
urfsegfault_cmd_printdstack:
  ; print first 8 items, that should be enough
  ; also, print them in backwards order (i.e. TOS will be printed last)
  ; print data stack depth
  urfsegfault_printstr "=== DATA STACK: "
  ld    eax,[pfa "sp0"]
  sub   eax,[urfsegfault_reg_esp]
  jp    nc,@f
  ; underflow
  urfsegfault_printstrnl " UNDERFLOWED ==="
  ret
@@:
  sar   eax,2   ; divide by cell
  push  eax
  urfsegfault_printdec eax
  urfsegfault_printstrnl " CELLS DEPTH ==="
  pop   ecx
  or    ecx,ecx
  jp    z,urfsegfault_cmd_printdstack_quit0

  urfsegfault_printstr "ESP="
  ld    eax,[urfsegfault_reg_esp]
  urfsegfault_printhex eax
  urfsegfault_cr

  cp    ecx,1
  jp    z,urfsegfault_cmd_printdstack_tos
  cp    ecx,[urfsegfault_dump_ddepth]
  jr    c,@f
  ld    ecx,[urfsegfault_dump_ddepth]
@@:
  ; now print cells
  dec   ecx   ; TOS
  ld    esi,[urfsegfault_reg_esp]
  shl   ecx,2
  add   esi,ecx
  shr   ecx,2
@@:
  or    ecx,ecx
  jr    z,@f
  sub   esi,4
  ld    eax,[esi]
  urfsegfault_printstr "  "
  urfsegfault_printhex esi
  urfsegfault_printstr ":  "
  urfsegfault_printhex eax
  urfsegfault_printstr " | "
  urfsegfault_printdec eax
  urfsegfault_cr
  dec   ecx
  jp    @b
@@:
urfsegfault_cmd_printdstack_tos:
  ; print TOS
  urfsegfault_printstr "  "
  urfsegfault_printstr "  TOS   "
  urfsegfault_printstr ":  "
  urfsegfault_printhex [urfsegfault_reg_ecx]
  urfsegfault_printstr " | "
  urfsegfault_printdec [urfsegfault_reg_ecx]
  urfsegfault_cr

urfsegfault_cmd_printdstack_quit0:
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; debugger segfault handler
;;
urfd_sigact:
urfd_sigact.sa_handler:  ;dd 0  ; union with .sa_sigaction
urfd_sigact.sa_sigaction: dd urfsegfault_segfault_action
urfd_sigact.sa_mask:      dd 0 ;0xffffffff
urfd_sigact.sa_flags:     dd 4  ; SA_SIGINFO
urfd_sigact.sa_restorer:  dd 0

;; struc siginfo {
;;   .si_signo   dword 1
;;   .si_errno   dword 1
;;   .si_code    dword 1
;;   ; for segfaults
;;   ;._addr      dword 1
;;   ._pad       dword 29
;;   if 0
;;   ._kill:     ;kill
;;   ._timer:    ;timer
;;   .__rt:      ;_rt
;;   ._sigchld:  ;sigchld
;;   ._sigfault: ;sigfault
;;   ._sigpoll:  ;sigpoll
;;   ._pad       dword 29
;;   end if
;; }

sigcontext_t.gs            equ  0  ; word 1
sigcontext_t.__gsh         equ  2  ; word 1
sigcontext_t.fs            equ  4  ; word 1
sigcontext_t.__fsh         equ  6  ; word 1
sigcontext_t.es            equ  8  ; word 1
sigcontext_t.__esh         equ 10  ; word 1
sigcontext_t.ds            equ 12  ; word 1
sigcontext_t.__dsh         equ 14  ; word 1
sigcontext_t.edi           equ 16  ; dword 1
sigcontext_t.esi           equ 20  ; dword 1
sigcontext_t.ebp           equ 24  ; dword 1
sigcontext_t.esp           equ 28  ; dword 1
sigcontext_t.ebx           equ 32  ; dword 1
sigcontext_t.edx           equ 36  ; dword 1
sigcontext_t.ecx           equ 40  ; dword 1
sigcontext_t.eax           equ 44  ; dword 1
sigcontext_t.trapno        equ 48  ; dword 1
sigcontext_t.err           equ 52  ; dword 1
sigcontext_t.eip           equ 56  ; dword 1
sigcontext_t.cs            equ 60  ; word 1
sigcontext_t.__csh         equ 62  ; word 1
sigcontext_t.eflags        equ 64  ; dword 1
sigcontext_t.esp_at_signal equ 68  ; dword 1
sigcontext_t.ss            equ 72  ; word 1
sigcontext_t.__ssh         equ 74  ; word 1
sigcontext_t.fpstate       equ 78  ; dword 1
sigcontext_t.oldmask       equ 82  ; dword 1
sigcontext_t.cr2           equ 86  ; dword 1
sigcontext_t._size_        equ 88

sigaltstack_t.ss_sp    equ  0  ; dword 1
sigaltstack_t.ss_flags equ  4  ; dword 1
sigaltstack_t.ss_size  equ  8  ; dword 1
sigaltstack_t._size_   equ 12

sigset_t.sig    equ 0  ; dword 2
sigset_t._size_ equ 8

ucontext_t.uc_flags    equ 0  ; dword 1
ucontext_t.uc_link     equ 4  ; dword 1
ucontext_t.uc_stack    equ 8
ucontext_t.uc_mcontext equ ucontext_t.uc_stack+sigaltstack_t._size_
ucontext_t.uc_sigmask  equ ucontext_t.uc_mcontext+sigcontext_t._size_
ucontext_t._size_      equ ucontext_t.uc_sigmask+sigset_t._size_


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urfsegfault_segfault_action:
  ld    ebp,esp
  ; switch to our own stack
  ;ld    eax,[pfa "dp"]
  ;add   eax,1024*128
  ld    eax,[urfsegfault_stack_bottom]
  ld    esp,eax

  ; signum     : dword [ebp+4]
  ; siginfoptr : dword [ebp+8]
  ; ucontextptr: dword [ebp+12]

  ; copy registers from context to debugger data structures
  ld    esi,[ebp+12]
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  ld    [urfsegfault_dbg_retaddr],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eax]
  ld    [urfsegfault_reg_eax],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ebx]
  ld    [urfsegfault_reg_ebx],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ecx]
  ld    [urfsegfault_reg_ecx],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.edx]
  ld    [urfsegfault_reg_edx],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esi]
  ld    [urfsegfault_reg_esi],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.edi]
  ld    [urfsegfault_reg_edi],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ebp]
  ld    [urfsegfault_reg_ebp],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esp]
  ld    [urfsegfault_reg_esp],eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eflags]
  ld    [urfsegfault_reg_flags],eax


  urfsegfault_printstr "***TRAP: SEGFAULT!"


  ld    esi,[ebp+12]

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  urfsegfault_printstr "  EIP="
  urfsegfault_printhex eax

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esp]
  urfsegfault_printstr " ESP="
  urfsegfault_printhex eax

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.ebp]
  urfsegfault_printstr " EBP="
  urfsegfault_printhex eax

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.esi]
  urfsegfault_printstr "  ESI="
  urfsegfault_printhex eax

  urfsegfault_cr

  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  call  urfsegfault_find_by_addr
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "*** CURRENT WORD: "
  call  urfsegfault_nfaprint
  ; print line number if we know it
  ld    esi,eax
  ld    eax,[esi+ucontext_t.uc_mcontext+sigcontext_t.eip]
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "  {"
  call  urfsegfault_emit_dec_eax
  urfsegfault_printstr "}"
@@:
  urfsegfault_cr

  call  urfsegfault_cmd_printdstack
  call  urfsegfault_cmd_printrstack

  ld    eax,1
  ld    ebx,1
  syscall


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; called by the startup code
;;
urforth_setup_segfault_handler:
  save_all_regs
  ; set segfault hanlder
  ld    eax,67
  ld    ebx,11   ; SIGSEGV
  ;ld    ebx,2   ; SIGTERM
  ld    ecx,urfd_sigact
  xor   edx,edx  ; ignore old info
  syscall
  or    eax,eax
  jr    z,@f
  urfsegfault_printstrnl "TRAP: cannot setup segfault handler."
@@:
  restore_all_regs
  ret
endcode
