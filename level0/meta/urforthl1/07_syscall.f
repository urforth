;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: BYE  ( -- )
(noreturn)
  ;;call  fttylow_do_flush
  mov   eax,1
  xor   ebx,ebx
  syscall
endcode

code: N-BYE  ( exitcode -- )
(noreturn)
  ;;call  fttylow_do_flush
  mov   eax,1
  mov   ebx,TOS
  syscall
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (SYSCALL-0)  ( num -- res )
  ld    eax,TOS
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-1)  ( arg0 num -- res )
  ld    eax,TOS
  pop   ebx
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-2)  ( arg0 arg1 num -- res )
  ld    eax,TOS
  pop   ecx
  pop   ebx
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-3)  ( arg0 arg1 arg2 num -- res )
  ld    eax,TOS
  pop   edx
  pop   ecx
  pop   ebx
  push  EIP   ;; just in case
  syscall
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (SYSCALL-4)  ( arg0 arg1 arg2 arg3 num -- res )
  ;; this cannot be called recursively anyway
  ld    [fword_syscall4_eip_store],EIP
  ld    eax,TOS
  pop   esi
  pop   edx
  pop   ecx
  pop   ebx
  syscall
  ld    EIP,[fword_syscall4_eip_store]
  ld    TOS,eax
  urnext
fword_syscall4_eip_store: dd 0
endcode

code: (SYSCALL-5)  ( arg0 arg1 arg2 arg3 arg4 num -- res )
  ;; this cannot be called recursively anyway
  ld    [fword_syscall5_eip_store],EIP
  ld    eax,TOS
  pop   edi
  pop   esi
  pop   edx
  pop   ecx
  pop   ebx
  syscall
  ld    EIP,[fword_syscall5_eip_store]
  ld    TOS,eax
  urnext
fword_syscall5_eip_store: dd 0
endcode


code: (BRK)
;; ( newaddr -- newaddr )  returns current address if the request is invalid
  push  EIP     ;; just in case
  ld    eax,45  ;; brk
  ld    ebx,TOS ;; new address
  syscall
  ld    TOS,eax
  pop   EIP
  urnext
endcode

: (BRK-HERE)  ( -- curraddr )
  0 (brk)
;

;; throws OOM error
: BRK-ALLOC  ( size -- addr )
  dup 0< err-out-of-memory ?error
  (brk-here)  ;; ( size addr )
  swap ?dup if
    ;; ( addr size )
    over + dup (brk)
    u< err-out-of-memory ?error
  endif
;
