;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; Forth debugger
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$if URFORTH_DEBUG

code: (URFORTH-DEBUGGER-CODEBLOCK)
(hidden)
(codeblock)

urfdebug_active_flag: dd 0
urfdebug_input_fd: dd 0     ;; default input is stdin

urfdebug_dump_ddepth: dd 7     ;; default depth of the data stack dump-1
urfdebug_dump_rdepth: dd 7     ;; default depth of the data stack dump-1

urfdebug_bp_rdepth:    dd 0xffffffff  ;; otherwise activate when return stack depth reaches this
urfdebug_sobp_address: dd 0xffffffff  ;; breakpoint address (at which we should restore dword)
;urfdebug_sobp_value:   dd 0           ;; breakpoint address (at which we should restore dword)

;TODO
URFORTH_DEBUG_MAX_BPS equ 16


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urfdebug_tib: rb 258
urfdebug_tib_size equ $-urfdebug_tib


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; external API
urfdebug_activate_it:
  ld    dword [urfdebug_active_flag],1
  ld    dword [urforth_next_ptr],urfdebug_next
  ret

urfdebug_deactivate_it:
  ld    dword [urfdebug_active_flag],0
  ld    dword [urforth_next_ptr],urforth_next_normal
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print asciiz string from EAX
;; IN:
;;   EAX: start address
;; OUT:
;;   EAX: right after terminating zero
;; all other registers are preserved (excluding flags)
;;
urfdebug_emit_str_asciiz_eax:
  push  esi
  mov   esi,eax
@@:
  lodsb
  or    al,al
  jr    z,@f
  call  urfsegfault_emit_al
  jr    @b
@@:
  mov   eax,esi
  pop   esi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; read char to AL
;; all registers are preserved, excluding EAX
;; on EOF, EAX is 0, and zero flag is set
;; on success, EAX is !0, and zero flag is reset
;;
urfdebug_getch:
  save_all_regs_no_eax_f
  xor   eax,eax
  push  eax       ;; we will read char here
  mov   eax,3     ;; read
  mov   ebx,[urfdebug_input_fd]
  mov   ecx,esp   ;; address
  mov   edx,1     ;; length
  syscall
  ;; check number of read chars
  or    eax,eax
  ;; get read char
  pop   eax
  jr    z,urfdebug_getch_eof
  and   eax,0xff  ;; safety, and zero check
  jr    nz,urfdebug_getch_ok
  inc   al
  or    al,al     ;; reset zero flag
urfdebug_getch_ok:
  restore_all_regs_no_eax_f
  ret
urfdebug_getch_eof:
  xor   eax,eax   ;; also sets zero flag
  jr    urfdebug_getch_ok


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; read input line into urfdebug_tib
;;
;; OUT:
;;   ECX: line length, without CR
;;   zero flag: set on EOF (never set on non-empty line)
;;   other flags are dead
;;   other registers are preserved
;;   always terminates input string with 0
urfdebug_getline:
  push  edi
  push  eax
  mov   edi,urfdebug_tib
  mov   ecx,urfdebug_tib_size-1
urfdebug_getline_loop:
  ;; EDI: destination
  ;; ECX: bytes left in destination (excluding room for terminating 0)
  call  urfdebug_getch
  jr    z,urfdebug_getline_eof
  cmp   al,13
  jr    z,urfdebug_getline_done
  cmp   al,10
  jr    z,urfdebug_getline_done
  stosb
  loop  urfdebug_getline_loop
  ;; either EOL, or end of input buffer
  ;; we don't care
urfdebug_getline_done:
  ;; store terminating 0
  xor   al,al
  stosb
  ;; calculate line length
  mov   eax,urfdebug_tib_size-1
  sub   eax,ecx
  mov   ecx,eax
  ;; reset zero flag
  mov   al,1
  or    al,al
urfdebug_getline_exit:
  ;; exit
  pop   eax
  pop   edi
  ret
urfdebug_getline_eof:
  ;; eof: check for empty line
  cmp   edi,urfdebug_tib
  jr    nz,urfdebug_getline_done
  ;; empty line and eof: real eof
  ;; store terminating zero
  xor   al,al
  stosb
  ;; return 0 length, and zero flag set
  xor   ecx,ecx
  jr    urfdebug_getline_exit


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; skip blanks
;;
;; IN:
;;   ESI: input buffer start
;; OUT:
;;   ESI: pointer to non-blank or zero char
;;   EAX: non-blank char
;;   ZERO SET if ESI points to zero char (i.e. on EOL)
;; all other registers are preserved, except flags
;;
urfdebug_skip_blanks:
  lodsb
  or    al,al
  jr    z,urfdebug_skip_blanks_stop
  cmp   al,32+1
  jr    c,urfdebug_skip_blanks
urfdebug_skip_blanks_stop:
  dec   esi
  and   eax,0xff
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; convert AL to digit in hex
;; sets carry on error (and AL is undefined)
;; all other registers are preserved, except flags
;;
urfdebug_digit_al:
  sub   al,'0'
  ret   c
  cp    al,10
  ccf
  ret   nc
  sub   al,7
  ret   c
  cp    al,16
  ccf
  ret   nc
  sub   al,32
  ret   c
  cp    al,16
  ccf
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; parse number
;;
;; IN:
;;   ESI: input buffer start (blanks must be skipped!)
;; OUT:
;;   ESI: pointer to non-blank or zero char
;;   EAX: parsed number
;;   CARRY SET if not a number (in this case ESI is not modified, and EAX is garbage)
;; all other registers are preserved, except flags
;;
urfdebug_parse_number:
  push  esi   ;; we may need to restore it
  xor   eax,eax
  push  eax   ;; negate flag
  push  eax   ;; number base
  push  eax   ;; number accumulator
  ;; locals:
  ;;   [esp]   -- accumulator
  ;;   [esp+4] -- base
  ;;   [esp+8] -- negate flag
  ld    al,[esi]
  or    al,al
  jp    z,urfdebug_parse_number_error

  ;; check for negative/positive
  cp    al,'-'
  jr    nz,@f
  ld    dword [esp+8],1
  inc   esi
  jr    urfdebug_parse_number_check_prefixes
@@:
  cp    al,'+'
  jr    nz,@f
  inc   esi
@@:

urfdebug_parse_number_check_prefixes:
  ;; default base
  ld    dword [esp+4],10
  ;; check for possible sigils
  ld    al,[esi]
  cp    al,'$'
  jr    nz,@f
  ld    dword [esp+4],16
  inc   esi
@@:
  cp    al,'#'
  jr    nz,@f
  ld    dword [esp+4],16
  inc   esi
@@:
  cp    al,'%'
  jr    nz,@f
  ld    dword [esp+4],2
  inc   esi
@@:
  ;; check for possible C-like prefix
  cp    al,'0'
  jp    nz,urfdebug_parse_number_no_c_prefix
  cp    byte [esi+1],'x'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],16
  jp    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'X'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],16
  jr    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'b'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],2
  jr    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'B'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],2
  jr    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'o'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],8
  jr    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'O'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],8
  jr    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'d'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],10
  jr    urfdebug_parse_number_no_c_prefix
@@:
  cp    byte [esi+1],'D'
  jr    nz,@f
  add   esi,2
  ld    dword [esp+4],10
  jr    urfdebug_parse_number_no_c_prefix
@@:

urfdebug_parse_number_no_c_prefix:
  ;; first must be a digit
  ld    al,[esi]
  call  urfdebug_digit_al
  jr    c,urfdebug_parse_number_error
  cp    al,[esp+4]
  jr    nc,urfdebug_parse_number_error

urfdebug_parse_number_loop:
  lodsb
  cp    al,32+1
  jr    c,urfdebug_parse_number_complete
  cp    al,'_'
  jr    z,urfdebug_parse_number_loop
  call  urfdebug_digit_al
  jr    c,urfdebug_parse_number_error
  cp    al,byte [esp+4]
  jr    nc,urfdebug_parse_number_error
  push  edx
  push  ecx
  push  eax
  xor   edx,edx
  xor   ecx,ecx
  ld    eax,[esp+4+4+4]
  ld    cl,[esp+4+4+4+4]
  mul   ecx
  ld    edx,eax
  pop   eax
  and   eax,0xff
  add   edx,eax
  ld    [esp+4+4],edx
  pop   ecx
  pop   edx
  jr    urfdebug_parse_number_loop

urfdebug_parse_number_complete:
  dec   esi       ;; back to the last non-number char
  call  urfdebug_skip_blanks
  ld    eax,[esp]
  cp    dword [esp+8],0
  jr    z,@f
  neg   eax
@@:
  add   esp,4*4
  or    eax,eax   ;; reset carry
  ret

urfdebug_parse_number_error:
  add   esp,4*3
  pop   esi
  scf
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find command
;;
;; IN:
;;   ESI: input buffer start
;; OUT:
;;  found:
;;   ESI: next input buffer token (blanks skipped)
;;   EAX: command handler address
;;   ZERO RESET
;;  not found:
;;   ESI: unchanged
;;   EAX: 0
;;   ZERO SET
;; all registers are preserved, except flags
;;
urfdebug_find_command:
  push  esi       ;; in case we'll need to restore it
  call  urfdebug_skip_blanks
  jr    z,urfdebug_find_command_eol
  ;; save two reginster we will use as working set
  push  edi
  push  ecx
  ;; load table address
  mov   edi,urfdebug_command_table
urfdebug_find_command_loop:
  ;; ESI: input buffer
  ;; EDI: commant table
  ;; EAX,ECX: scratchpad
  mov   eax,[edi] ;; handler address
  or    eax,eax
  jr    z,urfdebug_find_command_not_found
  ;; EAX contains handler address
  ;; save it for now
  push  eax
  ;; compare name
  add   edi,4     ;; skip handler address
  ;; save esi, because we will need to restore it on failure
  push  esi
  ;; compare until different, or until table name terminates
urfdebug_find_command_cmpname_loop:
  mov   al,[esi]  ;; input byte
  ;; if space, convert to 0, so we can compare it with name terminator
  cmp   al,32+1
  jr    nc,urfdebug_find_command_cmpname_loop_good_al
  mov   al,0
urfdebug_find_command_cmpname_loop_good_al:
  cmp   al,[edi]  ;; compare with name byte
  jr    nz,urfdebug_find_command_cmpname_loop_failed
  ;; the bytes are the same, advance pointers
  inc   esi
  inc   edi
  ;; check for end-of-word
  or    al,al
  jr    nz,urfdebug_find_command_cmpname_loop
  ;; command name matches
  ;; ESI: after the command text, and the last space/0
  ;; EDI: after the command name in the table, and the trailing 0
  ;; on stack: original ESI, handler address
  dec   esi       ;; backup one char in case it was 0
  call  urfdebug_skip_blanks
  pop   eax       ;; drop original ESI, we don't need it anymore
  pop   eax       ;; this is our return result
  ;; restore trashed registers
  pop   ecx
  pop   edi
  add   esp,4   ;; ignore saved oritinal ESI
  ret

urfdebug_find_command_cmpname_loop_failed:
  ;; ESI: in the command text
  ;; EDI: in the name text
  ;; on stack: original ESI, handler address
  pop   esi     ;; restore original ESI
  pop   eax     ;; we don't need it, so this is just "drop"
  ;; skip the rest of the name
urfdebug_find_command_cmpname_loop_failed_skip:
  mov   al,[edi]
  inc   edi
  or    al,al
  jr    nz,urfdebug_find_command_cmpname_loop_failed_skip
  ;; skip help text
@@:
  mov   al,[edi]
  inc   edi
  or    al,al
  jr    nz,@b
  jr    urfdebug_find_command_loop

urfdebug_find_command_not_found:
  pop   ecx
  pop   edi
urfdebug_find_command_eol:
  xor   eax,eax
  pop   esi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$if URFORTH_EXTRA_STACK_CHECKS
;; flags and EAX are already saved; all others should be preserved
urfdebug_stack_check:
  ld    eax,[pfa "sp0"]
  sub   eax,4*3
  cmp   esp,eax
  jr    be,.ok
  ;;jr    b,.ok
  ld    dword [urfdebug_active_flag],1
.ok:
  ret
$endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main debugger entry point
;; all registers are preserved, including flags
;;
urfdebug_next:
  ;; save eax and flags, check for early exit
  pushfd
  cmp   dword [urfdebug_active_flag],0xffffffff
  jr    z,@f
  push  eax
  $if URFORTH_EXTRA_STACK_CHECKS
  call  urfdebug_stack_check
  $endif
  cmp   dword [urfdebug_active_flag],0
  jr    nz,urfdebug_active
  ;; special flag, means "no breakpoint checks"
  ;; check for breakpoints
  ld    eax,[urfdebug_sobp_address]
  add   eax,4     ;; compensate for loaded dword
  cp    eax,EIP
  jr    z,urfdebug_active
  ;; check for rdepth breakpoint
  cp    ERP,[urfdebug_bp_rdepth]
  jr    z,urfdebug_active
  ;; no breakpoints, get out
  pop   eax
@@:
  popfd
  ;;ret
  jmp   eax

  ;; debugger is active here
urfdebug_active:
  ;; save other registers
  pop   eax   ;; original eax
  mov   [urfsegfault_reg_eax],eax
  pop   eax   ;; flags
  mov   [urfsegfault_reg_flags],eax
  mov   [urfsegfault_reg_ebx],ebx
  mov   [urfsegfault_reg_ebx],ebx
  mov   [urfsegfault_reg_ecx],ecx
  mov   [urfsegfault_reg_edx],edx
  mov   [urfsegfault_reg_esi],esi
  mov   [urfsegfault_reg_edi],edi
  mov   [urfsegfault_reg_ebp],ebp
  $if 0
    ;; get return address
    pop   eax
    ;; and save it too
    mov   [urfsegfault_dbg_retaddr],eax
  $endif
  ;; now save stack pointer
  mov   [urfsegfault_reg_esp],esp
  ;; just in case, ensure that the direction flag is clear (autoincrement)
  cld

  ;; switch to our own stack
  ld    eax,[urfsegfault_stack_bottom]
  ld    esp,eax

  ;; activate debugger
  ;;ld    dword [urfdebug_active_flag],1
  call  urfdebug_activate_it
  ;; reset rdepth breakpoint
  ld    dword [urfdebug_bp_rdepth],0xffffffff
  ;; reset step-over breakpoint
  ld    dword [urfdebug_sobp_address],0xffffffff

  ;; main debugger loop
urfdebug_loop:
  ;; print debugger prompt, data stack depth, return stack depth
  ld    eax,[urfsegfault_reg_esp]
  call  urfsegfault_emit_hex_eax
  urfsegfault_emit '|'
  urfsegfault_printstr "UDBG:D("

  ;; print data stack depth
  mov   eax,[pfa "sp0"]
  sub   eax,[urfsegfault_reg_esp]
  sar   eax,2   ;; divide by cell
  urfsegfault_printdec eax

  ;; print return stack depth
  urfsegfault_printstr "):R("
  mov   eax,[pfa "rp0"]
  sub   eax,[urfsegfault_reg_ebp]
  sar   eax,2   ;; divide by cell
  urfsegfault_printdec eax

  ;; print the word we're currently executing
  urfsegfault_printstr "):"
  mov   eax,[urfsegfault_reg_esi]
  sub   eax,4   ;; because we just loaded it
  call  urfsegfault_find_by_addr
  ;; print line number if we know it
  push  eax
  ld    esi,eax
  ld    eax,[urfsegfault_reg_esi]
  sub   eax,4   ;; because we just loaded it
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  urfsegfault_printstr "{"
  call  urfsegfault_emit_dec_eax
  urfsegfault_printstr "}"
@@:
  pop   eax
  ;; done printing line
  call  urfsegfault_nfaprint

  ;; print the word we'll jump into
  urfsegfault_printstr " -> "
  mov   eax,[urfsegfault_reg_eax]
  call  urfsegfault_find_by_addr
  push  eax
  call  urfsegfault_nfaprint
  pop   eax
  ld    esi,[urfsegfault_reg_esi]
  call  urfsegfault_nfa_print_arg

  ;; print current EIP
  urfsegfault_printstr " | "
  mov   eax,[urfsegfault_reg_esi]
  sub   eax,4   ;; because we just loaded it
  call  urfsegfault_emit_hex_eax

  ;; print next EIP
  urfsegfault_printstr " -> "
  mov   eax,[urfsegfault_reg_eax]
  call  urfsegfault_emit_hex_eax

  ;; print the rest of the prompt
  urfsegfault_printstr " |>"

  ;; read user command
  call  urfdebug_getline
  ;; ECX is length; ZERO SET on EOF
  ;; deactivate on EOF
  jr    z,urfdebug_deactivate
  ;; continue on empty command
  or    ecx,ecx
  jr    z,urfdebug_exit

  mov   esi,urfdebug_tib
  call  urfdebug_find_command
  jr    z,urfdebug_unknown_command

  ;;call  uefdebug_call_eax
  call  eax
  jp    urfdebug_loop

urfdebug_unknown_command:
  ;; for now, simply echo the command
  mov   esi,urfdebug_tib
urfdebug_echo_loop:
  lodsb
  or    al,al
  jr    z,urfdebug_echo_loop_done
  call  urfsegfault_emit_al
  jr    urfdebug_echo_loop
urfdebug_echo_loop_done:
  ;;urfsegfault_emit '|'
  ;;urfsegfault_cr
  urfsegfault_printstrnl "? what?"
  jp    urfdebug_loop

urfdebug_deactivate:
  urfsegfault_cr
urfdebug_deactivate_nocr:
  ;;mov   dword [urfdebug_active_flag],0
  call  urfdebug_deactivate_it
  ;; and exit
urfdebug_exit:
  ;; restore registers
  mov   ebx,[urfsegfault_reg_ebx]
  mov   ecx,[urfsegfault_reg_ecx]
  mov   edx,[urfsegfault_reg_edx]
  mov   esi,[urfsegfault_reg_esi]
  mov   edi,[urfsegfault_reg_edi]
  mov   ebp,[urfsegfault_reg_ebp]
  ;; restore flags
  mov   eax,[urfsegfault_reg_flags]
  push  eax
  popfd
  ;; restore stack
  mov   esp,[urfsegfault_reg_esp]
  $if 0
    ;; push return address
    mov   eax,[urfsegfault_dbg_retaddr]
    push  eax
  $endif
  ;; restore eax, and exit
  mov   eax,[urfsegfault_reg_eax]
  ;;ret
  jmp   eax


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; command handlers
;; handler will be called with ESI pointing to unparsed arguments (blanks skipped)
;;
urfdebug_command_table:
  dd  urfdebug_cmd_printdstack
    db  ".",0
    db  "dump data stack",0
  dd  urfdebug_cmd_printrstack
    db  ".r",0
    db  "dump return stack",0
  dd  urfdebug_cmd_step_out
    db  "sup",0
    db  "continue until current word returned",0
  dd  urfdebug_cmd_step_over
    db  "s",0
    db  "step over the current word",0
  dd  urfdebug_cmd_deactivate
    db  "c",0
    db  "deactivate debugger",0

  dd  urfdebug_cmd_drop
    db  "drop",0
    db  "drop number from data stack",0
  dd  urfdebug_cmd_swap
    db  "swap",0
    db  "swap numbers at data stack",0
  dd  urfdebug_cmd_push
    db  "push",0
    db  "push number at data stack",0

  dd  urfdebug_cmd_ddepth
    db  "depth",0
    db  "set data stack dump depth",0
  dd  urfdebug_cmd_rdepth
    db  "rdepth",0
    db  "set return stack dump depth",0

  dd  urfdebug_cmd_see
    db  "see",0
    db  "decompile forth word",0

  dd  urfdebug_cmd_fcall
    db  "fcall",0
    db  "call forth word",0

  dd  urfdebug_cmd_help
    db  "h",0
    db  "show this help",0
  dd  0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; drop value from data stack
;;
urfdebug_cmd_drop:
  ld    eax,[pfa "sp0"]
  sub   eax,[urfsegfault_reg_esp]
  jp    nc,@f
  ;; underflow
  urfsegfault_printstrnl "stack underflowed"
  ret
@@:
  ret   z
  ;; drop value
  ld    edi,[urfsegfault_reg_esp]
  ;; set new TOS
  ld    eax,[edi]
  ld    [urfsegfault_reg_ecx],eax
  add   edi,4
  ld    [urfsegfault_reg_esp],edi
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; swap values on data stack
;;
urfdebug_cmd_swap:
  ld    eax,[pfa "sp0"]
  sub   eax,[urfsegfault_reg_esp]
  jp    nc,@f
  ;; underflow
  urfsegfault_printstrnl "stack underflowed"
  ret
@@:
  cp    al,2
  jr    nc,@f
  urfsegfault_printstrnl "stack should have at least two values"
  ret
@@:
  ;; swap values
  ld    edi,[urfsegfault_reg_esp]
  ld    eax,[edi]
  ld    ecx,[urfsegfault_reg_ecx]
  ld    [edi],ecx
  ld    [urfsegfault_reg_ecx],eax
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; push value on data stack
;;
urfdebug_cmd_push:
  ld    eax,[pfa "sp0"]
  sub   eax,[urfsegfault_reg_esp]
  jp    nc,@f
  ;; underflow
  urfsegfault_printstrnl "stack underflowed"
  ret
@@:
urfdebug_cmd_push_parse_loop:
  call  urfdebug_skip_blanks
  ret   z
  call  urfdebug_parse_number
  jr    nc,@f
  urfsegfault_printstrnl "not a number!"
  ret
@@:
  ld    edi,[urfsegfault_reg_esp]
  ld    ecx,[urfsegfault_reg_ecx]
  sub   edi,4
  ld    [edi],ecx
  ld    [urfsegfault_reg_ecx],eax
  ld    [urfsegfault_reg_esp],edi
  jr    urfdebug_cmd_push_parse_loop


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; show command help
;;
urfdebug_cmd_help:
  urfsegfault_printstrnl "------ HELP ------"
  ld    esi,urfdebug_command_table
urfdebug_cmd_help_loop:
  lodsd
  or    eax,eax
  ret   z
  ld    eax,esi
  urfsegfault_printstr "  "
  call  urfdebug_emit_str_asciiz_eax
  urfsegfault_emit 9
  call  urfdebug_emit_str_asciiz_eax
  urfsegfault_cr
  ld    esi,eax
  jr    urfdebug_cmd_help_loop


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; deactivate until current word exited
;;
urfdebug_cmd_step_out:
  ;; check return stack depth
  ld    eax,[pfa "rp0"]
  cmp   eax,[urfsegfault_reg_ebp]
  jr    z,urfdebug_cmd_step_out_cannot
  jr    c,urfdebug_cmd_step_out_cannot
  ld    eax,[urfsegfault_reg_ebp]
  add   eax,4     ;; one level up
  ld    [urfdebug_bp_rdepth],eax
  ;; done, deactivate debugger
  ;;jp    urfdebug_deactivate_nocr
  ld    dword [urfdebug_active_flag],0
  jp    urfdebug_exit
urfdebug_cmd_step_out_cannot:
  urfsegfault_printstrnl "cannot step out, return stack is empty!"
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; perform "step over"
;;
urfdebug_cmd_step_over:
  ;; sanity check, just in case
  cp    dword [urfdebug_sobp_address],0xffffffff
  jr    nz,urfdebug_cmd_step_over_error
  ;; check if the next address is still in the current word
  ;; that next address is actually in EIP
  mov   eax,[urfsegfault_reg_esi]
  call  urfsegfault_find_by_addr
  jp    z,urfdebug_cmd_step_over_bad_addr
  mov   ecx,eax
  mov   eax,[urfsegfault_reg_esi]
  sub   eax,4   ;; current instruction
  call  urfsegfault_find_by_addr
  jp    z,urfdebug_cmd_step_over_bad_addr
  cp    eax,ecx
  jp    nz,urfdebug_cmd_step_over_bad_addr
  ;; get flags
  mov   eax,[urfsegfault_reg_eax]
  call  urfsegfault_find_by_addr
  jp    z,urfdebug_cmd_step_over_bad_addr
  ld    edi,eax
  movzx eax,word [edi+2]
  test  eax,FLAG_NORETURN
  jp    nz,urfdebug_cmd_step_over_noreturn
  ;; load argument type
  movzx eax,byte [edi+1]
  ld    esi,[urfsegfault_reg_esi]
  ;; check instruction argument type
  cp    al,WARG_BRANCH
  jr    z,urfdebug_cmd_step_over_branch
  ;; skip argument
  call  urfsegfault_nfa_skip_arg_type_in_eax
urfdebug_cmd_step_over_set_sobp:
  ;; setup debugger activation address
  ld    dword [urfdebug_sobp_address],esi
  ;; done, deactivate debugger
  ;;jp    urfdebug_deactivate_nocr
  ld    dword [urfdebug_active_flag],0
  jp    urfdebug_exit
urfdebug_cmd_step_over_error:
  urfsegfault_printstrnl "step-over breakpoint already set for some strange reason!"
  ret
urfdebug_cmd_step_over_bad_addr:
  ;;urfsegfault_printstrnl "cannot step-over outside of the word!"
  ;;ret
  ;; just continue into it
  jp    urfdebug_exit
urfdebug_cmd_step_over_branch:
  ;;urfsegfault_printstrnl "cannot step over branches yet!"
  ;;ret
  ;; just continue into it
  jp    urfdebug_exit
urfdebug_cmd_step_over_noreturn:
  ;;urfsegfault_printstrnl "cannot step over NORETURN words!"
  ;;ret
  ;; just continue into it
  jp    urfdebug_exit


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print return stack
;;
urfdebug_cmd_printrstack:
  push  dword [urfsegfault_dump_rdepth]
  ld    eax,[urfdebug_dump_rdepth]
  ld    [urfsegfault_dump_rdepth],eax
  call  urfsegfault_cmd_printrstack
  pop   dword [urfsegfault_dump_rdepth]
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print data stack
;;
urfdebug_cmd_printdstack:
  push  dword [urfsegfault_dump_ddepth]
  ld    eax,[urfdebug_dump_ddepth]
  ld    [urfsegfault_dump_ddepth],eax
  call  urfsegfault_cmd_printdstack
  pop   dword [urfsegfault_dump_ddepth]
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; deactivate debugger
;;
urfdebug_cmd_deactivate:
  jp    urfdebug_deactivate_nocr


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; set data stack dump depth
;;
urfdebug_cmd_ddepth:
  call  urfdebug_parse_number
  jr    nc,@f
  urfsegfault_printstrnl "number expected!"
  ret
@@:
  cp    eax,2
  jr    nc,@f
  urfsegfault_printstrnl "minimum value is 2!"
  ret
@@:
  cp    eax,201
  jr    c,@f
  urfsegfault_printstrnl "minimum value is 200!"
  ret
@@:
  dec   eax
  ld    [urfdebug_dump_ddepth],eax
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; set return stack dump depth
;;
urfdebug_cmd_rdepth:
  call  urfdebug_parse_number
  jr    nc,@f
  urfsegfault_printstrnl "number expected!"
  ret
@@:
  cp    eax,2
  jr    nc,@f
  urfsegfault_printstrnl "minimum value is 2!"
  ret
@@:
  cp    eax,201
  jr    c,@f
  urfsegfault_printstrnl "minimum value is 200!"
  ret
@@:
  dec   eax
  ld    [urfdebug_dump_rdepth],eax
  ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; decompile forth word
;;
urfdebug_cmd_see:
  ;; find argument length
  ld    edi,esi
urfdebug_cmd_see_arglen_loop:
  lodsb
  cp    al,33
  jr    nc,urfdebug_cmd_see_arglen_loop
  dec   esi
  ld    ecx,esi
  sub   ecx,edi
  jr    nz,@f
  urfsegfault_printstrnl "what word?"
  ret
@@:
  ;; save starting address and length
  push  edi
  push  ecx
  ;; call (WFIND-STR)
  push  edi   ;; addr
  mov   ebp,[urfsegfault_reg_ebp]
  ld    eax,cfa "(wfind-str)"
  call  ur_mc_fcall
  or    TOS,TOS
  jr    nz,@f
  pop   edx
  pop   ecx
  call  urfsegfault_emit_str_ecx_edx_safe
  urfsegfault_printstrnl "? what is it?"
  ret
@@:
  pop   ebx       ;; word cfa
  pop   edx       ;; saved count
  pop   ecx       ;; saved address
  call  urfsegfault_emit_str_ecx_edx_safe
  urfsegfault_printstr " is "
  ld    eax,ebx
  call  urfsegfault_find_by_addr
  or    eax,eax
  jr    nz,@f
  urfsegfault_printstrnl "some shit"
  ret
@@:
  ;;ld    esi,eax   ;; save cfa
  ;;call  urfsegfault_cfa2nfa
  ld    [urfdebug_cmd_see_nfa],eax  ;; we'll need it for debug info printing
  call  urfsegfault_nfaprint
  urfsegfault_printstr " at (nfa) 0x"
  call  urfsegfault_emit_hex_eax
  urfsegfault_cr

  ;; now decompile it
  ld    eax,[urfsegfault_fba_nfa]
  call  urfsegfault_nfa2cfa
  mov   esi,eax
  lodsb
  cp    al,0xe8   ;; call?
  jp    nz,urfdebug_cmd_see_notforth
  lodsd
  ld    ecx,esi
  add   ecx,eax
  cp    ecx,cfa "(URFORTH-DOFORTH-CODEBLOCK)"
  jp    nz,urfdebug_cmd_see_notforth
urfdebug_cmd_see_loop:
  cmp   esi,[urfsegfault_fba_end]
  jr    nc,urfdebug_cmd_see_loop_done
  ;; print address (and store it on the stack)
  ld    eax,esi
  push  eax
  call  urfsegfault_emit_hex_eax
  urfsegfault_printstr ": "
  lodsd           ;; cfa
  call  urfsegfault_cfa2nfa
  call  urfsegfault_nfaprint
  ;; print arguments
  call  urfsegfault_nfa_print_arg
  ;; print source code line
  pop   eax
  push  esi
  ld    esi,[urfdebug_cmd_see_nfa]
  call  urfsegfault_find_pc_line_nfa
  or    eax,eax
  jr    z,@f
  push  eax
  urfsegfault_printstr 9,"; line #"
  pop   eax
  call  urfsegfault_emit_dec_eax
@@:
  pop   esi
  ;; done
  urfsegfault_cr
  jr    urfdebug_cmd_see_loop

urfdebug_cmd_see_loop_done:
  urfsegfault_cr
  ret

urfdebug_cmd_see_notforth:
  urfsegfault_printstrnl "cannot decompile non-Forth words yet."
  ret

urfdebug_cmd_see_nfa: dd 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; call forth words
;;
urfdebug_cmd_fcall:
  ;; find argument length
  call  urfdebug_skip_blanks
  ld    edi,esi
@@:
  lodsb
  cp    al,33
  jr    nc,@b
  dec   esi
  ld    ecx,esi
  sub   ecx,edi
  jr    nz,@f
  ;;urfsegfault_printstrnl "what word?"
  ret
@@:
  ;; save starting address and length
  push  edi
  push  ecx
  ;; call (WFIND-STR)
  push  edi   ;; addr
  mov   ebp,[urfsegfault_reg_ebp]
  ld    eax,cfa "(wfind-str)"
  call  ur_mc_fcall
  or    TOS,TOS
  jr    nz,@f
  ;; try to parse it as a number
  pop   ecx
  pop   esi
  push  esi
  push  ecx
  call  urfdebug_parse_number
  jr    c,.nan
  ;; number, push it
  ld    edi,[urfsegfault_reg_esp]
  ld    ecx,[urfsegfault_reg_ecx]
  sub   edi,4
  ld    [edi],ecx
  ld    [urfsegfault_reg_ecx],eax
  ld    [urfsegfault_reg_esp],edi
  pop   ecx
  pop   edi
  ;; esi is ok here
  jr    urfdebug_cmd_fcall
.nan:
  pop   edx
  pop   ecx
  call  urfsegfault_emit_str_ecx_edx_safe
  urfsegfault_printstrnl "? what is it?"
  ret
@@:
  pop   eax       ;; word cfa
  ;; switch data stacks
  ld    [urfdebug_cmd_fcall_esp],esp
  ld    esp,[urfsegfault_reg_esp]
  ld    TOS,[urfsegfault_reg_ecx]
  call  ur_mc_fcall
  ld    [urfsegfault_reg_ecx],TOS
  ld    [urfsegfault_reg_esp],esp
  ld    esp,[urfdebug_cmd_fcall_esp]
  ;; skip word and continue
  pop   ecx
  pop   esi
  add   esi,ecx
  jp    urfdebug_cmd_fcall

urfdebug_cmd_fcall_esp: dd 0

endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DBG  ( ??? )
  call  urfdebug_activate_it
  urnext
endcode

$constant "(HAS-DEBUGGER?)" 1
$constant "(NEXT-REF-ADDR)" urforth_next_ptr

$else
$constant "(HAS-DEBUGGER?)" 0
;; so i can avoid conditional compilation
;; it MUST be 0
$constant "(NEXT-REF-ADDR)" 0

code: DBG  ( ??? )
  urnext
endcode
$endif
