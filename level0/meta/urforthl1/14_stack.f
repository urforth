;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: SP@  ( -- sp )
  push  TOS
  mov   TOS,esp
  urnext
endcode

code: RP@  ( -- rp )
  push  TOS
  mov   TOS,ERP
  urnext
endcode

code: SP!  ( n -- )
  mov   esp,TOS
  pop   TOS
  urnext
endcode

code: RP!  ( n -- )
  mov   ERP,TOS
  pop   TOS
  urnext
endcode

code: SP0!  ( -- )
  mov   esp,[pfa "sp0"]
  xor   TOS,TOS
  urnext
endcode

code: RP0!  ( -- )
  mov   ERP,[pfa "rp0"]
  urnext
endcode

code: (SP-CHECK)  ( -- ok-flag )
  mov   eax,esp
  cmp   eax,[pfa "sp0"]
  jr    na,.ok
  mov   esp,[pfa "sp0"]
  xor   TOS,TOS
  push  TOS
  urnext
.ok:
  push  TOS
  mov   TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: RDUP  ( -- | n -- n n )
  peekr eax
  pushr eax
  urnext
endcode

code: RDROP ( -- | n -- )
  dropr
  urnext
endcode

code: >R  ( n -- | -- n )
  pushr TOS
  pop   TOS
  urnext
endcode

code: R>  ( -- n | n -- )
  push  TOS
  popr  TOS
  urnext
endcode

code: R@  ( -- n | n -- n )
  push  TOS
  peekr TOS
  urnext
endcode


code: 2RDROP  ( -- | n0 n1 -- )
  add   ERP,4*2
  urnext
endcode

code: 2>R  ( n0 n1 -- | -- n0 n1 )
  pop   eax   ;; n0
  pushr eax
  pushr TOS
  pop   TOS
  urnext
endcode

code: 2R>  ( -- n0 n1 | n0 n1 -- )
  push  TOS
  popr  TOS
  popr  eax
  push  eax
  urnext
endcode

code: 2R@  ( -- n0 n1 | n0 n1 )
  push  TOS
  push  dword [ERP+4]
  mov   TOS,dword [ERP]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DUP  ( n -- n n )
  push  TOS
  urnext
endcode

code: 2DUP  ( n0 n1 -- n0 n1 n0 n1 )
  mov   eax,[esp]
  ;; TOS: n1
  ;; EAX: n0
  push  TOS
  push  eax
  urnext
endcode

code: ?DUP  ( n0 -- n0 n0  ||  0 -- 0 )
  or    TOS,TOS
  jr    z,@f
  push  TOS
@@:
  urnext
endcode

;; drop if zero
code: ?DROP  ( n0 -- n0  ||  0 -- )
  or    TOS,TOS
  jr    nz,@f
  pop   TOS
@@:
  urnext
endcode

code: DROP  ( n0 -- )
  pop   TOS
  urnext
endcode

code: 2DROP  ( n0 n1 -- )
  pop   TOS
  pop   TOS
  urnext
endcode

code: SWAP  ( n0 n1 -- n1 n0 )
  xchg  [esp],TOS
  urnext
endcode

code: 2SWAP  ( n0 n1 n2 n3 -- n2 n3 n0 n1 )
  ;; TOS=n3
  pop   eax       ;; EAX=n2
  pop   FREEREG   ;; FRG=n1
  xchg  [esp],eax ;; EAX=n0
  push  TOS
  push  eax
  mov   TOS,FREEREG
  urnext
endcode

code: OVER  ( n0 n1 -- n0 n1 n0 )
  push  TOS
  mov   TOS,[esp+4]
  urnext
endcode

code: 2OVER  ( n0 n1 n2 n3 -- n0 n1 n2 n3 n0 n1 )
  ;; TOS=n3
  push  TOS
  mov   eax,[esp+12]
  mov   TOS,[esp+8]
  push  eax
  urnext
endcode

code: ROT  ( n0 n1 n2 -- n1 n2 n0 )
  pop   FREEREG
  pop   eax
  push  FREEREG
  push  TOS
  mov   TOS,eax
  urnext
endcode

code: NROT  ( n0 n1 n2 -- n2 n0 n1 )
  pop   FREEREG
  pop   eax
  push  TOS
  push  eax
  mov   TOS,FREEREG
  urnext
endcode

code: -ROT  ( n0 n1 n2 -- n2 n0 n1 )
  pop   FREEREG
  pop   eax
  push  TOS
  push  eax
  mov   TOS,FREEREG
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SWAP DROP
code: NIP  ( n1 n2 -- n2 )
  pop   eax
  urnext
endcode

;; SWAP OVER
code: TUCK  ( n1 n2 -- n2 n1 n2 )
  pop   eax
  push  TOS
  push  eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DEPTH  ( -- stack-depth-before-this-call )
  push  TOS
  ld    TOS,[pfa "sp0"]
  sub   TOS,esp
  sar   TOS,2
  dec   TOS
  urnext
endcode

code: RDEPTH  ( -- rstack-depth-before-this-call )
  push  TOS
  ld    TOS,[pfa "rp0"]
  sub   TOS,ebp
  sar   TOS,2
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; remove idx, copy item; 0 PICK is the same as DUP
code: PICK  ( ... idx -- ... n[top-idx-1] )
  ld    TOS,[esp+TOS*4]
  urnext
endcode

;; remove idx, move item; 0 ROLL is the same as NOOP
code: ROLL  ( ... idx -- ... n[top-idx-1] )
  jecxz .quit
  ld    FREEREG,esi
  ld    eax,[esp+TOS*4]
  lea   esi,[esp+TOS*4]
  ld    edi,esi
  sub   esi,4
  std
  rep movsd
  cld
  ld    esi,FREEREG
  ld    [esp],eax
.quit:
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; remove idx, copy item from return stack; 0 RPICK is the same as R@
code: RPICK  ( ... idx -- ... n[top-idx-1] )
  ld    TOS,[ebp+TOS*4]
  urnext
endcode
