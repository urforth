;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (DLCLOSE)  ( handle-or-0 -- )
  jecxz .fucked
  ;; push registers
  push  EIP
  push  ERP
  ;; call
  push  TOS
  call  [elfimp_dlclose]
  add   esp,4   ;; remove args
.fucked:
  pop   TOS
  urnext
endcode

code: (DLOPEN-ASCIIZ)  ( addr -- handle-or-0 )
  jecxz .fucked
  ;; save registers
  push  EIP
  push  ERP
  ;; call function
  ;;ld    eax,1+256+8   ;; RTLD_LAZY+RTLD_GLOBAL+RTLD_DEEPBIND
  push  1+256+8   ;; RTLD_LAZY+RTLD_GLOBAL+RTLD_DEEPBIND
  push  TOS
  call  [elfimp_dlopen]
  add   esp,4*2       ;; remove arguments
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
.fucked:
  urnext
endcode

: (DLOPEN)  ( addr count -- handle-or-0 )
  tmp-pool-ensure-asciiz >r
  (DLOPEN-ASCIIZ)
  r> tmp-pool-release
;

code: (DLSYM-ASCIIZ)  ( addr handle -- address-or-0 )
  pop   edi
  or    edi,edi
  jr    z,.fucked
  ;; save registers
  push  EIP
  push  ERP
  ;; call function
  push  edi
  push  TOS
  call  [elfimp_dlsym]
  add   esp,4*2       ;; remove arguments
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
.fucked:
  xor   TOS,TOS
  urnext
endcode

: (DLSYM)  ( addr count handle -- handle-or-0 )
  >r tmp-pool-ensure-asciiz r> swap >r
  (DLSYM-ASCIIZ)
  r> tmp-pool-release
;

$constant "RTLD_DEFAULT" 0
$constant "RTLD_NEXT" -1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (FCLOSE)  ( fd -- flag )
  ;; save registers
  push  EIP
  push  ERP
  ;; syscall
  ld    eax,6
  ld    ebx,TOS
  syscall
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (FOPEN-ASCIIZ)  ( addr flags mode -- fd-or-minusone )
  pop   edx   ;; flags
  pop   edi   ;; addr
  ld    eax,TOS
  ;; EAX: mode
  ;; EDX: flags
  ;; EDI: addr
  or    edi,edi
  jr    z,.fucked
  ;; save registers
  push  EIP
  push  ERP
  ;; syscall
  ld    ebx,edi
  ld    ecx,edx
  ld    edx,eax
  ld    eax,5
  syscall
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
.fucked:
  ld    TOS,-1
  urnext
endcode

: (FOPEN)  ( addr count flags mode -- handle-or-0 )
  2swap tmp-pool-ensure-asciiz >r nrot
  (FOPEN-ASCIIZ)
  r> tmp-pool-release
;

code: (FREAD)  ( addr count fd -- count )
  pop   edx   ;; count
  pop   edi   ;; addr
  ;; save registers
  push  EIP
  push  ERP
  ;; syscall
  ld    eax,3
  ld    ebx,TOS
  ld    ecx,edi
  syscall
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (FWRITE)  ( addr count fd -- count )
  pop   edx   ;; count
  pop   edi   ;; addr
  ;; save registers
  push  EIP
  push  ERP
  ;; syscall
  ld    eax,4
  ld    ebx,TOS
  ld    ecx,edi
  syscall
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
endcode

code: (LSEEK)  ( ofs whence fd -- res )
  ld    ebx,ecx
  pop   edx
  pop   ecx
  ;; save registers
  push  EIP
  push  ERP
  ;; syscall
  ld    eax,19
  ;;ld    ebx,fd
  ;;ld    ecx,offt
  ;;ld    edx,whence
  syscall
  ;; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
endcode


$constant "O-RDONLY" 0
$constant "O-WRONLY" 1
$constant "O-RDWR"   2

$constant "O-CREAT"     0x000040  ;; 0100
$constant "O-EXCL"      0x000080  ;; 0200
$constant "O-NOCTTY"    0x000100  ;; 0400
$constant "O-TRUNC"     0x000200  ;; 01000
$constant "O-APPEND"    0x000400  ;; 02000
$constant "O-NONBLOCK"  0x000800  ;; 04000
$constant "O-DSYNC"     0x001000  ;; 010000
$constant "O-SYNC"      0x101000  ;; 04010000
$constant "O-RSYNC"     0x101000  ;; 04010000
$constant "O-DIRECTORY" 0x010000  ;; 0200000
$constant "O-NOFOLLOW"  0x020000  ;; 0400000
$constant "O-CLOEXEC"   0x080000  ;; 02000000

$constant "O-ASYNC"     0x002000  ;; 020000
$constant "O-DIRECT"    0x004000  ;; 040000
$constant "O-LARGEFILE" 0x008000  ;; 0100000
$constant "O-NOATIME"   0x040000  ;; 01000000
$constant "O-PATH"      0x200000  ;; 010000000
$constant "O-TMPFILE"   0x410000  ;; 020200000
$constant "O-NDELAY"    0x000800  ;; 04000

$constant "O-CREATE-WRONLY-FLAGS" 0x000241
$constant "O-CREATE-MODE-NORMAL"  0x1A4

$constant "(SEEK-SET)" 0
$constant "(SEEK-CUR)" 1
$constant "(SEEK-END)" 2

$constant "S-ISUID" 0x800  ;; 04000
$constant "S-ISGID" 0x400  ;; 02000
$constant "S-ISVTX" 0x200  ;; 01000
$constant "S-IRUSR" 0x100  ;; 0400
$constant "S-IWUSR" 0x080  ;; 0200
$constant "S-IXUSR" 0x040  ;; 0100
$constant "S-IRWXU" 0x1c0  ;; 0700
$constant "S-IRGRP" 0x020  ;; 0040
$constant "S-IWGRP" 0x010  ;; 0020
$constant "S-IXGRP" 0x008  ;; 0010
$constant "S-IRWXG" 0x038  ;; 0070
$constant "S-IROTH" 0x004  ;; 0004
$constant "S-IWOTH" 0x002  ;; 0002
$constant "S-IXOTH" 0x001  ;; 0001
$constant "S-IRWXO" 0x007  ;; 0007


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "STAT.st_dev"         0  ;; rd 1
$constant "STAT.st_ino"         4  ;; rd 1
$constant "STAT.st_mode"        8  ;; rw 1
$constant "STAT.st_nlink"      10  ;; rw 1
$constant "STAT.st_uid"        12  ;; rw 1
$constant "STAT.st_gid"        14  ;; rw 1
$constant "STAT.st_rdev"       16  ;; rd 1
$constant "STAT.st_size"       20  ;; rd 1
$constant "STAT.st_blksize"    24  ;; rd 1
$constant "STAT.st_blocks"     28  ;; rd 1
$constant "STAT.st_atime"      32  ;; rd 1
$constant "STAT.st_atime_nsec" 36  ;; rd 1
$constant "STAT.st_mtime"      40  ;; rd 1
$constant "STAT.st_mtime_nsec" 44  ;; rd 1
$constant "STAT.st_ctime"      48  ;; rd 1
$constant "STAT.st_ctime_nsec" 52  ;; rd 1
$constant "STAT.__unused4"     56  ;; rd 1
$constant "STAT.__unused5"     60  ;; rd 1
$constant "STAT.statsize"      64


;; is regular file?
code: (STAT-MODE-ASCIIZ)  ( addr -- mode // -1 )
  jecxz .fucked
  ;; EDI: addr
  ;; save registers
  push  EIP
  push  ERP
  ;; alloc stat struct
  sub   esp,64  ;;stat.statsize
  ;; syscall
  ld    eax,106
  ld    ebx,TOS
  ld    ecx,esp
  syscall
  ;; get mode to ecx
  movzx TOS,word [esp+8]  ;; st_mode
  ;; drop stat struct
  add   esp,64  ;;stat.statsize
  ;; restore registers
  pop   ERP
  pop   EIP
  or    eax,eax
  jr    nz,.fucked
  urnext
.fucked:
  ld    TOS,-1
  urnext
endcode


;; is regular file?
: (FILE?)  ( addr count -- flag )
  tmp-pool-ensure-asciiz >r
  (STAT-MODE-ASCIIZ)
  r> tmp-pool-release
  dup -1 <> if 0x8000 and notnot else drop false endif  ;; 0100000, S_IFREG
;

;; is directory?
: (DIR?)  ( addr count -- flag )
  tmp-pool-ensure-asciiz >r
  (STAT-MODE-ASCIIZ)
  r> tmp-pool-release
  dup -1 <> if 0x4000 and notnot else drop false endif  ;; 0040000, S_IFDIR
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get seconds since epoch
code: SYS-TIME  ( -- seconds )
  push  TOS
  push  EIP
  push  ERP
  ld    eax,13   ;; sys_time
  xor   ebx,ebx  ;; result only in eax
  syscall
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
endcode

$constant "CLOCK-REALTIME"            0
$constant "CLOCK-MONOTONIC"           1
$constant "CLOCK-PROCESS-CPUTIME-ID"  2
$constant "CLOCK-THREAD-CPUTIME-ID"   3
$constant "CLOCK-MONOTONIC-RAW"       4
$constant "CLOCK-REALTIME-COARSE"     5
$constant "CLOCK-MONOTONIC-COARSE"    6
$constant "CLOCK-BOOTTIME"            7
$constant "CLOCK-REALTIME-ALARM"      8
$constant "CLOCK-BOOTTIME-ALARM"      9

$constant "NANOSECONDS-PER-SECOND"  1000000000
$constant "NANOSECONDS-PER-MSEC"    1000000

;; get seconds and nanoseconds (since some random starting point)
code: SYS-CLOCK-GETTIME  ( clockid -- seconds nanoseconds )
  push  EIP
  push  ERP
  sub   esp,4+4  ;; timespec
  ld    eax,265  ;; sys_clock_gettime
  ld    ebx,TOS  ;; clockid
  ld    ecx,esp
  syscall
  or    eax,eax
  jr    nz,.error
  ld    eax,[esp]    ;; seconds
  ld    TOS,[esp+4]  ;; nanoseconds
  add   esp,4+4
  pop   ERP
  pop   EIP
  push  eax
  urnext
.error:
  add   esp,4+4
  pop   ERP
  pop   EIP
  xor   TOS,TOS
  push  TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$value "(SYS-GETTICKCOUNT-START-SECS)" 0
(hidden)

: (SYS-GETTICKCOUNT-INIT)  ( -- )
  clock-monotonic sys-clock-gettime drop
  dup 1 > if 1- endif
  to (sys-gettickcount-start-secs)
;
(hidden)


: SYS-GETTICKCOUNT  ( -- msecs )
  clock-monotonic sys-clock-gettime
  nanoseconds-per-msec u/
  swap (sys-gettickcount-start-secs) - 1000 u*
  +
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; call a C function
code: (CINVOKE) ( ... argcount addr -- res )
  pushr TOS
  pop   eax
  pushr eax
  call  eax
  mov   TOS,eax
  popr  edx
  shl   edx,2
  add   esp,edx
  popr  TOS
  push  TOS
  urnext
endcode
