;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$variable "(CSP)" 0
(hidden)

: !CSP  ( -- )
  sp@ (csp) !
;

: ?CSP  ( -- )
  sp@ (csp) @ - err-unfinished-definition ?error
;

: ?COMP  ( -- )
  state @ 0 = err-compilation-only ?error
;

: ?EXEC  ( -- )
  state @ err-execution-only ?error
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; compile call to cfa
;; do not use comma directly, so high-level words will survive any threaded code changes
: COMPILE,  ( cfa -- )
  ,
;


: COMPILE-STR,  ( addr count -- )
  ;; check if the string is at "here" (just in case)
  over here here cell+ 1- bounds? if
    ;; move forward to make room for counter
    dup >r here cell+ swap move
    here cell+ r>
  endif
  dup cell+ 1+ n-allot
  ;; ( addr count destaddr )
  2dup ! cell+  ;; length
  2dup + 0c!    ;; terminating zero byte
  swap move     ;; string itself
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: LITERAL  ( n -- )
  state @ if
    compile lit
    ,
  endif
; immediate


: CFALITERAL  ( cfa -- )
  state @ if
    compile litcfa
    ,
  endif
; immediate


: CUSTOM-SLITERAL  ( addr count cfa -- )
  over 0< err-negative-allot ?error
  >r  ;; save cfa
  ;; check if the string is at "here" (just in case)
  over here here 2 +cells 1- bounds? if
    ;; move forward to make room for "litstr" and other things
    dup >r here 2 +cells swap move
    here 2 +cells r>
  endif
  ;; compile cfa
  r> compile,
  ;; and string
  compile-str,
;

: SLITERAL  ( addr count -- )
  state @ if
    ['] litstr custom-sliteral
  endif
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; write 4-byte displacement for CALL/JMP jmpdest instruction to addr
;; addr should point after the instruction, at the first displacement byte
: (DISP32!)  ( jmpdest addr -- )
  dup cell+ rot swap - swap !
; (hidden)

: (DISP32@)  ( addr -- jumpdest )
  dup @ swap cell+ +
; (hidden)

;; write 5-byte CALL calldest machine instruction to addr
: (CALL!)  ( calldest addr -- )
  0xe8 over c! 1+  ;; write CALL, advance address
  (disp32!)
; (hidden)

;; write 5-byte JMP jmpdest machine instruction to addr
: (JMP!)  ( jmpdest addr -- )
  0xe9 over c! 1+  ;; write JMP, advance address
  (disp32!)
; (hidden)

;; compile 5-byte CALL calldest machine instruction to HERE
: (CALL,)  ( calldest -- )
  5 n-allot (call!)
; (hidden)

;; compile 5-byte JMP jmpdest machine instruction to HERE
: (JMP,)  ( jmpdest -- )
  5 n-allot (jmp!)
; (hidden)
