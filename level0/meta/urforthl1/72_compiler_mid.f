;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


: RECURSE  ( -- )
  latest-cfa compile,
; immediate


: [CHAR]  ( -- ch )  \ word
  parse-name 1 <> err-char-expected ?error
  c@ [compile] literal
; immediate


: '  ( -- cfa )  \ word
  -find-required
;

: [']  ( -- cfa )  \ word
  -find-required
  [compile] cfaliteral
; immediate


: COMPILE  ( -- )
  ?comp
  -find-required
  [compile] cfaliteral
  compile compile,
; immediate

: [COMPILE]  ( -- )
  ?comp
  -find-required
  compile,
; immediate


;; ANS idiocity, does this:
;; if the next word is immediate, compiles it in the current word
;; if the next word is not immediate, compiles "compile nextword"
;; shit
: POSTPONE  ( -- )
  ?comp
  -find-required
  dup cfa->nfa nfa->ffa ffa@ (wflag-immediate) and
  ifnot
    ;; not immediate, do what "COMPILE" does
    [compile] cfaliteral
    compile compile,
  else
    ;; immediate, do what "[COMPILE]" does
    compile,
  endif
; immediate


: [  ( -- )
  state 0!
; immediate

: ]  ( -- )
  state 1!
;


: (  ( -- )
  41 parse 2drop
; immediate

: \
  parse-skip-to-eol
; immediate

: \\
  parse-skip-to-eol
; immediate

: ;;
  parse-skip-to-eol
; immediate

;; multiline comment
;; (* .... *)
: (*  ( -- )  ;; *)
  comment-multiline
; immediate

;; nested multiline comment
;; (+ .... +)
: (+  ( -- ) ;; +)
  comment-multiline-nested
; immediate


: (parse-and-unescape)  ( ch -- addr count )
  parse dup #pad-area cell- u> err-string-too-long ?error
  over >r >r pad r@ move
  pad r@ str-unescape
  ;; it can never be bigger that the original, so it is save to compare here
  2dup r> r> 2dup 2>r s= if
    2drop 2r>
  else
    2rdrop
  endif
;

: "  ( -- addr count )  \ word  ;; "
  34 (parse-and-unescape) [compile] sliteral
; immediate

$alias '"' 'S"' immediate  ;; '


: ."  ( -- )  \ word  ;; "
  34 (parse-and-unescape)
  state @ if
    ['] (.") custom-sliteral
  else
    type
  endif
; immediate

: .(  ( -- )
  [char] ) (parse-and-unescape) type
; immediate
