;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; very simple pool allocator for temporary data
;; allocates bytes from the end of the memory
$variable "(TMP-POOL-TOP)" 0
(hidden)
$constant "(TMP-POOL-BOT-OFS)" 1024
(hidden)

;; resets temporary pool (i.e. deallocates everything)
: TMP-POOL-RESET  ( -- )
  dp-end @ (tmp-pool-bot-ofs) - (tmp-pool-top) !
;

;; returns temp pool "here", i.e. current top
;; can be used to avoid keeping it elsewhere
: TMP-POOL-HERE  ( -- addr )
  (tmp-pool-top) @
;

;; returns temp pool "mark", which can be used in "TMP-POOL-RELEASE"
: TMP-POOL-MARK  ( -- addr )
  tmp-pool-here
;

;; deallocates temp pool bytes
;; aborts on invalid mark values
: TMP-POOL-RELEASE  ( addr -- )
  ;; trying to allocate something instead of freeing it?
  dup (tmp-pool-top) @ u< err-invalid-temp-pool-release ?error
  ;; trying to go beyound the end of the pool?
  dup dp-end @ (tmp-pool-bot-ofs) - u> err-invalid-temp-pool-release ?error
  ;; set this as new pool top
  (tmp-pool-top) !
;

;; deallocates temp pool bytes
;; aborts on invalid mark values
: TMP-POOL-ALLOC  ( size -- addr )
  ;; sanity check for size
  dup unused u> err-invalid-temp-pool-allocation ?error
  ;; check if we'll have less than 64KB of room between the temp pool and HERE
  dup here + 65536 + (tmp-pool-top) @ u> err-out-of-temp-pool ?error
  ;; allocate
  (tmp-pool-top) -!
  ;; return the address of the allocated memory
  tmp-pool-here
;


;; call this for any string
;; string that ends with 0 will not be copied
;; for ease of using, call "tmp-pool-release" anyway (the mark is valid)
: TMP-POOL-ENSURE-ASCIIZ  ( addr count -- addr poolmark )
  tmp-pool-mark >r
  ;; ( addr count )
  dup 0<= if
    ;; ( addr count )
    2drop 4 tmp-pool-alloc dup 0!
  else
    ;; ( addr count )
    2dup + c@ if
      dup 1+ tmp-pool-alloc  ;; ( addr count newaddr )
      2dup + 0c!             ;; ( addr count newaddr )
      dup >r swap move r>
    else
      drop  ;; we don't need any length
    endif
  endif
  r>
;
