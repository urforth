;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; swap bytes of the low word
;; high word is untouched
code: BSWAP-WORD  ( u -- u )
  xchg  cl,ch
  urnext
endcode


;; swap all dword bytes
code: BSWAP-DWORD  ( u -- u )
  bswap TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: CELLS  ( count -- count*4 )
  shl   TOS,2
  urnext
endcode

code: +CELLS  ( addr count -- addr+count*4 )
  shl   TOS,2
  pop   eax
  add   TOS,eax
  urnext
endcode

code: -CELLS  ( addr count -- addr-count*4 )
  shl   TOS,2
  pop   eax
  sub   eax,TOS
  ld    TOS,eax
  urnext
endcode

code: CELL+  ( count -- count+4 )
  add   TOS,4
  urnext
endcode

code: CELL-  ( count -- count-4 )
  sub   TOS,4
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: 1+  ( n -- n+1 )
  inc   TOS
  urnext
endcode

code: 1-  ( n -- n-1 )
  dec   TOS
  urnext
endcode

code: 2+  ( n -- n+2 )
  add   TOS,2
  urnext
endcode

code: 2-  ( n -- n-2 )
  sub   TOS,2
  urnext
endcode

code: 4+  ( n -- n+4 )
  add   TOS,4
  urnext
endcode

code: 4-  ( n -- n-4 )
  sub   TOS,4
  urnext
endcode

code: 8+  ( n -- n+8 )
  add   TOS,8
  urnext
endcode

code: 8-  ( n -- n-8 )
  sub   TOS,8
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: NOT  ( n -- !n )
  cmp   TOS,1
  ;;  C: TOS == 0
  ;; NC: TOS != 0
  mov   TOS,0
  adc   TOS,0
  urnext
endcode

code: NOTNOT  ( n -- !!n )
  cmp   TOS,1
  ;;  C: TOS == 0
  ;; NC: TOS != 0
  mov   TOS,1
  sbb   TOS,0
  urnext
endcode

code: BITNOT  ( n -- ~n )
  xor   TOS,0xffffffff
  urnext
endcode

code: AND  ( n0 n1 -- n0&n1 )
  pop   eax
  xchg  eax,TOS
  and   TOS,eax
  urnext
endcode

code: ~AND  ( n0 n1 -- n0&~n1 )
  xor   TOS,0xffffffff
  pop   eax
  xchg  eax,TOS
  and   TOS,eax
  urnext
endcode

code: OR  ( n0 n1 -- n0|n1 )
  pop   eax
  xchg  eax,TOS
  or    TOS,eax
  urnext
endcode

code: XOR  ( n0 n1 -- n0^n1 )
  pop   eax
  xchg  eax,TOS
  xor   TOS,eax
  urnext
endcode


code: LOGAND  ( n0 n1 -- n0&&n1 )
  pop   eax
  or    al,al
  setnz al
  or    cl,cl
  setnz cl
  and   cl,al
  and   ecx,0xff
  urnext
endcode

code: LOGOR  ( n0 n1 -- n0||n1 )
  pop   eax
  or    TOS,eax
  or    TOS,TOS
  setnz cl
  and   ecx,0xff
  urnext
endcode


code: LSHIFT  ( n0 n1 -- n0<<n1 )
  pop   eax
  cmp   TOS,32
  jr    nc,fword_shl_zero
  ;; assume that TOS is in ECX
  shl   eax,cl
  mov   TOS,eax
  urnext
fword_shl_zero:
  xor   TOS,TOS
  urnext
endcode

code: RSHIFT  ( n0 n1 -- n0>>n1 )
  pop   eax
  cmp   TOS,32
  jr    nc,.zero
  ;; assume that TOS is in ECX
  shr   eax,cl
  mov   TOS,eax
  urnext
.zero:
  xor   TOS,TOS
  urnext
endcode

code: ARSHIFT  ( n0 n1 -- n0>>n1 )
  pop   eax
  cmp   TOS,32
  jr    nc,.toobig
  ;; assume that TOS is in ECX
  sar   eax,cl
  mov   TOS,eax
  urnext
.toobig:
  mov   TOS,-1
  cmp   eax,0x80000000
  adc   TOS,0
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: LROTATE  ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   eax,cl
  ld    TOS,eax
  urnext
endcode

code: RROTATE  ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   eax,cl
  ld    TOS,eax
  urnext
endcode

code: LROTATE-WORD  ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   ax,cl
  ld    TOS,eax
  urnext
endcode

code: RROTATE-WORD  ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   ax,cl
  ld    TOS,eax
  urnext
endcode

code: LROTATE-BYTE  ( n0 n1 -- n0 rol n1 )
  pop   eax
  and   cl,31
  rol   al,cl
  ld    TOS,eax
  urnext
endcode

code: RROTATE-BYTE  ( n0 n1 -- n0 ror n1 )
  pop   eax
  and   cl,31
  ror   al,cl
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: +  ( n0 n1 -- n0+n1 )
  pop   eax
  add   TOS,eax
  urnext
endcode

code: -  ( n0 n1 -- n0-n1 )
  pop   eax
  ;; EAX=n0
  ;; TOS=n1
  sub   eax,TOS
  mov   TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: 2*  ( n -- n*2 )
  shl   TOS,1
  urnext
endcode

code: 2/  ( n -- n/2 )
  sar   TOS,1
  urnext
endcode


code: 2U*  ( n -- n*2 )
  shl   TOS,1
  urnext
endcode

code: 2U/  ( n -- n/2 )
  shr   TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: NEGATE  ( n -- -n )
  neg   TOS
  urnext
endcode

code: ABS  ( n -- |n| )
  test  TOS,0x80000000
  jr    z,@f
  neg   TOS
@@:
  urnext
endcode


code: UMIN  ( u0 u1 -- umin )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    eax,TOS
  cmovc TOS,eax
  urnext
endcode

code: UMAX  ( u0 u1 -- umax )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    TOS,eax
  cmovc TOS,eax
  urnext
endcode

code: MIN  ( n0 n1 -- nmin )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    TOS,eax
  cmovg TOS,eax
  urnext
endcode

code: MAX  ( n0 n1 -- nmax )
  pop   eax
  ;; EAX=u0
  ;; TOS=u1
  cp    TOS,eax
  cmovl TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: C>S  ( n-8-bit -- n )
  movsx TOS,cl
  urnext
endcode

code: C>U  ( u-8-bit -- u )
  movzx TOS,cl
  urnext
endcode

;; with clamping
code: S>C  ( n -- n-8-bit )
  test  TOS,0x80000000
  jr    nz,.negative
  cp    TOS,0x80
  jr    c,.done
  ld    TOS,0x7f
  jr    .done
.negative:
  cp    TOS,0xffffff80
  jr    nc,.done
  ld    TOS,0x80
.done:
  movsx TOS,cl
  urnext
endcode

;; with clamping
code: U>C  ( u -- u-8-bit )
  cp    TOS,0x100
  jr    c,.done
  mov   cl,0xff
.done:
  movzx TOS,cl
  urnext
endcode
