;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; default abort calls ABORT-CLEANUP, and then MAIN-LOOP
$defer "ABORT-CLEANUP" cfa "(abort-cleanup)"

;; ye good olde ABORT, vectorized
;; this word should never return
$defer "ABORT" cfa "(abort)"

;; called when the system needs to abort with error message
;; this word should never return
;; ( errcode )
$defer "ERROR" cfa "(error)"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$constant "ERR-UNKNOWN-WORD"                   0
$constant "ERR-STACK-UNDERFLOW"                1
$constant "ERR-STACK-OVERFLOW"                 2
$constant "ERR-R-STACK-UNDERFLOW"              3
$constant "ERR-R-STACK-OVERFLOW"               4
$constant "ERR-OUT-OF-MEMORY"                  5
$constant "ERR-WORD-REDEFINED"                 6
$constant "ERR-FILE-NOT-FOUND"                 7
$constant "ERR-FILE-READ-ERROR"                8
$constant "ERR-FILE-WRITE-ERROR"               9
$constant "ERR-FILE-TOO-BIG"                  10
$constant "ERR-COMPILATION-ONLY"              11
$constant "ERR-EXECUTION-ONLY"                12
$constant "ERR-UNPAIRED-CONDITIONALS"         13
$constant "ERR-UNFINISHED-DEFINITION"         14
$constant "ERR-IN-PROTECTED-DICT"             15  ;; for "forget" (which will prolly never materialize)
$constant "ERR-NOT-DEFER"                     16
$constant "ERR-INVALID-WORD-NAME"             17
$constant "ERR-WORD-EXPECTED"                 18
$constant "ERR-INVALID-TEMP-POOL-RELEASE"     19
$constant "ERR-OUT-OF-TEMP-POOL"              20
$constant "ERR-INVALID-TEMP-POOL-ALLOCATION"  21
$constant "ERR-VOCABULARY-STACK-OVERFLOW"     22
$constant "ERR-CHAR-EXPECTED"                 23
$constant "ERR-STRING-EXPECTED"               24
$constant "ERR-NUMBER-EXPECTED"               25
$constant "ERR-VOCABULARY-EXPECTED"           26
$constant "ERR-INVALID-BREAK-CONT"            27
$constant "ERR-NONNAKED-SYSTEM"               28
$constant "ERR-NOT-IMPLEMENTED"               29
$constant "ERR-NEGATIVE-ALLOT"                30
$constant "ERR-NO-TEMP-HERE"                  31
$constant "ERR-TEMP-HERE-ALREADY"             32
$constant "ERR-CANNOT-OVERRIDE"               33
$constant "ERR-CANNOT-REPLACE"                34
$constant "ERR-INPUT-TOO-LONG"                35
$constant "ERR-THROW-WITHOUT-CATCH"           36
$constant "ERR-THROW-CHAIN-CORRUPTED"         37
$constant "ERR-UNBALANCED-IFDEF"              38
$constant "ERR-STRING-TOO-LONG"               39

$constant "ERR-USER-ERROR" 69


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$array "(ERROR-MSG-TABLE)"
(hidden)
  $tablemsg "ERR-UNKNOWN-WORD" "pardon?"
  $tablemsg "ERR-STACK-UNDERFLOW" "stack underflow"
  $tablemsg "ERR-STACK-OVERFLOW" "stack overflow"
  $tablemsg "ERR-R-STACK-UNDERFLOW" "return stack underflow"
  $tablemsg "ERR-R-STACK-OVERFLOW" "return stack overflow"
  $tablemsg "ERR-OUT-OF-MEMORY" "out of memory"
  $tablemsg "ERR-WORD-REDEFINED" "is not unique"
  $tablemsg "ERR-FILE-NOT-FOUND" "file not found"
  $tablemsg "ERR-FILE-READ-ERROR" "file read error"
  $tablemsg "ERR-FILE-WRITE-ERROR" "file write error"
  $tablemsg "ERR-FILE-TOO-BIG" "file too big"
  $tablemsg "ERR-COMPILATION-ONLY" "compilation only"
  $tablemsg "ERR-EXECUTION-ONLY" "execution only"
  $tablemsg "ERR-UNPAIRED-CONDITIONALS" "conditionals not paired"
  $tablemsg "ERR-UNFINISHED-DEFINITION" "definition not finished"
  $tablemsg "ERR-IN-PROTECTED-DICT" "in protected dictionary"
  $tablemsg "ERR-NOT-DEFER" "not a DEFER word"
  $tablemsg "ERR-INVALID-WORD-NAME" "invalid word name"
  $tablemsg "ERR-WORD-EXPECTED" "word expected"
  $tablemsg "ERR-INVALID-TEMP-POOL-RELEASE" "invalid temp pool release"
  $tablemsg "ERR-OUT-OF-TEMP-POOL" "out of memory for temp pool"
  $tablemsg "ERR-INVALID-TEMP-POOL-ALLOCATION" "invalid temp pool request"
  $tablemsg "ERR-VOCABULARY-STACK-OVERFLOW" "vocabulary stack overflow"
  $tablemsg "ERR-CHAR-EXPECTED" "character expected"
  $tablemsg "ERR-STRING-EXPECTED" "string expected"
  $tablemsg "ERR-INVALID-BREAK-CONT" "invalid break/continue"
  $tablemsg "ERR-NONNAKED-SYSTEM" "non-naked system"
  $tablemsg "ERR-NOT-IMPLEMENTED" "not implemented"
  $tablemsg "ERR-NEGATIVE-ALLOT" "negative ALLOT is not allowed"
  $tablemsg "ERR-NO-TEMP-HERE" "not allowed in transient HERE"
  $tablemsg "ERR-TEMP-HERE-ALREADY" "transient HERE already in use"
  $tablemsg "ERR-CANNOT-OVERRIDE" "cannot override non-Forth word"
  $tablemsg "ERR-CANNOT-REPLACE" "no code space to replace word"
  $tablemsg "ERR-INPUT-TOO-LONG" "input too long"
  $tablemsg "ERR-THROW-WITHOUT-CATCH" "THROW without CATCH"
  $tablemsg "ERR-THROW-CHAIN-CORRUPTED" "THROW chain corrupted"
  $tablemsg "ERR-UNBALANCED-IFDEF" "unbalanced ifdefs"
  $tablemsg "ERR-USER-ERROR" "user-defined error"
  $tablemsg "ERR-STRING-TOO-LONG" "string too long"
  dd 0xffffffff
$endarray


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (find-errmsg)
( id tbladdr -- addr count 1 )
( id tbladdr -- id         0 )
  swap >r
  begin
    dup @ 1+
  while
    dup @ r@ =
    if
      ;; i found her!
      rdrop cell+ zcount 1 exit
    endif
    ;; skip
    cell+ zcount + 1+
  repeat
  drop r> 0
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; error 0 is "unknown word"; usually called with the word in HERE
;; the word should be already dumped, and we won't show the usual "ERROR:" prompt
: ERROR-MESSAGE  ( errcode -- )
  dup err-unknown-word = if
    ;; unknown word
    (error-msg-table) (find-errmsg) ifnot drop ." pardon?" endif
    63 emit space type
  else
    ;; look for error message in the table
    ?endcr if space endif
    ." ERROR"
    (error-msg-table) (find-errmsg) if
      ." : " type
    else
      ." #"
      base @ swap decimal 0 .r base !
    endif
  endif
;


: ERROR-LINE.  ( -- )
  ;;tib_calc_currline ?dup
  tib-line# @ ?dup if
    ."  around line #"
    ;; if we just read CR, go back one line
    (tib-last-read-char) dup 10 = swap 13 = or if 1- 1 max endif
    base @ >r decimal . r> base !
    (tib-curr-fname) count ?dup
    if
      ." of file \`" type 34 emit space
    else
      drop
    endif
  endif
;

: (ERROR)  ( errcode -- )
  decimal  ;; because why not
  error-message
  error-line.
  ;; print stacks depth
    base @ >r decimal
    ."  D:"
    sp0 @ sp@ - 2 arshift .
    ." R:"
    rp0 @ rp@ - 2 arshift .
    ;;pardottype ")"
    cr
    r> base !
  cr abort
  ;; just in case it returns
  1 n-bye
; (noreturn) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; issue an error message with the given code if the boolean flag is true
: ?ERROR  ( flag code -- )
  swap if error endif
  drop
;


: (ABORT-CLEANUP)  ( -- )
  sp0!
  r@ rp0!
  state 0!
  tmp-pool-reset
  (dbginfo-reset)
  tload-verbose-default to tload-verbose
  dp-temp 0!
  >r
; (hidden)

: (ABORT)  ( -- )
  (abort-cleanup)
  main-loop
  ;; just in case it returns
  bye
; (noreturn) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dump data stack (unlimited depth)
: .STACK  ( -- )
  depth dup 0 < if
    ." stack underflowed" cr drop exit
  endif
  ?dup ifnot
    ." stack empty" cr exit
  endif
  dup ." stack depth: " . cr
  0 do
    depth 1- i - pick
    dup . ." | " u. cr
  loop
;
