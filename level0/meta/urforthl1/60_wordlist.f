;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; flags for "(XCFIND-FLAGS)"
XCFIND_FLAG_CASE_SENSITIVE  equ 0x01
XCFIND_FLAG_SKIP_MASK_CHECK equ 0x02

$constant "(VOC-FIND-CASE-SENSITIVE)" XCFIND_FLAG_CASE_SENSITIVE
$constant "(VOC-FIND-SKIP-MASK-CHECK)" XCFIND_FLAG_SKIP_MASK_CHECK

$variable "(VOC-FIND-FLAGS)" 0

;; only first (low) word is relevant
;; "(XCFIND)" fill bitand this with flags byte, and will
;; ignore all words with non-zero result
;; default value is "ignore SMUDGE"
$variable "(VOC-FIND-IGNORE-MASK)" FLAG_SMUDGE


$constant "(WFLAG-IMMEDIATE)" FLAG_IMMEDIATE
$constant "(WFLAG-SMUDGE)"    FLAG_SMUDGE
$constant "(WFLAG-NORETURN)"  FLAG_NORETURN
$constant "(WFLAG-HIDDEN)"    FLAG_HIDDEN
$constant "(WFLAG-CODEBLOCK)" FLAG_CODEBLOCK
$constant "(WFLAG-VOCAB)"     FLAG_VOCAB

$constant "(WARG-NONE)"   WARG_NONE
$constant "(WARG-BRANCH)" WARG_BRANCH
$constant "(WARG-LIT)"    WARG_LIT
$constant "(WARG-C4STRZ)" WARG_C4STRZ
$constant "(WARG-CFA)"    WARG_CFA
$constant "(WARG-CBLOCK)" WARG_CBLOCK
$constant "(WARG-VOCID)"  WARG_VOCID


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: CFA->PFA  ( cfa -- pfa )
  add   TOS,5     ;; skip call
  urnext
endcode

code: PFA->CFA  ( pfa -- cfa )
  sub   TOS,5     ;; undo skip call
  urnext
endcode

code: CFA->NFA  ( cfa -- nfa )
  movzx eax,byte [TOS-1]
  add   eax,1+4      ;; trailing length, lenflags
  sub   TOS,eax
  urnext
endcode

code: NFA->CFA  ( nfa -- cfa )
  movzx eax,byte [TOS]
  add   eax,1+4      ;; trailing length, lenflags
  add   TOS,eax
  urnext
endcode

code: CFA->LFA  ( cfa -- lfa )
  movzx eax,byte [TOS-1]
  add   eax,1+4+4+4  ;; trailing length, lenflags, namehash, lfa
  sub   TOS,eax
  urnext
  sub   TOS,4
  urnext
endcode

code: LFA->CFA  ( lfa -- cfa )
  movzx eax,byte [TOS+8]
  add   eax,1+4+4+4  ;; trailing length, lenflags, namehash, lfa
  add   TOS,eax
  urnext
endcode

code: LFA->NFA  ( lfa -- cfa )
  add   TOS,8
  urnext
endcode

code: NFA-COUNT  ( nfa -- addr count )
  movzx eax,byte [TOS]
  add   TOS,4  ;; leadlenflags
  push  TOS
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; to debugptr field
code: NFA->DFA  ( nfa -- dfa )
  sub   TOS,20
  urnext
endcode

;; to word size field
code: NFA->SFA  ( nfa -- sfa )
  sub   TOS,16
  urnext
endcode

;; to bucketptr field
code: NFA->BFA  ( nfa -- bfa )
  sub   TOS,12
  urnext
endcode

;; to word size field
code: NFA->LFA  ( nfa -- sfa )
  sub   TOS,8
  urnext
endcode

;; to name hash field
code: NFA->HFA  ( nfa -- hfa )
  sub   TOS,4
  urnext
endcode

;; flag fields area; 2 bytes
code: NFA->FFA  ( nfa -- ffa )
  add   TOS,2
  urnext
endcode

;; argument type area; 1 byte
code: NFA->TFA  ( nfa -- tfa )
  inc   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: BFA->NFA  ( bfa -- nfa )
  add   TOS,4+4+4
  urnext
endcode

code: BFA->LFA  ( bfa -- lfa )
  add   TOS,4
  urnext
endcode

code: BFA->HFA  ( bfa -- hfa )
  add   TOS,4+4
  urnext
endcode

code: HFA->BFA  ( bfa -- hfa )
  sub   TOS,4+4
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: FFA@  ( ffa -- ffa-value )
  movzx TOS,word [TOS]
  urnext
endcode

code: TFA@  ( tfa -- tfa-value )
  movzx TOS,byte [TOS]
  urnext
endcode

code: FFA!  ( ffa-value ffa -- )
  pop   eax
  ld    word [TOS],ax
  pop   TOS
  urnext
endcode

code: TFA!  ( tfa-value tfa -- )
  pop   eax
  ld    byte [TOS],al
  pop   TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: LATEST-LFA  ( -- lfa )
  current @ @
;

: LATEST-NFA  ( -- nfa )
  latest-lfa lfa->nfa
;

: LATEST-CFA  ( -- cfa )
  latest-lfa lfa->cfa
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (TOGGLE-LATEST-WFLAG)  ( flag -- )
  latest-nfa nfa->ffa swap wtoggle
; (hidden)

: (RESET-LATEST-WFLAG)  ( flag -- )
  latest-nfa nfa->ffa
  dup w@
  ;; ( flag addr oldflg )
  rot ~and swap w!
; (hidden)

: (SET-LATEST-WFLAG)  ( flag -- )
  latest-nfa nfa->ffa
  dup w@
  ;; ( flag addr oldflg )
  rot or swap w!
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SMUDGE  ( -- )
  (wflag-smudge) (toggle-latest-wflag)
;

: IMMEDIATE  ( -- )
  (wflag-immediate) (toggle-latest-wflag)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(XCFIND) ( addr count voclwptr -- cfa tf  ok)
;;         ( addr count voclwptr -- ff     bad)
code: VOC-FIND-STR-NOHASH
  pop   eax   ;; count
  pop   edi   ;; addr
  push  eax   ;; store length at [esp]
  ;; TOS=latest-ptr
  ;; EDI=addr
  ;; EAX=count (also at [esp])

  ;; check length
  cmp   eax,256
  jr    nc,.toolong
  or    eax,eax
  jr    z,.toolong  ;; too short, actually; zero-length words are not allowed, and cannot be found

.search_loop:
  ;; follow lfa
  mov   TOS,[TOS]
  jecxz .notfound
  ;; TOS=lfa

  ;; check "ignore mask" flag
  test  byte [pfa "(voc-find-flags)"],XCFIND_FLAG_SKIP_MASK_CHECK
  jr    nz,.skip_mask_check
  ;; check ignore mask
  mov   ax,word [TOS+2+8]
  test  ax,word [pfa "(voc-find-ignore-mask)"]
  ;; skip if we have some bits set
  ;; default setting: skip SMUDGE words
  jr    nz,.search_loop
.skip_mask_check:

  ;; compare name length
  movzx eax,byte [TOS+8]
  cmp   al,byte [esp]
  jr    nz,.search_loop
  ;; note that EAX (length) will never be zero here
  ;; this is invariant that is checked at the entry
  ;; (because 0-length strings are rejected)
  push  edi       ;; we'll need it later
  push  ecx       ;; we'll need it later
  ;; skip lfa, hash, and length cell
  add   TOS,4+4+4
  ;; compare bytes
  mov   edx,ecx   ;; EDX is name bytes
  mov   ecx,eax   ;; ECX is counter
  ;; EDI=straddr
  ;; EDX=name
  ;; ECX=namelen
.loop_cmp:
  mov   al,byte [edi]
  ;; check "case sensitive" flag
  test  byte [pfa "(voc-find-flags)"],XCFIND_FLAG_CASE_SENSITIVE
  jr    nz,.nocaseconv
  cmp   al,'a'
  jr    c,.nocaseconv
  cmp   al,'z'+1
  jr    nc,.nocaseconv
  sub   al,32
.nocaseconv:
  cmp   al,[edx]
  jr    nz,.cmpfailed
  inc   edi
  inc   edx
  loop  .loop_cmp
.loop_done:
  ;; name matches, we're done
  ;; stack: count addr nfa
  ;; we don't need any of that
  add   esp,4+4+4
  ;; EDX=after-name
  inc   edx       ;; skip trailing length
  push  edx       ;; cfa
  mov   TOS,1     ;; true
  jp    .done

.cmpfailed:
  pop   ecx
  pop   edi
  jr    .search_loop

.toolong:
  xor   TOS,TOS
.notfound:
  add   esp,4     ;; drop saved string length
.done:
  urnext
endcode


$if WLIST_HASH_BITS
;;(XCFIND) ( addr count voclwptr -- cfa tf  ok)
;;         ( addr count voclwptr -- ff     bad)
code: VOC-FIND-STR
  pop   eax   ;; count
  pop   edi   ;; addr

  ;; check length
  or    eax,eax  ;; zero-length words cannot be found
  jr    nz,@f
  xor   TOS,TOS
  urnext
@@:
  cp    eax,256
  jr    c,@f
  ;; invalid length, no need to search for anything
  xor   TOS,TOS
  urnext
@@:

  ;; calculate word hash
  ;; store count (it will become [esp+4])
  push  eax

  push  ecx
  push  edi
  ld    ecx,eax
  call  str_name_hash_edi_ecx
  ld    edx,eax
  pop   edi
  pop   ecx

  ;; store hash at [esp] (and count is at [esp+4] now)
  push  edx

  ;; fold hash, and move TOS to bucket link
  ld    eax,edx
  call  str_name_hash_fold_mask_eax
  add   eax,[pfa "(VOC-HEADER-SIZE-CELLS)"]
  shl   eax,2
  add   TOS,eax

  ;; TOS=bfa-ptr
  ;; EDI=addr

.search_loop:
  ;; follow bfa
  mov   TOS,[TOS]
  jecxz .notfound
  ;; TOS=bfa

  ;; check "ignore mask" flag
  test  byte [pfa "(voc-find-flags)"],XCFIND_FLAG_SKIP_MASK_CHECK
  jr    nz,.skip_mask_check
  ;; check ignore mask
  mov   ax,word [TOS+2+12]
  test  ax,word [pfa "(voc-find-ignore-mask)"]
  ;; skip if we have some bits set
  ;; default setting: skip SMUDGE words
  jr    nz,.search_loop
.skip_mask_check:

  ;; compare name hash
  ld    edx,[TOS+8]
  cp    edx,dword [esp]
  jr    nz,.search_loop

  ;; compare name length
  movzx eax,byte [TOS+12]
  cmp   al,byte [esp+4]
  jr    nz,.search_loop

  ;; note that EAX (length) will never be zero here
  ;; this is invariant that is checked at the entry
  ;; (because 0-length strings are rejected)
  push  edi       ;; we'll need it later
  push  ecx       ;; we'll need it later
  ;; skip bfa, lfa, hash, and length cell
  add   TOS,4+4+4+4
  ;; compare bytes
  mov   edx,ecx   ;; EDX is name bytes
  mov   ecx,eax   ;; ECX is counter
  ;; EDI=straddr
  ;; EDX=name
  ;; ECX=namelen
.loop_cmp:
  mov   al,[edi]
  ;; check "case sensitive" flag
  test  byte [pfa "(voc-find-flags)"],XCFIND_FLAG_CASE_SENSITIVE
  jr    nz,.nocaseconv
  cmp   al,'a'
  jr    c,.nocaseconv
  cmp   al,'z'+1
  jr    nc,.nocaseconv
  sub   al,32
.nocaseconv:
  cmp   al,[edx]
  jr    nz,.cmpfailed
  inc   edi
  inc   edx
  loop  .loop_cmp
.loop_done:
  ;; name matches, we're done
  ;; stack: count hash addr nfa
  ;; we don't need any of that
  add   esp,4+4+4+4
  ;; EDX=after-name
  inc   edx       ;; skip trailing length
  push  edx       ;; cfa
  mov   TOS,1     ;; true
  urnext

.cmpfailed:
  pop   ecx
  pop   edi
  jr    .search_loop

.notfound:
  add   esp,4+4   ;; drop saved string length and hash
  urnext

endcode
$else
$alias "VOC-FIND-STR-NOHASH" "VOC-FIND-STR"
$endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hidden words won't be found unless current vocabulary is the top context one
: (WFIND-STR-BASIC)
( addr count -- cfa tf  ok)
( addr count -- ff      bad)
  ;; save find mask
  (voc-find-ignore-mask) @ >r
  ;; fix "skip hidden" flag
  ;; skip smudge words
  (wflag-smudge) (voc-find-ignore-mask) or!
  ;; do not search in CURRENT
  2>r
  ;; search context stack
  context
  ;; ( ctxptr | addr count )
  begin
    dup @ ?dup
  while
    ;; ( ctxptr voclptr | addr count )
    ;; if voc-to-search is the current one, allow hidden words
    dup current @ = if
      ;; allow hidden words
      (wflag-hidden) (voc-find-ignore-mask) ~and!
    else
      ;; no hidden words
      (wflag-hidden) (voc-find-ignore-mask) or!
    endif
    ;; ( ctxptr voclptr | addr count )
    2r@ rot voc-find-str if
      ;; ( ctxptr cfa )
      nip 2rdrop 1
      ;; restore search mask
      r> (voc-find-ignore-mask) !
      exit
    endif
    ;; ( ctxptr | addr count )
    ;; if voc-to-search is the current one, look into parents
    dup @ current @ = if
      ;; hidden words already allowed above
      dup @
      begin
        vocid->parent @ ?dup
      while
        ;; ( ctxptr voclptr | savedmask addr count )
        dup 2r@ rot voc-find-str if
          ;; ( ctxptr voclptr cfa )
          nrot 2drop 2rdrop 1
          ;; restore search mask
          r> (voc-find-ignore-mask) !
          exit
        endif
      repeat
    endif
    ;; up context stack
    cell-
  repeat
  drop 2rdrop 0
  ;; restore search mask
  r> (voc-find-ignore-mask) !
; (hidden)

;; doesn't do any namespace resolution
$defer "WFIND-BASIC" cfa "(wfind-str-basic)"


: (WFIND-FLAGS>R)  ( -- )
  r>
  (voc-find-flags) @ >r
  (voc-find-ignore-mask) @ >r
  >r
; (hidden)

: (WFIND-R>FLAGS)  ( -- )
  r>
  r> (voc-find-ignore-mask) !
  r> (voc-find-flags) !
  >r
; (hidden)


;; this does namespace resolution
;; if "a:b" is not a known word, try to search "b" in dictionary "a"
;; things like "a:b:c" are allowed too
: (WFIND-STR)
( addr count -- cfa tf  ok)
( addr count -- ff      bad)
  ;; try full word first
  2dup wfind-basic if >r 2drop r> 1 exit endif
  ;; first colon
  2dup [char] : str-trim-at-char ?dup ifnot
    ;; no colon
    2drop drop 0 exit
  endif
  ;; try to find a vocabulary
  wfind-basic ifnot
    ;; not found
    2drop 0 exit
  endif
  ;; check if it is a vocabulary
  dup word-type? word-type-voc = ifnot
    ;; not a vocabulary
    2drop drop 0 exit
  endif
  ;; make sure that we can find any hidden word this way
  (wfind-flags>r)
  ;; fix flags
  (voc-find-skip-mask-check) (voc-find-flags) ~and!
  ;; fix mask
  (wflag-hidden) (voc-find-ignore-mask) ~and!
  ;; go on
  >r [char] : str-skip-after-char r>
  >r
  ;; ( addr count | vocab )
  begin
    2dup r@ voc-cfa->vocid voc-find-str
    if
      ;; i found her!
      ;; ( addr count cfa | vocab )
      >r 2drop r> rdrop 1 (wfind-r>flags) exit
    endif
    ;; ( addr count | vocab )
    2dup [char] : str-trim-at-char ?dup
    ifnot
      ;; no more colon
      ;; ( addr count addr | vocab )
      2drop drop rdrop 0 (wfind-r>flags) exit
    endif
    ;; vocabulary recursion
    ;; ( addr count addr count | vocab )
    r@ voc-cfa->vocid voc-find-str
    ifnot
      ;; not found
      ;; ( addr count | vocab )
      2drop rdrop 0 (wfind-r>flags) exit
    endif
    ;; ( addr count newvocabcfa | vocab )
    rdrop >r
    [char] : str-skip-after-char
  again
; (hidden)

$defer "WFIND-STR" cfa "(wfind-str)"


: -FIND  ( -- cfa true // false )  \ word
  parse-name wfind-str
;

: -FIND-REQUIRED  ( -- cfa )  \ word
  parse-name
  2dup wfind-str
  ifnot
    ;; ( addr count )
    ?endcr space ." \`" type ." \`? "
    err-word-expected ?error
  else
    nrot 2drop
  endif
;

: HAS-WORD?  ( addr count -- flag )
  wfind-str if
    drop 1
  else
    0
  endif
;

$alias "HAS-WORD?" "ENVIRONMENT?"
