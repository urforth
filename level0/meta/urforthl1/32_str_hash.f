;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (STR-NAME-HASH-CODEBLOCK)
(hidden)
(codeblock)
  ;; IN:
  ;;   EDI: address
  ;;   ECX: count
  ;; OUT:
  ;;   EAX: u32hash
  ;;   EDI,ECX,flags: destroyed
str_name_hash_edi_ecx:
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash <<= 4
  shl   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  add   eax,edx
  ;; high = hash&0xF0000000
  ld    edx,eax
  and   edx,0xf0000000
  ld    ebx,edx
  ;; hash ^= high>>24
  rol   edx,8
  xor   eax,edx
  ;; hash &= ~high
  xor   ebx,0xffffffff
  and   eax,ebx
  inc   edi
  loop  .hashloop
.done:
  ret

  $if WLIST_HASH_BITS
  ;; IN:
  ;;   EAX: u32hash
  ;; OUT:
  ;;   EAX: u8hash-masked
  ;;   flags: destroyed
str_name_hash_fold_mask_eax:
  push  ecx
  ;; fold u32->u16
  ld    ecx,eax
  shr   eax,16
  add   ax,cx
  ;; fold u16->u8
  add   al,ah
  ;; mask
  $if WLIST_HASH_MASK # 255
    and   al,WLIST_HASH_MASK
  $endif
  movzx eax,al
  pop   ecx
  ret
  $endif
endcode


;; this is used to calculate word name hashes
code: STR-NAME-HASH  ( addr count -- u32hash )
  pop   edi
  call  str_name_hash_edi_ecx
  ld    TOS,eax
  urnext
endcode

;; fold 32-bit hash to 16-bit hash
code: uhash32->16  ( u32hash -- u16hash )
  ld    eax,TOS
  shr   eax,16
  add   cx,ax
  movzx TOS,cx
  urnext
endcode

code: uhash16->8  ( u16hash -- u8hash )
  add   cl,ch
  movzx TOS,cl
  urnext
endcode

$if WLIST_HASH_BITS
;; this is used to calculate word name hashes
code: STR-NAME-HASH-FOLDED-MASKED  ( addr count -- maskedhash )
  pop   edi
  call  str_name_hash_edi_ecx
  call  str_name_hash_fold_mask_eax
  ld    TOS,eax
  urnext
endcode

;; this is used to calculate word name hashes
code: NAME-HASH-FOLD-MASK  ( u32hash -- maskedhash )
  ld    eax,TOS
  call  str_name_hash_fold_mask_eax
  ld    TOS,eax
  urnext
endcode
$endif
