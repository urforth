;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: (URFORTH-DOFORTH-CODEBLOCK)
(hidden)
(codeblock)
  pushr EIP
  pop   EIP
  urnext
endcode

code: (URFORTH-DOCONST-CODEBLOCK)
(hidden)
(codeblock)
  xchg  TOS,[esp]
  mov   TOS,[TOS]
  urnext
endcode

code: (URFORTH-DOVAR-CODEBLOCK)
(hidden)
(codeblock)
  xchg  TOS,[esp]
  urnext
endcode

code: (URFORTH-DOVALUE-CODEBLOCK)
(hidden)
(codeblock)
  xchg  TOS,[esp]
  mov   TOS,[TOS]
  urnext
endcode

code: (URFORTH-DODEFER-CODEBLOCK)
(hidden)
(codeblock)
  pop   eax
  mov   FREEREG,[eax]
  jp    FREEREG
endcode

code: (URFORTH-DODOES-CODEBLOCK)
(hidden)
(codeblock)
  ;; pfa is on the stack
  ;; EAX is new VM IP
  xchg  TOS,[esp]
  pushr EIP
  mov   EIP,eax
  urnext
endcode

code: (URFORTH-DOOVERRIDE-CODEBLOCK)
(hidden)
(codeblock)
  pushr EIP
  pop   EIP
  xchg  TOS,[esp]
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; use this subroutine to call a forth word from a machine code
;; EAX should point to the cfa
;; TOS and other things should be set accordingly
;; direction flag should be cleared
;; no registers are preserved
code: (URFORTH-MC-FCALL-CODEBLOCK)
(hidden)
(codeblock)
ur_mc_fcall:
  ;; move mc return address to rstack
  pop   FREEREG
  pushr FREEREG
  ;; push current EIP to rstack
  pushr EIP
  ;; set new EIP
  mov   EIP,.justexit

  ;; turn off debugger temporarily, because the debugger is using this
  ;;;;if URFORTH_DEBUG
  ;;ld    FREEREG,[urfdebug_active_flag]
  ;;pushr FREEREG
  ;;;; special flag, means "no breakpoint checks"
  ;;ld    dword [urfdebug_active_flag],0xffffffff
  ;;end if

  ;; and execute the word
  jp    eax
.justexit:
  dd  .fakeret_code
.fakeret_code:
  ;;;; restore debugger state
  ;;if URFORTH_DEBUG
  ;;popr  FREEREG
  ;;ld    dword [urfdebug_active_flag],FREEREG
  ;;end if

  ;; restore EIP
  popr  EIP
  ;; restore return address
  popr  eax
  ;; and jump there
  jp    eax
endcode
