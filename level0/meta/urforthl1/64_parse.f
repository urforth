;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; if set to true (non-zero), "PARSE-NAME" will understand comments without space delimiters
;; (except lone banana and backslash)
$value "PARSE-NAME-ADV-COMMENTS" 1


;;DIGIT ( c n1 -- n2 tf  ok)
;;      ( c n1 -- ff     bad)
;;Converts the ascii character c (using base n1) to its binary equivalent
;;n2, accompanied by a true flag. If the conversion is invalid, leaves
;;only a false flag.
code: DIGIT
  pop   eax
  ;; TOS=base
  ;; EAX=char
  jecxz .bad
  test  TOS,0x80000000
  jr    nz,.bad
  cp    TOS,36+1
  jr    nc,.bad
  sub   al,'0'
  jr    c,.bad
  cp    al,10
  jr    c,.check_base
  sub   al,7
  jr    c,.bad
  cp    al,10
  jr    c,.bad
  ;; upcase it
  cp    al,36
  jr    c,@f
  cp    al,42
  jr    c,.bad
  sub   al,32
@@:
.check_base:
  cp    al,cl     ;; ECX is TOS
  jr    nc,.bad
  movzx eax,al
  push  eax
  mov   TOS,1
  urnext
.bad:
  xor   TOS,TOS
  urnext
endcode

: DIGIT?  ( ch base -- flag )
  digit dup if nip endif
;


;; convert the ASCII text beginning at addr with regard to BASE.
;; the new value is accumulated into unsigned number u0, being left as u1.
;; addr1 and count1 are unparsed part of the string
;; will never read more than count bytes
;; doesn't skip any spaces, doesn't parse prefixes and signs
;; but skips '_'
: NUMBER-PARSE-SIMPLE  ( addr count u0 -- addr1 count1 u1 )
  over 0> ifnot exit endif
  >r
  ;; ( addr count | u )
  ;; first must be a digit
  over c@ base @ digit? if
    ;; main loop
    begin
      dup
    while
      ;; ( addr count | u )
      over c@
      ;; skip '_'
      dup [char] _ = if
        drop
      else
        ;; try digit
        base @ digit ifnot break endif
        r> base @ u* + >r
      endif
      ;; skip char
      1- swap 1+ swap
    repeat
  endif
  r>
; (hidden)


;; k8: non-conforming, because we cannot parse double numbers
: >NUMBER  ( ud1 c-addr1 u1 -- ud2 c-addr2 u2 )
  rot drop rot
  number-parse-simple
  nrot 0 nrot
;


: NUMBER-PARSE-PFX-SIGIL  ( addr count -- addr count newbase )
  ;; simple sigils
  dup 1 > ifnot 0 exit endif
  ;; simple sigils
  over c@
  ;; ( addr count char )
  dup [char] $ = if
    drop swap 1+ swap 1-
    16 exit
  endif
  dup [char] # = if
    drop swap 1+ swap 1-
    (shit-2012-idiocity) 1 and if 10 else 16 endif
    exit
  endif
  [char] % = if
    swap 1+ swap 1-
    2 exit
  endif
  0
;

: NUMBER-PARSE-PFX-0X  ( addr count -- addr count newbase )
  dup 2 > ifnot 0 exit endif
  over c@ [char] 0 = ifnot 0 exit endif
  over 1+ c@ upcase-char
  dup [char] X = if
    ;; ( addr count char )
    drop swap 2+ swap 2-
    16 exit
  endif
  dup [char] O = if
    drop swap 2+ swap 2-
    8 exit
  endif
  dup [char] B = if
    drop swap 2+ swap 2-
    2 exit
  endif
  [char] D = if
    swap 2+ swap 2-
    10 exit
  endif
  0
;

: NUMBER-PARSE-PFX-&  ( addr count -- addr count newbase )
  dup 2 > ifnot 0 exit endif
  over c@ [char] & = ifnot 0 exit endif
  over 1+ c@ upcase-char
  dup [char] H = if
    ;; ( addr count char )
    drop swap 2+ swap 2-
    16 exit
  endif
  dup [char] O = if
    drop swap 2+ swap 2-
    8 exit
  endif
  dup [char] B = if
    drop swap 2+ swap 2-
    2 exit
  endif
  [char] D = if
    swap 2+ swap 2-
    10 exit
  endif
  0
;


: (NUMBER-GOOD-BIN-DIGIT?)  ( ch -- flag )
  dup [char] _ = if drop true exit endif
  2 digit?
; (hidden)

: (NUMBER-PARSE-BOOL?)  ( addr count -- flag )
  dup 0> ifnot 2drop 0 exit endif
  ;; first must be digit
  over c@ 2 digit? ifnot 2drop 0 exit endif
  true nrot
  over + swap do
    ;; ( okflag )
    i c@ (number-good-bin-digit?) ifnot drop false unloop exit endif
  loop
; (hidden)

: NUMBER-PARSE-SFX  ( addr count -- addr count newbase )
  ;; check suffixes
  dup 2 > ifnot 0 exit endif
  2dup + 1- c@ upcase-char
  dup [char] H = if drop 1- 16 exit endif
  dup [char] O = if drop 1- 8 exit endif
  ;; the following suffix is allowed only if all digits are right, and we aren't in hex mode
  [char] B = base @ 12 < and if
    1-
    ;; ( addr count )
    2dup (number-parse-bool?) if 2 exit endif
    1+
  endif
  0
;


;; will return base according to prefix/suffix, and remove pfx/sfx from the string
;; returns 0 if no special base change found
: NUMBER-PARSE-PFX-SFX  ( addr count -- addr count newbase )
  number-parse-pfx-sigil ?dup if exit endif
  number-parse-pfx-0x ?dup if exit endif
  number-parse-pfx-& ?dup if exit endif
  number-parse-sfx
;


;; convert a character string left at addr to a signed number, using the current numeric base
: NUMBER  ( addr count -- n true // false )
  ;; check length
  dup 0<= if 2drop false exit endif
  ;; ok, we have at least one valid char
  ;; check for leading minus (only if 2012 so-called-standard idiocity is not turned on)
  (shit-2012-idiocity) 1 and if
    false
  else
    over c@ [char] - =
    if
      swap 1+ swap 1- true
    else
      false
    endif
  endif
  nrot
  ;; ( negflag addr count )
  base @ >r  ;; it can be changed by number prefix/suffix
  number-parse-pfx-sfx
  ;; done checking
  ?dup if base ! endif
  ;; check for leading minus (only if 2012 so-called-standard idiocity is turned on)
  (shit-2012-idiocity) 1 and if
    dup 0> if
      over c@ [char] - = if
        swap 1+ swap 1-
        rot drop 1 nrot
      endif
    endif
  endif
  ;; zero count means "nan"
  dup 0> ifnot
    ;; restore base
    r> base !
    ;; exit with failure
    2drop drop false exit
  endif
  ;; ( negflag addr count | oldbase )
  0 number-parse-simple
  ;; ( negflag addr count u | oldbase )
  ;; restore base
  r> base !
  ;; ( negflag addr count u )
  ;; if not fully parsed, it is nan
  swap
  ;; ( negflag addr u count )
  if
    ;; exit with failure
    drop 2drop false exit
  endif
  ;; ( negflag addr u )
  nip swap if negate endif
  ;; success
  true
;


;; scans TIB, returns parsed word
;; doesn't do any copying
;; trailing delimiter is skipped
;; HACK: sets (WORD-LAST-DELIMITER)
;;       this is so "comment-to-eol" could work
code: (WORD)  ( c skip-leading-delim? -- addr count )
(hidden)
  ;; reset last delimiter (it will be set later)
  mov   edx,[pfa "#tib"]
  sub   edx,[pfa ">in"]
  jr    c,.noinput_drop
  mov   edi,[pfa "tib"]
  add   edi,[pfa ">in"]
  ;; do we need to skip leading delimiters?
  jecxz .noskipdel
  pop   TOS   ;; get char in TOS
  ;; TOS=char
  ;; EDI=tibptr
  ;; EDX=tibleft
  ;; skip leading delimiters
.skipdel_loop:
  or    edx,edx
  jr    z,.noinput
  call  .cmp_cl_memedi
  jr    z,.skipchar
  or    al,al
  jr    z,.noinput
  jr    .startword
.skipchar:
  call  .count_lines
  inc   edi
  dec   edx
  jr    .skipdel_loop

.noskipdel:
  pop   TOS   ;; get char in TOS

.startword:
  ;; remember current position
  push  edi
  ;; skip until delimiter
.collect_loop:
  or    edx,edx
  jr    z,.done_noadv
  ;; count lines here, so we won't have to call it on word completion
  call  .count_lines
  call  .cmp_cl_memedi
  jr    z,.done
  or    al,al
  jr    z,.done_noadv
  inc   edi
  dec   edx
  jr    .collect_loop

.done:
  ;; skip delimiter
  or    edx,edx
  jr    z,.done_noadv
  cmp   byte [edi],0
  jr    z,.done_noadv
  dec   edx
  movzx eax,byte [edi]
  ld    [pfa "(tib-last-read-char)"],eax
.done_noadv:
  ;; the word is never empty here
  ;; stack: word start
  ;; EDI: word end (after the last char, at a delimiter)
  ;; EDX=tibleft
  ;; fix inptr
  mov   eax,[pfa "#tib"]
  sub   eax,edx
  mov   [pfa ">in"],eax
  ;; calc and store counter
  pop   ecx     ;; start address
  mov   eax,edi ;; EAX=end address
  sub   eax,ecx ;; EAX=length
  ;; truncate length
  ld    edx,1020
  cp    eax,edx
  cmovnc eax,edx
  ;; ECX=start address
  ;; EDI=end address
  ;; EAX=length
  ;; copy bytes
  push  TOS
  ld    TOS,eax
  urnext

.noinput_drop:
  add   esp,4   ;; drop char, as we didn't poped it yet
.noinput:
  ;; TOS=char
  ;; EDX=tibleft
  mov   eax,[pfa "#tib"]
  sub   eax,edx
  ld    edx,0
  cmovc eax,edx
  mov   [pfa ">in"],eax
  ;; push current tib position, and zero length
  add   eax,[pfa "tib"]
  push  eax
  xor   TOS,TOS
  urnext

; zero flag: equality
; al: byte at [edi]
.cmp_cl_memedi:
  movzx eax,byte [edi]
  ld    [pfa "(tib-last-read-char)"],eax
  cmp   al,1
  jr    nc,.cmp_cl_memedi_nonzero
  ret
.cmp_cl_memedi_nonzero:
  cmp   cl,32
  jr    nz,.cmp_cl_memedi_ok
  ;; coerce to space
  cmp   al,32
  jr    nc,.cmp_cl_memedi_ok
  mov   al,32
.cmp_cl_memedi_ok:
  cmp   al,cl
  ret

.count_lines:
  cp    byte [edi],10
  jr    nz,@f
  cp    dword [pfa "tib-line#"],0
  jr    z,@f
  inc   dword [pfa "tib-line#"]
@@:
  ret
endcode

;; this is a leftover from '"WORD" is always using real here'
$alias "HERE" "WORD-HERE"

: (WORD-OR-PARSE)  ( c skip-leading-delim?  -- wordhere )
  (word)
  1020 umin        ;; truncate length
  dup word-here !  ;; set counter
  word-here cell+ swap move  ;; copy string
  word-here count + 0c!      ;; put trailing zero byte
  word-here
; (hidden)

;; WARNING! it is using "HERE", so "DP-TEMP" is in effect
;; artificial word length limit: 1020 chars
;; longer words will be properly scanned, but truncated
;; adds trailing zero after the string (but doesn't include it in count)
;; string is cell-counted
: WORD  ( c  -- wordhere )
  true (word-or-parse)
;

: PARSE-TO-HERE  ( c -- wordhere )
  false (word-or-parse)
;

: PARSE  ( c -- addr count )
  0 (word)  ;; parse, don't skip leading delimiters
;

: PARSE-SKIP-BLANKS  ( -- )
  begin
    tib-peekch ?dup
  while
    32 > if exit endif
    tib-getch drop
  repeat
;

: PARSE-SKIP-BLANKS-NO-EOL  ( -- )
  begin
    tib-peekch ?dup
  while
    dup 32 > if drop exit endif
    dup 13 = swap 10 = or if exit endif
    tib-getch drop
  repeat
;

: PARSE-SKIP-BLANKS-EX  ( skipeol-flag -- )
  if parse-skip-blanks else parse-skip-blanks-no-eol endif
;


: parse-skip-to-eol  ( -- )
  ;; check last delimiter
  (tib-last-read-char) 10 = if exit endif
  begin
    tib-getch ?dup
  while
    ;; ( ch -- )
    dup 13 =
    if
      drop tib-peekch 10 = if tib-getch drop endif
      exit
    endif
    10 = if exit endif
  repeat
;

;; multiline comment
;; (* .... *) -- opening eaten
: comment-multiline  ( -- )
  begin
    tib-getch ?dup
  while
    ;; ( ch -- )
    [char] * = tib-peekch [char] ) = and if tib-getch drop exit endif
  repeat
;

;; nested multiline comment
;; (+ .... +) -- opening eaten
: comment-multiline-nested  ( -- )
  1  ;; current comment level
  begin
    tib-getch ?dup
  while
    ;; ( ch -- )
    8 lshift tib-peekch or
    dup 0x282b =  ;; (+?
    if
      drop tib-getch drop  1+
    else
      0x2b29 =   ;; +)
      if
        tib-getch drop  1-
        ?dup ifnot exit endif
      endif
    endif
  repeat
  drop
;


;; simple heuristic: if the word ends with ")", try to find it in current vocabs
;; as our searching is very fast, and this word is not invoked that often, it is ok
: (PARSE-GOOD-COMMENT?)  ( addr count -- addr count flag )
  true
; (hidden)

;; sorry for this huge mess
: (PARSE-NAME-EX)  ( -- addr count 1 // 0 )
  tibstate>r
  bl 1 (word)
(*
  ;; if word length is enough for normal comment processing, do it
  dup 1 >
  ifnot
    ;; empty word, or one-char word
    rdrop-tibstate true exit
  endif
  ;; check for single-char backslash
  over c@ [char] \ =
  if
    (parse-good-comment?) if
      2drop r>tibstate
      parse-skip-blanks
      ;; skip word char
      tib-getch drop
      comment_toeol
      false exit
    else
      true exit
    endif
  endif
*)
  ;; check for double-char comments
  dup 2 > ifnot rdrop-tibstate true exit endif
  ;; check two chars at once
  over w@
  ;; two semicolons or two slashes
  dup 0x3b3b = over 0x2f2f = or
  if
    drop
    (parse-good-comment?) if
      2drop r>tibstate
      parse-skip-blanks
      ;; skip word char
      tib-getch drop
      parse-skip-to-eol
      false exit
    else
      true exit
    endif
  endif
  ;; (* and (+
  dup 0x2a28 = over 0x2b28 = or if
    ;; nrot 2drop par_tibstate_rpop
    nrot (parse-good-comment?) if
      2drop r>tibstate
      ;; ( 2chars )
      ;; skip to the word
      parse-skip-blanks
      ;; skip two word chars
      tib-getch tib-getch 2drop
      0x2a28 = if comment-multiline else comment-multiline-nested endif
      false exit
    endif
  endif
  ;; not a comment
  drop rdrop-tibstate
  true
; (hidden)

: PARSE-NAME  ( -- addr count )
  bl 1 (word)  ;; parse, skip leading delimiters
;

: PARSE-NAME-EX  ( -- addr count )
  parse-name-adv-comments if
    begin (parse-name-ex) until
  else
    parse-name
  endif
;
