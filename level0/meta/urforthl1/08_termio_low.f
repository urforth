;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

$value "STDIN-FD"  0
$value "STDOUT-FD" 1
$value "STDERR-FD" 2

$variable "(EMIT-COL)" 0
(hidden)


code: (RESET-EMIT-COL)  ( -- )
(hidden)
  ld    dword [pfa "(emit-col)"],0
  urnext
endcode

code: (EMIT-FIX-COL-CODEBLOCK)
;; TOS: ch
;; EAX: dead
(hidden)
(codeblock)
emit_fix_col_subr:
  cp    cl,10
  jr    nz,@f
  ld    dword [pfa "(emit-col)"],0
  ret
@@:
  cp    cl,13
  jr    nz,@f
  ld    dword [pfa "(emit-col)"],0
  ret
@@:
  cp    cl,9
  jr    nz,@f
  ld    eax,[pfa "(emit-col)"]
  or    eax,7
  inc   eax
  ld    [pfa "(emit-col)"],eax
  ret
@@:
  cp    cl,8
  jr    nz,@f
  cp    dword [pfa "(emit-col)"],0
  jr    z,.skipdec
  dec   dword [pfa "(emit-col)"]
.skipdec:
  ret
@@:
  cp    cl,32
  jr    c,@f
  inc   dword [pfa "(emit-col)"]
@@:
  ret
endcode

code: (EMIT-FIX-COL)  ( ch -- ch )
(hidden)
  call  emit_fix_col_subr
  urnext
endcode

code: (EMIT)   ( ch -- )
  call  emit_fix_col_subr
;;
;;  cp    cl,9
;;  jr    nz,.normal
;;  ;; tab
;;.tabloop:
;;  ld    cl,32
;;  call  .normal
;;  ld    eax,[pfa "(emit-col)"]
;;  inc   eax
;;  ld    [pfa "(emit-col)"],eax
;;  and   al,7
;;  jr    nz,.tabloop
;;  pop   TOS
;;  urnext
;;.normal:
;;
  push  EIP
  push  TOS
  mov   eax,4    ;; function
  mov   ebx,[pfa "stdout-fd"]
  mov   ecx,esp  ;; address
  mov   edx,ebx  ;; length
  syscall
  pop   TOS
  pop   EIP
.done:
  pop   TOS
  urnext
endcode

code: (BELL)  ( -- )
  push  EIP
  push  TOS
  ld    eax,7
  push  eax
  mov   eax,4    ;; function
  mov   ebx,[pfa "stdout-fd"]
  mov   ecx,esp  ;; address
  mov   edx,1    ;; length
  syscall
  pop   eax
  pop   TOS
  pop   EIP
  urnext
endcode

: (CR)  ( -- )
  10 (emit)
;

: (ENDCR)  ( -- )
  (emit-col) @ if (cr) endif
;

code: (?ENDCR)  ( -- )
  push  TOS
  cp    dword [pfa "(emit-col)"],0
  setnz cl
  movzx ecx,cl
  urnext
endcode


$if 0
code: (TYPE)
;; ( addr length -- )
  test  TOS,0x80000000
  jr    nz,.fucked_length
  or    TOS,TOS
  jr    z,.fucked_length

  push  TOS
  push  esi
  ld    esi,[esp+4*2]
  ld    eax,TOS
.fixcol_loop:
  ld    cl,[esi]
  call  emit_fix_col_subr
  inc   esi
  dec   eax
  jr    nz,.fixcol_loop
  pop   esi
  pop   TOS

  mov   edx,TOS
  mov   eax,4
  mov   ebx,[pfa "stdout-fd"]
  pop   ecx
  push  EIP
  syscall
  pop   EIP
  pop   TOS
  urnext
.fucked_length:
  pop   TOS
  pop   TOS
  urnext
endcode
$endif


;; returns -1 on EOF, or [0..255]
code: (GETCH)  ( -- ch )
  push  TOS
  xor   eax,eax
  push  eax
  mov   eax,3     ;; read
  ld    ebx,[pfa "stdin-fd"]
  mov   ecx,esp   ;; address
  mov   edx,1     ;; length
  push  EIP
  syscall
  pop   EIP
  pop   TOS       ;; read char
  cp    eax,1
  ld    eax,-1
  cmovnz TOS,eax
  urnext
endcode
