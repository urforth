format ELF executable 3
entry urforth_entry_point

;; this is defined by the target compiler
;;urforth_code_base_addr equ $

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
URFORTH_DEBUG equ 1
URFORTH_EXTRA_STACK_CHECKS equ 0

;; various compile-time options
WLIST_HASH_BITS equ 6

;; size in items
DSTACK_SIZE equ 65536
RSTACK_SIZE equ 65536

TTYLOW_ALLOW_BUFFERED equ 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; register usage:
;;   ebp: return stack
;;   esp: data stack
;;   esi: instruction pointer
;;   ecx: TOS
;;
;; direction flag must NOT be set!
db "Alice!"

;; let it always be here
$if URFORTH_DEBUG
  ;; breakpoint
urforth_next_ptr: dd urforth_next_normal
  ;; normal next
urforth_next_normal:
  jp    eax
$endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some sanity checks
$if WLIST_HASH_BITS < 0
  $error "invalid WLIST_HASH_BITS: should be [0..8]!"
$endif

$if WLIST_HASH_BITS > 8
  $error "invalid WLIST_HASH_BITS: should be [0..8]"
$endif

;; sorry! one day i'll implement shifts in asm evaluator
$if WLIST_HASH_BITS = 1
WLIST_HASH_MASK equ 0x01
WLIST_HASH_CELLS equ 2
$endif

$if WLIST_HASH_BITS = 2
WLIST_HASH_MASK equ 0x03
WLIST_HASH_CELLS equ 4
$endif

$if WLIST_HASH_BITS = 3
WLIST_HASH_MASK equ 0x07
WLIST_HASH_CELLS equ 8
$endif

$if WLIST_HASH_BITS = 4
WLIST_HASH_MASK equ 0x0f
WLIST_HASH_CELLS equ 16
$endif

$if WLIST_HASH_BITS = 5
WLIST_HASH_MASK equ 0x1f
WLIST_HASH_CELLS equ 32
$endif

$if WLIST_HASH_BITS = 6
WLIST_HASH_MASK equ 0x3f
WLIST_HASH_CELLS equ 64
$endif

$if WLIST_HASH_BITS = 7
WLIST_HASH_MASK equ 0x7f
WLIST_HASH_CELLS equ 128
$endif

$if WLIST_HASH_BITS = 8
WLIST_HASH_MASK equ 0xff
WLIST_HASH_CELLS equ 256
$endif

WLIST_HASH_BYTES equ WLIST_HASH_CELLS*4


;; create "FORTH" vocabulary header first, because why not
$vocabheader "FORTH" forth_wordlist_vocid

$include "00_startup.f"
$include "01_segfault_handler.f"
$include "02_debugger.f"
$include "03_do_codeblocks.f"
$include "05_base_vars.f"
$include "07_syscall.f"
$include "08_termio_low.f"
$include "10_litbase.f"
$include "14_stack.f"
$include "16_peekpoke.f"
$include "17_dp.f"
$include "18_temp_pool.f"
$include "19_dbg_info.f"
$include "20_math_base.f"
$include "22_math_compare.f"
$include "24_math_muldiv.f"
$include "26_math_dbl_minimal.f"
$include "28_print_number.f"
$include "30_count_str.f"
$include "32_str_hash.f"
$include "40_termio_high.f"
$include "42_tib.f"
$include "46_os_face.f"
$include "50_error.f"
$include "52_exceptions.f"
$include "60_wordlist.f"
$include "62_create_vocbase.f"
$include "63_vocorder.f"
$include "64_parse.f"
$include "70_compiler_helpers.f"
$include "72_compiler_mid.f"
$include "82_tload.f"
$include "84_interpret.f"
$include "86_save.f"
$include "88_main_startup.f"

  db "Miriel!"

;; WARNING! it cannot be "equ"!
urforth_code_end:
