;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: =  ( n0 n1 -- n0=n1? )
  pop   eax
  ;; eax=n0
  ;; TOS=n1
  cp    eax,TOS
  setz  cl
  movzx TOS,cl
  urnext
endcode

code: <>  ( n0 n1 -- n0<>n1? )
  pop   eax
  ;; eax=n0
  ;; TOS=n1
  cp    eax,TOS
  setnz cl
  movzx TOS,cl
  urnext
endcode

code: <  ( n0 n1 -- n<n1? )
  pop   eax
  ;; eax=n0
  ;; TOS=n1
  cmp   eax,TOS
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: >  ( n0 n1 -- n0>n1? )
  pop   eax
  ;; eax=n0
  ;; TOS=n1
  cmp   eax,TOS
  setg  cl
  movzx TOS,cl
  urnext
endcode

code: <=  ( n0 n1 -- n<-n1? )
  pop   eax
  ;; eax=n0
  ;; TOS=n1
  cmp   eax,TOS
  setle cl
  movzx TOS,cl
  urnext
endcode

code: >=  ( n0 n1 -- n0>=n1? )
  pop   eax
  ;; eax=n0
  ;; TOS=n1
  cmp   eax,TOS
  setge cl
  movzx TOS,cl
  urnext
endcode

code: U<  ( u0 u1 -- u0<u1? )
  pop   eax
  ;; eax=u0
  ;; TOS=u1
  cmp   eax,TOS
  ;;  C: u0<u1
  ;; NC: u0>=u1
  mov   TOS,0
  adc   TOS,0
  urnext
endcode

code: U>  ( u0 u1 -- u0>u1? )
  pop   eax
  ;; eax=u0
  ;; TOS=u1
  cmp   TOS,eax
  ;;  C: u1<u0 (or u0>u1)
  ;; NC: u1>=u0
  mov   TOS,0
  adc   TOS,0
  urnext
endcode

code: U<=  ( u0 u1 -- u0<=u1? )
  pop   eax
  ;; eax=u0
  ;; TOS=u1
  cmp   TOS,eax
  ;;  C: u1<u0
  ;; NC: u1>=u0 (or u0<=u1)
  mov   TOS,1
  sbb   TOS,0
  urnext
endcode

code: U>=  ( u0 u1 -- u0>u1? )
  pop   eax
  ;; eax=u0
  ;; TOS=u1
  cmp   eax,TOS
  ;;  C: u0<u1
  ;; NC: u0>=u1
  mov   TOS,1
  sbb   TOS,0
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: 0= ; ( n0 -- n=0? )
  cp    TOS,1
  ;;  C: TOS==0
  ;; NC: TOS!=0
  ld    TOS,0
  adc   TOS,0
  urnext
endcode

code: 0<>  ( n0 -- n<>0? )
  cp    TOS,1
  ;;  C: TOS==0
  ;; NC: TOS!=0
  ld    TOS,1
  sbb   TOS,0
  urnext
endcode

code: 0<  ( n0 -- n<0? )
  cp    TOS,0
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: 0>  ( n0 -- n>0? )
  cp    TOS,0
  setg  cl
  movzx TOS,cl
  urnext
endcode

code: 0<=  ( n0 -- n<=0? )
  cp    TOS,0
  setle cl
  movzx TOS,cl
  urnext
endcode

code: 0>=  ( n0 -- n>=0? )
  cp    TOS,0
  setge cl
  movzx TOS,cl
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; u0 >= u1 and u0 <= u2
code: BOUNDS?  ( u0 u1 u2 -- flag )
  pop   ebx
  pop   edx
  cmp   edx,TOS
  ld    TOS,0
  jr    a,@f
  cmp   ebx,edx
  jr    a,@f
  inc   TOS
@@:
  urnext
endcode
