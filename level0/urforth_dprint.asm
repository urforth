;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; utilities
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

macro save_all_registers {
  pushad
  pushfd
}

macro restore_all_registers {
  popfd
  popad
}


macro dprint [str] {
common
  local nstart
  local nend
  jmp   nend
  nstart = $
forward
  db    str
common
  nend = $

  save_all_registers
  mov   eax,4
  mov   ebx,2
  mov   ecx,nstart
  mov   edx,nend-nstart
  syscall
  restore_all_registers
}

macro dprint_char_al {
  save_all_registers
  push  eax
  mov   eax,4
  mov   ebx,2
  mov   ecx,esp
  mov   edx,1
  syscall
  pop   eax
  restore_all_registers
}

macro dprint_char chr {
  if chr eq al
    dprint_char_al
  else
    push  eax
    mov   al,chr
    dprint_char_al
    pop   eax
  end if
}

macro dprint_cr {
  dprint_char 10
}

macro dprint_hex_al {
  save_all_registers
  mov   cl,al
  shr   al,4
  Nibble2Hex
  dprint_char_al
  mov   al,cl
  Nibble2Hex
  dprint_char_al
  restore_all_registers
}

macro dprint_hex_eax {
  local xloop
  save_all_registers
  mov   ecx,4
xloop:
  rol   eax,8
  dprint_hex_al
  loop  xloop
  restore_all_registers
}

macro dprint_hex reg {
  if reg eq eax
    dprint_hex_eax
  else
    push  eax
    mov   eax,reg
    dprint_hex_eax
    pop   eax
  end if
}

macro dprint_strz_esi {
  local shit,shitex
  push  esi
  push  eax
  pushfd
  cld
shit:
  lodsb
  or    al,al
  jr    z,shitex
  dprint_char_al
  jr    shit
shitex:
  popfd
  pop   eax
  pop   esi
}

macro dprint_strz_at reg {
  if reg eq esi
    dprint_strz_esi
  else
    push  esi
    mov   esi,reg
    dprint_strz_esi
    pop   esi
  end if
}
