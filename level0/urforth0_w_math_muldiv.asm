;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; remainder has the same sign as the original number


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "*",imul
;; ( n0 n1 -- n0*n1 )
  pop   eax
  xchg  eax,TOS
  imul  TOS,eax
  urnext
urword_end

urword_code "/",idiv
;; ( n0 n1 -- n0/n1 )
  pop   eax
  ; TOS=n1
  ; EAX=n0
  jecxz .zero
  cdq
  idiv  TOS
  mov   TOS,eax
.zero:
  urnext
urword_end

urword_code "MOD",imod
;; ( n0 n1 -- n0%n1 )
  pop   eax
  ; TOS=n1
  ; EAX=n0
  jecxz .zero
  cdq
  idiv  TOS
  mov   TOS,edx
.zero:
  urnext
urword_end

urword_code "/MOD",idivmod
;; ( n0 n1 -- n0%n1 n0/n1 )
  pop   eax
  ; TOS=n1
  ; EAX=n0
  jecxz .zero
  cdq
  idiv  TOS
  push  edx
  mov   TOS,eax
  urnext
.zero:
  push  TOS
  urnext
urword_end

urword_code "U*",umul
;; ( n0 n1 -- n0*n1 )
  pop   eax
  xchg  eax,TOS
  mul   TOS
  mov   TOS,eax
  urnext
urword_end

urword_code "U/",udiv
;; ( n0 n1 -- n0/n1 )
  pop   eax
  ; TOS=n1
  ; EAX=n0
  jecxz .zero
  xor   edx,edx
  div   TOS
  mov   TOS,eax
.zero:
  urnext
urword_end

urword_code "UMOD",umod
;; ( n0 n1 -- n0%n1 )
  pop   eax
  ; TOS=n1
  ; EAX=n0
  jecxz .zero
  xor   edx,edx
  div   TOS
  mov   TOS,edx
.zero:
  urnext
urword_end

urword_code "U/MOD",udivmod
;; ( n0 n1 -- umod ures )
  pop   eax
  ; TOS=n1
  ; EAX=n0
  jecxz .zero
  xor   edx,edx
  div   TOS
  push  edx
  mov   TOS,eax
  urnext
.zero:
  push  TOS
  urnext
urword_end
