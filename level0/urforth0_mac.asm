;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro ur_bp {
  if URFORTH_DEBUG
    urcall urforth_bp
  end if
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urnext {
  lodsd
  if URFORTH_DEBUG
  jmp  dword [urforth_next_ptr]
  else
  jmp   eax
  end if
}

macro rdrop {
  add   ERP,4
}

macro rpush reg* {
  sub   ERP,4
  mov   dword [ERP],reg
}

macro rpop reg* {
  mov   reg,dword [ERP]
  add   ERP,4
}

macro rpeek reg* {
  mov   reg,dword [ERP]
}


; *mem destroys EAX
macro rpushmem reg {
  sub   ERP,4
  mov   eax,reg
  mov   [ERP],eax
}

macro rpopmem reg {
  mov   eax,[ERP]
  mov   reg,eax
  add   ERP,4
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word header format:
;; note than name hash is ALWAYS calculated with ASCII-uppercased name
;; bfa points to next bfa or to 0
;; before nfa, we have such "hidden" fields
;;   dd dbgptr    ; can be 0 if debug info is missing
;;   dd sfa       ; points after the last word byte
;;   dd bfa       ; next word in hashtable bucket; it is always here, even if hashtable is turned off
;; lfa:
;;   dd lfa       ; previous word LFA or 0 (lfa link points here)
;;   dd namehash  ; it is always here, and always calculated, even if hashtable is turned off
;; nfa:
;;   dd flags-and-name-len   ; see below
;;   db name      ; no terminating byte here
;;   db namelen   ; yes, name length again, so CFA->NFA will work fast and stable
;;   machine code follows
;;   here we usually have CALL to word handler
;;   0xE8, 4-byte displacement
;;   (displacement is calculated from here)
;;
;; first word cell contains combined name length (low byte), argtype and flags (other bytes)
;; layout:
;;   db namelen
;;   db argtype
;;   dw flags
;;
;; i.e. we have 16 bits for flags, and 256 possible argument types. why not.
;;
;; flags:
;;  bit 0: immediate
;;  bit 1: smudge
;;  bit 2: noreturn
;;  bit 3: hidden
;;  bit 4: codeblock
;;  bit 5: vocabulary
;;
;;  argument type:
;;    0: none
;;    1: branch address
;;    2: cell-size numeric literal
;;    3: cell-counted string
;;    4: cfa of another word
;;    5: cblock

FLAG_IMMEDIATE = 0x0001
FLAG_SMUDGE    = 0x0002
FLAG_NORETURN  = 0x0004
; hidden words won't be shown by WORDS
; also, hidden words won't be found unless current vocabulary is the top context one
FLAG_HIDDEN    = 0x0008
; code blocks are there because there should be no data without word header
; for code block type, argtype contains real type (data/mc/etc)
FLAG_CODEBLOCK = 0x0010
FLAG_VOCAB = 0x0020

WARG_NONE = 0
WARG_BRANCH = 1
WARG_LIT = 2
WARG_C4STRZ = 3
WARG_CFA = 4
WARG_CBLOCK = 5


urforth_word_count = 0
urforth_last_word_nfa = 0
urforth_last_word_dfa = 0
urforth_last_word_sfa = 0
urforth_last_word_ffa = 0   ; flags
urforth_last_word_tfa = 0   ; argtypes
urforth_last_word_lfa = 0
urforth_last_word_bfa = 0
urforth_last_word_hash_fld = 0
urforth_last_word_cfa = 0
urforth_last_word_forth = 0
urforth_last_word_name equ ""

;; hashatble buckets
urforth_forth_hash_offset = 0x100000
virtual at urforth_forth_hash_offset
urforth_forth_hashtable::
  db  WLIST_HASH_BYTES dup(0)
  ;db  4096 dup(0)
end virtual


; creates word header
macro urword_header name*,cfalabel*,flags {
  local nlen
  local nstart
  local tmp
  ;; for hash calculation and hashtable update
  local bpos,bcount,hash,high,b
  local xbfa

  ; check for proper word termination
  if urforth_last_word_sfa <> 0
    load tmp dword from urforth_last_word_sfa
    if tmp = 0
      display "***",10
      display "*** UNTERMINATED WORD: "
      display urforth_last_word_name
      display 10,"***",10
      err "previous word wasn't properly terminated!"
    end if
  end if

  ; align headers (because why not?)
  if URFORTH_ALIGN_HEADERS
    while ($ and 3) <> 0
      db 0
    end while
  end if

  ; dfa
  urforth_last_word_dfa = $
  dd  0  ; debuginfo ptr

  ; sfa
  urforth_last_word_sfa = $
  dd  0  ; word size in bytes (will be patched later)

  ; bfa
  urforth_last_word_bfa = $
  dd 0

  ; lfa
  dd urforth_last_word_lfa
  urforth_last_word_lfa = $-4

  ; name hash (elfhash)
  urforth_last_word_hash_fld = $
  dd 0  ; will be patched below

  ; nfa
  urforth_last_word_nfa = $
  ; name length
  db  nlen
  ; argtype
  urforth_last_word_tfa = $
  db  0
  ; flags
  urforth_last_word_ffa = $
  if flags eq
    dw  0
  else
    dw  flags
  end if
  nstart = $
  db  name
  nlen = $-nstart
  db  nlen

  ; calculate name hash (elfhash)
  hash = 0
  bpos = 0
  bcount = nlen
  while bcount <> 0
    load b byte from nstart+bpos
    ;; uppercase it (this is how UrForth does it) -- this also distorts other chars, but who cares
    b = b and 0xdf
    bpos = bpos+1
    bcount = bcount-1
    hash = ((hash shl 4)+b) and 0xffffffff
    high = hash and 0xf0000000
    hash = hash xor (high shr 24)
    hash = hash and (high xor 0xffffffff)
  end while
  store dword hash at urforth_last_word_hash_fld

  ;; link to FORTH hashtable
  if WLIST_HASH_BITS
      ;display_hex hash
      ;display " -> "
    ; fold hash: 32->16
    high = (hash shr 16) and 0xffff
    hash = hash and 0xffff
    hash = (hash+high) and 0xffff
    ; fold hash: 16->8
    high = (hash shr 8) and 0xff
    hash = hash and 0xff
    hash = (hash+high) and 0xff
    hash = hash and WLIST_HASH_MASK
      ;display_hex hash
      ;display "  ",name
      ;display 10
    ; update bfa
    hash = hash*4
    load b dword from urforth_forth_hashtable:urforth_forth_hash_offset+hash
      ;display "  ofs="
      ;display_hex hash
      ;display "; value="
      ;display_hex b
      ;display 10
    store dword urforth_last_word_bfa at urforth_forth_hashtable:urforth_forth_hash_offset+hash
    store dword b at urforth_last_word_bfa
  end if

  ; cfa
  label fword_#cfalabel
  urforth_last_word_cfa = $

  urforth_word_count = urforth_word_count+1
  urforth_last_word_name equ name
  urforth_last_word_forth = 0
}

; terminate array word with this
; WARNING! no checks!
macro urword_end_array {
  local tmp
  ; check for proper word termination
  if urforth_last_word_sfa
    load tmp dword from urforth_last_word_sfa
    store dword $ at urforth_last_word_sfa
  else
    err "tried to end nothing!"
  end if
  ;store dword $ at urforth_last_word_sfa
}

; terminate each word with this
macro urword_end {
  local tmp
  ; check for proper word termination
  if urforth_last_word_sfa
    load tmp dword from urforth_last_word_sfa
    if tmp <> 0
      display "***",10
      display "*** DOUBLE-TERMINATED WORD: "
      display urforth_last_word_name
      display 10,"***",10
      err "previous word was already terminated!"
    end if
    if urforth_last_word_forth
      urforth_last_word_forth = 0
      ;; do not add EXIT to noreturn words
      load tmp word from urforth_last_word_ffa
      if tmp and FLAG_NORETURN
        ;; do nothing
        ;display "*** NORETURN WORD '"
        ;display urforth_last_word_name
        ;display "'",10
      else
        load tmp dword from $-4
        if tmp <> fword_exit
          ;display "*** AUTOADDED EXIT TO '"
          ;display urforth_last_word_name
          ;display "'",10
          dd fword_exit
        end if
      end if
    end if
    store dword $ at urforth_last_word_sfa
    urcond_check_balance
  else
    err "tried to end nothing!"
  end if
  ;store dword $ at urforth_last_word_sfa
}

;; this macro will be used to tell which words
;; are used by asm words
macro urword_uses [wlist] {
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; toggle IMMEDIATE flag on the last word
macro urword_toggle_flag flag* {
  local b
  load b word from urforth_last_word_ffa
  b = b xor flag
  store word b at urforth_last_word_ffa
}

macro urword_immediate { urword_toggle_flag FLAG_IMMEDIATE }
macro urword_smudge { urword_toggle_flag FLAG_SMUDGE }
macro urword_noreturn { urword_toggle_flag FLAG_NORETURN }
macro urword_hidden { urword_toggle_flag FLAG_HIDDEN }
macro urword_codeblock { urword_toggle_flag FLAG_CODEBLOCK }
macro urword_vocab { urword_toggle_flag FLAG_VOCAB }

macro urword_argtype atype* {
  store byte atype at urforth_last_word_tfa
}

macro urword_arg_branch { urword_argtype WARG_BRANCH }
macro urword_arg_lit { urword_argtype WARG_LIT }
macro urword_arg_c4strz { urword_argtype WARG_C4STRZ }
macro urword_arg_cfa { urword_argtype WARG_CFA }
macro urword_arg_cblock { urword_argtype WARG_CBLOCK }


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urcall wname {
  dd  fword_#wname
}

macro urlit num {
  dd  fword_lit
  dd  num
}

macro urlit_cfa wname {
  dd  fword_cfalit
  dd  fword_#wname
}

macro urcompile wname {
  dd  fword_par_compile
  dd  fword_#wname
}

macro urto wname {
  dd  fword_littopush
  dd  fword_#wname
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urword_code name*,cfalabel* {
  urword_header name,cfalabel,0
}

macro urword_forth name*,cfalabel* {
  urword_code name,cfalabel
  call   fword_par_urforth_nocall_doforth
  urforth_last_word_forth = 1
}

macro urword_alias name*,cfalabel*,othername* {
  urword_header name,cfalabel,0
  jmp   fword_#othername
  urword_end
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urword_const name*,cfalabel*,value* {
  urword_code name,cfalabel
  call  fword_par_urforth_nocall_doconst
  label fconst_#cfalabel#_data
  dd  value
  urword_end
}


macro urword_var name*,cfalabel*,value {
  urword_code name,cfalabel
  call  fword_par_urforth_nocall_dovar
  label fvar_#cfalabel#_data
  if value eq
    ; do nothing
  else
    dd value
    urword_end
  end if
}


macro urword_value name*,cfalabel*,value {
  urword_code name,cfalabel
  call  fword_par_urforth_nocall_dovalue
  label fval_#cfalabel#_data
  if value eq
    ; do nothing
  else
    dd value
    urword_end
  end if
}


macro urword_defer name*,cfalabel*,wordlabel* {
  urword_code name,cfalabel
  call  fword_par_urforth_nocall_dodefer
  label fdefer_#cfalabel#_data
  dd  fword_#wordlabel
  urword_end
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urcstr [str] {
common
  local nlen
  local nstart
  dd  nlen
  nstart = $
forward
  db  str
common
  nlen = $-nstart
  db  0
}


macro urprint [str] {
  urcall  pardottype
common
  local nlen
  local nstart
  dd    nlen
  nstart = $
forward
  db    str
common
  nlen = $-nstart
  db  0
}

macro urprintnl [str] {
  urcall  pardottype
common
  local nlen
  local nstart
  dd    nlen
  nstart = $
forward
  db    str
  db    10
common
  nlen = $-nstart
  db  0
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urforth [sequence] {
  common irps urfword, sequence
  \{
    local status
    define status 0
    local nstart,nlen
    match =0 =, , status urfword \\{
      dd  fword_comma
      define status 1
    \\}
    match =0 =. , status urfword \\{
      dd  fword_dot
      define status 1
    \\}
    match =0 =+ , status urfword \\{
      dd  fword_add
      define status 1
    \\}
    match =0 =- , status urfword \\{
      dd  fword_sub
      define status 1
    \\}
    match =0 =* , status urfword \\{
      dd  fword_mul
      define status 1
    \\}
    match =0 =/ , status urfword \\{
      dd  fword_div
      define status 1
    \\}
    ;match =0 == , status urfword \\{
    ;  dd  fword_equal
    ;  define status 1
    ;\\}
    match =0 =@ , status urfword \\{
      dd  fword_peek
      define status 1
    \\}
    match =0 =! , status urfword \\{
      dd  fword_poke
      define status 1
    \\}
    match =0 =xxx_or , status xxx_\#\urfword \\{
      dd  fword_or
      define status 1
    \\}
    match =0 =xxx_and , status xxx_\#\urfword \\{
      dd  fword_and
      define status 1
    \\}
    match =0 =xxx_xor , status xxx_\#\urfword \\{
      dd  fword_xor
      define status 1
    \\}
    match =0 =xxx_not , status xxx_\#\urfword \\{
      dd  fword_not
      define status 1
    \\}
    match =0 =xxx_c! , status xxx_\#\urfword \\{
      dd  fword_cpoke
      define status 1
    \\}
    match =0 =xxx_c@ , status xxx_\#\urfword \\{
      dd  fword_cpeek
      define status 1
    \\}
    match =0 =xxx_recurse , status xxx_\#\urfword \\{
      dd  urforth_last_word_cfa
      define status 1
    \\}
    match =0 =xxx_word , status xxx_\#\urfword \\{
      err "`WORD` is obsolete!"
    \\}
    ;match =0 == , status urfword \\{
    ;  dd  fword_equal
    ;  define status 1
    ;\\}
    if status = 0
      if urfword eqtype ""
        ; compile string
        urcstr \urfword
      else if defined(fword_\#\urfword)
        match =0 other , status urfword \\{
          dd  fword_\#\urfword
          define status 1
        \\}
      else if urfword eqtype 0
        ; compile numeric literal
        dd  fword_lit
        dd  \urfword
      else
        match =0  any , status urfword \\{
          err "wtf?!" urfword
        \\}
      end if
    end if
  \}
}

macro UF [args] {
  common urforth args
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro urbrn lbl {
  urcall  branch
  dd      lbl
}

macro ur0brn lbl {
  urcall  0branch
  dd      lbl
}

macro urtbrn lbl {
  urcall  tbranch
  dd      lbl
}
