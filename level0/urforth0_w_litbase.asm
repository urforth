;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "LIT",lit
urword_arg_lit
urword_hidden
  push  TOS
  lodsd
  mov   TOS,eax
  urnext
urword_end

urword_code "LITSTR",strlit
urword_arg_c4strz
urword_hidden
  push  TOS
  lodsd
  push  EIP
  mov   TOS,eax
  add   EIP,eax
  inc   EIP  ;; skip trailing zero
  urnext
urword_end

urword_code "LITCFA",cfalit
urword_arg_cfa
urword_hidden
  push  TOS
  lodsd
  mov   TOS,eax
  urnext
urword_end

urword_code "LITCBLOCK",cblocklit
urword_arg_cblock
urword_hidden
  ; next cell is continue address
  ; leave next next cell address as cfa
  push  TOS
  lodsd
  ld    TOS,EIP
  ld    EIP,eax
  urnext
urword_end

; used in "TO"
urword_code "LITTO!",littopush
urword_arg_cfa
urword_hidden
;; ( value -- )
  lodsd
  add   eax,5   ; skip cfa
  ld    [eax],TOS
  pop   TOS
  urnext
urword_end

; used in "+TO"
urword_code "LIT+TO!",litaddtopush
urword_arg_cfa
urword_hidden
;; ( value -- )
  lodsd
  add   eax,5   ; skip cfa
  add   [eax],TOS
  pop   TOS
  urnext
urword_end

; used in "-TO"
urword_code "LIT-TO!",litsubtopush
urword_arg_cfa
urword_hidden
;; ( value -- )
  lodsd
  add   eax,5   ; skip cfa
  sub   [eax],TOS
  pop   TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "BRANCH",branch
urword_arg_branch
urword_hidden
  lodsd
  mov   EIP,eax
  urnext
urword_end

urword_code "0BRANCH",0branch
urword_arg_branch
urword_hidden
  lodsd
  or    TOS,TOS
  pop   TOS
  jnz   @f
  mov   EIP,eax
@@:
  urnext
urword_end

urword_code "TBRANCH",tbranch
urword_arg_branch
urword_hidden
  lodsd
  or    TOS,TOS
  pop   TOS
  jz    @f
  mov   EIP,eax
@@:
  urnext
urword_end

;; branch if positive or zero
urword_code "+0BRANCH",p0branch
urword_arg_branch
urword_hidden
  lodsd
  cp    TOS,0
  pop   TOS
  jr    l,@f
  ld    EIP,eax
@@:
  urnext
urword_end

;; branch if negative or zero
urword_code "-0BRANCH",m0branch
urword_arg_branch
urword_hidden
  lodsd
  cp    TOS,0
  pop   TOS
  jr    g,@f
  ld    EIP,eax
@@:
  urnext
urword_end

;; branch if positive (not zero)
urword_code "+BRANCH",pbranch
urword_arg_branch
urword_hidden
  lodsd
  cp    TOS,0
  pop   TOS
  jr    le,@f
  ld    EIP,eax
@@:
  urnext
urword_end

;; branch if negative (not zero)
urword_code "-BRANCH",mbranch
urword_arg_branch
urword_hidden
  lodsd
  cp    TOS,0
  pop   TOS
  jr    ge,@f
  ld    EIP,eax
@@:
  urnext
urword_end

;; used in "CASE": drops additional value if branch is NOT taken
urword_code "0BRANCH-DROP",0branch_drop
urword_arg_branch
urword_hidden
  lodsd
  or    TOS,TOS
  pop   TOS
  jnz   @f
  mov   EIP,eax
  urnext
@@:
  ; branch not taken, drop one more data value
  pop   TOS
  urnext
urword_end

;; if two values on the stack are equal, drop them, and take a branch
;; if they aren't equal, do nothing
urword_code "?DO-BRANCH",qdo_branch
urword_arg_branch
urword_hidden
  lodsd
  cp    TOS,[esp]
  jr    nz,@f
  ;; values are equal, drop them, and take a branch
  pop   TOS
  pop   TOS
  mov   EIP,eax
@@:
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "EXECUTE",execute
  mov   eax,TOS
  pop   TOS
  jmp   eax
urword_end

urword_code "OVERRIDE-EXECUTE",override_execute
;; ( ... xtoken -- ... )
  mov   eax,TOS
  pop   TOS
  rpush EIP
  ld    EIP,eax
  urnext
urword_end

urword_code "(EXECUTE-INTR-CMPL)",par_execute_intr_cmpl
urword_hidden
;; ( intrcfa cmplcfa -- )
  pop   eax
  ;; TOS: cmplcfa
  ;; EDX: intrcfa
  cp    dword [fvar_state_data],0
  cmovnz eax,TOS
  pop   TOS
  jmp   eax
urword_end


urword_code "EXIT",exit
urword_noreturn
  rpop  EIP
  urnext
urword_end

urword_code "0?EXIT",q0exit
;; ( flag -- )
  or    TOS,TOS
  pop   TOS
  jr    nz,@f
  rpop  EIP
@@:
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(DO)",par_do
urword_hidden
;; ( limit start -- | limit counter )
;; loops from start to limit-1
  ; ANS loops
  pop   eax
  ld    edx,0x80000000
  sub   edx,eax
  add   TOS,edx
  sub   ERP,4+4
  ld    [ERP+4],edx  ; 80000000h-to
  ld    [ERP],TOS    ; 80000000h-to+from
  pop   TOS
  urnext
urword_end

urword_code "(+LOOP)",par_ploop
urword_hidden
;; ( delta -- | limit counter )
  ; ANS loops
  add   TOS,[ERP]
  jr    o,.done
  ; next iteration
  ld    [ERP],TOS
  lodsd
  mov   EIP,eax
  pop   TOS
  urnext
.done:
  add   esi,4
  add   ERP,4+4
  pop   TOS
  urnext
urword_end

urword_code "(LOOP)",par_loop
urword_hidden
urword_uses par_ploop
;; ( -- | limit counter )
  push  TOS
  ld    TOS,1
  jr    fword_par_ploop
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; as i moved high-level compiler out of the core, we should do it there
;urword_alias "LEAVE",leave,break
;urword_immediate

urword_code "UNLOOP",unloop
;; k8
;; ( | limit counter -- )
;; removes loop arguments from return stack
;; can be used as: UNLOOP EXIT
;; "BREAK" compiles this word before branching out of the loop
  add   ERP,4+4
  urnext
urword_end

urword_code "I",i
;; ( -- counter )
  push  TOS
  ld    TOS,[ERP]
  sub   TOS,[ERP+4]
  urnext
urword_end

urword_code "J",j
;; ( -- counter )
  push  TOS
  ld    TOS,[ERP+8]
  sub   TOS,[ERP+8+4]
  urnext
urword_end
