;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


urword_defer "EMIT",emit,paremit
;; ( ch -- )

urword_defer "CR",cr,parcr
;; ( -- )

urword_defer "BELL",bell,parbell
;; ( -- )

urword_defer "ENDCR",endcr,parendcr
;; ( -- )

;; should "ENDCR" do "CR"?
urword_defer "?ENDCR",qendcr,parqendcr
;; ( -- flag )

urword_defer "RESET-EMITCOL",reset_emitcol,par_reset_emitcol
;; ( -- )

urword_defer "KEY",key,pargetch
;; ( -- ch )


urword_forth "SAFE-EMIT",safeemit
;; ( ch -- )
  UF 0xff and
  UF dup 32 less
  ur_if
    ;; < 32: allow tab, cr, lf
    UF dup 9 equal
    UF over 10 equal or
    UF over 13 equal or
    ur_ifnot
      UF drop 63
    ur_endif
  ur_else
    UF dup 127 equal
    ur_if
      UF drop 63
    ur_endif
  ur_endif
  UF emit
urword_end


urword_forth "TYPE-WITH",par_type_with
;; ( addr length emitcfa -- )
  UF rpush
  ur_begin
    UF dup 0great
  ur_while
    UF swap dup cpeek rpeek execute 1inc swap 1dec
  ur_repeat
  UF 2drop rdrop
urword_end

urword_forth "TYPE",type
;; ( addr length -- )
  UF cfalit emit par_type_with
urword_end

urword_forth "SAFE-TYPE",safetype
;; ( addr length -- )
  UF cfalit safeemit par_type_with
urword_end

urword_forth "SPACE",space
;; ( -- )
  UF 32 emit
urword_end

urword_forth "SPACES",spaces
;; ( n -- )
  UF dup 0great
  ur_if
    UF 0
    ur_do
      UF space
    ur_loop
  ur_else
    UF drop
  ur_endif
urword_end

urword_forth "TYPE-ASCIIZ",type_asciiz
;; ( addr -- )
;; type asciiz string
  UF zcount type
urword_end

urword_forth "SAFE-TYPE-ASCIIZ",safetype_asciiz
;; ( addr -- )
;; type asciiz string
  UF zcount safetype
urword_end
