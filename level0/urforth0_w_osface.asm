;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ensure that the string at EDI with length in ECX is asciiz
;; returns new EDI if necessary
;; doesn't modify other registers (except flags)
;; but sets carry if ECX is invalid
;; rejects empty strings
;; may allocate temp pool, so take care of releasing it
;;
urword_code "(DONT-CALL-OSFACE-CSTR)",par_dontcall_osface_cstr
urword_uses par_no_call_temp_pool_subs
urword_hidden
ensure_asciiz_edi_ecx:
  or    ecx,ecx
  jr    z,.fucked
  test  ecx,0x80000000
  jr    nz,.fucked
  push  edi
  add   edi,ecx
  cp    byte [edi],0
  pop   edi
  jr    nz,@f
  or    ecx,ecx   ; reset carry flag
  ret
@@:
  push  eax
  push  ecx
  push  esi
  ld    esi,edi
  ld    eax,ecx
  inc   eax       ; for trailing zero
  call  urpool_alloc
  push  edi       ; save new address
  rep   movsb     ; copy string
  xor   al,al     ; store trailing zero
  stosb
  pop   edi       ; restore new address (we'll return it)
  pop   esi       ; restore ESI (because we promised to not change it)
  pop   ecx       ; restore length (just in case)
  pop   eax       ; restore EAX (because we promised to not change it)
  or    ecx,ecx   ; reset carry flag
  ret
.fucked:
  scf
  ret
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(DLCLOSE)",par_dlclose
;; ( handle-or-0 -- )
  or    TOS,TOS
  jr    z,fword_par_dlclose_fucked
  ; push registers
  push  EIP
  push  ERP
  ; call
  push  TOS
  call  [elfimp_dlclose]
  add   esp,4   ; remove args
fword_par_dlclose_fucked:
  pop   TOS
  urnext
urword_end

urword_code "(DLOPEN)",par_dlopen
;; ( addr count -- handle-or-0 )
urword_uses par_dontcall_osface_cstr
  pop   edi
  ; save pool mark
  call  urpool_mark
  push  eax
  call  ensure_asciiz_edi_ecx
  jr    c,fword_par_dlopen_fucked
  ; save registers
  push  EIP
  push  ERP
  ; call function
  ld    eax,1+256+8   ; RTLD_LAZY+RTLD_GLOBAL+RTLD_DEEPBIND
  push  eax
  push  edi
  call  [elfimp_dlopen]
  add   esp,4*2       ; remove arguments
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
fword_par_dlopen_fucked:
  xor   TOS,TOS
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
urword_end

urword_code "(DLSYM)",par_dlsym
;; ( addr count handle -- address-or-0 )
urword_uses par_dontcall_osface_cstr
  ld    FREEREG,TOS   ; save handle
  pop   ecx
  pop   edi
  ; save pool mark
  call  urpool_mark
  push  eax
  call  ensure_asciiz_edi_ecx
  jr    c,fword_par_dlsym_fucked
  ; save registers
  push  EIP
  push  ERP
  ; call function
  push  edi
  push  FREEREG
  call  [elfimp_dlsym]
  add   esp,4*2       ; remove arguments
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
fword_par_dlsym_fucked:
  xor   TOS,TOS
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
urword_end

urword_const "RTLD_DEFAULT",rtld_default,0
urword_const "RTLD_NEXT",rtld_next,-1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(FCLOSE)",par_fclose
;; ( fd -- flag )
  ; save registers
  push  EIP
  push  ERP
  ; syscall
  ld    eax,6
  ld    ebx,TOS
  syscall
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(FOPEN)",par_fopen
;; ( addr count flags mode -- fd-or-minusone )
urword_uses par_dontcall_osface_cstr
  pop   edx   ; flags
  pop   eax   ; count
  pop   edi   ; addr
  xchg  eax,ecx
  ; EDX: flags
  ; EAX: mode
  ; ECX: count
  ; EDI: addr
  push  eax
  ; save pool mark
  call  urpool_mark
  xchg  eax,[esp]
  call  ensure_asciiz_edi_ecx
  jr    c,@f
  ; save registers
  push  EIP
  push  ERP
  ; syscall
  ld    ebx,edi
  ld    ecx,edx
  ld    edx,eax
  ld    eax,5
  syscall
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
@@:
  ld    TOS,-1
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
urword_end

urword_code "(FREAD)",par_fread
;; ( addr count fd -- count )
  pop   edx   ; count
  pop   edi   ; addr
  ; save registers
  push  EIP
  push  ERP
  ; syscall
  ld    eax,3
  ld    ebx,TOS
  ld    ecx,edi
  syscall
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(FWRITE)",par_fwrite
;; ( addr count fd -- count )
  pop   edx   ; count
  pop   edi   ; addr
  ; save registers
  push  EIP
  push  ERP
  ; syscall
  ld    eax,4
  ld    ebx,TOS
  ld    ecx,edi
  syscall
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_code "(LSEEK)",par_lseek
;; ( ofs whence fd -- res )
  ld    ebx,ecx
  pop   edx
  pop   ecx
  ; save registers
  push  EIP
  push  ERP
  ; syscall
  ld    eax,19
  ;ld    ebx,fd
  ;ld    ecx,offt
  ;ld    edx,whence
  syscall
  ; restore registers
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
urword_end


urword_const "O-RDONLY",o_rdonly,0
urword_const "O-WRONLY",o_wronly,1
urword_const "O-RDWR",o_rdwr,2

urword_const "O-CREAT",    o_creat,    0x000040  ; 0100
urword_const "O-EXCL",     o_excl,     0x000080  ; 0200
urword_const "O-NOCTTY",   o_noctty,   0x000100  ; 0400
urword_const "O-TRUNC",    o_trunc,    0x000200  ; 01000
urword_const "O-APPEND",   o_append,   0x000400  ; 02000
urword_const "O-NONBLOCK", o_nonblock, 0x000800  ; 04000
urword_const "O-DSYNC",    o_dsync,    0x001000  ; 010000
urword_const "O-SYNC",     o_sync,     0x101000  ; 04010000
urword_const "O-RSYNC",    o_rsync,    0x101000  ; 04010000
urword_const "O-DIRECTORY",o_directory,0x010000  ; 0200000
urword_const "O-NOFOLLOW", o_nofollow, 0x020000  ; 0400000
urword_const "O-CLOEXEC",  o_cloexec,  0x080000  ; 02000000

urword_const "O-ASYNC",    o_async,    0x002000  ; 020000
urword_const "O-DIRECT",   o_direct,   0x004000  ; 040000
urword_const "O-LARGEFILE",o_largefile,0x008000  ; 0100000
urword_const "O-NOATIME",  o_noatime,  0x040000  ; 01000000
urword_const "O-PATH",     o_path,     0x200000  ; 010000000
urword_const "O-TMPFILE",  o_tmpfile,  0x410000  ; 020200000
urword_const "O-NDELAY",   o_ndelay,   0x000800  ; 04000

urword_const "O-CREATE-WRONLY-FLAGS",o_create_wronly_flags,(1 or 0x000040 or 0x000200)
urword_const "O-CREATE-MODE-NORMAL",o_create_mode_normal,(0x100 or 0x080 or 0x020 or 0x004)

urword_const "(SEEK-SET)",seek_set,0
urword_const "(SEEK-CUR)",seek_cur,1
urword_const "(SEEK-END)",seek_end,2

urword_const "S-ISUID",s_isuid,0x800  ; 04000
urword_const "S-ISGID",s_isgid,0x400  ; 02000
urword_const "S-ISVTX",s_isvtx,0x200  ; 01000
urword_const "S-IRUSR",s_irusr,0x100  ; 0400
urword_const "S-IWUSR",s_iwusr,0x080  ; 0200
urword_const "S-IXUSR",s_ixusr,0x040  ; 0100
urword_const "S-IRWXU",s_irwxu,0x1c0  ; 0700
urword_const "S-IRGRP",s_irgrp,0x020  ; 0040
urword_const "S-IWGRP",s_iwgrp,0x010  ; 0020
urword_const "S-IXGRP",s_ixgrp,0x008  ; 0010
urword_const "S-IRWXG",s_irwxg,0x038  ; 0070
urword_const "S-IROTH",s_iroth,0x004  ; 0004
urword_const "S-IWOTH",s_iwoth,0x002  ; 0002
urword_const "S-IXOTH",s_ixoth,0x001  ; 0001
urword_const "S-IRWXO",s_irwxo,0x007  ; 0007


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
struc stat {
  .st_dev        rd 1
  .st_ino        rd 1
  .st_mode       rw 1
  .st_nlink      rw 1
  .st_uid        rw 1
  .st_gid        rw 1
  .st_rdev       rd 1
  .st_size       rd 1
  .st_blksize    rd 1
  .st_blocks     rd 1
  .st_atime      rd 1
  .st_atime_nsec rd 1
  .st_mtime      rd 1
  .st_mtime_nsec rd 1
  .st_ctime      rd 1
  .st_ctime_nsec rd 1
  .__unused4     rd 1
  .__unused5     rd 1
  .statsize      rd 0
}

virtual at 0
  statshit stat
end virtual


;; is regular file?
urword_code "(FILE?)",par_is_file
;; ( addr count -- flag )
urword_uses par_dontcall_osface_cstr
  pop   edi   ; addr
  ; ECX: count
  ; EDI: addr
  push  eax   ; dummy
  ; save pool mark
  call  urpool_mark
  xchg  eax,[esp]
  call  ensure_asciiz_edi_ecx
  jr    c,@f
  ; save registers
  push  EIP
  push  ERP
  ; alloc stat struct
  sub   esp,statshit.statsize
  ; syscall
  ld    eax,106
  ld    ebx,edi
  ld    ecx,esp
  syscall
  ; get mode to ecx
  movzx TOS,word [esp+statshit.st_mode]
  ; drop stat struct
  add   esp,statshit.statsize
  ; restore registers
  pop   ERP
  pop   EIP
  or    eax,eax
  jr    nz,@f
  ; check flags
  and   TOS,0x8000  ; 0100000, S_IFREG
  jr    z,@f
  ; restore pool
  pop   eax
  call  urpool_release
  ld    TOS,1
  urnext
@@:
  xor   TOS,TOS
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
urword_end

;; is directory?
urword_code "(DIR?)",par_is_dir
;; ( addr count -- flag )
urword_uses par_dontcall_osface_cstr
  pop   edi   ; addr
  ; ECX: count
  ; EDI: addr
  push  eax   ; dummy
  ; save pool mark
  call  urpool_mark
  xchg  eax,[esp]
  call  ensure_asciiz_edi_ecx
  jr    c,@f
  ; save registers
  push  EIP
  push  ERP
  ; alloc stat struct
  sub   esp,statshit.statsize
  ; syscall
  ld    eax,106
  ld    ebx,edi
  ld    ecx,esp
  syscall
  ; get mode to ecx
  movzx TOS,word [esp+statshit.st_mode]
  ; drop stat struct
  add   esp,statshit.statsize
  ; restore registers
  pop   ERP
  pop   EIP
  or    eax,eax
  jr    nz,@f
  ; check flags
  and   TOS,0x4000  ; 0040000, S_IFDIR
  jr    z,@f
  ; restore pool
  pop   eax
  call  urpool_release
  ld    TOS,1
  urnext
@@:
  xor   TOS,TOS
  ; restore pool
  pop   eax
  call  urpool_release
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; get seconds since epoch
urword_code "SYS-TIME",sys_time
;; ( -- seconds )
  push  TOS
  push  EIP
  push  ERP
  ld    eax,13   ; sys_time
  xor   ebx,ebx  ; result only in eax
  syscall
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
urword_end

urword_const "CLOCK_REALTIME",clock_realtime,0
urword_const "CLOCK_MONOTONIC",clock_mono,1
urword_const "CLOCK_PROCESS_CPUTIME_ID",clock_process_cpu,2
urword_const "CLOCK_THREAD_CPUTIME_ID",clock_thread_cpu,3
urword_const "CLOCK_MONOTONIC_RAW",clock_mono_raw,4
urword_const "CLOCK_REALTIME_COARSE",clock_realtime_coarse,5
urword_const "CLOCK_MONOTONIC_COARSE",clock_mono_coarse,6
urword_const "CLOCK_BOOTTIME",clock_boot,7
urword_const "CLOCK_REALTIME_ALARM",clock_realtime_alarm,8
urword_const "CLOCK_BOOTTIME_ALARM",clock_boot_alarm,9

urword_const "NANOSECONS-PER-SECOND",nano_per_second,1000000000
urword_const "NANOSECONS-PER-MSEC",nano_per_msec,1000000

;; get seconds and nanoseconds (since some random starting point)
urword_code "SYS-CLOCK-GETTIME",sys_clock_gettime
;; ( clockid -- seconds nanoseconds )
  push  EIP
  push  ERP
  sub   esp,4+4  ; timespec
  ld    eax,265  ; sys_clock_gettime
  ld    ebx,TOS  ; clockid
  ld    ecx,esp
  syscall
  or    eax,eax
  jr    nz,.error
  ld    eax,[esp]    ; seconds
  ld    TOS,[esp+4]  ; nanoseconds
  add   esp,4+4
  pop   ERP
  pop   EIP
  push  eax
  urnext
.error:
  add   esp,4+4
  pop   ERP
  pop   EIP
  xor   TOS,TOS
  push  TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_const "(SYS-GETTICKCOUNT-START-SECS)",sys_gettickccount_start_secs,0
urword_hidden

urword_forth "(SYS-GETTICKCOUNT-INIT)",sys_gettickcount_init
urword_hidden
;; ( -- )
  UF clock_mono sys_clock_gettime drop
  UF dup 1 great
  ur_if
    UF 1dec
  ur_endif
  urlit fconst_sys_gettickccount_start_secs_data
  UF !
urword_end


urword_forth "SYS-GETTICKCOUNT",sys_gettickcount
  UF clock_mono sys_clock_gettime
  UF nano_per_msec udiv
  UF swap sys_gettickccount_start_secs - 1000 umul
  UF +
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "(CINVOKE)",par_cinvoke
;; ( ... argcount addr -- res )
;; call c function
  rpush TOS
  pop   eax
  rpush eax
  call  eax
  mov   TOS,eax
  rpop  edx
  shl   edx,2
  add   esp,edx
  rpop  TOS
  push  TOS
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; urword_const "MAP-FILE"            ,0x00000000
;; urword_const "MAP-SHARED"          ,0x00000001
;; urword_const "MAP-PRIVATE"         ,0x00000002
;; urword_const "MAP-SHARED_VALIDATE" ,0x00000003
;; urword_const "MAP-TYPE"            ,0x0000000f
;; urword_const "MAP-FIXED"           ,0x00000010
;; urword_const "MAP-ANON"            ,0x00000020
;; urword_const "MAP-ANONYMOUS"       ,0x00000020
;; urword_const "MAP-NORESERVE"       ,0x00004000
;; urword_const "MAP-GROWSDOWN"       ,0x00000100
;; urword_const "MAP-DENYWRITE"       ,0x00000800
;; urword_const "MAP-EXECUTABLE"      ,0x00001000
;; urword_const "MAP-LOCKED"          ,0x00002000
;; urword_const "MAP-POPULATE"        ,0x00008000
;; urword_const "MAP-NONBLOCK"        ,0x00010000
;; urword_const "MAP-STACK"           ,0x00020000
;; urword_const "MAP-HUGETLB"         ,0x00040000
;; urword_const "MAP-SYNC"            ,0x00080000
;; urword_const "MAP-FIXED_NOREPLACE" ,0x00100000

;; urword_const "PROT-NONE"  ,0
;; urword_const "PROT-READ"  ,1
;; urword_const "PROT-WRITE" ,2
;; urword_const "PROT-EXEC"  ,4
urword_const "PROT-R/W",mmap_const_prot_rw,3

;; urword_const "PROT-GROWSDOWN" ,0x01000000
;; urword_const "PROT-GROWSUP"   ,0x02000000

;; WARNING! this can return negative for high addresses
;;          therefore, check for error like this:
;;          err -4095 0 within, or use "MMAP-ERROR?"
;; it is ok to use "0" as modeflags, they will be set to private
urword_code "MMAP",mmap  ;;( size protflags -- addr true // error false )
  ;; fix flags (we always doing anon private alloc)
  xor   ebx,ebx  ;; address
  ld    edx,TOS  ;; protflags
  pop   ecx      ;; size
  push  EIP
  push  ERP
  ld    esi,0x0000_0022  ;; we always doing anon private alloc
  xor   ebp,ebp  ;; offset, it is ignored, but why not
  xor   edi,edi  ;; fd (-1)
  dec   edi
  ld    eax,192
  syscall
  pop   ERP
  pop   EIP
  push  eax
  cp    eax,0xfffff000
  setc  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "MUNMAP",munmap  ;;( addr size -- res )
  pop   ebx
  push  EIP
  push  ERP
  ld    eax,91
  syscall
  pop   ERP
  pop   EIP
  ld    TOS,eax
  urnext
urword_end
