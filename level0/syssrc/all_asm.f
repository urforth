;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; loads libraries with assembler code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

.LIB-START" extended kernel words (asm)"

" math-single.f" tload
" math-double.f" tload
" math-extprec.f" tload

" print-double.f" tload

" str-hash.f" tload
" str-ext-asm.f" tload

.LIB-END
