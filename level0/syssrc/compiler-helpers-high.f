;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; compiler helper words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1 already has it in the kernel
forth:urforth-level [IFNOT]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: c4strz-unaligned,  ( addr count -- )
  dup unused 2u/ u>= ERR-STRING-TOO-LONG ?error
  ;; check if the string is at "here" (just in case)
  over here here cell+ 1- bounds? if  ;; move forward to make room for counter
    dup >r here cell+ swap move
    here cell+ r>
  endif
  dup cell+ 1+ n-allot  ( addr count destaddr )
  2dup ! cell+  ;; length
  2dup + 0c!    ;; terminating zero byte
  swap move     ;; string itself
  \ align-here    ;; always align
;

: c4strz,  ( addr count -- )  c4strz-unaligned, ;

: c1strz-unaligned,  ( addr count -- )
  dup 255 u> ERR-STRING-TOO-LONG ?error
  ;; check if the string is at "here" (just in case)
  over here = if  ;; move forward to make room for counter
    dup >r here 1+ swap move
    here 1+ r>
  endif
  dup 2+ n-allot  ( addr count destaddr )
  2dup c! 1+    ;; length
  2dup + 0c!    ;; terminating zero byte
  swap move     ;; string itself
  \ align-here    ;; always align
;

: c1strz,  ( addr count -- ) c1strz-unaligned, ;

;; byte-counted
: custom-c1sliteral,  ( addr count cfa -- )
  ?dup if
    over 255 u> ERR-STRING-TOO-LONG ?error
    >r  ;; save cfa
    ;; check if the string is at "here" (just in case)
    over here here cell+ bounds? if  ;; move forward to make room for "litstr" and other things
      dup >r here cell+ swap move
      here cell+ r>
    endif
    r> compile,  ;; literal word
  endif
  c1strz,
;

: custom-c4sliteral,  ( addr count cfa -- )
  ?dup if
    over forth:unused 2u/ u>= ERR-STRING-TOO-LONG ?error
    >r  ;; save cfa
    ;; check if the string is at "here" (just in case)
    over here here 2 +cells 1- bounds? if  ;; move forward to make room for "litstr" and other things
      dup >r here 2 +cells swap move
      here 2 +cells r>
    endif
    r> compile,  ;; literal word
  endif
  c4strz,
;

: SLITERAL  ( addr count )
  state @ if
    ?dup if
      2dup here 2 +cells swap move ;; copy to HERE+8 (reserving room for strlit and counter)
      compile LITSTR
      dup , allot 0 c, drop
    else
      compile LITSTR 0 , 0 c,
    endif
  endif
; immediate

[ENDIF]
