;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; ANS extended precision maths
;;
;; partially based on the code from Robert Smith
;; (from COLDFORTH Version 0.8, GPL)
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tri negate
: TNEGATE  ( t1lo t1mid t1hi -- t2lo t2mid t2hi )
  bitnot >r
  bitnot >r
  bitnot 0 0xffffffff 0xffffffff d+ s>d r> 0 d+
  r> +
;

;; unsigned double by an unsigned integer to give a tri result
: UT*  ( ulo uhi u -- utlo utmid uthi )
  swap >r dup >r
  um* 0 r> r> um* d+
  ;; alternative version
  ;; tuck um* 2swap um* swap >r 0 d+ r> nrot
;

;; divide a tri number by an integer
: UT/  ( utlo utmid uthi n -- d1 )
  dup >r um/mod nrot r> um/mod nip swap
;

;; double by a integer to give a tri result
: MT*  ( lo hi n -- tlo tmid thi )
  dup 0< if
    abs over 0< if
      >r dabs r> ut*
    else
      ut* tnegate
    endif
  else
    over 0< if
      >r dabs r> ut* tnegate
    else
      ut*
    endif
  endif
;

;; multiply d1 by n1 producing the triple-cell intermediate result t
;; divide t by +n2 giving the double-cell quotient d2
;; an ambiguous condition exists if +n2 is zero or negative,
;; or the quotient lies outside of the range of a double-precision
;; signed integer
: M*/  ( d1 n1 +n2 -- d2 )
  >r mt* dup 0<
  if
    tnegate r> ut/ dnegate
  else
    r> ut/
  endif
;
