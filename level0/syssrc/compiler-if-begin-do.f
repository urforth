;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; high-level structured programming words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; alas, the kernel neeeds them
\ 0 var (CSP)  (hidden)
\ : !CSP  ( -- )  sp@ (csp) ! ;
\ : ?CSP ( -- )   sp@ (csp) @ - err-unfinished-definition ?error ;

\ : ?COMP  ( -- )  state @ 0= err-compilation-only ?error ;
\ : ?EXEC  ( -- )  state @ err-execution-only ?error ;

;; CSP check for loops
: ?CSP-LOOP  ( -- )
  sp@ (csp) @ u> err-unpaired-conditionals ?error
;

: ?PAIRS  ( n1 n2 -- )
  <> err-unpaired-conditionals ?error
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?ANY-PAIR  ( id v0 v1 -- )
  >r over <>
  swap r> <>
  and err-unpaired-conditionals ?error
;

: ?PAIRS-ANY-KEEPID  ( id v0 v1 -- id )
  >r over <>  ;; ( id v0<>id | v1 )
  over r> <>  ;; ( id v0<>id v1<>id )
  and err-unpaired-conditionals ?error
;

;; unused
;; doesn't error out, returns flag instead
\ : ?OK-PAIR  ( id qid -- true // id false )  over = dup if nip endif ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; usage:
;;  compile 0branch
;;  (mark>)
;;  ...
;;  (resolve>)
;;
;;  (<mark)
;;  ...
;;  compile branch
;;  (<resolve)

;; write "branch to destaddr" address to addr
\ hidden:: (BRANCH-ADDR!)  ( destaddr addr -- )  !  ;
alias ! (BRANCH-ADDR!)  (hidden)
alias @ (BRANCH-ADDR@)  (hidden)


;; reserve room for branch address, return addr suitable for "(RESOLVE-J>)"
hidden:: (MARK-J>)  ( -- addr )
  here 0 ,
;

;; compile "forward jump" from address to HERE
;; addr is the result of "(MARK-J>)"
hidden:: (RESOLVE-J>)  ( addr -- )
  here swap (branch-addr!)
;


;; return addr suitable for "(<J-RESOLVE)"
hidden:: (<J-MARK)  ( -- addr )
  here
;

;; patch "forward jump" address to HERE
;; addr is the result of "(<J-MARK)"
hidden:: (<J-RESOLVE)  ( addr -- )
  cell n-allot (branch-addr!)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; yeah, the kernel has its own implementations, so we can load this in runtime

;; each of these has one argument
1 constant (CTLID-IF)  (hidden)
2 constant (CTLID-ELSE)  (hidden)

3 constant (CTLID-BEGIN)  (hidden)
4 constant (CTLID-WHILE)  (hidden)

5 constant (CTLID-CASE)  (hidden)
6 constant (CTLID-OF)  (hidden)
7 constant (CTLID-ENDOF)  (hidden)
8 constant (CTLID-OTHERWISE)  (hidden)

9 constant (CTLID-DO)  (hidden)
10 constant (CTLID-DO-BREAK)  (hidden)
11 constant (CTLID-DO-CONTINUE)  (hidden)

12 constant (CTLID-CBLOCK)  (hidden)
13 constant (CTLID-CBLOCK-INTERP)  (hidden)

14 constant (CTLID-?DO)  (hidden)

663 constant (CTLID-SC-COLON)  (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: IF
  ?comp
  compile 0branch
  (mark-j>)
  (CTLID-IF)
; immediate

: IFNOT
  ?comp
  compile tbranch
  (mark-j>)
  (CTLID-IF)
; immediate

;; if negative (not zero)
: -IF
  ?comp
  compile +0branch
  (mark-j>)
  (CTLID-IF)
; immediate

;; if positive (not zero)
: +IF
  ?comp
  compile -0branch
  (mark-j>)
  (CTLID-IF)
; immediate

;; if negative or zero
: -0IF
  ?comp
  compile +branch
  (mark-j>)
  (CTLID-IF)
; immediate

;; if positive or zero
: +0IF
  ?comp
  compile -branch
  (mark-j>)
  (CTLID-IF)
; immediate

: ELSE
  ?comp (CTLID-IF) ?pairs
  compile branch
  (mark-j>)
  swap (resolve-j>)
  (CTLID-ELSE)
; immediate

: ENDIF
  ?comp
  (CTLID-IF) (CTLID-ELSE) ?any-pair
  (resolve-j>)
; immediate

alias endif THEN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; you can use as many "while" blocks as you want to
;; any loop can be finished with AGAIN/REPEAT/UNTIL
;; "BREAK" and "CONTINUE" cannot be used inside conditionals yet
;;

: BEGIN
  ?comp
  (<j-mark)
  (CTLID-BEGIN)
; immediate

;; there might be a lot of "while" blocks, pop them all
;; compile jump back to begin
hidden:: (END-BEGIN)  ( pairs... jumpcfa -- )
  ;; this is done recursively, because this way i can get rid of `par_resolve_jfwd_over_branch`
  ;; also, we don't have working loops at this point, so recursion is the only choice ;-)
  ?csp-loop
  over (CTLID-BEGIN) =
  if
    compile,
    (CTLID-BEGIN) ?pairs
    (<j-resolve)
  else
    swap
    (CTLID-WHILE) ?pairs
    swap >r recurse r>
    (resolve-j>)
  endif
;

;; repeats while the condition is false
: UNTIL
  ?comp
  ['] 0branch
  (end-begin)
; immediate

;; repeats while the condition is true
: NOT-UNTIL
  ?comp
  ['] tbranch
  (end-begin)
; immediate

: AGAIN
  ?comp
  ['] branch
  (end-begin)
; immediate

alias AGAIN REPEAT

hidden:: (COMP-WHILE)  ( jumpcfa )
  ?comp
  >r (CTLID-BEGIN) (CTLID-WHILE) ?pairs-any-keepid r>
  compile,
  (mark-j>)
  (CTLID-WHILE)
;

: WHILE
  ['] 0branch
  (comp-while)
; immediate

: NOT-WHILE
  ['] tbranch
  (comp-while)
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; alas, i had to use one global var
;; <>0: drop when we'll see CASE
;; set to 0 by (CTLID-OF) or (CTLID-OTHERWISE)
0 value (B/C-CASE-DROP)  (hidden)

;; workhorse for break/continue
;; type:
;;   0: break
;;   1: continue
hidden:: (BREAK/CONTINUE)  ( type )
  ?comp
  0 2>r  ;; type and counter
  ;; drop on case by default
  1 (to-compile-time-only) (b/c-case-drop)
  begin
    ?csp-loop
    ;; check for valid ctlid
    dup (CTLID-DO-CONTINUE) > over (CTLID-IF) < or ERR-INVALID-BREAK-CONT ?error
    ;; while not begin and not do
    dup (CTLID-BEGIN) = over (CTLID-DO) = or not
  while
    ;; move to rstack
      ;; DEBUG
      ;; 2dup pardottype "SAVE: ctlid: " dot  pardottype " addr: " udot cr
    ;; process case:
    ;;   if we're in (CTLID-CASE) or in (CTLID-ENDOF), compile DROP
    dup (CTLID-CASE) = if
      (b/c-case-drop) if
          ;; DEBUG
          ;; pardottype " compiling DROP (" dup dot pardottype ")" cr
        compile drop
      endif
      ;; drop on next case by default
      1 (to-compile-time-only) (b/c-case-drop)
    endif
    dup (CTLID-OF) = over (CTLID-OTHERWISE) = or
    if
      ;; don't drop on next case by default
      0 (to-compile-time-only) (b/c-case-drop)
    endif
    2r> 2swap >r >r 2+ 2>r
  repeat
  ;; return stack contains saved values and counter
  dup (CTLID-DO) =
  if
    ;; do...loop
    ;; check type
    1 rpick  ;; peek the type
    if
        ;; DEBUG
        ;; pardottype "DO/LOOP: continue" cr
      ;; coninue: jump to (LOOP)
      compile branch
      (mark-j>)
      (CTLID-DO-CONTINUE)
    else
      ;; break: drop do args, jump over (LOOP)
        ;; DEBUG
        ;; pardottype "DO/LOOP: break" cr
      compile unloop  ;; remove do args
      compile branch
      (mark-j>)
      (CTLID-DO-BREAK)
    endif
  else
    (CTLID-BEGIN) ?pairs
    ;; check type
    1 rpick  ;; i.e. peek the type
    if
      ;; coninue
        ;; DEBUG
        ;; pardottype "BEGIN: continue" cr
      dup            ;; we still need the address
      compile branch
      (<j-resolve)
      (CTLID-BEGIN)    ;; restore ctlid
    else
      ;; break
        ;; DEBUG
        ;; pardottype "BEGIN: break" cr
      (CTLID-BEGIN)    ;; restore ctlid
      compile branch
      (mark-j>)
      (CTLID-WHILE)
    endif
  endif

  ;; move saved values back to the data stack
  r> rdrop  ;; drop type
    ;; DEBUG
    ;; dup pardottype "RESTORE " dot pardottype "items" cr
  begin
    ?dup
  while
    r> swap 1 -
  repeat
    ;; DEBUG
    ;; dup . over udot cr
;

: CONTINUE
  1 (break/continue)
; immediate

: BREAK
  0 (break/continue)
; immediate

;; this has to be here
alias BREAK LEAVE


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; data stack:
;;   0 (CTLID-CASE)
;;     addr (CTLID-OF) -- when in "OF"
;;     addr (CTLID-ENDOF) -- when "ENDOF" compiled
;;     0 (CTLID-OTHERWISE) -- when "OTHERWISE" compiled
;; note that "(CTLID-ENDOF)"s will be accumulated, and resolved in "ENDCASE"
;;

hidden:: (X-OF)  ( ... word-to-compare )
  ?comp
  >r  ;; save XOF args
  (CTLID-CASE) (CTLID-ENDOF) ?pairs-any-keepid   ;; we should be in normal CASE
  \ compile over  ;; special compare words will do this for us
  r> compile,  ;; comparator
  compile 0branch-drop
  (mark-j>)
  (CTLID-OF)
;

hidden:: (END-CASE)
  dup (CTLID-OTHERWISE) = if
    ;; "otherwise", no drop needed
    (CTLID-OTHERWISE) ?pairs
    0 ?pairs  ;; check dummy argument
  else
    ;; no "otherwise", compile DROP
    compile drop
  endif
  ;; patch branches
  begin
    ?csp-loop
    dup (CTLID-CASE) <>
  while
    (CTLID-ENDOF) ?pairs
    (resolve-j>)
  repeat
  (CTLID-CASE) ?pairs
  0 ?pairs  ;; check dummy argument
;

: CASE
  ?comp
  0 (CTLID-CASE)  ;; with dummy argument
; immediate

: OF  ['] forth:(of=) (x-of) ; immediate
: NOT-OF  ['] forth:(of<>) (x-of) ; immediate
: <OF  ['] forth:(of<) (x-of) ; immediate
: <=OF  ['] forth:(of<=) (x-of) ; immediate
: >OF  ['] forth:(of>) (x-of) ; immediate
: >=OF  ['] forth:(of>=) (x-of) ; immediate
: U<OF  ['] forth:(of-U<) (x-of) ; immediate
: U<=OF  ['] forth:(of-U<=) (x-of) ; immediate
: U>OF  ['] forth:(of-U>) (x-of) ; immediate
: U>=OF  ['] forth:(of-U>=) (x-of) ; immediate
: &OF  ['] forth:(of-and) (x-of) ; immediate
: AND-OF  ['] forth:(of-and) (x-of) ; immediate
: ~AND-OF  ['] forth:(of-~and) (x-of) ; immediate
: WITHIN-OF  ['] forth:(of-within) (x-of) ; immediate
: UWITHIN-OF  ['] forth:(of-uwithin) (x-of) ; immediate
: BOUNDS-OF  ['] forth:(of-bounds) (x-of) ; immediate

: ENDOF
  ?comp (CTLID-OF) ?pairs
  compile branch
  (mark-j>)
  swap (resolve-j>)
  (CTLID-ENDOF)
; immediate

: OTHERWISE
  (CTLID-CASE) (CTLID-ENDOF) ?pairs-any-keepid
  0 (CTLID-OTHERWISE)
; immediate

: ENDCASE
  ?comp (end-case)
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DO
  ?comp
  compile (do)
  (<j-mark)
  (CTLID-DO)
; immediate

hidden:: (END-LOOP)  ( endloopcfa )
  ;; this is done recursively, because this way i can get rid of `par_resolve_jfwd_over_branch`
  ?csp-loop
  over (CTLID-DO) =
  if
    compile,
    (CTLID-DO) ?pairs
    (<j-resolve)
    ;; resolve ?DO jump, if it is there
    dup (CTLID-?DO) = if drop (resolve-j>) endif
  else
    ;; "continue" should be compiled before recursion, and "break" after it
    swap
    dup (CTLID-DO-CONTINUE) =
    if
      ;; patch "continue" branch
      (CTLID-DO-CONTINUE) ?pairs
      swap (resolve-j>)
      recurse
    else
      (CTLID-DO-BREAK) ?pairs
      swap >r recurse r>
      ;; here, loop branch already compiled
      (resolve-j>)
    endif
  endif
;

: LOOP
  ?comp
  ['] (loop) (end-loop)
; immediate

: +LOOP
  ?comp
  ['] (+loop) (end-loop)  ;; +)
; immediate

: ?DO
  ?comp
  compile ?do-branch
  (mark-j>)
  (CTLID-?DO)
  [compile] do
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: [:
  state @ if
    ;; compiling
    compile LITCBLOCK
    (mark-j>)
    (CTLID-CBLOCK)
  else
    ;; interpreting, use temporary dp
    dp-temp @ err-temp-here-already ?error
    ;; compile to temporary area
    dp @ 42666 + dp-temp !
    state 1!
    dp-temp @
    !csp
    (CTLID-CBLOCK-INTERP)
  endif
  ['] (URFORTH-DOFORTH-CODEBLOCK) (call,)
; immediate

: ;]
  ?comp
  dup (CTLID-CBLOCK-INTERP) =
  if
    ;; used from the interpreter
    (CTLID-CBLOCK-INTERP) ?pairs
    ?csp
    compile exit
    ;; `(CTLID-CBLOCK-INTERP)` argument is cblock CFA
    state 0!
    dp-temp 0!
  else
    ;; inside a word
    (CTLID-CBLOCK) ?pairs
    compile exit
    (resolve-j>)
  endif
; immediate
