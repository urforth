;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; loads libraries without assembler code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

" compiler-misc-colon.f" tload
" compiler-helpers-low.f" tload
" compiler-misc-low.f" tload
" compiler-if-begin-do.f" tload
" compiler-misc-semi.f" tload
" condcomp.f" tload

.LIB-START" extended kernel words (forth)"

" compiler-helpers-high.f" tload
" compiler-misc-high.f" tload

" create-and-voc.f" tload
" create-and-voc-vocabs.f" tload
" create-and-voc-replace-override.f" tload

" tib.f" tload

" within.f" tload
" structs.f" tload
" str-ext.f" tload
" brk-buffer.f" tload
" ans-math-shit.f" tload

" error-table.f" tload

" c4s.f" tload

.LIB-END
