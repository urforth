;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; string words with assembler code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: COMPARE  ( c-addr1 u1 c-addr2 u2 -- n )
  ld    edx,TOS
  pop   edi
  pop   ecx
  xchg  esi,[esp]
  cp    ecx,edx
  pushfd
  jr    c,.lbl1
  mov   ecx,edx
.lbl1:
  jecxz .lbl2
  repz cmpsb
  jr    z,.lbl2
  pop   ebx
  mov   eax,0xffffffff
  jr    c,.lbl3
  neg   eax
  jr    .lbl3
.lbl2:
  xor   eax,eax
  popfd
  jr    z,.lbl3
  mov   eax,0xffffffff
  jr    c,.lbl3
  neg   eax
.lbl3:
  ld    TOS,eax
  pop   esi
  urnext
endcode

\ ' COMPARE disasm-word


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search the string specified by c-addr1 u1 for the string specified by c-addr2 u2.
;; if flag is true, a match was found at c-addr3 with u3 characters remaining.
;; if flag is false there was no match and c-addr3 is c-addr1 and u3 is u1.
code: SEARCH  ( c-addr1 u1 c-addr2 u2 -- c-addr3 u3 flag )
  push  TOS
  pushr EIP
  mov   ebx,[esp]
  or    ebx,ebx
  jr    z,.zerou2
  mov   edx,[esp+8]
  mov   edi,[esp+12]
  add   edx,edi
.scanloop:
  mov   esi,[esp+4]
  lodsb
  mov   ecx,edx
  sub   ecx,edi
  jecxz .notfound
  repnz scasb
  jr    nz,.notfound   ; no first char found
  cmp   ebx,1
  jr    z,.found       ; a pattern is one char, and it was found
  mov   ecx,ebx
  dec   ecx
  mov   eax,edx
  sub   eax,edi
  cmp   eax,ecx
  jr    c,.notfound    ; the rest is shorter than a pattern
  push  edi
  repz  cmpsb
  pop   edi
  jr    nz,.scanloop
.found:
  dec   edi            ; exact match found
  sub   edx,edi
  mov   [esp+12],edi
  mov   [esp+8],edx
.zerou2:
  mov   eax,1
  jr    .done
.notfound:
  xor   eax,eax
.done:
  add   esp,8
  ld    TOS,eax
  popr  EIP
  urnext
endcode

\ ' SEARCH disasm-word


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: LOCASE-CHAR  ( ch -- ch )
  and   TOS,0xff
  cp    cl,'A'
  jr    c,@f
  cp    cl,'Z'+1
  jr    nc,@f
  add   cl,32
@@:
  urnext
endcode

code: LOCASE-STR  ( addr count -- )
  pop   edi
  test  TOS,0x80000000
  jr    nz,.done
  or    TOS,TOS
  jr    z,.done
.strloop:
  ld    al,[edi]
  cp    al,'A'
  jr    c,@f
  cp    al,'Z'+1
  jr    nc,@f
  add   byte [edi],32
@@:
  inc   edi
  loop  .strloop
.done:
  pop   TOS
  urnext
endcode
