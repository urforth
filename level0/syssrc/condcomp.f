;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; conditional compilation
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ([CND]-S=CI?EXIT)  ( addr1 count1 addr2 count2 -- true[exit the caller] // addr1 count1 )
  2over s=ci if rdrop 2drop true exit endif
; (hidden)

: ([IF]?)  ( addr count -- addr count false // true )
  s" [IF]" ([cnd]-s=ci?exit)
  s" [IFNOT]" ([cnd]-s=ci?exit)
  s" [IFDEF]" ([cnd]-s=ci?exit)
  s" [IFNDEF]" ([cnd]-s=ci?exit)
  false
; (hidden)

: ([ENDIF]?)  ( addr count -- addr count flag )
  s" [ENDIF]" ([cnd]-s=ci?exit)
  s" [THEN]" ([cnd]-s=ci?exit)
  false
; (hidden)

: ([ELSE]?)  ( addr count -- addr count flag )
  s" [ELSE]" ([cnd]-s=ci?exit)
  false
; (hidden)

: [ELSE]  ( -- )
  1  ;; level
  begin
    begin
      parse-name dup
      ifnot
        refill not ERR-UNBALANCED-IFDEF ?error
      endif
      dup
    until
         ([IF]?) if 1+
    else ([ELSE]?) if 1- dup if 1+ endif
    else ([ENDIF]?) if 1-
    else 2drop
    endif endif endif
    dup not
  until
  drop
; immediate


: [ENDIF]  ( -- )  ; immediate
alias [ENDIF] [THEN]


: [IF]  ( cond -- )
  ifnot [compile] [ELSE] endif
; immediate

: [IFNOT]  ( cond -- )
  if [compile] [ELSE] endif
; immediate

: [IFDEF]  ( -- )  \ word
  parse-name has-word? ifnot [compile] [ELSE] endif
; immediate

: [IFNDEF]  ( -- )  \ word
  parse-name has-word? if [compile] [ELSE] endif
; immediate

: [DEFINED]  ( -- flag )  \ word
  parse-name has-word?
; immediate

: [UNDEFINED]  ( -- flag )  \ word
  parse-name has-word? not
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
63 constant (#user-abort-msg) (hidden)
create (user-abort-msg) 0 c, (#user-abort-msg) allot create; (hidden)

: (abort-msg-reset)  ( -- ) (user-abort-msg) 0c! ;
: (abort-msg-emit)  ( ch -- )  (user-abort-msg) c@ (#user-abort-msg) < if (user-abort-msg) dup c@ + 1+ c! 1 (user-abort-msg) c+! else drop endif ;
: (abort-msg-type)  ( addr count -- )  dup +if (#user-abort-msg) min over + swap do i c@ (abort-msg-emit) loop else 2drop endif ;

: (abort-with-built-msg)  ( -- )
  endcr ." ABORT: " (user-abort-msg) ( ccount) dup c@ swap 1+ swap type space (abort-msg-reset) forth:error-line. cr
  ERR-USER-ERROR throw
;

: (abort-with-msg)  ( addr count -- )  (abort-msg-reset) (abort-msg-type) (abort-with-built-msg) ;

: (xabort-par)  ( -- )
  ?comp
  compile litstr
  (parse-compile-c4str)
  compile (abort-with-msg)
; (hidden)

: abort"  ( -- )  ;; "
  state @ if (xabort-par) else 34 parse (abort-with-msg) endif
; immediate

: ?abort"  ( flag -- )  ;; "
  state @ if
    [compile] if (xabort-par) [compile] endif
  else
    34 parse rot if (abort-with-msg) else 2drop endif
  endif
; immediate

: not-?abort"  ( flag -- )  ;; "
  state @ if
    [compile] ifnot (xabort-par) [compile] endif
  else
    34 parse rot ifnot (abort-with-msg) else 2drop endif
  endif
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sorry, i should find a better place for this
create (.lib-name) 1024 allot

: .LIB-START"  ( -- sttime stunused )  ;;"
  34 parse 1020 umin (.lib-name) C4S-COPY-A-C
  TLOAD-VERBOSE-LIBS if
    ." *** compiling library \`" (.lib-name) count type ." \`\n"
  endif
  sys-gettickcount unused
;


: .LIB-END  ( sttime stunused -- )  ;;"
  sys-gettickcount swap
  TLOAD-VERBOSE-LIBS if
    ." *** compiled  library \`" (.lib-name) count type ." \`, size is " unused - . ." bytes, " swap - . ." msecs\n"
  else
    2drop
  endif
;
