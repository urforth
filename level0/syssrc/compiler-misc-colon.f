;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; misc compiler words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; we don't have any high-level tools yet, so let's do it the hard way ;-)

(dbginfo-reset)
(create) :
  !csp
.stack
-find-required (URFORTH-DOFORTH-CODEBLOCK)
(call,)
]
  ?exec !csp

  (dbginfo-reset)
  \ 0 to (dbginfo-active?)

  (create) [
-find-required LITCFA
compile,
-find-required (URFORTH-DOFORTH-CODEBLOCK)
,
] (call,)
  [compile] ]
  exit
[ .stack ?csp smudge


: ;
  ?comp ?csp
  compile exit
  ;; set final word size
  create;
  smudge [compile] [
(* we don't have conditions yet; we'll replace this word later
  ;; save debug info
  (DBGINFO-GET-ADDR-SIZE) if
    ;; ( addr bytes )
    ;; save here to debug info field
    here latest-nfa nfa->dfa !
    ;; allocate and copy to here (in reverse order ;-)
    >r here r@ cmove
    r> allot
  endif
*)
  ;; deactivate
  (dbginfo-reset)
  exit
[ smudge immediate


;; creates new word definition.
;; the code field contains the routine that returns the address of the word's parameter field.
;; the new word is created in the current vocabulary.
: CREATE  ( -- )  \ word
  (create) smudge
  ;; emit default CFA
  [
-find-required LITCFA
compile,
-find-required (URFORTH-DOVAR-CODEBLOCK)
compile,
] (call,)
  create;
;
