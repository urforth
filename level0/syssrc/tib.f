;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; WITHIN
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: (swap!+cell)  ( addr value -- addr+cell )
  over ! cell+
;

: TIBSTATE-SAVE  ( bufaddr -- )
  tib @ (swap!+cell)
  #tib @ (swap!+cell)
  >in @ (swap!+cell)
  tib-line# @ (swap!+cell)
  (tib-last-read-char) @ (swap!+cell)
  drop
;

hidden:: (@+cell)  ( addr -- addr+cell value )
  dup cell+ swap @
;

: TIBSTATE-RESTORE  ( bufaddr -- )
  (@+cell) tib !
  (@+cell) #tib !
  (@+cell) >in !
  (@+cell) tib-line# !
  (@+cell) (tib-last-read-char) !
  drop
;
