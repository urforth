;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; misc compiler words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: (start-forth-compiler)  ( -- )
  (dbginfo-reset)
  true (to-compile-time-only) (dbginfo-active?)
  ['] (URFORTH-DOFORTH-CODEBLOCK) (call,)
  [compile] ]
;

hidden:: (end-forth-compiler)  ( -- )
  ;; set final word size
  create;
  smudge [compile] [
  ;; save debug info
  (DBGINFO-GET-ADDR-SIZE) if
    ;; ( addr bytes )
    ;; save here to debug info field
    here latest-nfa nfa->dfa !
    ;; allocate and copy to here
    dup n-allot  ;; ( addr bytes newaddr )
    swap cmove
  endif
  ;; deactivate
  (dbginfo-reset)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; we have conditions now, replace old semi with the new one
hidden:: (:-NEW)
  ?exec
  (create)
  (start-forth-compiler)
  !csp
;
replace : (:-NEW)


hidden:: (;-NEW)
  ?comp ?csp
  compile exit
  (end-forth-compiler)
;
replace ; (;-NEW)


: :NONAME
  ?exec
  here 0 (create-str) (hidden)
  (start-forth-compiler)
  latest-cfa
  !csp
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: (DEFER?)  ( cfa -- cfa true // false )
  dup c@ 0xe8 = ifnot drop false exit endif
  dup 1+ (disp32@)
  dup ['] (URFORTH-DODEFER-CODEBLOCK) =
  swap ['] (URFORTH-DOVALUE-CODEBLOCK) = or
  ifnot
    drop false
  else
    true
  endif
;

: TO  ( value -- )  \ name
  -find-required
  (defer?) not ERR-NOT-DEFER ?error
  state @ if
    compile LITTO!
    compile,
  else
    cfa->pfa !
  endif
; immediate

alias TO IS


: DEFER-@  ( -- cfa )  \ name
  -find-required
  (defer?) not ERR-NOT-DEFER ?error
  state @ if
    compile LITCFA
    ,
    compile cfa->pfa
    compile @
  else
    cfa->pfa @
  endif
; immediate


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; scattering colon implementation
;; based on the idea by M.L. Gassanenko ( mlg@forth.org )
;; written from scratch by Ketmar Dark
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; placeholder for scattering colon
;; it will compile two branches:
;; the first branch will jump to the first "..:" word (or over the two branches)
;; the second branch is never taken, and works as a pointer to the latest branch addr in the list
;; this way, each extension word will simply fix the last branch address, and update list tail
;; at the creation time, second branch points to the first branch
: ...  ( -- )
  ?comp
  latest-cfa dup word-type? word-type-forth <> ERR-ELLIPSIS-FORTH ?error
  cfa->pfa here <> ERR-ELLIPSIS-FIRST ?error
  compile branch (<j-mark) (mark-j>)
  compile branch swap (<j-resolve)
  (resolve-j>)
; immediate


;; start scattered colon extension code
;; TODO: better error checking!
;; this does very simple sanity check, and remembers the address of the tail pointer
: ..:  ( -- )  \ word
  ?exec
  ' dup word-type? word-type-forth <> ERR-ELLIPSIS-FORTH ?error
  \ FIXME: make check and patch more independed from codegen internals
  cfa->pfa dup @ ['] branch <> ERR-ELLIPSIS-FIRST ?error
  2 +cells dup @ ['] branch <> ERR-ELLIPSIS-FIRST ?error
  cell +  ;; pointer to the tail pointer
  \ [compile] :noname cfa->pfa ;; :noname leaves our cfa
  here 0 (create-str) (start-forth-compiler) latest-cfa cfa->pfa
  (CTLID-SC-COLON)  ;; pttp ourcfa flag
  !csp
;

;; this ends the extension code
;; it patches jump at which list tail points to jump to our pfa, then
;; it compiles jump right after the list tail, and then
;; it updates the tail to point at that jump address
: ;..  ( -- )  \ word
  ?comp ?csp
  (CTLID-SC-COLON) ?pairs
  ;; ( pttp ourcfa )
  over (branch-addr@) (branch-addr!)
  >r compile branch here 0 , r@ cell+ swap (branch-addr!)
  here cell- r> (branch-addr!)
  ;; we're done here
  (end-forth-compiler)
; immediate
