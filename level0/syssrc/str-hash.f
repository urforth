;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; string hashing (for hashtables)
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(*
;; fold 32-bit hash to 16-bit hash
code: uhash32->16  ( u32hash -- u16hash )
  ld    eax,TOS
  shr   eax,16
  add   cx,ax
  movzx TOS,cx
  urnext
endcode

code: uhash16->8  ( u16hash -- u8hash )
  add   cl,ch
  movzx TOS,cl
  urnext
endcode
*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: STR-HASH-DSFORTH  ( addr count -- u8hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  rol   al,1
  add   al,byte [edi]
  inc   edi
  loop  .hashloop
.done:
  ld    TOS,eax
  urnext
endcode


code: STR-HASH-ELF  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash <<= 4
  shl   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  add   eax,edx
  ;; high = hash&0xF0000000
  ld    edx,eax
  and   edx,0xf0000000
  ld    ebx,edx
  ;; hash ^= high>>24
  rol   edx,8
  xor   eax,edx
  ;; hash &= ~high
  xor   ebx,0xffffffff
  and   eax,ebx
  inc   edi
  loop  .hashloop
.done:
  ld    TOS,eax
  urnext
endcode


code: STR-HASH-JOAAT  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash += *key
  movzx edx,byte [edi]
  add   eax,edx
  ;; hash += hash<<10
  ld    edx,eax
  shl   eax,10
  add   eax,edx
  ;; hash ^= hash>>6
  ld    edx,eax
  shr   eax,6
  xor   eax,edx
  inc   edi
  loop  .hashloop
  ;; final permutation
  ;; hash += hash<<3
  ld    edx,eax
  shl   eax,3
  add   eax,edx
  ;; hash ^= hash>>11
  ld    edx,eax
  shr   eax,11
  xor   eax,edx
  ;; hash += hash<<15
  ld    edx,eax
  shl   eax,15
  add   eax,edx
.done:
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tests
0 [IF]
\ ' STR-HASH-DSFORTH disasm-word
.( testing hashing functions...\n)

: (str-hash-nassert) ( v0 v1 -- )
  2dup - if
    ." assertion failed!\n"
    ." EXPECTED: $" .hex8 cr
    ."      GOT: $" .hex8 cr
    1 bye
  endif
  2drop
;

s" a" str-hash-dsforth  $00000061 (str-hash-nassert)
s" ab" str-hash-dsforth $00000024 (str-hash-nassert)
s" The quick brown fox jumps over the lazy dog" str-hash-dsforth  $00000063 (str-hash-nassert)

s" a" str-hash-elf  $00000061 (str-hash-nassert)
s" ab" str-hash-elf $00000672 (str-hash-nassert)
s" The quick brown fox jumps over the lazy dog" str-hash-elf  $04280C57 (str-hash-nassert)

s" a" str-hash-joaat  0xca2e9442 (str-hash-nassert)
s" ab" str-hash-joaat $45E61E58 (str-hash-nassert)
s" The quick brown fox jumps over the lazy dog" str-hash-joaat  0x519e91f5 (str-hash-nassert)

.( done testing hashing functions...\n)
[ENDIF]
