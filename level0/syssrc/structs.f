;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; standard struct creation words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; created word returns structure size
: BEGIN-STRUCTURE  ( "<spaces>name" -- struct-sys 0 )
  create
  here 0 0 ,  ;;  mark stack, lay dummy
 does>
  @    ;; -- rec-len
;
alias BEGIN-STRUCTURE BEGIN-STRUCT

: END-STRUCTURE  ( struct-sys +n -- )
  swap !  ;; set len
;
alias END-STRUCTURE END-STRUCT

;; field usage: addr fldname --> addr+fldoffset
: +FIELD  ( n1 n2 "<spaces>name" -- n3 )
  create
  over , +
 does>
  @ +
;

;; field usage: addr fldname --> addr+fldoffset
;; creates field of one-cell size (and aligns it at a cell boundary)
: FIELD:  ( n1 "<spaces>name" -- n2 )
  create
  ;; align
  0x03 + 0x03 bitnot and
  dup , cell+
 does>
  @ +
;
