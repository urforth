;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; advanced vocabulary manipulation and creation words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: get-current  ( -- vocid )  current @ ;
: set-current  ( vocid -- )  current ! ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: WLIST-HASH-BYTES  ( -- n )
  1 WLIST-HASH-BITS lshift cells
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: VOCID-OF  ( -- vocid )
  -find-required
  dup word-type? WORD-TYPE-VOC <> err-vocabulary-expected ?error
  ;; ( cfa )
  voc-cfa->vocid
;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary structure
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;; for UrForth level 1:
;;   dd headernfa (can be zero)
;; for all:
;;   hashtable (if enabled)

: (NEW-WORDLIST)  ( -- vocid )
  here  ;; this is vocid
  0 ,             ;; latest
  here
  (VOC-LINK) @ ,  ;; voclink
  (VOC-LINK) !
  0 ,             ;; parent
  [ forth:urforth-level ] [IF]
  0 ,             ;; headernfa
  [ENDIF]
  ;; words hashtable buckets
  [ WLIST-HASH-BITS ] [IF]
    WLIST-HASH-BYTES n-allot WLIST-HASH-BYTES erase
  [ENDIF]
; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; vocabulary support
: VOCABULARY  ( -- )
  [ forth:urforth-level ] [IF]
    (create) smudge
    ;; toggle "vocabulary" flag
    (WFLAG-VOCAB) (toggle-latest-wflag)
    ;; vocabulary is using normal Forth CFA
    ['] (urforth-doforth-codeblock) (call,)
    ['] (VOCAB-DOES-CODE) compile,
    ;; we will patch vocid here
    here >r 0 ,
    (NEW-WORDLIST) r> !
    create;
  [ELSE]
    create
    (NEW-WORDLIST) drop
    ;; toggle "vocabulary" flag
    (WFLAG-VOCAB) (toggle-latest-wflag)
    ;; this is basically what "(DOES>)" does, because we cannot have this code here
    ;; does code
    here
    ;; mov eax,(VOCAB-DOES-CODE)
    0xb8 c,
    ['] (VOCAB-DOES-CODE) cfa->pfa ,
    ;; jump there
    ['] (URFORTH-DODOES-CODEBLOCK) (jmp,)
    ;; fix CFA
    latest-cfa (call!)
    create;
 (* sadly, the kernel needs this code for "FORTH" vocabulary
 does>
  ;; ( latest-ptr )
  ;; puts this one to the context stack, replacing the top one
  context
  (forth-voc-stack)
  u<
  if
    ;; we don't have any vocabularies in the stack, push one
    (forth-voc-stack) to context
  endif
  ;; replace top context vocabulary
  context !
 *)
  [ENDIF]
;

;; new vocabulary will see all definitions from CURRENT vocabulary
: NESTED-VOCABULARY  ( -- )
  vocabulary
  ;; set "parent"
  current @ latest-cfa voc-cfa->vocid 2 +cells !
;


;; UrForth level 1 has those in the kernel
forth:urforth-level [IFNOT]

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; move to voclink field
: VOCID->VOCLINK  ( lfaptr -- voclink )
  cell+
;

WLIST-HASH-BITS [IF]
;; move to hashtable
: VOCID->HTABLE  ( lfaptr -- htable )
  3 +cells
;
[ENDIF]


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; makes all newly created words public
: <PUBLIC-WORDS>  ( -- )
  (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) bitnot and to (CURRENT-CREATE-MODE)
;

;; makes all newly created words hidden
: <HIDDEN-WORDS>  ( -- )
  (CURRENT-CREATE-MODE) (WFLAG-HIDDEN) or to (CURRENT-CREATE-MODE)
;

;; sets top context vocabulary as the current one
;; resets current mode to public
: DEFINITIONS  ( -- )
  context @ current ! <public-words>
;

;; makes top context vocabulary as the only one, and makes it current too
;; resets current mode to public
: ONLY  ( -- )
  (forth-voc-stack) to context
  context @ current !
  <PUBLIC-WORDS>
;

;; duplicates top context vocabulary, so it could be replaced with another one
;; resets current mode to public
: ALSO  ( -- )
  ;; check for vocstack overflow
  context cell+ dup (forth-voc-stack-end) u>= ERR-VOCABULARY-STACK-OVERFLOW ?error
  context @ over !
  to context
;

;; drop topmost context vocabulary
;; resets current mode to public
: PREVIOUS  ( -- )
  ;; check for vocstack underflow
  context (forth-voc-stack) u>=
  if
    context cell- to context
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; prints vocabulary name
: VOCID.  ( vocid -- )
  pfa->cfa cfa->nfa id.
;

;; shows current search order
: ORDER  ( -- )
  ." CONTEXT:"
  context
  begin
    dup (forth-voc-stack) u>=
  while
    dup @ space vocid.
    cell-
  repeat
  drop
  space ." (ROOT)\nCURRENT:"
  current @ space vocid. cr
;
[ENDIF]


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; call CFA for each vocabulary in system
;; passed vocid is suitable for "FOREACH-WORDS" and "(VOC.)"
;; cfa: ( vocid -- endflag ) -- return non-zero to exit with this value
: FOREACH-VOC  ( cfa -- endflag )
  (voc-link)
  begin
    @ ?dup
  while
    ;; make sure that data stack doesn't contain our data,
    ;; so cblock can use it to count something, for example
    2dup 2>r cell- swap execute ?dup
    if
      2rdrop exit
    endif
    2r>
  repeat
  drop 0
;

;; call CFA for each word in vocabulary
;; vocid is what "FOREACH-VOC" is passing to cblock
;; note that no words are skipped (i.e. SMUDGE and HIDDEN words are enumerated too)
;; use "context @" to get CONTEXT vocabulary vocid
;; use "current @" to get CURRENT vocabulary vocid (the top one)
;; use "VOCID-OF name" to get vocid from vocabulary name
;; cfa: ( nfa -- endflag ) -- return non-zero to exit with this value
: FOREACH-WORD  ( vocid cfa -- endflag )
  swap
  begin
    @ ?dup
  while
    ;; make sure that data stack doesn't contain our data,
    ;; so cblock can use it to count something, for example
    2dup 2>r lfa->nfa swap execute ?dup
    if
      2rdrop exit
    endif
    2r>
  repeat
  drop 0
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; shows all known vocabularies
: VOC-LIST  ( -- )
  ." KNOWN VOCABULARIES:\n"
  [:
    ;; ( vocid -- endflag )
    2 spaces vocid. cr
    false
  ;] foreach-voc drop
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: WORDS  ( -- )
  endcr ." --- WORDS OF " context @ vocid. ."  ---\n"
  context @
  [:
    ;; ( nfa -- exitflag )
    ;; skip SMUDGE and HIDDEN words
    dup nfa->ffa ffa@
    [ (WFLAG-SMUDGE) (WFLAG-HIDDEN) or ] literal and
    if
      drop
    else
      id. space
    endif
    0
  ;] foreach-word drop
  endcr
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: WORD-CODE-END  ( cfa -- addr )
  cfa->nfa nfa->sfa @
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: (WORD-VOCID-IP->NFA)  ( ip latestptr -- nfa-or-0 )
  [: ;; ( ip nfa -- ip 0 // ip nfa )
      ;; UF 2dup swap dothex8 space dup dothex8 space dup nfa2sfa @ dothex8 space iddot cr
    2dup dup nfa->sfa @ 1- bounds?
    ;; ( ip nfa flag )
    ifnot
      drop false
    endif
  ;] foreach-word nip
;


: WORD-IP->NFA  ( ip -- nfa-or-0 )
  [: ;; ( ip voclptr -- ip 0 // ip nfa )
    over swap (word-vocid-ip->nfa)
  ;] foreach-voc nip
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (voc-search-with-mask)  ( addr count vocid flags -- addr count false // cfa true )
  (wfind-flags>r) (voc-find-flags) 0! (voc-find-ignore-mask) !
  >r 2dup r> voc-find-str
  if nrot 2drop true else false endif
  (wfind-r>flags)
;

;; this searches a vocabulary for a word, ignoring hidden and smudged
: voc-search  ( addr count vocid -- addr count false // cfa true )
  [ (wflag-smudge) (wflag-hidden) or (wflag-vocab) or (wflag-codeblock) or ] literal
  (voc-search-with-mask)
;


;; this searches a vocabulary for a word, ignoring hidden, smudged and immediate
: voc-search-noimm  ( addr count vocid -- addr count false // cfa true )
  [ (wflag-smudge) (wflag-hidden) or (wflag-vocab) or (wflag-codeblock) or (wflag-immediate) or ] literal
  (voc-search-with-mask)
;
