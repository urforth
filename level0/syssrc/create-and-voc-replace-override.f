;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; advanced vocabulary manipulation and creation words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; returns vocabulary latest-ptr
: VOCID:  ( -- addr )  \ name
  -find-required
  dup word-type? WORD-TYPE-VOC <> ERR-VOCABULARY-EXPECTED ?error
  ;; ( cfa )
  state @ if
    [compile] cfaliteral
    compile voc-cfa->vocid
  else
    voc-cfa->vocid
  endif
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word overriding mechanics
;; this is different from REPLACE -- you can call old word if you want to

;; get xtoken for the previous override, to use in override chain
;; if the word is not overriden, returns 0
: (GET-OVERRIDE-OLDCFA)  ( oldcfa -- xtoken // 0 )
  dup word-type? WORD-TYPE-OVERRIDEN =
  if
    1+ (disp32@) cfa->pfa
  else
    drop 0
  endif
;

: (OVERRIDE-OLDCFA-NEWCFA)  ( oldcfa newcfa -- )
  ;; check if newcfa is a Forth word
  dup word-type? WORD-TYPE-FORTH <> err-cannot-override ?error
  ;; check if oldcfa is a Forth word, or an overriden word
  ;; override chain works right for overriden words
  over word-type? dup WORD-TYPE-FORTH =
  ;; ( oldcfa newcfa wtype forth-flag )
  if
    drop
    ;; fix new word CFA to use par_urforth_nocall_dooverride
    ['] (URFORTH-DOOVERRIDE-CODEBLOCK) over (call!)
    ;; fix old word CFA so it will call new CFA instead
    swap (call!)
  else
    ;; override chain
    ;; ( oldcfa newcfa wtype )
    WORD-TYPE-OVERRIDEN <> err-cannot-override ?error
    ;; fix new word CFA to use par_urforth_nocall_dooverride
    ['] (URFORTH-DOOVERRIDE-CODEBLOCK) over (call!)
    ;; fix old word CFA so it will call new CFA instead
    swap (call!)
  endif
;

: OVERRIDE  ( -- )  \ oldword newword
  -find-required
  -find-required
  (override-oldcfa-newcfa)
;

;; get xtoken for the previous override, to use in override chain
;; if the word is not overriden, returns 0
: GET-OVERRIDE  ( -- xtoken // 0 )  \ oldword
  -find-required
  (get-override-oldcfa)
;
