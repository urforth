;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; advanced vocabulary manipulation and creation words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

hidden:: (DOES>)
  ;; replace CFA CALL with the call to the code we'll build
  here latest-cfa (call!)
  ;; build machine code:
  ;;   mov   eax,value
  ;;   jmp   fword_par_urforth_nocall_dodoes
  0xb8 c, ;; mov  eax,...
  r> ,    ;;   value
  ['] (URFORTH-DODOES-CODEBLOCK) (jmp,)
  create;
;

: DOES>  ( -- pfa )
  ?comp
  ?csp ;; make sure that all our conditionals are complete
  compile (does>) ;; and compile the real word
; immediate
