;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; misc compiler words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: VAR-NOALLOT  ( -- )
  (create)
  ['] (URFORTH-DOVAR-CODEBLOCK) (call,)
  smudge create;
;

: CREATE-NAMED  ( addr count -- )
  (create-str)
  ['] (URFORTH-DOVAR-CODEBLOCK) (call,)
  smudge create;
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: CONSTANT  ( n -- )
  (create)
  ['] (URFORTH-DOCONST-CODEBLOCK) (call,)
  ,
  smudge create;
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: VALUE  ( n -- )
  (create)
  ['] (URFORTH-DOVALUE-CODEBLOCK) (call,)
  ,
  smudge create;
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DEFERED  ( cfa -- )
  (create)
  ['] (URFORTH-DODEFER-CODEBLOCK) (call,)
  , smudge create;
;

: DEFER  ( -- )
  ['] noop defered
;

hidden:: (NOTIMPL)  ( -- )
  err-not-implemented error
  ;; just in case it returns
  1 n-bye
; (noreturn)

: DEFER-NOT-YET  ( -- )
  ['] (NOTIMPL) defered
;

hidden:: (to-compile-time-only)  ( value -- )  \ name
  -find-required
  dup c@ 0xe8 <> ERR-NOT-DEFER ?error
  dup 1+ (disp32@)
  dup ['] (URFORTH-DODEFER-CODEBLOCK) =
  swap ['] (URFORTH-DOVALUE-CODEBLOCK) = or not ERR-NOT-DEFER ?error
  state @ not err-compilation-only ?error
  compile LITTO!
  compile,
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create alias for some word
hidden:: (ALIAS-STR)  ( cfa addr count -- )
  (create-str)
  (jmp,) ;; emit jump to the old cfa
  create;
  smudge
;

;; will not inherit word attributes
hidden:: (ALIAS-NO-ATTRS)  ( cfa -- )  \ word
  parse-name (alias-str)
;

;; will inherit word attributes "immediate" and "hidden"
hidden:: (ALIAS)  ( cfa -- )  \ word
  dup >r (alias-no-attrs)
  ;; copy "hidden" and "immediate" flags
  r> cfa->nfa nfa->ffa ffa@
  (wflag-hidden) (wflag-immediate) or and >r
  latest-cfa cfa->nfa nfa->ffa
  dup ffa@ (wflag-hidden) (wflag-immediate) or bitnot and r> or
  swap ffa!
;

: ALIAS  ( -- )  \ oldword newword
  -find-required
  (alias)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word replace mechanics
hidden:: (REPLACE)  ( oldcfa newcfa -- )
  ;; it is possible to replace any word with any word, because why not?
  ;; the only requirement is oldcfa word size: it should have at least 5 bytes
  over cfa->nfa nfa->sfa @ 5 u< err-cannot-replace ?error
  ;; patch jump to newcfa at oldcfa
  swap (jmp!)
;

: REPLACE  ( -- )  \ oldword newword
  -find-required
  -find-required
  (replace)
;
