;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; string words without assembler code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

create (NULLSTRING) 0 , 0 ,  (hidden)

: NullString  ( addr 0 -- )
  (NULLSTRING) count
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: BCOUNT  ( addr -- addr+1 count )
  dup c@ swap 1+ swap
;

alias BCOUNT CCOUNT


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: STR-CAT  ( daddr dcount saddr scount -- daddr dcount+scount )
  dup 0>
  if
    2>r 2dup + 2r>
    >r swap r@ move r> +
  else
    drop
  endif
;

alias C@ CCOUNT-ONLY   ( addr -- count )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; adjust the character string at c-addr1 by n characters.
;; the resulting character string, specified by c-addr2 u2,
;; begins at c-addr1 plus n characters and is u1 minus n characters long.
: /STRING  ( c-addr1 u1 n -- c-addr2 u2 )
  dup >r - swap r> + swap
;


;; if u1 is greater than zero, u2 is equal to u1 less the number of
;; spaces at the end of the character string specified by c-addr u1.
;; if u1 is zero or the entire string consists of spaces, u2 is zero.
: -TRAILING  ( c-addr u1 -- c-addr u2 )
  begin
    dup 0>
  while
    2dup + 1- c@ 32 <=
  while
    1-
  repeat
;
