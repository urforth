;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; double math words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: S>D  ( n -- d )
  ld    eax,TOS
  cdq
  push  eax
  ld    TOS,edx
  urnext
endcode

code: U>D  ( u -- ud )
  push  TOS
  xor   TOS,TOS
  urnext
endcode

;; with overflow clamping
code: D>U  ( ud -- u )
  pop   eax
  jcxz  .done
  ld    eax,0xffffffff
.done:
  urnext
endcode

code: D>S  ( d -- n )
  pop   eax
  jcxz  .poscheck
  ; overflow or negative
  test  TOS,0x80000000
  jr    z,.posoverflow
  ; definitely negative
  inc   TOS
  jr    nz,.negoverflow
  cp    eax,0x80000000
  jr    nc,.good
.negoverflow:
  ld    TOS,0x80000000
  urnext
.poscheck:
  ; positive, check for positive overflow
  cp    eax,0x80000000
  jr    c,.good
.posoverflow:
  ; positive overflow
  ld    TOS,0x7fffffff
  urnext
.good:
  ld    TOS,eax
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UM*  ( u0 u1 -- ud )
  pop   eax
  xchg  eax,TOS
  mul   TOS
  push  eax
  mov   TOS,edx
  urnext
endcode

code: UM/MOD  ( ud1 u1 -- umod ures )
  pop   edx
  pop   eax
  or    TOS,TOS
  jr    z,@f
  div   TOS
  push  edx
  mov   TOS,eax
  urnext
@@:
  push  TOS
  urnext
endcode

code: UM/  ( ud1 u1 -- ures )
  pop   edx
  pop   eax
  or    TOS,TOS
  jr    z,@f
  div   TOS
  mov   TOS,eax
  urnext
@@:
  push  TOS
  urnext
endcode

code: UMMOD  ( ud1 u1 -- umod )
  pop   edx
  pop   eax
  or    TOS,TOS
  jr    z,@f
  div   TOS
  mov   TOS,edx
  urnext
@@:
  push  TOS
  urnext
endcode


code: M*  ( n0 n1 -- d )
  pop   eax
  xchg  eax,TOS
  imul  TOS
  push  eax
  mov   TOS,edx
  urnext
endcode

code: M/MOD  ( d1 n1 -- nmod nres )
  pop   edx
  pop   eax
  or    TOS,TOS
  jr    z,@f
  idiv  TOS
  push  edx
  mov   TOS,eax
  urnext
@@:
  push  TOS
  urnext
endcode

code: M/  ( d1 n1 -- nres )
  pop   edx
  pop   eax
  or    TOS,TOS
  jr    z,@f
  idiv  TOS
  mov   TOS,eax
  urnext
@@:
  push  TOS
  urnext
endcode

code: MMOD  ( d1 n1 -- nmod )
  pop   edx
  pop   eax
  or    TOS,TOS
  jr    z,@f
  idiv  TOS
  mov   TOS,edx
  urnext
@@:
  push  TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D2*  ( d -- d*2 )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  shl   edx,1
  rcl   ecx,1
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: D2/  ( d -- d/2 )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  sar   ecx,1
  rcr   edx,1
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: D2U/  ( d1 -- d/2 )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  shr   ecx,1
  rcr   edx,1
  ;; push ECX:EDX
  push  edx
  urnext
endcode

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: DNEGATE  ( d -- -d )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  xor   ecx,0xffffffff
  xor   edx,0xffffffff
  add   edx,1
  adc   ecx,0
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: DABS  ( d -- |d| )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  test  ecx,0x80000000
  jr    z,@f
  xor   ecx,0xffffffff
  xor   edx,0xffffffff
  add   edx,1
  adc   ecx,0
@@:
  ;; push ECX:EDX
  push  edx
  urnext
endcode

code: DSGN  ( d -- -1/0/1 )
  pop   edx
  ld    eax,edx
  or    eax,TOS
  jr    nz,@f
  urnext
@@:
  test  TOS,0x80000000
  jr    nz,@f
  ld    TOS,1
  urnext
@@:
  ld    TOS,-1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D0=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  or    ecx,edx
  cp    ecx,1
  ;;  C: ECX==0
  ;; NC: ECX!=0
  ld    ecx,0
  adc   ecx,0
  urnext
endcode

code: D0!=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  or    ecx,edx
  cp    ecx,1
  ;;  C: ECX==0
  ;; NC: ECX!=0
  ld    ecx,1
  sbb   ecx,0
  urnext
endcode

code: D0<  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,1
  sbb   ecx,0
  urnext
endcode

code: D0>  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  ld    eax,ecx
  or    eax,edx
  jr    z,@f
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,0
  adc   ecx,0
@@:
  urnext
endcode

code: D0<=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  ld    eax,ecx
  or    eax,edx
  jr    z,@f
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,1
  sbb   ecx,0
  urnext
@@:
  ld    TOS,1
  urnext
endcode

code: D0>=  ( d -- flag )
  pop   edx
  ;; ECX=d1-high
  ;; EDX=d1-low
  ld    eax,ecx
  or    eax,edx
  jr    z,@f
  cp    ecx,0x80000000
  ;;  C: ECX>=0
  ;; NC: ECX<0
  ld    ecx,0
  adc   ecx,0
  urnext
@@:
  ld    TOS,1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D+  ( d1 d2 -- d )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  add   eax,ebx
  adc   edx,ecx
  ;; push EDX:EAX
  push  eax
  ld    TOS,edx
  urnext
endcode

code: D-  ( d1 d2 -- d )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  ;; push EDX:EAX
  push  eax
  ld    TOS,edx
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: D=  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  or    eax,edx
  jr    z,@f
  ld    TOS,0
  urnext
@@:
  ld    TOS,1
  urnext
endcode

code: D<>  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  or    eax,edx
  jr    z,@f
  ld    TOS,1
  urnext
@@:
  ld    TOS,0
  urnext
endcode

code: D<  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d1-d2: d1<d2:C; d1>d2:nc
  sub   eax,ebx
  sbb   edx,ecx
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: D>  ( d1 d2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d2-d1
  sub   ebx,eax
  sbb   ecx,edx
  setl  cl
  movzx TOS,cl
  urnext
endcode

code: DU<  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d1-d2
  sub   eax,ebx
  sbb   edx,ecx
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: DU>  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  ;; d2-d1
  sub   ebx,eax
  sbb   ecx,edx
  setc  cl
  movzx TOS,cl
  urnext
endcode

code: DU<=  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   ebx,eax
  sbb   ecx,edx
  ld    TOS,1
  sbb   TOS,0
  urnext
endcode

code: DU>=  ( ud1 ud2 -- flag )
  pop   ebx
  pop   edx
  pop   eax
  ;; ECX=d2-high
  ;; EBX=d2-low
  ;; EDX=d1-high
  ;; EAX=d1-low
  sub   eax,ebx
  sbb   edx,ecx
  ld    TOS,1
  sbb   TOS,0
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DMAX  ( d1 d2 -- max[d1,d2] )
  2over 2over d< if 2swap 2drop endif
;

: DMIN   ( d1 d2 -- min[d1,d2] )
  2over 2over d> if 2swap 2drop endif
;

: M+  ( d1|ud1 n -- d2|ud2 )
  s>d d+
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: UDS*  ( ud1 u --> ud2 )
  ld    edi,TOS
  pop   ebx
  pop   ecx
  ld    eax,ecx
  mul   edi
  push  edx
  ld    ecx,eax
  ld    eax,ebx
  mul   edi
  pop   edx
  add   eax,edx
  push  ecx
  ld    TOS,eax
  urnext
endcode
