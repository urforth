;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; single math words
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: SGN  ( n -- -1/0/1 )
  or    TOS,TOS
  jr    nz,@f
  urnext
@@:
  test  TOS,0x80000000
  jr    nz,@f
  ld    TOS,1
  urnext
@@:
  ld    TOS,-1
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: */  ( n0 n1 n2 -- n0*n1/n2 )
  pop   eax
  pop   ebx
  jecxz .zero
  ; TOS=n2
  ; EAX=n1
  ; EBX=n0
  imul  ebx
  idiv  TOS
  ld    TOS,eax
.zero:
  urnext
endcode


code: */MOD  ( n0 n1 n2 -- n0*n1/n2 n0*n1%n2 )
  pop   eax
  pop   ebx
  jecxz .zero
  ; TOS=n2
  ; EAX=n1
  ; EBX=n0
  imul  ebx
  idiv  TOS
  push  edx
  ld    TOS,eax
.zero:
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rounds toward zero
code: SM/REM  ( d n -- nmod ndiv )  \ ANS
  pop   edx
  pop   eax
  jecxz .zero
  idiv  TOS
  push  edx
  ld    TOS,eax
  urnext
.zero:
  push  TOS
  urnext
endcode

;; rounds toward negative infinity
code: FM/MOD  ( d n -- nmod ndiv )  \ ANS
  pop   edx
  pop   eax
  jecxz @f
  ld    ebx,edx
  idiv  TOS
  or    edx,edx
  jr    z,@f
  xor   ebx,TOS
  jr    ns,@f
  dec   eax
  add   edx,TOS
@@:
  push  edx
  ld    TOS,eax
  urnext
.zero:
  push  TOS
  urnext
endcode


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ~and  ( u0 u1 -- u0&~u1 )
  bitnot and
;
