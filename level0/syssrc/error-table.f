;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;; WITHIN
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: error-table-msg,"  ( code -- )  \ msg"
  ;; UrForth level 0 uses byte for error code, UrForth level 1 uses dword
  [ forth:urforth-level ] [IF]
    ,
  [ELSE]
    c,
  [ENDIF]
  34 parse dup n-allot swap move
  0 c,
;


;; compile end of list
: error-table-end  ( -- )
  ;; UrForth level 0 uses two zero bytes, UrForth level 1 uses -1
  [ forth:urforth-level ] [IF]
    -1 ,
  [ELSE]
    0 w,
  [ENDIF]
;
