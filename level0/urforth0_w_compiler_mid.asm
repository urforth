;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


urword_forth "RECURSE",recurse
urword_immediate
;; ( -- )
  UF latestcfa compile_comma
urword_end


urword_forth "[CHAR]",imm_char
urword_immediate
;; ( -- ch )  \ word
  UF parse_name
  UF 1 nequ errid_char_expected qerror
  UF c@ literal
urword_end


urword_forth "'",tick
;; ( -- cfa )  \ word
  UF mfind not errid_word_expected qerror
urword_end


urword_forth "(COMPILE)",par_compile
urword_hidden
urword_arg_cfa
;; ( -- )
  ;; UF qcomp
  UF rpop dup cellinc rpush @ compile_comma
urword_end

urword_forth "COMPILE",compile
urword_immediate
;; ( -- )
  UF qcomp mfind not errid_word_expected qerror
  urcompile par_compile
  UF compile_comma
urword_end

urword_forth "[COMPILE]",imm_compile
urword_immediate
;; ( -- )
  UF qcomp
  UF mfind not errid_word_expected qerror
  UF compile_comma
urword_end

;; ANS idiocity, does this:
;; if the next word is immediate, compiles it in the current word
;; if the next word is not immediate, compiles "compile nextword"
;; shit
urword_forth "POSTPONE",imm_postpone
urword_immediate
;; ( -- )
  UF qcomp mfind not errid_word_expected qerror
  UF dup cfa2nfa nfa2ffa ffapeek par_wflag_immediate and
  ur_ifnot
    urcompile par_compile
  ur_endif
  UF compile_comma
urword_end


urword_forth "[",lsqparen
urword_immediate
;; ( -- )
  UF state 0poke
urword_end

urword_forth "]",rsqparen
;; ( -- )
  UF state 1poke
urword_end


urword_forth "(",comment_lparen
urword_immediate
  UF 41 parse 2drop
urword_end

urword_forth '\',comment_toeol
urword_immediate
  ;; check last delimiter
  UF par_last_read_char @ 10 equal
  ur_if
    UF exit
  ur_endif
  ur_begin
    UF tib_getch qdup
  ur_while
    ;; ( ch -- )
    UF dup 13 equal
    ur_if
      UF drop tib_peekch 10 equal
      ur_if
        UF tib_getch drop
      ur_endif
      UF exit
    ur_endif
    UF 10 equal
    ur_if
      UF exit
    ur_endif
  ur_repeat
urword_end

; aliases
urword_alias ";;",comment_toeol_semisemi,comment_toeol
urword_immediate
urword_alias "//",comment_toeol_slashslash,comment_toeol
urword_immediate

; multiline comment
; (* .... *)
urword_forth "(*",comment_multiline
urword_immediate
  ur_begin
    UF tib_getch qdup
  ur_while
    ;; ( ch -- )
    UF 42 equal tib_peekch 41 equal and
    ur_if
      UF tib_getch drop exit
    ur_endif
  ur_repeat
urword_end

; nested multiline comment
; (+ .... +)
urword_forth "(+",comment_multiline_nested
urword_immediate
  UF 1  ; current comment level
  ur_begin
    UF tib_getch qdup
  ur_while
    ;; ( ch -- )
    UF 8 lshift tib_peekch or
    UF dup 0x282b equal  ;; (+?
    ur_if
      UF drop tib_getch drop  1inc
    ur_else
      UF 0x2b29 equal   ;; +)
      ur_if
        UF tib_getch drop  1dec
        UF qdup
        ur_ifnot
          UF exit
        ur_endif
      ur_endif
    ur_endif
  ur_repeat
  UF drop
urword_end


urword_forth "(PARSE-COMPILE-C4STR)",par_parse_compile_strlit
urword_hidden
;; ( ch -- ... )
  UF 34 parse_to_here
  ; unescape it (we don't need returned data)
  UF count str_unescape swap celldec !  ;; update length
  UF state @
  ur_if
    ; compile string literal (with trailing zero byte, not included in count)
    ;; this is either noop, or real copy (due to DP-TEMP)
    UF word_here here over count_only cellinc move
    UF here count_only cellinc allot
    UF 0 ccomma
  ur_else
    UF word_here pad word_here count_only cellinc cmove pad count
    ; put trailing 0 (not included in count)
    UF 2dup + 0cpoke
  ur_endif
urword_end

urword_forth '"',double_quote
urword_immediate
;; ( -- addr count )  \ word
  UF state @
  ur_if
    ; compile string literal
    urcompile strlit
  ur_endif
  UF par_parse_compile_strlit
urword_end

urword_alias 'S"',sdquote,double_quote
urword_immediate


urword_forth '(.")',pardottype
urword_hidden
urword_arg_c4strz
  UF rpeek count type rpop count + 1inc rpush
urword_end


urword_forth '."',dot_double_quote
urword_immediate
;; ( -- )  \ word
  UF state @
  ur_if
    ; compile string literal
    urcompile pardottype
  ur_endif
  UF par_parse_compile_strlit
  UF state @
  ur_ifnot
    UF type
  ur_endif
urword_end

urword_forth ".(",dot_lparen
urword_immediate
;; ( -- )  \ word
  UF 41 parse_to_here count str_unescape type
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1 already has it in the kernel, so i have to move it here
urword_forth "[']",imm_tick
urword_immediate
;; ( -- cfa )  \ word
  UF tick cfaliteral
urword_end

urword_forth "CFALITERAL",cfaliteral
urword_immediate
  UF state @ q0exit
  urcompile cfalit
  UF compile_comma
urword_end
