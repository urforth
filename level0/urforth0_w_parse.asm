;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; if set to true (non-zero), "PARSE-NAME" will understand comments without space delimiters
;; (except a lone banana)
urword_value "PARSE-NAME-ADV-COMMENTS",parse_name_adv_comments,1


;DIGIT ( c n1 -- n2 tf  ok)
;      ( c n1 -- ff     bad)
;Converts the ascii character c (using base n1) to its binary equivalent
;n2, accompanied by a true flag. If the conversion is invalid, leaves
;only a false flag.
urword_code "DIGIT",digit
  pop   eax
  ; TOS=base
  ; EAX=char
  jecxz .bad
  test  TOS,0x80000000
  jr    nz,.bad
  cp    TOS,36+1
  jr    nc,.bad
  sub   al,'0'
  jr    c,.bad
  cp    al,10
  jr    c,.check_base
  sub   al,7
  jr    c,.bad
  cp    al,10
  jr    c,.bad
  ; upcase it
  cp    al,36
  jr    c,@f
  cp    al,42
  jr    c,.bad
  sub   al,32
@@:
.check_base:
  cp    al,cl     ; ECX is TOS
  jr    nc,.bad
  movzx eax,al
  push  eax
  mov   TOS,1
  urnext
.bad:
  xor   TOS,TOS
  urnext
urword_end

urword_forth "DIGIT?",digitq
;; ( ch base -- flag )
  UF digit dup
  ur_if
    UF nip
  ur_endif
urword_end


;; convert the ASCII text beginning at addr with regard to BASE.
;; the new value is accumulated into unsigned number u0, being left as u1.
;; addr1 and count1 are unparsed part of the string
;; will never read more than count bytes
;; doesn't skip any spaces, doesn't parse prefixes and signs
;; but skips '_'
urword_forth "(NUMBER-PARSE-SIMPLE)",par_number_parse_simple
;; ( addr count u0 -- addr1 count1 u1 )
  UF over 0great
  ur_ifnot
    UF exit
  ur_endif
  UF rpush
  ;; ( addr count | u )
  ;; first must be a digit
  UF over cpeek base @ digitq
  ur_if
    ;; main loop
    ur_begin
      UF dup
    ur_while
      ;; ( addr count | u )
      UF over cpeek
      ;; skip '_'
      UF dup 95 equal
      ur_if
        UF drop
      ur_else
        ;; try digit
        UF base @ digit
        ur_ifnot
          UF rpop exit  ;; replace with BREAK
        ur_endif
        UF rpop base @ umul + rpush
      ur_endif
      ;; skip char
      UF 1dec swap 1inc swap
    ur_repeat
  ur_endif
  UF rpop
urword_end

;; k8: non-conforming, because we cannot parse double numbers
urword_forth ">NUMBER",in_number
;; ( ud1 c-addr1 u1 -- ud2 c-addr2 u2 )
  UF rot drop rot
  UF par_number_parse_simple
  UF nrot 0 nrot
urword_end


urword_forth "NUMBER-PARSE-PFX-SIGIL",number_parse_sigil
;; ( addr count -- addr count newbase )
  ;; simple sigils
  UF dup 1 great
  ur_ifnot
    UF 0 exit
  ur_endif
  ;; simple sigils
  UF over cpeek
  ;; ( addr count char )
  UF dup 36 equal  ;; dollar
  ur_if
    UF drop swap 1inc swap 1dec
    UF 16 exit
  ur_endif
  UF dup 35 equal  ;; hash
  ur_if
    UF drop swap 1inc swap 1dec
    UF shit2012shit 1 and
    ur_if
      UF 10
    ur_else
      UF 16
    ur_endif
    UF exit
  ur_endif
  UF 37 equal  ;; percent
  ur_if
    UF swap 1inc swap 1dec
    UF 2 exit
  ur_endif
  UF 0
urword_end

urword_forth "NUMBER-PARSE-PFX-0X",number_parse_0x
;; ( addr count -- addr count newbase )
  UF dup 2 great
  ur_ifnot
    UF 0 exit
  ur_endif
  UF over cpeek 48 equal
  ur_ifnot
    UF 0 exit
  ur_endif
  UF over 1inc cpeek upcase_char
  UF dup 88 equal  ;; X
  ur_if
    ;; ( addr count char )
    UF drop swap 2inc swap 2dec
    UF 16 exit
  ur_endif
  UF dup 79 equal  ;; O
  ur_if
    UF drop swap 2inc swap 2dec
    UF 8 exit
  ur_endif
  UF dup 66 equal  ;; B
  ur_if
    UF drop swap 2inc swap 2dec
    UF 2 exit
  ur_endif
  UF 68 equal  ;; D
  ur_if
    UF swap 2inc swap 2dec
    UF 10 exit
  ur_endif
  UF 0
urword_end

urword_forth "NUMBER-PARSE-PFX-&",number_parse_pfxamp
;; ( addr count -- addr count newbase )
  UF dup 2 great
  ur_ifnot
    UF 0 exit
  ur_endif
  UF over cpeek 38 equal
  ur_ifnot
    UF 0 exit
  ur_endif
  UF over 1inc cpeek upcase_char
  UF dup 72 equal  ;; H
  ur_if
    ;; ( addr count char )
    UF drop swap 2inc swap 2dec
    UF 16 exit
  ur_endif
  UF dup 79 equal  ;; O
  ur_if
    UF drop swap 2inc swap 2dec
    UF 8 exit
  ur_endif
  UF dup 66 equal  ;; B
  ur_if
    UF drop swap 2inc swap 2dec
    UF 2 exit
  ur_endif
  UF 68 equal  ;; D
  ur_if
    UF swap 2inc swap 2dec
    UF 10 exit
  ur_endif
  UF 0
urword_end


urword_forth "(NUMBER-GOOD-BIN-DIGIT?)",par_number_isbindig
urword_hidden
;; ( ch -- flag )
  UF dup 95 equal
  ur_if
    UF drop true exit
  ur_endif
  UF 2 digitq
urword_end

urword_forth "(NUMBER-PARSE-BOOL?)",number_parse_boolq
urword_hidden
;; ( addr count -- flag )
  UF dup 0great
  ur_ifnot
    UF 2drop 0 exit
  ur_endif
  ;; first must be digit
  UF over cpeek 2 digitq
  ur_ifnot
    UF 2drop 0 exit
  ur_endif
  UF true nrot
  UF over + swap
  ur_do
    ;; ( okflag )
    UF i cpeek par_number_isbindig
    ur_ifnot
      UF drop false leave
    ur_endif
  ur_loop
urword_end

urword_forth "NUMBER-PARSE-SFX",number_parse_sfx
;; ( addr count -- addr count newbase )
  ;; check suffixes
  UF dup 2 great
  ur_ifnot
    UF 0 exit
  ur_endif
  UF 2dup + 1dec cpeek upcase_char
  UF dup 72 equal  ;; H
  ur_if
    UF drop 1dec
    UF 16 exit
  ur_endif
  UF dup 79 equal  ;; O
  ur_if
    UF drop 1dec
    UF 8 exit
  ur_endif
  ;; the following suffix is allowed only if all digits are right
  UF 66 equal  ;; B
  ur_if
    UF 1dec
    ;; ( addr count )
    UF 2dup number_parse_boolq
    ur_if
      UF 2 exit
    ur_endif
    UF 1inc
  ur_endif
  UF 0
urword_end


;; will return base according to prefix/suffix, and remove pfx/sfx from the string
;; returns 0 if no special base change found
urword_forth "NUMBER-PARSE-PFX-SFX",number_parse_prefix_suffix
;; ( addr count -- addr count newbase )
  UF number_parse_sigil qdup
  ur_if
    UF exit
  ur_endif
  UF number_parse_0x qdup
  ur_if
    UF exit
  ur_endif
  UF number_parse_pfxamp qdup
  ur_if
    UF exit
  ur_endif
  UF number_parse_sfx
urword_end


urword_forth "NUMBER",number
;; ( addr count -- n true // false )
; convert a character string left at addr to a signed number, using the current numeric base.
  ;; check length
  UF dup 0lessequ
  ur_if
    UF 2drop 0 exit
  ur_endif
  ;; ok, we have at least one valid char
  ;; check for leading minus (only if 2012 so-called-standard idiocity is not turned on)
  UF shit2012shit 1 and
  ur_if
    UF 0
  ur_else
    UF over cpeek 45 equal
    ur_if
      UF swap 1inc swap 1dec 1
    ur_else
      UF 0
    ur_endif
  ur_endif
  UF nrot
  ;; ( negflag addr count )
  UF base @ rpush  ;; it can be changed by number prefix/suffix
  UF number_parse_prefix_suffix
  ;; done checking
  UF qdup
  ur_if
    UF base !
  ur_endif
  ;; check for leading minus (only if 2012 so-called-standard idiocity is turned on)
  UF shit2012shit 1 and
  ur_if
    UF dup 0great
    ur_if
      UF over cpeek 45 equal
      ur_if
        UF swap 1inc swap 1dec
        UF rot drop 1 nrot
      ur_endif
    ur_endif
  ur_endif
  ;; zero count means "nan"
  UF dup 0great
  ur_ifnot
    ;; restore base
    UF rpop base !
    ;; exit with failure
    UF 2drop drop 0 exit
  ur_endif
  ;; ( negflag addr count | oldbase )
  UF 0 par_number_parse_simple
  ;; ( negflag addr count u | oldbase )
  ;; restore base
  UF rpop base !
  ;; ( negflag addr count u )
  ;; if not fully parsed, it is nan
  UF swap
  ;; ( negflag addr u count )
  ur_if
    ;; exit with failure
    UF drop 2drop 0 exit
  ur_endif
  ;; ( negflag addr u )
  UF nip swap
  ur_if
    UF negate
  ur_endif
  ;; success
  UF 1
urword_end


;; scans TIB, returns parsed word
;; doesn't do any copying
;; trailing delimiter is skipped
;; HACK: sets (WORD-LAST-DELIMITER)
;;       this is so "comment-to-eol" could work
urword_code "(WORD)",par_word
urword_hidden
;; ( c skip-leading-delim? -- addr count )
  ; reset last delimiter (it will be set later)
  mov   edx,[fvar_tibsize_data]
  sub   edx,[fvar_inptr_data]
  jc    .noinput_drop
  mov   edi,[fvar_tib_data]
  add   edi,[fvar_inptr_data]
  ; do we need to skip leading delimiters?
  jecxz .noskipdel
  pop   TOS   ; get char in TOS
  ; TOS=char
  ; EDI=tibptr
  ; EDX=tibleft
  ; skip leading delimiters
.skipdel_loop:
  or    edx,edx
  jz    .noinput
  call  .cmp_cl_memedi
  jz    .skipchar
  or    al,al
  jz    .noinput
  jmp   .startword
.skipchar:
  call  .count_lines
  inc   edi
  dec   edx
  jmp   .skipdel_loop

.noskipdel:
  pop   TOS   ; get char in TOS

.startword:
  ; remember current position
  push  edi
  ; skip until delimiter
.collect_loop:
  or    edx,edx
  jz    .done_noadv
  ; count lines here, so we won't have to call it on word completion
  call  .count_lines
  call  .cmp_cl_memedi
  jz    .done
  or    al,al
  jz    .done_noadv
  inc   edi
  dec   edx
  jmp   .collect_loop

.done:
  ; skip delimiter
  or    edx,edx
  jr    z,.done_noadv
  cmp   byte [edi],0
  jr    z,.done_noadv
  dec   edx
  movzx eax,byte [edi]
  ld    [fvar_par_last_read_char_data],eax
.done_noadv:
  ; the word is never empty here
  ; stack: word start
  ; EDI: word end (after the last char, at a delimiter)
  ; EDX=tibleft
  ; fix inptr
  mov   eax,[fvar_tibsize_data]
  sub   eax,edx
  mov   [fvar_inptr_data],eax
  ; calc and store counter
  pop   ecx     ; start address
  mov   eax,edi ; EAX=end address
  sub   eax,ecx ; EAX=length
  ; truncate length
  ld    edx,1020
  cp    eax,edx
  cmovnc eax,edx
  ; ECX=start address
  ; EDI=end address
  ; EAX=length
  ; copy bytes
  push  TOS
  ld    TOS,eax
  urnext

.noinput_drop:
  add   esp,4   ; drop char, as we didn't poped it yet
.noinput:
  ; TOS=char
  ; EDX=tibleft
  mov   eax,[fvar_tibsize_data]
  sub   eax,edx
  ld    edx,0
  cmovc eax,edx
  mov   [fvar_inptr_data],eax
  ; push current tib position, and zero length
  add   eax,[fvar_tib_data]
  push  eax
  xor   TOS,TOS
  urnext

; zero flag: equality
; al: byte at [edi]
.cmp_cl_memedi:
  movzx eax,byte [edi]
  ld    [fvar_par_last_read_char_data],eax
  cmp   al,1
  jnc   .cmp_cl_memedi_nonzero
  ret
.cmp_cl_memedi_nonzero:
  cmp   cl,32
  jnz   .cmp_cl_memedi_ok
  ; coerce to space
  cmp   al,32
  jnc   .cmp_cl_memedi_ok
  mov   al,32
.cmp_cl_memedi_ok:
  cmp   al,cl
  ret

.count_lines:
  cp    byte [edi],10
  jr    nz,@f
  cp    dword [fvar_tiblineno_data],0
  jr    z,@f
  inc   dword [fvar_tiblineno_data]
@@:
  ret
urword_end

;; this is a leftover from '"WORD" is always using real here'
;urword_forth "WORD-HERE",word_here
;  UF dp @
;urword_end
urword_alias "WORD-HERE",word_here,here

urword_forth "(WORD-OR-PARSE)",par_word_or_parse
urword_hidden
;; ( c skip-leading-delim?  -- wordhere )
  UF par_word
  UF 1020 umin        ;; truncate length
  UF dup word_here !  ;; set counter
  UF word_here cellinc swap move  ;; copy string
  UF word_here count + 0cpoke     ;; put trailing zero byte
  UF word_here
urword_end

;; HACK: sets (WORD-LAST-DELIMITER)
;;       this is so "comment-to-eol" could work
;; WARNING! it is using "HERE", so "DP-TEMP" is in effect
;; artificial word length limit: 1020 chars
;; longer words will be properly scanned, but truncated
;; adds trailing zero after the string (but doesn't include it in count)
;; string is cell-counted
urword_forth "WORD",word_obsolete
;; ( c  -- wordhere )
  UF 1 par_word_or_parse
urword_end

urword_forth "PARSE-TO-HERE",parse_to_here
;; ( c -- wordhere )
  UF 0 par_word_or_parse
urword_end

urword_forth "PARSE",parse
;; ( c -- addr count )
  UF 0 par_word       ;; parse, don't skip leading delimiters
urword_end

urword_forth "PARSE-SKIP-BLANKS",par_parse_skip_blanks
;; ( -- )
  ur_begin
    UF tib_peekch qdup
  ur_while
    UF 32 great
    ur_if
      UF exit
    ur_endif
    UF tib_getch drop
  ur_repeat
urword_end

urword_forth "PARSE-SKIP-BLANKS-NO-EOL",par_parse_skip_blanks_no_eol
;; ( -- )
  ur_begin
    UF tib_peekch qdup
  ur_while
    UF dup 32 great
    ur_if
      UF drop exit
    ur_endif
    UF dup 13 equal swap 10 equal or
    ur_if
      UF exit
    ur_endif
    UF tib_getch drop
  ur_repeat
urword_end

urword_forth "PARSE-SKIP-BLANKS-EX",par_parse_skip_blanks_ex
;; ( skipeol-flag -- )
  ur_if
    UF par_parse_skip_blanks
  ur_else
    UF par_parse_skip_blanks_no_eol
  ur_endif
urword_end


;; simple heuristic: if the word ends with ")", try to find it in current vocabs
;; as our searching is very fast, and this word is not invoked that often, it is ok
urword_forth "(PARSE-GOOD-COMMENT?)",par_parse_good_commentq
;; ( addr count -- addr count flag )
if 0
    UF 2dup endcr pardottype "CHECKING: <" type pardottype ">: "
  UF 2dup wfind_str dup
    UF dup dot cr
  ur_if
    UF nip
  ur_endif
  UF not
else
  UF true
end if
urword_end

;; sorry for this huge mess
urword_forth "(PARSE-NAME-EX)",par_parse_name_ex
urword_hidden
;; ( -- addr count 1 // 0 )
  UF par_tibstate_rpush
  UF bl 1 par_word
 if 0
  ;; if word length is enough for normal comment processing, do it
  UF dup 1 great
  ur_ifnot
    ;; empty word, or one-char word
    UF par_tibstate_rdrop true exit
  ur_endif
  ;; check for single-char backslash
  UF over cpeek 92 equal
  ur_if
    UF par_parse_good_commentq
    ur_if
      UF 2drop par_tibstate_rpop
      UF par_parse_skip_blanks
      ;; skip word char
      UF tib_getch drop
      UF comment_toeol
      UF false exit
    ur_else
      UF true exit
    ur_endif
  ur_endif
 end if
  ;; check for double-char comments
  UF dup 2 great
  ur_ifnot
    UF par_tibstate_rdrop true exit
  ur_endif
  ;; check two chars at once
  UF over wpeek
  ;; two semicolons or two slashes
  UF dup 0x3b3b equal over 0x2f2f equal or
  ur_if
    UF drop
    UF par_parse_good_commentq
    ur_if
      UF 2drop par_tibstate_rpop
      UF par_parse_skip_blanks
      ;; skip word char
      UF tib_getch drop
      UF comment_toeol
      UF false exit
    ur_else
      UF true exit
    ur_endif
  ur_endif
  ;; (* and (+
  UF dup 0x2a28 equal over 0x2b28 equal or
  ur_if
    ;; UF nrot 2drop par_tibstate_rpop
    UF nrot par_parse_good_commentq
    ur_if
      UF 2drop par_tibstate_rpop
      ;; ( 2chars )
      ;; skip to the word
      UF par_parse_skip_blanks
      ;; skip two word chars
      UF tib_getch tib_getch 2drop
      UF 0x2a28 equal
      ur_if
        UF comment_multiline
      ur_else
        UF comment_multiline_nested
      ur_endif
      UF false exit
    ur_endif
  ur_endif
  ;; not a comment
  UF drop par_tibstate_rdrop
  UF true
urword_end

urword_forth "PARSE-NAME",parse_name
;; ( -- addr count )
  UF bl 1 par_word    ;; parse, skip leading delimiters
urword_end

urword_forth "PARSE-NAME-EX",parse_name_ex
;; ( -- addr count )
  UF parse_name_adv_comments
  ur_if
    ;; save current tib, so we will be able to reparse comments
    ur_begin
      UF par_parse_name_ex
    ur_until
  ur_else
    UF parse_name
  ur_endif
urword_end

