;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; general machine instruction builder (based on various parsed fields)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 value *OpArray  \ used by various opcode doers, to store pointer to internal data

0 value *AdSize   \ if 2, emit "address size" prefix
0 value *OpSize   \ if 2, emit "data size" prefix
0 value *SegReg   \ if >= 0, emit segment override prefix
0 value *OpcSize  \ number of bytes in `*OpCode` (1 to 4); if 0, `Build-Instruction` will do nothing
0 value *OpCode   \ command bytes

0 value *ImSize   \ set by `Imm`
0 value *Imm      \ set by `Imm`
0 value *OpReloc    \ set to number of labels used in `*Imm` (by `Imm`); for now, it could be only 0 or 1

0 value *Mod      \ set by `?MemReg`
0 value *Reg      \ set by `?Reg`
0 value *R/M      \ set by `?MemReg`
0 value *Scale    \ set by `?MemReg`
0 value *Index    \ set by `?MemReg`
0 value *Base     \ set by `?MemReg`
0 value *OfSize   \ set by `?MemReg`
0 value *Offset   \ set by `?MemReg`
0 value *OffReloc    \ set to number of labels used in `?MemReg`; for now, it could be only 0 or 1

\ allow data size overrides bigger than dword?
0 value *PtrKingSize?

\ JCond original PC (-1 means "no jcond")
-1 value *JCondPC

0 value *ImmForthType
0 value *OffForthType
0 value *GenForthType

\ is label used in expression defined?
false value *ImmLabelDefined
false value *OffLabelDefined
false value *GenLabelDefined

0 value *Imm-Allow-Other-Delims?

\ labels found while parsing
\ byte-counted strings
256 buffer: *OffName  \ offset to
256 buffer: *ImmName  \ immediate
256 buffer: *GenName  \ `Label?` should put byte-counted string here


hidden:: (Reset-Gen-Label)  ( -- )
  0 to *GenForthType
  false to *GenLabelDefined
  *GenName 0!
;


;; reset all variables (i.e. prepare to parsing)
: Reset-Instruction  ( -- )
  0 to *AdSize
  0 to *OpSize
  -1 to *SegReg
  0 to *OpcSize
  0 to *ImSize
  0 to *Imm
  0 to *OpReloc
  -1 to *Mod
  -1 to *Reg
  -1 to *R/M
  -1 to *Scale
  -1 to *Index
  -1 to *Base
  0 to *OfSize
  0 to *Offset
  0 to *OffReloc
  0 to *PtrKingSize?
  false to *ImmLabelDefined
  false to *OffLabelDefined
  false to *GenLabelDefined
  0 to *Imm-Allow-Other-Delims?
  0 to *ImmForthType
  0 to *OffForthType
  0 to *GenForthType
  *OffName 0!
  *ImmName 0!
  *GenName 0!
  asm-PC to asm-$
;


: (Build-Seg-Prefix)  ( -- )
  *SegReg
  case
    0 of ( ES: ) $26 endof
    1 of ( CS: ) $2E endof
    2 of ( SS: ) $36 endof
    3 of ( DS: ) $3E endof
    4 of ( FS: ) $64 endof
    5 of ( GS: ) $65 endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  asm-c,
;


: (Build-Prefixes)  ( -- )
  ;; put size prefixes
  *AdSize 2 = if $67 asm-c, endif
  *OpSize 2 = if $66 asm-c, endif
  ;; put segment override prefix
  *SegReg 0< ifnot (Build-Seg-Prefix) endif
;

: (Build-Fixed-Opcode)  ( -- )
  ;; put fixed opcode part
  *OpCode *OpcSize asm-#,
;

: (Build-Mod/R/M)  ( -- )
  ;; has mod?
  *Mod 0< ifnot
    ;; generate mod/r/m
    *Mod 3 and 6 lshift
    *Reg 7 and 3 lshift or
    *R/M 7 and or
    asm-c,
  endif
;

: (Build-SIB)  ( -- )
  ;; has scale and mod?
  *Scale 0< *Mod 0< or ifnot
    ;; generate sib
    *Scale 3 and 6 lshift
    *Index 7 and 3 lshift or
    *Base 7 and or
    asm-c,
  endif
;

: (Build-OffReloc)  ( -- )
  ;; notify label manager about possible offset fixup
  *OffReloc if
    *OfSize 4 = ERRID_ASM_EXPECT_32BIT_OFFS not-?asm-error
    *OffName bcount *OfSize *OffForthType asm-Label-Fixup
  endif
;

: (Build-OfSize)  ( -- )
  ;; generate i don't fuckin' know what
  *OfSize if
    *Offset *OfSize asm-#,
  endif
;

: (Build-OpReloc)  ( -- )
  ;; notify label manager about possible immediate fixup
  *OpReloc if
    *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
    *ImmName bcount *ImSize *ImmForthType asm-Label-Fixup
  endif
;

: (Build-Immediate)  ( -- )
  ;; generate immediate value
  *ImSize if
    *Imm *ImSize asm-#,
  endif
;

: Build-Instruction  ( -- )
  *OpcSize if
    (Build-Prefixes)
    (Build-Fixed-Opcode)
    (Build-Mod/R/M)
    (Build-SIB)
    (Build-OffReloc)
    (Build-OfSize)
    (Build-OpReloc)
    (Build-Immediate)
  endif
;
