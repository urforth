;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various instructions from different groups
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: IncDec  ( name ( byte_opc b_reg word_opc w_reg reg_baseopc dummy -- )
                 ( -- )
  \ one-byte opcodes only, but with possible "reg" modifier
  create 3 0 do swap c, c, loop
 does>
  to *OpArray
  Reset-Instruction
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize ERRID_ASM_EXPECT_TYPE_OVERRIDE not-?asm-error
  1 to *OpcSize
  *Mod 3 = *OpSize 1 > and
  if
    -1 to *Mod
    *OpArray c@ *R/M +
  else
    *OpSize 1 = if 4 else 2 endif
    *OpArray + dup 1+ c@ to *Reg
    c@
  endif
  to *OpCode
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: NegNotMulDiv  ( name ( byte_opc b_reg word_opc w_reg -- )
                       ( -- )
  \ one-byte opcodes only, but with possible "reg" modifier
  create 2 0 do swap c, c, loop
 does>
  to *OpArray
  Reset-Instruction
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize ERRID_ASM_EXPECT_TYPE_OVERRIDE not-?asm-error
  1 to *OpcSize
  *OpArray *OpSize 1 = if 2 + endif
  dup 1+ c@ to *Reg
  c@ to *OpCode
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: LxS  ( name ( opcode opcsize -- )
              ( -- )
  create c, w,
 does>
  Reset-Instruction
  dup c@ to *OpcSize
  1+ w@ to *OpCode
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize to *OpArray
  *OpSize 2 < ERRID_ASM_TYPE_MISMATCH ?asm-error
  ExpectComma
  *OpSize 2 = *OpCode $8D = or  \ Opcode 8D belongs to LEA
  if
    *OpCode $8D = if 0 else 4 endif to *OpSize
    ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
    *Mod 3 = ERRID_ASM_INVALID_OPERAND ?asm-error
    *OpCode $8D = if *OpArray else 2 endif to *OpSize
  else
    ?Ptr ERRID_ASM_TYPE_MISMATCH ?asm-error
    ?FWord drop 4 to *OpSize
    ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
    *Mod 3 = ERRID_ASM_INVALID_OPERAND ?asm-error
  endif
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Shift  ( name ( reg -- )
                ( -- )
  \ hardcoded opcodes - "reg" modifiers only
  create c,
 does>
  Reset-Instruction
  c@ to *Reg
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  [char] , ?CheckDelim ifnot
    ;; default arg is 1
    *OpSize 1 = if $D0 else $D1 endif
    0 to *ImSize
  else
    ;;ExpectComma
    ?Reg8 if
      1 = ERRID_ASM_INVALID_OPERAND not-?asm-error
      *OpSize 1 = if $D2 else $D3 endif
    else
      Imm *Imm $FF U> ERRID_ASM_NUMBER_TOO_BIG ?asm-error
      *Imm 1 = if
        *OpSize 1 = if $D0 else $D1 endif
        0 to *ImSize
      else
        *OpSize 1 = if $C0 else $C1 endif
        1 to *ImSize
      endif
    endif
  endif
  to *OpCode
  1 to *OpcSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: MOVxX  ( name ( opc1 opc2 -- )
                ( -- )
  \ two-byte opcodes only
  create w, w,
 does>
  Reset-Instruction
  to *OpArray
  2 to *OpcSize
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize >r
  0 to *OpSize
  ExpectComma
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 4 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  *OpSize 2 = r> to *OpSize
  if
    *OpSize 4 = ERRID_ASM_TYPE_MISMATCH not-?asm-error
    *OpArray w@ to *OpCode
  else
    *OpArray 2+ w@ to *OpCode
  endif
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Bit  ( name ( c1opc c1reg c2opc c2reg -- )
              ( -- )
  \ two-byte opcodes only, with possible "reg" modifier
  create w, c, w, c,
 does>
  Reset-Instruction
  to *OpArray
  2 to *OpcSize
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 2 < ERRID_ASM_INVALID_OPERAND ?asm-error
  ExpectComma
  ?Reg if
    *OpArray 3 +
  else
   Imm
   *Imm -128 128 within ERRID_ASM_NUMBER_TOO_BIG not-?asm-error
   1 to *ImSize
   *OpArray 2+ c@ to *Reg
   *OpArray
  endif
  w@ to *OpCode
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SETxx  ( name ( opc -- )
                ( -- )
  \ two-byte opcodes only
  create w,
 does>
  Reset-Instruction
  w@ to *OpCode
  2 to *OpcSize
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 1 = ERRID_ASM_INVALID_OPERAND not-?asm-error
  0 to *Reg
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SHxD  ( name ( opc1 opc2 -- )
               ( -- )
  \ two-byte opcodes only
  create w, w,
 does>
  Reset-Instruction
  to *OpArray
  2 to *OpcSize
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 2 < ERRID_ASM_INVALID_OPERAND ?asm-error
  ExpectComma
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ExpectComma
  ?Reg8 if
    1 = ERRID_ASM_INVALID_OPERAND not-?asm-error
    *OpArray
  else
    Imm *Imm $FF > ERRID_ASM_NUMBER_TOO_BIG ?asm-error
    *OpArray 2+
  endif
  w@ to *OpCode
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: BSx  ( name ( word -- )
              ( -- )
  create w,
 does>
  Reset-Instruction w@ to *OpCode
  2 to *OpcSize
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 2 < ERRID_ASM_INVALID_OPERAND ?asm-error
  ExpectComma
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: RETx  ( name ( opc1 opc2 -- )
               ( -- )
  create swap c, c,
 does>
  Reset-Instruction
  1 to *OpcSize
  lexer:tktype if
    1+
    2 to *OpSize
    Imm
    *ImSize 4 = ERRID_ASM_NUMBER_TOO_BIG ?asm-error
    2 to *ImSize
  endif
  c@ to *OpCode
  0 to *OpSize
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: AAx  ( name ( opc -- )
              ( -- )
  create c,
 does>
  c@
  Reset-Instruction
  lexer:tktype if
    1 to *OpSize
    Imm
    *ImSize 1 > ERRID_ASM_NUMBER_TOO_BIG ?asm-error
    *Imm
  else
    10
  endif
  Reset-Instruction
  2 to *OpcSize
  8 LShift + to *OpCode
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: LxxCMOV  ( name ( opc -- )
                  ( -- )
  create bswap-word w,
 does>
  Reset-Instruction
  w@ to *OpCode
  2 to *OpcSize
  ?Reg dup if drop *OpSize 1 > endif ERRID_ASM_INVALID_OPERAND not-?asm-error
  ExpectComma
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  Build-Instruction
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: P5Op  ( name ( opc1 opc2 -- )
               ( -- )
  create swap bswap-word w, bswap-word w,
 does>
  Reset-Instruction
  2 to *OpcSize
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ExpectComma
  ?Reg ERRID_ASM_INVALID_OPERAND not-?asm-error
  *OpSize 1 = ifnot 2+ endif w@ to *OpCode
  Build-Instruction
;
