;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define jump/call/loop instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: CondAllowed?  ( addr count -- flag )
  over c@ [char] J = if 2drop false exit endif  \ all jumps will do it by themselves
  2dup " MOV" s= if 2drop false exit endif  \ it does this by itself
  2dup " LD" s= if 2drop false exit endif  \ it does this by itself
  ;; loops too
  4 >= if
    4 " LOOP" s= not
  else
    drop true
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dispsize is in bytes
: (Build-JxxDisp)  ( dispsize -- )
  ;; sanity check
  dup 1 = over 4 = or ERRID_ASM_INTERNAL_ERROR not-?asm-error
  ;; label?
  *ImmName c@ if
    ;; register fixup (anyway)
    dup >r *ImmName bcount r> 1 = *ImmForthType swap
    if asm-JR-Label-Fixup else asm-Jmp-Label-Fixup endif
    ;; for undefined labels, store imm offset
    \ *ImmName bcount asm-Label-Defined?
    *ImmLabelDefined
    ifnot
      1 = if
        *Imm -128 128 within ERRID_ASM_JUMP_OUT_OF_RANGE not-?asm-error
        *Imm asm-c,
      else
        *Imm asm-,
      endif
      exit
    endif
  endif
  1 = if
    *Imm asm-PC 1+ -
    dup -128 128 within ERRID_ASM_JUMP_OUT_OF_RANGE not-?asm-error
    asm-c,
  else
    *Imm asm-PC 4+ - asm-,
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: JRx  ( -- )
  create
 does>
  drop  ;; we have no data
  Reset-Instruction
  4 to *OpSize
  ?JCond if
    $70 or  ;; cond opcode
    ExpectComma
  else
    $EB
  endif
  Imm
  asm-c,  ;; opcode
  1 (Build-JxxDisp)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; jump instructions should generate reloc table and other important things
;; this is used for near jumps and calls
: (Jxx-ShortOpcSize)  ( -- n )  *OpArray c@ ;
: (Jxx-ShortOpc)  ( -- n )  *OpArray 1+ w@ ;
: (Jxx-NearOpcSize)  ( -- n )  *OpArray 3 + c@ ;
: (Jxx-NearOpc)  ( -- n )  *OpArray 4+ w@ ;
: (Jxx-IndirOpcSize)  ( -- n )  *OpArray 6 + c@ ;
: (Jxx-IndirOpc)  ( -- n )  *OpArray 7 + w@ ;
: (Jxx-IndirReg)  ( -- n )  *OpArray 9 + c@ ;

;; call this BEFORE any code was generated!
: (Jxx-GoodShortDist?)  ( -- flag )
  *Imm asm-PC (Jxx-ShortOpcSize) + 1+ - -128 128 within
;

: (Jxx-CanUseShort)  ( -- flag )
  ;; early exit
  (Jxx-ShortOpcSize) ifnot false exit endif

  ;; if we got a label, check if we can rely on its value
  *ImmName c@ if
    ;; if we don't have near form, allow short one in any case
    ;; label manager fixuper should take care of distance checks later
    (Jxx-NearOpcSize) ifnot true exit endif
    ;; undefined (forward) labels always generate near form
    \ *ImmName bcount asm-Label-Defined?
    *ImmLabelDefined
    ifnot
      false exit
    endif
  endif

  (Jxx-GoodShortDist?)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; translate z80-like "jmp cond,dest"
: (Jxx-Cond-Trans)  ( -- doneflag )
  ;; sorry for this hack
  ;; we'll rebuild the token, yeah
  ;; sanity check: should have both near and short versions (see jump opcodes)
  (Jxx-ShortOpcSize) (Jxx-NearOpcSize) logand ifnot false exit endif
  ?JCond ifnot false exit endif
  ExpectComma
  case
    0 of " JO" endof
    1 of " JNO" endof
    2 of " JC" endof
    3 of " JNC" endof
    4 of " JZ" endof
    5 of " JNZ" endof
    6 of " JNA" endof
    7 of " JA" endof
    8 of " JS" endof
    9 of " JNS" endof
   10 of " JPE" endof
   11 of " JPO" endof
   12 of " JL" endof
   13 of " JGE" endof
   14 of " JLE" endof
   15 of " JG" endof
   ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  lexer:asm-pad-addr 300 + c4s-copy-a-c lexer:asm-pad-finish
  lexer:asm-pad-addr 300 + IFind ERRID_ASM_INTERNAL_ERROR not-?asm-error
  execute
  true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: Jxx  ( name ( indir_opcsize i_opc i_reg near_sze n_opc short_sz s_opc -- )
              ( -- )
   create
   swap c, w, swap c, w, rot c, swap w, c,
   ;; +0: short size (byte)
   ;; +1: short opcode (word)
   ;; +3: near size (byte)
   ;; +4: near opcode (word)
   ;; +6: indir opsize (byte)
   ;; +7: indir opcode (word)
   ;; +9: indir reg (byte)
 does>
  to *OpArray

  ;; sorry for this hack
  (Jxx-Cond-Trans) if exit endif

  Reset-Instruction
  ;; put "mode select" flag to rstack
  ;; 0: detect; <0: always near; >0: always short
  0 >r
  tk-id? if
    " SHORT" ass=? if
      rdrop
      ;; check if we have a short form at all
      (Jxx-ShortOpcSize) ERRID_ASM_INVALID_OPERAND not-?asm-error
      1 >r
    else
      " NEAR" ass=? if
        rdrop
        ;; check if we have a near form at all
        (Jxx-NearOpcSize) ERRID_ASM_INVALID_OPERAND not-?asm-error
        -1 >r
      endif
    endif
  endif

  0 to *AdSize

  ;; indirect with register?
  ?MemReg if
    rdrop ;; ignore "range flag" here
    (Jxx-IndirOpcSize) ?dup ERRID_ASM_INVALID_OPERAND not-?asm-error
    to *OpcSize
    (Jxx-IndirOpc) to *OpCode
    (Jxx-IndirReg) to *Reg
    Build-Instruction
    exit
  endif

  4 to *OpSize
  Imm
  ;; need operand? (dunno, this is just a sanity check)
  (Jxx-ShortOpcSize) (Jxx-NearOpcSize) or ERRID_ASM_INVALID_OPERAND not-?asm-error

  ;; generate "use short form" flag
  r> case
    -1 of  \ always near
      false
    endof
    1 of  \ always short, check range
      ;; for undefined labels, `*Imm` will hold offset
      true
    endof
    0 of  \ detect
      (Jxx-CanUseShort)
    endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase

  if
    ;; generate short form (it is guaranteed to exist here, but check it just in case)
    (Jxx-ShortOpc) (Jxx-ShortOpcSize) asm-#,
    1 (Build-JxxDisp)
  else
    ;; generate near form
    ;; it is not guaranteed to exist, so check it
    (Jxx-NearOpcSize) ERRID_ASM_JUMP_OUT_OF_RANGE not-?asm-error
    (Jxx-NearOpc) (Jxx-NearOpcSize) asm-#,
    4 (Build-JxxDisp)
  endif
;
