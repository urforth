;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; define general ALU instructions
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (ALU-Imm-2-4-0)  ( -- opcarray-offset )
  *OpArray
  ;; EAX has special shorter forms
  ;; but 1-byte imm is even shorter
  *ImSize 1 = if 8
  else *Reg if 10
  else ;; EAX
    -1 to *Mod
    -1 to *Scale
    0 to *OfSize
    *OpSize to *ImSize
    14
  endif endif +
  dup 1+ c@ to *Reg
;

: (ALU-Reg)  ( reg -- opcarray-offset )
  ExpectComma
  ?MemReg if
    *OpSize
    case
      1 of *OpArray 2+ endof
      2 of *OpArray endof
      4 of *OpArray endof
      ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error
    endcase
  else
    3 to *Mod
    *Reg to *R/M
    Imm
    *OpSize
    case
      1 of
        *OpArray
        *Reg if 12 else -1 to *Mod -1 to *Scale 0 to *OfSize 16 endif +
        dup 1+ c@ to *Reg
      endof
      2 of (ALU-Imm-2-4-0) endof
      4 of (ALU-Imm-2-4-0) endof
      ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error
    endcase
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (ALU-Imm-2-4-1)  ( -- opcarray-offset )
  *OpArray
  *ImSize 1 = if 8 else 10 endif
  + dup 1+ c@ to *Reg
;

: (ALU-MemReg)  ( -- opcarray-offset )
  ?MemReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  ExpectComma
  ?Reg if
    *OpSize
    case
      1 of *OpArray 6 + endof
      2 of *OpArray cell+ endof
      4 of *OpArray cell+ endof
      ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error
    endcase
  else
    Imm
    *OpSize
    case
      1 of *OpArray 12 + dup 1+ c@ to *Reg endof
      2 of (ALU-Imm-2-4-1) endof
      4 of (ALU-Imm-2-4-1) endof
      ERRID_ASM_EXPECT_TYPE_OVERRIDE asm-error
    endcase
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ALU  ( name ( c1opc c1reg ... c9opc c9reg -- )
              ( -- )
  \ one-byte opcodes only, but with possible "reg" modifier
  \ 8 opcodes
  create 9 0 do swap c, c, loop
 does>
  to *OpArray
  Reset-Instruction
  1 to *OpcSize
  ?Reg if (ALU-Reg) else (ALU-MemReg) endif
  c@ dup to *OpCode
  $F7 = if *OpSize to *ImSize endif
  Build-Instruction
;
