;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiling support
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary asm-meta
also asm-meta definitions

;; used to get current assembling address
;; should return virtual address
: PC  ( -- addr )
  forth:here
;

;; size is never zero or negative
;; returns alloted addres (it will be used with `(asm-c!)`
;; should return virtual address
: n-allot  ( size -- addr )
  forth:n-allot
;

;; the only writing primitive
;; always writing to alloted address
;; accepts virtual address
: c!  ( byte addr -- )
  dup forth:here u< if
    forth:c!
  else
    asmx86:ERRID_ASM_INTERNAL_META_ERROR asmx86:asm-error
  endif
;

;; the only reading primitive
;; always reading from alloted address
;; accepts virtual address
: c@  ( addr -- byte )
  dup forth:here u< if
    forth:c@
  else
    asmx86:ERRID_ASM_INTERNAL_META_ERROR asmx86:asm-error
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous definitions

;; plug in our memory r/w module

' asm-meta:PC to asmx86:asm-PC
' asm-meta:n-allot to asmx86:asm-n-allot
' asm-meta:c@ to asmx86:asm-c@
' asm-meta:c! to asmx86:asm-c!
