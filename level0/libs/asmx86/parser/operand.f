;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; instruction operand parser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

0 value *Sign
0 value *MemRef-Last-Was-Number


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SCopy  ( src-caddr dst-caddr -- )
  over c@ 1+ cmove
;

: StrCCopy  ( src-caddr src-count dst-caddr -- )
  >r
  255 min 0 max
  dup r@ c!
  r> 1+ swap cmove
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tktype=  ( tktype flag )
  lexer:tktype =
;

: tktype-checker  ( tktype -- )
                  ( -- flag )
  create c,
 does>
  c@ tktype=
;

lexer:tktype-eol tktype-checker tk-eol?
lexer:tktype-id tktype-checker tk-id?
lexer:tktype-delim tktype-checker tk-delim?
lexer:tktype-number tktype-checker tk-number?
lexer:tktype-str tktype-checker tk-str?


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this exits the caller with false if the current token is not identifier
: x-check-id  ( -- // false )
  tk-id? ifnot rdrop false endif
;


;; this exits the caller with id and true if the current token matches
: xass=noeat  ( id addr2 count2 -- id true // nothing )
  lexer:tkvalue count s= if rdrop true else drop endif
;

;; this exits the caller with id and true if the current token matches
: xass=  ( id addr2 count2 -- id true // nothing )
  lexer:tkvalue count s= if rdrop true lexer:NextToken else drop endif
;

;; this exits the caller with id and true if the current token matches
: xass=(eatchar)  ( id addr2 count2 -- id true // nothing )
  lexer:tkvalue count s= if rdrop true tib-getch drop lexer:NextToken else drop endif
;

;; this one doesn't jump out
: ass=?  ( addr2 count2 -- true // false )
  lexer:tkvalue count s= dup if lexer:NextToken endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; try to refill input stream until we got non-eol token
;; fail if we can't
: EnsureNotEOL  ( -- )
  begin
    tk-eol?
  while
    tib-peekch ?dup if
      10 = if tib-getch drop endif
    else
      forth:refill ERRID_ASM_UNEXPECTED_EOF not-?asm-error
    endif
    lexer:PrepareLineParser
    lexer:NextToken
  repeat
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ExpectDelimChar  ( ch err -- )
  tk-delim? over not-?asm-error
  swap lexer:tkvalue = swap not-?asm-error
  lexer:NextToken
;

: ExpectComma  ( -- )
  [char] , ERRID_ASM_COMMA_EXPECTED ExpectDelimChar
;

: ExpectColon  ( -- )
  [char] : ERRID_ASM_COLON_EXPECTED ExpectDelimChar
;

: ExpectCommaOrEOL  ( -- )
  lexer:tktype ifnot exit endif
  ExpectComma
;

;; eats delimiter
: ?CheckDelim  ( ch -- flag )
  tk-delim? if
    lexer:tkvalue = dup if lexer:NextToken endif
  else
    drop false
  endif
;

: ?LBracket  ( -- flag )
  [char] [ ?CheckDelim
;

: ?Star  ( -- flag )
  [char] * ?CheckDelim
;

: ?Comma  ( -- flag )
  [char] , ?CheckDelim
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: CheckRelJump  ( n -- n )
  dup -128 < over 128 > or ERRID_ASM_JUMP_OUT_OF_RANGE ?asm-error
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?SegReg  ( -- segreg true // false )
  tib-peekch [char] : = if false exit endif
  x-check-id
  0 " ES" xass=
  1 " CS" xass=
  2 " SS" xass=
  3 " DS" xass=
  4 " FS" xass=
  5 " GS" xass=
  asm-ignore-ts if
    " TS" lexer:tkvalue count s= ERRID_ASM_TS_DISABLED ?asm-error
  else
    4 " TS" xass=  ;; tls register (FS)
  endif
  false
;

: ?SegReg-Colon  ( -- segreg true // false )
  tib-peekch [char] : = ifnot false exit endif
  x-check-id
  0 " ES" xass=(eatchar)
  1 " CS" xass=(eatchar)
  2 " SS" xass=(eatchar)
  3 " DS" xass=(eatchar)
  4 " FS" xass=(eatchar)
  5 " GS" xass=(eatchar)
  asm-ignore-ts if 3 else 4 endif " TS" xass=(eatchar)  ;; tls register (FS)
  false
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?JCond  ( -- n true // false )
  \ ?JCondGlyhs ?dup if exit endif
  x-check-id
   0 " O"  xass=  ;; O=1  (overflow)
   1 " NO" xass=  ;; O=0  (overflow)
   2 " C"  xass=  ;; C=1
   3 " NC" xass=  ;; C=0
   4 " Z"  xass=  ;; Z=1
   5 " NZ" xass=  ;; Z=0
   6 " NA" xass=  ;; C=1 || Z=1
   7 " A"  xass=  ;; C=0 && Z=0
   8 " S"  xass=  ;; S=1
   9 " NS" xass=  ;; S=0
  10 " PE" xass=  ;; P=1  (parity)
  11 " PO" xass=  ;; P=0  (parity)
  12 " L"  xass=  ;; S<>O (sign <> overflow)
  13 " GE" xass=  ;; S=O  (sign = overflow)
  14 " LE" xass=  ;; S<>O || Z=1 (sign <> overflow, or zero)
  15 " G"  xass=  ;; S=O && Z=0  (sign = overflow and non-zero)
  ;; aliases
   2 " B"   xass= ;; C=1
   2 " NAE" xass= ;; C=1
   3 " AE"  xass= ;; C=0
   3 " NB"  xass= ;; C=0
   4 " E"   xass= ;; Z=1
   5 " NE"  xass= ;; Z=0
   6 " BE"  xass= ;; C=1 || Z=1
   7 " NBE" xass= ;; C=0 && Z=0
  10 " P"   xass= ;; P=1  (parity)
  11 " NP"  xass= ;; P=0  (parity)
  12 " NGE" xass= ;; S<>O (sign <> overflow)
  13 " NL"  xass= ;; S=O  (sign = overflow)
  14 " NG"  xass= ;; S<>O || Z=1 (sign <> overflow, or zero)
  15 " NLE" xass= ;; S=O && Z=0  (sign = overflow and non-zero)
  false
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?Reg8  ( -- n true // false )
  x-check-id
  0 " AL" xass=
  1 " CL" xass=
  2 " DL" xass=
  3 " BL" xass=
  4 " AH" xass=
  5 " CH" xass=
  6 " DH" xass=
  7 " BH" xass=
  false
;

: ?Reg16  ( -- n true // false )
  x-check-id
  0 " AX" xass=
  1 " CX" xass=
  2 " DX" xass=
  3 " BX" xass=
  4 " SP" xass=
  5 " BP" xass=
  6 " SI" xass=
  7 " DI" xass=
  false
;

: ?Reg32  ( -- n true // false )
  x-check-id
  ;; register aliases
  1 " TOS" xass=      ;; ECX
  6 " EIP" xass=      ;; ESI
  5 " ERP" xass=      ;; EBP
  2 " FREEREG" xass=  ;; EDX
  ;; normal registers
  0 " EAX" xass=
  1 " ECX" xass=
  2 " EDX" xass=
  3 " EBX" xass=
  4 " ESP" xass=
  5 " EBP" xass=
  6 " ESI" xass=
  7 " EDI" xass=
  false
;

: ?STReg  ( -- n true // false )
  x-check-id
  0 " ST0" xass=
  1 " ST1" xass=
  2 " ST2" xass=
  3 " ST3" xass=
  4 " ST4" xass=
  5 " ST5" xass=
  6 " ST6" xass=
  7 " ST7" xass=
  " ST" ass=? ifnot false exit endif
  [char] ( ?CheckDelim  \ )
  if
    tk-number? ERRID_ASM_INVALID_NUMBER not-?asm-error
    lexer:tkvalue dup 0 7 bounds? ERRID_ASM_NUMBER_TOO_BIG not-?asm-error
    lexer:NextToken
    [char] ) ?CheckDelim ERRID_ASM_SYNTAX_ERROR not-?asm-error
  else
    0
  endif
  true exit
;

: ?MMReg  ( -- n true // false )
  x-check-id
  0 " MM0" xass=
  1 " MM1" xass=
  2 " MM2" xass=
  3 " MM3" xass=
  4 " MM4" xass=
  5 " MM5" xass=
  6 " MM6" xass=
  7 " MM7" xass=
  false
;

: ?CRReg  ( -- n true // false )
  x-check-id
  0 " CR0" xass=
  2 " CR2" xass=
  3 " CR3" xass=
  4 " CR4" xass=
  false
;

: ?DRReg  ( -- n true // false )
  x-check-id
  0 " DR0" xass=
  1 " DR1" xass=
  2 " DR2" xass=
  3 " DR3" xass=
  4 " DR4" xass=
  5 " DR5" xass=
  6 " DR6" xass=
  7 " DR7" xass=
  false
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?Reg  ( -- flag )
  ?Reg32 if
    *OpSize 1 2 bounds? ERRID_ASM_TYPE_MISMATCH ?asm-error
    4 to *OpSize
    to *Reg
    true
    exit
  endif
  ?Reg8 if
    *OpSize 2 4 bounds? ERRID_ASM_TYPE_MISMATCH ?asm-error
    1 to *OpSize
    to *Reg
    true
    exit
  endif
  ?Reg16 if
    *OpSize 4 =  *OpSize 1 =  or  ERRID_ASM_TYPE_MISMATCH ?asm-error
    2 to *OpSize
    to *Reg
    true
    exit
  endif
  false
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (Save-Label-Name)  ( -- )
  lexer:tkvalue count  dup 1 255 within ERRID_ASM_TOKEN_TOO_LONG not-?asm-error
  dup *GenName c!
  *GenName 1+ swap cmove
  lexer:NextToken
;

: (?Label)  ( -- addr|0 true // false )
  x-check-id
  \ TODO: use IFind to reject reserved words here?
  lexer:tkvalue count asm-Get-Label
  ;; copy label name if it is known
  dup if
    (Save-Label-Name)
    ;; if it is defined, returned success flag is positive
    0> dup to *GenLabelDefined
    ;; if not defined, its value is zero
    ifnot drop 0 endif
    true
  endif
  0 to *GenForthType
;

: (?Forth-Label)  ( type -- value true )
  tk-str? ERRID_ASM_STRING_EXPECTED not-?asm-error
  *GenForthType ERRID_ASM_DUPLICATE_LABEL ?asm-error
  to *GenForthType
  lexer:tkvalue count *GenForthType asm-Get-Forth-Word
  dup if
    ;; if it is defined, returned success flag is positive
    ;; but don't mess with the returned value in any way (see label manager API)
    dup 0> to *GenLabelDefined
  endif
  ERRID_ASM_UNRESOLVED_LABEL not-?asm-error
  (Save-Label-Name)
  true
;

;; doesn't eat it
: ?xFA= ( -- n true // false )
  x-check-id
  LABEL-TYPE-CFA " CFA" xass=noeat
  LABEL-TYPE-PFA " PFA" xass=noeat
  \ LABEL-TYPE-NFA " NFA" xass=noeat
  \ LABEL-TYPE-LFA " LFA" xass=noeat
  \ LABEL-TYPE-BFA " BFA" xass=noeat
  \ LABEL-TYPE-SFA " SFA" xass=noeat
  \ LABEL-TYPE-DFA " DFA" xass=noeat
  \ LABEL-TYPE-FFA " HFA" xass=noeat
  false
;

: ?Label  ( -- addr|0 true // false )
  x-check-id
  " OFFSET" ass=? if
    (?Label) ERRID_ASM_LABEL_EXPECTED not-?asm-error
    true
  else
    ?xFA= if lexer:NextToken (?Forth-Label)
    else (?Label)
    endif
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: QStr-To-Num  ( -- n true // false )
  lexer:tkvalue count
  dup 1 5 within ifnot 2drop false exit endif  ;; ERRID_ASM_INVALID_NUMBER not-?asm-error
  ;; ( addr count )
  over + swap
  ;; ( addrhi addrlo )
  0 nrot do
    ;; ( num )
    8 lshift i c@ or
  loop
  lexer:NextToken
  true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?Number  ( -- n true // false )
  lexer:tktype case
    lexer:tktype-delim of
      lexer:tkvalue [char] $ = if
        lexer:NextToken
        asm-$
        true
      else
        false
      endif
    endof
    lexer:tktype-number of
      lexer:tkvalue
      lexer:NextToken
      true
    endof
    lexer:tktype-id of
      ?xFA= if drop false
      else
        lexer:tkvalue count asm-Get-Constant
        dup if lexer:NextToken endif
      endif
    endof
    lexer:tktype-str of
      QStr-To-Num
    endof
    otherwise
      drop false
  endcase
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
: (SignNegate)  ( n -- n // -n )
  *Sign -1 = if negate endif
;


;; current token is a label, process it
: (Imm-ProcessLabel)  ( addr -- )
  ;; if we referenced a label, force immediate size to 4 bytes
  \ only one label reference allowed for now
  *OpReloc ERRID_ASM_TOO_MANY_LABEL_REFS ?asm-error
  *GenName c@ if
    *ImmName c@ ERRID_ASM_INVALID_FORWARD_REF ?asm-error
    *GenName *ImmName SCopy
    *Sign 1 > ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  endif
  ;; if the label is not defined yet, assume that it is zero
  ;; this is required to store proper offset for label manager fixuper
  *GenForthType to *ImmForthType
  *GenLabelDefined to *ImmLabelDefined
  ;; cannot mul to undefined label
  *ImmLabelDefined ifnot
    *Sign [char] * = ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  endif
  (SignNegate) +to *Imm
  1 to *OpReloc
  ;; finally, force immediate size to 4 bytes (but only if it is wasn't forced by a caller)
  ;; if opsize is not specified, assume "anything is ok"
  *OpSize 1 2 bounds? ERRID_ASM_TYPE_MISMATCH ?asm-error
  ;; force it, because why not?
  4 to *OpSize
  ;; wipe "genlabel"
  (Reset-Gen-Label)
;


;; parse optional leading sign (plus or minus)
: (Imm-ParseSign)  ( -- )
  tk-delim? if
    lexer:tkvalue case
      [char] - of -1 to *Sign lexer:NextToken endof
      [char] + of lexer:NextToken endof
    endcase
  endif
;


;; parse immediate, perform label processing, and simple math
: (Imm-ParseImm)  ( -- )
  begin
    ?Number if
      ;; number
      *Sign [char] * = if
        *Imm * to *Imm
      else
        (SignNegate) +to *Imm
      endif
    else
      ?Label if
        (Imm-ProcessLabel)
      else
        ;; not a number, not a label, but we want one
        lexer:tktype if ERRID_ASM_SYNTAX_ERROR else ERRID_ASM_OPERAND_EXPECTED endif asm-error
      endif
    endif

    lexer:tktype lexer:tktype-delim > ERRID_ASM_INVALID_FORWARD_REF ?asm-error
    lexer:tktype if
      ;; not an eol, should be either arith operator, or a comma
      lexer:tkvalue case
        [char] + of  0 to *Sign false endof
        [char] - of -1 to *Sign false endof
        [char] * of [char] * to *Sign false endof
        [char] , of true endof
        otherwise
          drop *Imm-Allow-Other-Delims? ERRID_ASM_SYNTAX_ERROR not-?asm-error
          true
      endcase
      dup ifnot lexer:NextToken endif
    else
      ;; eol
      true
    endif
  until
;


;; setup `*ImSize` according to `*OpSize` (and fix `*OpSize` if necessary)
: (Imm-SetupImSize)  ( -- )
  ;; if we've seen any label, there is nothing really to check
  *OpReloc if
    *OpSize 1 3 bounds? ERRID_ASM_INVALID_FORWARD_REF ?asm-error
    4 to *ImSize exit
  endif

  *OpSize
  case
    0 of  ;; always dword
      4 to *OpSize
      4 to *ImSize
    endof
    1 of
      *Imm -128 256 within ERRID_ASM_NUMBER_TOO_BIG not-?asm-error
      1 to *ImSize
    endof
    2 of
      *Imm -32768 65536 within ERRID_ASM_NUMBER_TOO_BIG not-?asm-error
      *Imm -128 128 within if 1 else 2 endif to *ImSize
     endof
    4 of
      *Imm -128 128 within if 1 else 4 endif to *ImSize
    endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
;


;; parse immediate operand
;; does simple arithmetics (add/sub)
;; the caller may force the maximum immediate size by setting `*OpSize`
;; parsed result will be placed at `*Imm`
;; `*OpReloc` will indicate if any label was used
;; `*OpSize` can be set to maximum operand size allowed;
;; if set to 0, no checks for "minimum size" will be done, and the resulting sizes are always 4
;; also, `*ImSize` will be set to the 1 if immediate fits into byte, or to the resulting `*OpSize`
: Imm  ( -- )
  0 to *Sign
  0 to *Imm
  0 to *OpReloc
  0 to *ImmForthType
  false to *ImmLabelDefined
  (Reset-Gen-Label)
  (Imm-ParseSign)
  (Imm-ParseImm)
  (Imm-SetupImSize)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

: (?Ptr)  ( -- n true // false )
  x-check-id
  4 " DWORD" xass=
  1 " BYTE"  xass=
  2 " WORD"  xass=
  *PtrKingSize? if
     8 " QWORD" xass=
    10 " TBYTE" xass=
  endif
  false
;

: SkipOptPtr  ( -- )
  x-check-id
  " PTR" ass=?
;

: ?Ptr  ( -- flag )
  (?Ptr) ifnot false exit endif
  *OpSize
  if
    *OpSize = ERRID_ASM_TYPE_MISMATCH not-?asm-error
  else
    to *OpSize
  endif
  SkipOptPtr drop
  true
;

: ?FWord  ( -- flag )
  x-check-id
  " FWORD" ass=? ifnot false exit endif
  SkipOptPtr drop
  true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;; sib multiplier
: SIBMult  ( n1 -- n2 )
  case
    1 of 0 endof
    2 of 1 endof
    4 of 2 endof
    8 of 3 endof
    ERRID_ASM_INVALID_SCALE asm-error
  endcase
;

: (Parse-MemRef-R32-Index-Scale)  ( reg -- )
  *Index 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
  tk-number? ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
  *Scale -1 > ERRID_ASM_EXTRA_SCALE ?asm-error
  lexer:tkvalue SIBMult to *Scale
  to *Index
  *Index 4 = ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  lexer:NextToken
;

: (Parse-MemRef-R32-Base-Index)  ( index -- )
  *Index 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
  0 to *Scale
  ;; convert [ebp+esp] to [esp+ebp]
  dup 4 = *Base 5 = and if to *Base 5 endif
  to *Index
  \ *Base to *Index
  \ *Index 4 = ERRID_ASM_INVALID_FORWARD_REF ?asm-error
;

: (Parse-MemRef-R32)  ( reg -- )
  *Sign ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  *AdSize 2 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  4 to *AdSize
  ?Star if
    (Parse-MemRef-R32-Index-Scale)
  else
    *Base 0>= if
      (Parse-MemRef-R32-Base-Index)
    else
      to *Base
    endif
  endif
  false to *MemRef-Last-Was-Number
;


: (Parse-MemRef-R16)  ( reg -- )
  *Sign ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  *AdSize 4 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  2 to *AdSize
  case
    3 of *Base 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error  3 to *Base endof
    5 of *Base 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error  5 to *Base endof
    6 of *Index 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error 6 to *Index endof
    7 of *Index 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error 7 to *Index endof
    ERRID_ASM_INVALID_FORWARD_REF asm-error
  endcase
  false to *MemRef-Last-Was-Number
;


: (Parse-MemRef-Number-Scale)  ( n -- )
  *MemRef-Last-Was-Number if
    *Offset * to *Offset
  else
    false to *MemRef-Last-Was-Number
    *Index 0< ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
    *Scale -1 > ERRID_ASM_EXTRA_SCALE ?asm-error
    SIBMult to *Scale
    ?Reg32 ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
    to *Index
    *Index 4 = ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  endif
;

: (Parse-MemRef-Number)  ( n -- )
  true to *MemRef-Last-Was-Number
  *AdSize ifnot 4 to *AdSize endif
  *Sign case
    -1 of
      -to *Offset
    endof
    [char] * of
      *Offset * to *Offset
    endof
    swap
    ?Star if
      (Parse-MemRef-Number-Scale)
    else
      +to *Offset
    endif
  endcase
;


: (Parse-MemRef-Label)  ( n -- )
  false to *MemRef-Last-Was-Number
  *AdSize 2 = ERRID_ASM_TYPE_MISMATCH ?asm-error
  ;; only one label allowed
  *OffReloc ERRID_ASM_TOO_MANY_LABEL_REFS ?asm-error
  4 to *AdSize
  *GenName c@ ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
  *OffName c@ ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  *GenName *OffName SCopy
  *GenForthType to *OffForthType
  *GenLabelDefined to *OffLabelDefined
  ;; cannot mul to undefined label
  *OffLabelDefined ifnot
    *Sign [char] * = ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  endif
  ;;k8: wtf i meant here?
  ;;dup asm-PC <> if
  ;;  *Sign ERRID_ASM_INVALID_FORWARD_REF ?asm-error
  ;;endif
  ;; indicate that we got a label
  1 to *OffReloc
  *Sign if -to *Offset else +to *Offset endif
  ;; wipe "genlabel"
  (Reset-Gen-Label)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (Parse-MemRef-CFA)  ( doneflag cfacheck cfaexec -- doneflag )
  rot ifnot
    >r
    execute if
      r> execute true
    else
      rdrop false
    endif
  else
    2drop true
  endif
;


: (Parse-MemRef-Offset)  ( -- )
  0 to *Sign
  0 to *Offset
  0 to *OffForthType
  false to *OffLabelDefined
  false to *MemRef-Last-Was-Number
  (Reset-Gen-Label)
  begin
    0
    ['] ?Reg32 ['] (Parse-MemRef-R32) (Parse-MemRef-CFA)
    ['] ?Reg16 ['] (Parse-MemRef-R16) (Parse-MemRef-CFA)
    ['] ?Number ['] (Parse-MemRef-Number) (Parse-MemRef-CFA)
    ['] ?Label ['] (Parse-MemRef-Label) (Parse-MemRef-CFA)
    ;; one of the above should match
    ERRID_ASM_SYNTAX_ERROR not-?asm-error

    tk-delim? ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
    lexer:tkvalue case
      [char] + of  0 to *Sign false endof
      [char] - of -1 to *Sign false endof
      [char] * of
        *MemRef-Last-Was-Number ERRID_ASM_INVALID_SCALE not-?asm-error
        [char] * to *Sign
        false
      endof
      [char] ] of true endof
      ERRID_ASM_SYNTAX_ERROR asm-error
    endcase
    lexer:NextToken
  until
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (Parse-SegOverride)  ( require-memref-flag -- require-memref-flag )
  ;; segment override?
  ?SegReg-Colon if
    \ TODO: better error message!
    *SegReg -1 <> ERRID_ASM_MEMREF_EXPECTED ?asm-error
    to *SegReg  ;; set "segment override" register (returned by `?SegReg`)
    \ ExpectColon
    ;; segment override always requires memory reference
    drop true
  endif
;

;; this could (or should) be a memory reference ([...])
: (Parse-MemRef)  ( -- flag )
  ?Ptr  ;; "require memory reference" flag
  (Parse-SegOverride)
  ?LBracket if
    drop  ;; we don't need "require" flag anymore
    (Parse-MemRef-Offset)
    true
  else
    ERRID_ASM_MEMREF_EXPECTED ?asm-error
    false
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (MemReg-ParseReg)  ( -- flag )
  *Reg  ;; `?Reg` can modify it, and we have to restore it
  ?Reg if
    ;; register, save it to `*R/M`
    *Reg to *R/M
    to *Reg    ;; restore old register
    3 to *Mod  ;; set "use register" mod/r/m mode
    true
  else
    drop       ;; `*Reg` wasn't changed, no need to restore it
    (Parse-MemRef)
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (MemReg-Fix-OfSize)  ( -- )
  *OfSize ifnot
    *Offset if
      *Offset -128 128 within if
        1 to *OfSize
      else
        4 to *OfSize
        *AdSize 2 = if
          *Offset -32768 65536 within ERRID_ASM_INVALID_OFFSET not-?asm-error
          2 to *OfSize
        endif
      endif
    endif
  endif

  *OffReloc if 4 to *OfSize endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (MemReg-AdSize-2-Base-3)  ( -- r/m )
  *Index
  case
    6 of 0 endof
    7 of 1 endof
    otherwise drop 7
  endcase
;

: (MemReg-AdSize-2-Base-5)  ( -- r/m )
  *Index
  case
    6 of 2 endof
    7 of 3 endof
    otherwise drop 6
  endcase
;

: (MemReg-AdSize-2-Base-Other)  ( -- r/m )
  *Index
  case
    6 of 4 endof
    7 of 5 endof
    otherwise
      drop
      0 to *Mod
      2 to *OfSize
      6
  endcase
;

: (MemReg-Check-AdSize-2-Base)  ( -- )
  *Base
  case
    3 of (MemReg-AdSize-2-Base-3) endof
    5 of (MemReg-AdSize-2-Base-5) endof
    otherwise drop (MemReg-AdSize-2-Base-Other)
  endcase
  to *R/M
;

: (MemReg-Check-AdSize-2)  ( -- )
  *AdSize 2 = ifnot exit endif
  *OfSize to *Mod
  *Base 6 = *Index 0< *OfSize 0= and and
  if
    1 to *Mod
    0 to *Offset
    1 to *OfSize
  endif
  (MemReg-Check-AdSize-2-Base)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (MemReg-AdSize-4-No-Base)  ( -- )
  0 to *Mod
  *Index 0< if 5 to *R/M else 4 to *R/M 5 to *Base endif
  4 to *OfSize
;

: (MemReg-Check-AdSize-4)  ( -- )
  *AdSize 4 = ifnot exit endif
  *Base 0< if
    (MemReg-AdSize-4-No-Base)
  else
    *OfSize 4 = if 2 else *OfSize endif to *Mod
    *Base 5 = *Mod 0= and if
      0 to *Offset
      1 to *Mod
      1 to *OfSize
    endif
    *Index 0< if
      *Base 4 =
      if
        0 to *Scale
        4 to *Index
        4 to *R/M
      endif
      *Base to *R/M
    else
      4 to *R/M
    endif
  endif
;

: (MemReg-Check-AdSize)  ( -- )
  (MemReg-Check-AdSize-4)
  (MemReg-Check-AdSize-2)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ?MemReg  ( -- flag )
  (MemReg-ParseReg)
  dup ifnot exit endif
  (MemReg-Fix-OfSize)
  (MemReg-Check-AdSize)
;


: ?XMemReg  ( -- flag )
  true to *PtrKingSize?
  ?MemReg
  false to *PtrKingSize?
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; for JConds; sorry for this
: (x-resetjump)  ( -- )
  -1 to *JCondPC
;

: (x-mkjump)  ( code -- )
  1 xor  ;; invert condition
  $70 or asm-c,  ;; compile short conditional jump
  0 asm-c,       ;; disp is unknown yet
  asm-PC to *JCondPC
;

: (x-fixjump)  ( -- )
  *JCondPC -1 <> if
    asm-PC *JCondPC - CheckRelJump
    *JCondPC 1- asm-c!
    (x-resetjump)
  endif
;
