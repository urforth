;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; low-level input buffer tokeniser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

nested-vocabulary lexer
also lexer definitions

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NextToken sets the following three values
0 constant tktype-eol
1 constant tktype-delim
2 constant tktype-number
3 constant tktype-id
4 constant tktype-str

0 value tktype
0 value tkvalue  ;; c4str/char/number
false value last-token-was-eol

0 value tk-start->in


hidden:: (dump-token)  ( value type -- )
  case
    lexer:tktype-eol of drop ." <EOL>" endof
    lexer:tktype-delim of ." <DELIM:" dup 32 < if 0 .r else emit endif ." >" endof
    lexer:tktype-number of ." <NUM:" 0 .r ." >" endof
    lexer:tktype-id of ." <ID:" count dup 0 .r ." :" type ." >" endof
    lexer:tktype-str of ." <STR:" count dup 0 .r ." :" type ." >" endof
    otherwise ." <UNK:" 0 .r ." :" 0 .r ." >"
  endcase
;


hidden:: (dump-current-token)  ( -- )
  tkvalue tktype (dump-token)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ base address for token collecting area
\ maximum token size is 254 chars and cell for counter (i.e. 258 chars)
1024 buffer: asm-pad-base-addr
\ the tokenizer will set this to 512 for `PeekToken`
\ do not mess with this manually
0 value asm-extra-pad-offset

: (Token-Temp-Str-Addr)  ( -- addr )
  asm-pad-base-addr 512 +
;

: (Token-Copy-Temp-Str)  ( addr count -- addr count )
  (Token-Temp-Str-Addr) dup >r swap dup >r move r> r> swap
;

: (Token-Copy-Temp-Char)  ( ch -- addr count )
  (Token-Temp-Str-Addr) dup >r c! r> 1
;

: (ch>Token-Temp-Str)  ( ch -- )
  (Token-Temp-Str-Addr) count + c!
  (Token-Temp-Str-Addr) 1+!
;

: (Token-Quote-Temp-Str)  ( addr count -- addr count )
  (Token-Temp-Str-Addr) 0!
  34 (ch>Token-Temp-Str)
  dup 0> if
    over + swap do
      i c@ 32 < if
        [char] \ (ch>Token-Temp-Str)
        [char] x (ch>Token-Temp-Str)
        base @ >r hex
        i c@ 0 <# # # #> over + swap do i c@ (ch>Token-Temp-Str) loop
        r> base !
      else
        i c@ case
          [char] " of [char] \ (ch>Token-Temp-Str) [char] ` c@ (ch>Token-Temp-Str) endof  ;; "
          [char] \ of [char] \ (ch>Token-Temp-Str) i c@ (ch>Token-Temp-Str) endof
          i c@ (ch>Token-Temp-Str)
        endcase
      endif
    loop
  else
    2drop
  endif
  34 (ch>Token-Temp-Str)
  (ch>Token-Temp-Str) count
;


: Token->Temp-Str  ( -- addr count )
  tktype case
    lexer:tktype-eol of NullString endof
    lexer:tktype-delim of tkvalue (Token-Copy-Temp-Char) endof
    lexer:tktype-number of base @ >r hex tkvalue 0 <# 8 0 do # loop [char] $ hold #> r> base ! (Token-Copy-Temp-Str) endof
    lexer:tktype-id of tkvalue count (Token-Copy-Temp-Str) endof
    lexer:tktype-str of tkvalue count (Token-Quote-Temp-Str) endof
    otherwise ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ASM-PAD-ADDR ( -- addr )
  asm-pad-base-addr asm-extra-pad-offset +
;

: ASM-PAD-INIT ( -- )
  asm-pad-addr 0!
;

: ASM-PAD-COUNT ( -- )
  asm-pad-addr count
;

: Asm-Pad-Check-Length  ( -- )
  asm-pad-count 254 > ERRID_ASM_TOKEN_TOO_LONG ?asm-error drop
;

: CH->ASM-PAD ( ch -- )
  asm-pad-count + c!
  1 asm-pad-addr +!
  asm-pad-check-length
;

: ASM-PAD-FINISH ( -- )
  asm-pad-count + 0!
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; quote char is at TIB
;; parse it to asm-pad-addr as cell-counted string
: PARSE-QSTR  ( -- )
  tib-getch >r  ;; ( | qch )
  begin
    tib-peekch
  while
    tib-getch dup r@ = if drop break endif
    dup ch->asm-pad
    [char] \ = if
      tib-getch ?dup if ch->asm-pad endif
    endif
  repeat
  rdrop
  asm-pad-count str-unescape swap 4- !
;


: DIGIT?  ( ch -- flag )
  10 digit if drop 1 else 0 endif
;

: HEX-DIGIT?  ( ch -- flag )
  16 digit if drop 1 else 0 endif
;

: ID-CHAR?  ( ch -- flag )
  upcase-char
  dup [char] A >=
  over [char] Z <= and
  over [char] @ = or
  over [char] . = or
  swap [char] _ = or
;

: ID-CHAR-OR-DIGIT?  ( ch -- flag )
  dup id-char? swap digit? or
;

: SkipBlanks ( -- )
  begin
    tib-peekch ?dup
  while
    dup 10 = over 13 = or if drop exit endif
    32 <=
  while
    tib-getch drop
  repeat
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; save/restore parser state

hidden:: +ofs: ( n -- n+4 )  create dup , cell+ does> ( addr -- addr+ofs ) @ + ;

0
  +ofs: sbuf.>in  (hidden)
  +ofs: sbuf.line#  (hidden)
  +ofs: sbuf.last-was-eol  (hidden)
  +ofs: sbuf.start->in (hidden)
  +ofs: sbuf.tktype  (hidden)
  +ofs: sbuf.tkvalue  (hidden)
  +ofs: sbuf.tkstring  (hidden)
  256 +  ;; for string
constant sbuf.size  (hidden)


sbuf.size buffer: (peek-svbuf)  (hidden)  \ temp buffer to save current state for PeekToken
sbuf.size buffer: (saved-svbuf) (hidden)  \ this is where "SaveCurrentPos" will save, and what RestoreSavedPos will restore

(* this doesn't work
hidden:: (dump-svbuf)  ( addr -- )
  endcr ." SAVED BUFFER (" dup .hex8 ." ):\n"
  dup sbuf.>in @
    ."   >in: " . cr
  dup sbuf.line# @
    ."   tib-line#: " . cr
  dup sbuf.last-was-eol @
    ."   last-token-was-eol: " . cr
  >r
    ."   token: "
  r@ sbuf.tkvalue @ r@ sbuf.tktype @ (dump-token) cr
  rdrop
  ." -------------\n"
;
*)

hidden:: (Save-Parser-State-To)  ( addr -- )
  >in @ over sbuf.>in !
  tib-line# @ over sbuf.line# !
  last-token-was-eol over sbuf.last-was-eol !
  tk-start->in over sbuf.start->in !
  tktype over sbuf.tktype !
  tkvalue over sbuf.tkvalue !
  ;; save string
  >r asm-pad-base-addr r> sbuf.tkstring 255 cell+ cmove
;

hidden:: (Restore-Parser-State-From)  ( addr -- )
  dup sbuf.>in @ >in !
  dup sbuf.line# @ tib-line# !
  dup sbuf.last-was-eol @ to last-token-was-eol
  dup sbuf.start->in @ to tk-start->in
  dup sbuf.tktype @ to tktype
  dup sbuf.tkvalue @ to tkvalue
  ;; restore string
  sbuf.tkstring asm-pad-base-addr 255 cell+ cmove
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SaveCurrentPos  ( -- )
  (saved-svbuf) (Save-Parser-State-To)
;

: RestoreSavedPos  ( -- )
  (saved-svbuf) (Restore-Parser-State-From)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: PrepareLineParser  ( -- )
  false to last-token-was-eol
  tktype-eol to tktype
  0 to tkvalue
  ;; just in case
  (peek-svbuf) (Save-Parser-State-To)
  (saved-svbuf) (Save-Parser-State-To)
;

(*
: SetEOLState  ( -- )
  true to last-token-was-eol
  tktype-eol to tktype
  0 to tkvalue
;
*)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: (Token-Peek-Char)  ( -- ch )
  last-token-was-eol if 0 endif
  ;; we aren't using "last read tib char", so no need to save/restore it
  >in @ >r
  tib-line# @ >r
  SkipBlanks
  tib-peekch
  r> tib-line# !
  r> >in !
;


;; parse input string, put it into asm-pad-addr as cell-counted
hidden:: (NextToken)  ( -- c4str type )
  asm-pad-init
  SkipBlanks

  >in @ to tk-start->in

  ;; quoted string?
  tib-peekch dup [char] " = swap [char] ' = or if  \ "
    parse-qstr
    asm-pad-finish
    asm-pad-addr tktype-str exit
  endif

  ;; digit?
  tib-peekch digit? if
    begin
      tib-peekch id-char-or-digit?
    while
      tib-getch upcase-char ch->asm-pad
    repeat
    asm-pad-finish
    asm-pad-addr tktype-number exit
  endif

  ;; identifier?
  tib-peekch id-char? if
    begin
      tib-peekch id-char-or-digit?
    while
      tib-getch upcase-char ch->asm-pad
    repeat
    asm-pad-finish
    asm-pad-addr tktype-id exit
  endif

  ;; delimiter
  tib-peekch if
    tib-getch dup ch->asm-pad
    ;; $ID?
    dup [char] $ = tib-peekch id-char? and if
      drop
      begin
        tib-peekch id-char-or-digit?
      while
        tib-getch upcase-char ch->asm-pad
      repeat
      ;; try it as a number, "NextToken" will take care of it
      asm-pad-finish
      asm-pad-addr tktype-number exit
    endif
    ;; check for numeric sigils
    case
      [char] # of true endof
      [char] $ of true endof
      [char] % of true endof
      otherwise drop false
    endcase
    if
      tib-peekch hex-digit? if
        begin
          tib-peekch id-char-or-digit?
        while
          tib-getch upcase-char ch->asm-pad
        repeat
        asm-pad-finish
        asm-pad-addr tktype-number exit
      endif
    endif
  endif
  asm-pad-finish
  asm-pad-addr tktype-delim
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: NextToken  ( -- )
  last-token-was-eol if
    asm-pad-finish
    0 to tkvalue
    tktype-eol to tktype
    exit
  endif
  (NextToken)  ;; ( c4str type )
  case
    tktype-delim of  \ always one char
      ;; empty string?
      count
      ifnot
        drop
        true to last-token-was-eol
        0 to tkvalue
        tktype-eol to tktype
        exit
      endif
      ;; end of line?
      c@ dup
      case
        [char] ; of [compile] \ true endof
        10 of true endof
        [char] \ of [compile] \ true endof
        0 of true endof
        otherwise
          drop 0
      endcase
      \ eol?
      if
        ;; just in case, reinit string
        drop
        true to last-token-was-eol
        0 to tkvalue
        tktype-eol to tktype
      else
        ;; delimiter
        to tkvalue
        tktype-delim to tktype
      endif
    endof
    tktype-number of
      ;; use standard number parser, because now we can ;-)
      count
      ;; ( addr count )
      2dup number ifnot
        over c@ [char] $ = ERRID_ASM_INVALID_NUMBER not-?asm-error
        drop cell-
        to tkvalue
        tktype-id to tktype
      else
        nrot 2drop
        ;; ( n )
        to tkvalue
        tktype-number to tktype
      endif
    endof
    tktype-id of
      to tkvalue
      tktype-id to tktype
    endof
    tktype-str of to tkvalue tktype-str to tktype endof
    ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: PeekToken  ( -- value type )
  ;; check for eol
  last-token-was-eol if
    asm-pad-finish
    asm-pad-addr 0
    exit
  endif
  ;; offset pad
  512 to asm-extra-pad-offset
  asm-pad-init
  ;; save current position, and token data
  (peek-svbuf) (Save-Parser-State-To)
  [:
    ;; get next token
    NextToken
    ;; return values
    tkvalue tktype
  ;] catch
  (peek-svbuf) (Restore-Parser-State-From)
  ;; restore pad offset
  0 to asm-extra-pad-offset
  ?dup if throw endif
;


previous definitions
<hidden-words>
