;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; db/dw/dd
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
hidden:: (defx)  ( size -- )
  >r  ;; save size on the rstack
  begin
    tk-eol?
  not-while
    ;; string?
    tk-str? if
      ;; allow strings in any more; they will still be one-byte strings, though
      \ r@ 4 = ERRID_ASM_TYPE_MISMATCH not-?asm-error
      lexer:tkvalue count over + swap ?do
        i c@ 1 asm-n-allot asm-c!
      loop
      lexer:NextToken
    else
      Reset-Instruction
      r@ to *OpSize
      Imm
      ;; label?
      *OpReloc if
        r@ 4 = ERRID_ASM_TYPE_MISMATCH not-?asm-error
        ;; signal fixup
        *ImmName bcount ( *ImSize ) 4 *ImmForthType asm-Label-Fixup
      endif
      ;; put value
      *Imm
      r@ case
        1 of $ff and asm-c, endof
        2 of $ffff and asm-w, endof
        4 of asm-, endof
        otherwise ERRID_ASM_INTERNAL_ERROR asm-error
      endcase
    endif
    ExpectCommaOrEOL
  repeat
  rdrop  ;; drop size
;

hidden:: (resx)  ( size -- )
  ;; convert size to reserve word cfa
  case
    1 of ['] asm-c, endof
    2 of ['] asm-w, endof
    4 of ['] asm-, endof
    otherwise ERRID_ASM_INTERNAL_ERROR asm-error
  endcase
  Get-Imm-Defined
  dup 0< ERRID_ASM_INVALID_OPERAND ?asm-error
  0 ?do 0 over execute loop
  drop  ;; drop cfa
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: DB  ( -- )  1 (defx)  ;
: DW  ( -- )  2 (defx)  ;
: DD  ( -- )  4 (defx)  ;

: DEFB  ( -- )  1 (defx)  ;
: DEFW  ( -- )  2 (defx)  ;
: DEFD  ( -- )  4 (defx)  ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; "reserve" fills with zeroes
: RB  ( -- )  1 (resx)  ;
: RW  ( -- )  2 (resx)  ;
: RD  ( -- )  4 (resx)  ;
