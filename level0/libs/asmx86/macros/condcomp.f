;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple conditional compilation
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: $ELSE  ( -- )
  1  ;; level
  begin
    EnsureNotEOL
      \ dup . asmx86:lexer:(dump-current-token) cr
    asmx86:tk-id? if
      lexer:tkvalue count s" $IF" s=
      lexer:tkvalue count s" $IFNOT" s= or
      \ not yet
      \ lexer:tkvalue count s" $IFDEF" s= or
      \ lexer:tkvalue count s" $IFNDEF" s= or
      if
        1+ ;; next level
      else
        lexer:tkvalue count s" $ELSE" s=
        if
          1-
          dup if 1+ endif
        else
          lexer:tkvalue count s" $ENDIF" s=
          if 1- endif
        endif
      endif
    endif
    lexer:NextToken
    dup not
  until
  drop
;

: $ENDIF  ( -- )  ;


hidden:: (if-eval-cond-check-char)
( cfa0 true cfa1 ch -- cfa true )
( cfa0 false cfa1 ch -- cfa1 true // cfa0 false )
  rot if
    2drop true
  else
    lexer:tkvalue = if
      nip true
    else
      drop false
    endif
  endif
;

hidden:: (if-eval-cond)  ( -- bool )
  Get-Imm-Defined-Allow-Delims
  ;; comparisons
  tk-delim? if
    \ FIXME: make this nicer
    0 false
    ['] < [char] < (if-eval-cond-check-char)
    ['] > [char] > (if-eval-cond-check-char)
    ['] = [char] = (if-eval-cond-check-char)
    ['] <> [char] # (if-eval-cond-check-char)
    if
      ;; ( value compcfa )
      lexer:NextToken
      Get-Imm-Defined swap execute
    else
      drop  ;; unused compop
    endif
  endif
;

: $IF  ( -- )
  (if-eval-cond)
  ifnot $ELSE endif
;

: $IFNOT  ( -- )
  (if-eval-cond)
  if $ELSE endif
;


: $ERROR  ( -- )
  tk-str? if
    endcr ." ERROR: " lexer:tkvalue count type cr
  endif
  ERRID_ASM_USER_ERROR asm-error
;
