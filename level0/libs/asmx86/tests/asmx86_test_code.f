;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tester
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ " asmx86.f" tload

.stack

code: my666  ( -- 666 )
  push  TOS
  ld    TOS,29ah  ; comment
  next
endcode

.( ---------------------------) cr
' my666 dup cfa->nfa nfa->sfa @ swap - . cr
' my666 disasm-word
my666 . cr


.stack

code: my667  ( n -- n+1//666)
  cp    TOS,666
  jr    z,.done
@global:
  inc   TOS
.done:
  next
  nop
  nop
  nop
  align 16,$90
  ld    eax,lbl
  jp    lbl
  ld    eax,lbl+2
  jp    lbl-2
  nop
lbl: jp    lbl
  ld    ebx,lbl
  jr  @f
  nop
@@:
  nop
  jr @b
  nop
  ;jr @f
  ld    eax,lbl+1
  ld    eax,lbl
  jp    lbl-2
  syscall
  ld    eax,[42]
  ld    eax,[pfa "state"]
endcode

.stack
.( ---------------------------) cr
.( STATE pfa: 0x) ' state cfa->pfa .hex8 cr
.( STATE pfa: 0x) state .hex8 cr
' my667 dup cfa->nfa nfa->sfa @ swap - . cr
' my667 disasm-word
42 my667 . cr
my666 my667 . cr

asmx86:asm-Dump-Labels

.stack
bye
