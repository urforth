;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; error codes and error reporting
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; label manager should change this

1 constant ERRID_ASM_COMMA_EXPECTED
2 constant ERRID_ASM_COLON_EXPECTED
3 constant ERRID_ASM_MEMREF_EXPECTED
4 constant ERRID_ASM_OPERAND_EXPECTED
5 constant ERRID_ASM_LABEL_EXPECTED
6 constant ERRID_ASM_SYNTAX_ERROR
7 constant ERRID_ASM_TOKEN_TOO_LONG
8 constant ERRID_ASM_ENDCODE_EXPECTED
9 constant ERRID_ASM_REGISTER_EXPECTED

10 constant ERRID_ASM_EXPECT_32BIT_OFFS
11 constant ERRID_ASM_EXPECT_32BIT_OPERAND
12 constant ERRID_ASM_EXPECT_SEGREG
13 constant ERRID_ASM_EXPECT_TYPE_OVERRIDE
14 constant ERRID_ASM_TYPE_MISMATCH
15 constant ERRID_ASM_STRING_EXPECTED
16 constant ERRID_ASM_UNEXPECTED_EOF

20 constant ERRID_ASM_NUMBER_TOO_BIG
21 constant ERRID_ASM_INVALID_SCALE
22 constant ERRID_ASM_INVALID_OFFSET
23 constant ERRID_ASM_JUMP_OUT_OF_RANGE
24 constant ERRID_ASM_INVALID_OPERAND
25 constant ERRID_ASM_INVALID_NUMBER
26 constant ERRID_ASM_INVALID_ALIGN
27 constant ERRID_ASM_INVALID_CONDITION

40 constant ERRID_ASM_INVALID_FORWARD_REF
41 constant ERRID_ASM_INVALID_BACKWARD_REF
42 constant ERRID_ASM_UNRESOLVED_LABEL
43 constant ERRID_ASM_DUPLICATE_LABEL
44 constant ERRID_ASM_DUPLICATE_CONSTANT
45 constant ERRID_ASM_INVALID_LABEL_NAME
46 constant ERRID_ASM_EXTRA_SCALE

50 constant ERRID_ASM_NO_LABEL
51 constant ERRID_ASM_TOO_MANY_LABEL_REFS
52 constant ERRID_MACRO_BUFFER_OVERFLOW


66 constant ERRID_ASM_INTERNAL_META_ERROR
67 constant ERRID_ASM_USER_ERROR
68 constant ERRID_ASM_TS_DISABLED
69 constant ERRID_ASM_INTERNAL_ERROR


create asm-error-table
  ERRID_ASM_COMMA_EXPECTED       error-table-msg," comma expected"
  ERRID_ASM_COLON_EXPECTED       error-table-msg," colon expected"
  ERRID_ASM_MEMREF_EXPECTED      error-table-msg," memory reference expected"
  ERRID_ASM_OPERAND_EXPECTED     error-table-msg," operand expected"
  ERRID_ASM_LABEL_EXPECTED       error-table-msg," label expected"
  ERRID_ASM_SYNTAX_ERROR         error-table-msg," syntax error"
  ERRID_ASM_TOKEN_TOO_LONG       error-table-msg," token too long"
  ERRID_ASM_ENDCODE_EXPECTED     error-table-msg," ENDCODE expected"
  ERRID_ASM_REGISTER_EXPECTED    error-table-msg," register expected"

  ERRID_ASM_EXPECT_32BIT_OFFS    error-table-msg," 32-bit offset expected"
  ERRID_ASM_EXPECT_32BIT_OPERAND error-table-msg," 32-bit operand expected"
  ERRID_ASM_EXPECT_SEGREG        error-table-msg," segment register expected"
  ERRID_ASM_EXPECT_TYPE_OVERRIDE error-table-msg," expect type override"
  ERRID_ASM_TYPE_MISMATCH        error-table-msg," type mismatch"
  ERRID_ASM_STRING_EXPECTED      error-table-msg," string expected"
  ERRID_ASM_UNEXPECTED_EOF       error-table-msg," unexpected end of file"

  ERRID_ASM_NUMBER_TOO_BIG       error-table-msg," number too big"
  ERRID_ASM_INVALID_SCALE        error-table-msg," invalid scale"
  ERRID_ASM_INVALID_OFFSET       error-table-msg," invalid offset"
  ERRID_ASM_JUMP_OUT_OF_RANGE    error-table-msg," jump out of range"
  ERRID_ASM_INVALID_OPERAND      error-table-msg," invalid operand"
  ERRID_ASM_INVALID_NUMBER       error-table-msg," invalid number"
  ERRID_ASM_INVALID_ALIGN        error-table-msg," invalid align value"
  ERRID_ASM_INVALID_CONDITION    error-table-msg," invalid condition"

  ERRID_ASM_TYPE_MISMATCH        error-table-msg," type mismatch"

  ERRID_ASM_INVALID_FORWARD_REF  error-table-msg," invalid forward reference"
  ERRID_ASM_INVALID_BACKWARD_REF error-table-msg," invalid backward reference"
  ERRID_ASM_UNRESOLVED_LABEL     error-table-msg," unresolved label"
  ERRID_ASM_DUPLICATE_LABEL      error-table-msg," duplicate label"
  ERRID_ASM_DUPLICATE_CONSTANT   error-table-msg," duplicate constant"
  ERRID_ASM_INVALID_LABEL_NAME   error-table-msg," invalid label name"
  ERRID_ASM_EXTRA_SCALE          error-table-msg," extra scale"

  ERRID_ASM_NO_LABEL             error-table-msg," label not found"
  ERRID_ASM_TOO_MANY_LABEL_REFS  error-table-msg," too many label references"
  ERRID_MACRO_BUFFER_OVERFLOW    error-table-msg," macro buffer overflow (line too long)"

  ERRID_ASM_INTERNAL_META_ERROR  error-table-msg," internal metacompiler error (accessing unallocated memory)"
  ERRID_ASM_USER_ERROR           error-table-msg," error"
  ERRID_ASM_TS_DISABLED          error-table-msg," Task Segment is disabled"
  ERRID_ASM_INTERNAL_ERROR       error-table-msg," internal assembler error (something is VERY wrong!)"

  error-table-end


: asm-error ( errcode -- )
  asm-Labman-Error-Reset
  ."  ASM ERROR: "
  asm-error-table forth:(find-errmsg) if
    type
  else
    base @ >r decimal ." #" 0 .r r> base !
  endif
  forth:error-line.
  cr
  abort
;


: ?asm-error ( flag errcode -- )
\ errors if flag is !0
  swap ifnot drop else asm-error endif
;

: not-?asm-error ( flag errcode -- )
\ errors if flag is 0
  swap if drop else asm-error endif
;
