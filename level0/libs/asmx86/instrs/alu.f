;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ALU
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$14 0 $15 0 $80 2 $81 2 $83 2 $10 0 $11 0 $12 0 $13 0 ALU ADC
$04 0 $05 0 $80 0 $81 0 $83 0 $00 0 $01 0 $02 0 $03 0 ALU ADD
$24 0 $25 0 $80 4 $81 4 $83 4 $20 0 $21 0 $22 0 $23 0 ALU AND
$3C 0 $3D 0 $80 7 $81 7 $83 7 $38 0 $39 0 $3A 0 $3B 0 ALU CMP
$1C 0 $1D 0 $80 3 $81 3 $83 3 $18 0 $19 0 $1A 0 $1B 0 ALU SBB
$2C 0 $2D 0 $80 5 $81 5 $83 5 $28 0 $29 0 $2A 0 $2B 0 ALU SUB
$34 0 $35 0 $80 6 $81 6 $83 6 $30 0 $31 0 $32 0 $33 0 ALU XOR
$0C 0 $0D 0 $80 1 $81 1 $83 1 $08 0 $09 0 $0A 0 $0B 0 ALU OR
$A8 0 $A9 0 $F6 0 $F7 0 $F7 0 $84 0 $85 0 $84 0 $85 0 ALU TEST
alias CMP CP
