;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FPU
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: FNSTSW  ( -- )
  Reset-Instruction
  1 to *OpcSize
  ?Reg16 if
    ERRID_ASM_INVALID_OPERAND ?asm-error
    $DF to *OpCode
    4 to *Reg
    3 to *Mod
    0 to *R/M
  else
    (FPU-MemReg)
    *OpSize dup 0= swap 2 = or ERRID_ASM_TYPE_MISMATCH not-?asm-error
    $DD to *OpCode
    7 to *Reg
  endif
  0 to *OpSize
  Build-Instruction
;


: FSTSW  ( -- )
  $9B asm-c, FNSTSW
;


\ FPU
0 0 4 0 0 0 4 0 F2Op FADD
0 1 4 1 0 1 4 1 F2Op FMUL
0 4 4 4 0 4 4 5 F2Op FSUB
0 5 4 5 0 5 4 4 F2Op FSUBR
0 6 4 6 0 6 4 7 F2Op FDIV
0 7 4 7 0 7 4 6 F2Op FDIVR

0 2 4 2 -1 0 0 2 F1Op FCOM
0 3 4 3 -1 0 0 3 F1Op FCOMP
1 2 5 2 -1 0 5 2 F1Op FST
1 0 5 0  3 5 1 0 F1Op FLD
1 3 5 3  3 7 5 3 F1Op FSTP

6 0 2 0 -1 0 FIxx FIADD
6 1 2 1 -1 0 FIxx FIMUL
6 2 2 2 -1 0 FIxx FICOM
6 3 2 3 -1 0 FIxx FICOMP
6 4 2 4 -1 0 FIxx FISUB
6 5 2 5 -1 0 FIxx FISUBR
6 6 2 6 -1 0 FIxx FIDIV
6 7 2 7 -1 0 FIxx FIDIVR
7 2 3 2 -1 0 FIxx FIST
7 0 3 0  7 5 FIxx FILD
7 3 3 3  7 7 FIxx FISTP

1 2 0 F0Op FNOP
1 4 0 F0Op FCHS
1 4 1 F0Op FABS
1 4 4 F0Op FTST
1 4 5 F0Op FXAM
1 5 0 F0Op FLD1
1 5 1 F0Op FLDL2T
1 5 2 F0Op FLDL2E
1 5 3 F0Op FLDPI
1 5 4 F0Op FLDLG2
1 5 5 F0Op FLDLN2
1 5 6 F0Op FLDZ
1 6 0 F0Op F2XM1
1 6 1 F0Op FYL2X
1 6 2 F0Op FPTAN
1 6 3 F0Op FPATAN
1 6 4 F0Op FXTRACT
1 6 5 F0Op FPREM1
1 6 6 F0Op FDECSTP
1 6 7 F0Op FINCSTP
1 7 0 F0Op FPREM
1 7 1 F0Op FYL2XP1
1 7 2 F0Op FSQRT
1 7 3 F0Op FSINCOS
1 7 4 F0Op FRNDINT
1 7 5 F0Op FSCALE
1 7 6 F0Op FSIN
1 7 7 F0Op FCOS
2 5 1 F0Op FUCOMPP
3 4 0 F0Op FNENI
3 4 1 F0Op FNDISI
3 4 2 F0Op FNCLEX
3 4 3 F0Op FNINIT
6 3 1 F0Op FCOMPP

3 4 0 FW0Op FENI
3 4 1 FW0Op FDISI
3 4 2 FW0Op FCLEX
3 4 3 FW0Op FINIT
3 4 4 FW0Op FSETPM

$9B 0 1 4 FCtl FLDENV
  0 0 1 5 FCtl FLDCW
$9B 0 1 6 FCtl FSTENV
$9B 0 1 7 FCtl FSTCW
$9B 0 5 4 FCtl FRSTOR
$9B 0 5 6 FCtl FSAVE
  0 0 1 4 FCtl FNLDENV
  0 0 1 6 FCtl FNSTENV
  0 0 1 7 FCtl FNSTCW
  0 0 5 4 FCtl FNRSTOR
  0 0 5 6 FCtl FNSAVE
$9B 2 1 4 FCtl FLDENVW
$9B 2 1 6 FCtl FSTENVW
$9B 2 5 4 FCtl FRSTORW
$9B 2 5 6 FCtl FSAVEW
$9B 4 1 4 FCtl FLDENVD
$9B 4 1 6 FCtl FSTENVD
$9B 4 5 4 FCtl FRSTORD
$9B 4 5 6 FCtl FSAVED
  0 2 1 4 FCtl FNLDENVW
  0 2 1 6 FCtl FNSTENVW
  0 2 5 4 FCtl FNRSTORW
  0 2 5 6 FCtl FNSAVEW
  0 4 1 4 FCtl FNLDENVD
  0 4 1 6 FCtl FNSTENVD
  0 4 5 4 FCtl FNRSTORD
  0 4 5 6 FCtl FNSAVED

4 FBxx FBLD
6 FBxx FBSTP

1 1 FRegOp FXCH
5 0 FRegOp FFREE
5 4 FRegOp FUCOM
5 5 FRegOp FUCOMP
6 0 FRegOp FADDP
6 1 FRegOp FMULP
6 4 FRegOp FSUBRP
6 5 FRegOp FSUBP
6 6 FRegOp FDIVRP
6 7 FRegOp FDIVP

2 0 FP6Op FCMOVB
2 1 FP6Op FCMOVE
2 2 FP6Op FCMOVBE
2 3 FP6Op FCMOVU
3 0 FP6Op FCMOVNB
3 1 FP6Op FCMOVNE
3 2 FP6Op FCMOVNBE
3 3 FP6Op FCMOVNU
3 5 FP6Op FUCOMI
3 6 FP6Op FCOMI
7 5 FP6Op FUCOMIP
7 6 FP6Op FCOMIP

alias WAIT FWAIT


;; FFREE ST(i) and pop stack
: FFREEP  ( -- )
  Reset-Instruction
  ;; should have one operand -- FPU register
  ?STReg ERRID_ASM_INVALID_OPERAND not-?asm-error
  2 to *OpcSize
  8 lshift $C0DF + to *OpCode
  Build-Instruction
;
