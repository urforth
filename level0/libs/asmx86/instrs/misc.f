;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Native x86 GNU/Linux Forth System
;; Natural-syntax x86 assembler
;; Modelled after SMAL32 built-in assembler
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
$FE 0 $FF 0 $40 0 IncDec INC
$FE 1 $FF 1 $48 0 IncDec DEC

$F6 3 $F7 3 NegNotMulDiv NEG
$F6 2 $F7 2 NegNotMulDiv NOT
$F6 4 $F7 4 NegNotMulDiv MUL
$F6 6 $F7 6 NegNotMulDiv DIV
$F6 7 $F7 7 NegNotMulDiv IDIV

  $8D 1 LxS LEA
  $C5 1 LxS LDS
  $C4 1 LxS LES
$B20F 2 LxS LSS
$B40F 2 LxS LFS
$B50F 2 LxS LGS

$BE0F $BF0F MOVxx MOVSX
$B60F $B70F MOVxx MOVZX

0 $A30F 4 $BA0F Bit BT
0 $BB0F 7 $BA0F Bit BTC
0 $B30F 6 $BA0F Bit BTR
0 $AB0F 5 $BA0F Bit BTS

$BC0F BSx BSF
$BD0F BSx BSR

\ with numeric argument
$C3 $C2 RETx RETN
$CB $CA RETx RETF

alias RETN RET

$D4 AAx AAM
$D5 AAx AAD
