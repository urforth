;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; save current image to ELF executable file
;; we're using the fact that our ELF header is loaded into memory, and is writeable
;; metacompiler will prolly need to rebuild it from scratch, but for now... meh
urword_forth "(SAVE)",par_save
urword_hidden
;; ( fd -- successflag )
  ;; just in case, check fd
  UF dup 0less
  ur_if
    UF drop 0 exit
  ur_endif
  ;; move fd to return stack
  UF rpush
  ;; fix code segment size
  ;; write code from code start to real HERE
  UF real_here par_code_base_addr - dup par_code_base_addr 0x84 + !
  UF drop  ;; do not fix virtual size
  ;; par_code_base_addr 0x88 + !
  ;; everything in our header is ok now, including entry point (it isn't changed)
  ;; write everything up until import table
  UF par_code_base_addr elfhead_size rpeek par_fwrite
  UF elfhead_size equal
  ur_ifnot
    UF rdrop 0 exit
  ur_endif
  ;; write zero bytes for imports: this is where import addresses will be put by ld.so
  ;; use HERE for this
  UF real_here par_code_imports_size erase
  UF real_here par_code_imports_size rpeek par_fwrite
  UF par_code_imports_size equal
  ur_ifnot
    UF rdrop 0 exit
  ur_endif
  ;; setup some variables first, so the new image will process CLI args
  ;; don't bother moving old values to "safe place", data stack is good enough
  UF arg_next
  UF process_cli @
  UF 1
  urto arg_next
  UF 1 process_cli !
  ;; write code from imports start to real here
  UF par_code_imports_end_addr real_here par_code_imports_end_addr - rpeek par_fwrite
  ;; restore variables, just in case (TOS is write result)
  UF rpush  ;; move write result to the safe place
  UF process_cli !
  urto arg_next
  UF rpop   ;; restore write result
  ur_ifnot
    UF rdrop 0 exit
  ur_endif
  ;; you may not believe me, but we're done!
  UF rdrop    ;; got rid of fd
  UF 1        ;; exit with success
urword_end


urword_forth "SAVE",save
;; ( addr count -- successflag )
  ;; create output file
  UF o_wronly o_creat or o_trunc or  ;; flags
  UF s_irwxu s_irgrp or s_ixgrp or s_iroth or s_ixoth or  ;; mode
  UF par_fopen
  ;; check success
  UF dup 0less
  ur_if
    UF drop 0 exit
  ur_endif
  UF dup par_save
  ;; ( fd successflag )
  UF swap par_fclose 0equal
  UF land
urword_end
