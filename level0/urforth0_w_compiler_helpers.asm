;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_var "(CSP)",csp,0
urword_hidden

urword_forth "!CSP",savecsp
  UF spget csp !
urword_end

urword_forth "?CSP",qcsp
;; ( -- )
  UF spget csp @ - errid_unfinished_definition qerror
urword_end

urword_forth "?COMP",qcomp
  UF state @ 0 equal errid_compilation_only qerror
urword_end

urword_forth "?EXEC",qexec
  UF state @ errid_execution_only qerror
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "LITERAL",literal
urword_immediate
  UF state @
  ur_if
    urcompile lit
    UF ,
  ur_endif
urword_end

urword_forth "COMPILE,",compile_comma
urword_arg_cfa
;; ( cfa -- )
  UF ,
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; write 4-byte displacement for CALL/JMP jmpdest instruction to addr
; addr should point after the instruction, at the first displacement byte
urword_forth "(DISP32!)",par_disp32set
urword_hidden
;; ( jmpdest addr -- )
  UF dup cellinc rot swap - swap !
urword_end

urword_forth "(DISP32@)",par_disp32get
urword_hidden
;; ( addr -- jumpdest )
  UF dup @ swap cellinc +
urword_end

; write 5-byte CALL calldest machine instruction to addr
urword_forth "(CALL!)",par_callset
urword_hidden
;; ( calldest addr -- )
  UF 0xe8 over cpoke 1inc  ; write CALL, advance address
  UF par_disp32set
urword_end

; write 5-byte JMP jmpdest machine instruction to addr
urword_forth "(JMP!)",par_jmpset
urword_hidden
;; ( jmpdest addr -- )
  UF 0xe9 over cpoke 1inc  ; write JMP, advance address
  UF par_disp32set
urword_end

; compile 5-byte CALL calldest machine instruction to HERE
urword_forth "(CALL,)",par_callcomma
urword_hidden
;; ( calldest -- )
  UF 5 n_allot par_callset
urword_end

; compile 5-byte JMP jmpdest machine instruction to HERE
urword_forth "(JMP,)",par_jmpcomma
urword_hidden
;; ( jmpdest -- )
  UF 5 n_allot par_jmpset
urword_end
