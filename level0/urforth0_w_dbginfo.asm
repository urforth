;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; currently, debug info is very simple, it contains only PC->LINE mapping
;;
;; header:
;;   dd itemcount  ; this may be used as "extended" later, so bit 31 is resv
;;
;; items:
;;   dd pc
;;   dd line
;;
;; itemcount bit 31 should always be 0
;;
;; the compiler doesn't store each PC, it only stores line changes
;; that is, the range for the line lasts until the next item
;; items should be sorted by PC (but the code should not fail on
;; unsorted data)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_value "(DBGINFO-ENABLED?)",dbginfo_enabledq,1
urword_hidden

urword_value "(DBG-BUF-PTR)", dbuf_addr,0
urword_hidden
urword_value "(DBG-BUF-SIZE)",dbuf_size,1024*64
urword_hidden
urword_value "(DBGINFO-ACTIVE?)",dbginfo_activeq,0
urword_hidden


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "DEBUG-INFO-ON",debug_info_on
  UF 1
  urto dbginfo_enabledq
urword_end

urword_forth "DEBUG-INFO-OFF",debug_info_off
  UF 0
  urto dbginfo_enabledq
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(DBGINFO-RESET)",debug_info_reset
urword_hidden
;; ( -- )
  UF dbuf_size
  ur_if
    UF dbuf_addr 0poke
  ur_endif
  UF 0
  urto dbginfo_activeq
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(DBGINFO-GET-ADDR-SIZE)",debug_info_addr_size
urword_hidden
;; ( -- addr bytes 1 )
;; ( -- 0 )
  UF dbginfo_activeq dbginfo_enabledq land
  ur_ifnot
    UF 0 exit
  ur_endif
  UF dbuf_size
  ur_ifnot
    UF 0 exit
  ur_endif
  UF dbuf_addr @ qdup
  ur_ifnot
    UF 0 exit
  ur_endif
  UF dup 0xffffffff equal
  ur_if
    UF drop 0 exit
  ur_endif
  ; convert to bytes
  UF 2 cells umul cellinc
  ; address
  UF dbuf_addr
  UF swap 1
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(DBGINFO-DUMP-AT)",debug_info_dump_at
urword_hidden
;; ( addr -- )
  UF qdup
  ur_ifnot
    UF exit
  ur_endif
  UF dup @
  ur_ifnot
    UF drop exit
  ur_endif
  ; print counter
  UF rpush
  urprint "debug info containts "
  UF rpeek @ dot
  urprintnl "items"
  ; end
  UF rpeek @ 2 cells umul cellinc rpeek +
  ; start
  UF rpeek cellinc
  ur_do
    UF i dothex8
    urprint ": pc=0x"
    UF i @ dothex8
    urprint " at "
    UF i cellinc @ dot
    UF cr
  UF 2 cells
  ur_ploop
  UF rdrop
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(DBGINFO-DUMP)",debug_info_dump
urword_hidden
;; ( -- )
  UF dbuf_size
  ur_ifnot
    urprintnl "debug info disabled"
    UF 2drop exit
  ur_endif
  UF dbuf_addr @ qdup
  ur_ifnot
    urprintnl "no debug info"
    UF exit
  ur_endif
  UF 0xffffffff equal
  ur_if
    urprintnl "debug info overflowed"
    UF exit
  ur_endif
  UF dbuf_addr debug_info_dump_at
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(DBGINFO-ADD-PC)",debug_info_add_pc
urword_hidden
;; ( pc line -- )
  ; never put zero line
  UF qdup
  ur_ifnot
    UF drop exit
  ur_endif
  UF dbuf_size
  ur_ifnot
    UF 2drop exit
  ur_endif
  ; just inited?
  UF dbuf_addr @ qdup
  ur_ifnot
    ; yes, store the item
    ; assume that we always have room for at least one
    UF dbuf_addr 1poke
    UF swap dbuf_addr cellinc !
    UF dbuf_addr 2 addcells !
    UF exit
  ur_endif
  ; special? -1 items means "out of room"
  UF dup 0xffffffff equal
  ur_if
    UF drop 2drop exit
  ur_endif
  ; calculate address of the last item
  UF 1dec 2 cells umul cellinc dbuf_addr +
  ; check if the line is the same
  ;TODO: check for sorted PC here?
  UF 2dup cellinc @ equal
  ur_if
    ; no need to store this item
    UF drop 2drop exit
  ur_endif
  ; advance address
  UF 4 addcells
  ; check if we have enough room
  UF dup dbuf_size dbuf_addr + ugreat
  ur_if
    ; out of buffer, abort debug info generation
    UF drop 2drop 0xffffffff dbuf_addr ! exit
  ur_endif
  UF 2 subcells
  ; ok, we have enough room, store new item
    if 0
    UF rpush base @ rpush
    UF 2dup
    urprint "adding pc 0x"
    UF swap dothex8
    urprint " at line "
    UF dot cr
    UF rpop base ! rpop
    end if
  UF rot over !  cellinc !
  ; and increment the counter
  UF 1 dbuf_addr addpoke
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_forth "(DBGINFO-ADD-HERE)",dbginfo_add_here
urword_hidden
  ; put debug info
  UF dbginfo_activeq dbginfo_enabledq land
  ur_if
    UF here tiblineno @ debug_info_add_pc
  ur_endif
urword_end
