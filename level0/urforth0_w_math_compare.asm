;; Native x86 GNU/Linux Forth System, Direct Threaded Code
;;
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3 of the License ONLY.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "=",equal
;; ( n0 n1 -- n0=n1? )
  pop   eax
  ; eax=n0
  ; TOS=n1
  cp    eax,TOS
  setz  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "<>",nequ
;; ( n0 n1 -- n0<>n1? )
  pop   eax
  ; eax=n0
  ; TOS=n1
  cp    eax,TOS
  setnz cl
  movzx TOS,cl
  urnext
urword_end

urword_code "<",less
;; ( n0 n1 -- n<n1? )
  pop   eax
  ; eax=n0
  ; TOS=n1
  cmp   eax,TOS
  setl  cl
  movzx TOS,cl
  urnext
urword_end

urword_code ">",great
;; ( n0 n1 -- n0>n1? )
  pop   eax
  ; eax=n0
  ; TOS=n1
  cmp   eax,TOS
  setg  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "<=",lessequ
;; ( n0 n1 -- n<-n1? )
  pop   eax
  ; eax=n0
  ; TOS=n1
  cmp   eax,TOS
  setle cl
  movzx TOS,cl
  urnext
urword_end

urword_code ">=",greatequ
;; ( n0 n1 -- n0>=n1? )
  pop   eax
  ; eax=n0
  ; TOS=n1
  cmp   eax,TOS
  setge cl
  movzx TOS,cl
  urnext
urword_end

urword_code "U<",uless
;; ( u0 u1 -- u0<u1? )
  pop   eax
  ; eax=u0
  ; TOS=u1
  cmp   eax,TOS
  ;  C: u0<u1
  ; NC: u0>=u1
  mov   TOS,0
  adc   TOS,0
  urnext
urword_end

urword_code "U>",ugreat
;; ( u0 u1 -- u0>u1? )
  pop   eax
  ; eax=u0
  ; TOS=u1
  cmp   TOS,eax
  ;  C: u1<u0 (or u0>u1)
  ; NC: u1>=u0
  mov   TOS,0
  adc   TOS,0
  urnext
urword_end

urword_code "U<=",ulessequ
;; ( u0 u1 -- u0<=u1? )
  pop   eax
  ; eax=u0
  ; TOS=u1
  cmp   TOS,eax
  ;  C: u1<u0
  ; NC: u1>=u0 (or u0<=u1)
  mov   TOS,1
  sbb   TOS,0
  urnext
urword_end

urword_code "U>=",ugreatequ
;; ( u0 u1 -- u0>u1? )
  pop   eax
  ; eax=u0
  ; TOS=u1
  cmp   eax,TOS
  ;  C: u0<u1
  ; NC: u0>=u1
  mov   TOS,1
  sbb   TOS,0
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "0=",0equal
;; ( n0 -- n=0? )
  cp    TOS,1
  ;  C: TOS==0
  ; NC: TOS!=0
  ld    TOS,0
  adc   TOS,0
  urnext
urword_end

urword_code "0<>",0nequ
;; ( n0 -- n<>0? )
  cp    TOS,1
  ;  C: TOS==0
  ; NC: TOS!=0
  ld    TOS,1
  sbb   TOS,0
  urnext
urword_end

urword_code "0<",0less
;; ( n0 -- n<0? )
  cp    TOS,0
  setl  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "0>",0great
;; ( n0 -- n>0? )
  cp    TOS,0
  setg  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "0<=",0lessequ
;; ( n0 -- n<=0? )
  cp    TOS,0
  setle cl
  movzx TOS,cl
  urnext
urword_end

urword_code "0>=",0greatequ
;; ( n0 -- n>=0? )
  cp    TOS,0
  setge cl
  movzx TOS,cl
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
urword_code "UWITHIN",quwithin
;; ( u0 u1 u2 -- flag )
;; u0 >= u1 and u0 < u2
  pop   ebx
  pop   edx
  cmp   edx,TOS
  ld    TOS,0
  jae   @f
  cmp   ebx,edx
  ja    @f
  inc   TOS
@@:
  urnext
urword_end

urword_code "BOUNDS?",qbounds
;; ( u0 u1 u2 -- flag )
;; u0 >= u1 and u0 <= u2
  pop   ebx
  pop   edx
  cmp   edx,TOS
  ld    TOS,0
  ja    @f
  cmp   ebx,edx
  ja    @f
  inc   TOS
@@:
  urnext
urword_end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; the following words are used in various CASE-OF
;;
urword_code "(OF=)",par_of_equ  ;; ( n0 n1 -- n0 n0=n1 )
urword_hidden
  ld    eax,[esp]
  cp    eax,TOS
  sete  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF<>)",par_of_nequ  ;; ( n0 n1 -- n0 n0<>n1? )
urword_hidden
  ld    eax,[esp]
  cp    eax,TOS
  setne cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF<)",par_of_less  ;; ( n0 n1 -- n0 n0<n1 )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setl  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF>)",par_of_gt  ;; ( n0 n1 -- n0>n1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setg  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF<=)",par_of_lessequ  ;; ( n0 n1 -- n<=n1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setle cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF>=)",par_of_gtequ  ;; ( n0 n1 -- n0>=n1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setge cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF-U<)",par_of_uless  ;; ( u0 u1 -- u0<u1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setb  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF-U>)",par_of_ugt  ;; ( u0 u1 -- u0>u1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  seta  cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF-U<=)",par_of_ulessequ  ;; ( u0 u1 -- u0<=u1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setbe cl
  movzx TOS,cl
  urnext
urword_end

urword_code "(OF-U>=)",par_of_ugtequ  ;; ( u0 u1 -- u0>=u1? )
urword_hidden
  ld    eax,[esp]
  cmp   eax,TOS
  setae cl
  movzx TOS,cl
  urnext
urword_end


urword_code "(OF-AND)",par_of_and  ;; ( n0 n1 -- n0 n0&n1 )
urword_hidden
  ld    eax,[esp]
  xchg  eax,TOS
  and   TOS,eax
  urnext
urword_end

urword_code "(OF-~AND)",par_of_notand  ;; ( n0 n1 -- n0&~n1 )
urword_hidden
  not   TOS
  ld    eax,[esp]
  xchg  eax,TOS
  and   TOS,eax
  urnext
urword_end

;; n >= a and n < b
urword_code "(OF-WITHIN)",par_of_within  ;; ( n a b -- a flag )
urword_hidden
  pop   ebx
  ld    eax,[esp]
  ld    edx,eax
  sub   TOS,ebx
  sub   edx,ebx
  sub   edx,TOS
  sbb   TOS,TOS
  neg   TOS      ;; because our "true" is 1, not -1
  urnext
urword_end

;; u >= ua and u < ub (unsigned compare)
urword_code "(OF-UWITHIN)",par_of_uwithin  ;; ( u ua ub -- n flag )
urword_hidden
  pop   ebx
  ld    edx,[esp]
  cmp   edx,TOS
  ld    TOS,0
  jr    ae,@f
  cmp   ebx,edx
  jr    a,@f
  inc   TOS
@@:
  urnext
urword_end

;; u >= ua and u <= ub (unsigned compare)
urword_code "(OF-BOUNDS)",par_of_bounds  ;; ( u ua ub -- n flag )
urword_hidden
  pop   ebx
  ld    edx,[esp]
  cmp   edx,TOS
  ld    TOS,0
  jr    a,@f
  cmp   ebx,edx
  jr    a,@f
  inc   TOS
@@:
  urnext
urword_end
