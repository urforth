;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: os-dir

.( === iterator ===\n)
: dir-test
  " ." open-dir throw >r
  begin pad 256 r@ read-dir throw ?dup while
    ." inode: " .hex8 ."  <" type ." >\n"
  repeat 2drop
  r> close-dir throw
;
dir-test

.( === foreach ===\n)
: dir-test-1
  " ." open-dir throw dup >r
  [:  ( addr count inode -- stopflag )
    2>r over . 2r> ." inode: " .hex8 ."  <" type ." >\n"
  false ;] foreach-dir throw . cr
  r> close-dir throw
;
42 dir-test-1 drop


.stack bye
