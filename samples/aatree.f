(* Andersson Tree -- balanced binary (actually, 2-3) tree *)
(* based on the code from https://web.archive.org/web/20160303175254/http://www.eternallyconfuzzled.com/tuts/datastructures/jsw_tut_andersson.aspx *)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; node manipulation words
;;
use-lib: struct-accessors

(* WARNING! empty tree should be "aat-nil", not 0! *)

(*
  node:
    ptr left
    ptr right
    cell level
    data...
*)

" aat" struct-accessors:begin
  field: left
  field: right
  field: level
  field: data
end-@sizeof


( sentinel node: points to itself, has lowest possible level )
create aat-nil  aat-nil reladdr, aat-nil reladdr, 0 , create;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; new node creation
;;
: aat-node-allocate  ( -- node )  aat-@sizeof brk-alloc ;
: aat-node-free  ( node -- )  -666 swap aat-level! ;


: aat-nil?  ( node -- flag )  aat-nil = ;

0 value aat-created-nodes

: aat-new-node  ( data -- node )
  aat-node-allocate
  aat-nil over aat-left!
  aat-nil over aat-right!
  swap over aat-data!
  dup aat-level^ 1!
  1 +to aat-created-nodes
;

: aat-delete-node  ( node -- )
  aat-created-nodes not-?abort" trying to delete inexisting node"
  dup aat-nil? ?abort" trying to delete nil node"
  dup aat-level@ 0< ?abort" trying to delete already deleted node"
  aat-node-free
  1 -to aat-created-nodes
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; various iterators
: aat-foreach  ( func rootnode -- )
  dup aat-nil? if 2drop
  else
    2dup aat-left@ recurse
    2dup aat-data@ swap execute
    aat-right@ recurse-tail
  endif
;

: aat-maxdepth  ( rootnode -- depth )
  dup aat-nil? if drop 0
  else
    dup aat-left@ recurse
    swap aat-right@ recurse
    max 1+
  endif
;

: aat-count-nodes  ( rootnode -- count )
  dup aat-nil? if drop 0
  else
    dup aat-left@ recurse
    swap aat-right@ recurse
    + 1+
  endif
;

: (aat-addr.)  ( node -- )  dup aat-nil? if drop ." ........" else .hex8 endif ; (hidden)

: aat-dump-tree  ( indent rootnode -- )
  dup aat-nil? if 2drop
  else
    over 4 .r space dup (aat-addr.) space dup aat-data@ 5 .r space dup aat-left@ (aat-addr.) space dup aat-right@ (aat-addr.) space dup aat-level@ 0 .r cr
    over 1+ over aat-left@ recurse
    swap 1+ swap aat-right@ recurse-tail
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tree balance fixups
;;
: aat-skew  ( rootnode -- rootnode )
  dup aat-level@ ?dup if
    over aat-left@ aat-level@ = if
      dup >r       ;; oldroot
      aat-left@  ;; root = root->left
      dup aat-right@ r@ aat-left!  ;; oldroot->left = root->right
      r> over aat-right!  ;; root->right = oldroot
    endif
    dup aat-right@ recurse over aat-right!  ;; root->right = skew(root->right)
  endif
; (hidden)

: aat-split  ( rootnode -- rootnode )
  dup aat-level@ ?dup if
    over aat-right@ aat-right@ aat-level@ = if
      dup >r        ;; oldroot
      aat-right@  ;; root = root->right
      dup aat-left@ r@ aat-right!  ;; save->right = root->left
      r> over aat-left!  ;; root->left = save
      dup aat-level^ 1+!   ;; ++root->level
      dup aat-right@ recurse over aat-right!  ;; root->right = split(root->right)
    endif
  endif
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; cmpfn: ( data0 data1 -- cmpres )
;; cmpres:
;;   <0: data0 < data1
;;   =0: data0 = data1
;;   >0: data0 > data1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tree validity checker
;;

;; node must not be nil
: (aat-xload-nodes-data)  ( cmpfn node movecfa -- )
  >r dup aat-data@ swap r> execute
  dup aat-nil? if 2drop drop rdrop exit endif
  aat-data@ swap rot execute-tail
; (hidden)

;; node must not be nil
: (aat-check-left)  ( cmpfn node -- )
  ['] aat-left@ (aat-xload-nodes-data)
  0>= ?abort" broken tree (left)"
; (hidden)

;; node must not be nil
: (aat-check-right)  ( cmpfn node -- )
  ['] aat-right@ (aat-xload-nodes-data)
  0<= ?abort" broken tree (right)"
; (hidden)

: aat-validate  ( cmpfn rootnode -- )
  dup aat-nil? if 2drop
  else
    2dup (aat-check-left)
    2dup (aat-check-right)
    2dup aat-left@ recurse
    aat-right@ recurse-tail
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; find data in the tree
;;
: aat-find  ( data cmpfn rootnode -- rootnode true // false )
  swap >r begin dup aat-nil? not-while
    2dup aat-data@ swap r@ execute
    dup ifnot drop break endif
    -if aat-right^ else aat-left^ endif @
  repeat rdrop nip
  dup aat-nil? if drop false else true endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; insert data into the tree
;;
: (aat-repair)  ( rootnode -- rootnode )  aat-skew [execute-tail] aat-split ; (hidden)

;; returns "false" and data node if duplcate data found
: aat-insert  ( data cmpfn rootnode -- rootnode true // datanode false )
  dup aat-nil? if 2drop aat-new-node true
  else
    dup >r aat-data@ swap >r  ( data nodedata | rootnode cmpfn )
    ( data cmpfn rootnode flag )
    over r@ execute r> r> rot  ( data cmpfn rootnode flag )
    ?dup ifnot nrot 2drop false exit endif
    ;; root->link[dir] = jsw_insert(root->link[dir], data)
    -if dup aat-right^ else dup aat-left^ endif  ;; root->data < data: right
    ( data cmpfn rootnode childptraddr )
    swap >r dup >r @ recurse  ( node flag | rootnode childptraddr )
    ifnot 2rdrop false exit endif
    r> ! r> (aat-repair) true ;; root = split(skew(root))
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; delete data from the tree
;; note that deleting may move data between nodes (swap)
;; "aat-delete-node" will be called for the node with the right data (maybe swapped)
;;
: (aat-delete-final-fixup)  ( rootnode -- rootnode )
  ;; min(root->left->level, root->right->level) < root->level-1
  dup >r aat-left@ aat-level@ r@ aat-right@ aat-level@ min r@ aat-level@ 1- < r> swap if
    dup aat-level^ 1-!  ;; --root->level
    ;; root->right->level = min(root->right->level, root->level)
    dup aat-level@ over aat-right@ aat-level@ min over aat-right@ aat-level!
    [execute-tail] (aat-repair)
  endif
; (hidden)

;; delete rightmost leaf
: (aat-delete-heir)  ( node -- node )
  dup aat-right@ dup aat-nil? if drop dup aat-left@ swap aat-delete-node
  else  \ root->right = aat-delete-heir(root->right)
    recurse over aat-right! [execute-tail] (aat-delete-final-fixup)
  endif
; (hidden)

: (aat-swap-datas)  ( node0 node1 -- )
  2dup aat-data@ swap aat-data@  ( node0 node1 node1data node0data )
  rot aat-data! swap aat-data!
;

;; used to swap datas in delete
: (aat-find-rightmost)  ( node -- node )
  0 swap begin nip dup aat-right@ dup aat-nil? until drop
; (hidden)

;; this is called when we need to remove a rootnode with two children
;; heir is rootnode->left
: (aat-delete-raise)  ( heir rootnode -- rootnode )
  swap 2dup (aat-find-rightmost) (aat-swap-datas)  ;; swap datas
  (aat-delete-heir)
  over aat-left!
  [execute-tail] (aat-delete-final-fixup)
; (hidden)

;; returns "false" if no data found
: aat-delete  ( data cmpfn rootnode -- rootnode true // false )
  dup aat-nil? if drop 2drop false
  else
    >r 2dup r@ aat-data@ nrot execute ?dup ifnot nrot 2drop  ;; data found, we don't need it (and comparator) anymore
      r@ aat-left@  \ heir = root->left
      dup aat-nil? if drop r@ aat-right@ r> aat-delete-node
      else r@ aat-right@ aat-nil? if r> aat-delete-node
      else r> (aat-delete-raise) endif endif
    else  ;; not equal, recurse to find the data
      ( data cmpfn flag | rootnode )
      \ root->leftright = jsw_remove(root->leftright, data)
      -if r@ aat-right^ else r@ aat-left^ endif
      dup >r @ recurse ifnot 2rdrop false exit endif
      r> ! r> (aat-delete-final-fixup)
    endif
    true
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; test/demo
;;
false constant debug-print

2048 constant value-count
: prng-seed  ( -- dlo dhi )  69 666 ;


aat-nil value aatree

: test-creation  ( -- )
  prng-seed
  value-count for
    prng:pcg32-next 0xffff and
    ['] ncmp aatree aat-insert ifnot
      [ debug-print ] [IF]
        aat-data@ ." duplicate: " . cr
      [ELSE] drop [ENDIF]
    else to aatree ['] ncmp aatree aat-validate endif
  endfor 2drop
;
test-creation

: test-presence  ( -- )
  ['] ncmp aatree aat-validate
  prng-seed
  value-count for
    prng:pcg32-next 0xffff and
    dup ['] ncmp aatree aat-find ifnot ." NOT FOUND: " . cr abort" broken" endif
    aat-data@ = not-?abort" wuta?!"
  endfor 2drop
;
test-presence

." max tree depth: " aatree aat-maxdepth . ." (" aatree aat-count-nodes . ." nodes)\n"

\ [: . cr ;] aatree aat-foreach
\ 0 aatree aat-dump-tree

: test-deletion  ( -- )
  prng-seed
  begin aatree aat-nil? not-while
    prng:pcg32-next 0xffff and
      \ ." === removing: " dup . cr
      \ ." +++ before tree +++\n" 0 aatree aat-dump-tree  \ [: 2 spaces . cr ;] aatree aat-foreach
    dup ['] ncmp aatree aat-delete ifnot
      [ debug-print ] [IF]
        ." missing: " . cr
      [ELSE] drop [ENDIF]
    else nip to aatree
        \ ." +++ after tree +++\n" 0 aatree aat-dump-tree  \ [: 2 spaces . cr ;] aatree aat-foreach
      ['] ncmp aatree aat-validate
    endif
  repeat 2drop
;
test-deletion

aat-created-nodes ?abort" some nodes were not deleted"

.stack bye
