require m-inherited minioof


\ use-lib: ans-tty

." ROOT OBJECT=0x" object .hex8 cr

Object class
  cell var text
  cell var len
  cell var x
  cell var y
  method init
  method draw
end-class Button
." BUTTON=0x" Button .hex8 cr

;; Now, implement the two methods, draw and init:

:noname  ( o -- ) >r
  \ r@ x @ r@ y @ at-xy
  ." x=" r@ x @ 0 .r ." ; y=" r@ y @ 0 .r ." ; text=<"
  r@ text @ r> len @ type
  ." >"
;
Button defines draw

:noname  ( addr u o -- ) >r
." INIT: class=0x" r@ @class .hex8 ." ; parentclass=0x" r@ @class @class-parent .hex8 cr
  6 r@ x ! 12 r@ y ! dup r@ len !
  ;; allocate string at "here"
  dup n-allot dup r> text ! swap move
;
Button defines init

;; For inheritance, we define a class bold-button, with no new data and no new methods.

Button class
end-class Bold-Button
." BOLD-BUTTON=0x" Bold-Button .hex8 cr

: bold    ( .ansi-escape ." 1m" ) ." [BOLD]" ;
: normal  ( .ansi-escape ." 0m" ) ." [NORMAL]" ;

\ this does early (static) binding
\ :noname  ( o -- ) bold [ Button :: draw ] normal ;
\ this does dynamic binding
\ :noname  ( o -- ) bold  dup @class @class-parent m-invoke draw  normal ;
\ or this (dynamic too)
:noname  ( o -- ) bold  m-inherited draw  normal ;
Bold-Button defines draw

;; And finally, some code to demonstrate how to create objects and apply methods:

Button new constant foo
s" thin foo" foo init
\ page
foo draw cr
Bold-Button new constant bar
s" fat bar" bar init
bar y 1+!
2 bar x +!
bar draw cr

\ 0 999 at-xy

.stack
bye
