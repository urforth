;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; slightly more complicated OO system than mini-oof
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: oof


0 oop:class
  class-field clsvar
  field x
  field y
  method init
  method xy!
  method .xy
  class-method clsinit
end-class: Object

.( Object cls=) Object .hex8 cr

Object method: clsinit  ( -- )
  endcr ." clsinit: cls=" self .hex8 cr
  42 to clsvar
  endcr ." class init! clsvar=" clsvar . cr
;

Object method: init  ( -- )
  ." init " class-name type ." (" static-class-name type ." ) ! self=" self .hex8
  ."  class=" my-class .hex8 cr
  .xy cr
  ."  clsvar=" clsvar . cr
  clsinit
  42 to x  666 to y
  .xy cr
;

Object method: .xy  ( -- )  ." x=" x . ." y=" y 0 .r ;

Object ::invoke Object clsinit

Object oop:new-allot value obj
obj .hex8 cr

obj ::invoke Object init

order
(self@) . cr
.stack


Object oop:class
  method child-mt
  field child-var
  class-method child-class-mt
  class-field child-cv
  method dispatch-test
  method (unknown-dispatch)  ( ... addr count -- )
end-class: Child

Child method: init  ( -- )
  ." init " class-name type ." (" static-class-name type ." ) ! self=" self .hex8
  ."  class=" my-class .hex8 cr
  call-super
  ." child inited; " .xy cr
  ." :0:x=" self var^ x @ . cr
  \ ." :1:x=" addr: x @ . cr
  ." :1:x=" to^ x @ . cr
  inherited clsinit
;

Child method: .xy  ( -- )  ." [" inherited .xy ." ]" ;

Child method: (unknown-dispatch)  ( ... addr count -- )
  ." UNKNOWN DISPATCH \`" type ." \` in self=0x" self .hex8 cr
;

Child method: dispatch-test  ( -- )
  ." testing dispatch: self=0x" self .hex8 ."  class=0x" my-class .hex8 cr
  \ " child-cv" dispatch-str
  " child-mt" dispatch-str
  ." dispatch complete: self=0x" self .hex8 cr
  ." dispatch class: " self invoke class-name type cr
  ." static class: 0x" static-class .hex8 cr
;

Child oop:new-allot value childobj
childobj .hex8 cr

childobj ::invoke Object init

obj ::invoke Object init

: a childobj ::invoke Object init ;
a

Child method: child-class-mt  child-cv . cr 69 to child-cv ." Child class method; " child-cv . cr ;
Child method: child-mt  child-var . cr 666 to child-var ." childcv! " child-cv . child-var . cr ;

Child Child:: child-class-mt
childobj Child:: child-mt

childobj ::invoke Child dispatch-test

childobj " child-yy" ::dispatch-str
childobj " dispatch-test" ::dispatch-str
childobj " child-xx" ::dispatch-str
childobj " child-mt" ::dispatch-str
childobj ." *** class name: " ::invoke Object class-name type cr

childobj ::invoke Child x ." x=" . cr

order
(self@) . cr
.stack
bye
