use-lib: unix-time

: dump-utime  ( utime -- )  >r
  ." ===========\n"
  r@ unix-time:->time unix-time:(.time) type cr
  r@ unix-time:->date unix-time:(.date) type cr
  r@ unix-time:->date unix-time:date->dow unix-time:dow->long-name type ." |" cr
  r@ unix-time:->date unix-time:date->dow unix-time:dow->short-name type ." |" cr

  r@ unix-time:->date
  ." year : " rot . cr
  ." month: " swap unix-time:month->long-name type cr
  ." day  : " . cr

  r@ unix-time:->date
  ." year : " rot . cr
  ." month: " swap unix-time:month->short-name type cr
  ." day  : " . cr

  r@ unix-time:->date/time unix-time:date/time-> r> = not-?abort" invalid roundtrip"
;

1147483647 dump-utime
1157483647 dump-utime
1168483647 dump-utime
1172683647 dump-utime
1172713647 dump-utime


.stack bye
