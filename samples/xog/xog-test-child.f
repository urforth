use-lib: xog

0 value timer-counter

also x11 also xconst

CoreFont oop:new-allot oop:value font BaseFont


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Button oop:class
end-class: MyButton


MyButton method: init  ( -- )
  call-super
  64 to posx  128 to posy
;

MyButton method: on-created  ( -- )
  ." button " (debug-id.) space ." created.\n"
;

MyButton method: on-destroyed  ( -- )
  ." button " (debug-id.) space ." destroyed!\n"
;


MyButton oop:new-allot oop:value but0:: MyButton
MyButton oop:new-allot oop:value but1:: MyButton


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow oop:class
  field bpress-count

  method button-pressed  ( -- )
  method draw-it  ( -- )
end-class: MyWindow

MyWindow oop:new-allot oop:value win:: MyWindow

(*
MyWindow method: PropertyNotify-Handler  ( -- )
  (event) XPropertyEvent atom @ xog-dpy XGetAtomName >r
  endcr ." PROPERTY \`" r@ zcount type ." \` is "
  (event) XPropertyEvent state @ PropertyDelete = if ." deleted" else ." set" endif
  cr r> XFree
;
*)

MyWindow method: get-caption  ( -- addr count )  " Simple UrForth Window" ;

MyWindow method: button-pressed  ( -- )
  1 +to bpress-count
  weak-invalidate
;

MyWindow method: init  ( -- )
  call-super
  128 64 set-min-size
  612 356 set-max-size
  0 to bpress-count

  but0:: init
  " &Button" but0:: set-caption
  [: ." *** Button " .hex8 ."  pressed!\n"
    win:: button-pressed
  ;] but0:: set-action
  \ [char] B but0:: set-hotkey
  but0:: self append-child

  but1:: init
  " &Quit" but1:: set-caption
  [: MyButton:: top-parent BaseWindow:: close ;] but1:: set-action
  but1:: posx but0:: width + 8 + but1:: posy but1:: set-pos
  \ [char] Q but1:: set-hotkey
  but1:: self append-child
;

MyWindow method: on-created  ( -- )
  ." creating buttons...\n"
  \ self but0:: create-ex  not-?abort" cannot create button"
  \ but0:: show

  \ self but1:: create-ex  not-?abort" cannot create button"
  \ but1:: show

  0 (debug-dump-children)
;

MyWindow method: on-destroyed  ( -- )
  ." MyWindow destroyed!\n"
;

MyWindow method: bg-color  ( -- color )  " #0000ff" xog-get-color ;

MyWindow method: draw-it  ( -- )
  \ " #00ff00" set-named-color
  \ 0 0 8192 8192 fill-rect

  (basewin-debug-show-events?) if width 0 .r ." x" height . cr endif

  " #0000ff" set-named-color
  0 0 width height fill-rect

  " #f0fff0" set-named-color
  0 0 width height draw-line
  width 0 0 height draw-line

  " #ff0000" set-named-color
  1 1 32 100 fill-rect

  " #ff0000" set-named-color
  width 120 - 2/ height 92 - 2/ 120 92 draw-ellipse

  " #00ffff" set-named-color
  width 100 - 2/ height 62 - 2/ 100 62 fill-ellipse

  " #ff7f00" set-named-color
  " HELLO!" 2dup 1 1 self font fill
  2dup font log-height 1+ 1 swap self font draw

  " #ffffff" set-named-color
  30 30 width 60 - height 60 - 4 8 draw-rounded-rect

  " #aaaaaa" set-named-color
  width 2/ 30 - 42 60 20 6 4 fill-rounded-rect

  " #ffff00" set-named-color
  timer-counter <#u #s #>
  2dup font log-width width swap - 1 self font fill

  bpress-count ?dup if
    <#u #s " times pressed: " holds #> 128 32 self font fill
  endif
;


MyWindow method: on-draw   ( -- )
  draw-it
;

MyWindow method: on-keydown  ( keysym -- )
  case
    XK_Escape of close endof
    32 of (send-expose-event) endof
    XK_Return of (basewin-debug-show-events?) not to (basewin-debug-show-events?) endof
  endcase
;

\ MyWindow method: on-keyup  ( keysym -- )  drop ;

MyWindow method: on-close-query  ( -- allow-close-flag )
  true ." close query for mywindow " (debug-id.) cr
;

MyWindow method: on-state-change  ( newstate -- )
  ." mywindow " (debug-id.) space ." newstate: "
  case
    WithdrawnState of ." widthdrawn\n" endof
    NormalState of ." normal\n" endof
    IconicState of ." iconic\n" endof
    otherwise . cr
  endcase
;


MyWindow method: on-button-down  ( bnum -- )
  ." mywindow " (debug-id.) space ." bdown: " . cr
  focus
;

MyWindow method: on-button-up  ( bnum -- )
  ." mywindow " (debug-id.) space ." bup: " . cr
;

MyWindow method: on-focus  ( -- )
  ." mywindow " (debug-id.) space ." focus\n"
;

MyWindow method: on-blur  ( -- )
  ." mywindow " (debug-id.) space ." blur\n"
;

MyWindow method: on-show  ( -- )
  ." mywindow " (debug-id.) space ." on-show: visible=" visible? . ."  mapped=" mapped? . cr
;

MyWindow method: on-hide  ( -- )
  ." mywindow " (debug-id.) space ." on-hide: visible=" visible? . ."  mapped=" mapped? . cr
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
..: (xog-after-event-handle)  ( -- )
  xog-has-event-timeout? ifnot
    \ endcr ." TIMER!\n"
    1000 xog-set-event-timeout
    1 +to timer-counter
    win:: weak-invalidate
  endif
;..


: run-main  ( -- )
  xog-open-dpy

  14 FontType-Normal font build-font-name
  font create not-?abort" cannot create X11 font"

  win:: init
  win:: create
  win:: show

  1000 xog-set-event-timeout
  xog-event-loop

  font destroy

  xog-close-dpy
;

run-main

.stack bye
