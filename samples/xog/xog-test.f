use-lib: xog

false constant use-draw-part?

0 value timer-counter

also x11 also xconst

CoreFont oop:new-allot oop:value font BaseFont


BaseWindow oop:class
  method draw-it  ( -- )
end-class: MyWindow


MyWindow method: get-caption  ( -- addr count )  " Simple UrForth Window" ;

MyWindow method: init  ( -- )
  call-super
  128 64 set-min-size
  612 356 set-max-size
;

MyWindow method: on-created  ( -- )  ." MyWindow created\n"  ;
MyWindow method: on-destroyed  ( -- )  ." MyWindow destroyed\n" ;
MyWindow method: on-show   ( -- )  ." MyWindow is visible\n" ;
MyWindow method: on-hide   ( -- )  ." MyWindow is invisible\n" ;

MyWindow method: draw-it  ( -- )
  \ " #00ff00" set-named-color
  \ 0 0 8192 8192 fill-rect

  (basewin-debug-show-events?) if width 0 .r ." x" height . cr endif

  " #0000ff" set-named-color
  0 0 width height fill-rect

  " #f0fff0" set-named-color
  0 0 width height draw-line
  width 0 0 height draw-line

  " #ff0000" set-named-color
  1 1 32 100 fill-rect

  " #ff0000" set-named-color
  width 120 - 2/ height 92 - 2/ 120 92 draw-ellipse

  " #00ffff" set-named-color
  width 100 - 2/ height 62 - 2/ 100 62 fill-ellipse

  " #ff7f00" set-named-color
  " HELLO!" 2dup 1 1 self font fill
  2dup font log-height 1+ 1 swap self font draw

  " #ffffff" set-named-color
  30 30 width 60 - height 60 - 4 8 draw-rounded-rect

  " #aaaaaa" set-named-color
  width 2/ 30 - 42 60 20 6 4 fill-rounded-rect

  " #ffff00" set-named-color
  timer-counter <#u #s #>
  2dup font log-width width swap - 1 self font fill
;


use-draw-part? [IF]
MyWindow method: on-draw-part   ( x y width height count -- )
  >r
  (basewin-debug-show-events?) if
    ." EXPOSE! x=" 2swap swap . ." y=" . ." width=" swap . ." height=" . ." count=" r@ . cr
  else 2drop 2drop endif
  ;; process only the very last expose event
  r> ifnot draw-it endif
;
[ELSE]
MyWindow method: on-draw   ( -- )
  draw-it
;
[ENDIF]

MyWindow method: on-resize ( oldwidth oldheight -- )
  ." resizing from " swap 0 .r ." x" . ." to " width 0 .r ." x" height . cr
;

MyWindow method: on-keydown  ( keysym -- )
  case
    XK_Escape of close endof
    32 of (send-expose-event) endof
    XK_Return of (basewin-debug-show-events?) not to (basewin-debug-show-events?) endof
  endcase
;

MyWindow method: on-keyup  ( keysym -- )  drop ;

MyWindow method: on-close-query  ( -- allow-close-flag )  true ." closing MyWindow\n" ;



\ MyWindow oop:new-allot oop:value win MyWindow
0 oop:value win MyWindow
MyWindow oop:new-allot oop:to win


: dump-rect  ( x y width height -- )
  swap 2swap swap ." x=" . ." y=" . ." width=" . ." height=" 0 .r
;

0 var event-count

..: (xog-after-event-read)  ( -- )  event-count 1+! ;..

..: (xog-after-event-handle)  ( -- )
  xog-has-event-timeout? ifnot
    \ endcr ." TIMER!\n"
    1000 xog-set-event-timeout
    1 +to timer-counter
    win invalidate
  endif
;..


: run-main  ( -- )
  xog-open-dpy

  16 FontType-Normal font build-font-name
  font create not-?abort" cannot create X11 font"

  ." font: ascent=" font ascent@ . ." descent=" font descent@ . cr

  " FOO!" 2dup font ink-extents dump-rect cr font log-extents dump-rect cr

  \ true to (basewin-debug-show-events?)

  win init
  win create
  win show

  1000 xog-set-event-timeout
  xog-event-loop

  font destroy

  xog-close-dpy
  event-count @ . ." events processed\n"
;

run-main

.stack bye
