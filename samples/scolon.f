;; scattered colon example
;; based on the idea by M.L. Gassanenko ( mlg@forth.org )
;; implemented from scratch by Ketmar Dark

: init ... ." init complete\n" ;
init

.( =================\n)
..: init ." init phase1\n" ;..
init

.( =================\n)
..: init ." init phase2\n" ;..
init

.( =================\n)
..: init ." init phase3\n" ;..
init

.( =================\n)
..: init ." init phase4\n" ;..
init

.( =================\n)
..: init ." preinit phase 1\n" <;..
init

.( =================\n)
..: init ." preinit phase 0\n" <;..
init

.( =================\n)
..: init ." init phase5\n" ;..
init

.( =================\n)

.( =================\n)
: init2 ... ." init2 complete\n" ;
init2

.( =================\n)
..: init2 ." pteinit2 phase1\n" <;..
init2

.( =================\n)
..: init2 ." init2 phase0\n" ;..
init2

.( =================\n)
..: init2 ." pteinit2 phase0\n" <;..
init2

.( =================\n)
..: init2 ." init2 phase1\n" ;..
init2

.stack
bye
