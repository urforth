here os:gettickcount
use-lib: x11
os:gettickcount rot here swap - . ." bytes, loaded in " swap - . ." msecs\n"

also x11 also xconst

0 value dpy
0 value win
1 value win-width
1 value win-height
0 value fontset
0 value myGC

512 value win-default-width
256 value win-default-height

0 value WM_PROTOCOLS
0 var XA_WM_DELETE_WINDOW

: open-dpy ( -- )
  0 XOpenDisplay to dpy
  dpy ifnot ." FATAL: cannot open X11 display!\n" 1 n-bye endif
  false " WM_PROTOCOLS" drop dpy XInternAtom to WM_PROTOCOLS
  false " WM_DELETE_WINDOW" drop dpy XInternAtom XA_WM_DELETE_WINDOW !
;

: win-set-xhints  ( -- )
  ;; class hints
  XClassHint @sizeof ralloca >r
  r@ XClassHint @sizeof erase
  " UrForth X11 Demo" drop dup r@ XClassHint res_name ! r@ XClassHint res_class !
  r@  ;; class hints address
  ;; WM hints
  XWMHints @sizeof ralloca >r
  r@ XWMHints @sizeof erase
  InputHint r@ XWMHints flags !
  true r@ XWMHints input !
  r@  ;; WM hints address
  ;; size hints
  XSizeHints @sizeof ralloca >r
  r@ XSizeHints @sizeof erase
  [ PMinSize PMaxSize or ] literal r@ XSizeHints flags or!
  win-default-width dup r@ XSizeHints min_width ! 256 + r@ XSizeHints max_width !
  win-default-height dup r@ XSizeHints min_height ! 256 + r@ XSizeHints max_height !
  r@  ;; size hints address
  0 0 0 0  ;; argc, argv, iconname windowname
  win dpy XSetWMProperties drop
  ;; free structures
  rdrop XSizeHints @sizeof rdealloca
  rdrop XWMHints @sizeof rdealloca
  rdrop XClassHint @sizeof rdealloca
;

: create-simple-win  ( titleaddr titlecount -- )
  0 0  ;; bg border
  0    ;; bwidth
  win-default-height win-default-width 0 0  ;; h w y x
  dpy XDefaultRootWindow dpy  ;; parent dpy
  XCreateSimpleWindow to win
  dpy ifnot ." FATAL: cannot create X11 window!\n" 1 n-bye endif
  1 XA_WM_DELETE_WINDOW win dpy XSetWMProtocols . cr
  ;; store title
  ensure-asciiz >r win dpy XStoreName r> free-asciiz
  ;; set input mask
  [ NoEventMask
    KeyPressMask or
    KeyReleaseMask or
    ButtonPressMask or
    ButtonReleaseMask or
    EnterWindowMask or
    LeaveWindowMask or
    PointerMotionMask or
    \ PointerMotionHintMask or
    \ Button1MotionMask or
    \ Button2MotionMask or
    \ Button3MotionMask or
    \ Button4MotionMask or
    \ Button5MotionMask or
    \ ButtonMotionMask or
    \ KeymapStateMask or
    ExposureMask or
    VisibilityChangeMask or
    StructureNotifyMask or
    \ ResizeRedirectMask or
    SubstructureNotifyMask or
    \ SubstructureRedirectMask or
    FocusChangeMask or
    PropertyChangeMask or
    \ ColormapChangeMask or
    \ OwnerGrabButtonMask or
  ] literal
  win dpy XSelectInput
  win-set-xhints
  win dpy XMapWindow
  \ 0 dpy XSync
;

: init-window  ( -- )
  open-dpy
  " UrForth X11 Demo" create-simple-win
;

: init-font  ( -- )
  \ 0 var crap cell allot
  2 cells ralloca >r
  0 ( crap) r@ dup cell+ " -*-helvetica-*-r-*-*-14-*-*-*-*-*-*-*" drop dpy XCreateFontSet
  ?dup ifnot
    ." cannot create Helvetica fontset, trying \`*\`...\n"
    0 ( crap) r@ dup cell+ " -*-*-*-r-*-*-16-*-*-*-*-*-*-*" drop dpy XCreateFontSet dup not-?abort" cannot create 'any' fontset"
  endif
  to fontset
  rdrop 2 cells rdealloca
;

: init-gc  ( -- )
  0 0 win dpy XCreateGC to myGC drop  ;; "drop" due to "..."
;

: deinit-window  ( -- )
  win dpy XUnmapWindow
  win dpy XDestroyWindow . cr
  0 to win
;

: deinit-fontset  ( -- )
  fontset dpy XFreeFontSet . cr
  0 to fontset
;

: deinit-gc  ( -- )
  myGC dpy XFreeGC
  0 to myGC
;

: deinit-dpy  ( -- )
  dpy XCloseDisplay . cr
  0 to dpy
;


\ in the real app you should cache this
: get-color  ( addr count -- color )
  XColor @sizeof ralloca >r
  r@ dup  ;; xcolor
  2swap ensure-asciiz >r  ;; colorname
  dpy XDefaultScreen dpy XDefaultColormap  ;; cmap
  dpy XAllocNamedColor not-?abort" cannot allocate color"
  r> free-asciiz
  r> XColor pixel @
  XColor @sizeof rdealloca
;


create Handlers  [: LASTEvent 1+ for  ['] noop reladdr, endfor ;] execute

: Event!  ( n -- )  cells Handlers + ! ;
: Event@  ( n -- )  dup 0< over LASTEvent > or if drop LASTEvent endif cells Handlers + @ ;

create event XEvent @sizeof allot
0 var event-count

: handle-event  ( flag -- )
  if dpy XPending else true endif
  if
    event dpy XNextEvent event-count 1+!
    \ ." EVENT: " event @ . cr
    event @ Event@ execute
    (sp-check) ifnot err-stack-underflow error 1 n-bye endif
  endif
;


false value quit?
false value win-mapped?

0 var comp_stat 0 ,
0 var look_key
create look_chars 4 allot

:noname  ( -- )
  ." key pressed: " event XKeyEvent keycode @ .
                    event XKeyEvent state @ .
  0  event XKeyEvent keycode @  dpy XKeycodeToKeysym dup . cr
  XK_Escape = if true to quit? endif
;  KeyPress Event!

:noname  ( -- )
  ." key released: " event XKeyEvent keycode @ . cr
;  KeyRelease Event!

:noname  ( -- )
  ." button pressed: " event XButtonEvent button @ .
                       event XButtonEvent state @ .hex8 space
                       event XButtonEvent time @ u. cr
; ButtonPress Event!

:noname  ( -- )
  ." button released: " event XButtonEvent button @ .
                        event XButtonEvent state @ .hex8 space
                        event XButtonEvent time @ u. cr
; ButtonRelease Event!

:noname  ( -- )
  ." enter: " event XCrossingEvent focus @ .
              event XCrossingEvent detail @ . cr
; EnterNotify Event!

:noname  ( -- )
  ." leave: " event XCrossingEvent focus @ .
              event XCrossingEvent detail @ . cr
; LeaveNotify Event!

:noname  ( -- )
  ." focus in: " event XFocusChangeEvent mode @ .
                 event XFocusChangeEvent detail @ . cr
; FocusIn Event!

:noname  ( -- )
  ." focus out: " event XFocusChangeEvent mode @ .
                  event XFocusChangeEvent detail @ . cr
; FocusOut Event!

: (win-draw)  ( -- )
  win-mapped? if
    ." *** DRAW WINDOW ***\n"

    " #0000ff" get-color myGC dpy XSetForeground
      win-width . ." x" win-height . cr
    \ win-height win-width 0 0 myGC win dpy XFillRectangle
    8192 8192 0 0 myGC win dpy XFillRectangle

    " #ff0000" get-color myGC dpy XSetForeground
    100 32 1 1 myGC win dpy XFillRectangle

    " #ff7f00" get-color myGC dpy XSetForeground
    " HELLO!" swap 16 2 myGC fontset win dpy XmbDrawImageString
  endif
;

:noname  ( -- )
  ." expose: " event XExposeEvent width @ 0 u.r ." x"
               event XExposeEvent height @ .
               event XExposeEvent count @ . cr
  (win-draw)
; Expose Event!

:noname  ( -- )
  ." graphics expose: " event XGraphicsExposeEvent width @ 0 u.r ." x"
               event XGraphicsExposeEvent height @ .
               event XGraphicsExposeEvent count @ .
               event XGraphicsExposeEvent major_code @ .
               event XGraphicsExposeEvent minor_code @ . cr
; GraphicsExpose Event!

:noname  ( -- )
  ." no expose: "
               event XNoExposeEvent major_code @ .
               event XNoExposeEvent minor_code @ . cr
; NoExpose Event!

:noname  ( -- )  ." motion: " event XMotionEvent is_hint @ . cr  ; MotionNotify Event!
:noname  ( -- )  ." visibility: " event XVisibilityEvent state @ . cr ; VisibilityNotify Event!
:noname  ( -- )  ." create: " cr ; CreateNotify Event!
:noname  ( -- )  ." destroy: " cr ; DestroyNotify Event!
:noname  ( -- )  ." map request: " cr ; MapRequest Event!
:noname  ( -- )  ." reparent: " cr ; ReparentNotify Event!
:noname  ( -- )  ." gravity: " cr ; GravityNotify Event!
:noname  ( -- )  ." circulate: " event XCirculateEvent place @ . cr ; CirculateNotify Event!
:noname  ( -- )  ." selection clear: " cr ; SelectionClear Event!
:noname  ( -- )  ." selection request: " cr ; SelectionRequest Event!
:noname  ( -- )  ." selection: " cr ; SelectionNotify Event!
:noname  ( -- )  ." colormap: " cr ; ColormapNotify Event!

:noname  ( -- )  ." unmap: " cr  false to win-mapped? ; UnmapNotify Event!
:noname  ( -- )  ." map: " cr  true to win-mapped? ; MapNotify Event!

:noname  ( -- )
  ." configure: " event XConfigureEvent width @ 0 u.r ." x"
                  event XConfigureEvent height @ . cr
  event XConfigureEvent width @ to win-width
  event XConfigureEvent height @ to win-height
; ConfigureNotify Event!

:noname  ( -- )
  ." configure request: "
                  event XConfigureRequestEvent width @ 0 u.r ." x"
                  event XConfigureRequestEvent height @ . cr
; ConfigureRequest Event!

:noname  ( -- )
  ." resize: " event XResizeRequestEvent width @ 0 u.r ." x"
               event XResizeRequestEvent height @ . cr
; ResizeRequest Event!

:noname  ( -- )
  ." circulate request: "
   event XCirculateRequestEvent place @ . cr
; CirculateRequest Event!

:noname  ( -- )
  ." property: " event XPropertyEvent state @ . cr
; PropertyNotify Event!

:noname  ( -- )
  ." client message: " event XClientMessageEvent message_type @ . cr
  event XClientMessageEvent message_type @ WM_PROTOCOLS = if
    event XClientMessageEvent data @  XA_WM_DELETE_WINDOW @ = if
      ." *** QUIT ***\n"
      true to quit?
    endif
  endif
; ClientMessage Event!

:noname  ( -- )
  ." mapping: "
  event XMappingEvent request @ .
  event XMappingEvent first_keycode @ .
  event XMappingEvent count @ . cr
; MappingNotify Event!


: event-loop  ( -- )
  begin false handle-event quit? until
;

: run-main  ( -- )
  false to quit?
  false to win-mapped?

  init-window
  init-font
  init-gc

  event-loop

  deinit-window
  deinit-fontset
  deinit-gc
  deinit-dpy

  event-count @ . ." events processed\n"
;
\ " z00.elf" save not-?abort" cannot save!\n"

run-main

.stack
bye
