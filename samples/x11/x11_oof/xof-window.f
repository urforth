use-libs: oof locals x11
also x11 also xconst


0 value dpy  ;; X11 display

0 value WM_PROTOCOLS
0 value XOF_INTERNAL_CLOSE
0 var XA_WM_DELETE_WINDOW


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ in the real app you should cache this
: x11-get-color  ( addr count -- color )
  dpy if
    XColor @sizeof ralloca >r
    r@ dup  ;; xcolor
    2swap ensure-asciiz >r  ;; colorname
    dpy XDefaultScreen dpy XDefaultColormap  ;; cmap
    dpy XAllocNamedColor not-?abort" cannot allocate color"
    r> free-asciiz
    r> XColor pixel @
    XColor @sizeof rdealloca
  else 2drop 0 endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 oop:class
  var fset  ;; XFontSet
  var ascent
  var descent
  var max-width

  method (setup-font-sizes)

  method init  ( -- )
  method create  ( addr count -- successflag )
  method destroy

  method get-fset  ( -- fset )
  method get-ascent  ( -- ascent )
  method get-descent  ( -- descent )
  method get-height  ( -- height )
  method get-max-width  ( -- max-width )

  ;; y is "up from the base line"
  method text-ink-extents  ( addr count -- x y width height )
  method text-log-extents  ( addr count -- x y width height )

  method text-ink-width  ( addr count -- width )
  method text-ink-height  ( addr count -- height )
  method text-log-width  ( addr count -- width )
  method text-log-height  ( addr count -- height )
end-class: BaseFont


BaseFont method: init  ( -- )
  fset 0!
  ascent 0! descent 0! max-width 0!
;


BaseFont method: destroy  ( -- )
  fset @ if
    dpy if fset @ dpy XFreeFontSet drop endif
    fset 0!
    ascent 0! descent 0! max-width 0!
  endif
;


BaseFont method: (setup-font-sizes)  ( -- )
  ascent 0! descent 0! max-width 0!  ;; just in case
  2 cells ralloca >r  ;; XFontStruct **, char **font_names
  r@ 2 cells erase
  r@ cell+ ( names) r@ ( fstructs) fset @ XFontsOfFontSet  ;; returns count
  begin dup +while
    (*
      ." font: " r@ cell+ @ @ zcount type space
      ." ascent: " r@ @ @ XFontStruct ascent c@ .
      ." descent: " r@ @ @ XFontStruct descent c@ . cr
    *)
    r@ @ @
    dup XFontStruct ascent c@ ascent @ max ascent !
    XFontStruct descent c@ descent @ max descent !
    cell r@ +! cell r@ cell+ +! 1-  ;; is this right?
  repeat drop rdrop 2 cells rdealloca
  (*
    ." final ascent: " ascent @ . cr
    ." final descent: " descent @ . cr
  *)
;


BaseFont method: create  ( addr count -- successflag )
  destroy
  dpy not-?abort" cannot create fontset without opened X11 display"
  ensure-asciiz >r
  2 cells ralloca >r
  0 r@ rot >r dup cell+ r> dpy XCreateFontSet
  rdrop 2 cells rdealloca r> free-asciiz
  dup fset ! notnot
  dup if (setup-font-sizes) endif
;


BaseFont method: get-fset  ( -- fset )  fset @ ;
BaseFont method: get-ascent  ( -- ascent )  ascent @ ;
BaseFont method: get-descent  ( -- descent )  descent @ ;
BaseFont method: get-height  ( -- height )  ascent @ descent @ + ;
BaseFont method: get-max-width  ( -- max-width )  max-width @ ;

: (xrect>stack)  ( xrect-addr -- x y width height )  >r
  r@ XRectangle x w@ w>s r@ XRectangle y w@ w>s
  r@ XRectangle width w@ w>s r> XRectangle height w@ w>s
;

;; `XmbTextExtents` returns log width

;; y is "up from the base line"
BaseFont method: text-ink-extents  ( addr count -- x y width height )
  0 max fset @ over or if
    0 XRectangle @sizeof ralloca dup >r 2swap swap fset @ XmbTextExtents drop \ ." !!!" . cr
    r> (xrect>stack) XRectangle @sizeof rdealloca
  else 2drop 0 0 0 0 endif
;

;; y is "up from the base line"
BaseFont method: text-log-extents  ( addr count -- x y width height )
  0 max fset @ over or if
    XRectangle @sizeof ralloca dup >r 0 2swap swap fset @ XmbTextExtents drop \ ." !!!" . cr
    r> (xrect>stack) XRectangle @sizeof rdealloca
  else 2drop 0 0 0 0 endif
;

BaseFont method: text-ink-width  ( addr count -- width )  text-ink-extents 2swap 2drop drop ;
BaseFont method: text-ink-height  ( addr count -- height )  text-ink-extents 2swap 2drop nip ;
BaseFont method: text-log-width  ( addr count -- width )  text-log-extents 2swap 2drop drop ;
BaseFont method: text-log-height  ( addr count -- height )  text-log-extents 2swap 2drop nip ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseFont oop:new-allot oop:value default-fset BaseFont


: init-default-fontset ( -- )
  default-fset init
  " -*-helvetica-*-r-*-*-14-*-*-*-*-*-*-*" default-fset create ifnot
    ." cannot create Helvetica default fontset, trying \`*\`...\n"
    " -*-*-*-r-*-*-16-*-*-*-*-*-*-*" default-fset create ifnot
      ." cannot create any fontset. oops.\n"
    endif
  endif
;


: deinit-default-fontset  ( -- )
  default-fset destroy 0 oop:to default-fset
;

: open-dpy ( -- )
  dpy ?abort" X11 display already opened"
  0 XOpenDisplay to dpy
  dpy ifnot ." FATAL: cannot open X11 display!\n" 1 n-bye endif
  false " WM_PROTOCOLS" drop dpy XInternAtom to WM_PROTOCOLS
  false " WM_DELETE_WINDOW" drop dpy XInternAtom XA_WM_DELETE_WINDOW !
  false " URFORTH_XOF_INTERNAL_CLOSE" drop dpy XInternAtom to XOF_INTERNAL_CLOSE
  init-default-fontset
;


: close-dpy  ( -- )
  dpy not-?abort" X11 display not opened"
  deinit-default-fontset
  dpy XCloseDisplay drop
  0 to dpy
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 value (winlist-tail)

0 oop:class
  \ class-var x11-class  ;; name of X11 class for this window (asciiz)
  \ class-method class-init
  ;; internal list
  var (prev-win)
  class-method (dispatch-event)  ( event -- dispatch-success-flag )
  method (register)  ( -- )
  method (unregister)  ( -- )
  method (send-close-event)  ( -- )
  method (send-expose-event)  ( -- )
  ;;
  var win    ;; X11 window handle
  var wingc  ;; default GC for this window
  var min-width
  var min-height
  var max-width   ;; <=0: unlimited
  var max-height  ;; <=0: unlimited
  var motion-events?  ;; set on window creation; default is false
  var visible?    ;; initial and current; initial is hidden
  var invalidate-sent?
  ;; initial window position
  var x
  var y
  ;; initial window size
  var width
  var height
  ;; methods
  method init  ;; set initial values
  method (init-gc)
  method (deinit-gc)
  method (set-xhints)

  method KeyPress-Handler  ( event -- )
  method KeyRelease-Handler  ( event -- )
  method ButtonPress-Handler  ( event -- )
  method ButtonRelease-Handler  ( event -- )
  method MotionNotify-Handler  ( event -- )
  method EnterNotify-Handler  ( event -- )
  method LeaveNotify-Handler  ( event -- )
  method FocusIn-Handler  ( event -- )
  method FocusOut-Handler  ( event -- )
  method KeymapNotify-Handler  ( event -- )
  method Expose-Handler  ( event -- )
  method GraphicsExpose-Handler  ( event -- )
  method NoExpose-Handler  ( event -- )
  method VisibilityNotify-Handler  ( event -- )
  method CreateNotify-Handler  ( event -- )
  method DestroyNotify-Handler  ( event -- )
  method UnmapNotify-Handler  ( event -- )
  method MapNotify-Handler  ( event -- )
  method MapRequest-Handler  ( event -- )
  method ReparentNotify-Handler  ( event -- )
  method ConfigureNotify-Handler  ( event -- )
  method ConfigureRequest-Handler  ( event -- )
  method GravityNotify-Handler  ( event -- )
  method ResizeRequest-Handler  ( event -- )
  method CirculateNotify-Handler  ( event -- )
  method CirculateRequest-Handler  ( event -- )
  method PropertyNotify-Handler  ( event -- )
  method SelectionClear-Handler  ( event -- )
  method SelectionRequest-Handler  ( event -- )
  method SelectionNotify-Handler  ( event -- )
  method ColormapNotify-Handler  ( event -- )
  method ClientMessage-Handler  ( event -- )
  method MappingNotify-Handler  ( event -- )
  method GenericEvent-Handler  ( event -- )
  method UnknownEvent-Handler  ( event -- )
  method (process-event)  ( event -- )

  method (get-keysym)  ( event -- keysym )
  method (destroy-cleanup)  ( -- )  ;; win is zero here (i.e. the window is already destroyed)
  ;;
  method is-valid?  ( -- flag )
  method create  ;; create window
  method flush
  method show
  method hide
  method close
  method invalidate
  method set-min-size  ( -- minwidth minheight )
  method set-max-size  ( -- maxwidth maxheight )
  ;; getters to override
  method (get-class)  ( -- addr count )
  method (get-title)  ( -- addr count )
  ;; event handlers
  method on-created  ( -- )
  method on-destroyed  ( -- )  ;; win is zero here
  method on-visibility ( visible-flag -- )
  method on-show   ( -- )
  method on-hide   ( -- )
  method on-draw-part   ( x y width height count -- )
  method on-draw   ( -- )
  method on-resize ( oldwidth oldheight -- )
  method on-keydown  ( event keysym -- )
  method on-keyup  ( event keysym -- )

  method on-close-query  ( -- allow-close-flag )

  ;; simple drawing
  method set-color  ( color -- )
  method set-named-color  ( addr count -- )
  method set-bg-color  ( color -- )
  method set-named-bg-color  ( addr count -- )

  method draw-point  ( x y -- )
  method draw-line  ( x0 y0 x1 y1 -- )
  method fill-rect  ( x y w h -- )
  method draw-rect  ( x y w h -- )
  method draw-ellipse  ( x0 y0 w h -- )
  method fill-ellipse  ( x0 y0 w h -- )
  method fill-text  ( addr count x y fsetobj -- )  ;; with background
  method draw-text  ( addr count x y fsetobj -- )

  method draw-rounded-rect  ( x y w h ew eh -- )
  method fill-rounded-rect  ( x y w h ew eh -- )
end-class: BaseWindow


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
\ BaseWindow method: class-init  ( -- )
\   " UrForth BaseWindow" drop x11-class !
\ ;
\ BaseWindow ::invoke BaseWindow class-init

BaseWindow method: (register)  ( -- )
  (winlist-tail) (prev-win) ! self to (winlist-tail)
    \ ." registered window " win @ . ." self=0x" self .hex8 ."  winlist-tail=0x" (winlist-tail) .hex8 cr
;

BaseWindow method: (unregister)  ( -- )
  (winlist-tail) self = if
    (prev-win) @ to (winlist-tail)
  else
    ;; find next window
    (winlist-tail) begin dup var^ (prev-win) @ dup while dup self = not-while nip repeat
    not-?abort" cannot find previous window"
    (prev-win) @ swap var^ (prev-win) !
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: (dispatch-event)  ( event -- dispatch-success-flag )
  dup XAnyEvent display @ dpy = ifnot drop false exit endif
  dup XAnyEvent window @ (winlist-tail)
  begin dup while 2dup var^ win @ = not-while var^ (prev-win) @ repeat
  nip dup if invoke (process-event) true
  else 2drop false endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: (send-close-event)  ( -- )
  win @ if
    XEvent @sizeof ralloca >r
    r@ XEvent @sizeof erase
    ClientMessage r@ XClientMessageEvent type !
    r@ XClientMessageEvent send_event 1!
    win @ r@ XClientMessageEvent window !
    XOF_INTERNAL_CLOSE r@ XClientMessageEvent message_type !
    32 r@ XClientMessageEvent format !
    r@ 0 true win @ dpy XSendEvent drop
    rdrop XEvent @sizeof rdealloca
  endif
;


BaseWindow method: (send-expose-event)  ( -- )
  win @ if
    XEvent @sizeof ralloca >r
    r@ XEvent @sizeof erase
    Expose r@ XExposeEvent type !
    r@ XExposeEvent send_event 1!
    win @ r@ XExposeEvent window !
    r@ XExposeEvent x 0!
    r@ XExposeEvent y 0!
    width @ r@ XExposeEvent width !
    height @ r@ XExposeEvent height !
    r@ 0 true win @ dpy XSendEvent drop
    rdrop XEvent @sizeof rdealloca
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: init  ( -- )
  (prev-win) 0!
  win 0! wingc 0!
  motion-events? 0!
  min-width 0! min-height 0!
  max-width 0! max-height 0!
  x 0! y 0!
  512 width ! 256 height !
  visible? 0!
  invalidate-sent? 0!
;

BaseWindow method: invalidate  ( -- )
  invalidate-sent? @ ifnot
    win @ if (send-expose-event) invalidate-sent? 1! endif
  endif
;


BaseWindow method: is-valid?  ( -- flag )
  dpy if win @ notnot else false endif
;


BaseWindow method: create  ( -- )
  win @ ?abort" x11 window already created"
  0 0  ;; bg border
  0    ;; bwidth
  height @ 1 max width @ 1 max y @ x @  ;; h w y x
  dpy XDefaultRootWindow dpy  ;; parent dpy
  XCreateSimpleWindow dup win ! not-?abort" cannot create X11 window"
  (register)
  1 XA_WM_DELETE_WINDOW win @ dpy XSetWMProtocols drop
  ;; store title
  (get-title) ensure-asciiz >r win @ dpy XStoreName r> free-asciiz
  ;; set input mask
  [ NoEventMask
    KeyPressMask or
    KeyReleaseMask or
    ButtonPressMask or
    ButtonReleaseMask or
    \ EnterWindowMask or
    \ LeaveWindowMask or
    \ PointerMotionMask or
    \ PointerMotionHintMask or
    \ Button1MotionMask or
    \ Button2MotionMask or
    \ Button3MotionMask or
    \ Button4MotionMask or
    \ Button5MotionMask or
    \ ButtonMotionMask or
    \ KeymapStateMask or
    ExposureMask or
    VisibilityChangeMask or
    StructureNotifyMask or
    \ ResizeRedirectMask or
    SubstructureNotifyMask or
    \ SubstructureRedirectMask or
    FocusChangeMask or
    PropertyChangeMask or
    \ ColormapChangeMask or
    \ OwnerGrabButtonMask or
  ] literal motion-events? @ if PointerMotionMask or endif
  win @ dpy XSelectInput
  (set-xhints)
  (init-gc)
;

BaseWindow method: (set-xhints)  ( -- )
  (get-class) ensure-asciiz >r
  ;; class hints
  XClassHint @sizeof ralloca >r
  r@ XClassHint @sizeof erase
  dup r@ XClassHint res_name ! r@ XClassHint res_class !
  r@  ;; class hints address
  ;; WM hints
  XWMHints @sizeof ralloca >r
  r@ XWMHints @sizeof erase
  InputHint r@ XWMHints flags !
  true r@ XWMHints input !
  r@  ;; WM hints address
  ;; size hints
  XSizeHints @sizeof ralloca >r
  r@ XSizeHints @sizeof erase
  min-width @ 0 max min-height @ 0 max or if
    PMinSize r@ XSizeHints flags or!
    min-width @ r@ XSizeHints min_width !
    min-height @ r@ XSizeHints min_height !
  endif
  max-width @ 0 max max-height @ 0 max or if
    PMaxSize r@ XSizeHints flags or!
    max-width @ r@ XSizeHints max_width !
    max-height @ r@ XSizeHints max_height !
  endif
  r@  ;; size hints address
  0 0 0 0  ;; argc, argv, iconname windowname
  win @ dpy XSetWMProperties drop
  ;; free structures
  rdrop XSizeHints @sizeof rdealloca
  rdrop XWMHints @sizeof rdealloca
  rdrop XClassHint @sizeof rdealloca
  r> free-asciiz
;

BaseWindow method: flush  ( -- )
  win @ if dpy XFlush endif
;


BaseWindow method: (get-keysym)  ( event -- keysym )
  0 swap XKeyEvent keycode @ dpy XKeycodeToKeysym
;

BaseWindow method: (destroy-cleanup)  ( -- )  ;; win is zero here (i.e. the window is already destroyed)
  (deinit-gc) (unregister) on-destroyed
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: KeyPress-Handler  ( event -- )  dup (get-keysym) on-keydown ;
BaseWindow method: KeyRelease-Handler  ( event -- )  dup (get-keysym) on-keyup ;
BaseWindow method: ButtonPress-Handler  ( event -- )  drop ;
BaseWindow method: ButtonRelease-Handler  ( event -- )  drop ;
BaseWindow method: MotionNotify-Handler  ( event -- )  drop ;
BaseWindow method: EnterNotify-Handler  ( event -- )  drop ;
BaseWindow method: LeaveNotify-Handler  ( event -- )  drop ;
BaseWindow method: FocusIn-Handler  ( event -- )  drop ;
BaseWindow method: FocusOut-Handler  ( event -- )  drop ;
BaseWindow method: KeymapNotify-Handler  ( event -- )  drop ;
BaseWindow method: Expose-Handler  ( event -- )
  invalidate-sent? 0!
  dup >r XExposeEvent x @ r@ XExposeEvent y @
  r@ XExposeEvent width @ r@ XExposeEvent height @
  r> XExposeEvent count @
  on-draw-part
;
BaseWindow method: GraphicsExpose-Handler  ( event -- )  drop ;
BaseWindow method: NoExpose-Handler  ( event -- )  drop ;
BaseWindow method: VisibilityNotify-Handler  ( event -- )  XVisibilityEvent state @ VisibilityFullyObscured <> on-visibility ;
BaseWindow method: CreateNotify-Handler  ( event -- )  drop (register) on-created ;
BaseWindow method: DestroyNotify-Handler  ( event -- )  drop 0 win ! (destroy-cleanup) ;
BaseWindow method: UnmapNotify-Handler  ( event -- )  drop visible? 0! on-hide ;
BaseWindow method: MapNotify-Handler  ( event -- )  drop visible? 1! on-show ;
BaseWindow method: MapRequest-Handler  ( event -- )  drop ;
BaseWindow method: ReparentNotify-Handler  ( event -- )  drop ;
BaseWindow method: ConfigureNotify-Handler  ( event -- )
  dup XConfigureEvent width @
  swap XConfigureEvent height @
  2dup height @ = swap width @ = and ifnot
    width @ height @  ;; old size
    2swap height ! width !  ;; set new size
    on-resize
    (send-expose-event)
  else 2drop
  endif
;
BaseWindow method: ConfigureRequest-Handler  ( event -- )  drop ;
BaseWindow method: GravityNotify-Handler  ( event -- )  drop ;
BaseWindow method: ResizeRequest-Handler  ( event -- )  drop ;
BaseWindow method: CirculateNotify-Handler  ( event -- )  drop ;
BaseWindow method: CirculateRequest-Handler  ( event -- )  drop ;
BaseWindow method: PropertyNotify-Handler  ( event -- )  drop ;
BaseWindow method: SelectionClear-Handler  ( event -- )  drop ;
BaseWindow method: SelectionRequest-Handler  ( event -- )  drop ;
BaseWindow method: SelectionNotify-Handler  ( event -- )  drop ;
BaseWindow method: ColormapNotify-Handler  ( event -- )  drop ;
BaseWindow method: ClientMessage-Handler  ( event -- )
  dup XClientMessageEvent message_type @ case
    WM_PROTOCOLS of
      XClientMessageEvent data @  XA_WM_DELETE_WINDOW @ = if
        on-close-query if ( win @ dpy XDestroyWindow drop) (send-close-event) endif
      endif
    endof
    XOF_INTERNAL_CLOSE of
      drop win @ dpy XDestroyWindow drop 0 win ! (destroy-cleanup)
    endof
    drop
  endcase
;
BaseWindow method: MappingNotify-Handler  ( event -- )  drop ;
BaseWindow method: GenericEvent-Handler  ( event -- )  drop ;
BaseWindow method: UnknownEvent-Handler  ( event -- )  drop ;

BaseWindow method: (process-event)  ( event -- )
    \ ." EVENT: type=" dup @ . dup @ get-x11-event-type-name type cr
  dup @ get-x11-event-handler-name dispatch-str
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: (init-gc)  ( -- )
  0 0 win @ dpy XCreateGC wingc ! drop  ;; "drop" due to "..."
  JoinMiter CapButt LineSolid 0 wingc @ dpy XSetLineAttributes drop
;

BaseWindow method: (deinit-gc)  ( -- )
  wingc @ ?dup if dpy XFreeGC wingc 0! endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: show  ( -- )
  win @ not-?abort" x11 window must be created"
  win @ dpy XMapWindow
;

BaseWindow method: hide  ( -- )
  win @ not-?abort" x11 window must be created"
  win @ dpy XUnmapWindow
;

BaseWindow method: close  ( -- )
  \ win @ ?dup if dpy XDestroyWindow drop  0 win ! endif
  (send-close-event)
;

BaseWindow method: set-min-size  ( -- minwidth minheight )
  0 max min-height !
  0 max min-width !
  win @ if (set-xhints) endif
;

BaseWindow method: set-max-size  ( -- maxwidth maxheight )
  0 max max-height !
  0 max max-width !
  win @ if (set-xhints) endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: (get-class)  ( -- addr count )
  " UrForth Window"
;

BaseWindow method: (get-title)  ( -- addr count )
  " UrForth BaseWindow"
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: on-created  ( -- )  ;
BaseWindow method: on-destroyed  ( -- )  ;
BaseWindow method: on-visibility  ( visible-flag -- )  drop ;
BaseWindow method: on-show  ( -- )  ;
BaseWindow method: on-hide  ( -- )  ;
BaseWindow method: on-draw-part  ( x y width height count -- )  drop 2drop 2drop ;
BaseWindow method: on-draw  ( -- )  0 0 width @ height @ 0 on-draw ;
BaseWindow method: on-resize ( oldwidth oldheight -- )  2drop ;
BaseWindow method: on-keydown  ( event keysym -- )  2drop ;
BaseWindow method: on-keyup  ( event keysym -- )  2drop ;
BaseWindow method: on-close-query  ( -- allow-close-flag )  true ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: set-color  ( color -- )
  wingc @ if wingc @ dpy XSetForeground else drop endif
;

BaseWindow method: set-named-color  ( addr count -- )
  wingc @ if x11-get-color set-color else 2drop endif
;

BaseWindow method: set-bg-color  ( color -- )
  wingc @ if wingc @ dpy XSetBackground else drop endif
;

BaseWindow method: set-named-bg-color  ( addr count -- )
  wingc @ if x11-get-color set-bg-color else 2drop endif
;


BaseWindow method: draw-point  ( x y -- )
  wingc @ if swap wingc @ win @ dpy XDrawPoint else 2drop endif
;

BaseWindow method: draw-line  ( x0 y0 x1 y1 -- )
  wingc @ if swap 2swap swap wingc @ win @ dpy XDrawLine else 2drop 2drop endif
;

BaseWindow method: fill-rect  ( x y w h -- )
  wingc @ if swap 2swap swap wingc @ win @ dpy XFillRectangle else 2drop 2drop endif
;

BaseWindow method: draw-rect  ( x y w h -- )
  wingc @ if swap 2swap swap wingc @ win @ dpy XDrawRectangle else 2drop 2drop endif
;

BaseWindow method: draw-ellipse  ( x0 y0 w h -- )
  wingc @ if swap 2swap swap 23040 0 2nrot wingc @ win @ dpy XDrawArc else 2drop 2drop endif
;

BaseWindow method: fill-ellipse  ( x0 y0 w h -- )
  wingc @ if swap 2swap swap 23040 0 2nrot wingc @ win @ dpy XFillArc else 2drop 2drop endif
;

BaseWindow method: fill-text  ( addr count x y fsetobj -- )  ;; with background
  dup if >r
    r@ ::invoke BaseFont get-fset if
      r@ ::invoke BaseFont get-ascent +  ;; to draw from (x, y)
      2swap swap 2swap swap wingc @ r@ ::invoke BaseFont get-fset win @ dpy XmbDrawImageString rdrop
    else rdrop 2drop 2drop endif
  else drop 2drop 2drop endif
;

BaseWindow method: draw-text  ( addr count x y fsetobj -- )  ;; with background
  dup if >r
    r@ ::invoke BaseFont get-fset if
      r@ ::invoke BaseFont get-ascent +  ;; to draw from (x, y)
      2swap swap 2swap swap wingc @ r@ ::invoke BaseFont get-fset win @ dpy XmbDrawString rdrop
    else rdrop 2drop 2drop endif
  else drop 2drop 2drop endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
BaseWindow method: draw-rounded-rect  {{ x y w h ew eh | arcs curarc ew2 eh2 -- }}
  XArc @sizeof 8 * ralloca dup to arcs to curarc

  ew 0 max to ew
  eh 0 max to eh

  ew 1 lshift dup to ew2 w > if 0 dup to ew2 to ew endif
  eh 1 lshift dup to eh2 h > if 0 dup to eh2 to eh endif

  ;; [0]
  x curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 180 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [1]
  x ew + curarc XArc x w!
  y curarc XArc y w!
  w ew2 - curarc XArc width w!
  0 curarc XArc height w!
  [ 180 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [2]
  x w + ew2 - curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 90 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [3]
  x w + curarc XArc x w!
  y eh + curarc XArc y w!
  0 curarc XArc width w!
  h eh2 - curarc XArc height w!
  [ 90 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [4]
  x w + ew2 - curarc XArc x w!
  y h + eh2 - curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 0 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [5]
  x ew + curarc XArc x w!
  y h + curarc XArc y w!
  w ew2 - curarc XArc width w!
  0 curarc XArc height w!
  [ 0 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [6]
  x curarc XArc x w!
  y h + eh2 - curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 270 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [7]
  x curarc XArc x w!
  y eh + curarc XArc y w!
  0 curarc XArc width w!
  h eh2 - curarc XArc height w!
  [ 270 64 * ] literal curarc XArc angle1 w!
  [ -180 64 * ] literal curarc XArc angle2 w!

  8 arcs wingc @ win @ dpy XDrawArcs

  XArc @sizeof 8 * rdealloca
;


BaseWindow method: fill-rounded-rect  {{ x y w h ew eh | arcs curarc rects currect gcvals ew2 eh2 -- }}
  XArc @sizeof 4 * ralloca dup to arcs to curarc
  XRectangle @sizeof 3 * ralloca dup to rects to currect
  XGCValues @sizeof ralloca to gcvals

  gcvals GCArcMode wingc @ dpy XGetGCValues drop
  gcvals XGCValues arc_mode @ ArcPieSlice <> if
    ArcPieSlice wingc @ dpy XSetArcMode drop
  endif

  ew 0 max to ew
  eh 0 max to eh

  ew 1 lshift dup to ew2 w > if 0 dup to ew2 to ew endif
  eh 1 lshift dup to eh2 h > if 0 dup to eh2 to eh endif

  ;; [0]
  x curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 180 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [1]
  x w + ew2 - 1- curarc XArc x w!
  y curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 90 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [2]
  x w + ew2 - 1- curarc XArc x w!
  y h + eh2 - 1- curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 0 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!
  curarc XArc @sizeof + to curarc

  ;; [3]
  x curarc XArc x w!
  y h + eh2 - 1- curarc XArc y w!
  ew2 curarc XArc width w!
  eh2 curarc XArc height w!
  [ 270 64 * ] literal curarc XArc angle1 w!
  [ -90 64 * ] literal curarc XArc angle2 w!

  4 arcs wingc @ win @ dpy XFillArcs

  ;; [0]
  x ew + currect XRectangle x w!
  y currect XRectangle y w!
  w ew2 - currect XRectangle width w!
  h currect XRectangle height w!
  currect XRectangle @sizeof + to currect

  ;; [1]
  x currect XRectangle x w!
  y eh + currect XRectangle y w!
  ew currect XRectangle width w!
  h eh2 - currect XRectangle height w!
  currect XRectangle @sizeof + to currect

  ;; [2]
  x w + ew - currect XRectangle x w!
  y eh + currect XRectangle y w!
  ew currect XRectangle width w!
  h eh2 - currect XRectangle height w!

  3 rects wingc @ win @ dpy XFillRectangles

  gcvals XGCValues arc_mode @ ArcPieSlice <> if
    gcvals XGCValues arc_mode @ wingc @ dpy XSetArcMode drop
  endif

  XGCValues @sizeof rdealloca
  XRectangle @sizeof 3 * rdealloca
  XArc @sizeof 4 * rdealloca
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous previous
