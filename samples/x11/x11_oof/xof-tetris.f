" tetris-base.f" tload
" xof-window.f" tload

also x11 also xconst


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
24 constant tile-size


BaseWindow oop:class
  var field-x
  var field-y
  var seed-lo
  var seed-hi
  var next-figure
  var fig-x
  var fig-y
  var game-over?
  var paused?
  var speed
  var score
  var next-down-time
  var (rndbit)
  var (rndbit-left)

  method rndbit  ( -- value ) ;; [0..1]
  method rnd8  ( -- value ) ;; [0..7]
  method rndu32  ( -- value )

  method convert-color  ( clr -- clr )
  method set-figure

  method start-game
  method gen-next-figure
  method set-next-figure

  method calc-next-down-time
  method get-next-down-left

  method (can-rotate-figure?)  ( cwflag -- xofs true // false )
  method (rotate-figure)  ( drawflag cwflag -- successflag )
  method rotate-figure-cw  ( -- )
  method rotate-figure-ccw  ( -- )

  method hmove-figure  ( delta -- )

  method draw-blank-tile  ( xpix ypix -- )
  method draw-tile  ( xpix ypix color -- )
  method draw-figure-at  ( xtile ytile -- )
  method erase-figure-at  ( xtile ytile -- )
  method draw-field-borders  ( -- )
  method draw-field  ( -- )
  method draw-next-figure  ( -- )

  method draw-current-figure  ( -- )
  method erase-current-figure  ( -- )

  method (down-figure)  ( dodraw -- successflag )
  method put-figure  ( -- )  ;; put current figure at current coords, compress field, generate new figure, set gameover
  method down-figure  ( -- )
  method drop-figure  ( -- )

  method draw-gameover  ( -- )
  method draw-status  ( -- )

  method is-playing?  ( -- flag )
  method is-paused?  ( -- flag )

  method set-game-over  ( -- )
end-class: MyWindow


MyWindow method: (get-class)  ( -- addr count )  " UrForth Tetris" ;
MyWindow method: (get-title)  ( -- addr count )  " UrForth Tetris" ;

MyWindow method: is-playing?  ( -- flag )  game-over? @ not ;
MyWindow method: is-paused?  ( -- flag )  paused? @ ;
MyWindow method: set-game-over  ( -- )  game-over? 1! ;

MyWindow method: init  ( -- )
  call-super
  tetris:field-width 2+ 6 + tile-size * width !
  tetris:field-height 2+ tile-size * height !
  width @ height @ 2dup set-min-size set-max-size

  tile-size dup field-x ! field-y !
  game-over? 1!

  prng:gen-dseed seed-hi ! seed-lo !
  (rndbit-left) 0!
;

MyWindow method: rndu32  ( -- value )
  seed-lo @ seed-hi @ prng:pcg32-next nrot seed-hi ! seed-lo !
;

MyWindow method: rndbit  ( -- value ) ;; [0..1]
  (rndbit-left) @ ifnot rndu32 (rndbit) ! 32 (rndbit-left) ! endif
  (rndbit) @ dup 1 rshift (rndbit) ! (rndbit-left) 1-!  1 and
;

MyWindow method: rnd8  ( -- value )
  rndbit rndbit 1 lshift or rndbit 2 lshift or
;


MyWindow method: convert-color  ( clr -- clr )
  base @ >r hex <#u # # # # # # [char] # hold #> r> base ! x11-get-color
;


MyWindow method: set-figure  ( fig -- )
  tetris:set-figure
  tetris:current-color convert-color to tetris:current-color
;


MyWindow method: gen-next-figure  ( -- )
  0 begin drop rnd8 dup tetris:#figures < until
  next-figure !
;


MyWindow method: calc-next-down-time  ( -- )
  speed @ 0 9 clamp 100 * 50 max os:GetTickCount + next-down-time !
;

MyWindow method: get-next-down-left  ( -- msecs )
  next-down-time @ os:GetTickCount 2dup > if - else 2drop 0 endif
;


MyWindow method: start-game  ( -- )
  tetris:field-clear
  game-over? 0!
  paused? 0!
  gen-next-figure
  set-next-figure
  9 speed !
  score 0!
  calc-next-down-time
;

MyWindow method: set-next-figure  ( -- )
  next-figure @ set-figure
  gen-next-figure
  tetris:field-width 2/ tetris:current-width 2/ - fig-x ! fig-y 0!
  draw-next-figure
;

MyWindow method: (can-rotate-figure?)  ( cwflag -- xofs true // false )
  tetris:save-figure
  if tetris:rotate-figure-cw else tetris:rotate-figure-ccw endif
  dup if drop -1  ;; ( offset )
    tetris:current-width for
      fig-x @ i - fig-y @ tetris:field-free-at? if drop i break endif
    endfor
    dup -if drop false else negate true endif
  endif
  tetris:restore-figure
;

MyWindow method: (rotate-figure)  ( cwflag -- )
  dup (can-rotate-figure?) if  ;; ( cwflag xofs )
    erase-current-figure
    fig-x +! if tetris:rotate-figure-cw else tetris:rotate-figure-ccw endif drop
    draw-current-figure ( flush)
  else drop endif
;

MyWindow method: rotate-figure-cw  ( -- )  true (rotate-figure) ;
MyWindow method: rotate-figure-ccw  ( -- )  false (rotate-figure) ;


MyWindow method: (down-figure)  ( -- successflag )
  fig-x @ fig-y @ 1+ tetris:field-free-at? dup if
    erase-current-figure fig-y 1+! draw-current-figure ( flush)
  endif
;

;; put current figure at current coords, compress field, generate new figure, set gameover
MyWindow method: put-figure  ( -- )
  score @  ;; to compare
  10 score +!  ;; always
  fig-x @ fig-y @ tetris:current-color tetris:field-put-at
  tetris:field-compress  \ dup . cr
  dup 100 * score +!  ;; cleared lines
  ;; if can't compress, redraw current figure (just in case), otherwise redraw the whole field
  dup ifnot draw-current-figure endif
  set-next-figure
  fig-x @ fig-y @ tetris:field-free-at? not game-over? !
  if draw-field else game-over? @ ifnot draw-current-figure endif endif
  ;; advance speed
  2500 / score @ 2500 / swap - speed @ swap - 0 max speed !
  draw-status
  ( flush)
  calc-next-down-time
;

MyWindow method: down-figure  ( -- )
  (down-figure) ifnot put-figure endif
  calc-next-down-time
;


MyWindow method: drop-figure  ( -- )
  fig-y @ dup
  begin fig-x @ over 1+ tetris:field-free-at? while 1+ repeat
  2dup = if  ;; cannot drop
    2drop put-figure
  else
    erase-current-figure fig-y ! drop draw-current-figure ( flush)
    ;; check if we can skip drop pause
    fig-x @ 1- fig-y @ tetris:field-free-at? if calc-next-down-time  \ ." can-left\n"
    else fig-x @ 1+ fig-y @ tetris:field-free-at? if calc-next-down-time  \ ." can-right\n"
    else true (can-rotate-figure?) if drop  calc-next-down-time  \ ." can-rotate\n"
    else put-figure
    endif endif endif
  endif
;


MyWindow method: draw-blank-tile  ( xtile ytile -- )
  0 set-color
  tile-size * field-y @ +
  swap tile-size * field-x @ + swap
  tile-size dup fill-rect
;

MyWindow method: draw-tile  ( xtile ytile color -- )
  dup set-color nrot
  tile-size * field-y @ +
  swap tile-size * field-x @ + swap
  2dup tile-size dup fill-rect
  rot ifnot " #444444" else " #000000" endif set-named-color
  tile-size 2/ dup >r + swap r> + swap 3 3 fill-rect
;


MyWindow method: draw-figure-at  ( xtile ytile -- )
  tetris:current-figure
  4 for 4 for
      \ dup c@ 0 .r
    dup c@ if nrot 2dup j + swap i + swap tetris:current-color draw-tile rot endif
  1+ endfor endfor drop 2drop
;

MyWindow method: erase-figure-at  ( xtile ytile -- )
  tetris:current-color nrot  0 to tetris:current-color  draw-figure-at  to tetris:current-color
;

MyWindow method: draw-current-figure  ( -- )  fig-x @ fig-y @ draw-figure-at ;
MyWindow method: erase-current-figure  ( -- )  fig-x @ fig-y @ erase-figure-at ;


MyWindow method: draw-next-figure  ( -- )
  game-over? @ ifnot
    tetris:save-figure
    next-figure @ tetris:set-figure
    tetris:field-width 4+ tetris:current-width 2/ -  1 ;; figure start x and y
    4 for 4 for
      2dup i j tetris:figure@ if tetris:current-color draw-tile else draw-blank-tile endif
    swap 1+ swap endfor 1+ swap 4- swap endfor 2drop
    tetris:restore-figure
  endif
;


MyWindow method: draw-field-borders  ( -- )
  " #4000ff" set-named-color
  field-x @ tile-size -  field-y @  tile-size  tile-size tetris:field-height 1+ *  fill-rect  ;; left
  field-x @ tetris:field-width tile-size * +  field-y @  tile-size  tile-size tetris:field-height 1+ *  fill-rect  ;; right
  field-x @  field-y @ tetris:field-height tile-size * +  tetris:field-width tile-size *  tile-size  fill-rect ;; bottom
;

MyWindow method: draw-field  ( -- )
  draw-field-borders
  tetris:field-height for
    tetris:field-width for
      i j 2dup tetris:field-at@ draw-tile
    endfor
  endfor
  game-over? @ ifnot draw-current-figure endif
;


MyWindow method: hmove-figure  ( delta -- )
  fig-x @ over + fig-y @ tetris:field-free-at? if
    erase-current-figure fig-x +! draw-current-figure ( flush)
  else drop endif
;


MyWindow method: on-created  ( -- )  ." MyWindow created\n"  ;
MyWindow method: on-destroyed  ( -- )  ." MyWindow destroyed\n" ;
MyWindow method: on-show   ( -- )  ." MyWindow is visible\n" ;
MyWindow method: on-hide   ( -- )  ." MyWindow is invisible\n" ;

MyWindow method: on-draw-part   ( x y width height count -- )
  >r 2drop 2drop r> ifnot
    0 set-color 0 0 width @ height @ fill-rect
    draw-field draw-next-figure
    draw-status
    ( flush)
  endif
;

MyWindow method: on-resize ( oldwidth oldheight -- )
  ." resizing from " swap 0 .r ." x" . ." to " width @ 0 .r ." x" height @ . cr
;


MyWindow method: on-keydown  ( event keysym -- )
  nip dup case
    XK_KP_Left of drop XK_Left endof
    XK_KP_Right of drop XK_Right endof
    XK_KP_Up of drop XK_Up endof
    XK_KP_Down of drop XK_Down endof
  endcase
  game-over? @ if
    case
      XK_Escape of close endof
      [char] q of close endof
      XK_Return of start-game invalidate endof
    endcase
  else
    case
      XK_Escape of 1 paused? xor! invalidate endof
      bl of drop-figure endof
      XK_Left of -1 hmove-figure endof
      XK_Right of 1 hmove-figure endof
      XK_Up of rotate-figure-cw endof
      XK_Down of down-figure endof
      [char] q of paused? @ if game-over? 1! paused? 0! endif invalidate endof
    endcase
  endif
;

MyWindow method: on-keyup  ( event keysym -- )  2drop ;

MyWindow method: on-close-query  ( -- allow-close-flag )  true ." closing MyWindow\n" ;


: (#-spaced)  ( ud count -- ud )
  begin >r # r> 1- dup +while >r 2dup or r> swap not-until
  for bl hold endfor 2dup or if #s endif
;

MyWindow method: draw-status  ( -- )
  0 set-color
  0 0 width @ default-fset get-height fill-rect
  " #ff7f00" set-named-color
  \ score @ <#u # # # # # # # # " SCORE: " holds #> 16 0 default-fset self draw-text
  16 >r
  score @ <#u 8 (#-spaced) " SCORE: " holds #> 2dup r@ 0 default-fset self draw-text
  default-fset text-log-width 130 max r> + >r
  speed @ <#u # " SPEED: " holds #> 2dup r@ 0 default-fset self draw-text
  default-fset text-log-width 100 max r> + >r
  paused? @ if " #00ff00" set-named-color " PAUSED!" r@ 0 default-fset self draw-text endif
  rdrop
;


MyWindow method: draw-gameover  ( -- )
  (*
  0 set-color
  0 0 width @ default-fset get-height fill-rect
  " #ffff00" set-named-color
  " GAME OVER!" 16 0 default-fset self draw-text
  *)
  draw-status
;


MyWindow oop:new-allot oop:value win MyWindow


create event XEvent @sizeof allot
0 var event-count

: handle-event  ( -- )
  event dpy XNextEvent event-count 1+!
  event BaseWindow ::invoke BaseWindow (dispatch-event) ifnot
    ." cannot dispatch event " event @ . ." wid=" event XAnyEvent window @ . cr
  endif
  (sp-check) ifnot err-stack-underflow error 1 n-bye endif
;


: event-loop  ( -- )
  begin
    begin dpy XPending while handle-event repeat
    win is-playing? if
      win get-next-down-left ?dup if
        \ dpy XFlush  ;; send pending events
        os:#pollfd ralloca >r
        dpy XConnectionNumber r@ os:pollfd.fd !
        os:poll-in r@ os:pollfd.events !  ;; this also clears revents
        r@ 1 rot os:poll
        rdrop os:#pollfd rdealloca
        0> if handle-event endif
      else
        win is-paused? ifnot
          win down-figure
        else handle-event
        endif
      endif
    else
      win is-valid? if
        win draw-gameover
      endif
      handle-event
    endif
    begin dpy XPending while handle-event repeat
  (winlist-tail) not-until
;

: run-main  ( -- )
  open-dpy
  win init
  win create
  2 win set-figure
  win show
  \ win start-game
  event-loop
  close-dpy
  event-count @ . ." events processed\n"
;
\ " z00.elf" save not-?abort" cannot save!\n"

run-main

.stack
bye
