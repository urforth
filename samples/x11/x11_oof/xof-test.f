" xof-window.f" tload

also x11 also xconst


BaseWindow oop:class
end-class: MyWindow


MyWindow method: (get-title)  ( -- addr count )  " UrForth Window" ;

MyWindow method: init  ( -- )
  call-super
  \ 0 0 set-min-size
  128 64 set-min-size
  \ 0 0 set-max-size
  612 356 set-max-size
;

MyWindow method: on-created  ( -- )  ." MyWindow created\n"  ;
MyWindow method: on-destroyed  ( -- )  ." MyWindow destroyed\n" ;
MyWindow method: on-show   ( -- )  ." MyWindow is visible\n" ;
MyWindow method: on-hide   ( -- )  ." MyWindow is invisible\n" ;

MyWindow method: on-draw-part   ( x y width height count -- )
  >r ." EXPOSE! x=" 2swap swap . ." y=" . ." width=" swap . ." height=" . ." count=" r@ . cr
  ;; process only the very last expose event
  r> ifnot
    " #00ff00" set-named-color
    0 0 8192 8192 fill-rect

    width @ 0 .r ." x" height @ . cr
    " #0000ff" set-named-color
    0 0 width @ height @ fill-rect

    " #f0fff0" set-named-color
    0 0 width @ height @ draw-line
    width @ 0 0 height @ draw-line

    " #ff0000" set-named-color
    1 1 32 100 fill-rect

    " #ff0000" set-named-color
    width @ 120 - 2/ height @ 92 - 2/ 120 92 draw-ellipse

    " #00ffff" set-named-color
    width @ 100 - 2/ height @ 62 - 2/ 100 62 fill-ellipse

    " #ff7f00" set-named-color
    " HELLO!" 2dup 1 1 default-fset self fill-text
    2dup default-fset text-log-height 1+ 1 swap oop:value@ default-fset draw-text

    " #ffffff" set-named-color
    30 30 width @ 60 - height @ 60 - 4 8 draw-rounded-rect

    " #aaaaaa" set-named-color
    width @ 2/ 30 - 42 60 20 6 4 fill-rounded-rect
  endif
;

MyWindow method: on-resize ( oldwidth oldheight -- )
  ." resizing from " swap 0 .r ." x" . ." to " width @ 0 .r ." x" height @ . cr
;

MyWindow method: on-keydown  ( event keysym -- )
  nip case
    XK_Escape of close endof
    32 of (send-expose-event) endof
  endcase
;

MyWindow method: on-keyup  ( event keysym -- )  2drop ;

MyWindow method: on-close-query  ( -- allow-close-flag )  true ." closing MyWindow\n" ;



create event XEvent @sizeof allot
0 var event-count

: handle-event  ( flag -- )
  if dpy XPending else true endif
  if
    event dpy XNextEvent event-count 1+!
    event BaseWindow ::invoke BaseWindow (dispatch-event) ifnot
      ." cannot dispatch event " event @ . ." wid=" event XAnyEvent window @ . cr
    endif
    (sp-check) ifnot err-stack-underflow error 1 n-bye endif
  endif
;


: event-loop  ( -- )
  begin false handle-event (winlist-tail) not-until
;

\ MyWindow oop:new-allot oop:value win MyWindow
0 oop:value win MyWindow
MyWindow oop:new-allot oop:to win

MyWindow .hex8 cr
win class .hex8 cr
MyWindow ::invoke MyWindow class .hex8 cr

: cctest win class .hex8 cr ; cctest

.( parent: ) BaseWindow .hex8 cr
MyWindow ::invoke MyWindow parent-class .hex8 cr
win parent-class .hex8 cr
: pcctest win parent-class .hex8 cr ; pcctest

MyWindow BaseWindow oop:child-of? .stack not-?abort" oops" .stack
win child-of: BaseWindow .stack not-?abort" oops" .stack
: xctest win child-of: BaseWindow not-?abort" oops" ; xctest
: yctest BaseWindow win child-of? not-?abort" oops!" ; yctest


: dump-rect  ( x y width height -- )
  swap 2swap swap ." x=" . ." y=" . ." width=" . ." height=" 0 .r
;

: run-main  ( -- )
  open-dpy
  ." font: ascent=" default-fset get-ascent . ." descent=" default-fset get-descent . cr
  ." fsid: " default-fset get-fset . cr

  " FOO!" 2dup default-fset text-ink-extents dump-rect cr default-fset text-log-extents dump-rect cr

  win init
  win create
  win show
  \ 0 dpy XSync
  event-loop
  close-dpy
  event-count @ . ." events processed\n"
;
\ " z00.elf" save not-?abort" cannot save!\n"

run-main

.stack
bye
