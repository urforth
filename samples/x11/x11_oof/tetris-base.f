vocabulary tetris also tetris definitions

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(* figure:
  dd color
  db c00,c01,c02,c03
  db c00,c01,c02,c03
  db c00,c01,c02,c03
  db c00,c01,c02,c03
*)

7 constant #figures
#figures cells buffer: figures

: figure  ( index color -- index+1 )  \ figdef
  over cells here swap figures + !
  ,  ;; color
  4 for parse-name
    4 = not-?abort" invalid figure definition"
    4 for
      dup c@ case
        [char] # of 1 endof
        [char] . of 0 endof
        abort" invalid figure definition"
      endcase
      c,
    1+ endfor drop
  endfor
  1+
;

0
0x00f0f0 figure
#...
#...
#...
#...

0x0000f0 figure
#...
###.
....
....

0xf0a000 figure
..#.
###.
....
....

0xf0f000 figure
##..
##..
....
....

0x00f000 figure
.##.
##..
....
....

0xf00000 figure
##..
.##.
....
....

0xaf00f0 figure
.#..
###.
....
....

drop


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
0 value current-color
0 value current-width
4 4 * buffer: current-figure
4 4 * buffer: temp-figure


: row-width  ( y -- width )
  dup 0 4 within if
    4 * 3 + current-figure + 4 swap 4 for dup c@ if break endif swap 1- swap 1+ endfor drop
  else drop 0 endif
;

: calc-width  ( -- )
  0 4 for i row-width max endfor to current-width
;

: set-figure  ( fig -- )
  7 and #figures umod cells figures + @
  dup @ to current-color cell+
  current-figure [ 4 4 * ] literal cmove
  calc-width
;

: figure-ofs  ( x y -- ofs true // false )
  over 0 4 within over 0 4 within and if 4 * + true else 2drop false endif
;

: figure@  ( x y -- flag )  figure-ofs if current-figure + c@ else 0 endif ;

: temp-figure!  ( value x y -- )  figure-ofs if temp-figure + c! else drop endif ;

: temp-need-shift-left?  ( -- flag )
  true temp-figure 4 for dup c@ if swap drop false swap break endif 4+ endfor drop
;

: temp-shift-figure-left  ( -- )
  temp-figure 4 for dup dup 1+ swap 3 move dup 3 + 0c! 4+ endfor drop
;

: temp-need-shift-up?  ( -- flag )
  true temp-figure 4 for dup c@ if swap drop false swap break endif 1+ endfor drop
;

: temp-shift-figure-up  ( -- )
  temp-figure dup 4+ swap [ 4 3 * ] literal move
  temp-figure [ 4 3 * ] literal + 4 erase
;

: (rotated?)  ( -- flag )
  0 [ 4 4 * ] literal for current-figure i + c@ temp-figure i + c@ xor or endfor notnot
; (hidden)

: (set-from-temp)  ( -- changedflag )
  begin temp-need-shift-left? while temp-shift-figure-left repeat
  begin temp-need-shift-up? while temp-shift-figure-up repeat
  (rotated?)  temp-figure current-figure [ 4 4 * ] literal cmove  calc-width
; (hidden)

: rotate-figure-cw  ( -- rotatedflag )
  4 for 4 for j 3 i - figure@ i j temp-figure! endfor endfor
  (set-from-temp)
;

: rotate-figure-ccw  ( -- rotatedflag )
  4 for 4 for 3 j - i figure@ i j temp-figure! endfor endfor
  (set-from-temp)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
4 4 * buffer: saved-figure
0 value saved-color

: save-figure  ( -- )
  current-color to saved-color
  current-figure saved-figure [ 4 4 * ] literal cmove
;

: restore-figure  ( -- )
  saved-color to current-color
  saved-figure current-figure [ 4 4 * ] literal cmove
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
10 constant field-width
20 constant field-height

field-width field-height * cells buffer: field

: field-clear  ( -- )  field [ field-width field-height * cells ] literal erase ;

: field-addr  ( x y -- addr true // false )
  over 0 field-width within over 0 field-height within and if
    field-width * + cells field + true
  else 2drop false endif
;

: field-at@  ( x y -- color )
  field-addr if @ else -1 endif
;

: field-at!  ( color x y -- )
  field-addr if ! else drop endif
;

: field-free-at?  ( x y -- canput-flag )
  4 for 4 for
    i j figure@ if
      over i + over j + field-at@ if 2drop unloop unloop false exit endif
    endif
  endfor endfor 2drop true
;

: field-put-at  ( x y color -- )
  4 for 4 for
    i j figure@ if >r 2dup r@ nrot r> nrot j + swap i + swap field-at! endif
  endfor endfor drop 2drop
;

: field-full-line?  ( y -- flag )
  true swap field-width for
    i over field-at@ ifnot nip false swap break endif
  endfor drop
;

: field-remove-line  ( y -- )
  dup 0 field-height within if
    field dup field-width +cells rot field-width * cells move
    field field-width cells erase
  else drop endif
;

: field-compress  ( -- line-count )
  0 field-height begin dup +while 1-
    dup field-full-line? if
      dup field-remove-line 1+ swap 1+ swap
    endif
  repeat drop
;

previous definitions
