" FCP_ur.f" tload

true to ANSI-BOARD?
true to showCoords?

: go-watch
  ." \x1b[2J"
  ." \x1b[1;1H"
  .board
  begin
    key drop
    ." \x1b[J"
    think DROP    \ press any key to stop thinking and make a move
    pv @ ?DUP IF
      ." Move found: " DUP .move CR
      makeMove DROP
    THEN
    ." \x1b[1;1H"
    .board
    .result?
  until
;
