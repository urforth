require char+ !libs/ans/crap.f

(*
S" [UNDEFINED]" PAD C! PAD CHAR+ PAD C@ CMOVE
PAD FIND NIP 0= [IF]
: [UNDEFINED] ( "word" -- tf ) BL WORD FIND NIP 0= ; IMMEDIATE
[THEN]
[UNDEFINED] [DEFINED] [IF]
: [DEFINED] ( "word" -- tf ) BL WORD FIND NIP ; IMMEDIATE
[THEN]
*)

[UNDEFINED] CELL [IF]
1 CELLS CONSTANT CELL
[THEN]
[UNDEFINED] CELL- [IF]
: CELL- POSTPONE CELL POSTPONE - ; IMMEDIATE
[THEN]

[UNDEFINED] KEY? [IF]
: KEY?  ( -- flag ) false ;
[ENDIF]

\ Wil Baden implements wbMACRO for portably inlining code
: wbMACRO  ( "name <char> ccc<char>" -- )
  :
  parse-name 1 <> err-char-expected ?error c@ \ CHAR
  PARSE  POSTPONE SLITERAL  POSTPONE EVALUATE
  POSTPONE ; IMMEDIATE
;
(*
: wbMACRO  ( "name <char> ccc<char>" -- )
  create [compile] immediate
  parse-name 1 = not-?abort" fuck?"
  c@ parse dup cell+ n-allot c4s:copy-counted
 does>
  count evaluate
;
*)
\ ' MACRO ALIAS wbMACRO \ if line-profiling on Win32Forth

[UNDEFINED] tolower [IF]
\ : tolower ( C -- c ) $20 OR ;
alias locase-char tolower
[THEN]
