\ SHA1 test
use-lib: sha1

CR SHA1:Sha-Init
   SHA1:Sha-Final .( DA39A3EE 5E6B4B0D 3255BFEF 95601890 AFD80709 )
   CR SHA1:.SHA CR

CR SHA1:Sha-Init  S" a" SHA1:Sha-Update
   SHA1:Sha-Final .( 86F7E437 FAA5A7FC E15D1DDC B9EAEAEA 377667B8 )
   CR SHA1:.SHA CR

CR SHA1:Sha-Init  S" abc" SHA1:Sha-Update
   SHA1:Sha-Final .( A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D )
   CR SHA1:.SHA CR

CR SHA1:Sha-Init  S" abcdefghijklmnopqrstuvwxyz" SHA1:Sha-Update
   SHA1:Sha-Final .( 32D10C7B 8CF96570 CA04CE37 F2A19D84 240D3A89 )
   CR SHA1:.SHA CR

CR SHA1:Sha-Init
   S" abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
      SHA1:Sha-Update
   SHA1:Sha-Final .( 84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1 )
   CR SHA1:.SHA CR

\  A million repetitions of "a".

: HACK              ( -- )
    SHA1:Sha-Init
    1000000 0 DO
        S" aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            SHA1:Sha-Update
    50 +LOOP
    SHA1:Sha-Final ;

CR  .( 34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F )
CR HACK SHA1:.SHA CR

.stack
bye
