\ Calculation of digits of pi without floating-point
\ The algorithm calculate one digit at the time
\ Michel Jean, April 2020
\ adapded to UrForth by Ketmar Dark

1001 constant nbr-digits                \ number of digit - add one, the last digit is not printed
0 var pos                               \ position in the array
0 var ret                               \ carry (retenue in french ;-) )
nbr-digits  10 3 */ constant nbr-cells  \ initialisation on the number of cells
0 var arr-cells nbr-cells cells allot

: initialisation ( -- ) \ initialisation of the array
  nbr-cells for 2 arr-cells i +cells ! endfor
;

: algo-base ( -- )
  pos @ 1 = if
    cell arr-cells + @ 10 * ret @ +
    dup
    10 mod arr-cells cell+ !
    10 / ret !
  else
    pos @ cells arr-cells + @ 10 * ret @ +
    2 pos @ * 1-
    2dup
    mod arr-cells pos @ +cells !
    / pos @ 1- * ret !
  endif
;

: release-stack  \ hold the last digit and release other digits of the stack
  depth 1- +if depth 1- roll 0 .r recurse endif
;

: release-stack+1  \ hold the last digit and release other digits of the stack + 1 modulo 10
  depth 1- +if depth 1- roll 1+ 10 mod 0 .r recurse endif
;

: set-predigit ( -- n )
  ret @ dup 9 < if release-stack else
  dup 10 = if release-stack+1 drop 0
  endif endif
;

: 1digit ( -- )  \ calculate 1 digit at the time
  1 nbr-cells do i pos ! algo-base -1 +loop set-predigit
;

: run ( -- )
  ret 0! initialisation
  nbr-digits 50 / for
    50 for 1digit endfor     \ 50 digits by line
  cr endfor
  drop  \ drop the last digit (value not safe)
;

run

.stack bye
