;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
use-lib: minithreads

(*
.( calling default `printf`...\n)
: prstr  " num=%d (0x%08x)\n" drop ;
" printf" os:rtld-default os:dlsym dup not-?abort" dlsym failed" value libc.printf
0x29a 42 prstr libc.printf 3 os:cinvoke . cr
*)


: trdtest  ( -- )
  300 os:mssleep

  ." created a child thread with tid " os:get-tid . cr
  ."   child sp=0x" sp0 @ .hex8 cr
  ." child0: sp=0x" sp0 @ .hex8 ."  tls=0x" forth:(user-base-addr) @ .hex8 ."  pid=" os:get-pid . ." tid=" os:get-tid . cr

  (* this doesn't work (yet?)
  ." calling default `printf`...\n"
  " printf" os:rtld-default os:dlsym dup not-?abort" dlsym failed" >r
  0x29a 42 " num=%d (0x%08x)\n" r> 3 os:cinvoke . cr
  *)

  \ 0 os:exit-thread
;


' trdtest thread:spawn
.stack
500 os:mssleep
0 os:exit-thread
\ bye
