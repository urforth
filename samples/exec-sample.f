use-libs: exec os-errno

: (errno?)  ( errno false // ... true -- ... )
  ifnot
    endcr ." CANNOT EXEC; ERROR: " os:errno-name type cr
    1 n-bye
  endif
;

: test-exec  ( -- )
  ;; env
  os:strarray-envp (errno?)
  os:strarray-finalize dup >r os:strarray->array
    \ r@ os:strarray-dump .stack bye
  ;; args
  os:strarray-init (errno?)
  " /usr/bin/ls" os:strarray-push (errno?)
  " -la" os:strarray-push (errno?)
  os:strarray-finalize dup >r os:strarray->array
    \ r@ os:strarray-dump .stack bye
  r@ os:strarray-first
    \ dup zcount type cr
  os:exec
  r> os:strarray-free r> os:strarray-free
  ifnot
    endcr ." CANNOT EXEC; ERROR: " os:errno-name type cr
    1 n-bye
  endif
    \ .stack
  ." PID: " dup . cr
  ( os:WEXITED) 0 os:wait ifnot
    endcr ." CANNOT WAIT; ERROR: " os:errno-name type cr
    1 n-bye
  endif
  ." WAIT PID: " . cr
  ." WAIT CODE: 0x" dup .hex8 cr
  ." WTERMSIG: " dup os:WTERMSIG . cr
  ." WEXITSTATUS: " dup os:WEXITSTATUS . cr
  drop
;


test-exec
.stack bye
