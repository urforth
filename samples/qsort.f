\ ANEW --QSORT--                                \  Wil Baden  1999-04-13

\          QSORT from _Forth Dimensions_ vol.5

\  Leo Wong resurrected a version of Quicksort that I published
\  in 1983. I no longer had a copy, and had forgotten it. I
\  recall that a design constraint was to fit in one screen.

\  It doesn't do median-of-three or insertion sort under a
\  threshold. It is recursive.

\  To my shock it has been 20-25 percent faster in tests than my
\  "improved" version.

\  PRECEDES            ( addr_1 addr_2 -- flag )
\      Defer-word for comparison.  Return TRUE for "lower".

\  SPRECEDES           ( addr_1 addr_2 -- flag )
\      String comparison for `PRECEDES`.

\  EXCHANGE            ( addr_1 addr_2 -- )
\      Exchange contents of two addresses.

\  CELL-               ( addr -- addr' )
\      Decrement address.

\  PARTITION           ( lo hi -- lo_1 hi_1 lo_2 hi_2 )
\      Partition array around its median.

\  QSORT               ( lo hi -- )
\      Partition array until done.

\  SORT                ( addr n -- )
\      Setup array for recursive partitioning.

\  Set PRECEDES for different datatypes or sort order.

DEFER-NOT-YET PRECEDES  ' < IS PRECEDES

\  For sorting character strings in increasing order:

(*
: SPRECEDES                 ( addr addr -- flag )
    >R COUNT R> COUNT COMPARE 0< ;

  ' SPRECEDES IS PRECEDES
*)

: exchange  ( addr_1 addr_2 -- )  dup @ >r  over @ swap !  r> swap ! ;

: partition  ( lo hi -- lo_1 hi_1 lo_2 hi_2 )
  2dup over - bytes->cells 2u/ +cells @ >r  ( R: median)
  2dup begin                      ( lo_1 hi_2 lo_2 hi_1)
    swap begin dup @ r@  precedes while cell+ repeat
    swap begin r@ over @ precedes while cell- repeat
    2dup > ifnot 2dup exchange  >r cell+ r> cell- endif
  2dup > until                    ( lo_1 hi_2 lo_2 hi_1)
  r> drop                         ( R: )
  swap rot                        ( lo_1 hi_1 lo_2 hi_2)
;

: qsort  ( lo hi -- )
  partition                       ( lo_1 hi_1 lo_2 hi_2)
  2over 2over  - +                ( . . . . lo_1 hi_1+lo_2-hi_2)
       < if  2swap  then          ( lo_1 hi_1 lo_2 hi_2)
  2dup < if  recurse  else  2drop  then
  2dup < if  recurse  else  2drop  then ;

: sort  ( addr n -- )
  dup 2 < if  2drop  exit then
  1- cells over + ( addr addr+{n-1}cells) qsort ( )
;

\\  *************************  End of QSORT  *************************

1 [IF]
\ create test-array here 10 , 50 , 40 , 32 , 666 , 69 , 42 , here swap - create; bytes->cells constant #test-array
24 constant #test-array
#test-array cells buffer: test-array

: gen-random-array  ( addr n -- )
  2>r prng:gen-dseed prng:pcg32-seed-u64 2r> for
    >r prng:pcg32-next 32767 and r@ ! r> cell+
  endfor drop 2drop
;
test-array #test-array gen-random-array

: check-array  ( addr n -- )
  dup 2 < if 2drop exit endif
  1- for dup @ over cell+ @ <= not-?abort" sortbug" cell+ endfor drop
;


: dump-array  ( addr n -- )
  endcr for dup @ . cell+ loop drop cr
;

test-array #test-array dump-array
test-array #test-array sort
test-array #test-array dump-array
test-array #test-array check-array

[ENDIF]

.stack
bye
