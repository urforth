#!/bin/sh

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"
rm urforth 2>/dev/null
./urforth1_min meta/asm-metc.f "$@"
res=$?
cd "$odir"
exit $res
