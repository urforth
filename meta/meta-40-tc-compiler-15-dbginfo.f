;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; currently, debug info is very simple, it contains only PC->LINE mapping
;;
;; header:
;;   dw itemcount        ; files with more that 64kb lines? 'cmon!
;;   dw firstline        ; files with more that 64kb lines? 'cmon!
;;   dd filename-c1strz  ; this points to special hidden buffer word, or 0
;;
;; items:
;;   db pcoffs   ; offset from the previous PC (first: from the CFA)
;;   db lineofs  ; offset from the previous line
;;
;; if lineofs is 255, next word is 16-bit line offset
;; if pcofs is 255, next word is 16-bit line offset
;;
;; the compiler doesn't store each PC, it only stores line changes
;; that is, the range for the line lasts until the next item
;; items should be sorted by PC (but the code should not fail on
;; unsorted data)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; list of known source files
;; structure:
;;   dd nextptr     ;; or 0
;;   dd tcaddr

0 var tc-(dbg-file-list-head)

;; no checks are made
: tc-(dbg-add-file)  ( addr count -- straddr )
  2 cells brk-alloc
  tc-(dbg-file-list-head) @ over !
  dup tc-(dbg-file-list-head) ! cell+
  tc-here swap !
  ;; copy string
  dup 1+ tc-n-allot dup >r
  2dup tc-c! 1+ tc->real swap 0 max cmove
  r>
;

;; this can return 0
: tc-(dbg-find-or-add-file)  ( addr count -- straddr )
  str-extract-name ?dup if
    tc-(dbg-file-list-head)
    begin
      @ ?dup
    while
      >r 2dup r@ cell+ @ tc->real bcount s= if
        2drop r> cell+ @ exit
      endif
      r>
    repeat
    ;; add new
    tc-(dbg-add-file)
  else
    drop 0
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create compressed debug info, set dfa

;; let's use globals here, because why not?
0 value tc-(dbginfo-cpbuf-start)
0 value tc-(dbginfo-cpbuf-curpos)
0 value tc-(dbginfo-cpbuf-currline)
0 value tc-(dbginfo-cpbuf-currpc)

: tc-(dbginfo-build-byte)  ( b -- )
  tc-(dbginfo-cpbuf-curpos) c!
  tc-(dbginfo-cpbuf-curpos) 1+ to tc-(dbginfo-cpbuf-curpos)
;

: tc-(dbginfo-build-word)  ( w -- )
  dup 0xff and tc-(dbginfo-build-byte)
  8 rshift tc-(dbginfo-build-byte)
;

: tc-(dbginfo-build-dword)  ( dw -- )
  dup 0xffff and tc-(dbginfo-build-word)
  16 rshift tc-(dbginfo-build-word)
;


;; prerequisite: debug info must exist, and must not be empty
: tc-(dbginfo-build-compressed)  ( destbuf -- destbuf size )
  ;; build file name (do it here, because it may allocate at here)
  forth:(tib-curr-fname) count tc-(dbg-find-or-add-file) >r
  ;; init vars
  dup to tc-(dbginfo-cpbuf-start)
  to tc-(dbginfo-cpbuf-curpos)
  ;; get starting line
  tc-(dbgbuf-base-addr) @ to tc-(dbginfo-cpbuf-currline)
  ;; get starting PC
  tc-latest-cfa to tc-(dbginfo-cpbuf-currpc)
  ;; create header
  ;; item count (we know that in advance)
  tc-(dbgbuf-curr-addr) @ tc-(dbgbuf-base-addr) - 3 rshift tc-(dbginfo-build-word)
  ;; first line
  tc-(dbginfo-cpbuf-currline) tc-(dbginfo-build-word)
  ;; file name
  r> tc-(dbginfo-build-dword)
  tc-(dbgbuf-curr-addr) @ tc-(dbgbuf-base-addr) do
    ;;   db pcoffs   ; offset from the previous PC (first: from the CFA)
    ;;   db lineofs  ; offset from the previous line
    ;;
    ;; if lineofs is 255, next word is 16-bit line offset
    ;; if pcofs is 255, next word is 16-bit line offset
    ;; PC
    i cell+ @ tc-(dbginfo-cpbuf-currpc) - dup 255 < if
      ;; 8-bit offset
      tc-(dbginfo-build-byte)
    else
      ;; 16-bit offset
      255 tc-(dbginfo-build-byte)
      tc-(dbginfo-build-word)
    endif
    ;; update current PC
    i cell+ @ to tc-(dbginfo-cpbuf-currpc)
    ;; line number
    i @ tc-(dbginfo-cpbuf-currline) - dup 255 < if
      ;; 8-bit offset
      tc-(dbginfo-build-byte)
    else
      ;; 16-bit offset
      255 tc-(dbginfo-build-byte)
      tc-(dbginfo-build-word)
    endif
    ;; update current line
      \ endcr i @ . tc-(dbginfo-cpbuf-currline) . cr
    i @ to tc-(dbginfo-cpbuf-currline)
  2 cells +loop
  ;; return addr and size
  tc-(dbginfo-cpbuf-start) tc-(dbginfo-cpbuf-curpos) over -
;

: tc-(dbginfo-finalize-and-copy)  ( -- )
  tc-(dbginfo-active?) tc-(dbginfo-enabled?) logand if
    tc-(dbgbuf-base-addr) ?dup if
      @ if
        ;; save current PC
        tc-(dbgbuf-curr-addr) @ 0! tc-(dbginfo-add-here)
        ;; compressed should never be bigger than the original
        tc-(dbgbuf-curr-addr) @ tc-(dbgbuf-base-addr) - 4 +cells dup >r
        os:prot-r/w os:mmap not-?abort" out of memory" dup >r
        tc-(dbginfo-build-compressed)
          \ endcr ." debug: from " tc-(dbgbuf-curr-addr) @ tc-(dbgbuf-base-addr) - . ." to " dup . cr
        ;; save here to debug info field
        tc-here tc-latest-nfa tc-nfa->dfa tc-!
        dup tc-n-allot  ;; ( addr bytes rva-newaddr )
        tc->real swap 0 max cmove
        r> r> swap os:munmap drop
      else
        drop
      endif
    endif
  endif
  ;; always deactivate
  tc-(dbginfo-reset)
;
