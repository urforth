;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; setup some predefined constants
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: tc-define-config-constants  ( -- )
  " URFORTH_BSS_RESERVE" tc-image-vsize 65536 max asmx86:asm-Make-Constant

  " URFORTH_ALIGN_HEADERS" tc-align-headers notnot asmx86:asm-Make-Constant
  " URFORTH_ALIGN_CFA" tc-align-cfa notnot asmx86:asm-Make-Constant
  " URFORTH_ALIGN_PFA" tc-align-pfa notnot asmx86:asm-Make-Constant
  " URFORTH_ALIGN_CFA_PFA" tc-align-cfa tc-align-pfa or notnot asmx86:asm-Make-Constant

  " URFORTH_CFA_SIZE" tc-align-pfa if 8 else 5 endif asmx86:asm-Make-Constant

  " URFORTH_DYNAMIC_BINARY" tc-dynamic-binary notnot asmx86:asm-Make-Constant

  " URFORTH_NAME_HASH_TYPE" tc-wordhash-type asmx86:asm-Make-Constant

  " URFORTH_NAME_HASH_ELF" tc-nhash-elf asmx86:asm-Make-Constant
  " URFORTH_NAME_HASH_JOAAT" tc-nhash-joaat asmx86:asm-Make-Constant
  " URFORTH_NAME_HASH_ROT" tc-nhash-rot asmx86:asm-Make-Constant

  " URFORTH_DEBUGGER_ENABLED" tc-debugger-enabled notnot asmx86:asm-Make-Constant
  " URFORTH_DEBUG_INFO_ENABLED" tc-debug-info-enabled notnot asmx86:asm-Make-Constant

  " URFORTH_MAX_WORD_NAME_LENGTH" tc-max-word-name-length asmx86:asm-Make-Constant

  " URFORTH_MAX_USERAREA_SIZE" tc-#userarea asmx86:asm-Make-Constant
  " URFORTH_MAX_USERAREA_CELLS" tc-#userarea 3 + 2 rshift asmx86:asm-Make-Constant

  " URFORTH_TLS_TYPE_NONE" tc-tls-none asmx86:asm-Make-Constant
  " URFORTH_TLS_TYPE_FS" tc-tls-fs asmx86:asm-Make-Constant
  " URFORTH_TLS_TYPE" tc-tls-type asmx86:asm-Make-Constant

  " URFORTH_LINUX_X86" tc-LINUX/X86 asmx86:asm-Make-Constant
  " URFORTH_WIN32" tc-WIN32 asmx86:asm-Make-Constant
  " URFORTH_OS" tc-os asmx86:asm-Make-Constant
;
