;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiler target buffer
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

\ $0804_8000 value elf-base-rva
$0040_0000 value elf-base-rva
0 value elf-entry-point-addr  ;; not rva
0 value elf-code-size-addr  ;; not rva
0 value elf-import-table-rva
0 value elf-import-table-size


;; allocate memory for target image
;; its virtual address is elf-base-rva
;; 1MB should be more than enough
1024 1024 * constant elf-target-memory-size
elf-target-memory-size brk-buffer: elf-target-memory

elf-base-rva value elf-current-pc  ;; rva


: tc-valid-addr?  ( rva -- flag )  elf-base-rva dup elf-target-memory-size + 1- bounds? ;
: tc-check-addr  ( rva -- rva )  dup tc-valid-addr? not-?abort" tc segmentation fault" ;

: tc->real  ( rva -- addr )  tc-check-addr elf-base-rva - elf-target-memory + ;
: real->tc  ( addr -- rva )  elf-target-memory - elf-base-rva + tc-check-addr ;

: curr-code-size  ( -- size ) elf-current-pc elf-base-rva - ;
