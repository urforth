;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; the following words are not in "tc interpreter vocabulary"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; sorry for this hack!
false value tc-(enum-mode)  (hidden)

: tc-(?not-enum)  ( -- )  tc-(enum-mode) ?abort" cannot be used in enum definition" ; (hidden)
: tc-(?enum)  ( -- )  tc-(enum-mode) not-?abort" cannot be used out of enum definition" ; (hidden)
