;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some compilers
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: tc-compile  ( -- )  \ word
  parse-name tc-cfa,-(str)
  " COMPILE," tc-compile,-(str)
;

: tc-[compile]  ( -- )  \ word
  parse-name tc-compile,-(str)
;

: tc-[']  ( -- )  \ word
  parse-name tc-cfa,-(str)
;

: tc-[execute-tail]  ( -- )  \ word
  tc-state @ not-?abort" [execute-tail] is compile-time only!"
  " LIT-EXECTAIL" tc-compile,-(str)
  parse-name tc-cfa,-(str-raw)
;

: tc-[char]  ( -- )  \ char
  parse-name 1 = not-?abort" character expected"
  c@ tc-literal
;

: tc-(parse-unescape-str)  ( -- addr count )
  34 parse 2dup here swap move nip here swap str-unescape
;

: tc-"  ( -- )  ;; "
  tc-(parse-unescape-str)
  tc-SLITERAL
;

alias tc-" tc-s"

: tc-."  ( -- )  ;; "
  tc-state @ not-?abort" tc-.\`: compile-time only!"
  " (.\`)" tc-compile,-(str)
  tc-(parse-unescape-str)
  tc-(c1strz)
;

: tc-vocid:  ( -- ) // ( -- vocid )  \ vocname
  tc-state @ not-?abort" tc-.\`: compile-time only!"
  parse-name x-tc-xcfind-must
  dup tc-cfa->ffa tc-ffa@ tc-(wflag-vocab) and not-?abort" not a vocabulary"
  tc-voc-cfa->vocid tc-literal
;


: tc-TRUE   ( -- 1 ) tc-state @ if " FORTH:LIT-TRUE" tc-compile,-(str) else true endif ;
: tc-FALSE  ( -- 1 ) tc-state @ if " FORTH:LIT-FALSE" tc-compile,-(str) else false endif ;

: tc-[  tc-state 0! ;

: tc-LITERAL  ( -- ) [compile] tc-literal ;
