;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; colon, semicolon, variable, etc.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: tc-(  [compile] ( ;
: tc-\  ( -- )  [compile] \ ;
: tc-(*  ( -- )  [compile] (* ;  ;; *)
[DEFINED] (( [IF]
: tc-((  ( -- )  [compile] (( ;  ;; ))
[ELSE]
: tc-((  ( -- )  forth:skip-comment-multiline-nested ;  ;; ))
[ENDIF]
: tc-;;  ( -- )  [compile] ;; ;
: tc-//  ( -- )  [compile] ;; ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-start-compile-forth-word  ( -- )
  tc-(dbginfo-reset)
  true to tc-(dbginfo-active?)
    \ endcr ."   START FORTH AT 0x" tc-here .hex8 cr
  tc-rva-doforth tc-compile-do-call
    \ endcr ."   FORTH PFA AT 0x" tc-here .hex8 cr
  tc-state 1!
  tc-jstack-init
; (hidden)

: tc-end-compile-forth-word  ( -- )
  tc-create;
  tc-smudge tc-state 0!
  ;; save debug info (this also resets and deactivates it)
  tc-(dbginfo-finalize-and-copy)
  tc-optimiser:optimise-jumps
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-:
  tc-?exec
  tc-(dbginfo-reset)
  tc-create-header
  tc-start-compile-forth-word
  ( tc-!csp)
  tc-(ctlid-colon)
;

(* i didn't decided how i should implement macros yet
: tc-macro:
  tc-?exec tc-!csp
  tc-(dbginfo-reset)
  tc-create-header
  tc-start-compile-forth-word
  tc-!csp
  tc-set-macro
;
*)

;; this is barely usable now, so it is commented out
;; it is barely usable because target compiler doesn't fully interpret the source,
;; so target CFA cannot be used in any meaningful way yet
(*
: tc-:NONAME
  tc-?exec
  tc-(dbginfo-reset)
  NullString tc-(create-header-nocheck) tc-hidden
  tc-start-compile-forth-word
  tc-latest-cfa
  tc-!csp
;
*)

: tc-;
  tc-?comp ( tc-?csp)
  tc-(ctlid-colon) tc-(ctlid-does) tc-?any-pair
  tc-compile exit
  tc-end-compile-forth-word
;

: tc-DOES>  ( -- pfa )
  tc-?comp tc-?non-macro
  \ tc-(ctlid-colon) tc-?pairs ( tc-?csp)  ;; make sure that all our conditionals are complete
  tc-(ctlid-colon) tc-(ctlid-does) tc-?any-pair
  tc-compile latest-cfa
  tc-compile (does>)  ;; and compile the real word
  tc-align-pfa if tc-check-align-here endif
  tc-optimiser:optimise-jumps
  tc-jstack-init
  tc-(ctlid-does)
;

: tc-recurse  ( -- )
  tc-?comp tc-?non-macro
  tc-latest-cfa
  tc-compile,
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-[:
  tc-?comp tc-?non-macro
    \ endcr ."   CBLOCK START AT 0x" tc-here .hex8 cr
  tc-align-pfa if tc-check-align-here endif
  ;; compiling
  tc-compile LITCBLOCK
    \ endcr ."   CBLOCK AFTER-LIT AT 0x" tc-here .hex8 cr
  tc-(mark-j>)
  tc-(CTLID-CBLOCK)
    \ endcr ."   CBLOCK CODE AT 0x" tc-here .hex8 cr
  tc-rva-doforth tc-compile-do-call
  tc-jstack-subinit
;

: tc-;]
  tc-?comp
  ;; inside a word
  tc-(CTLID-CBLOCK) tc-?pairs
  tc-compile exit
  tc-optimiser:optimise-jumps
  tc-(resolve-j>)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-to  ( value -- )  \ name
  " LITTO!" tc-compile,-(str)
  parse-name tc-cfa,-(str-raw)
;

: tc-+to  ( value -- )  \ name
  " LIT+TO!" tc-compile,-(str)
  parse-name tc-cfa,-(str-raw)
;

: tc--to  ( value -- )  \ name
  " LIT-TO!" tc-compile,-(str)
  parse-name tc-cfa,-(str-raw)
;

: tc-to^  ( -- addr )  \ name
  " LIT^TO" tc-compile,-(str)
  parse-name tc-cfa,-(str-raw)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; scattering colon implementation
;; based on the idea by M.L. Gassanenko ( mlg@forth.org )
;; written from scratch by Ketmar Dark
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; placeholder for scattered colon
;; it will compile two branches:
;; the first branch will jump to the first "..:" word (or over the two branches)
;; the second branch is never taken, and works as a pointer to the latest branch addr in the list
;; this way, each extension word will simply fix the last branch address, and update list tail
;; at the creation time, second branch points to the first branch
: tc-...  ( -- )
  tc-?comp tc-?non-macro
  tc-latest-cfa \ FIXME: dup word-type? word-type-forth <> ERR-ELLIPSIS-FORTH ?error
  tc-cfa->pfa tc-here <> ?abort" ellipsis must be the first word"
  tc-compile branch tc-(<j-mark) tc-(mark-j>)
  tc-compile branch swap tc-(<j-resolve)
  tc-(resolve-j>)
  tc-set-scolon
;


;; start scattered colon extension code
;; TODO: better error checking!
;; this does very simple sanity check, and remembers the address of the tail pointer
: tc-..:  ( -- )  \ word
  tc-?exec tc-?non-macro
  parse-name x-tc-xcfind-must \ FIXME: dup word-type? word-type-forth <> ERR-ELLIPSIS-FORTH ?error
  dup tc-cfa-scolon? not-?abort" scattered colon word expected"
  \ FIXME: make check and patch more independed from codegen internals
  tc-cfa->pfa \ FIXME: dup @ ['] branch <> ERR-ELLIPSIS-FIRST ?error
  2 +cells \ FIXME: dup @ ['] branch <> ERR-ELLIPSIS-FIRST ?error
  cell+  ;; pointer to the tail pointer
  NullString tc-(create-header-nocheck) tc-hidden tc-start-compile-forth-word tc-latest-cfa \ [compile] :noname
  tc-cfa->pfa  ;; :noname leaves our cfa
  ( tc-!csp)
  tc-(CTLID-SC-COLON)  ;; pttp ourcfa flag
;

;; this ends the extension code
;; it patches jump at which list tail points to jump to our pfa, then
;; it compiles jump right after the list tail, and then
;; it updates the tail to point at that jump address
: tc-;..  ( -- )  \ word
  tc-?comp tc-?non-macro
  tc-(CTLID-SC-COLON) tc-?pairs ( tc-?csp)
  ;; ( pttp ourcfa )
  over tc-(branch-addr@) tc-(branch-addr!)
  >r tc-compile branch tc-here 0 tc-, r@ cell+ swap tc-(branch-addr!)
  tc-here cell- r> tc-(branch-addr!)
  ;; we're done here
  tc-end-compile-forth-word
;

;; this ends the extension code
;; makes the code first in the jump list
;; jumps to the destination of the first jump
;; patches the first jump so it points to our nonamed code
;; patches tail pointer so it points to our jump
: tc-<;..  ( -- )
  tc-?comp tc-?non-macro
  tc-(CTLID-SC-COLON) tc-?pairs ( tc-?csp)
  >r  ;; ( pttp | ourcfa )
  ;; get first jump destination
  tc-compile branch tc-here 0 tc-,  ;; ( pttp jpatchaddr )
  over 2 -cells tc-(branch-addr@) over tc-(branch-addr!)  ;; fix our jump
  over 2 -cells r> swap tc-(branch-addr!)  ;; fix first jump
  ;; patch original jump if there are no items in scattered chain yet
    \ endcr ." pptp:" over .hex8 ."  jpa:" dup .hex8 over compiler:(branch-addr@) ."  pptp-j:" .hex8 cr
  over dup tc-(branch-addr@) 2 +cells = if
      \ endcr ." !!!patch!\n"
    swap tc-(branch-addr!)
  else 2drop endif
  \ 2drop \ swap compiler:(branch-addr!)
  ;; we're done here
  tc-end-compile-forth-word
;
