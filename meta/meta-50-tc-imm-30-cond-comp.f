;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

vocabulary (concomp-flow) (hidden) also (concomp-flow) definitions

: [IF]  ( level -- newlevel )  1+ ;
alias [IF] [IFNOT]
alias [IF] $if

: [ELSE] ( level -- newlevel ) 1- dup if 1+ endif ;
alias [ELSE] $else

: [ENDIF]  ( level -- newlevel )  1- ;
alias [ENDIF] [THEN]
alias [ENDIF] $endif

previous definitions


: tc-[ELSE]  ( -- )
  1 ;; level
  begin
    begin
      [DEFINED] parse-name-ex [IF] parse-name-ex [ELSE] parse-skip-comments parse-name [ENDIF]
      dup ifnot tc-refill not ERR-UNBALANCED-IFDEF ?error endif
    dup until
    vocid: (concomp-flow) voc-search-noimm if execute else 2drop endif
  dup not-until drop
; ( immediate )
alias tc-[ELSE] tc-$ELSE

: tc-[ENDIF]  ( -- )  ; immediate
alias tc-[ENDIF] tc-[THEN]
alias tc-[ENDIF] tc-$endif


: tc-[IF]  ( cond -- )
  ifnot [compile] tc-[ELSE] endif
; ( immediate )

: tc-[IFNOT]  ( cond -- )
  if [compile] tc-[ELSE] endif
; ( immediate )

(*
: tc-[IFDEF]  ( -- )  \ word
  parse-name has-word? ifnot [compile] tc-[ELSE] endif
; immediate

: tc-[IFNDEF]  ( -- )  \ word
  parse-name has-word? if [compile] tc-[ELSE] endif
; immediate

: tc-[DEFINED]  ( -- flag )  \ word
  parse-name has-word?
; immediate

: tc-[UNDEFINED]  ( -- flag )  \ word
  parse-name has-word? not
; immediate
*)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-$IF  ( -- )  \ expr
  asmx86:lexer:PrepareLineParser
  tc-next-token
  asmx86:macro-instrs:(if-eval-cond)
  asmx86:tk-eol? not-?abort" eol expected"
  ifnot [compile] tc-[ELSE] endif
; ( immediate )
