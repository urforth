;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; low-level target compiler words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; addr is NOT rva!
: tc-xcfind-plain  ( addr count voclptr -- rva-cfa true // false )
  over 1 tc-max-word-name-length bounds? ifnot drop 2drop false exit endif
  begin
    tc-@ ?dup
  while
    ;; ( addr count rva-lfa )
      \ >r endcr 2dup type ."  : " r@ tc-lfa->nfa tc-nfa-count swap tc->real swap type space r@ .hex8 cr r>
    >r 2dup r@ tc-lfa->nfa
    ;; check smudge flag
    dup tc-nfa->ffa tc-ffa@ tc-(wflag-smudge) and ifnot
      tc-nfa-count swap tc->real swap s=ci if
        ;; i found her!
        2drop r> tc-lfa->cfa true exit
      endif
    else
      drop 2drop
    endif
    r>
  repeat
  2drop false
;


: tc-xcfind  ( addr count voclptr -- rva-cfa true // false )
  forth-hashtable-bits ifnot tc-xcfind-plain exit endif
  dup tc-vocid-hashed? ifnot tc-xcfind-plain exit endif
  over 1 tc-max-word-name-length bounds? ifnot drop 2drop false exit endif
  ;; calculate name hash
  >r 2dup tc-str-name-hash-real-addr
  ;; ( addr count u32hash | voclptr )
  ;; calc bucket address
  dup tc-name-hash-fold-mask cells r> tc-vocid->htable +
  swap >r
  ;; ( addr count bucketaddr | u32hash )
  begin
    tc-@ ?dup
  while
    ;; ( addr count rva-bfa | u32hash )
    ;; check hash
    dup tc-bfa->hfa tc-@ r@ = if
      ;; hash is ok, check name
      ;; no need to check length separately, because string comparison will do it for us
      dup tc-bfa->nfa
      ;; ( addr count rva-bfa rva-nfa | u32hash )
      2over  ;; ( addr count rva-bfa rva-nfa addr count | u32hash )
      rot    ;; ( addr count rva-bfa addr count rva-nfa | u32hash )
      tc-nfa-count swap tc->real swap s=ci if
        ;; ( addr count rva-bfa | u32hash )
        dup tc-bfa->ffa tc-ffa@ tc-(wflag-smudge) and ifnot
          ;; ( addr count rva-bfa | u32hash )
          tc-bfa->nfa tc-nfa->cfa
          nrot 2drop rdrop
          true
          exit
        endif
      endif
    endif
  repeat
  ;; ( addr count | u32hash )
  2drop rdrop
  false
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (x-tc-find-simple)  ( addr count -- rva-cfa true // false )
  tc-current ?dup ifnot 2drop false exit endif
  tc-@ >r 2dup r> tc-xcfind if nrot 2drop true exit endif
  tc-forth dup tc-current tc-@ = if drop 2drop false exit endif
  tc-xcfind
;

: x-tc-xcfind  ( addr count -- rva-cfa true // false )
  2dup (x-tc-find-simple) if nrot 2drop true exit endif
  ;; very simple one-level colon resolution
  2dup [char] : str-trim-at-char ?dup ifnot
    drop 2drop false exit
  endif
  (x-tc-find-simple) ifnot 2drop false exit endif
  dup tc-cfa->ffa tc-ffa@ tc-(wflag-vocab) and ifnot
    drop 2drop false
  else
    tc-voc-cfa->vocid nrot
    [char] : str-skip-after-char rot
    tc-xcfind
  endif
;

: x-tc-xcfind-must  ( addr count -- rva-cfa )
  2dup 2>r x-tc-xcfind ifnot
    2r> endcr ." UNKNOWN WORD: \`" type ." \`\n"
    abort" unknown target system word"
  else
    2rdrop
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (tc-fix-label-name)  ( addr count -- addr count )
  ;; check for colon (i should introduce "str-chr" or something! ;-)
  tc-current tc-@ tc-forth <> if
    2dup [char] : str-trim-at-char nip ifnot
      ;; no colon, and not FORTH dictionary, get dict name
      tc-current tc-@ tc-vocid->headnfa tc-@
      tc-id-count swap tc->real swap pad 256 + c4s:copy-counted
      " :" pad 256 + c4s:cat-counted
      ;; append word name
      pad 256 + c4s:cat-counted
      ;; new name at pad
      pad 256 + count
        \ endcr ." SYNTH NAME:<" 2dup type ." >\n"
    endif
  endif
  ;; remove "forth:" prefix, if there is any
  2dup [char] : str-trim-at-char dup if
    2dup " FORTH" s=ci if
      1+ nip  ;; we'll cut with this
      /string
        \ endcr 2dup ." ||" type cr
    else
      2drop
    endif
  else
    2drop
  endif
;


;; this prepends the name of the current vocabulary if it is not tc-forth
;; (and if string contains no colon)
: (tc-create-forth-label)  ( addr count value forward -- )
  asm-labman:do-fix-label-name? >r
  0 to asm-labman:do-fix-label-name?
  2swap (tc-fix-label-name) 2swap
  asmx86:asm-Make-Forth-Label
  r> to asm-labman:do-fix-label-name?
;

: tc-create-forward-forth-label  ( addr count -- )
  dup if 0 true (tc-create-forth-label) else 2drop endif
;

: (tc-create-forth-label-fixup)  ( addr count -- )
  asm-labman:do-fix-label-name? >r
  0 to asm-labman:do-fix-label-name?
  (tc-fix-label-name)
  4 asmx86:LABEL-TYPE-CFA asmx86:asm-Label-Fixup
  r> to asm-labman:do-fix-label-name?
;

: tc-create-forth-label-disp-fixup  ( addr count -- )
  dup if
    asm-labman:do-fix-label-name? >r
    0 to asm-labman:do-fix-label-name?
    (tc-fix-label-name)
    ;; it should not be different from a normal label, so...
    ;; (if it is different, it is a bug)
    ( asmx86:LABEL-TYPE-CFA ) asmx86:LABEL-TYPE-NORMAL asmx86:asm-Jmp-Label-Fixup
    r> to asm-labman:do-fix-label-name?
  else
    2drop
  endif
;

: tc-create-forward-forth-label-and-fixup  ( addr count -- )
  dup if
    2dup tc-create-forward-forth-label
    (tc-create-forth-label-fixup)
  else
    2drop
  endif
;

;; non-forward
: tc-create-forth-label  ( addr count value -- )
  over if false (tc-create-forth-label) else drop 2drop endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; WARNING! call this ONLY if you're ABSOLUTELY sure that the word is not in the tc system yet!
;; addr is NOT rva!
: tc-cfa,-(str)-nochecks  ( addr count -- )
  tc-create-forward-forth-label-and-fixup
  0 tc-,
;

;; WARNING! call this ONLY if you're ABSOLUTELY sure that the word is not in the tc system yet!
;; addr is NOT rva!
: tc-compile,-(str)-nochecks  ( addr count -- )
  tc-create-forward-forth-label-and-fixup
  0 tc-,
;

;; addr is NOT rva!
: tc-compile,-(str)  ( addr count -- )
  2dup x-tc-xcfind if
    tc-compile,
      \ endcr ." KNOWN:<" 2dup type ." >\n"
    2drop
  else
      \ endcr ." *UNKNOWN:<" 2dup type ." >\n"
    tc-compile,-(str)-nochecks
  endif
;

;; addr is NOT rva!
: tc-cfa,-(str-raw)  ( addr count -- )
  2dup x-tc-xcfind if
    tc-reladdr, 2drop
  else
    \ FIXME! this WILL break if i'll make "," and "compile," different!
    tc-cfa,-(str)-nochecks
  endif
;

;; addr is NOT rva!
: tc-cfa,-(str)  ( addr count -- )
  " LITCFA" tc-compile,-(str)
  tc-cfa,-(str-raw)
;

: tc-compile  ( -- )  \ word
  parse-name [compile] sliteral
  compile tc-compile,-(str)
; immediate


0
dup constant tc-rva-doforth cell+
dup constant tc-rva-doconst cell+
dup constant tc-rva-dovar cell+
dup constant tc-rva-dovalue cell+
dup constant tc-rva-dodefer cell+
dup constant tc-rva-dodoes cell+
dup constant tc-rva-dooverride cell+
dup constant tc-rva-douservar cell+
constant tc-rva-do-size

tc-rva-do-size buffer: tc-rva-do-table
tc-rva-do-table tc-rva-do-size erase


: tc-get-do-label-name  ( type -- addr count )
  case
    tc-rva-doforth of " ur_doforth" endof
    tc-rva-doconst of " ur_doconst" endof
    tc-rva-dovar of " ur_dovar" endof
    tc-rva-dovalue of " ur_dovalue" endof
    tc-rva-dodefer of " ur_dodefer" endof
    tc-rva-dodoes of " ur_dodoes" endof
    tc-rva-dooverride of " ur_dooverride" endof
    tc-rva-douservar of " ur_douservar" endof
    abort" tc-get-do-label-name: invalid do label type"
  endcase
;

;; returns false if the label is not defined yet
;; updates label cache if it is defined
: tc-get-label-addr  ( type -- addr true // false )
  dup tc-rva-do-table + @ dup ifnot drop  ;; no cached address yet, check if it is defined
    dup tc-get-do-label-name asmx86:asm-Get-Label  dup not-?abort" tc-get-label-addr: wutafuck?"
    ;; ( type value 1 // type value -1 )
    -if 2drop false else swap tc-rva-do-table + 2dup ! drop true endif
  else nip true endif
;

;; this puts fixup if necessary
: tc-put-do-label-disp  ( type -- )
  dup tc-get-label-addr if  ;; known address
    asmx86:asm-PC 4+ - asmx86:asm-, drop
  else  ;; unknown address, create fixup
    tc-get-do-label-name asmx86:LABEL-TYPE-NORMAL asmx86:asm-Jmp-Label-Fixup
    0 asmx86:asm-,
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (tc-is-cfa-call?)  ( rvacfa dotype -- flag )
  tc-get-label-addr ifnot drop false
  else over tc-c@ 0xe8 = ifnot 2drop false
  else swap 1+ dup tc-@ 4+ + =
  endif endif
;

: tc-is-constant?  ( rvacfa -- flag )  tc-rva-doconst (tc-is-cfa-call?) ;
: tc-is-variable?  ( rvacfa -- flag )  tc-rva-dovar (tc-is-cfa-call?) ;
: tc-is-value?  ( rvacfa -- flag )  tc-rva-dovalue (tc-is-cfa-call?) ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-compile-do-call  ( type -- )
  tc-check-align-here
  0xe8 tc-c, ;; CALL
  tc-put-do-label-disp
  tc-align-here
;

: tc-compile-call-cfa  ( rva-cfa -- )
  2dup x-tc-xcfind if
    tc-(cfa-call,)
    \ endcr ." WORD FOUND: <" 2dup type ." >\n"
    2drop
  else
    \ endcr ." FORWARD WORD: <" 2dup type ." >\n"
    2dup tc-create-forward-forth-label
    tc-(cfa-0call,)
    tc-create-forth-label-disp-fixup
  endif
;

: tc-compile-call  ( -- )  \ word
  parse-name [compile] sliteral
  compile tc-compile-call-cfa
; immediate


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-GetForthLabel  ( addr count type -- value 1 // value -1 // false )
\ asm-labman:dump-labels
  asm-labman:do-fix-label-name? >r
  0 to asm-labman:do-fix-label-name?
  ;; only CFA and PFA references are allowed
  dup asmx86:LABEL-TYPE-CFA = over asmx86:LABEL-TYPE-PFA = or asmx86:ERRID_ASM_INVALID_FORWARD_REF asmx86:not-?asm-error
  >r
  2dup x-tc-xcfind if
    ;; i found her!
    nrot 2drop
    r> asmx86:LABEL-TYPE-PFA = if tc-cfa->pfa endif
    1  ;; defined
  else
    ;; upcase it
    pad 256 + c4s:copy-counted
    pad 256 + count 2dup upcase-str
    0 r> asmx86:LABEL-TYPE-PFA = if tc-cfa->pfa endif
    dup >r
    ;; ( addr count value forward -- )
    true asmx86:asm-Make-Forth-Label
    r>
    -1  ;; undefined yet
  endif
  r> to asm-labman:do-fix-label-name?
;

' tc-GetForthLabel to asmx86:asm-Get-Forth-Word
