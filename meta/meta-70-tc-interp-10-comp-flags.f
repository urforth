;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: ALIAS  ( -- )
  parse-name x-tc-xcfind-must
  dup >r
  parse-name tc-create-header-named tc-smudge
  tc-(jmp,)
  ;; copy "hidden" and "immediate" flags
  r> tc-cfa->ffa tc-ffa@
  tc-(wflag-hidden) tc-(wflag-immediate) or and >r
  tc-latest-cfa tc-cfa->ffa
  dup tc-ffa@ tc-(wflag-hidden) tc-(wflag-immediate) or ~and r> or
  swap tc-ffa!
  tc-create;
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: CREATE  ( -- )
  parse-name tc-(variable-header-str)
  tc-create;
;

: CREATE;  ( -- )
  tc-create;
;

: BUFFER:  ( size -- )
  parse-name tc-(variable-header-str)
  tc-n-allot drop
  tc-create;
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (HIDDEN)  ( -- )  tc-hidden ;
: (PUBLIC)  ( -- )  tc-public ;
: (NORETURN)  ( -- )  tc-noreturn ;
: (CODEBLOCK)  ( -- )  tc-codeblock ;
: IMMEDIATE  ( -- )  tc-immediate ;
: IMMEDIATE-NOOP  ( -- )  tc-immediate tc-(noop) ;

: (ARG-NONE)    ( -- )  tc-arg-none ;
: (ARG-BRANCH)  ( -- )  tc-arg-branch ;
: (ARG-LIT)     ( -- )  tc-arg-lit ;
: (ARG-C4STRZ)  ( -- )  tc-arg-c4strz ;
: (ARG-CFA)     ( -- )  tc-arg-cfa ;
: (ARG-CBLOCK)  ( -- )  tc-arg-cblock ;
: (ARG-VOCID)   ( -- )  tc-arg-vocid ;
: (ARG-C1STRZ)  ( -- )  tc-arg-c1strz ;
: (ARG-U8)      ( -- )  tc-arg-u8 ;
: (ARG-S8)      ( -- )  tc-arg-s8 ;
: (ARG-U16)     ( -- )  tc-arg-u16 ;
: (ARG-S16)     ( -- )  tc-arg-s16 ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: error-table-msg,"  ( code -- )  \ msg"
  ;; UrForth level 0 uses byte for error code, UrForth level 1 uses 4
  tc-,  ;; store code
  34 parse dup tc-n-allot tc->real swap move
  0 tc-c, ;; terminating zero for the string
;

: error-table-end  ( -- )
  0 tc-,
  tc-create;
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ]
  tc-state 1!
;
