;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; create aliases for all public words from vocid
: tc-populate-vocid  ( srcvocid destvocid -- )
  current @ >r ;; save "CURRENT"
  current !
  [:  ( nfa -- exitflag )
    dup nfa->ffa ffa@ [ (WFLAG-SMUDGE) (WFLAG-HIDDEN) or ] literal and
    ifnot
      ;; sanity check
      dup id-count 3 > swap 3 " TC-" s= or if
          \ endcr dup id-count 3 /string type cr
        ;; (ALIAS)
        ;; create new word
        dup nfa->cfa swap id-count 3 /string
        forth:(alias-str)
      else
        endcr ." non-hidden non-tc tc immediate \`" id-count type ." \`\n"
        abort" non-hidden non-tc tc immediate"
        \ drop
      endif
    else
      drop
    endif
    false
  ;] foreach-word drop
  \ previous
  r> current !  ;; restore "CURRENT"
;


vocabulary tc-immediates
\ also tc-immediates definitions
vocid-of tc-immediates-src vocid-of tc-immediates tc-populate-vocid
