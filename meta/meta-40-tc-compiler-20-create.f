;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; wordlist management for metacompiler
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: tc-create;  ( -- )
  tc-here tc-latest-nfa tc-nfa->sfa tc-!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word header format:
;; note than name hash is ALWAYS calculated with ASCII-uppercased name
;; (actually, bit 5 is always reset for all bytes, because we don't need the
;; exact uppercase, only something that resembles it)
;; bfa points to next bfa or to 0 (this is "hash bucket pointer")
;; before nfa, we have such "hidden" fields:
;;   dd dfa       ; pointer to the debug data; can be 0 if debug info is missing
;;   dd sfa       ; points *after* the last word byte
;;   dd bfa       ; next word in hashtable bucket; it is always here, even if hashtable is turned off
;;                ; if there is no hashtable, this field is not used
;; lfa:
;;   dd lfa       ; previous word LFA or 0 (lfa links points here)
;;   dd namehash  ; it is always here, and always calculated, even if hashtable is turned off
;; nfa:
;;   dd flags-and-name-len   ; see below
;;   db name      ; no terminating zero or other "termination flag" here
;;   db namelen   ; yes, name length again, so CFA->NFA can avoid guessing
;;   machine code follows
;;   here we usually have CALL to word handler
;;   0xE8, 4-byte displacement
;;   (displacement is calculated from here)
;;
;; first word cell contains combined name length (low byte), argtype and flags (other bytes)
;; layout:
;;   db namelen
;;   db argtype
;;   dw flags
;;
;; i.e. we have 16 bits for flags, and 256 possible argument types. why not.
;;
;; flags:
;;  bit 0: immediate
;;  bit 1: smudge
;;  bit 2: noreturn
;;  bit 3: hidden
;;  bit 4: codeblock
;;  bit 5: vocabulary
;;  bit 6: main scattered colon word (with "...")
;;  bit 7: macro (it *may* be inlined)
;;
;;  argtype is the type of the argument that this word reads from the threaded code.
;;  possible argument types:
;;    0: none
;;    1: branch address
;;    2: cell-size numeric literal
;;    3: cell-counted string with terminating zero (not counted)
;;    4: cfa of another word
;;    5: cblock
;;    6: vocid
;;    7: byte-counted string with terminating zero (not counted)
;;    8: unsigned byte
;;    9: signed byte
;;   10: unsigned word
;;   11: signed word

;; addr is NOT RVA!
: tc-(create-header-nocheck)  ( addr count -- )
  0 max tc-max-word-name-length min  ;; sanitize length, just in case
  tc-align-headers if
    begin tc-here 3 and while 0 tc-c, 1 +to tc-align-headers-wasted repeat
  endif
  dup if tc-created-words-count 1+ to tc-created-words-count endif
    \ endcr ." NEW HEADER AT 0x" tc-here .hex8 ."  <" 2dup type ." >\n"
  0 tc-,  ;; allocate dfa
  0 tc-,  ;; allocate sfa
  0 tc-,  ;; allocate bfa (it will be patched later)
  tc-here ;; remember HERE (it will become the new latest)
  tc-latest-lfa tc-,   ;; put lfa
  tc-current tc-@ tc-! ;; update latest
  dup if
    ;; put name hash
    2dup tc-str-name-hash-real-addr dup tc-,
    ;; fix bfa
    forth-hashtable-bits if
      ;; ( addr count hash )
      tc-current tc-@ tc-vocid-hashed? if
        ;; fold hash
        tc-name-hash-fold-mask  \ endcr dup ." !!! 0x" .hex8 cr
        ;; calc bucket address
        tc-current tc-@ tc-vocid->htable cells^
        ;; ( addr count bkptr )
        ;; load old bfa link
        dup tc-@
        ;; ( addr count bkptr oldbfa )
        ;; store current bfa address
        tc-here tc-nfa->bfa rot tc-!
        ;; update bfa
        tc-here tc-nfa->bfa tc-!
      else drop endif
    else
      drop  ;; we don't really need any hash
    endif
  else
    0 tc-,  ;; namehash
  endif
  ;; remember HERE (we will need to fix some name fields later)
  tc-here >r
  ;; compile counter (we'll add flags and other things later)
  ;; it is guaranteed that all fields except length are zero here
  ;; (due to word header layout, and length check above)
  dup tc-,
  dup if
    ;; copy parsed word to HERE (and allocate name bytes for it)
    dup tc-n-allot tc->real swap move
    ;; change the case of a new name?
    tc-create-case? ?dup if r@ tc->real count rot +if upcase-str else locase-str endif endif
  else 2drop endif
  ;; put flags (ffa is 16 bits at nfa+2)
  (wflag-smudge) ;; current_mode or
  r@ tc-nfa->ffa tc-tfa!  ;; it is safe to poke here, ffa is empty
  tc-align-cfa tc-align-pfa or if  ;; align CFA at 4 bytes; put incremented length byte just before CFA
    0 tc-c,
    r@ tc-c@ begin tc-here 3 and while 1+ 0 tc-c, 1 +to tc-align-cfa-wasted repeat
    dup 255 u> ?abort" aligned name too long"
    tc-here 1- tc-c!
  else
    r@ tc-c@ tc-c,  ;; put length again (trailing length byte)
  endif
  r@ tc->real dup c@ swap 4+ swap  ;; we are at CFA, fixup references  ( addr count value forward -- )
    \ endcr ."   CFA AT 0x" tc-here .hex8 cr
  tc-here tc-create-forth-label
  rdrop  ;; we don't need nfa address anymore
  tc-create;  ;; setup initial size, why not
;

;; addr is NOT RVA!
: tc-(create-header)  ( addr count -- )
  ;; check length
  dup 1 tc-max-word-name-length bounds? not-?abort" invalid word name"
  ensure-forth-hashtable
  ;; check for duplicate word?
  \ true if
    ;; look only in the current dictionary
    2dup tc-current tc-@ tc-xcfind if
      tc-cfa->nfa tc->real dup c@ swap cell+ swap type ." redefined" error-line. cr
    endif
  \ endif
  tc-(create-header-nocheck)
;


: tc-create-header-named  ( addr count -- )
  tc-(create-header)
;

: tc-create-header  ( -- )  \ word
  parse-name tc-create-header-named
;


: (tc-xxx-hdr-str)  ( dotype -- )
  create ,
 does>  ( addr count pfa -- )  ( pfa: dotype )
  @ nrot tc-(create-header) tc-smudge tc-compile-do-call
;

tc-rva-dovar      (tc-xxx-hdr-str) tc-(variable-header-str)
tc-rva-doconst    (tc-xxx-hdr-str) tc-(constant-header-str)
tc-rva-dovalue    (tc-xxx-hdr-str) tc-(value-header-str)
tc-rva-dodefer    (tc-xxx-hdr-str) tc-(defer-header-str)


0 value tc-(ua-rva)

: tc-get-ua-rva  ( offs -- rva-addr )
  tc-(ua-rva) ?dup ifnot
    " ua_default_values" asmx86:asm-Get-Label 1 <> ?abort" ua_default_values must be defined here"
    dup to tc-(ua-rva)
  endif
  +
;


;; uservar is:
;;   dw ua_offset
;;   initvalues must be put to userarea at the given offset
: tc-(uservar-header-str)  ( size addr count -- ua_offset )
  rot dup 1 < ?abort" invalid uservar size"
  tc-userarea-used over + tc-#userarea u> ?abort" too many uservars"
  >r
  tc-(create-header)
  tc-rva-douservar tc-compile-do-call
  ;; put offset
  tc-userarea-used dup tc-,
  r> over + to tc-userarea-used
  tc-create; tc-smudge
;

: tc-(userval-allot)  ( n -- )
  dup 0< ?abort" userallot must not be negative"
  dup tc-userarea-used + tc-#userarea u> ?abort" too many uservars"
  tc-userarea-used tc-get-ua-rva tc->real over erase
  tc-userarea-used + to tc-userarea-used
;


;; this a normal forth code; the magic is in its first word
: tc-(vocab-header-str)  ( addr count -- )
  tc-(create-header) tc-smudge tc-vocab
  tc-rva-doforth tc-compile-do-call
  ;; put vocab does
  ;; " (VOCAB-DOES-CODE)" tc-compile,-(str)
  tc-compile (VOCAB-DOES-CODE)
;
