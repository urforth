;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; target code writing for assembler
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

asmx86:asm-Labman-Reinit
false to asm-labman:lman-allow-forth-implicit


;; do not generate instrumented code (yet)
;; not that it matters, because we will replace "NEXT" anyway
0 to asmx86:asm-instrumented-next-addr


vocabulary metc-meta
also metc-meta definitions

;; used to get current assembling address
;; should return virtual address
: mc-PC  ( -- addr )
  elf-current-pc
;

;; size is never zero or negative
;; returns alloted addres (it will be used with `(asm-c!)`
;; should return virtual address
: mc-n-allot  ( size -- addr )
  dup 0< ?abort" negative tc allot"
  dup elf-target-memory-size u>= ?abort" trying to allocate too many tc bytes"
  elf-current-pc  ;; result
  swap over +     ;; end addr
  dup elf-base-rva elf-target-memory-size + u> ?abort" out of tc memory"
  to elf-current-pc
;

;; the only writing primitive
;; always writing to alloted address
;; accepts virtual address
: mc-c!  ( byte addr -- )
  tc->real forth:c!
;

;; the only reading primitive
;; always reading from alloted address
;; accepts virtual address
: mc-c@  ( addr -- byte )
  tc->real forth:c@
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous definitions

;; plug in our memory r/w module
: setup-asm-tc  ( -- )
  ['] metc-meta:mc-PC to asmx86:asm-PC
  ['] metc-meta:mc-n-allot to asmx86:asm-n-allot
  ['] metc-meta:mc-c@ to asmx86:asm-c@
  ['] metc-meta:mc-c! to asmx86:asm-c!
;
setup-asm-tc
