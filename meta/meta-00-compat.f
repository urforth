;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; compatibility with older builds
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[DEFINED] forth:(last-tload-path-addr) [IF]
: @tc-tload-last-include-dir-c4s  ( -- addr )  forth:(last-tload-path-addr) ;
: !tc-tload-last-include-dir-c4s  ( addr -- )  to forth:(last-tload-path-addr) ;
[ELSE]
: @tc-tload-last-include-dir-c4s  ( -- addr )  forth:tloader:last-include-dir-c4s ;
: !tc-tload-last-include-dir-c4s  ( addr -- )  to forth:tloader:last-include-dir-c4s ;
[ENDIF]


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[UNDEFINED] forth:parse-skip-to-eol [IF]
: parse-skip-to-eol  ( -- )
  ;; check last delimiter
  (tib-last-read-char) @ 10 = if exit endif
  begin
    tib-getch ?dup
  while
    ;; ( ch -- )
    10 = if exit endif
  repeat
;
[ENDIF]

[UNDEFINED] forth:skip-comment-multiline [IF]
;; multiline comment
;; (* .... *) -- opening eaten
: skip-comment-multiline  ( -- )
  begin
    tib-getch ?dup
  while
    ;; ( ch -- )
    [char] * = tib-peekch [char] ) = and if tib-getch drop exit endif
  repeat
;
[ENDIF]

[UNDEFINED] forth:skip-comment-multiline-nested [IF]
;; nested multiline comment
;; (( .... )) -- opening eaten
: skip-comment-multiline-nested  ( -- )
  1  ;; current comment level
  begin
    tib-getch ?dup
  while
    ;; ( ch -- )
    8 lshift tib-peekch or
    dup 0x2828 =  ;; ((?
    if
      drop tib-getch drop  1+
    else
      0x2929 =   ;; ))
      if
        tib-getch drop  1-
        ?dup ifnot exit endif
      endif
    endif
  repeat
  drop
;
[ENDIF]


[UNDEFINED] (tib-set-to) [IF]
: (tib-set-to)  ( addr count -- )
  #tib ! tib ! >in 0! bl (tib-last-read-char) !
;
[ENDIF]


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; os interface was moved to the separate vocabulary
[UNDEFINED] os [IF]
vocabulary os also os definitions
alias forth:sys-gettickcount gettickcount
alias forth:(fopen) open
alias forth:(fclose) close
alias forth:(fread) read
alias forth:(fwrite) write
alias forth:(lseek) lseek
alias forth:(seek-set) seek-set
alias forth:(seek-end) seek-end
alias forth:o-rdonly o-rdonly
alias forth:o-wronly o-wronly
alias forth:o-creat o-creat
alias forth:o-trunc o-trunc
alias forth:s-irwxu s-irwxu
alias forth:s-irgrp s-irgrp
alias forth:s-ixgrp s-ixgrp
alias forth:s-iroth s-iroth
alias forth:s-ixoth s-ixoth
alias forth:mmap mmap
alias forth:munmap munmap
alias forth:prot-r/w prot-r/w
previous definitions
[ENDIF]

[UNDEFINED] parse-name-ex [IF]
: parse-name-ex  ( -- addr count )  parse-skip-comments parse-name ;
[ENDIF]


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[UNDEFINED] enum{ [IF]
vocabulary (enums) (hidden)
also (enums) definitions

;; etype: 0 -- bitenum, otherwise delta

: value  ( etype evalue -- etype enextvalue )  \ name
  dup constant
  over ?dup if + else ?dup if 1 lshift else 1 endif endif
;

: }  ( etype evalue -- )
  2drop previous
;

: set      ( etype evalue newvalue -- etype newvalue )  nip ;
: set-bit  ( etype evalue newbit -- etype 1<<newbit )  nip 1 swap lshift ;
: -set     ( etype evalue delta -- etype evalue-delta ) - ;
: +set     ( etype evalue delta -- etype evalue+delta ) + ;

previous definitions

: enum{  ( -- etype enextvalue )  1 0 also (enums) ;
: bitenum{  ( -- etype enextvalue )  0 1 also (enums) ;
[ENDIF]
