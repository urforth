;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; basic on-the-fly optimiser
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
256 constant tc-#jstack
tc-#jstack cells buffer: tc-jstack
-1 value tc-jsp

: tc-jpush  ( n -- )
  tc-jsp dup -if 2drop
  else dup tc-#jstack < if tc-jstack cells^ ! 1 +to tc-jsp
  else 2drop -1 to tc-jsp  ;; overflow
    endcr ." WARNING: branch optimiser stack overflow in \`" tc-latest-nfa tc-id. ." \`\n"
  endif endif
;

: tc-jpop  ( -- n // -1 )
  tc-jsp 1- dup -if drop -1
  else dup to tc-jsp tc-jstack cells^ @ endif
;

: tc-jpush-here  ( -- )  tc-here tc-jpush ;

: tc-jpush-brn  ( -- )  tc-jpush-here 0 tc-jpush ;
: tc-jpush-xbrn  ( -- )  tc-jpush-here 1 tc-jpush ;

: tc-jstack-reset  ( -- )  -1 to tc-jsp ;

: tc-jstack-init  ( -- )  tc-opt-branches? if 0 else -1 endif to tc-jsp ;
: tc-jstack-subinit  ( -- )  tc-opt-branches? if tc-jsp 0 max to tc-jsp -1 tc-jpush endif ;


: tc-jstack-pop-frame  ( -- )
  tc-jsp +if begin tc-jpop 0>= while tc-jpop drop repeat else tc-jstack-reset endif
;


;; cfa: ( addr type -- stopcode )
: tc-jstack-foreach  ( cfa -- stopcode )
  tc-jsp 1- dup -if drop false
  else
    0 swap do
      i swap >r tc-jstack cells^ dup @ swap cell- @ swap
      dup -1 = if 2drop rdrop unloop false exit endif
      r@ execute ?dup if rdrop unloop exit endif
    r> -2 +loop drop false
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary tc-optimiser
also tc-optimiser definitions


;; replace alias call with referred address
: optimise-alias-call  ( rva-cfa -- true // cfa false )
  begin
    dup elf-base-rva u< if false exit endif  ;; this seems to be a forward reference
    ;; check for jmp
    dup tc-c@ 0xe9 = ifnot false exit endif
      \ endcr ." ALIAS?: " dup tc-cfa-wsize . ." <" dup tc-cfa->nfa tc->real id. ." >" cr
      \ endcr ."  start: 0x" dup .hex8 ."  end: 0x" dup tc-cfa->nfa tc-nfa->sfa tc-@ .hex8 cr
    ;; should be exactly 5 bytes
    dup tc-cfa-wsize tc-align-pfa if 8 else 5 endif
    = ifnot false exit endif
      \ endcr ." ALIAS: " dup tc-cfa->nfa tc->real id. ."  -> " dup 1+ tc-(DISP32@) tc-cfa->nfa tc->real id. cr
    ;; get new cfa address, and check if it is an alias too
    1+ tc-(disp32@)
  again
;

\ false value (was-jump-fix)
false value tc-(optimise-jumps-again)

: optimise-jumps  ( -- )
  tc-jsp +if
    begin
      false to tc-(optimise-jumps-again)
        \ false to (was-jump-fix)
        \ endcr ." === JUMPS ===\n"
      [:  ( addr type -- stopflag )
        ifnot
            \ endcr ." === checking JUMP at 0x" dup .hex8 ."  ===\n"
          [:  ( addr braddr type -- addr stopflag )
            >r 2dup cell+ tc-@ = if  ;; braddr jumps to addr, reroute it
                \ (was-jump-fix) ifnot endcr ." === checking JUMP at 0x" over .hex8 ."  ===\n" endif
                \ endcr ."   fixing " r@ if ." X" else space endif ." BRN at 0x" dup .hex8 cr
                \ true to (was-jump-fix)
              cell+ over cell+ tc-@  2dup swap tc-@ <> if  ( addr braddr+4 [addr+4] )
                swap tc-!  true to tc-(optimise-jumps-again)
              else 2drop endif
            else drop endif
            rdrop false
          ;] tc-jstack-foreach 2drop
        else drop endif
        false
      ;] tc-jstack-foreach drop
    tc-(optimise-jumps-again) not-until
      tc-jstack-pop-frame
      \ (was-jump-fix) if tc-latest-nfa tc-id. cr endif
  else tc-jstack-reset endif
;


previous definitions


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-COMPILE,  ( rva-cfa -- )
  tc-opt-aliases? if tc-optimiser:optimise-alias-call if exit endif endif tc-reladdr,
;


