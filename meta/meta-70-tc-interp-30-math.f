;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: +  forth:+ ;
: -  forth:- ;
: *  forth:* ;
: /  forth:/ ;
: */  forth:*/ ;
: MOD  forth:mod ;
: U*  forth:U* ;
: U/  forth:U/ ;
\ : U*/  forth:U*/ ;
: UMOD  forth:UMOD ;
: lshift  forth:lshift ;
: rshift  forth:rshift ;
: CELL  forth:cell ;
: CELLS  forth:cells ;
: CELLS^  forth:cells^ ;
: CELL+  forth:cell+ ;
: CELL-  forth:cell- ;
: +CELLS  forth:+cells ;
: -CELLS  forth:-cells ;
: OR  forth:or ;
: AND  forth:and ;
: ~AND  forth:~and ;
