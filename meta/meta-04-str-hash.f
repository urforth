;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word name hashing
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
code: TC-STR-NAME-HASH-ELF  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash <<= 4
  shl   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  add   eax,edx
  ;; high = hash&0xF0000000
  ld    edx,eax
  and   edx,0xf0000000
  ld    ebx,edx
  inc   edi
  ;; hash ^= high>>24
  rol   edx,8
  xor   eax,edx
  ;; hash &= ~high
  not   ebx   ;;xor   ebx,0xffffffff
  and   eax,ebx
  dec   ecx
  jr    nz,.hashloop
  ;;loop  .hashloop
.done:
  ld    TOS,eax
  urnext
endcode


code: TC-STR-NAME-HASH-JOAAT  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash += *key
  movzx edx,byte [edi]
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  add   eax,edx
  ;; hash += hash<<10
  ld    edx,eax
  shl   eax,10
  add   eax,edx
  inc   edi
  ;; hash ^= hash>>6
  ld    edx,eax
  shr   eax,6
  xor   eax,edx
  dec   ecx
  jr    nz,.hashloop
  ;;loop  .hashloop
  ;; final permutation
  ;; hash += hash<<3
  ld    edx,eax
  shl   eax,3
  add   eax,edx
  ;; hash ^= hash>>11
  ld    edx,eax
  shr   eax,11
  xor   eax,edx
  ;; hash += hash<<15
  ld    edx,eax
  shl   eax,15
  add   eax,edx
.done:
  ld    TOS,eax
  urnext
endcode


code: TC-STR-NAME-HASH-ROT  ( addr count -- u32hash )
  pop   edi
  xor   eax,eax
  jecxz .done
.hashloop:
  ;; hash = lrot(hash, 4)
  rol   eax,4
  ;; hash += *key
  movzx edx,byte [edi]
  inc   edi
  ;; upcase it (this also distorts other chars, but who cares)
  and   dl,0xdf
  xor   eax,edx
  ;;add   eax,edx
  dec   ecx
  jr    nz,.hashloop
  ;; hash ^= (hash>>10)^(hash>>20)
  ld    ecx,eax
  shr   ecx,10
  ld    edx,eax
  shr   edx,20
  xor   eax,ecx
  xor   eax,edx
.done:
  ld    TOS,eax
  urnext
endcode
