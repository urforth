;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; creating new vocabularies
;; note that i didn't bother to implement proper vocabulary stack (yet)
;; the FORTH vocabulary is always searched after CURRENT
;; also, hidden words are visible
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary structure
;;   dd latest
;;   dd voclink (voclink always points here)
;;   dd parent (if not zero, all parent words are visible)
;;   dd headernfa (can be zero)
;;   hashtable (if enabled)

;; ensure that "(voc-link)" is defined
: (ensure-voclink-defined)  ( -- voclink-pfa )
  tc-forth not-?abort" wut?! no FORTH vocabulary"
  " (VOC-LINK)" tc-forth tc-xcfind not-?abort" where is tc (voc-link)?"
  tc-cfa->pfa
; (hidden)


;; headernfa can be zero for anonymous
: tc-(NEW-WORDLIST-EX)  ( headernfa usehash -- vocid )
  >r    ;; save "usehash" flag
  (ensure-voclink-defined) >r
  tc-here            ;; this is vocid
  0 tc-,             ;; latest
  ;; fix target (voc-link)
  tc-here
  r@ tc-@ tc-,       ;; voclink  r@: tc-(VOC-LINK)
  r> tc-!            ;; r>: tc-(VOC-LINK)
  ;; done fixing target (voc-link)
  0 tc-,             ;; parent
  swap tc-reladdr,   ;; headernfa
  ;; words hashtable buckets
  forth-hashtable-bits 0< ?abort" wut?! hashtable is not initialized"
  forth-hashtable-bits if
    r> if  ;; create empty hashtable
      1 forth-hashtable-bits lshift cells dup tc-n-allot tc->real swap erase
    else -1 tc-, endif
  else rdrop -1 tc-, endif
; (hidden)


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; vocabulary support
: VOCABULARY-EX  ( usehash -- )
  parse-name tc-(vocab-header-str)
  ;; we will patch vocid here
  tc-here >r 0 tc-,
  tc-latest-nfa swap tc-(NEW-WORDLIST-EX) r> tc-!
  tc-create;
;

: VOCABULARY  ( -- )  true vocabulary-ex ;
: VOCABULARY-NOHASH  ( -- )  false vocabulary-ex ;


;; new vocabulary will see all definitions from CURRENT vocabulary
;; it has to be "_" due to parser specifics
: NESTED-VOCABULARY-EX  ( usehash -- )
  vocabulary-ex
  ;; set "parent"
  tc-current tc-@ tc-latest-cfa tc-voc-cfa->vocid tc-vocid->parent tc-!
;

: NESTED-VOCABULARY  ( -- )  true nested-vocabulary-ex ;
: NESTED-VOCABULARY-NOHASH  ( -- )  false nested-vocabulary-ex ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: VOC-SET-ACTIVE  ( -- )
  parse-name x-tc-xcfind-must
  dup tc-cfa->ffa tc-ffa@ tc-(wflag-vocab) and not-?abort" not a vocabulary"
  tc-voc-cfa->vocid tc-current tc-!
;
