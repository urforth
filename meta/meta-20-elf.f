;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; simple elf header creation, and writing binary elf file
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: (put-b)  ( addr value -- addr+1 )
  over c! 1+
;

: (put-w)  ( addr value -- addr+2 )
  over w! 2+
;

: (put-dw)  ( addr value -- addr+4 )
  over ! cell+
;

: (put-strz)  ( addr count -- addr+count+1 )
  dup 0> if
    over + swap do i c@ (put-b) loop
    0 (put-b)
  else
    drop
  endif
;


0 constant DT_NULL
1 constant DT_NEEDED
4 constant DT_HASH
5 constant DT_STRTAB
6 constant DT_SYMTAB
10 constant DT_STRSZ
11 constant DT_SYMENT
17 constant DT_REL
18 constant DT_RELSZ
19 constant DT_RELENT

4 4 + 4 + 1 + 1 + 2 + constant ELF32_SYM_SIZE
4 4 + constant ELF32_REL_SIZE

: build-elf-header  ( addr -- eaddr )
  ;; signature
  $7F (put-b)
  [char] E (put-b)
  [char] L (put-b)
  [char] F (put-b)

  1 (put-b)     ;; bitness
  1 (put-b)     ;; endianness
  1 (put-b)     ;; header version
  3 (put-b)     ;; abi

  8 0 do 0 (put-b) loop

  2 (put-w)     ;; e_type
  3 (put-w)     ;; e_machine

  1 (put-dw)    ;; e_version
  dup to elf-entry-point-addr
  0 (put-dw)    ;; e_entry
  $34 (put-dw)  ;; e_phoff
  0 (put-dw)    ;; e_shoff
  0 (put-dw)    ;; e_flags

  $34 (put-w)   ;; e_ehsize
  32 (put-w)    ;; e_phentsize
  tc-dynamic-binary if
    3 (put-w)     ;; e_phnum
  else
    2 (put-w)     ;; e_phnum
  endif
  40 (put-w)    ;; e_shentsize
  0 (put-w)     ;; e_shnum
  0 (put-w)     ;; e_shstrndx

  tc-dynamic-binary ifnot
    ;; first segment: executable code
    1 (put-dw)          ;; type
    $00000000 (put-dw)  ;; foffset
    elf-base-rva (put-dw)  ;; vaddr
    elf-base-rva (put-dw)  ;; shit
    dup to elf-code-size-addr
    $00000000 (put-dw)  ;; fsize
    tc-image-vsize (put-dw)  ;; msize
    $00000007 (put-dw)  ;; flags
    $00001000 (put-dw)  ;; align
  else
    ;; first segment: interpreter
    3 (put-dw)          ;; type
    $00000094 (put-dw)  ;; foffset
    elf-base-rva $094 + (put-dw)  ;; vaddr
    elf-base-rva $094 + (put-dw)  ;; shit
    $00000013 (put-dw)  ;; fsize
    $00000013 (put-dw)  ;; msize
    $00000007 (put-dw)  ;; flags  ;; was 4
    $00000001 (put-dw)  ;; align

    ;; second segment: dynamic imports
    2 (put-dw)          ;; type
    $000000A7 (put-dw)  ;; foffset
    elf-base-rva $0A7 + (put-dw)  ;; vaddr
    elf-base-rva $0A7 + (put-dw)  ;; shit
    $00000050 (put-dw)  ;; fsize
    $00000050 (put-dw)  ;; msize
    $00000007 (put-dw)  ;; flags  ;; was 4
    $00000001 (put-dw)  ;; align

    ;; third segment: executable code
    1 (put-dw)          ;; type
    $00000000 (put-dw)  ;; foffset
    elf-base-rva (put-dw)  ;; vaddr
    elf-base-rva (put-dw)  ;; shit
    dup to elf-code-size-addr
    $00000000 (put-dw)  ;; fsize
    tc-image-vsize (put-dw)  ;; msize
    $00000007 (put-dw)  ;; flags
    $00001000 (put-dw)  ;; align

    ;; first segment data: write interpreter string
    " /lib/ld-linux.so.2" (put-strz)

    ;; second segment data: write import table
    ;; the only thing we need is .so management functions, so we'll create
    ;; a very simple import table for "libdl.so", with 3 imports:
    ;; "dlopen", "dlclose", "dlsym"
    DT_NEEDED (put-dw)  1 (put-dw) ;; elfhead_str_libdl-elfhead_strtab
    DT_STRTAB (put-dw)  elf-base-rva $137 + (put-dw)  ;; elfhead_strtab
    DT_STRSZ (put-dw)   $0000001F (put-dw)  ;; elfhead_strsz
    DT_SYMTAB (put-dw)  elf-base-rva $0F7 + (put-dw)  ;; elfhead_symtab
    DT_SYMENT (put-dw)  ELF32_SYM_SIZE (put-dw)
    DT_REL (put-dw)     elf-base-rva $156 + (put-dw)  ;; elfhead_rel
    DT_RELSZ (put-dw)   $00000018 (put-dw)  ;; elfhead_relsz (put-dw)
    DT_RELENT (put-dw)  ELF32_REL_SIZE (put-dw)
    DT_HASH (put-dw)    elf-base-rva $16E + (put-dw)  ;; elfhead_hash (put-dw)
    DT_NULL (put-dw)    0 (put-dw)

    ;; here starts executable segment
    ;; we're putting rest of import table into it; this is prolly not right, but it works

    ;; import symbol table
    ;; NULL import, should always be here
      0 (put-dw)  ;; name
      0 (put-dw)  ;; value
      0 (put-dw)  ;; size
    $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
      0 (put-b)   ;; other
      0 (put-w)   ;; shndx
    ;; import "dlopen"
     10 (put-dw)  ;; name
      0 (put-dw)  ;; value
      0 (put-dw)  ;; size
    $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
      0 (put-b)   ;; other
      0 (put-w)   ;; shndx
    ;; import "dlclose"
     17 (put-dw)  ;; name
      0 (put-dw)  ;; value
      0 (put-dw)  ;; size
    $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
      0 (put-b)   ;; other
      0 (put-w)   ;; shndx
    ;; import "dlsym"
     25 (put-dw)  ;; name
      0 (put-dw)  ;; value
      0 (put-dw)  ;; size
    $12 (put-b)   ;; (STB_GLOBAL<<4)|STT_FUNC
      0 (put-b)   ;; other
      0 (put-w)   ;; shndx

    ;; string table
    0 (put-b)
    " libdl.so" (put-strz)
    " dlopen" (put-strz)
    " dlclose" (put-strz)
    " dlsym" (put-strz)

    ;; importer will use this to fix relocations
    ;; dlopen
    elf-base-rva $18A + (put-dw)  ;; offset to elfimp_dlopen
    $0101     (put-dw)  ;; high bit is symbol index, low bit is R_386_32
    ;; dlclose
    elf-base-rva $18E + (put-dw)  ;; offset to elfimp_dlclose
    $0201     (put-dw)  ;; high bit is symbol index, low bit is R_386_32
    ;; dlsym
    elf-base-rva $192 + (put-dw)  ;; offset to elfimp_dlsym
    $0301     (put-dw)  ;; high bit is symbol index, low bit is R_386_32

    ;; fake import hash table with one bucket
    1 (put-dw)  ;; bucket size
    4 (put-dw)  ;; chain size (including NULL import)
    0 (put-dw)  ;; fake bucket, just one hash value
    ;; hashtable bucket
    1 (put-dw)
    2 (put-dw)
    3 (put-dw)
    4 (put-dw)

    ;; loadef will add symbol offets to the following three dwords
    dup real->tc to elf-import-table-rva
    0 (put-dw)  ;; dlopen
    0 (put-dw)  ;; dlclose
    0 (put-dw)  ;; dlsym
    dup real->tc elf-import-table-rva - to elf-import-table-size
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: create-elf-tc-constants  ( -- )
  ;; create some assembler constants
  " urforth_code_base_addr" elf-base-rva asmx86:asm-Make-Constant
  " elfhead_codesize_addr" elf-code-size-addr real->tc asmx86:asm-Make-Constant

  tc-dynamic-binary if
    " elfhead_impstart" elf-import-table-rva asmx86:asm-Make-Constant
    " elfhead_implen" elf-import-table-size asmx86:asm-Make-Constant

    " elfimp_dlopen" elf-import-table-rva 0 +cells asmx86:asm-Make-Constant
    " elfimp_dlclose" elf-import-table-rva 1 +cells asmx86:asm-Make-Constant
    " elfimp_dlsym" elf-import-table-rva 2 +cells asmx86:asm-Make-Constant
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: elf-reserve-bss  ( size -- )
  65536 max [ 1024 1024 * 512 * ] literal min
  1- 4095 or 1+  curr-code-size 1- 4095 or 1+  +
  dup elf-code-size-addr cell+ !  ;; fix BSS size
  elf-base-rva + 1- " urforth_dict_lastaddr" rot asmx86:asm-Make-Label
  " urforth_dict_protected" elf-current-pc asmx86:asm-Make-Label
;


: save-elf-binary  ( addr count -- )
  endcr ." code size: " curr-code-size . ." bytes\n"

  curr-code-size elf-code-size-addr !
  \ do not fix virtual size
  \ curr-code-size elf-code-size-addr cell+ !

  ;; create output file
  [ os:o-wronly os:o-creat or os:o-trunc or ] literal  ;; flags
  [ os:s-irwxu os:s-irgrp or os:s-ixgrp or os:s-iroth or os:s-ixoth or ] literal  ;; mode
  os:open
  ;; check success
  dup 0< ?abort" cannot create output file"
  >r
  elf-target-memory curr-code-size r@ os:write
  curr-code-size <> ?abort" error writing file"
  r> os:close ?abort" error closing file"
;
