;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some utilities to move from CFA to other fields, and word flag control
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

bitenum{
  value tc-(WFLAG-IMMEDIATE)
  value tc-(WFLAG-SMUDGE)
  value tc-(WFLAG-NORETURN)
  value tc-(WFLAG-HIDDEN)
  value tc-(WFLAG-CODEBLOCK)
  value tc-(WFLAG-VOCAB)
  value tc-(WFLAG-SCOLON)
  value tc-(WFLAG-MACRO)
}

enum{
  value tc-(WARG-NONE)
  value tc-(WARG-BRANCH)
  value tc-(WARG-LIT)
  value tc-(WARG-C4STRZ)
  value tc-(WARG-CFA)
  value tc-(WARG-CBLOCK)
  value tc-(WARG-VOCID)
  value tc-(WARG-C1STRZ)
  value tc-(WARG-U8)
  value tc-(WARG-S8)
  value tc-(WARG-U16)
  value tc-(WARG-S16)
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; create asm macros (to avoid code duplication and sync problems)
" FLAG_IMMEDIATE"  tc-(WFLAG-IMMEDIATE) asmx86:asm-Make-Constant
" FLAG_SMUDGE"     tc-(WFLAG-SMUDGE) asmx86:asm-Make-Constant
" FLAG_NORETURN"   tc-(WFLAG-NORETURN) asmx86:asm-Make-Constant
" FLAG_HIDDEN"     tc-(WFLAG-HIDDEN) asmx86:asm-Make-Constant
" FLAG_CODEBLOCK"  tc-(WFLAG-CODEBLOCK) asmx86:asm-Make-Constant
" FLAG_VOCAB"      tc-(WFLAG-VOCAB) asmx86:asm-Make-Constant
" FLAG_SCOLON"     tc-(WFLAG-SCOLON) asmx86:asm-Make-Constant
" FLAG_MACRO"      tc-(WFLAG-MACRO) asmx86:asm-Make-Constant

" WARG_NONE"    tc-(WARG-NONE) asmx86:asm-Make-Constant
" WARG_BRANCH"  tc-(WARG-BRANCH) asmx86:asm-Make-Constant
" WARG_LIT"     tc-(WARG-LIT) asmx86:asm-Make-Constant
" WARG_C4STRZ"  tc-(WARG-C4STRZ) asmx86:asm-Make-Constant
" WARG_CFA"     tc-(WARG-CFA) asmx86:asm-Make-Constant
" WARG_CBLOCK"  tc-(WARG-CBLOCK) asmx86:asm-Make-Constant
" WARG_VOCID"   tc-(WARG-VOCID) asmx86:asm-Make-Constant
" WARG_C1STRZ"  tc-(WARG-C1STRZ) asmx86:asm-Make-Constant
" WARG_U8"      tc-(WARG-U8) asmx86:asm-Make-Constant
" WARG_S8"      tc-(WARG-S8) asmx86:asm-Make-Constant
" WARG_U16"     tc-(WARG-U16) asmx86:asm-Make-Constant
" WARG_S16"     tc-(WARG-S16) asmx86:asm-Make-Constant


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; to debugptr field
: tc-NFA->DFA  ( nfa -- dfa )
  20 -
;

;; to word size field
: tc-NFA->SFA  ( nfa -- sfa )
  16 -
;

;; to bucketptr field
: tc-NFA->BFA  ( nfa -- bfa )
  12 -
;

;; to word size field
: tc-NFA->LFA  ( nfa -- sfa )
  8 -
;

;; to name hash field
: tc NFA->HFA  ( nfa -- hfa )
  4-
;

;; argument type area; 1 byte
: tc-NFA->TFA  ( nfa -- tfa )
  1+
;

;; flag fields area; 2 bytes
: tc-NFA->FFA  ( nfa -- ffa )
  2+
;

: tc-NFA->CFA  ( nfa -- cfa )
  dup tc-c@ +
  tc-align-cfa if 4+ 3 or 1+ else 5 + endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-LFA->NFA  ( nfa -- sfa )
  8 +
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-CFA->PFA  ( cfa -- pfa )
  tc-align-pfa if 8 else 5 endif +  ;; skip CALL
;

: tc-PFA->CFA  ( pfa -- cfa )
  tc-align-pfa if 8 else 5 endif -  ;; undo skip CALL
;

: tc-CFA->NFA  ( cfa -- nfa )
  1- dup tc-c@ - 4-
;

: tc-CFA->FFA  ( cfa -- nfa )
  tc-cfa->nfa tc-nfa->ffa
;

: tc-CFA->LFA  ( cfa -- lfa )
  tc-cfa->nfa tc-nfa->lfa
;

: tc-LFA->CFA  ( lfa -- cfa )
  tc-lfa->nfa tc-nfa->cfa
;

: tc-NFA-COUNT  ( nfa -- addr count )
  dup tc-c@ swap 4+ swap
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-BFA->NFA  ( bfa -- nfa )
  12 +
;

: tc-BFA->FFA  ( bfa -- ffa )
  14 +
;

: tc-BFA->LFA  ( bfa -- lfa )
  4+
;

: tc-BFA->HFA  ( bfa -- hfa )
  8 +
;

: tc-HFA->BFA  ( bfa -- hfa )
  8 -
;

;; calculate word size from cfa
: tc-CFA-WSIZE  ( rva-cfa -- size )
  dup tc-cfa->nfa tc-nfa->sfa tc-@ swap -
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-FFA@  ( ffa -- ffa-value )
  tc-w@
;

: tc-TFA@  ( tfa -- tfa-value )
  tc-c@
;

: tc-FFA!  ( ffa-value ffa -- )
  swap 0xffff and swap tc-w!
;

: tc-TFA!  ( tfa-value tfa -- )
  swap 0xff and swap tc-c!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-cfa-immediate?  ( cfa -- flag )
  tc-cfa->ffa tc-ffa@ tc-(wflag-immediate) and notnot
;

: tc-cfa-hidden?  ( cfa -- flag )
  tc-cfa->ffa tc-ffa@ tc-(wflag-hidden) and notnot
;

: tc-cfa-vocab?  ( cfa -- flag )
  tc-cfa->ffa tc-ffa@ tc-(wflag-vocab) and notnot
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-voc-cfa->vocid  ( rva-cfa -- rva-vocid ) tc-cfa->pfa cell+ tc-@ ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-latest-lfa  ( -- lfa )  tc-current tc-@ tc-@ ;
: tc-latest-nfa  ( -- nfa )  tc-latest-lfa tc-lfa->nfa ;
: tc-latest-cfa  ( -- cfa )  tc-latest-lfa tc-lfa->cfa ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word [addr] ^= value
: tc-wtoggle  ( addr value -- )
  over tc-w@ xor 0xffff and swap tc-w!
;

: tc-(toggle-latest-wflag)  ( flag -- )
  tc-latest-nfa tc-nfa->ffa swap tc-wtoggle
;

: tc-(reset-latest-wflag)  ( flag -- )
  tc-latest-nfa tc-nfa->ffa
  dup tc-w@
  ;; ( flag addr oldflg )
  rot bitnot and swap tc-w!
;

: tc-(set-latest-wflag)  ( flag -- )
  tc-latest-nfa tc-nfa->ffa
  dup tc-w@
  ;; ( flag addr oldflg )
  rot or swap tc-w!
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(noop)  ( -- )
  tc-latest-nfa tc-id-count swap tc->real swap
  get-current >r vocid: tc-imm-noops set-current create-named 1 , forth:create; r> set-current
;

: tc-smudge     ( -- )  tc-(wflag-smudge) tc-(toggle-latest-wflag) ;
: tc-immediate  ( -- )  tc-(wflag-immediate) tc-(toggle-latest-wflag) ;
: tc-hidden     ( -- )  tc-(wflag-hidden) tc-(set-latest-wflag) ;
: tc-public     ( -- )  tc-(wflag-hidden) tc-(reset-latest-wflag) ;
: tc-noreturn   ( -- )  tc-(wflag-noreturn) tc-(set-latest-wflag) ;
: tc-codeblock  ( -- )  tc-(wflag-codeblock) tc-(set-latest-wflag) ;
: tc-vocab      ( -- )  tc-(wflag-vocab) tc-(set-latest-wflag) ;

: tc-set-scolon   ( -- )  tc-(wflag-scolon) tc-(set-latest-wflag) ;
: tc-cfa-scolon?  ( tccfa -- flag )  tc-cfa->nfa tc-nfa->ffa tc-w@ tc-(wflag-scolon) and notnot ;

: tc-set-macro      ( -- )  tc-(wflag-macro) tc-(set-latest-wflag) ;
: tc-reset-macro    ( -- )  tc-(wflag-macro) tc-(reset-latest-wflag) ;
: tc-latest-macro?  ( -- flag )  tc-latest-nfa tc-nfa->ffa tc-w@ tc-(wflag-macro) and notnot ;

: tc-arg-none    ( -- )  tc-(WARG-NONE) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-branch  ( -- )  tc-(WARG-BRANCH) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-lit     ( -- )  tc-(WARG-LIT) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-c4strz  ( -- )  tc-(WARG-C4STRZ) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-cfa     ( -- )  tc-(WARG-CFA) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-cblock  ( -- )  tc-(WARG-CBLOCK) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-vocid   ( -- )  tc-(WARG-VOCID) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-c1strz  ( -- )  tc-(WARG-C1STRZ) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-u8      ( -- )  tc-(WARG-U8) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-s8      ( -- )  tc-(WARG-S8) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-u16     ( -- )  tc-(WARG-U16) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
: tc-arg-s16     ( -- )  tc-(WARG-S16) tc-latest-nfa tc-nfa->tfa tc-tfa! ;
