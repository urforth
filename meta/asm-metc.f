;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; metacompiling support
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

false to tload-verbose

" meta-00-compat.f" tload
asmx86:asm-Labman-Reinit


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
only forth definitions
vocabulary ur-meta
also ur-meta definitions

enum{
  value tc-LINUX/X86
  value tc-WIN32
}

tc-LINUX/X86 value tc-os

;; 0: don't change case; 1: upcase; -1: locase
0 value tc-create-case?

;; align headers to dword?
true value tc-align-headers
true value tc-align-cfa
true value tc-align-pfa

(*
false to tc-align-cfa
false to tc-align-pfa
*)

0 value tc-align-headers-wasted
0 value tc-align-cfa-wasted
0 value tc-align-pfa-wasted

;; set to `false` to create static ELF (`dlopen` will not be included)
true value tc-dynamic-binary

;; set to `false` to omit debugger (faster, but no debugger at all)
;; disable interactive debugger, i don't need it. the system is faster this way.
false value tc-debugger-enabled
;; but enable debug info for backtraces
true value tc-debug-info-enabled

false value tc-verbose

enum{
  value tc-nhash-elf    ;; use elf hash
  value tc-nhash-joaat  ;; use Bob Jenkins' One-At-A-Time for hashing word names?
  value tc-nhash-rot    ;; use rotating hash
}
\ tc-nhash-rot value tc-wordhash-type
tc-nhash-joaat value tc-wordhash-type

enum{
  value tc-tls-none
  value tc-tls-fs
}
tc-tls-fs value tc-tls-type

;; only words with headers
0 value tc-created-words-count

240 value tc-max-word-name-length

;; optimise chain of branches?
;; i.e. branch to branch to branch
true value tc-opt-branches?

;; set to `true` to enable optimising of aliases
;; please note that alias optimiser cannot optimise forward references (yet)
false value tc-opt-aliases?

;; 4096 constant tc-#userarea
1024 constant tc-#userarea
0 value tc-userarea-used

;; total image size; as all so-called "modern" distros are broken, we have to reserve it in ELF header
1024 1024 * 2 * value tc-image-vsize


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; some debug toops

;; dump Forth forward references?
false value tc-dump-forwards?


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
" meta-01-helper-low.f" tload

0 value prev-include-buf

: tc-refill  ( -- flag )
  prev-include-buf ifnot false exit endif
  prev-include-buf dup @ to prev-include-buf
  cell+ tibstate-restore
  ;; restore file name
  forth:(tib-curr-fname) cell- @ to forth:(tib-curr-fname)
  ;; restore tload path
  @tc-tload-last-include-dir-c4s cell- @ !tc-tload-last-include-dir-c4s
  \ ." resuming: " forth:(tib-curr-fname) count type cr
  true
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-token-check-comments  ( -- againflag )
  false
  tib-peekch dup 32 <= if
    ;; single-char comments
    drop
    asmx86:lexer:tkvalue case
      [char] ( of  ;; )
        [char] ) parse 2drop
        drop true
      endof
      [char] \ of
        parse-skip-to-eol
        drop true
      endof
    endcase
  else
    ;; multi-char comments
    8 lshift asmx86:lexer:tkvalue or
    case
      0x2828 of  ;; ((
        skip-comment-multiline-nested
        drop true
      endof
      0x2A28 of  ;; (*
        skip-comment-multiline
        drop true
      endof
      0x2F2F of  ;; //
        parse-skip-to-eol
        drop true
      endof
    endcase
  endif
;

: tc-fix-id  ( addr count -- )
  asmx86:lexer:tkvalue c4s:copy-counted
;

: tc-check-special-token  ( -- )
  \ asmx86:tk-id? ifnot exit endif
  asmx86:lexer:tkvalue count
  2dup s" CODE" s= if
    2drop
    tib-peekch [char] : = not-?abort" `:` expected"
    tib-getch drop
    ;; the next one should be blank
    tib-peekch 32 <= not-?abort" blank char expected"
    tib-getch drop
    " CODE:" tc-fix-id
    exit
  endif
  2dup s" END" s= if
    2drop
    tib-peekch [char] - = ifnot exit endif
    tib-getch drop
    tib-peekch upcase-char [char] C = not-?abort" end-code expected"
    asmx86:lexer:NextToken
    asmx86:tk-id? not-?abort" end-code expected"
    asmx86:lexer:tkvalue count " CODE" s= not-?abort" end-code expected"
    " ENDCODE" tc-fix-id
  endif
  2drop
;

;; this skips more comments
: tc-next-token-noread  ( -- )
  begin
    ;; check for EOL
    asmx86:lexer:tktype ifnot
      ;; EOL
      tib-peekch ?dup if
        10 = if tib-getch drop endif
      else
        tc-refill ifnot break endif
      endif
      asmx86:lexer:PrepareLineParser
      asmx86:lexer:NextToken
      continue
    endif
    ;; check for multiline comments
    asmx86:tk-delim? if
      tc-token-check-comments if
        asmx86:lexer:PrepareLineParser
        asmx86:lexer:NextToken
        continue
      endif
    else
      asmx86:tk-id? if tc-check-special-token endif
    endif
    break
  again
    \ endcr asmx86:lexer:(dump-current-token) cr
;

: tc-next-token  ( -- )
  asmx86:lexer:NextToken
  tc-next-token-noread
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (file-error)  ( addr count msgaddr msgcount -- )
  endcr type space 34 emit type 34 emit space forth:error-line. cr
  \ abort
  1 n-bye
;

;; please, don't write such huge words!
: include-file  ( addr count -- )
  dup not-?abort" cannot include nothing"
  ;; save tib state
  #tib-save-buffer cell+ brk-alloc
  prev-include-buf over ! to prev-include-buf
  prev-include-buf cell+ tibstate-save
  ;; prepend current path (remember the original address, tho)
  2dup @tc-tload-last-include-dir-c4s count pad 1024 + c4s:copy-counted pad 1024 + c4s:cat-counted
  pad 1024 + count os:o-rdonly 0 os:open
  dup 0< if drop " cannot open file" (file-error) endif
  >r
  ;; ( addr count | fd )
  0 os:seek-end r@ os:lseek
  ;; ( addr count size | fd )
  dup 0< if
    drop r> os:close drop
    " cannot get size of file" (file-error)
  endif
  ;; seek back to file start
  ;; ( addr count size | fd )
  0 os:seek-set r@ os:lseek
  if
    drop r> os:close drop
    " cannot rewind file" (file-error)
  endif
  ;; allocate temp pool space
  \ TODO: free memory somehow
  ;; ( addr count size | fd )
  dup brk-alloc
  ;; ( addr count size bufaddr | fd )
  ;; load file
  2dup swap r@ os:read
  ;; ( addr count size bufaddr readbytes | fd )
  rot over = ifnot
    ;; ( addr count bufaddr readbytes | fd )
    drop 2drop r> os:close drop
    " cannot get size of file" (file-error)
  endif
  ;; close file
  r> os:close drop
  ;; ( addr count bufaddr readbytes )
  (tib-set-to)
  tib-line# 1!
  [DEFINED] (tib-last-read-char) [IF]
    bl (tib-last-read-char) !
  [ENDIF]
  ;; replace current file name (for error reporting)
  dup 2 +cells brk-alloc  ;; ( addr count namebuffaddr )
  forth:(tib-curr-fname) over ! cell+
  to forth:(tib-curr-fname)
  2dup forth:(tib-curr-fname) c4s:copy-counted
  ;; show message, and go with the new tib
  tc-verbose if endcr ." processing: " type cr else 2drop endif
  ;; replace current tload path (for relative loads)
  pad 1024 + count dup 2 +cells brk-alloc
  ;;  ( addr count newbuf )
  @tc-tload-last-include-dir-c4s over ! cell+
  dup !tc-tload-last-include-dir-c4s
  c4s:copy-counted
  ;; remove file name from the path
  @tc-tload-last-include-dir-c4s dup count str-extract-path nip swap !
  ;; done with bookkeeping
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
" meta-02-mem.f" tload
" meta-04-str-hash.f" tload
" meta-10-asm-tc.f" tload
" meta-14-asm-defconsts.f" tload
" meta-20-elf.f" tload

;; for "immediate (noop)" words; just a list
vocabulary tc-imm-noops

" meta-40-tc-compiler-00-dbginfo.f" tload
" meta-40-tc-compiler-04-low.f" tload
" meta-40-tc-compiler-05-cfa-cvt.f" tload
" meta-40-tc-compiler-07-opt-basic.f" tload
" meta-40-tc-compiler-06-name-hash.f" tload
" meta-40-tc-compiler-10-word-utils.f" tload
" meta-40-tc-compiler-15-dbginfo.f" tload
" meta-40-tc-compiler-20-create.f" tload
" meta-40-tc-compiler-30-mid.f" tload

;; later, we will create "tc-immediates" vocabulary, and populate it with words from "tc-immediates-src"
;; this is to avoid prepending "tc-" to immediate words (it is faster this way)
nested-vocabulary tc-immediates-src
also tc-immediates-src definitions

" meta-50-tc-imm-00-if-begin-do.f" tload
" meta-50-tc-imm-10-colon-var.f" tload
" meta-50-tc-imm-20-compile-tick.f" tload
" meta-50-tc-imm-30-cond-comp.f" tload
" meta-50-tc-imm-40-asm-misc.f" tload

previous definitions

;; this creates "tc-immediates" vocabulary, and populates it
" meta-50-tc-imm-99-populate.f" tload

\ also tc-immediates words previous


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
also asmx86 definitions
nested-vocabulary tc-instrs
also tc-instrs definitions
<public-words>

" meta-60-asm-commands.f" tload
" meta-60-asm-commands-macros.f" tload

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
previous previous definitions

;; now put our instructions into asmx86:instructions vocabulary
vocid-of asmx86:tc-instrs asmx86:Register-Macros-From-VocId


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
" meta-70-tc-interp-00-internal.f" tload
nested-vocabulary tc-interp-words
also tc-interp-words definitions
" meta-70-tc-interp-05-vocab.f" tload
" meta-70-tc-interp-10-comp-flags.f" tload
" meta-70-tc-interp-20-mem.f" tload
" meta-70-tc-interp-30-math.f" tload
previous definitions


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-find-interp  ( addr count -- addr count false // cfa true )
  vocid: tc-interp-words voc-search
;

: tc-find-imm  ( addr count -- addr count false // cfa true )
  vocid: tc-immediates voc-search
;

: tc-is-allowed-instr  ( addr count -- addr count flag )
  over c@ [char] $ = if true exit endif
  dup 2 = if
    2dup " DB" s=ci if true exit endif
    2dup " DW" s=ci if true exit endif
    2dup " DD" s=ci if true exit endif
    2dup " RB" s=ci if true exit endif
    2dup " RW" s=ci if true exit endif
    2dup " RD" s=ci if true exit endif
  endif
  false
;

: tc-find-tc-instr  ( addr count -- addr count false // cfa true )
  tc-is-allowed-instr ifnot false exit endif
  vocid: asmx86:instructions voc-search
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(code-word)  ( -- )
  ;; we won't create debug info for code words (yet)
  tc-(dbginfo-reset)
  tc-create-header
  parse-skip-to-eol
  asmx86:lexer:PrepareLineParser
  tc-next-token
  begin
    asmx86:tk-id? if
        \ lexer:tkvalue count endcr type cr
      asmx86:lexer:tkvalue count " ENDCODE" s= if break endif
    endif
    asmx86:(asm-tib) (sp-check) ifnot error-line. cr dbg endif
    tc-next-token-noread
    asmx86:lexer:tktype ifnot asmx86:ERRID_ASM_ENDCODE_EXPECTED asmx86:not-?asm-error endif
  again
  tc-create;
  ;; latest disasm-word
  asmx86:asm-Check-Undef-Labels
  tc-smudge
;

1024 constant #asm-line-buf
#asm-line-buf 2+ brk-buffer: asm-line-buf

: tc-collect-asm-line  ( -- addr count )
  0  ;; length
  begin
    tib-getch dup 10 <>
  while
    over asm-line-buf + c!
    1+ dup #asm-line-buf >= ?abort" asm line too long"
  repeat
  drop
  asm-line-buf swap
  2dup + 0c!
;

: tc-(code-line)  ( -- )
  tc-collect-asm-line
  tibstate>r
  (tib-set-to)
  ;; we won't create debug info for code words (yet)
  tc-(dbginfo-reset)
  asmx86:lexer:PrepareLineParser
  tc-next-token
  begin
    asmx86:tk-eol?
  not-while
    asmx86:(asm-tib) (sp-check) ifnot error-line. cr dbg endif
    tc-next-token-noread
  repeat
  ;; latest disasm-word
  asmx86:asm-Check-Undef-Labels
  r>tibstate
;


;; also, skips it
: next-equ?  ( -- flag )
  tibstate>r
  parse-name
  r>tibstate
  " EQU" s=ci if
    parse-name 2drop true
  else
    false
  endif
;

: tc-(equ) ( addr count -- )
  \ pad c4c-copy-a-c  ;; save constant name
  asmx86:lexer:PrepareLineParser
  tc-next-token
  asmx86:Reset-Instruction
  4 to asmx86:*OpSize
  ;; copy name to `*OffName`, it is not used by `Imm`
  asmx86:*OffName c1s:copy-counted
  asmx86:Imm
  ;; check if it is using only defined labels
  asmx86:*OpReloc if
    asmx86:*ImmForthType asmx86:ERRID_ASM_INVALID_FORWARD_REF asmx86:?asm-error
    asmx86:*ImmName c@ asmx86:ERRID_ASM_INVALID_FORWARD_REF asmx86:not-?asm-error
    \ *ImmName bcount asm-Label-Defined? ERRID_ASM_INVALID_FORWARD_REF not-?asm-error
    asmx86:*ImmLabelDefined asmx86:ERRID_ASM_INVALID_FORWARD_REF asmx86:not-?asm-error
  endif
  \ ." |" *OffName bcount type ." | " *Imm . cr
  asmx86:*OffName bcount asmx86:*Imm asmx86:asm-Make-Constant
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-interp-find-tc-word  ( addr count -- rva-cfa flags true // false )
  x-tc-xcfind dup if over tc-cfa->nfa tc-nfa->ffa tc-ffa@ swap endif
;

: tc-interp-find-tc-word-no-imm  ( addr count -- rva-cfa true // false )
  2dup tc-interp-find-tc-word if
    tc-(wflag-immediate) and if drop
      ;; ignore (noop) words
      vocid: tc-imm-noops voc-search ifnot
        endcr ." ERROR: \`" type ." \` is immediate!\n"
        abort" no TC immediate words allowed"
      else drop 0 true endif  ;; 0 cfa won't be compiled
    else nrot 2drop true endif
  else 2drop false endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main metacompiler loop
: format  ( -- )
  parse-name " ELF" s=ci not-?abort" not an elf?"
  parse-name " executable" s=ci not-?abort" not an elf executable?"
  ;; parse-name s=ci " 3" not-?abort" not an x86 abi?"
  parse-skip-to-eol
  1 to asmx86:asm-Labman-Unresolved-As-Forwards?
  begin
      (sp-check) ifnot error-line. cr dbg endif
    \ tc-state @ ifnot break endif
    begin
      parse-name-ex  ;; ( addr count )
    ?dup not-while
      drop tc-refill ifnot
        tc-state @ ?abort" definition not finished"
        0 0 break
      endif
    repeat
    dup ifnot break endif
    ;; ( addr count )
      \ endcr ." TC: " 2dup type cr
    ;; here we'll do some hackery: for compiling mode, try "tc-immediates" vocab first
    tc-find-imm if execute continue endif
    tc-state @ if
      ;; compiling, try to find a tc word
      2dup tc-interp-find-tc-word-no-imm if  ;; ( addr count rva-cfa )
        ;; i found her!
        nrot 2drop  ?dup if tc-compile, endif  ;; compile it to the target area
      else
        ;; unknown word, try to parse it as a number
        2dup number if
          nrot 2drop
          tc-literal
        else
          ;; this should be a forward reference
          tc-dump-forwards? if
            ;; this seems to be a forward reference
            endcr ." forwardref to \`" 2dup type ." \`"
            [DEFINED] forth:(tib-fname>error-fname) [IF] forth:(tib-fname>error-fname) [ENDIF]
            ERROR-LINE.
          endif
          tc-compile,-(str)-nochecks
        endif
      endif
    else
      ;; interpreting
        \ endcr ." |" 2dup type ." | " tib-line# @ . cr
      tc-find-interp if execute continue endif
      tc-find-tc-instr if
          \ dup cfa->nfa id. cr
          \ tibstate>r parse-name r>tibstate endcr ." 000:<" type ." >\n"
        asmx86:lexer:PrepareLineParser
        tc-next-token
        execute
          \ tibstate>r parse-name r>tibstate endcr ." 001:<" type ." >\n"
        continue
      endif
      2dup " code:" s=ci if 2drop tc-(code-word) continue endif
      2dup " $asm" s=ci if 2drop tc-(code-line) continue endif
      ;; HACK! check if the next word is equ
      next-equ? if tc-(equ) continue endif
      ;; try to find a tc word -- this can be a constant, for example
      2dup tc-interp-find-tc-word-no-imm if
             dup tc-is-constant? if nrot 2drop tc-cfa->pfa tc-@
        else dup tc-is-value? if nrot 2drop tc-cfa->pfa tc-@
        else dup tc-is-variable? if nrot 2drop tc-cfa->pfa
        else drop endcr ." ERROR: uninterpretable tc word \`" type ." \`" abort" wut?!"
        endif endif endif
      else
        ;; try to parse it as a number
        2dup number ifnot endcr ." ERROR: unknown tc word \`" type ." \`" abort" wut?!" endif
        nrot 2drop
      endif
    endif
  again
  2drop
  asmx86:asm-Check-Undef-Labels
;

;;previous


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: brk-alloc-c4str  ( addr count -- newaddr )
  dup 0< ERR-OUT-OF-MEMORY ?error
  dup cell+ 1+ brk-alloc  ;; ( addr count newaddr )
  dup >r
  2dup ! cell+            ;; ( addr count newaddr+4 | newaddr )
  2dup + 0c!
  swap 0 max cmove
  r>
;


0 value input-file-name
0 value output-file-name


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vocabulary cliargs
also cliargs definitions

: (str-arg)  ( -- addr count true // false )
  cli-arg-next argc < ifnot false exit endif
  cli-arg-next argv-str cli-arg-skip true
; (hidden)

: --help  ( -- )
  ." known metacompiler cli args:\n"
  vocid: cliargs [: ( nfa -- stopflag )
    dup nfa->cfa cfa-hidden? ifnot
      id-count pad c4s:copy-counted
      pad count locase-str
      2 spaces pad count type cr
    else drop endif
    false
  ;] foreach-word drop
  bye
;

: --dump-forwards  ( -- ) true to tc-dump-forwards? ;

: --verbose  ( -- )  true to tc-verbose ;
: --no-verbose  ( -- )  false to tc-verbose ;
: --quiet  ( -- )  false to tc-verbose ;

: --tls-none  ( -- )  tc-tls-none to tc-tls-type ;
: --tls-fs  ( -- )  tc-tls-fs to tc-tls-type ;

: --static  ( -- ) false to tc-dynamic-binary ;
: --dynamic  ( -- ) true to tc-dynamic-binary ;

: --align  ( -- ) true to tc-align-headers true to tc-align-cfa true to tc-align-pfa ;
: --no-align  ( -- ) false to tc-align-headers false to tc-align-cfa false to tc-align-pfa ;

: --align-headers  ( -- ) true to tc-align-headers ;
: --no-align-headers  ( -- ) false to tc-align-headers ;

: --align-cfa  ( -- ) true to tc-align-cfa ;
: --no-align-cfa  ( -- ) false to tc-align-cfa ;

: --align-pfa  ( -- ) true to tc-align-pfa ;
: --no-align-pfa  ( -- ) false to tc-align-pfa ;

: --debug  ( -- ) true to tc-debugger-enabled ;
: --no-debug  ( -- ) false to tc-debugger-enabled ;

: --debug-info  ( -- ) true to tc-debug-info-enabled ;
: --no-debug-info  ( -- ) false to tc-debug-info-enabled ;

: --opt-alias  ( -- ) true to tc-opt-aliases? ;
: --no-opt-alias  ( -- ) false to tc-opt-aliases? ;

: --opt-branch  ( -- ) true to tc-opt-branches? ;
: --no-opt-branch  ( -- ) false to tc-opt-branches? ;

: --whash  ( -- )
  (str-arg) not-?abort" '--whash' requires hash name"
  2dup " elf" s=ci if 2drop tc-nhash-elf to tc-wordhash-type exit endif
  2dup " joaat" s=ci if 2drop tc-nhash-joaat to tc-wordhash-type exit endif
  2dup " rot" s=ci if 2drop tc-nhash-rot to tc-wordhash-type exit endif
  endcr ." \`" type ." \` doesn't look like a known hash name!\n"
  1 n-bye
;

: -i  ( -- )
  (str-arg) not-?abort" '-i' requires input file name"
  brk-alloc-c4str to input-file-name
;

: -o  ( -- )
  (str-arg) not-?abort" '-o' requires input file name"
  brk-alloc-c4str to output-file-name
;

previous definitions


: parse-cli-args ( -- )
  begin cli-arg-next argc < while
    cli-arg-next argv-str
    vocid: cliargs voc-search if cli-arg-skip execute
    else endcr ." ERROR: invalid command line argument: \`" cli-arg-next argv-str type ." \`!\n" 1 n-bye
    endif
  repeat
  ;; set default names
  input-file-name ifnot " ../level1/urforth.f" brk-alloc-c4str to input-file-name endif
  output-file-name ifnot " urforth" brk-alloc-c4str to output-file-name endif
  ;; aligned PFA must be used with aligned CFA
  tc-align-pfa if true to tc-align-cfa endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
parse-cli-args

endcr ." compiling to \`" output-file-name count type ." \`...\n"

;; create ELF header
elf-target-memory build-elf-header elf-target-memory - elf-base-rva + to elf-current-pc

previous definitions
alias ur-meta:format format

ur-meta:tc-tls-type ur-meta:tc-tls-fs <> to asmx86:asm-ignore-ts

ur-meta:create-elf-tc-constants
ur-meta:tc-define-config-constants

os:gettickcount
ur-meta:input-file-name count tload

;; set user area size label
" ur_userarea_default_size" ur-meta:tc-userarea-used asmx86:asm-Make-Label
;; fix dictionary end address
ur-meta:tc-image-vsize ur-meta:elf-reserve-bss


\ asmx86:asm-Dump-Labels
asmx86:asm-Check-Undef-Labels-Final

os:gettickcount swap - endcr ." build time: " . ." msecs, "
ur-meta:tc-created-words-count . ." words defined, "
ur-meta:tc-userarea-used . ." bytes of user area allocated.\n"
[: ur-meta:tc-align-headers-wasted ?dup if . ." bytes wasted on aligned headers.\n" endif ;] execute
[: ur-meta:tc-align-cfa-wasted ?dup if . ." bytes wasted on aligned CFA.\n" endif ;] execute
[: ur-meta:tc-align-pfa-wasted ?dup if . ." bytes wasted on aligned PFA.\n" endif ;] execute

ur-meta:output-file-name count ur-meta:save-elf-binary
[:
  endcr ." new " ur-meta:tc-dynamic-binary if ." dynamic" else ." static" endif
  ."  binary (using "
  ur-meta:tc-wordhash-type case
    ur-meta:tc-nhash-elf of ." ELF" endof
    ur-meta:tc-nhash-joaat of ." JOAAT" endof
    ur-meta:tc-nhash-rot of ." ROT" endof
    ." WUTAFUCK?"
  endcase
  ."  vochash) created: " ur-meta:output-file-name count type
  cr
;] execute

[DEFINED] asm-labman:(mem-allocated) [IF]
asm-labman:(mem-allocated) @ . ." bytes used by assembler\n"
[ENDIF]
.stack
bye
