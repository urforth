;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

1024 64 * constant tc-(dbgbuf-maxsize) (hidden)
true value tc-(dbginfo-enabled?) (hidden)
false value tc-(dbginfo-active?) (hidden)
0 value tc-(dbgbuf-base-addr) (hidden)
0 value tc-(dbgbuf-end-addr) (hidden)
0 var tc-(dbgbuf-curr-addr) (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-debug-info-on  ( -- )
  \ tc-debugger-enabled to tc-(dbginfo-enabled?)
  tc-debug-info-enabled to tc-(dbginfo-enabled?)
;

: tc-debug-info-off  ( -- )
  false to tc-(dbginfo-enabled?)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(dbginfo-reset)  ( -- )
  tc-(dbgbuf-base-addr) tc-(dbgbuf-curr-addr) !
  false to tc-(dbginfo-active?)
  ;; set first line to 0 (to ease checks in "add-pc")
  tc-(dbgbuf-base-addr) ?dup if 0! endif
  \ tc-debugger-enabled to tc-(dbginfo-enabled?)
  ;; we still need debug info for backtraces
  tc-debug-info-enabled to tc-(dbginfo-enabled?)
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-debug-initialize  ( -- )
  tc-(dbgbuf-base-addr) ?abort" debuginfo writer already initialized"
  tc-(dbgbuf-maxsize) 0< ?abort" debuginfo writer: invalid config"
  tc-(dbgbuf-maxsize) ?dup if
    brk-alloc dup to tc-(dbgbuf-base-addr)
    tc-(dbgbuf-maxsize) + to tc-(dbgbuf-end-addr)
  else
    false to tc-(dbginfo-enabled?)
  endif
  tc-(dbginfo-reset)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this stores line and PC (in this order) as dwords
: tc-(dbginfo-add-pc)  ( pc line -- )
  ;; never put zero line
  ?dup ifnot drop exit endif
  ;; special? -1 items means "out of room"
  tc-(dbgbuf-curr-addr) @ ?dup ifnot 2drop exit endif
  ;; ( pc line dbgbufaddr )
  ;; check if the line is the same (if we have no lines, there will be zero)
  2dup @ = if drop 2drop exit endif
  ;; check if we have enough room
  dup 3 +cells tc-(dbgbuf-end-addr) u> if
    ;; out of buffer, abort debug info generation
    drop 2drop tc-(dbgbuf-curr-addr) 0!
    exit
  endif
  ;; put line and pc
  ;; ( pc line dbgbufaddr )
  swap over ! cell+  ;; line
  swap over ! cell+  ;; pc
  ;; put current line number to the next item (for equality check above)
  dup 2 -cells @ over !
  tc-(dbgbuf-curr-addr) !
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-(dbginfo-add-here)  ( -- )
  ;; put debug info
  tc-(dbginfo-active?) tc-(dbginfo-enabled?) logand if
    elf-current-pc ;; tc-here -- sorry
    tib-line# @ (tib-last-read-char) @ 10 = if 1- 0 max endif
    tc-(dbginfo-add-pc)
  endif
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
tc-debug-initialize
