;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; word name hashing
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

defer-not-yet tc-str-name-hash-real-addr  ( addr count -- u32hash )

:noname  ;; setup name hashing function
  tc-wordhash-type case
    tc-nhash-elf of ['] tc-str-name-hash-elf endof
    tc-nhash-joaat of ['] tc-str-name-hash-joaat endof
    tc-nhash-rot of ['] tc-str-name-hash-rot endof
    abort" unknown name hash type"
  endcase
  dup to tc-str-name-hash-real-addr execute
; to tc-str-name-hash-real-addr

: tc-str-name-hash  ( rva-addr count -- u32hash )
  swap tc->real swap tc-str-name-hash-real-addr
;

: tc-name-hash-fold-mask  ( u32hash -- folded-masked-hash )
  uhash32->8 -1 forth-hashtable-bits lshift bitnot and
;
