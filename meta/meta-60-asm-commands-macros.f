;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional assembler macros
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; convert low 4 bits of AL to hex digit ready to print
;; in: AL: nibble
;; out: AL: hex digit ready to print (uppercased)
;; it's voodoo %-) can somebody explain this code? %-)
;; heh, it's one byte shorter than the common snippets i've seen
;; many times in teh internets.
;; actually, this is the code from my Z80 library. %-)
: Nibble2Hex  ( -- )
  " and   al,0Fh" asm-str
  " cmp   al,0Ah" asm-str
  " sbb   al,69h" asm-str
  " das" asm-str ;; yeah, yeah, das ist fantastich!
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: SAVE_ALL_REGS_NO_EAX_F  ( -- )
  ;; generate code
  " push  ebx" asm-str
  " push  ecx" asm-str
  " push  edx" asm-str
  " push  esi" asm-str
  " push  edi" asm-str
  " push  ebp" asm-str
;

: RESTORE_ALL_REGS_NO_EAX_F  ( -- )
  ;; generate code
  " pop   ebp" asm-str
  " pop   edi" asm-str
  " pop   esi" asm-str
  " pop   edx" asm-str
  " pop   ecx" asm-str
  " pop   ebx" asm-str
;

: SAVE_ALL_REGS  ( -- )
  ;; generate code
  " pushfd" asm-str
  " pushad" asm-str
;

: RESTORE_ALL_REGS  ( -- )
  ;; generate code
  " popad" asm-str
  " popfd" asm-str
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (emit-common)  ( callstr-addr callstr-count regstr-addr regstr-count -- )
  tk-id? if
    2dup lexer:tkvalue count s= if
      2drop
      asm-str
      lexer:NextToken
      exit
    endif
  endif
  ;; prepare macro load
  " push  eax" asm-str
\ lexer:(dump-current-token) cr
  macro-instrs:(init-macro-buf)
  " ld " macro-instrs:(s>macro-buf)
  macro-instrs:(s>macro-buf)  ;; register
  [char] , macro-instrs:(c>macro-buf)
  macro-instrs:(load-tib-line-to-macro-buf)
  macro-instrs:(asm-macro-buf)
  asm-str
  " pop   eax" asm-str
; (hidden)


;; print char
;; all registers are preserved, including flags
: urfsegfault_emit  ( -- )
  " call  urfsegfault_emit_al"  " AL" (emit-common)
;

;; print newline
;; all registers are preserved, including flags
: urfsegfault_cr  ( -- )
  ;; prepare macro load
  " push  eax" asm-str
  " ld    al,10" asm-str
  " call  urfsegfault_emit_al" asm-str
  " pop   eax" asm-str
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; print byte in hex
;; all registers are preserved, including flags
: urfsegfault_printhex_byte  ( -- )
  " call  urfsegfault_emit_hex_al"  " AL" (emit-common)
;

;; print register in hex
;; all registers are preserved, including flags
: urfsegfault_printhex  ( -- )
  " call  urfsegfault_emit_hex_eax"  " EAX" (emit-common)
;

;; print register as signed decimal
;; all registers are preserved, including flags
: urfsegfault_printdec  ( -- )
  " call  urfsegfault_emit_dec_eax"  " EAX" (emit-common)
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: (printstr-common)  ( addnlflag -- )
  ;; reserve room for jump (only short for now)
  0xeb asm-c,
  0 asm-c,
  asm-PC >r
  ;; put string
  1 macro-instrs:(defx)
  ;; put eol?
  if 10 asm-c, endif
  ;; calculate message length
  asm-PC r@ -
  ;; fix jump
  dup r@ 1- asm-c!
  ;; ( asmlen | asmaddr )
  " push  ecx" asm-str
  " push  edx" asm-str
  ;; load start addr
  macro-instrs:(init-macro-buf)
  " ld    ecx," macro-instrs:(s>macro-buf)
  r> macro-instrs:(u>macro-buf)
  macro-instrs:(asm-macro-buf)
  ;; load length
  macro-instrs:(init-macro-buf)
  " ld    edx," macro-instrs:(s>macro-buf)
  macro-instrs:(u>macro-buf)
  macro-instrs:(asm-macro-buf)
  " call  urfsegfault_emit_str_ecx_edx" asm-str
  " pop   edx" asm-str
  " pop   ecx" asm-str
; (hidden)

;; print string
;; string data immediately follows the code
;; all registers are preserved, including flags
: urfsegfault_printstr  ( -- )
  0 (printstr-common)
;

;; print string with a newline
;; string data immediately follows the code
;; all registers are preserved, including flags
: urfsegfault_printstrnl  ( -- )
  1 (printstr-common)
;
