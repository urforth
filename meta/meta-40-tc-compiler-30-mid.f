;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mid-level target compiler words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

0 var tc-state
(*
0 var tc-(csp)

: tc-!CSP  ( -- )
  sp@ tc-(csp) !
;

: tc-?CSP  ( -- )
    \ endcr sp@ .hex8 space tc-(csp) @ .hex8 space  sp@ tc-(csp) @ - . cr
  sp@ tc-(csp) @ - err-unfinished-definition ?error
;

;; CSP check for loops
: tc-csp-loop  ( -- )
  sp@ tc-(csp) @ u> err-unpaired-conditionals ?error
;
*)

: tc-?COMP  ( -- )
  tc-state @ not err-compilation-only ?error
;

: tc-?NON-MACRO  ( -- )
  tc-latest-macro? err-nonmacro-only ?error
;

: tc-?EXEC  ( -- )
  tc-state @ err-execution-only ?error
;

: tc-?stack  ( -- )
  depth 0<= err-unpaired-conditionals ?error
;

: tc-?pairs  ( n1 n2 -- )
  <> err-unpaired-conditionals ?error
;

: tc-?any-pair  ( id v0 v1 -- )
  >r over <>
  swap r> <>
  and err-unpaired-conditionals ?error
;

: tc-?pairs-any-keepid  ( id v0 v1 -- id )
  >r over <>  ;; ( id v0<>id | v1 )
  over r> <>  ;; ( id v0<>id v1<>id )
  and err-unpaired-conditionals ?error
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; usage:
;;  compile 0branch
;;  (mark>)
;;  ...
;;  (resolve>)
;;
;;  (<mark)
;;  ...
;;  compile branch
;;  (<resolve)

;; write "branch to destaddr" address to addr
: tc-(branch-addr!)  ( rva-destaddr rva-addr -- )  tc-! ;
: tc-(branch-addr@)  ( rva-destaddr -- rva-addr )  tc-@ ;


;; reserve room for branch address, return addr suitable for "tc-(resolve-j>)"
: tc-(mark-j>)  ( -- rva-addr )
  tc-here 0 tc-,
;

;; compile "forward jump" from address to HERE
;; addr is the result of "tc-(mark-j>)"
: tc-(resolve-j>)  ( rva-addr -- )
  tc-here swap tc-(branch-addr!)
;


;; return addr suitable for "tc-(<j-resolve)"
: tc-(<j-mark)  ( -- rva-addr )
  tc-here
;

;; patch "forward jump" address to HERE
;; addr is the result of "tc-(<j-mark)"
: tc-(<j-resolve)  ( rva-addr -- )
  cell tc-n-allot tc-(branch-addr!)
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; each of these has one argument
enum{
  1 set
  value tc-(ctlid-if)
  value tc-(ctlid-else)

  value tc-(ctlid-begin)
  value tc-(ctlid-while)

  value tc-(ctlid-case)
  value tc-(ctlid-of)
  value tc-(ctlid-endof)
  value tc-(ctlid-otherwise)

  value tc-(ctlid-do)
  value tc-(ctlid-do-break)
  value tc-(ctlid-do-continue)

  value tc-(ctlid-cblock)
  value tc-(ctlid-cblock-interp)

  value tc-(ctlid-?do)

  value tc-(ctlid-colon)
  value tc-(ctlid-does)

  666 +set
  value tc-(CTLID-SC-COLON)  (hidden)
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; jumpcfa-type:
enum{
  value tc-type-branch
  value tc-type-0branch
  value tc-type-tbranch
  value tc-type-+0branch
  value tc-type--0branch
  value tc-type-+branch
  value tc-type--branch
}

: tc-(compile-typed-branch)  ( jumpcfa-type  -- )
  case
    tc-type-branch of tc-compile branch endof
    tc-type-0branch of tc-compile 0branch endof
    tc-type-tbranch of tc-compile tbranch endof
    tc-type-+0branch of tc-compile +0branch endof
    tc-type--0branch of tc-compile -0branch endof
    tc-type-+branch of tc-compile +branch endof
    tc-type--branch of tc-compile -branch endof
    abort" tc-(compile-typed-branch): wut?!"
  endcase
; (hidden)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; this is NOT immediate!
: tc-LITERAL  ( n -- )
  tc-state @ not-?abort" tc-LITERAL: compile-time only!"
    \ " FORTH:LIT" tc-compile,-(str) tc-, exit
  tc-align-pfa if
    dup case
       0 of drop " FORTH:LIT0" tc-compile,-(str) endof
       1 of drop " FORTH:LIT1" tc-compile,-(str) endof
      -1 of drop " FORTH:LIT-1" tc-compile,-(str) endof
      " FORTH:LIT" tc-compile,-(str) tc-,
    endcase
  else
    dup case
       0 of drop " FORTH:LIT0" tc-compile,-(str) endof
       1 of drop " FORTH:LIT1" tc-compile,-(str) endof
      -1 of drop " FORTH:LIT-1" tc-compile,-(str) endof
           0   256 within-of " FORTH:LITU8" tc-compile,-(str) tc-c, endof
        -128   128 within-of " FORTH:LITS8" tc-compile,-(str) tc-c, endof
           0 65536 within-of " FORTH:LITU16" tc-compile,-(str) tc-w, endof
      -32768 32768 within-of " FORTH:LITS16" tc-compile,-(str) tc-w, endof
      " FORTH:LIT" tc-compile,-(str) tc-,
    endcase
  endif
; ( immediate )

: tc-(putstrz)  ( addr count rva-dest )
  tc->real swap 2dup + >r move r> 0c!
;

;; always align after string literals
: tc-align-after-strlit  ( -- )
  begin tc-here 3 and while 0 tc-c, repeat
;

;; this is NOT immediate!
;; addr is NOT rva!
: tc-(c4strz)  ( addr count -- )
  dup cell+ 1+ tc-n-allot  ;; ( addr count rva-dest )
  2dup tc-! cell+
  tc-(putstrz) tc-align-after-strlit
; ( immediate )

;; this is NOT immediate!
;; addr is NOT rva!
: tc-(c1strz)  ( addr count -- )
  dup 0 255 bounds? not-?abort" invalid c1 string length"
  dup 2+ tc-n-allot  ;; ( addr count rva-dest )
  2dup tc-c! 1+
  tc-(putstrz) tc-align-after-strlit
; ( immediate )

;; this is NOT immediate!
;; addr is NOT rva!
: tc-C4SLITERAL  ( addr count -- )
  tc-state @ not-?abort" tc-C4sLITERAL: compile-time only!"
  " LITC4STR" tc-compile,-(str)
  tc-(c4strz) tc-align-after-strlit
; ( immediate )

;; this is NOT immediate!
;; addr is NOT rva!
;; generate byte-counted string literal if possible
: tc-SLITERAL  ( addr count -- )
  tc-state @ not-?abort" tc-SLITERAL: compile-time only!"
  dup 0 255 bounds? if
    " LITC1STR" tc-compile,-(str)
    tc-(c1strz)
  else
    " LITC4STR" tc-compile,-(str)
    tc-(c4strz)
  endif
; ( immediate )
