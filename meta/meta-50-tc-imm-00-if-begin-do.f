;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; immediate metacompiler words
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: tc-IF
  tc-?comp
  tc-jpush-xbrn
  tc-compile 0branch
  tc-(mark-j>)
  tc-(ctlid-if)
; ( immediate )

: tc-IFNOT
  tc-?comp
  tc-jpush-xbrn
  tc-compile tbranch
  tc-(mark-j>)
  tc-(ctlid-if)
; ( immediate )

;; if negative (not zero)
: tc--IF
  tc-?comp
  tc-jpush-xbrn
  tc-compile +0branch
  tc-(mark-j>)
  tc-(ctlid-if)
; ( immediate )

;; if positive (not zero)
: tc-+IF
  tc-?comp
  tc-jpush-xbrn
  tc-compile -0branch
  tc-(mark-j>)
  tc-(ctlid-if)
; ( immediate )

;; if negative or zero
: tc--0IF
  tc-?comp
  tc-jpush-xbrn
  tc-compile +branch
  tc-(mark-j>)
  tc-(ctlid-if)
; ( immediate )

;; if positive or zero
: tc-+0IF
  tc-?comp
  tc-jpush-xbrn
  tc-compile -branch
  tc-(mark-j>)
  tc-(ctlid-if)
; ( immediate )

: tc-ELSE
  tc-?comp tc-(ctlid-if) tc-?pairs
  tc-jpush-brn
  tc-compile branch
  tc-(mark-j>)
  swap tc-(resolve-j>)
  tc-(ctlid-else)
; ( immediate )

: tc-ENDIF
  tc-?comp
  tc-(ctlid-if) tc-(ctlid-else) tc-?any-pair
  tc-(resolve-j>)
; ( immediate )

alias tc-endif tc-then  ( immediate )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; you can use as many "while" blocks as you want to
;; any loop can be finished with AGAIN/REPEAT/UNTIL
;; "BREAK" and "CONTINUE" cannot be used inside conditionals yet
;;

: tc-BEGIN
  tc-?comp
  tc-(<j-mark)
  tc-(ctlid-begin)
; ( immediate )


;; there might be a lot of "while" blocks, pop them all
;; compile jump back to begin
: (END-BEGIN)  ( pairs... jumpcfa-type  -- )
  ;; this is done recursively, because this way i can get rid of `par_resolve_jfwd_over_branch`
  ;; also, we don't have working loops at this point, so recursion is the only choice ;-)
  tc-?stack
  over tc-(ctlid-begin) =
  if
    dup tc-type-branch = if tc-jpush-brn else tc-jpush-xbrn endif
    tc-(compile-typed-branch)
    tc-(ctlid-begin) tc-?pairs
    tc-(<j-resolve)
  else
    swap
    tc-(ctlid-while) tc-?pairs
    swap >r recurse r>
    tc-(resolve-j>)
  endif
; (hidden)

;; repeats while the condition is false
: tc-UNTIL
  tc-?comp
  tc-type-0branch (end-begin)
; ( immediate )

;; repeats while the condition is true
: tc-NOT-UNTIL
  tc-?comp
  tc-type-tbranch (end-begin)
; ( immediate )

;; repeats while the condition is positive
: tc--UNTIL
  tc-?comp
  tc-type-+0branch (end-begin)
; ( immediate )

;; repeats while the condition is negative
: tc-+UNTIL
  tc-?comp
  tc-type--0branch (end-begin)
; ( immediate )

;; repeats while the condition is positive or zero
: tc--0UNTIL
  tc-?comp
  tc-type-+branch (end-begin)
; ( immediate )

;; repeats while the condition is negative or zero
: tc-+0UNTIL
  tc-?comp
  tc-type--branch (end-begin)
; ( immediate )

: tc-AGAIN
  tc-?comp
  tc-type-branch (end-begin)
; ( immediate )

alias tc-AGAIN tc-REPEAT  ( immediate )

: (COMP-WHILE)  ( jumpcfa-type )
  tc-?comp
  >r tc-(ctlid-begin) tc-(ctlid-while) tc-?pairs-any-keepid r>
  dup tc-type-branch = if tc-jpush-brn else tc-jpush-xbrn endif
  tc-(compile-typed-branch)
  tc-(mark-j>)
  tc-(ctlid-while)
; (hidden)

: tc-WHILE
  tc-type-0branch (comp-while)
; ( immediate )

: tc-NOT-WHILE
  tc-type-tbranch (comp-while)
; ( immediate )

: tc--WHILE
  tc-type-+0branch (comp-while)
; ( immediate )

: tc-+WHILE
  tc-type--0branch (comp-while)
; ( immediate )

: tc--0WHILE
  tc-type-+branch (comp-while)
; ( immediate )

: tc-+0WHILE
  tc-type--branch (comp-while)
; ( immediate )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; alas, i had to use one global var
;; <>0: drop when we'll see CASE
;; set to 0 by tc-(ctlid-of) or tc-(ctlid-otherwise)
0 value (B/C-CASE-DROP)  (hidden)

;; workhorse for break/continue
;; type:
;;   0: break
;;   1: continue
: (BREAK/CONTINUE)  ( type )
  tc-?comp
  0 2>r  ;; type and counter
  ;; drop on case by default
  1 to (b/c-case-drop)
  begin
    tc-?stack
    ;; check for valid ctlid
    dup tc-(ctlid-do-continue) > over tc-(ctlid-if) < or ERR-INVALID-BREAK-CONT ?error
    ;; while not begin and not do
    dup tc-(ctlid-begin) = over tc-(ctlid-do) = or not
  while
    ;; move to rstack
      ;; DEBUG
      ;; 2dup pardottype "SAVE: ctlid: " dot  pardottype " addr: " udot cr
    ;; process case:
    ;;   if we're in tc-(ctlid-case) or in tc-(ctlid-endof), compile DROP
    dup tc-(ctlid-case) = if
      (b/c-case-drop) if
          ;; DEBUG
          ;; pardottype " compiling DROP (" dup dot pardottype ")" cr
        tc-compile drop
      endif
      ;; drop on next case by default
      1 to (b/c-case-drop)
    endif
    dup tc-(ctlid-of) = over tc-(ctlid-otherwise) = or
    if
      ;; don't drop on next case by default
      0 to (b/c-case-drop)
    endif
    2r> 2swap >r >r 2+ 2>r
  repeat
  ;; return stack contains saved values and counter
  dup tc-(ctlid-do) =
  if
    ;; do...loop
    ;; check type
    1 rpick  ;; peek the type
    if
        ;; DEBUG
        ;; pardottype "DO/LOOP: continue" cr
      ;; coninue: jump to (LOOP)
      tc-jpush-brn
      tc-compile branch
      tc-(mark-j>)
      tc-(ctlid-do-continue)
    else
      ;; break: drop do args, jump over (LOOP)
        ;; DEBUG
        ;; pardottype "DO/LOOP: break" cr
      tc-compile unloop  ;; remove do args
      tc-jpush-brn
      tc-compile branch
      tc-(mark-j>)
      tc-(ctlid-do-break)
    endif
  else
    tc-(ctlid-begin) tc-?pairs
    ;; check type
    1 rpick  ;; i.e. peek the type
    if
      ;; coninue
        ;; DEBUG
        ;; pardottype "BEGIN: continue" cr
      dup            ;; we still need the address
      tc-jpush-brn
      tc-compile branch
      tc-(<j-resolve)
      tc-(ctlid-begin)    ;; restore ctlid
    else
      ;; break
        ;; DEBUG
        ;; pardottype "BEGIN: break" cr
      tc-(ctlid-begin)    ;; restore ctlid
      tc-jpush-brn
      tc-compile branch
      tc-(mark-j>)
      tc-(ctlid-while)
    endif
  endif

  ;; move saved values back to the data stack
  r> rdrop  ;; drop type
    ;; DEBUG
    ;; dup pardottype "RESTORE " dot pardottype "items" cr
  begin
    ?dup
  while
    r> swap 1 -
  repeat
    ;; DEBUG
    ;; dup . over udot cr
; (hidden)

: tc-CONTINUE
  1 (break/continue)
; ( immediate )

: tc-BREAK
  0 (break/continue)
; ( immediate )

;; this has to be here
alias tc-BREAK tc-LEAVE  ( immediate )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; data stack:
;;   0 tc-(ctlid-case)
;;     addr tc-(ctlid-of) -- when in "OF"
;;     addr tc-(ctlid-endof) -- when "ENDOF" compiled
;;     0 tc-(ctlid-otherwise) -- when "OTHERWISE" compiled
;; note that "tc-(ctlid-endof)"s will be accumulated, and resolved in "ENDCASE"
;;

: tc-CASE
  tc-?comp
  0 tc-(ctlid-case)  ;; with dummy argument
; ( immediate )

: (X-OF)  ( ... word-to-compare-addr word-to-compare-count )
  tc-?comp
  2>r  ;; save XOF args
  tc-(ctlid-case) tc-(ctlid-endof) tc-?pairs-any-keepid   ;; we should be in normal CASE
  \ tc-compile over  ;; special compare words will do this for us
  2r> tc-compile,-(str)  ;; comparator
  \ tc-jpush-xbrn  ;; there is no real reason to check such jumps; and we may have A LOT of them
  tc-compile 0branch-drop
  tc-(mark-j>)
  tc-(ctlid-of)
; (hidden)

: tc-OF  " (OF=)" (x-of) ; ( immediate )
: tc-NOT-OF  " (OF<>)" (x-of) ; ( immediate )
: tc-<OF  " (OF<)" (x-of) ; ( immediate )
: tc-<=OF  " (OF<=)" (x-of) ; ( immediate )
: tc->OF  " (OF>)" (x-of) ; ( immediate )
: tc->=OF  " (OF>=)" (x-of) ; ( immediate )
: tc-U<OF  " (OF-U<)" (x-of) ; ( immediate )
: tc-U<=OF  " (OF-U<=)" (x-of) ; ( immediate )
: tc-U>OF  " (OF-U>)" (x-of) ; ( immediate )
: tc-U>=OF  " (OF-U>=)" (x-of) ; ( immediate )
: tc-&OF  " (OF-and)" (x-of) ; ( immediate )
: tc-AND-OF  " (OF-and)" (x-of) ; ( immediate )
: tc-~AND-OF  " (OF-~and)" (x-of) ; ( immediate )
: tc-WITHIN-OF  " (OF-WITHIN)" (x-of) ; ( immediate )
: tc-UWITHIN-OF  " (OF-UWITHIN)" (x-of) ; ( immediate )
: tc-BOUNDS-OF  " (OF-BOUNDS)" (x-of) ; ( immediate )

: tc-ENDOF
  tc-?comp tc-(ctlid-of) tc-?pairs
  tc-jpush-brn
  tc-compile branch
  tc-(mark-j>)
  swap tc-(resolve-j>)
  tc-(ctlid-endof)
; ( immediate )

: tc-OTHERWISE
  tc-(ctlid-case) tc-(ctlid-endof) tc-?pairs-any-keepid
  0 tc-(ctlid-otherwise)
; ( immediate )

: tc-ENDCASE
  tc-?comp
  dup tc-(ctlid-otherwise) =
  if
    ;; "otherwise", no drop needed
    tc-(ctlid-otherwise) tc-?pairs
    0 tc-?pairs  ;; check dummy argument
  else
    ;; no "otherwise", compile DROP
    tc-compile drop
  endif
  ;; patch branches
  begin
    tc-?stack
    dup tc-(ctlid-case) <>
  while
    tc-(ctlid-endof) tc-?pairs
    tc-(resolve-j>)
  repeat
  tc-(ctlid-case) tc-?pairs
  0 tc-?pairs  ;; check dummy argument
; ( immediate )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: tc-DO
  tc-?comp
  tc-compile (do)
  tc-(<j-mark)
  tc-(ctlid-do)
; ( immediate )

;; endloopcfa-type
;;   0: (loop)
;;   1: (+loop)
: (END-LOOP)  ( endloopcfa-type )
  ;; this is done recursively, because this way i can get rid of `par_resolve_jfwd_over_branch`
  tc-?stack
  over tc-(ctlid-do) =
  if
    \ tc-compile,
    case
      0 of tc-compile (loop) endof
      1 of tc-compile (+loop) endof  ;; +)
      abort" (end-loop): wut?!"
    endcase
    tc-(ctlid-do) tc-?pairs
    tc-(<j-resolve)
    ;; resolve ?DO jump, if it is there
    dup tc-(ctlid-?do) = if drop tc-(resolve-j>) endif
  else
    ;; "continue" should be compiled before recursion, and "break" after it
    swap
    dup tc-(ctlid-do-continue) =
    if
      ;; patch "continue" branch
      tc-(ctlid-do-continue) tc-?pairs
      swap tc-(resolve-j>)
      recurse
    else
      tc-(ctlid-do-break) tc-?pairs
      swap >r recurse r>
      ;; here, loop branch already compiled
      tc-(resolve-j>)
    endif
  endif
; (hidden)

: tc-LOOP
  tc-?comp
  0 (end-loop)
; ( immediate )

: tc-+LOOP
  tc-?comp
  1 (end-loop)
; ( immediate )

: tc-?DO
  tc-?comp
  tc-jpush-xbrn
  tc-compile ?do-branch
  tc-(mark-j>)
  tc-(ctlid-?do)
  [compile] tc-do
; ( immediate )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; "FOR" is for loops with "step by 1", from 0
: tc-FOR
  tc-?comp
  tc-jpush-xbrn
  tc-compile (for)
  tc-(mark-j>)
  tc-(CTLID-?DO)
  tc-(<j-mark)
  tc-(CTLID-DO)
; ( immediate )

: tc-ENDFOR
  [compile] tc-loop
; ( immediate )
