;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; additional assembler commands
;; WARNING! there should be NO argument-less asm commands
;;          they will break the parser!
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; app entry point
: $ENTRY  ( -- )
  Reset-Instruction
  4 to *OpSize
  Imm
  ;; label?
  *Imm elf-entry-point-addr !
  *OpReloc if
    *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
    *ImmName bcount *ImSize *ImmForthType
    ;; HACK!
    elf-current-pc >r
    elf-entry-point-addr real->tc to elf-current-pc
    asm-Label-Fixup
    r> to elf-current-pc
  endif
;

: $ALIGN  ( -- )  macro-instrs:align ;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ($VAR-ETC-CREATE)  ( tc-cfa -- )
  ;; get name
  tk-str? ERRID_ASM_STRING_EXPECTED not-?asm-error
  ;; create word header and CFA
  lexer:tkvalue count 2dup upcase-str rot execute
  lexer:NextToken
; (hidden)

: ($VAR-COMPILE-VALUE)  ( -- )
  ;; parse word value
  Reset-Instruction
  4 to *OpSize
  Imm
  ;; label?
  *OpReloc if
    ;; create label fixup
    *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
    *ImmName bcount *ImSize *ImmForthType asm-Label-Fixup
  endif
  ;; put value
  *Imm tc-,
  tc-create;
; (hidden)

: ($VAR-ETC)  ( tc-cfa -- )
  ($VAR-ETC-CREATE)
  ($VAR-COMPILE-VALUE)
; (hidden)

;; this one doesn't take a value
: $DVAR         ( -- )  ['] tc-(variable-header-str) ($VAR-ETC-CREATE) 0 tc-, 0 tc-, tc-create; ;

: $VARIABLE     ( -- )  ['] tc-(variable-header-str) ($VAR-ETC) ;
: $CONSTANT     ( -- )  ['] tc-(constant-header-str) ($VAR-ETC) ;
: $VALUE        ( -- )  ['] tc-(value-header-str) ($VAR-ETC) ;
: $DEFER        ( -- )  ['] tc-(defer-header-str) ($VAR-ETC) ;

: $USERVAR  ( -- )
  tc-tls-type tc-tls-fs = if
    4 ['] tc-(uservar-header-str) ($VAR-ETC-CREATE)
    ;; create offset constant
    tk-id? ERRID_ASM_LABEL_EXPECTED not-?asm-error
    dup lexer:tkvalue count rot asmx86:asm-Make-Label
    lexer:NextToken
    ;; parse value
    Reset-Instruction
    4 to *OpSize
    Imm
    ;; label?
    *OpReloc if
      ;; create label fixup
      *ImSize 4 = ERRID_ASM_EXPECT_32BIT_OPERAND not-?asm-error
      ;; hack!
      asmx86:asm-PC >r dup to elf-current-pc
      *ImmName bcount *ImSize *ImmForthType asm-Label-Fixup
      r> to elf-current-pc
    endif
    ;; put value
    *Imm swap tc-get-ua-rva tc-!
    tc-create;
  else
    ['] tc-(variable-header-str) ($VAR-ETC-CREATE)
    ;; create offset constant
    tk-id? ERRID_ASM_LABEL_EXPECTED not-?asm-error
    lexer:tkvalue count asmx86:asm-PC asmx86:asm-Make-Label
    lexer:NextToken
    ($VAR-COMPILE-VALUE)
  endif
;

: $ALLOT  ( -- )
  ;; parse word value
  Reset-Instruction
  4 to *OpSize
  Imm
  ;; label?
  *OpReloc ?abort" cannot use labels in $allot"
  *Imm tc-n-allot drop
;

: $USERALLOT  ( -- )
  ;; parse word value
  Reset-Instruction
  4 to *OpSize
  Imm
  ;; label?
  *OpReloc ?abort" cannot use labels in $userallot"
  *Imm
  tc-tls-type tc-tls-fs = if
    tc-(userval-allot)
  else
    tc-n-allot drop
  endif
;


: $VOCABHEADER  ( -- )
  ['] tc-(vocab-header-str) ($VAR-ETC)
  tc-create;
  ;; patch wordlist name pointer
  *OpReloc if *ImmLabelDefined else true endif
  if
    ;; address is known, fix name pointer
    tc-latest-nfa
    \ tc-here cell- tc-@  ;; get wordlist address
    *Imm                  ;; our wordlist address lives here too ;-)
    tc-vocid->headnfa
    ;; sanity check
    dup tc-@ ?abort" trying to create two headers for one wordlist"
    tc-!
  else
    ;; address is unknown
    ?abort" cannot create vocabulary headers with forwards (yet)"
  endif
;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
true value tc-use-longer-next

-1 value tc-has-debugger?  (hidden)
0 value tc-urforth-next-ptr  (hidden)

: (ur-has-dbg?)  ( -- flag )
  tc-has-debugger? -if
    " URFORTH_DEBUG" asmx86:asm-Get-Constant if
      notnot to tc-has-debugger?
      tc-has-debugger? if
        \ " urforth_next_ptr" asmx86:asm-Get-Label 1 <> ?abort" \`urforth_next_ptr\` must be defined"
        " urforth_next" asmx86:asm-Get-Label 1 <> ?abort" \`urforth_next\` must be defined"
        to tc-urforth-next-ptr
      endif
    endif
  endif
  tc-has-debugger?
; (hidden)


: tc-NEXT  ( -- )
\ \ \   \ " lodsd" asm-str
\ \ \   $AD asm-c,
\ \ \   \ old: either "jmp eax" of "jmp dword [nextref]"
\ \ \   \ new: either "jmp eax" of "jmp tc-urforth-next-ptr"
\ \ \   (ur-has-dbg?) +if
\ \ \     \ $FF asm-c,
\ \ \     \ $25 asm-c, tc-urforth-next-ptr asm-,
\ \ \     $E9 asm-c, tc-urforth-next-ptr asm-pc 4+ - asm-,
\ \ \   else
\ \ \     $E0FF asm-w,
\ \ \   endif
  (ur-has-dbg?) +if
    \ " lodsd" asm-str
    $AD asm-c,
    \ old: either "jmp eax" of "jmp dword [nextref]"
    \ new: either "jmp eax" of "jmp tc-urforth-next-ptr"
    \ $FF asm-c,
    \ $25 asm-c, tc-urforth-next-ptr asm-,
    $E9 asm-c, tc-urforth-next-ptr asm-pc 4+ - asm-,
  else
    tc-use-longer-next if
      \ !\ mov  eax, esi
      \ !$C68B asm-w,
      \ !\ lea  esi, [esi+4]
      \ !$768D asm-w, $04 asm-c,
      \ !\ jmp  [eax]
      \ !$20FF asm-w,

      \ lea  esi, [esi+4]
      $768D asm-w, $04 asm-c,
      \ jmp  [esi-4]
      $66FF asm-w, $FC asm-c,
    else
      \ lodsd
      $AD asm-c,
      \ jmp eax
      $E0FF asm-w,
    endif
  endif
; (hidden)

replace asmx86:macro-instrs:NEXT tc-NEXT
