;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

: tc-$LABEL  ( -- ) \ name
  parse-name dup not-?abort" label name?"
  2dup + 1- c@ [char] : = ?abort" label should not end with colon"
    \ 2dup endcr ." NEW LABEL: " type cr
  asmx86:asm-PC asmx86:asm-Make-Label
;


: tc-$INCLUDE  ( -- )
  parse-name dup not-?abort" include name?"
  include-file
;
