;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UrForth level 1: self-hosting 32-bit Forth compiler
;; Metacompiler
;; Copyright (C) 2020 Ketmar Dark // Invisible Vector
;; GPLv3 ONLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: ,    ( n -- )  tc-, ;
: w,   ( n -- )  tc-w, ;
: c,   ( n -- )  tc-c, ;

;; the following can be customized for special builds
: reladdr, ( cfa -- )  tc-reladdr, ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: c@  ( addr -- value )  tc-c@ ;
: w@  ( addr -- value )  tc-w@ ;
: @  ( addr -- value )  tc-@ ;

: c!  ( value addr -- )  tc-c! ;
: w!  ( value addr -- )  tc-w! ;
: !  ( value addr -- )  tc-! ;

: +c!  ( value addr -- )  dup tc-c@ rot + swap tc-c! ;
: +w!  ( value addr -- )  dup tc-w@ rot + swap tc-w! ;
: +!  ( value addr -- )  dup tc-@ rot + swap tc-! ;

: -c!  ( value addr -- )  dup tc-c@ rot - swap tc-c! ;
: -w!  ( value addr -- )  dup tc-w@ rot - swap tc-w! ;
: -!  ( value addr -- )  dup tc-@ rot - swap tc-! ;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sorry for this hack!

: constant  ( val -- )  \ name
  tc-(?not-enum)
  parse-name tc-(constant-header-str) tc-, tc-create;
;

: var  ( val -- )  \ name
  tc-(?not-enum)
  parse-name tc-(variable-header-str) tc-, tc-create;
;

: variable  ( val -- )  \ name
  tc-(?not-enum)
  parse-name tc-(variable-header-str) 0 tc-, tc-create;
;

: value  ( val -- )  \ name
  parse-name tc-(enum-mode) if
    ( etype enextvalue addr count )
    tc-(constant-header-str)
    dup nrot  ;; for "tc-,"
    over ?dup if + else ?dup if 1 lshift else 1 endif endif  ;; advance current value
    rot
  else
    tc-(value-header-str)
  endif
  tc-, tc-create;
;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
: }  ( etype evalue -- )
  tc-(?enum)
  2drop false to tc-(enum-mode)
;

: set      ( etype evalue newvalue -- etype newvalue )  tc-(?enum) nip ;
: set-bit  ( etype evalue newbit -- etype 1<<newbit )  tc-(?enum) nip 1 swap lshift ;
: -set     ( etype evalue delta -- etype evalue-delta ) tc-(?enum) - ;
: +set     ( etype evalue delta -- etype evalue+delta ) tc-(?enum) + ;

: enum{  ( -- etype enextvalue )  tc-(?not-enum) 1 0 true to tc-(enum-mode) ;
: bitenum{  ( -- etype enextvalue )  tc-(?not-enum) 0 1 true to tc-(enum-mode) ;
